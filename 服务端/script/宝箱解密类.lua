local 宝箱解密类 = class()

function 宝箱解密类:初始化()
end

function 宝箱解密类:开启活动()
	宝箱解密开关 = true
	self.密码表 = {}
	self.时间表 = {}
	self.耗时表 = {}
	self.回合表 = {}
	self.数据表 = {}
end

function 宝箱解密类:数据处理(id, 参数, 序号, 内容)
	if 序号 == 1 then
		if 宝箱解密开关 == false then
			发送数据(玩家数据[id].连接id, 7, "#y/本活动开启时间为每日18点-19点")

			return 0
		elseif 玩家数据[id].角色.数据.等级 < 60 then
			发送数据(玩家数据[id].连接id, 7, "#y/只有等级达到60级的玩家才可参加本活动")

			return 0
		elseif self.数据表[id] ~= nil and self.数据表[id] >= 5 then
			发送数据(玩家数据[id].连接id, 7, "#y/每日只能进行5次猜数字游戏")

			return 0
		else
			if self.数据表[id] == nil then
				self.数据表[id] = 0
			else
				self.数据表[id] = self.数据表[id] + 1
			end

			self:生成钥匙(玩家数据[id].数字id)
			发送数据(玩家数据[id].连接id, 10003, "1")
		end
	elseif 序号 == 42 then
		self.收到信息 = {}
		self.临时信息 = 分割文本(内容, "*-*")

		for n = 1, 4 do
			self.收到信息[n] = self.临时信息[n] + 0
		end

		self:选定处理(id, self.收到信息)
	end
end

function 宝箱解密类:选定处理(id, 密码)
	if self.回合表[玩家数据[id].数字id] >= 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/你已经耗尽了回合数，请重新开始游戏")

		return 0
	end

	self.是否通关 = false
	self.正确数量 = 0
	self.完美数量 = 0
	self.显示文本 = ""
	self.正确密码 = ""
	self.回合表[玩家数据[id].数字id] = self.回合表[玩家数据[id].数字id] + 1

	for n = 1, 4 do
		self.显示文本 = self.显示文本 .. 密码[n]

		for i = 1, 4 do
			if 密码[n] == self.密码表[玩家数据[id].数字id][i] then
				self.正确数量 = self.正确数量 + 1
			end
		end

		if 密码[n] == self.密码表[玩家数据[id].数字id][n] then
			self.完美数量 = self.完美数量 + 1
		end
	end

	for n = 1, 4 do
		self.正确密码 = self.正确密码 .. self.密码表[玩家数据[id].数字id][n]
	end

	发送数据(玩家数据[id].连接id, 10004, "第" .. self.回合表[玩家数据[id].数字id] .. "回：" .. self.显示文本 .. "，正确" .. self.正确数量 .. ",完美" .. self.完美数量)
	发送数据(玩家数据[id].连接id, 7, "#y/当前耗时#r/" .. os.time() - self.时间表[玩家数据[id].数字id] .. "#y/秒")

	if self.完美数量 == 4 then
		发送数据(玩家数据[id].连接id, 7, "#y/恭喜你成功破解了密码")
		发送数据(玩家数据[id].连接id, 7, "#y/正确密码为#r/" .. self.正确密码)

		self.耗时表[玩家数据[id].数字id] = os.time() - self.时间表[玩家数据[id].数字id]
		self.奖励参数 = 1.5
		self.奖励数量 = 0

		if self.耗时表[玩家数据[id].数字id] <= 70 then
			self.奖励参数 = 0.8
			self.奖励数量 = 2

			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在帮助钱庄老板破解宝箱密码时，居然以#r/" .. self.耗时表[玩家数据[id].数字id] .. "#y/秒的惊人速度成功破解出密码，获得了150%的奖励以及天眼通符*10")
		elseif self.耗时表[玩家数据[id].数字id] <= 100 then
			self.奖励参数 = 0.8
			self.奖励数量 = 2

			广播消息(9, "#xt/#g/ " .. 玩家数据[id].角色.数据.名称 .. "#y/在帮助钱庄老板破解宝箱密码时，居然以#r/" .. self.耗时表[玩家数据[id].数字id] .. "#y/秒的惊人速度成功破解出密码，获得了130%的奖励以及天眼通符*8")
		else
			self.奖励参数 = 0.8
			self.奖励数量 = 2
		end

		self.临时等级 = 玩家数据[id].角色.数据.等级
		self.临时经验 = math.floor(self.临时等级 * self.临时等级 * self.临时等级 / 3 * self.奖励参数)

		玩家数据[id].角色:添加经验(self.临时经验, id, 13)

		if 玩家数据[id].召唤兽.数据.参战 ~= 0 then
			self.临时等级 = 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].等级
			self.临时经验 = math.floor(self.临时等级 * self.临时等级 * self.临时等级 / 3 * self.奖励参数 * 0.35)

			玩家数据[id].召唤兽:添加经验(self.临时经验, id, 玩家数据[id].召唤兽.数据.参战, 2)
		end

		if self.奖励数量 ~= 0 then
			玩家数据[id].道具:给予道具(id, "天眼通符", "功能", self.奖励数量)
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了" .. self.奖励数量 .. "张天眼通符")
		end

		self.回合表[玩家数据[id].数字id] = 10
	elseif self.回合表[玩家数据[id].数字id] >= 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/很遗憾，你未能在10回合内破解密码")
		发送数据(玩家数据[id].连接id, 7, "#y/正确密码为#r/" .. self.正确密码)
	end
end

function 宝箱解密类:生成钥匙(id)
	self.密码表[id] = {}
	self.时间表[id] = os.time()
	self.回合表[id] = 0

	for n = 1, 4 do
		self.密码表[id][n] = 取随机数(1, 10) - 1

		while self:计算重复(id, n) == false do
			self.密码表[id][n] = 取随机数(1, 10) - 1
		end
	end
end

function 宝箱解密类:计算重复(id, 编号)
	for i = 1, 4 do
		if 编号 ~= i and self.密码表[id][编号] == self.密码表[id][i] then
			return false
		end
	end

	return true
end

function 宝箱解密类:更新(dt)
end

function 宝箱解密类:显示(x, y)
end

return 宝箱解密类
