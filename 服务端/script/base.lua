local _class 	= {}--保存父类
function class(...)
	local class_type 	= {}--创建的对象
	class_type.super 	= {...}
	class_type.初始化 	= false
	class_type.创建 	= function(...)
		local ctor = {}
			setmetatable(ctor,{ __index = _class[class_type] })--继承类属性和函数
			do--获得初始化属性
				local create;
				create = function(c,...)
					if #c.super > 0	then--继承属性
						for i,v in ipairs(c.super) do
						 	create(v,...)
						end
					end
					if c.初始化 then c.初始化(ctor,...) end
				end
				create(class_type,...)--递归所有父类
			end
			function ctor:运行父函数(id,name,...)
				if type(id) == 'number' then
					if _class[class_type.super[id]] and _class[class_type.super[id]][name] then
					    return _class[class_type.super[id]][name](self,...)
					end
				else
					if _class[id] and _class[id][name] then
					    return _class[id][name](self,...)
					end
				end
			end
		return ctor
	end
	_class[class_type] = {}--类函数实际保存
	local mt = {}
	mt.__newindex 	= _class[class_type]
	mt.__call		= function (t,...)
		return t.创建(...)
	end
	setmetatable(class_type,mt)
	if #class_type.super > 0 then--继承函数
		setmetatable(_class[class_type],{__index = function (t,k)
		    for i,v in ipairs(class_type.super) do
		        local ret = _class[v][k]
		        if ret then
		            return ret
		        end
		    end
		end})
	end
	return class_type
end


function math.pow(a, b)
	return a ^ b
end


table.tostring = function (t)
	if t then
		local mark={}
		local assign={}
		local function ser_table(tbl,parent)
			mark[tbl]=parent
			local tmp={}
			for k,v in pairs(tbl) do
				local key= type(k)=="number" and string.format("[%s]", k) or k--string.format("[%q]", k)
				if type(v)=="table" then
					local dotkey= parent.. key
					if mark[v] then
						table.insert(assign,string.format("%s='%s'", dotkey,mark[v]))
					else
						table.insert(tmp, string.format("%s=%s", key,ser_table(v,dotkey)))
					end
				elseif type(v) == "string" then
					table.insert(tmp, string.format("%s=%q", key,v))
				elseif type(v) == "number" or type(v) == "boolean" then
					table.insert(tmp, string.format("%s=%s", key,v))
				end
			end
			return string.format("{%s}", table.concat(tmp,","))
		end
		return string.format("do local ret=%s%s return ret end", ser_table(t,"ret"),table.concat(assign," "))
	end
end

function setfenv(fn, env)
	local i = 1
	while true do
	  local name = debug.getupvalue(fn, i)
	  if name == "_ENV" then
		debug.upvaluejoin(fn, i, (function()
		  return env
		end), 1)
		break
	  elseif not name then
		break
	  end
	  
	  i = i + 1
	end
	return fn
end

table.loadstring = function (t)
	if t then
		local f = load(t) --loadstring
		if f then
			setfenv(f, {})
			return f()
		end
	end
end

table.copy = function (ori_tab)
    if (type(ori_tab) ~= "table") then
    	error("not table, can not copy.")
    end
    local new_tab = {};
    for i,v in pairs(ori_tab) do
        local vtyp = type(v);
        if (vtyp == "table") then
            new_tab[i] = table.copy(v);
        elseif (vtyp == "thread") then
            error("copy fail, error type.")
        elseif (vtyp == "userdata") then
            error("copy fail, error type.")
        else
            new_tab[i] = v;
        end
    end
    return new_tab;
end

-- table.copy = function(t)
-- 	return table.loadstring(table.tostring(t))
-- end

table.print = function (root)
	local print = print
	local tconcat = table.concat
	local tinsert = table.insert
	local srep = string.rep
	local type = type
	local pairs = pairs
	local tostring = tostring
	local next = next
	local cache = {  [root] = "." }
	local function _dump(t,space,name)
		local temp = {}
		for k,v in pairs(t) do
			local key = tostring(k)
			if cache[v] then
				tinsert(temp,"." .. key .. " {" .. cache[v].."}")
			elseif type(v) == "table" then
				local new_key = name .. "." .. key
				cache[v] = new_key
				tinsert(temp,"." .. key .. _dump(v,space .. (next(t,k) and "|" or " " ).. srep(" ",#key),new_key))
			else
				tinsert(temp,"." .. key .. " [" .. tostring(v).."]")
			end
		end
		return tconcat(temp,"\n"..space)
	end
	print(_dump(root, "",""))
end


string.split = function (str, delimiter)
	if str  then
	    local result = {}
	    str = str..delimiter
	    delimiter = "(.-)"..delimiter
	    for match in str:gmatch(delimiter) do
	        table.insert(result, match)
	    end
	    return result
	end
	return {}
end

function 分割文本(szFullString, szSeparator)
	local nFindStartIndex = 1
	local nSplitIndex = 1
	local nSplitArray = {}
	while true do
	  local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
	  if not nFindLastIndex then
		  nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
		  break
	  end
	  nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
	  nFindStartIndex = nFindLastIndex + string.len(szSeparator)
	  nSplitIndex = nSplitIndex + 1
	end
	return nSplitArray
  end




__颜色 = {
	深蓝 = 1,
	深绿 = 2,
	深青 = 3,
	深红 = 4,
	深紫 = 5,
	深黄 = 6,
	深白 = 7,
	深灰 = 8,

	蓝色 = 9,
	绿色 = 10,
	青色 = 11,
	红色 = 12,
	紫色 = 13,
	黄色 = 14,
	白色 = 15
}



f函数 = {}
function f函数.读配置(文件,节点,名称)
	if type(文件) ~= 'string' then printf('读取配置文件出错\n') return end
	if not ccc_isExist(文件) then printf('读取配置文件不存在\n') return end
	if type(节点) ~= 'string' then printf('读取配置节点出错\n') return end
	if type(名称) ~= 'string' then printf('读取配置名称出错\n') return end
	return ccc_getini(文件, 节点, 名称, "空");
end
function f函数.写配置(文件,节点,名称,值)
	if type(文件) ~= 'string' then printf('写入配置文件出错\n') return end
	-- if not ccc_isExist(文件) then printf('写入配置文件不存在\n') return end
	if type(节点) ~= 'string' then printf('写入配置节点出错\n') return end
	if type(名称) ~= 'string' then printf('写入配置名称出错\n') return end
	if 值 == nil or type(值) == 'table' then printf('写入配置值出错\n') return end
	return ccc_setini(文件,节点,名称, tostring(值))
end

function f函数.文件是否存在(file)
	if type(file) ~= 'string' or #file == 0 then printf('目录存在 param error\n') return false end
	return ccc_isExist(file)
	-- local f = io.open(file, 'rb')
	-- if not f then return false end
	-- f:close()
	-- return true
end

目录是否存在 = f函数.文件是否存在

function 读入文件(file)
	if type(file) ~= 'string'  or #file == 0 then printf('读入文件 param error\n') return '' end
	local f = io.open(file, 'rb')
	if not f then return '' end
	local readall = f:read("*a")
	f:close()
	return readall
end


function 写出文件(file, content)
	if type(file) ~= 'string' or #file == 0 then printf('写出文件 param error\n') return end
	if content == nil or type(content) == 'table' then printf('写出文件 param error\n') return end
	local f = io.open(file, 'w')
	if not f then return end
	f:write(tostring(content))
	f:close()
end

function 创建目录(addr)
	if type(addr) ~= 'string' or #addr == 0 then printf('创建目录 param error\n') return false end
	-- printf('创建目录 ' .. tostring(addr) .. '\n')
	-- ccc_isExist(addr)
	-- printf('ccc_isExist ' .. addr .. '\n')
	ccc_makedir(addr)
	-- printf('ccc_makedir ' .. addr .. '\n')
	return ccc_isExist(addr)
end