local 网络处理类 = class()

function 网络处理类:初始化()
 self.rwdh={[1] = "飞燕女",[2] = "英女侠",[3] = "巫蛮儿",[5] = "逍遥生",[6] = "剑侠客",[7] = "狐美人",[8] = "骨精灵",[10] = "杀破狼",[11] = "巨魔王",[12] = "虎头怪",[13] = "舞天姬",[14] = "玄彩娥",[16] = "羽灵神",[17] = "神天兵",[18] = "龙太子",[4] = "偃无师",[9] = "鬼潇潇",[15] = "桃夭夭",}
end
function 网络处理类:更新(dt)
end
function 网络处理类:数据处理(序号, 内容, id, ip)
	if not __C客户信息[id].是否可行连接 then
		发送数据(id,7,"#y/非法连接")
	    __C客户信息[id].是否可行连接=0
	    return 0
	end
	if 序号 >= 4 and (内容.数字id == nil or 玩家数据[内容.数字id] == nil) then
		return 0
	end
	self.处理信息1 = 内容
	-- 内容 = table.tostring(内容)
	if 序号 == 1 then
		self:账号处理(内容, id, ip)
	elseif 序号 == 2 then
		self:新建处理(内容, id, ip)
	elseif 序号 == 3 then
		-- 这里是 玩家ID
		self:退出处理(内容)
	elseif 序号 == 4 then
		地图处理类:数据处理(内容)
	elseif 序号 == 5 then
		self:角色处理(内容, id, ip)
	elseif 序号 == 6 then
		对话处理类:数据处理(内容)
	elseif 序号 == 7 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].角色:学习技能处理(self.处理信息1.数字id, self.处理信息1)
	elseif 序号 == 8 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].角色:学习生活技能处理(self.处理信息1.数字id, self.处理信息1)
	elseif 序号 == 9 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].角色:学习辅助技能处理(self.处理信息1.数字id, self.处理信息1)
	elseif 序号 == 10 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].角色:学习强化技能处理(self.处理信息1.数字id, self.处理信息1)
	elseif 序号 == 11 then
		队伍处理类:数据处理(内容)
	elseif 序号 == 12 then
		聊天处理类:数据处理(内容)
	elseif 序号 == 13 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].道具:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
	elseif 序号 == 14 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].商店处理类:数据处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 15 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].道具:交换格子(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
	elseif 序号 == 16 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].道具:空间互换(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
	elseif 序号 == 17 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].装备:数据处理(self.处理信息1.数字id, 4006, self.处理信息1.参数 + 0, self.处理信息1.序号 + 0, self.处理信息1.文本)
	elseif 序号 == 18 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].装备:数据处理(self.处理信息1.数字id, 4032, self.处理信息1.参数 + 0, self.处理信息1.序号 + 0, self.处理信息1.文本)
	elseif 序号 == 19 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].道具:飞行符传送(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 20 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].道具:仓库存放事件(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 21 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end
		玩家数据[self.处理信息1.数字id].道具:仓库拿走事件(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 22 then
		-- self.处理信息1 = table.loadstring(内容)

		发送数据(玩家数据[self.处理信息1.数字id].连接did, 2006, 玩家数据[self.处理信息1.数字id].召唤兽:获取数据())
	elseif 序号 == 23 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		if self.处理信息1.序号 == 1 then
			玩家数据[self.处理信息1.数字id].召唤兽:放生召唤兽(self.处理信息1.数字id, self.处理信息1.参数)
		elseif self.处理信息1.序号 == 2 then
			玩家数据[self.处理信息1.数字id].召唤兽:参战召唤兽(self.处理信息1.数字id, self.处理信息1.参数)
		elseif self.处理信息1.序号 == 3 then
			玩家数据[self.处理信息1.数字id].召唤兽:改名召唤兽(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.文本)
		elseif self.处理信息1.序号 == 4 then
			玩家数据[self.处理信息1.数字id].召唤兽:加点召唤兽(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.文本)
		end
	elseif 序号 == 24 then
		-- self.处理信息1 = table.loadstring(内容)

		战斗准备类:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数, id, ip)
	elseif 序号 == 25 then
		-- self.处理信息1 = table.loadstring(内容)

		任务处理类:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
	elseif 序号 == 26 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.找到id = self.处理信息1.参数 + 0

		if 玩家数据[self.找到id] == nil then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/这个玩家并不存在")

			return 0
		elseif 玩家数据[self.找到id].给予物品 == false then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/对方没有打开接受物品开关")

			return 0
		elseif 玩家数据[self.找到id].角色.数据.等级 < 60 then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/对方等级未达到60级无法给予物品")

			return 0
		elseif 玩家数据[self.处理信息1.数字id].角色.数据.等级 < 60 then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/你的等级未达到60级无法给予物品")

			return 0
		else
			if 玩家数据[self.处理信息1.数字id].角色.数据.道具.货币.银子 < 0 or 玩家数据[self.找到id].角色.数据.道具.货币.银子 < 0 then
				发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/错误的给予数据")

				return 0
			end

			self.发送信息 = {
				id = self.找到id,
				道具 = 玩家数据[self.处理信息1.数字id].道具:索要玩家道具(self.处理信息1.数字id, "包裹"),
				名称 = 玩家数据[self.找到id].角色.数据.名称,
				等级 = 玩家数据[self.找到id].角色.数据.等级
			}

			发送数据(玩家数据[self.处理信息1.数字id].连接did, 3039, self.发送信息)
		end
	elseif 序号 == 27 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].道具:交易条件判断(self.处理信息1.数字id, self.处理信息1.参数)
	elseif 序号 == 28 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].角色:玩家切磋(self.处理信息1.数字id, self.处理信息1.参数)
	elseif 序号 == 29 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].道具:给予玩家物品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 32 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.处理信息1.参数 = self.处理信息1.参数 + 0

		if 交易数据[self.处理信息1.参数] == nil then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#w/这样的交易并不存在")
		else
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 3015, 玩家数据[self.处理信息1.数字id].召唤兽:获取数据())
		end
	elseif 序号 == 31 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.处理信息1.参数 = self.处理信息1.参数 + 0

		if 交易数据[self.处理信息1.参数] == nil then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#w/这样的交易并不存在")
		else
			self.发送信息 = {
				道具 = 玩家数据[self.处理信息1.数字id].道具:索要玩家道具(self.处理信息1.数字id, "包裹")
			}

			发送数据(玩家数据[self.处理信息1.数字id].连接did, 3014, self.发送信息)
		end
	elseif 序号 == 33 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.处理信息1.参数 = self.处理信息1.参数 + 0

		玩家数据[self.处理信息1.数字id].道具:锁定交易(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 34 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.处理信息1.参数 = self.处理信息1.参数 + 0

		玩家数据[self.处理信息1.数字id].道具:完成交易(self.处理信息1.数字id, self.处理信息1.参数)
	elseif 序号 == 35 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		self.处理信息1.参数 = self.处理信息1.参数 + 0
		self.交易id = 玩家数据[self.处理信息1.数字id].交易

		if 交易数据[self.交易id] == nil then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 3016, "11")
		end

		发送数据(玩家数据[交易数据[self.交易id].发起方.id].连接did, 7, "#y/" .. 玩家数据[self.处理信息1.数字id].角色.数据.名称 .. "取消了交易")
		发送数据(玩家数据[交易数据[self.交易id].接受方.id].连接did, 7, "#y/" .. 玩家数据[self.处理信息1.数字id].角色.数据.名称 .. "取消了交易")
		发送数据(玩家数据[交易数据[self.交易id].发起方.id].连接did, 3016, "11")
		发送数据(玩家数据[交易数据[self.交易id].接受方.id].连接did, 3016, "11")

		玩家数据[交易数据[self.交易id].接受方.id].交易 = 0
		玩家数据[交易数据[self.交易id].发起方.id].交易 = 0
		交易数据[self.交易id] = false
	elseif 序号 == 36 then
		-- self.处理信息1 = table.loadstring(内容)

		商会处理类:数据处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 37 then
		-- self.处理信息1 = table.loadstring(内容)

		商会处理类:上柜商品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 38 then
		-- self.处理信息1 = table.loadstring(内容)

		商会处理类:购买商品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 39 then
		-- self.处理信息1 = table.loadstring(内容)

		玩家数据[self.处理信息1.数字id].道具:染色处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 40 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].道具:城市快捷传送(self.处理信息1.数字id)
	elseif 序号 == 41 then
		-- self.处理信息1 = table.loadstring(内容)

		宝箱解密类:数据处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
	elseif 序号 == 42 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].摆摊 ~= nil then
			return 0
		end

		玩家数据[self.处理信息1.数字id].角色:坐骑处理(self.处理信息1.数字id)
	elseif 序号 == 43 then
		-- self.处理信息1 = table.loadstring(内容)

		if 玩家数据[self.处理信息1.数字id].角色.数据.pk开关 == false then
			玩家数据[self.处理信息1.数字id].角色.数据.pk开关 = true
			玩家数据[self.处理信息1.数字id].角色.数据.pk时间 = os.time() + 86400

			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/你打开了pk开关，现在可以自由攻击其他玩家了")
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/pk开关最少要在24小时后才可被关闭")
		elseif os.time() < 玩家数据[self.处理信息1.数字id].角色.数据.pk时间 then
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/您需要等待#r/" .. math.floor((玩家数据[self.处理信息1.数字id].角色.数据.pk时间 - os.time()) / 60) .. "#y/分钟后才可关闭pk开关")
		else
			玩家数据[self.处理信息1.数字id].角色.数据.pk开关 = false
		end
	elseif 序号 == 44 then
		-- self.处理信息1 = table.loadstring(内容)

		摆摊处理类:数据处理(self.处理信息1.数字id, self.处理信息1.序号 + 0, self.处理信息1.参数, self.处理信息1.文本)
	elseif 序号 == 45 then
		-- self.处理信息1 = table.loadstring(内容)
		self.处理信息1.参数 = self.处理信息1.参数 + 0
		展示数据["[bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]"] = {
			数据 = 玩家数据[self.处理信息1.数字id].召唤兽:获取指定数据(self.处理信息1.参数),
			时间 = os.time()
		}

		if bb数据列表[self.处理信息1.数字id] == nil then
			bb数据列表[self.处理信息1.数字id] = {}
		end

		bb数据列表[self.处理信息1.数字id][#bb数据列表[self.处理信息1.数字id] + 1] = {
			内容 = "[" .. "bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]",
			名称 = 玩家数据[self.处理信息1.数字id].召唤兽.数据[self.处理信息1.参数].造型
		}
		玩家数据[self.处理信息1.数字id].组合语句 = 玩家数据[self.处理信息1.数字id].召唤兽.数据[self.处理信息1.参数 + 0].造型

		发送数据(玩家数据[self.处理信息1.数字id].连接did, 20028, "[" .. "bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]")
	elseif 序号 == 46 then
		发送数据(玩家数据[self.处理信息1.数字id].连接did, 20029, 展示数据[self.处理信息1.参数])
	elseif 序号 == 47 then
		-- self.处理信息1 = table.loadstring(内容)

		发送数据(玩家数据[self.处理信息1.数字id].连接did, 20030, 玩家数据[self.处理信息1.数字id].角色:取坐骑数据())
	elseif 序号 == 48 then
		-- self.处理信息1 = table.loadstring(内容)

		玩家数据[self.处理信息1.数字id].角色:坐骑骑乘处理(self.处理信息1.数字id, self.处理信息1.参数)
	elseif 序号 == 49 then
		-- self.处理信息1 = table.loadstring(内容)

		玩家数据[self.处理信息1.数字id].角色:坐骑驯养处理(self.处理信息1.数字id, self.处理信息1.参数)
	elseif 序号 == 50 then
		-- self.处理信息1 = table.loadstring(内容)

		玩家数据[self.处理信息1.数字id].角色:坐骑加点处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号 + 0)
	elseif 序号 == 93 then
		-- self.处理信息1 = table.loadstring(内容)
		if 银子检查(self.处理信息1.数字id, 10000000)  then
			玩家数据[self.处理信息1.数字id].角色:扣除银子(self.处理信息1.数字id, 10000000, 27)
			玩家数据[self.处理信息1.数字id].角色:清空奇经八脉()
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 20041, 玩家数据[self.处理信息1.数字id].角色:经脉处理())
			发送数据(玩家数据[self.处理信息1.数字id].连接did, 7, "#y/你清空了奇经八脉")
			return 0
		else
			发送数据(id, 7, "#y/你没那么多的银子")
			return 0
		end
	elseif 序号 == 94 then
		-- self.处理信息1 = table.loadstring(内容)
		玩家数据[self.处理信息1.数字id].角色:增加奇经八脉(self.处理信息1.序号)
		发送数据(玩家数据[self.处理信息1.数字id].连接did, 20041, 玩家数据[self.处理信息1.数字id].角色:经脉处理())
	elseif 序号 == 95 then
		-- self.处理信息1 = table.loadstring(内容)
		if 玩家数据[self.处理信息1.数字id].角色.数据.门派 == "无" or 玩家数据[self.处理信息1.数字id].角色.数据.门派 == nil then
			发送数据(id, 7, "#y/你还没有门派请加入门派后再来吧！")
			return 0
		end
		发送数据(玩家数据[self.处理信息1.数字id].连接did, 20041, 玩家数据[self.处理信息1.数字id].角色:经脉处理())

	elseif 序号 == 96 then
		__S服务:输出('隐藏访问 96')
		do return 0 end
		-- self.处理信息1 = table.loadstring(内容)
		self.处理信息1.参数 = self.处理信息1.参数 + 0

		if 玩家数据[self.处理信息1.参数] ~= nil then
			玩家数据[self.处理信息1.参数].连接bid = self.处理信息1.连接bid

			发送数据(6, {
				文本 = self.处理信息1.参数,
				id = self.处理信息1.连接bid,pid=self.处理信息1.连接did
			})
		end
	elseif 序号 == 99 and 调试模式 then
		__S服务:输出('隐藏访问 99')
		do return 0 end
		-- self.处理信息1 = table.loadstring(内容)

		战斗准备类:创建战斗(self.处理信息1.数字id, 100001, 0)
	-- elseif 序号 == 8591 and 取管理ip(ip) then
	-- 	管理工具类:数据处理(内容)
		elseif 序号==105 then --道具交换格子处理

			-- self.处理信息1=table.loadstring(内容) --符石开孔
			if 玩家数据[self.处理信息1.数字id].摆摊~=nil then return 0 end
			玩家数据[self.处理信息1.数字id].装备:数据处理(self.处理信息1.数字id,4007,self.处理信息1.参数+0,self.处理信息1.序号+0,self.处理信息1.文本)
	elseif 序号==106 then --道具交换格子处理
			-- self.处理信息1=table.loadstring(内容) --符石开孔
			if 玩家数据[self.处理信息1.数字id].摆摊~=nil then return 0 end
		玩家数据[self.处理信息1.数字id].装备:数据处理(self.处理信息1.数字id,4009,self.处理信息1.参数+0,self.处理信息1.序号+0,self.处理信息1.文本)

	elseif 序号== 9961  then --请求快捷技能数据
		-- self.处理信息1=table.loadstring(内容)

		发送数据(玩家数据[self.处理信息1.数字id].连接did,2074,玩家数据[self.处理信息1.数字id].角色:获取快捷技能数据())


	elseif 序号 == 50001 then
		__S服务:输出('隐藏访问 50001')
		do return 0 end
		self.处理信息1 = table.loadstring(内容)
		服务端参数.管理ip = self.处理信息1.文本

		f函数.写配置(程序目录 .. "配置文件.ini", "主要配置", "管理ip", 服务端参数.管理ip)
	end
end

function 网络处理类:退出处理(内容, 强制)
	内容 = 内容 + 0
	if 玩家数据[内容]~= nil and 玩家数据[内容].摆摊 ~= nil and 强制 == nil then
		发送数据(玩家数据[内容].连接did, 99999, 玩家数据[内容].连接bid)
		return 0
	end
	if 玩家数据[内容] ~= nil then
		if 玩家数据[内容].观战模式 and 玩家数据[内容].战斗 ~= 0 then
			战斗准备类.战斗盒子[玩家数据[内容].战斗]:退出观战(内容)
			玩家数据[内容].战斗 = 0
		end
		if 玩家数据[内容].战斗 ~= 0 then
			玩家数据[内容].下线标记 = true
			if 玩家数据[内容].队伍 ~= 0 then
				广播队伍消息(内容, 21, "#dw/#g/" .. 玩家数据[内容].角色.数据.名称 .. "#y/掉线...")
			end
		else
			if 玩家数据[内容].角色.数据.帮派 ~= nil and 帮派数据[玩家数据[内容].角色.数据.帮派].成员名单[内容] ~= nil then
				帮派数据[玩家数据[内容].角色.数据.帮派].成员名单[内容].离线时间 = os.time()
			end
			if 玩家数据[内容].队伍 ~= 0 then
				队伍处理类:离开队伍(玩家数据[内容].队伍, 内容, 1)
			end
			地图处理类:移除玩家(玩家数据[内容].地图, 内容)
			玩家数据[内容].角色:存档(内容)
			发送数据(玩家数据[内容].连接did, 99999, 玩家数据[内容].连接bid)
			玩家数据[内容] = nil
		end
	end
end

function 网络处理类:角色处理(内容, id, ip)
	self.处理信息 = 内容 --table.loadstring(内容)

	if self.处理信息.序号 == 1 then
		发送数据(玩家数据[self.处理信息.数字id].连接id,2002,玩家数据[self.处理信息.数字id].角色:获取角色数据())
	elseif self.处理信息.序号 == 2 then
		玩家数据[self.处理信息.数字id].角色:升级请求(self.处理信息.数字id)
	elseif self.处理信息.序号 == 3 then
		玩家数据[self.处理信息.数字id].角色:加点事件(self.处理信息.数字id, self.处理信息.文本)
	elseif self.处理信息.序号 == 4 then
		玩家数据[self.处理信息.数字id].角色:角色改名(self.处理信息.文本)
	end
end

----------------------账号处理不一样------------------
function 网络处理类:账号处理(内容, id, ip)
	-- __S服务:输出('账号处理:' .. table.tostring(内容))
	self.账号信息 = 内容 --table.loadstring(内容)
	-- local fun  = require("ffi函数")
	if self.账号信息.密码 == nil or self.账号信息.卡洛== nil then
		self:强制断开1(id,"#y/非法登陆" )
		return 0
	elseif 目录是否存在(data目录 .. self.账号信息.账号) == false or 目录是否存在(data目录 .. self.账号信息.账号) == false or f函数.文件是否存在(程序目录..data目录 .. self.账号信息.账号.. _账号txt) == false then
		self:强制断开1(id,"#y/对不起你的账号不存在,请你注册账号先,谢谢!" )
		return 0
	elseif f函数.文件是否存在(程序目录..data目录 .. self.账号信息.账号.. _账号txt) and f函数.读配置(程序目录..data目录 .. self.账号信息.账号.. _账号txt, "账号信息", "密码")~=self.账号信息.密码 then
		self:强制断开1(id,"#y/对不起你的密码不正确,请核对后在登陆,谢谢!" )
		return 0
	elseif  f函数.读配置(程序目录..data目录 .. self.账号信息.账号.. _账号txt, "账号信息", "仙玉")< "0" then
		self:强制断开1(id,"#y/#y/您仙玉为负数，暂不能登录。" )
		return 0
	elseif f函数.文件是否存在(程序目录..data目录 .. self.账号信息.账号 .. _角色txt) == false then
		发送数据(id,4,"cjzh")
	elseif f函数.读配置(程序目录..data目录 .. self.账号信息.账号.. _账号txt, "账号信息", "封禁") == "1" then
			self:强制断开1(id,"#y/#y/您的账号已经被封禁，暂不能登录。" )
		return 0
	else
		local 临时角色 = 读入文件(data目录 .. self.账号信息.账号 .. _角色txt)
		临时角色 = table.loadstring(临时角色)
		if not 临时角色 then 
			self:强制断开1(id,"#y/#y/无效账号。" )
			return 0
		end

		if 玩家数据[临时角色.id] == nil then
			玩家数据[临时角色.id] = {
				角色 = 角色处理类.创建(临时角色.id, ip),
				数字id = 临时角色.id,

				连接bid =self.账号信息.id,
				连接id =id,
				连接did =id,-- self.账号信息.id,
				连接真id = id,
				连接真ip = ip,
				仙玉 = f函数.读配置(程序目录..data目录 .. self.账号信息.账号.. _账号txt, "账号信息", "仙玉") + 0,
				管理 = f函数.读配置(程序目录..data目录 .. self.账号信息.账号.. _账号txt, "账号信息", "管理")
			}
			__C客户信息[id].连接账号id =临时角色.id
			玩家数据[临时角色.id].角色:加载数据(读入文件(data目录 .. self.账号信息.账号 .. _角色txt))
			玩家数据[临时角色.id].角色.数据.日志编号 = 读入文件(data目录 .. self.账号信息.账号 .. "/日志编号.txt")
			玩家数据[临时角色.id].角色.消费日志 = 读入文件(data目录 .. self.账号信息.账号 .. _日志目录 .. 玩家数据[临时角色.id].角色.数据.日志编号 .. ".txt")
			玩家数据[临时角色.id].召唤兽 = 召唤兽处理类.创建(临时角色.id)
			玩家数据[临时角色.id].召唤兽:加载数据(读入文件(data目录 .. self.账号信息.账号 .. _召唤兽txt), 临时角色.id)
			玩家数据[临时角色.id].账号 = self.账号信息.账号
			玩家数据[临时角色.id].世界频道 = os.time()
			玩家数据[临时角色.id].传闻频道 = os.time()
			玩家数据[临时角色.id].队伍频道 = os.time()
			玩家数据[临时角色.id].门派频道 = os.time()
			玩家数据[临时角色.id].队伍 = 0
			玩家数据[临时角色.id].请求数量=0
			玩家数据[临时角色.id].观战模式 = false
			玩家数据[临时角色.id].队长 = false
			玩家数据[临时角色.id].当前频道 = os.time()
			玩家数据[临时角色.id].遇怪时间 = {
				起始 = os.time(),
				间隔 = 取随机数(8, 20)
			}
			玩家数据[临时角色.id].摆摊 = nil
			玩家数据[临时角色.id].首席保护 = 0
			玩家数据[临时角色.id].请求数量 = 0
			玩家数据[临时角色.id].战斗对象 = 0
			玩家数据[临时角色.id].地图 = 玩家数据[临时角色.id].角色.数据.地图数据.编号
			玩家数据[临时角色.id].副本 = 0
			玩家数据[临时角色.id].付费 = false
			玩家数据[临时角色.id].下线标记 = false
			玩家数据[临时角色.id].状态 = 1
			玩家数据[临时角色.id].移动目标 = 0
			玩家数据[临时角色.id].npc = 0
			玩家数据[临时角色.id].比武保护期 = 0
			玩家数据[临时角色.id].任务id = 0
			玩家数据[临时角色.id].战斗 = 0
			玩家数据[临时角色.id].交易 = 0
			玩家数据[临时角色.id].召唤兽id = 0
			玩家数据[临时角色.id].道具id = 0
			玩家数据[临时角色.id].包裹类型 = 0
			玩家数据[临时角色.id].给予物品 = true
			玩家数据[临时角色.id].道具 = 道具处理类.创建(临时角色.id)
			玩家数据[临时角色.id].装备 = 装备处理类.创建(临时角色.id)
			玩家数据[临时角色.id].道具:加载数据(读入文件(data目录 .. self.账号信息.账号 .. _道具txt))
			玩家数据[临时角色.id].角色:道具异常处理(临时角色.id)
			玩家数据[临时角色.id].商店处理类 = 商店处理类.创建()
			玩家数据[临时角色.id].商店处理类:加载商品()
			   if 玩家数据[临时角色.id].角色.数据.礼包.新手 == 0  then
	            玩家数据[临时角色.id].道具:给予道具(临时角色.id,"新手玩家礼包","功能")
	            玩家数据[临时角色.id].角色.数据.礼包.新手= 1
			  end
			玩家数据[临时角色.id].角色:发送欢迎信息()
			发送数据(id,5,玩家数据[临时角色.id].角色:获取角色数据())
			发送数据(id, 6, 玩家数据[临时角色.id].角色.数据.离线时间)
			发送数据(id, 10, os.time())
            发送数据(id, 2005, 玩家数据[临时角色.id].召唤兽:取头像数据())
			地图处理类:加入玩家(临时角色.id)

			玩家数据[临时角色.id].帮派进入编号 = 玩家数据[临时角色.id].角色.数据.帮派
			if 玩家数据[临时角色.id].角色.数据.帮派 ~= nil then
				if 帮派数据[玩家数据[临时角色.id].角色.数据.帮派] == nil then
					玩家数据[临时角色.id].角色.数据.帮派 = nil
				end

				if 玩家数据[临时角色.id].角色.数据.帮派 ~= nil and 帮派数据[玩家数据[临时角色.id].角色.数据.帮派] and 帮派数据[玩家数据[临时角色.id].角色.数据.帮派].成员名单[临时角色.id] == nil then
					玩家数据[临时角色.id].角色:添加系统消息(临时角色.id, "#h/你已经被请离了#y/" .. 帮派数据[玩家数据[临时角色.id].角色.数据.帮派].名称 .. "#h/帮派，你现在是无帮人士了")
					玩家数据[临时角色.id].角色.数据.帮派 = nil
				end
			end
			if 玩家数据[临时角色.id].角色:取任务id(8001) ~= 0 then
				玩家数据[临时角色.id].副本 = 玩家数据[临时角色.id].角色:取任务id(8001)
			elseif 玩家数据[临时角色.id].角色:取任务id(8010) ~= 0 then
				玩家数据[临时角色.id].副本 = 玩家数据[临时角色.id].角色:取任务id(8010)
			end
			if 玩家数据[临时角色.id].地图 == 5135 then
				地图处理类:指定跳转1(临时角色.id, 1001, 364, 36)
			elseif 玩家数据[临时角色.id].地图 == 6001 or 玩家数据[临时角色.id].地图 == 6002 or 玩家数据[临时角色.id].地图 == 6003 then
				地图处理类:指定跳转1(临时角色.id, 1001, 429, 164)
			elseif 玩家数据[临时角色.id].地图 == 3131 or 玩家数据[临时角色.id].地图 == 3132 then
				地图处理类:指定跳转1(临时角色.id, 1001, 284, 83)
			elseif 玩家数据[临时角色.id].地图 == 4131 or 玩家数据[临时角色.id].地图 == 4132 or 玩家数据[临时角色.id].地图 == 4133 then
				地图处理类:指定跳转1(临时角色.id, 1070, 127, 145)
			elseif 玩家数据[临时角色.id].地图 > 7000 and 玩家数据[临时角色.id].地图 <= 7020 and 幻域迷宫开关 == false then
				地图处理类:指定跳转1(临时角色.id, 1092, 203, 16)
			elseif 玩家数据[临时角色.id].地图 == 1875 or 玩家数据[临时角色.id].地图 == 1815 or 玩家数据[临时角色.id].地图 == 1825 or 玩家数据[临时角色.id].地图 == 1835 or 玩家数据[临时角色.id].地图 == 1845 or 玩家数据[临时角色.id].地图 == 1855 or 玩家数据[临时角色.id].地图 == 1865 or 玩家数据[临时角色.id].地图 == 1875 then
				地图处理类:指定跳转1(临时角色.id, 1001, 419, 8)
			end
		else
			if 玩家数据[临时角色.id].观战模式 and 玩家数据[临时角色.id].战斗 ~= 0 then
				战斗准备类.战斗盒子[玩家数据[临时角色.id].战斗]:退出观战(临时角色.id)
				玩家数据[临时角色.id].战斗 = 0
			end
			if 玩家数据[临时角色.id].战斗 == 0 then
				if 玩家数据[临时角色.id].摆摊 ~= nil then
					玩家数据[临时角色.id].摆摊 = nil
					地图处理类:更新摊位(临时角色.id, "", 玩家数据[临时角色.id].地图)
				end
				self.下线id = 玩家数据[临时角色.id].连接bid
				发送数据(id,99998,"您的账号已经在其它设备上登录，您被迫下线")
				--__S服务:发送(id, 99999, self.下线id)

				玩家数据[临时角色.id].连接bid = self.账号信息.id
				玩家数据[临时角色.id].下线标记 = false
				玩家数据[临时角色.id].观战模式 = false
				玩家数据[临时角色.id].连接did =id
				玩家数据[临时角色.id].连接id =id
				玩家数据[临时角色.id].连接真id = id
				__C客户信息[id].连接账号id =临时角色.id
				玩家数据[临时角色.id].角色:发送欢迎信息()
				发送数据(id,5,玩家数据[临时角色.id].角色:获取角色数据())
				发送数据(id, 6, 玩家数据[临时角色.id].角色.数据.离线时间)
				发送数据(id, 10, os.time())
				发送数据(id,2005, 玩家数据[临时角色.id].召唤兽:取头像数据())
				地图处理类:加入玩家1(临时角色.id)
			else
				self.下线id = 玩家数据[临时角色.id].连接bid

				if 战斗准备类.战斗盒子[玩家数据[临时角色.id].战斗] == nil then
						发送数据(id,99998,"战斗数据异常，请等待战斗结束后再尝试登录")
					--__S服务:发送(id, 99999, self.下线id)
				else
		        	发送数据(id,99998,"您的账号已经在其它设备上登录，您被迫下线")
					--__S服务:发送(id, 99999, self.下线id)

					玩家数据[临时角色.id].连接bid = self.账号信息.id
					玩家数据[临时角色.id].下线标记 = false
					玩家数据[临时角色.id].观战模式 = false
					玩家数据[临时角色.id].连接did =id
					玩家数据[临时角色.id].连接id =id
					玩家数据[临时角色.id].连接真id = id
					__C客户信息[id].连接账号id =临时角色.id
					玩家数据[临时角色.id].角色:发送欢迎信息()
					发送数据(id,5,玩家数据[临时角色.id].角色:获取角色数据())
					发送数据(id, 6, 玩家数据[临时角色.id].角色.数据.离线时间)
					发送数据(id, 10, os.time())
					发送数据(id, 2005, 玩家数据[临时角色.id].召唤兽:取头像数据())
					地图处理类:加入玩家1(临时角色.id)
					战斗准备类.战斗盒子[玩家数据[临时角色.id].战斗]:重连数据发送(临时角色.id)
					if 玩家数据[临时角色.id].队伍 ~= 0 then
						队伍处理类:发送队伍信息(玩家数据[临时角色.id].队伍, 5009, 玩家数据[临时角色.id].连接did)
						广播队伍消息(临时角色.id, 21, "#dw/#g/" .. 玩家数据[临时角色.id].角色.数据.名称 .. "#y/上线")
						if 玩家数据[临时角色.id].队长 then
							发送数据(玩家数据[临时角色.id].连接did, 5002, "66")
						end
					end
				end
			end
		end

		玩家数据[临时角色.id].角色.数据.蚩尤元神 = 0
		玩家数据[临时角色.id].角色:刷新装备属性(临时角色.id)
		updateRanking(临时角色.id, 玩家数据[临时角色.id].角色.数据)
		for n = 1, #玩家数据[临时角色.id].召唤兽.数据 do
			玩家数据[临时角色.id].召唤兽:刷新装备属性(n, 临时角色.id)
		end
		-- 发送数据(玩家数据[临时角色.id].连接did, 31415926, 玩家数据[临时角色.id].连接did)
	end
end
---------------------------新建处理不一样-----------------
function 网络处理类:新建处理(内容, id, ip)
	self.新建信息 = 内容 --table.loadstring(内容)
	-- local fun  = require("ffi函数")
	if self.新建信息.名称 == nil then
		self.新建信息.名称 = "名字" .. 服务端参数.角色id + 1
	end
	if self.新建信息.密码 == nil or self.新建信息.卡洛 == nil then
		self:强制断开1(self.新建信息.id,"#y/非法登陆!")
		return 0
	end
	-- if 目录是否存在(data目录 .. self.新建信息.账号) ~= "0" then
	-- 	发送数据(7, {
	-- 		文本 = "#y/建立角色时出现严重错误，错误代号#r/2001，#y/请联系游戏管理员解决",
	-- 		id = self.新建信息.id
	-- 	})

	-- 	return 0
	-- else
	if f函数.文件是否存在(程序目录..data目录 .. self.新建信息.账号 .. _角色txt) then
		发送数据(id,7, "#y/建立角色时出现严重错误，错误代号#r/2002，#y/请联系游戏管理员解决")
		return 0
	elseif 目录是否存在(data目录 .. self.新建信息.账号) == false or 目录是否存在(data目录 .. self.新建信息.账号) == false or f函数.文件是否存在(程序目录..data目录 .. self.新建信息.账号.. _账号txt) == false then
		self:强制断开1(self.新建信息.id,"#y/对不起你的账号不存在,请你注册账号先,谢谢!" )
		return 0
	elseif f函数.文件是否存在(程序目录..data目录 .. self.新建信息.账号.. _账号txt) and f函数.读配置(程序目录..data目录 .. self.新建信息.账号.. _账号txt, "账号信息", "密码")~=self.新建信息.密码  then
		self:强制断开1(self.新建信息.id,"#y/对不起你的密码不正确,请核对后在登陆,谢谢!" )
		return 0
	elseif self.rwdh[self.新建信息.编号] == nil then
		发送数据(id,7, "#y/建立角色时出现严重错误，错误代号#r/2003，#y/请联系游戏管理员解决")
		return
	elseif self:名称检查(self.新建信息.名称, id, ip) == false then
		return 0
	else
		--printf('新建处理 日志')
		创建目录(data目录 .. self.新建信息.账号)
		创建目录(data目录 .. self.新建信息.账号 .. "/日志")
		-- 写出文件(data目录 .. self.新建信息.账号.. _账号txt, self.新建信息.文件信息)

		服务端参数.角色id = 服务端参数.角色id + 1

		f函数.写配置(程序目录 .. "配置文件.ini", "主要配置", "id", 服务端参数.角色id)

		local 临时角色 = 角色处理类.创建(id, ip)

		临时角色:创建新角色(self.新建信息.账号, self.新建信息.名称, self.rwdh[self.新建信息.编号], self.新建信息.编号, 性别, 服务端参数.角色id)
		写出文件(data目录 .. self.新建信息.账号 .. "/日志编号.txt", "1")
		写出文件(data目录 .. self.新建信息.账号 .. _充值记录txt, "日志创建")
		写出文件(data目录 .. self.新建信息.账号 .. "/日志" .. "/1.txt", "日志创建#换行符")
		写出文件(data目录 .. self.新建信息.账号 .. _角色txt, table.tostring(临时角色.数据))
		写出文件(data目录 .. self.新建信息.账号 .. _召唤兽txt, table.tostring({}))
		写出文件(data目录 .. self.新建信息.账号 .. _道具txt, table.tostring({}))

		-- 名称数据[#名称数据 + 1] = {
		-- 	名称 = self.新建信息.名称,
		-- 	账号 = self.新建信息.账号,
		-- 	ip = self.新建信息.ip
		-- }
		名称数据[#名称数据 + 1] = self.新建信息.名称

		-- 写出文件("tysj/名称数据.txt", table.tostring(名称数据))
		self:账号处理(self.新建信息, id, ip)
	end
end
--------------------创建玩家账号是多出来的！！---------------
function 网络处理类:创建玩家账号(账号,密码,安全码,id,ip)
--   local fun  = require("ffi函数")
-- __S服务:输出(账号 .. 密码 .. 安全码 .. tostring(id) .. ip)
  if  创建目录(data目录..账号)  then
	-- __S服务:输出("创建玩家配置")
	local file = data日录..账号..[[\账号.txt]]
	local section = "账号信息"
	local t = os.time()
	local y = os.date("%Y", t)
	local m = os.date("%m", t)
	local d = os.date("%d", t)
	local hms = os.date("%X", t)
	local tt = "".. m .."月".. d .."日 ".. hms
   f函数.写配置(file,section,"密码",密码)
   f函数.写配置(file, section, "安全码",安全码)
   f函数.写配置(file, section,"仙玉","0")
   f函数.写配置(file, section,"点数","0")
   f函数.写配置(file, section,"体验","0")
   f函数.写配置(file, section,"管理","0")
   f函数.写配置(file, section,"创建员",账号)
   f函数.写配置(file, section,"ip",ip)
   f函数.写配置(file,section,"注册时间",tt)
    -- __S服务:输出("创建玩家配置完毕")
	local rec = data目录..账号..[[\操作记录.txt]]
   写出文件(rec,"账号创建")
  else
    发送数据(id,7,"#y/无法建立账号数据，请联系管理员")
  end
end

function 网络处理类:名称检查(姓名, idd, ip)
	if string.len(姓名) < 6 then
		发送数据(idd,7,"#y/角色名称长度过短")
		return false
	elseif string.len(姓名) > 12 then
		发送数据(idd,7,"#y/角色名称长度过长")
		return false
	elseif 姓名 == "" then
			发送数据(idd,7,"#y/角色名称不能为空")
		return false
	elseif string.find(姓名, "/") ~= nil then
		发送数据(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "/") ~= nil then
		发送数据(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "@") ~= nil then
		发送数据(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "#") ~= nil then
		发送数据(idd,7,"#y/角色名称含有特殊符号")

		return false
	elseif string.find(姓名, "*") ~= nil then
       发送数据(idd,7,"#y/角色名称含有特殊符号")

		return false
	end

	-- for n = 1, string.len(姓名) do
	-- 	if __gge.crc32(string.sub(姓名, n, n)) == -378745019 or __gge.crc32(string.sub(姓名, n, n)) == -1327500718 then
	-- 	发送数据(idd,7,"#y/角色名称含有特殊符号")

	-- 		return false
	-- 	end
	-- end

	for n = 1, #敏感词数据 do
		if string.find(姓名, 敏感词数据[n]) ~= nil then
			发送数据(idd,7,"#y/角色名称存在不允许使用的字符")

			return false
		end
	end

	for n = 1, #名称数据 do
		if 名称数据[n] == 姓名 then --.名称
			发送数据(idd,7,"#y/该角色名称已被其他玩家使用")
			return false
		end
	end

	return true
end



function 网络处理类:删除临时用户(id)
  self.临时客户[id]=nil
end

function 网络处理类:连接断开(id)
	local 客户id=连接id转玩家id(id)
	if 客户id~=0 then
     发送数据(玩家数据[客户id].连接did,8,消息)
     登录处理类:玩家退出(id)
     __N连接数 = __N连接数-1
     -- __S服务:断开连接(id)
     发送数据(id, 2, '不丝滑')
     return 0
  else
    __N连接数 = __N连接数-1
    发送数据(id,8,消息)
   -- __S服务:断开连接(id)
   发送数据(id, 2, '不丝滑')

   self.临时客户[id]=0
   return 0
   end
 __N连接数 = __N连接数-1
 -- __S服务:断开连接(id)
 发送数据(id, 2, '不丝滑')
end

function 网络处理类:强制断开(id,消息)
  --临时客户
  local 客户id=连接id转玩家id(id)

  if 客户id~=0 then
     发送数据(玩家数据[客户id].连接did,8,消息)
     登录处理类:玩家退出(id)
     __N连接数 = __N连接数-1
     -- __S服务:断开连接(id)
     发送数据(id, 2, 消息)
     return 0
  elseif self.临时客户[id]~=nil  then
      --do
    __N连接数 = __N连接数-1
    发送数据(id,8,消息)
   -- __S服务:断开连接(id)
   发送数据(id, 2, 消息)
   self.临时客户[id]=0
   return 0
   end
 __N连接数 = __N连接数-1
 -- __S服务:断开连接(id)
 -- __S服务:发送(id,2, 消息)
    发送数据(id, 2, 消息)
end

function 网络处理类:强制断开1(id,消息)
	-- 发送数据(id,7,消息)
	-- __S服务:发送(id,2, 消息)
   -- __S服务:断开连接(id,99999)
   发送数据(id, 2, 消息)

end

function 网络处理类:强制断开2(id,消息)
  发送数据(self.临时客户[id].连接did,8,消息)

  __N连接数 = __N连接数-1
  -- __S服务:断开连接(self.临时客户[id].连接did)
  发送数据(self.临时客户[id].连接did, 2, '不丝滑')
   self.临时客户[id]=nil
end

return 网络处理类
