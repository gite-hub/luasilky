
local 战斗处理类 = class()
local 法术状态名称={
  [1] ="横扫千军",
  [2]  ="后发制人",
  [3] ="似玉生香",
  [4] ="杀气诀",
  [5] ="催眠符",
  [6] ="失魂符",
  [7] ="定身符",
  [8] ="佛法无边",
  [9] ="金刚护体",
  [10] ="一苇渡江",
  [11] ="乘风破浪",
  [12] ="魔王回首",
  [13] ="百毒不侵2",
  [14] ="烟雨剑法",
  [15] ="百万神兵",
  [16] ="镇妖",
  [17] ="含情脉脉",
  [18] ="如花解语",
  [19] ="莲步轻舞",
  [20] ="日月乾坤",
  [21] ="连环击",
  [22] ="鹰击",
  [23] ="变身",
  [24] ="天雷斩",
  [25] ="杀气诀",
  [26] ="针灸",
  [27] ="推气过宫",
  [28] ="普渡众生",
  [29] ="生命之泉",
  [30] ="炼气化神",
  [31] ="驱魔",
  [32] ="尸腐毒",
  [33] ="幽冥鬼眼",
  [34] ="天神护体",
  [35] ="定心术",
  [36] ="杨柳甘露",
  [37] ="紧箍咒",
  [38] ="天罗地网",
  [39] ="摄魄",
  [40] ="勾魂",
  [41] ="解封",
  [42] ="清心",
  [43] ="解封",
  [44] ="我佛慈悲",
  [45] ="归元咒",
  [46] ="碎甲符",
  [47] ="明光宝烛",
  [48] ="解毒",
  [50] ="极度疯狂",
  [51] ="楚楚可怜",
  [53] ="反间之计",
  [54] ="失心符",
  [55] ="威慑",
  [56] ="天罗地网",
  [57] ="推气过宫",
  [58] ="金刚护法",
  [59] ="百毒不侵",
  [60] ="寡欲令",
  [61] ="宁心",
  [62] ="极度疯狂",
  [63] ="满天花雨",
  [49] ="象形",
  [64] ="放下屠刀",
  [65] ="野兽之力",
  [66] ="魔兽之印",
  [67] ="光辉之甲",
  [68] ="圣灵之甲",
  [69] ="流云诀",
  [70] ="啸风诀",
  [71] ="失忆符",
  [72] ="乘风破浪",
  [73] ="地涌金莲",
  [74] ="太极护法",
  [75] ="罗汉金钟",
  [76] ="鬼魂术",
  [77] ="隐身",
  [78] ="失忆符",
  [79] ="夜舞倾城",
  [80] ="错乱",
  [81] ="四海升平",
  [82] ="玉清诀",
  [83] ="晶清诀",
  [84] ="水清诀",
  [85] ="冰清诀",
  [86] ="勾魂",
  [87] ="摄魄",
  [88] ="推气过宫",
  [89] ="高级鬼魂术",
  [90] ="毒",
  [91] ="失忆符",
  [92] ="魔王回首",
  [93] ="离魂符",
  [94] ="追魂符",
  [95] ="一苇渡江",
  [96] ="宁心",
  [97] ="牛劲",
  [52] ="幽冥鬼眼",
  [98] ="象形限制",
  [99] ="盘丝阵",
  [100] ="魔音摄魂",
  [101] ="复苏",
  [102] ="破釜沉舟",
  [103] ="安神诀",
  [104] ="佛法无边",
  [105] ="一笑倾城",
  [106] ="飞花摘叶",
  [107] ="分身术",
  [108] ="碎甲符",
  [109] ="神龙摆尾",
  [110] ="金刚镯",
  [111] ="乾坤妙法",
  [112] ="天地同寿",
  [113] ="颠倒五行",
  [114] ="灵动九天",
  [115] ="火甲术",
  [116] ="黄泉之息",
  [117] ="还阳术",
  [118] ="魔息术",
  [119] ="瘴气",
  [120] ="翻江搅海",
  [121] ="天崩地裂",
  [122] ="断岳势",
  [123] ="惊涛怒",
  [124] ="碎星诀",
  [125] ="不动如山",
  [126] ="夺魄令",
  [127] ="煞气诀",
  [128] ="摧心术",
  [129] ="雾杀",
  [130] ="金身舍利",
  [131] ="蜜润",
  [132] ="血雨",
  [133] ="镇魂诀",
  [134] ="腾雷",
  [135] ="灵法",
  [136] ="灵断",
  [137] ="护佑",
  [138] ="御风",
  [139] ="怒吼",
  [140] ="阳护",
  [141] ="灵刃",
  [142] ="瞬击",
  [143] ="瞬法",
  [144] ="混元伞",
  [145] ="宝烛",
  [146] = "分水",
  [147] = "赤焰",
  [148] = "神木宝鼎",
  [149] = "我佛慈悲状态",
  [150] = "舍身取义状态"
  }
function 战斗处理类:初始化()
 self.回合数=0
 self.理直气壮=false
 self.结束条件=false
 self.中断结束=false
 self.死亡惩罚=true
 self.观战玩家={}
 self.加载起始=0
 self.加载等待=0
 self.战意点数=2
 self.战斗开始=false





 end



function 战斗处理类:更新()
  if self.防卡战斗.回合==self.回合数 and os.time()-self.防卡战斗.时间>=180 and self.防卡战斗.执行==false then
   self.防卡战斗.执行=true
   self:强制结束战斗()
   end
  if self.回合进程=="加载回合" then
   if os.time()-self.加载起始>=self.加载等待 then
      self:结束加载回合()
     end
  elseif self.回合进程=="命令回合" then
    if os.time()-self.等待起始>=self.等待结束 then
      self:命令回合处理()
     end
  elseif self.回合进程=="执行回合" then
   if os.time()-self.等待起始>=self.等待结束 then
      self:执行回合处理()
     end
  elseif self.回合进程=="结束回合1" then
     self.回合进程="结束回合2"
    self:发送结束信息()
    end
 end
function 战斗处理类:结束加载回合()
 self.回合进程="加载回合结束"
  for n=1,#self.参战玩家 do
   if self.参战玩家[n].加载==false then
     end
    end
 self:战斗开始前处理()
 end
function 战斗处理类:战斗开始前处理()
 self.等待结束=0
 self.战斗流程={信息提示={}}
 for n=1,#self.参战单位 do
   if self.参战单位[n].隐身~=0 then
     self:隐身处理(n)
     end
   if self.参战单位[n].附加技能~=nil and #self.参战单位[n].附加技能>0 then
      for i=1,#self.参战单位[n].附加技能 do
       self.参战单位[n].命令数据={下达=false,类型="技能",目标=n,附加=0,参数=self.参战单位[n].附加技能[i]}
       self:技能计算(n,self.参战单位[n].附加技能[i])
       self.参战单位[n].下达=true
       end
     end
   end
 if #self.战斗流程==0 then
   self:发送命令回合()
  else
     self.回合进程="执行回合"
     self.等待起始=os.time()
     self.循环开关=false
     self.接收数量=#self.参战玩家
   for n=1,#self.参战玩家 do
     发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6005,self.战斗流程)
     end
   end
 end
function 战斗处理类:瞬法处理(编号)
 if self.参战单位[编号]==nil then
   return 0
   end
      self.相关id=self.参战单位[编号].队伍id
      self.临时参数=self.参战单位[编号].法术状态组["瞬法"].技能
      self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
      self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      self.参战单位[编号].法术状态组["瞬法"].回合=1
      self.参战单位[编号].命令数据={下达=false,类型="技能",目标=self.临时目标[1],附加=0,参数=self.临时参数}
      self:技能计算(编号,self.参战单位[编号].法术状态组["瞬法"].技能)
      self.参战单位[编号].下达=true
     end
function 战斗处理类:瞬击处理(编号)
 if self.参战单位[编号]==nil then
   return 0
   end
      self.相关id=self.参战单位[编号].队伍id
      self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      self.参战单位[编号].法术状态组["瞬击"].回合=1
      self.参战单位[编号].命令数据={下达=false,类型="攻击",目标=self.临时目标[1],附加=0}
      self:攻击流程计算(编号,1)
      self.参战单位[编号].下达=true
     end
function 战斗处理类:隐身处理(编号)
 if self.参战单位[编号]==nil then
   return 0
   end
 self.战斗流程[#self.战斗流程+1]={流程=108,执行=false,攻击方=编号,结束=false,类型="技能",参数="修罗隐身"}
 self.等待结束=self.等待结束+4
 if self.参战单位[编号].法术状态组 == nil then self.参战单位[编号].法术状态组 = {} end
 if self.参战单位[编号].法术状态组["隐身"]==nil then self.参战单位[编号].法术状态组["隐身"]={有无=false} end
 self.参战单位[编号].法术状态组["隐身"].有无=true
 self.参战单位[编号].法术状态组["隐身"].回合=self.参战单位[编号].隐身
 end
function 战斗处理类:发送命令回合()
 self.战斗开始=true
 for n=1,#self.参战玩家 do
   self.命令单位={}
   for i=1,#self.参战单位 do
     self.参战单位[i].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
     if self.参战单位[i].玩家id==self.参战玩家[n].玩家id then
         self.命令单位[#self.命令单位+1]=i
       end
     end
   if 玩家数据[self.参战玩家[n].玩家id]~=nil  then
      发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6004,self.命令单位)
   end
   end
 self.回合进程="命令回合"
 self.等待起始=os.time()
 self.等待结束=self.等待时间.初始+self.等待时间.延迟
 self.循环开关=false
 self.接收数量=0
 for n=1,#self.参战玩家 do
   if self.参战玩家[n].逃跑==false then
     self.接收数量=self.接收数量+1
     end
   end
 end
function 战斗处理类:执行回合计算()
 self.战斗流程={信息提示={}}
 for n=1,#self.执行单位 do
   if self:取存活状态(self.执行单位[n]) then --存活则进行结算
     for i=1,#法术状态名称 do
       if  self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].有无 then
          if self.参战单位[self.执行单位[n]].法术状态组.魔音摄魂.有无==false and (法术状态名称[i]=="普渡众生" or 法术状态名称[i]=="生命之泉" or 法术状态名称[i]=="宝烛" ) then
            if 法术状态名称[i]=="宝烛" and self.参战单位[self.执行单位[n]].当前气血 <= self.参战单位[self.执行单位[n]].气血上限*0.7 then
              self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=102,挨打方=self.执行单位[n],伤害=self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害,伤害类型="加血",死亡=0}
              self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血+self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害
              self.等待结束=self.等待结束+2
              if self.参战单位[self.执行单位[n]].当前气血>self.参战单位[self.执行单位[n]].气血上限 then
                self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].气血上限
              end
            elseif 法术状态名称[i]=="普渡众生" or 法术状态名称[i]=="生命之泉" then
              self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=102,挨打方=self.执行单位[n],伤害=self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害,伤害类型="加血",死亡=0}
              self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血+self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害
              self.等待结束=self.等待结束+2
              if self.参战单位[self.执行单位[n]].当前气血>self.参战单位[self.执行单位[n]].气血上限 then
                self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].气血上限
              end
            end
          elseif 法术状态名称[i]=="尸腐毒" or 法术状态名称[i]=="紧箍咒"  or 法术状态名称[i]=="雾杀" then
             self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害,伤害类型="掉血",死亡=0}
             self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血-self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害
             self.等待结束=self.等待结束+2
             if 法术状态名称[i]=="尸腐毒" and self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].蚩尤元神==1 then
             self.临时减魔=math.floor(self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].伤害*0.5)
             self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法-self.临时减魔
              if self.参战单位[self.执行单位[n]].当前魔法<0 then
               self.参战单位[self.执行单位[n]].当前魔法=0
               end
             end
             if self.参战单位[self.执行单位[n]].当前气血<=0 then
               self.参战单位[self.执行单位[n]].当前气血=0
               self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.执行单位[n]])
               end
             self:催眠状态解除("掉血",self.参战单位[self.执行单位[n]])
         elseif 法术状态名称[i]=="炼气化神" or 法术状态名称[i]=="魔息术" then
           self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法+self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].魔法
           if self.参战单位[self.执行单位[n]].当前魔法>self.参战单位[self.执行单位[n]].魔法上限 then
             self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].魔法上限
             end
         elseif 法术状态名称[i]=="毒"  then
            self.扣除气血=math.floor(self.参战单位[self.执行单位[n]].当前气血*0.1)
            if self.扣除气血>self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].等级*50 then
              self.扣除气血=self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].等级*50
               end
           if self.参战单位[self.执行单位[n]].当前气血>self.扣除气血 then

              self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=self.扣除气血,伤害类型="掉血",死亡=0}
              self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血-self.扣除气血
              self.等待结束=self.等待结束+2
              if self.参战单位[self.执行单位[n]].当前气血<=0 then
                self.参战单位[self.执行单位[n]].当前气血=0
                self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.执行单位[n]])
                end
              end
            self:催眠状态解除("掉血",self.参战单位[self.执行单位[n]])
         elseif 法术状态名称[i]=="横扫千军" then
          self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
         elseif 法术状态名称[i]=="含情脉脉" and self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].忘忧 ~= nil then
           self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法 - 60
            if self.参战单位[self.执行单位[n]].当前魔法<=0 then
             self.参战单位[self.执行单位[n]].当前魔法=0
            end
         elseif 法术状态名称[i]=="血雨" then
           self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
         elseif 法术状态名称[i]=="天崩地裂" then
          self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
         elseif 法术状态名称[i] == "赤焰" then
          self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法+取随机数(15,30)+self.参战单位[self.执行单位[n]].赤焰
            if self.参战单位[self.执行单位[n]].当前魔法>=self.参战单位[self.执行单位[n]].魔法上限 then
             self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].魔法上限
             end

         elseif 法术状态名称[i]=="鹰击" then
          if self.参战单位[self.执行单位[n]].命令数据.类型~="防御" then
            self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
            end
         elseif 法术状态名称[i]=="翻江搅海" then
          if self.参战单位[self.执行单位[n]].命令数据.类型~="防御" then
            self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
            end
          elseif 法术状态名称[i]=="后发制人" then
           if self:取存活状态(self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].目标)  then
             self.参战单位[self.执行单位[n]].命令数据.目标=self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].目标
             self.参战单位[self.执行单位[n]].命令数据.类型="技能"
             self.参战单位[self.执行单位[n]].命令数据.参数="后发制人"
             self:单体物理法术计算(self.执行单位[n])
             self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
             self:解除封印状态(self.参战单位[self.执行单位[n]],法术状态名称[i])
             self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].有无=false
            else
             self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=104,挨打方=self.执行单位[n],参数=法术状态名称[i]}
             self:解除封印状态(self.参战单位[self.执行单位[n]],法术状态名称[i])
             self.参战单位[self.执行单位[n]].法术状态组[法术状态名称[i]].有无=false
             self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
             end
            end
         end
       end
    else
      --检查后发
      if self.参战单位[self.执行单位[n]].法术状态组["后发制人"].有无 then
       self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=104,挨打方=self.执行单位[n],参数="后发制人"}
       self:解除封印状态(self.参战单位[self.执行单位[n]],"后发制人")
       self.参战单位[self.执行单位[n]].法术状态组["后发制人"].有无=false
       self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
        end
     end
   end
 if self.战斗类型==100005 and self.回合数>=3 then
   --寻找狸
   self.寻找编号=0
   for n=1,#self.参战单位 do
      if self.参战单位[n].队伍id==0 and self.参战单位[n].名称=="狸" then
        self.寻找编号=n
        end
     end
   if self.寻找编号~=0 and self:取存活状态(self.寻找编号)  then
      self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.寻找编号,伤害=self.参战单位[self.寻找编号].当前气血,伤害类型="掉血",死亡=0}
      self.参战单位[self.寻找编号].当前气血=0
      self.等待结束=self.等待结束+2
      if self.参战单位[self.寻找编号].当前气血<=0 then
        self.参战单位[self.寻找编号].当前气血=0
        self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.寻找编号])
        end
     end
   end

 for n=1,#self.执行单位 do
   if self.参战单位[self.执行单位[n]].经脉有无 ~= nil and self.参战单位[self.执行单位[n]].经脉有无 then
      self:经脉回合开始处理(self.参战单位[self.执行单位[n]])
   end
   if self.中断结束 then --行动状态统一判断
    elseif self:取存活状态(self.执行单位[n])==false or self.参战单位[self.执行单位[n]].法术状态组["破釜沉舟"].有无 or self.参战单位[self.执行单位[n]].法术状态组["横扫千军"].有无 or self.参战单位[self.执行单位[n]].法术状态组["催眠符"].有无 or self.参战单位[self.执行单位[n]].法术状态组["楚楚可怜"].有无  or self.参战单位[self.执行单位[n]].法术状态组["天崩地裂"].有无 or self.参战单位[self.执行单位[n]].法术状态组["煞气诀"].有无  or self.参战单位[self.执行单位[n]].法术状态组["血雨"].有无 then --存活状态判断
    else
     if self.参战单位[self.执行单位[n]].命令数据.类型=="攻击" and self.参战单位[self.执行单位[n]].攻击封印==false then

        if 取随机数()<=self.参战单位[self.执行单位[n]].连击 then

            self.参战单位[self.执行单位[n]].命令数据.类型="技能"
            self.参战单位[self.执行单位[n]].命令数据.参数="连击"
             self:单体物理法术计算(self.执行单位[n])

             if 取随机数(10,100)<=30 and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
               self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
               self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
               self:技能计算(self.执行单位[n],self.使用技能)
              end

       elseif self.参战单位[self.执行单位[n]].理直气壮 then
          self:攻击流程计算(self.执行单位[n])
                 if 取随机数(10,100)<=40  then
                self.理直气壮=true
                end
                if self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标) then
                if self.理直气壮 then
                self:攻击流程计算(self.执行单位[n],1)
                end
                end
                if self.理直气壮 and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
                self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
                self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
                --if self.
                self:技能计算(self.执行单位[n],self.使用技能)
                end
                if self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标) then
                if self.理直气壮 then
               self:攻击流程计算(self.执行单位[n],1)
                end
                end
                if self.理直气壮 and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
                self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
                self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
                --if self.
                self:技能计算(self.执行单位[n],self.使用技能)
                end
          else
                 self:攻击流程计算(self.执行单位[n])
         end
        if  self.参战单位[self.执行单位[n]].追击 then
          if self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标)==false then
             self:攻击流程计算(self.执行单位[n],1)
              if  self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
              self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
              self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
              self:技能计算(self.执行单位[n],self.使用技能)
              end
          end
       end
        if 取随机数(10,100)<=30 and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
         self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
         self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
         self:技能计算(self.执行单位[n],self.使用技能)
        end
        if self.参战单位[self.执行单位[n]].怒击 then
          if self.苍鸾怒击==true and
            -- 狼:互斥
            ((not self.参战单位[self.执行单位[n]].理直气壮) or (self.参战单位[self.执行单位[n]].理直气壮 == 0)) and
            ((not self.参战单位[self.执行单位[n]].连击)  or (self.参战单位[self.执行单位[n]].连击 == 0)) then
           self:攻击流程计算(self.执行单位[n],1)
         if  self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 then
           self.使用技能=self.参战单位[self.执行单位[n]].追加技能[取随机数(1,#self.参战单位[self.执行单位[n]].追加技能)]
           self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
           self:技能计算(self.执行单位[n],self.使用技能)
          end
          end
        end

        if self.参战单位[self.执行单位[n]].嗜血幡~=nil and 取随机数() <= self.参战单位[self.执行单位[n]].嗜血幡 and self:取存活状态(self.执行单位[n]) then

          self.相关id = self.参战单位[self.执行单位[n]].队伍id
          self.临时目标 = self:取随机单位(self.执行单位[n], self.相关id, 1, 1)

          if self.临时目标 == 0 then
            self.参战单位[self.执行单位[n]].命令数据.目标 = 0
          else
            self.参战单位[self.执行单位[n]].命令数据.目标 = self.临时目标[1]
          end
          self:攻击流程计算(self.执行单位[n], 1)
        end
      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="捕捉" then
        self:捕捉流程计算(self.执行单位[n])
     elseif self.参战单位[self.执行单位[n]].命令数据.类型=="逃跑" and self.战斗类型~=200003 then
        self:逃跑流程计算(self.执行单位[n])

      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="召唤" and self:取存活状态(self.执行单位[n]) then
        self:召唤流程计算(self.执行单位[n])

      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="道具" or self.参战单位[self.执行单位[n]].命令数据.类型=="法宝" then
        self:道具流程计算(self.执行单位[n])
      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="特技" and self.参战单位[self.执行单位[n]].特技封印==false then
        self.临时参数=self.参战单位[self.执行单位[n]].命令数据.参数

       if self.临时参数=="破碎无双" or self.临时参数=="弱点击破" or self.临时参数=="破血狂攻" or self.临时参数=="烟雨剑法"  or self.临时参数=="连环击" or self.临时参数=="断岳势" or self.临时参数=="腾雷" then --单体法术设置

          self:单体物理法术计算(self.执行单位[n],1)

        elseif self.临时参数=="放下屠刀" or self.临时参数=="凝神决" or self.临时参数=="凝气决" or self.临时参数=="气疗术" or self.临时参数=="命疗术" or self.临时参数=="命归术" or self.临时参数=="气归术" or self.临时参数=="冰清诀"  or self.临时参数=="水清诀"  then --单体法术设置
         self:单体恢复法术计算(self.执行单位[n],1)
        elseif  self.临时参数=="四海升平" or self.临时参数=="晶清诀" or self.临时参数=="玉清诀" or self.临时参数=="圣灵之甲" or self.临时参数=="魔兽之印" or self.临时参数=="罗汉金钟"   then --单体法术设置
         self:群体恢复法术计算(self.执行单位[n],1)
        elseif  self.临时参数=="冥王暴杀" or self.临时参数=="诅咒之伤"  then --单体法术设置
         self:单体法术计算(self.执行单位[n],1)
        elseif  self.临时参数=="野兽之力" or self.临时参数=="光辉之甲" or self.临时参数=="太极护法"   then --单体法术设置
         self:单体封印法术计算(self.执行单位[n],1)

         elseif  self.临时参数=="慈航普渡"  then --单体法术设置
         self:群体复活法术计算(self.执行单位[n],1)

        end
      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="技能" and self.参战单位[self.执行单位[n]].法术状态组.象形限制.有无==false and self.参战单位[self.执行单位[n]].法术状态组.隐身.有无==false and (self.参战单位[self.执行单位[n]].技能封印==false  or self.参战单位[self.执行单位[n]].命令数据.参数 == "雨落寒沙") then

        if self.参战单位[self.执行单位[n]].命令数据.参数=="摇头摆尾" then

         if 取随机数()<=50 then

           self.参战单位[self.执行单位[n]].命令数据.参数="飞砂走石"

          else
           self.参战单位[self.执行单位[n]].命令数据.参数="三昧真火"
           end

           self.参战单位[self.执行单位[n]].命令数据.摇头摆尾=1
         end
         self:技能计算(self.执行单位[n],self.参战单位[self.执行单位[n]].命令数据.参数)
      elseif self.参战单位[self.执行单位[n]].命令数据.类型=="技能1" and self.参战单位[self.执行单位[n]].技能封印==false then
        self.临时参数=self.参战单位[self.执行单位[n]].命令数据.参数

        if self.临时参数=="力劈华山" or self.临时参数=="夜舞倾城" or self.临时参数=="水攻" or self.临时参数=="雷击" or self.临时参数=="烈火" or self.临时参数=="落岩" or self.临时参数=="五雷咒" or self.临时参数=="龙腾" or self.临时参数=="三昧真火" or self.临时参数=="五雷轰顶" or self.临时参数=="荆棘舞"  or self.临时参数=="尘土刃" or self.临时参数=="冰川怒" or self.临时参数=="判官令" then --单体法术设置

          self:单体法术计算(self.执行单位[n])
          if  self.参战单位[self.执行单位[n]].法连>=取随机数() then
            self:单体法术计算(self.执行单位[n])
           end
          if self.临时参数 == "三昧真火" and self:取经脉(self.参战单位[self.执行单位[n]],"赤暖") and 取随机数() <= 50 then
            self:单体法术计算(self.执行单位[n])
          end

        elseif self.临时参数=="阎罗令" or self.临时参数=="天罗地网" or self.临时参数=="唧唧歪歪" or self.临时参数=="苍茫树" or self.临时参数=="靛沧海" or self.临时参数=="地裂火" or self.临时参数=="日光华" or self.临时参数=="夺命咒"  then --单体法术设置
          self:群体法术计算(self.执行单位[n])
       elseif self.临时参数=="龙卷雨击" or self.临时参数=="飞砂走石" or self.临时参数=="泰山压顶" or self.临时参数=="水漫金山" or self.临时参数=="奔雷咒" or self.临时参数=="地狱烈火" or self.临时参数=="龙吟"  or self.临时参数=="落叶萧萧"  then --单体法术设置
          self:特殊法术计算(self.执行单位[n])
          if  self.参战单位[self.执行单位[n]].法连>=取随机数() then
             self:特殊法术计算(self.执行单位[n])
           end
       elseif self.临时参数=="善恶有报" or self.临时参数=="横扫千军" or self.临时参数=="狮搏" or self.临时参数=="烟雨剑法"  or self.临时参数=="连环击"  or self.临时参数=="裂石"   or self.临时参数=="断岳势"  or self.临时参数=="天崩地裂"  or self.临时参数=="血雨"  or self.临时参数=="腾雷"  then --单体法术设置
          self:单体物理法术计算(self.执行单位[n])
          if self.临时参数 == "血雨" and self:取经脉(self.参战单位[self.执行单位[n]],"血契") and 取随机数()<=50 then
            self:单体物理法术计算(self.执行单位[n])
           end
        elseif  self.临时参数=="天雷斩" or  self.临时参数=="鹰击" or self.临时参数=="浪涌" or  self.临时参数=="惊涛怒" or self.临时参数=="翻江搅海"  then --单体法术设置
          self:群体物理法术计算(self.执行单位[n])
        elseif self.临时参数=="炼气化神" or self.临时参数=="含情脉脉" or  self.临时参数=="似玉生香" or  self.临时参数=="后发制人"  or  self.临时参数=="如花解语" or  self.临时参数=="莲步轻舞" or  self.临时参数=="日月乾坤" or  self.临时参数=="百万神兵"  or  self.临时参数=="镇妖" or  self.临时参数=="定身符" or  self.临时参数=="失魂符"  or  self.临时参数=="失忆符"  or  self.临时参数=="夺魄令" or  self.临时参数=="错乱" then --封印法术设置
         self:单体封印法术计算(self.执行单位[n])
         elseif  self.临时参数=="勾魂" or  self.临时参数=="摄魄"  then
          self:单体封印法术计算1(self.执行单位[n])
        elseif  self.临时参数=="尸腐毒" or  self.临时参数=="紧箍咒"   or  self.临时参数=="雾杀"   then --单体法术设置
         self:单体持续伤害法术计算(self.执行单位[n])
        elseif  self.临时参数=="针灸" or self.临时参数=="归元咒" or self.临时参数=="星月之惠"  or self.临时参数=="波澜不惊"  then --单体法术设置
         self:单体恢复法术计算(self.执行单位[n])
        elseif  self.临时参数=="推气过宫" or self.临时参数=="生命之泉"or self.临时参数=="普渡众生"  or self.临时参数=="地涌金莲"  then --单体法术设置
         self:群体恢复法术计算(self.执行单位[n])
         if self:取经脉(self.参战单位[self.执行单位[n]],"同舟共济") and 取随机数()<=30 then
          self:群体恢复法术计算(self.执行单位[n])
         end
         elseif  self.临时参数=="变身" or self.临时参数=="杀气决"   or self.临时参数=="乘风破浪" or self.临时参数=="金刚护体" or self.临时参数=="金刚护法" or self.临时参数=="碎星诀" or self.临时参数=="不动如山"   or self.临时参数=="明光宝烛"   or self.临时参数=="镇魂诀"     or self.临时参数=="法术防御"    then --单体法术设置
         self:群体状态法术计算(self.执行单位[n])
        elseif  self.临时参数=="我佛慈悲" or  self.临时参数=="杨柳甘露"   then --单体法术设置
          self:单体复活法术计算(self.执行单位[n])
         end
       end
     end
    end
 end
function 战斗处理类:状态数据更新()
 self.发送信息={}
 self.当前封印人数=0
 for n=1,#self.参战单位 do
 if self.参战单位[n].战斗类型~="角色"  then
  if self.参战单位[n].特技封印 == true  or self.参战单位[n].技能封印 == true or self.参战单位[n].催眠封印 == true or self.参战单位[n].攻击封印 == true then
    self.当前封印人数= self.当前封印人数 +1
 if self.当前封印人数 > 4 then
  for n=1,#self.参战单位 do
   self:解除异常状态(self.参战单位[n])
 end
 end
  end
  end

   --添加愤怒
   if self.参战单位[n].愤怒~=nil and self:取存活状态(n) then
     if self.参战单位[n].愤怒>150 then
       end
     end

   if self.参战单位[n].再生 ~= nil and self.参战单位[n].再生~=0 and self:取存活状态(n) then


     self.参战单位[n].当前气血=self.参战单位[n].当前气血+self.参战单位[n].再生

     if self.参战单位[n].当前气血>self.参战单位[n].气血上限 then

       self.参战单位[n].当前气血=self.参战单位[n].气血上限

       end


   end


    --if self.参战单位[n].冥想 == nil then self.参战单位[n].冥想 = 0 end
    if self.参战单位[n].冥想 ~= nil and self.参战单位[n].冥想~=0 and self:取存活状态(n) then

    -- print(self.参战单位[n].冥想,self.参战单位[n].当前魔法,self.参战单位[n].魔法上限)
     self.参战单位[n].当前魔法=self.参战单位[n].当前魔法+self.参战单位[n].冥想
     if self.参战单位[n].当前魔法>self.参战单位[n].魔法上限 then

       self.参战单位[n].当前魔法=self.参战单位[n].魔法上限

       end


     end




   for i=1,#法术状态名称 do

    -- __S服务:输出(法术状态名称[i])
     if self.参战单位[n].法术状态组[法术状态名称[i]] ~= nil and self.参战单位[n].法术状态组[法术状态名称[i]].有无 and self.参战单位[n].法术状态组[法术状态名称[i]].回合 then

       self.参战单位[n].法术状态组[法术状态名称[i]].回合=self.参战单位[n].法术状态组[法术状态名称[i]].回合-1
       if 法术状态名称[i]=="分身术" then

         self.参战单位[n].法术状态组[法术状态名称[i]].躲避=false

         end
      -- print(法术状态名称[i],self.参战单位[n].法术状态组[法术状态名称[i]].回合,n)
       if self.参战单位[n].法术状态组[法术状态名称[i]].回合<=0 then

         self.发送信息[#self.发送信息+1]={id=n,名称=法术状态名称[i],气血=self.参战单位[n].气血上限}
         self.参战单位[n].法术状态组[法术状态名称[i]].有无=false
         self:解除封印状态(self.参战单位[n],法术状态名称[i])

         end


       end


      end


   end
 if #self.发送信息>0 then

   for n=1,#self.参战玩家 do


      发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6006,self.发送信息)

     end

   end

 end
function 战斗处理类:技能计算(攻击方,技能名称)
 self.临时参数=技能名称
 if self.临时参数=="象形" or self.临时参数=="大闹天宫" or self.临时参数=="狮搏" or self.临时参数=="横扫千军" or self.临时参数=="烟雨剑法"  or self.临时参数=="连环击"   or self.临时参数=="善恶有报" or self.临时参数=="裂石"   or self.临时参数=="天崩地裂"    or self.临时参数=="断岳势"  or self.临时参数=="血雨"  or self.临时参数=="腾雷" then --单体法术设置
    self:单体物理法术计算(攻击方)
        if self.临时参数 == "血雨" and self:取经脉(self.参战单位[攻击方],"血契") and 取随机数()<=50 then
          self:单体物理法术计算(攻击方)
         end

  elseif self.临时参数=="夺命蛛丝" or self.临时参数=="天魔解体" or self.临时参数=="黄泉之息" or self.临时参数=="满天花雨" or self.临时参数=="力劈华山" or self.临时参数=="夜舞倾城" or self.临时参数=="水攻" or self.临时参数=="雷击" or self.临时参数=="烈火" or self.临时参数=="落岩" or self.临时参数=="五雷咒" or self.临时参数=="龙腾" or self.临时参数=="三昧真火" or self.临时参数=="五雷轰顶" or self.临时参数=="判官令"  then --单体法术设置
          self:单体法术计算(攻击方)
          if  self.参战单位[攻击方].法连>=取随机数() and self.临时参数~="力劈华山" then
            self:单体法术计算(攻击方)
           end
           if self.参战单位[攻击方].法术状态组.佛法无边.有无 and 取随机数(1,1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
             self:单体法术计算(攻击方)
             end
          if self.临时参数 == "三昧真火" and self:取经脉(self.参战单位[攻击方],"赤暖") and 取随机数() <= 50 then
            self:单体法术计算(攻击方)
          end
        elseif self.临时参数=="荆棘舞"  or self.临时参数=="尘土刃" or self.临时参数=="冰川怒"  then
              self:单体法术计算(攻击方)
               if self:取经脉(self.参战单位[攻击方],"追击") and 取随机数()<=50 then
                self:单体法术计算(攻击方)
               end
        elseif self.临时参数=="雷霆万钧"  or self.临时参数=="九天玄火"   or self.临时参数=="雨落寒沙" or self.临时参数=="落雷符" or self.临时参数=="阎罗令" or self.临时参数=="天罗地网" or self.临时参数=="唧唧歪歪" or self.临时参数=="苍茫树" or self.临时参数=="靛沧海" or self.临时参数=="地裂火" or self.临时参数=="日光华"  or self.临时参数=="夺命咒"  or self.临时参数=="摧心术"   then --单体法术设置
          self:群体法术计算(攻击方)
       elseif  self.临时参数=="二龙戏珠" or self.临时参数=="龙卷雨击" or self.临时参数=="飞砂走石" or self.临时参数=="泰山压顶" or self.临时参数=="水漫金山" or self.临时参数=="奔雷咒" or self.临时参数=="地狱烈火" or self.临时参数=="龙吟"  then --单体法术设置

          self:特殊法术计算(攻击方)
          if  self.参战单位[攻击方].法连>=取随机数() then

             self:特殊法术计算(攻击方)

           end

          if self.参战单位[攻击方].法术状态组.佛法无边.有无 and 取随机数(1,1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
            self:特殊法术计算(攻击方)

             end

          if self.临时参数=="龙卷雨击" and self.参战单位[攻击方].蚩尤元神==1 and 取随机数()<=25 then

             self:特殊法术计算(攻击方)

             end

       elseif self.临时参数=="落叶萧萧"  then

               self:群体法术计算(攻击方)
               if 取随机数(1,10)<=3 then

               self:群体法术计算(攻击方)

               end
        elseif self.临时参数=="飘渺式" or self.临时参数=="天雷斩" or  self.临时参数=="鹰击" or  self.临时参数=="破釜沉舟"  or self.临时参数=="浪涌"  or self.临时参数=="惊涛怒"   or self.临时参数=="翻江搅海" or self.临时参数=="剑荡四方"  then --单体法术设置

          self:群体物理法术计算(攻击方)
        elseif self.临时参数=="瘴气"  or self.临时参数=="魔息术" or self.临时参数=="一笑倾城" or self.临时参数=="炼气化神" or  self.临时参数=="威慑" or self.临时参数=="离魂符" or self.临时参数=="追魂符" or self.临时参数=="失心符" or self.临时参数=="催眠符" or self.临时参数=="含情脉脉" or  self.临时参数=="似玉生香" or  self.临时参数=="后发制人"  or  self.临时参数=="如花解语" or  self.临时参数=="莲步轻舞" or  self.临时参数=="日月乾坤" or  self.临时参数=="百万神兵"  or  self.临时参数=="镇妖" or  self.临时参数=="定身符" or  self.临时参数=="失魂符"  or  self.临时参数=="失忆符"  or  self.临时参数=="夺魄令"  or  self.临时参数=="煞气诀"  or  self.临时参数=="错乱"  then --封印法术设置

         self:单体封印法术计算(攻击方)
         elseif  self.临时参数=="勾魂" or  self.临时参数=="摄魄"  then

          self:单体封印法术计算1(攻击方)


        elseif  self.临时参数=="尸腐毒" or  self.临时参数=="紧箍咒"  or  self.临时参数=="雾杀"  then --单体法术设置
         self:单体持续伤害法术计算(攻击方)
        elseif self.临时参数=="舍身取义"  or self.临时参数=="驱尸" or  self.临时参数=="清心" or self.临时参数=="解毒" or self.临时参数=="归元咒" or   self.临时参数=="针灸" or self.临时参数=="星月之惠"  or self.临时参数=="波澜不惊"  then --单体法术设置
         self:单体恢复法术计算(攻击方)
          if self.参战单位[攻击方].法术状态组.佛法无边.有无 and 取随机数(1,1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
           self:单体恢复法术计算(攻击方)

             end

        elseif  self.临时参数=="推气过宫" or self.临时参数=="生命之泉" or self.临时参数=="普渡众生"  or self.临时参数=="地涌金莲"  then --单体法术设置
         self:群体恢复法术计算(攻击方)
         if self:取经脉(self.参战单位[攻击方],"同舟共济") and 取随机数()<=30 then
          self:群体恢复法术计算(攻击方)
         end

        elseif self.临时参数 == "金蟾"  then
         self:群体恢复法术计算(攻击方,nil,"金蟾")

         elseif self.临时参数=="火甲术"  or  self.临时参数=="颠倒五行" or self.临时参数=="飞花摘叶" or self.临时参数=="乾坤妙法"  or self.临时参数=="天地同寿"  or self.临时参数=="灵动九天"  or  self.临时参数=="金刚镯"  or  self.临时参数=="神龙摆尾" or self.临时参数=="分身术" or self.临时参数=="碎甲符" or self.临时参数=="佛法无边" or self.临时参数=="魔音摄魂" or self.临时参数=="安神诀" or self.临时参数=="复苏" or self.临时参数=="盘丝阵" or self.临时参数=="极度疯狂" or self.临时参数=="魔王回首" or  self.临时参数=="牛劲" or  self.临时参数=="幽冥鬼眼" or  self.临时参数=="驱魔" or  self.临时参数=="解封" or self.临时参数=="宁心" or self.临时参数=="天神护体" or self.临时参数=="一苇渡江" or self.临时参数=="百毒不侵" or self.临时参数=="楚楚可怜" or self.临时参数=="定心术" or self.临时参数=="变身" or self.临时参数=="杀气决"   or self.临时参数=="乘风破浪" or self.临时参数=="金刚护体" or self.临时参数=="金刚护法"  or self.临时参数=="碎星诀"  or self.临时参数=="不动如山" or self.临时参数=="明光宝烛" or self.临时参数=="镇魂诀" or self.临时参数=="法术防御" then --单体法术设置
         self:群体状态法术计算(攻击方)

        elseif self.临时参数=="金身舍利"  or  self.临时参数=="蜜润"  then --单体法术设置

          self:群体状态法术计算(攻击方)


        elseif self.临时参数=="还阳术" or  self.临时参数=="我佛慈悲" or  self.临时参数=="杨柳甘露"   then --单体法术设置

          self:单体复活法术计算(攻击方)

       elseif  self.临时参数=="修罗隐身"   then --单体法术设置

          self:修罗隐身计算(攻击方)

   end


 end
function 战斗处理类:取技能等级(攻击方,技能名称)
 if 装备特技[技能名称]~=nil then return 0 end
 for n=1,#攻击方.主动技能 do
    if 攻击方.主动技能[n].名称==技能名称 then
      return 攻击方.主动技能[n].等级
      end
   end

 if 攻击方.追加技能==nil then return 0 end
  for n=1,#攻击方.追加技能 do
    if 攻击方.追加技能[n]==技能名称 then
      return 攻击方.等级 + 10

      end

   end

  for n=1,#攻击方.附加技能 do

    if 攻击方.附加技能[n]==技能名称 then

      return 攻击方.等级 + 10

      end

   end


 return 0

 end
function 战斗处理类:取日月乾坤结束(n)
  for n=1,15 do

    if self.战斗类型==100000+n then

      return true

       end

   return false

    end
  end
function 战斗处理类:执行回合处理()
  self:状态数据更新()

   self.死亡人数=0
  for n=1,#self.参战单位 do
   if self.参战单位[n].队伍id==self.队伍区分[1] then
     if self:取存活状态(n)==false or (self.参战单位[n].队伍id==0 and self.参战单位[n].法术状态组.日月乾坤 and self.参战单位[n].法术状态组.日月乾坤.有无 and self:取日月乾坤结束(n)) then
       self.死亡人数=self.死亡人数+1
       end
     end
    end
   if self.死亡人数==self.队伍数量[1] then
    self:结束战斗(self.队伍区分[2])
    return 0
    end
  self.死亡人数=0
  for n=1,#self.参战单位 do
   if self.参战单位[n].队伍id==self.队伍区分[2] then
     if self:取存活状态(n)==false or (self.参战单位[n].队伍id==0 and self.参战单位[n].法术状态组.日月乾坤 and self.参战单位[n].法术状态组.日月乾坤.有无 and self:取日月乾坤结束(n)) then
       self.死亡人数=self.死亡人数+1
       end
     end
    end

   if self.死亡人数==self.队伍数量[2]  then

     self:结束战斗(self.队伍区分[1])
     return 0



    end

 self.回合数=self.回合数+1
 self.防卡战斗.回合=self.回合数
 self.防卡战斗.时间=os.time()

 self:发送命令回合()



 end
function 战斗处理类:命令回合处理()
  self.回合进程="计算回合"
 --计算所有单位命令是否已经下达
 for n=1,#self.参战单位 do
   if self.参战单位[n].命令数据.下达==false then
     self:添加攻击命令(n)
     end
    end
 self.临时单位={}
 for n=1,#self.参战单位 do

   self.临时单位[n]={id=n,sd=self.参战单位[n].速度}
   self.参战单位[n].行动=false


   end

 table.sort(self.临时单位,function(a,b) return a.sd>b.sd end )
 self.执行单位={}
 for n=1,#self.临时单位 do


   self.执行单位[n]=self.临时单位[n].id

   end
  self.等待结束=0
 self:执行回合计算()
 self.回合进程="执行回合"
 self.等待起始=os.time()
 self.循环开关=false
 self.结束等待=0
 --print(#self.战斗流程)

  for n=1,#self.参战玩家 do

   if self.参战玩家[n].逃跑==false then

     self.接收数量=self.接收数量+1
     end

   end


 self.接收数量=#self.参战玩家
 --table.print(self.战斗流程)
 for n=1,#self.观战玩家 do

   if self.观战玩家[n]~=0 and 玩家数据[self.观战玩家[n]]~=nil then
     发送数据(玩家数据[self.观战玩家[n]].连接id,6005,self.战斗流程)
    end
   end
  for n=1,#self.参战玩家 do
    发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6005,self.战斗流程)
    for i=1,#self.参战单位 do
      if self.参战单位[i].当前气血==nil then self.参战单位[i].当前气血=1 end
     if self.参战单位[i].玩家id==self.参战玩家[n].玩家id then
        if self.参战单位[i].战斗类型=="角色" and 玩家数据[self.参战玩家[n].玩家id]~=nil  then
         玩家数据[self.参战玩家[n].玩家id].角色.数据.当前气血=self.参战单位[i].当前气血
         玩家数据[self.参战玩家[n].玩家id].角色.数据.气血上限=self.参战单位[i].气血上限
         玩家数据[self.参战玩家[n].玩家id].角色.数据.最大气血=self.参战单位[i].最大气血
         玩家数据[self.参战玩家[n].玩家id].角色.数据.魔法上限=self.参战单位[i].魔法上限
         玩家数据[self.参战玩家[n].玩家id].角色.数据.当前魔法=self.参战单位[i].当前魔法
         玩家数据[self.参战玩家[n].玩家id].角色.数据.愤怒=self.参战单位[i].愤怒
          发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,1003,玩家数据[self.参战玩家[n].玩家id].角色:获取角色气血数据())
        elseif 玩家数据[self.参战玩家[n].玩家id].召唤兽.数据.参战~=0 then
          self.召唤兽id=玩家数据[self.参战玩家[n].玩家id].召唤兽.数据.参战
          玩家数据[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].当前气血=self.参战单位[i].当前气血
          玩家数据[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].气血上限=self.参战单位[i].气血上限
          玩家数据[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].当前魔法=self.参战单位[i].当前魔法
          玩家数据[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].魔法上限=self.参战单位[i].魔法上限
         self.发送信息={
         当前气血=self.参战单位[i].当前气血,
         气血上限=self.参战单位[i].气血上限,
         当前魔法=self.参战单位[i].当前魔法,
         魔法上限=self.参战单位[i].魔法上限

       }

          发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,2015,self.发送信息)



          end


       end


      end

    end

 end
function 战斗处理类:添加攻击命令(编号)
  self.相关id=self.参战单位[编号].队伍id
 if self.相关id~=0   then
      self.参战单位[编号].命令数据.下达=true
      self.参战单位[编号].命令数据.类型="攻击"
      self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
      else
      self.参战单位[编号].命令数据.目标=self.临时目标[1]
      end
  else
      self.参战单位[编号].命令数据.下达=true
      self.参战单位[编号].命令数据.类型="攻击"
      self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
      else
      self.参战单位[编号].命令数据.目标=self.临时目标[1]
      end
      -- print(self.参战单位[编号].特技数据[1])
      if self.参战单位[编号].特技数据~= nil and #self.参战单位[编号].特技数据>0  and 取随机数()<=20 then
        self.临时技能=self.参战单位[编号].特技数据[取随机数(1,#self.参战单位[编号].特技数据)].名称
        self.参战单位[编号].命令数据.下达=true
        self.参战单位[编号].命令数据.类型="特技"
        self.参战单位[编号].命令数据.参数=self.临时技能
        if 装备特技[self.临时技能]==nil then
        self.参战单位[编号].命令数据.类型=""
        return 0
        end
        if 装备特技[self.临时技能].对象==1 then
         self.参战单位[编号].命令数据.目标=编号
        elseif 装备特技[self.临时技能].对象==2 then
          self.临时目标=self:取随机单位(编号,self.相关id,2,1)
          if self.临时目标==0 then
          self.参战单位[编号].命令数据.目标=0
          else
          self.参战单位[编号].命令数据.目标=self.临时目标[1]
          end
        elseif 装备特技[self.临时技能].对象==3 then
          self.临时目标=self:取随机单位(编号,self.相关id,1,1)
          if self.临时目标==0 then
          self.参战单位[编号].命令数据.目标=0
          else
          self.参战单位[编号].命令数据.目标=self.临时目标[1]
          --self.参战单位[编号].命令数据.目标=1
          end
        end
      elseif #self.参战单位[编号].主动技能>0   then
        self.临时技能=self.参战单位[编号].主动技能[取随机数(1,#self.参战单位[编号].主动技能)].名称
         -- __S服务:输出('替换技能 ........................ ' .. enemy.主动技能[i])
         local ski = self.临时技能
         local enemy = self.参战单位[编号]
        if ski == '变身' and enemy.法术状态组.变身.有无 then
            local r = 取随机数()
          if r < 35 then ski = '鹰击'
          elseif r < 75 then ski = '狮搏'
          else ski = '连环击' end
        elseif ski == '牛劲' and enemy.法术状态组.牛劲.有无 then
            local r = 取随机数()
            if r < 35 then ski = '三昧真火'
            else ski = '飞沙走石' end
        end
        self.临时技能 = ski
        self.参战单位[编号].命令数据.下达=true
        self.参战单位[编号].命令数据.类型="技能"
        self.参战单位[编号].命令数据.参数=self.临时技能
        if 游戏技能[self.临时技能]==nil then
        self.参战单位[编号].命令数据.类型=""
        return 0
        end
        if 游戏技能[self.临时技能].对象==1 then
         self.参战单位[编号].命令数据.目标=编号
        elseif 游戏技能[self.临时技能].对象==2 then
          self.临时目标=self:取随机单位(编号,self.相关id,2,1)
          if self.临时目标==0 then
          self.参战单位[编号].命令数据.目标=0
          else
          self.参战单位[编号].命令数据.目标=self.临时目标[1]
          end
        elseif 游戏技能[self.临时技能].对象==3 then
          self.临时目标=self:取随机单位(编号,self.相关id,1,1)
          if self.临时目标==0 then
          self.参战单位[编号].命令数据.目标=0
          else
          self.参战单位[编号].命令数据.目标=self.临时目标[1]
          --self.参战单位[编号].命令数据.目标=1
          end
        end
      end
  end















 end
function 战斗处理类:进入战斗(玩家id,序号,任务id,地图)
 self.战斗类型=序号
 self.任务id=任务id
 self.战斗失败=false
 self.观战玩家={}
 self.中断计算=false
 self.参战单位={}
 self.参战玩家={}
 self.进入战斗玩家id=玩家id
 self.飞升序号=0
 self.止戈序号=0
 self.清心序号=0
 self.雷霆序号=0
 self.惜花序号=0
 self.忘情序号=0
 self.卧龙序号=0
 self.天象序号=0
 self.轮回序号=0
 self.娑罗序号=0
 self.渡劫序号=0
 self.战斗计时=os.time()
 self.加载等待=7
 self.加载数量=0
 self.回合进程="加载回合"
 self.等待时间={初始=30,延迟=5}
 self.队伍数量={[1]=0,[2]=0}
 self.观战方=玩家id
 self.对战方=任务id
 self.防卡战斗={回合=0,时间=os.time(),执行=false}
 self.pk战斗=false
 self.阵法使用=true
 self.结束等待=0

 if 玩家数据[玩家id].队伍==0 then
       self.发起id=玩家id
   else
      self.发起id=玩家数据[玩家id].队伍

   end

  self.队伍区分={[1]=self.发起id,[2]=0}


  --加载玩家单位
 if self.战斗类型~=200001 and self.战斗类型~=200002 and self.战斗类型~=200003 and self.战斗类型~=200004 then --pk类的战斗


   if 玩家数据[玩家id].队伍==0 then

      self:加载单个玩家(玩家id,1)

     else

       for n=1,#队伍数据[玩家数据[玩家id].队伍].队员数据 do


         self:加载单个玩家(队伍数据[玩家数据[玩家id].队伍].队员数据[n],n)

         end

       end
     self:加载敌方单位(地图)
     self.阵法使用=true
    else
      self.阵法使用=false
      self.pk战斗=true
      if self.战斗类型~=200003 then
        self.死亡惩罚=false
      end

      if 玩家数据[玩家id].队伍==0 then

       self:加载单个玩家(玩家id,1)

     else

       for n=1,#队伍数据[玩家数据[玩家id].队伍].队员数据 do


         self:加载单个玩家(队伍数据[玩家数据[玩家id].队伍].队员数据[n],n)

         end

       end

      if 玩家数据[任务id].队伍==0 then
        self.挑战id=任务id
      else
        self.挑战id=玩家数据[任务id].队伍

      end

      self.队伍区分[2]=self.挑战id

      if 玩家数据[任务id].队伍==0 then

       self:加载单个玩家(任务id,1)

     else

       for n=1,#队伍数据[玩家数据[任务id].队伍].队员数据 do


         self:加载单个玩家(队伍数据[玩家数据[任务id].队伍].队员数据[n],n)

         end

       end


   end

  self:重置战斗属性()
  self:加载单位结束()
  self.加载起始=os.time()

  for n=1,#self.参战玩家 do
   -- print(n,self.参战玩家[n].玩家id)

    地图处理类:更改战斗(self.参战玩家[n].玩家id,true)

    end


 if 调试模式  then  --发送战斗参数
   self.发送信息={}
   for n=1,#self.参战单位 do


      self.发送信息[#self.发送信息+1]="名称="..self.参战单位[n].名称.."编号="..n.."等级="..self.参战单位[n].等级.."气血="..self.参战单位[n].气血上限.."伤害="..self.参战单位[n].伤害.."防御="..self.参战单位[n].防御.."灵力="..self.参战单位[n].灵力.."法防="..self.参战单位[n].法防.."速度="..self.参战单位[n].速度.."必杀="..self.参战单位[n].必杀



     end

   发送数据(玩家数据[self.参战玩家[1].玩家id].连接id,22,self.发送信息)


   end
end
function 战斗处理类:加载敌方单位(单位组)
 if self.战斗类型==100001 then
    self:创建野外单位()
  else
    self:加载指定单位(单位组)
   end
 end
function 战斗处理类:加载指定单位(单位组)
  self.加载数值={"伤害","防御","灵力","速度","躲闪","力量","魔力","体质","耐力","敏捷","力量"}
  self.加载人数=#单位组
 if 调试模式 then
    -- self.加载人数=1
    self.加载人数=#单位组
  else
    self.加载人数=#单位组
   end
 local enemy
 local ski
 local ski1 = { '横扫千军', '破釜沉舟', '浪涌', '烟雨剑法', '飘渺式', '天雷斩', '满天花雨' }
 local ski2 = { '龙卷雨击', '龙腾', '二龙戏珠', '飞砂走石', '三昧真火', '雷霆万钧'}
 for n=1,self.加载人数 do

   local enemy = 单位组[n]
    for i=1, #enemy.主动技能 do
      ski = enemy.主动技能[i]
      if 取随机数() < 65 and (ski == '失心符' or ski == '莲步轻舞' or ski == '似玉生香' or ski == '日月乾坤' or ski == '含情脉脉'
         or ski == '日光华' or ski == '靛沧海' or ski == '巨岩破' or ski == '苍茫树' or ski == '夺命咒' or ski == '天罗地网' or ski == '判官令') then
         if enemy.伤害 > enemy.灵力 then
            enemy.主动技能[i] = ski1[取随机数(1, #ski1)]
         else
            enemy.主动技能[i] = ski2[取随机数(1, #ski2)]
         end
      end
   end
   while enemy.气血 > enemy.等级 * 500 do
     enemy.气血 = enemy.气血 * 0.7
   end
   while enemy.伤害 > enemy.等级 * 18 do
     enemy.伤害 = enemy.伤害 * 0.7
   end
   while enemy.防御 > enemy.等级 * 15 do
     enemy.防御 = enemy.防御 * 0.7
   end
   while enemy.灵力 > enemy.等级 * 15 do
     enemy.灵力 = enemy.灵力 * 0.7
   end
   while enemy.法防 > enemy.等级 * 8 do
     enemy.法防 = enemy.法防 * 0.7
   end
   if enemy.力量 > enemy.等级 * 5 then
      enemy.力量 = enemy.等级 * 5
   else
      enemy.力量 = enemy.力量 + enemy.等级
   end
   if enemy.耐力 > enemy.等级 * 5 then
      enemy.耐力 = enemy.等级 * 5
    else
      enemy.耐力 = enemy.耐力 + enemy.等级
   end
   if enemy.魔力 > enemy.等级 * 5 then
      enemy.魔力 = enemy.等级 * 5
    else
       enemy.魔力 = enemy.魔力 + enemy.等级
   end

   enemy.气血 = math.floor(enemy.气血) + enemy.等级 * 取随机数(32, 64)
   enemy.魔法 = enemy.魔法 + enemy.等级 * 50

   enemy.敏捷 = enemy.敏捷 + 取随机数(1, enemy.等级)
   -- enemy.体质 = enemy.体质 + enemy.等级

   -- enemy.封印必中 = false

   if enemy.等级 >= 109 then
     enemy.伤害 = enemy.伤害 + 取随机数(enemy.等级, enemy.等级 * 2)
     enemy.灵力 = enemy.灵力 + 取随机数(1, enemy.等级 * 1)
     enemy.防御 = enemy.防御 + 取随机数(1, enemy.等级)
     enemy.速度 = enemy.速度 + 取随机数(1, enemy.等级)
     enemy.躲闪 = enemy.躲闪 + 取随机数(1, enemy.等级)
   end
   if enemy.法防 < enemy.灵力 * 0.5 then
      enemy.法防 = math.floor(enemy.灵力 * 0.5)
   end



    self.参战单位[#self.参战单位+1]={}
    for i=1,#self.加载数值 do
     self.参战单位[#self.参战单位][self.加载数值[i]]=math.floor(单位组[n][self.加载数值[i]])
    end


    self.参战单位[#self.参战单位].当前气血=math.floor(单位组[n].气血)
    self.参战单位[#self.参战单位].气血上限=math.floor(单位组[n].气血)
    self.参战单位[#self.参战单位].当前魔法=math.floor(单位组[n].魔法)
    if self.参战单位[#self.参战单位].当前魔法==nil then self.参战单位[#self.参战单位].当前魔法=0 end
    self.参战单位[#self.参战单位].命中=self.参战单位[#self.参战单位].伤害
    self.参战单位[#self.参战单位].魔法上限=math.floor(单位组[n].魔法)
    self.参战单位[#self.参战单位].名称=单位组[n].名称
    self.参战单位[#self.参战单位].等级=单位组[n].等级
    self.参战单位[#self.参战单位].类型=单位组[n].类型
    self.参战单位[#self.参战单位].造型=单位组[n].造型
    self.参战单位[#self.参战单位].编号=#self.参战单位
    self.参战单位[#self.参战单位].法防=math.floor(单位组[n].法防)
    self.参战单位[#self.参战单位].变异=单位组[n].变异
    self.参战单位[#self.参战单位].技能=单位组[n].技能
    self.参战单位[#self.参战单位].战斗类型=单位组[n].战斗类型
    self.参战单位[#self.参战单位].武器数据=单位组[n].武器数据
    self.参战单位[#self.参战单位].染色=单位组[n].染色
    self.参战单位[#self.参战单位].门派技能={}
    self.参战单位[#self.参战单位].物伤减免=单位组[n].物伤减免
    self.参战单位[#self.参战单位].法伤减免=单位组[n].法伤减免
    self.参战单位[#self.参战单位].修炼数据={}
    for n=1,#全局变量.修炼名称 do
      self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=0
    end

    -- if enemy.等级 >= 109 then
      local xl = self.参战单位[#self.参战单位].修炼数据
      xl.攻击 = math.floor(enemy.等级 / 7)
      xl.防御 = math.floor(enemy.等级 / 10)
      xl.法术 = math.floor(enemy.等级 / 15)
      xl.法抗 = math.floor(enemy.等级 / 15)
    -- end

    self.参战单位[#self.参战单位].主动技能={}
    self.参战单位[#self.参战单位].特技数据={}
    for i=1,#单位组[n].主动技能 do
       self.参战单位[#self.参战单位].主动技能[#self.参战单位[#self.参战单位].主动技能+1]={名称=单位组[n].主动技能[i],等级=单位组[n].等级}
    end
    if 单位组[n].特技数据 ~=nil then
      for i=1,#单位组[n].特技数据 do
       self.参战单位[#self.参战单位].特技数据[#self.参战单位[#self.参战单位].特技数据+1]={名称=单位组[n].特技数据[i],等级=单位组[n].等级}
       end
     end

     self.参战单位[#self.参战单位].技能=单位组[n].技能
    self.参战单位[#self.参战单位].位置=n
    self.队伍区分[2]=0
    self.参战单位[#self.参战单位].队伍=0
    self.参战单位[#self.参战单位].怪物=true
    self.参战单位[#self.参战单位].队伍id=0
    self.参战单位[#self.参战单位].索引=n
    self.队伍数量[2]=self.队伍数量[2]+1

    if self.战斗类型==56 then
      self.参战单位[#self.参战单位].战斗类型="角色"
      self.参战单位[#self.参战单位].染色={a=0,b=0,c=0}
      self.参战单位[#self.参战单位].武器数据={强化=1,等级=120,名称=任务数据[self.任务id].武器.名称,类别=武器类型[任务数据[self.任务id].武器.名称]}
    elseif self.战斗类型==100029 then
      self.参战单位[#self.参战单位].不可封印=false
      self.参战单位[#self.参战单位].速度=math.floor(self.参战单位[#self.参战单位].速度*0.5)
    end
   end


 end
function 战斗处理类:创建野外单位()
 self.地图名称=地图处理类.地图数据[玩家数据[self.发起id].地图].名称
 self.地图等级=Q_地图数据[self.地图名称].等级
 self.地图经验=Q_地图数据[self.地图名称].经验
 self.地图物品=Q_地图数据[self.地图名称].物品/10
 self.队伍区分[2]=0
 self.临时数量=取随机数(#self.参战玩家+1,#self.参战玩家*2+1)
 if self.临时数量>10 then self.临时数量=10 end
 for n=1,self.临时数量 do
   self.怪物组=地图处理类.地图数据[玩家数据[self.发起id].地图].怪物
   self.宝宝=false
   self.变异=false
   self.头领=false
   self.造型=self.怪物组[取随机数(1,#self.怪物组)]
   self.更改名称=""
   self.生成等级=self.地图等级
   if 取随机数()<=1 then
     self.变异=true
     self.宝宝=true
     self.更改名称="变异"..self.造型
     self.生成等级=0
    elseif 取随机数()<=3 then
     self.宝宝=true
     self.更改名称=self.造型.."宝宝"
     self.生成等级=0
    elseif 取随机数()<=20 then
     self.头领=true
     self.更改名称=self.造型.."头领"
    else
     self.更改名称=self.造型

     end

    -- if self.参战单位 == nil then self.参战单位 = {} end
    self.参战单位[#self.参战单位+1]={}
    self.参战单位[#self.参战单位].名称=self.更改名称
    self.参战单位[#self.参战单位].位置=n
    self.参战单位[#self.参战单位].编号=#self.参战单位
    self.参战单位[#self.参战单位].造型=self.造型
    self.参战单位[#self.参战单位].变异=self.变异
    self.参战单位[#self.参战单位].等级=self.生成等级 + math.floor(取随机数(0, 10))
    self.参战单位[#self.参战单位].宝宝=self.宝宝
    self.参战单位[#self.参战单位].五行=生成五行()
    self.参战单位[#self.参战单位].法防=math.floor(self.生成等级 * 取随机数(5, 6))
    self.参战单位[#self.参战单位].灵力=math.floor(self.生成等级 * 取随机数(7, 9))
    self.参战单位[#self.参战单位].伤害=math.floor(self.生成等级 * 取随机数(8, 10))
    self.参战单位[#self.参战单位].技能={}
    self.参战单位[#self.参战单位].主动技能={}
    -- __S服务:输出(self.造型)
    -- 狼：没有 召唤兽[狂豹]
    -- __S服务:输出(table.tostring(召唤兽[self.造型]))
    if 召唤兽[self.造型].技能 == nil then 召唤兽[self.造型].技能 = {} end
    for n=1,#召唤兽[self.造型].技能 do
       if 取随机数() <= 75 then
         self.参战单位[#self.参战单位].技能[#self.参战单位[#self.参战单位].技能+1]=召唤兽[self.造型].技能[n]
         end

      end
    self.参战单位[#self.参战单位].命中=self.参战单位[#self.参战单位].伤害
    self.参战单位[#self.参战单位].防御=math.floor(self.生成等级 * 取随机数(5, 6))
    self.参战单位[#self.参战单位].速度=math.floor(self.生成等级 * 2.5)
    self.参战单位[#self.参战单位].躲闪=math.floor(self.生成等级 * 1.5)
    self.参战单位[#self.参战单位].当前气血=math.floor(self.生成等级 * 22.5)+10
    self.参战单位[#self.参战单位].气血上限=math.floor(self.生成等级 * 22.5)+10
    self.参战单位[#self.参战单位].当前魔法=math.floor(self.生成等级 * 7.5)+10
    self.参战单位[#self.参战单位].魔法上限=math.floor(self.生成等级 * 7.5)+10
    self.参战单位[#self.参战单位].队伍id=0
    if self.头领 then
     self.参战单位[#self.参战单位].伤害=math.floor(self.参战单位[#self.参战单位].伤害*1.05)
     self.参战单位[#self.参战单位].防御=math.floor(self.参战单位[#self.参战单位].防御*1.15)
     self.参战单位[#self.参战单位].灵力=math.floor(self.参战单位[#self.参战单位].灵力*1.15)
      end

    self.参战单位[#self.参战单位].修炼数据={}
    for n=1,#全局变量.修炼名称 do

      self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=0
      end

   self.队伍数量[2]=self.队伍数量[2]+1

   end

 end
function 战斗处理类:数据处理(id,序号,内容,参数)

  if 序号==231 then
    self:发送参战单位信息(id)
  elseif 序号==232 and self.回合进程=="加载回合" then
   self.临时玩家id=self:取参战玩家id(id)
   self.参战玩家[self.临时玩家id].加载=true
   self.加载数量=self.加载数量+1
  -- print()
   if self.加载数量==#self.参战玩家 then

     self:结束加载回合()
     end
  elseif 序号==235 and self.回合进程=="命令回合" then
   local 命令信息=分割文本(内容,"*-*")
   local 信息={}
   for n=1,#命令信息 do

     self.分割信息=分割文本(命令信息[n],"@-@")
     --table.print(self.分割信息)
     if self.分割信息[1]~="" then
     -- table.print(self.分割信息)
     信息[#信息+1]={编号=self.分割信息[1]+0,目标=self.分割信息[2]+0,类型=self.分割信息[3],参数=self.分割信息[4],附加=self.分割信息[5]}
      end

     end
   for n=1,#信息 do
      -- table.print(信息[n])
       if 信息[n].目标~=0 or 信息[n].类型=="逃跑" or 信息[n].类型=="防御" or self.参战单位[信息[n].编号].法术状态组.横扫千军.有无 or self.参战单位[信息[n].编号].法术状态组.破釜沉舟.有无 or self.参战单位[信息[n].编号].法术状态组.天崩地裂.有无  or self.参战单位[信息[n].编号].法术状态组.血雨.有无 then
          self.参战单位[信息[n].编号].命令数据={类型=信息[n].类型,目标=信息[n].目标,附加=信息[n].附加,参数=信息[n].参数,下达=true}
          if self.参战单位[信息[n].编号].法术状态组.楚楚可怜.有无 then

            self.参战单位[信息[n].编号].命令数据={类型="取消",目标=0,附加=信息[n].附加,参数=信息[n].参数,下达=true}

            end
        end
       end
     self.接收数量=self.接收数量-1

     if self.接收数量<=0 then

        self:命令回合处理()

       end
  elseif 序号==234 then

   if self.回合进程=="执行回合" then

      self.接收数量=self.接收数量-1
      --self.结束等待=os.time()

     if self.接收数量<=0 then

        self:执行回合处理()
       end
      end
  elseif 序号 == 240 then
    self:获取道具数据1(id)
  elseif 序号==238 then

   self:设置默认法术(id,内容,参数)
  elseif 序号==236 then

   self:获取道具数据(id)

   elseif 序号==237 then

   self:获取召唤兽数据(id)

  elseif 序号==239 then

    for n=1,#self.观战玩家 do

      if self.观战玩家[n]==id then

       玩家数据[id].战斗=0
       玩家数据[id].观战状态=false
       发送数据(玩家数据[self.观战玩家[n]].连接id,6007,"66")
       self.观战玩家[n]=0
        end
      end
    end
 end

function 战斗处理类:获取召唤兽数据(id)
 self.符合id=0
 for n=1,#self.参战单位 do
   if self.参战单位[n].战斗类型=="角色" and self.参战单位[n].玩家id==id then
     self.符合id=n
     end
   end
 if self.符合id~=0 then
    self.发送信息={}

    for n=1,#玩家数据[id].召唤兽.数据 do

     self.发送信息[n]={编号=n,等级=玩家数据[id].召唤兽.数据[n].等级,名称=玩家数据[id].召唤兽.数据[n].名称,当前气血=玩家数据[id].召唤兽.数据[n].当前气血,气血上限=玩家数据[id].召唤兽.数据[n].气血上限,当前魔法=玩家数据[id].召唤兽.数据[n].当前魔法,魔法上限=玩家数据[id].召唤兽.数据[n].魔法上限,忠诚=玩家数据[id].召唤兽.数据[n].忠诚}
     self.发送信息[n].参战=false
     for i=1,#self.参战单位[self.符合id].参战召唤兽 do
        if n==self.参战单位[self.符合id].参战召唤兽[i] then
         self.发送信息[n].参战=true
          end
       end
      end

   发送数据(玩家数据[id].连接id,6009,self.发送信息)
   end
 end
function 战斗处理类:获取道具数据(id)

 发送数据(玩家数据[id].连接id,6008,玩家数据[id].道具:索要道具1(id,"包裹"))
 end

 function 战斗处理类:获取道具数据1(id)
  发送数据(玩家数据[id].连接did, 6011, 玩家数据[id].道具:索要法宝道具(id, "法宝"))
end

function 战斗处理类:设置默认法术(id,参数,编号)
 --self.信息数据=table.loadstring(信息)
 编号=编号+0
 if 参数=="0" then 参数=nil end
 self.参战单位[编号].默认法术=参数

 if self.参战单位[编号].战斗类型=="角色" then

   玩家数据[id].角色.数据.默认法术=参数

  else
   玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].默认法术=参数
   end
 end
function 战斗处理类:取参战玩家id(id)
  for n=1,#self.参战玩家 do
   if self.参战玩家[n].玩家id==id then
      return n
    end
    end
  return 0
 end
function 战斗处理类:退出观战(id)
 for n=1,#self.观战玩家 do
    if self.观战玩家[n]==id then
      发送数据(玩家数据[self.观战玩家[n]].连接id,6007,"66")
      self.观战玩家[n]=0
      end
   end
  end
function 战斗处理类:观战数据发送(id,目标id)
 self.观战玩家[#self.观战玩家+1]=id
 for n=1,#self.参战玩家 do
    if self.参战玩家[n].玩家id==目标id then
      发送数据(玩家数据[id].连接id,6001,self.参战玩家[n].队伍id)
      发送数据(玩家数据[id].连接id,6010,#self.参战单位)
      end
   end
  end
function 战斗处理类:重连数据发送(id)
 for n=1,#self.参战玩家 do
    if self.参战玩家[n].玩家id==id then
      发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6001,self.参战玩家[n].队伍id)
      发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6002,#self.参战单位)
      end
   end
 end
function 战斗处理类:加载单位结束()
 for n=1,#self.参战玩家 do
    发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6001,self.参战玩家[n].队伍id)
    发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6002,#self.参战单位)
   end
 end


function 战斗处理类:发送参战单位信息(id)
    self.发送信息={}

    if not self.resetData then 
        self.resetData = {} 
        self:resetFight(self.resetData) 
    end
    for i=1,#self.参战单位 do
        local temp = { 法术状态组 = {} }
        for k, v in pairs(self.参战单位[i]) do
            if type(v) == 'table' then
                if k == '法术状态组' then
                    for kk, vv in pairs(v) do
                        if vv.有无 then temp.法术状态组[kk] = vv end
                    end
                else
                    temp[k] = v
                end
            elseif(self.resetData[k] == nil or self.resetData[k] ~= v) then
                temp[k] = v
            end
        end
        发送数据(玩家数据[id].连接id,6003, temp)
    end
end

function 战斗处理类:resetFight(个体)
    个体.攻击封印=false
    个体.技能封印=false
    个体.特技封印=false
    个体.道具封印=false
    个体.全能封印=false
    个体.跳过行动=false --横扫千军状态
    个体.催眠封印=false --催眠符状态
    个体.封印状态=false

    个体.必杀=5
    个体.百步穿杨=0
    个体.福泽天下=0
    个体.行云流水=0
    个体.索命无常=0
    个体.烟雨飘摇=0
    个体.天雷地火=0
    个体.石破天惊=0
    个体.网罗乾坤=0
    个体.云随风舞=0
    个体.心随我动=0
    个体.隔山打牛=0
    个体.灵刃=false
    个体.预知=false
    个体.灵动=false
    个体.瞬击=false
    个体.抗法=false
    个体.阳护=false
    个体.识物=false
    个体.洞察=false
    个体.弑神=false
    个体.御风=false
    个体.顺势=false
    个体.复仇=false
    个体.自恋=false
    个体.怒吼=false
    个体.暗劲=false
    个体.逆境=false
    个体.瞬法=false
    个体.灵法=false
    个体.灵断=false
    个体.吮吸=false
    个体.识药=0
    个体.护佑=false
    个体.抗物=false
    个体.吸血=0
    个体.溅射=0
    个体.狂怒=0
    个体.阴伤=0
    个体.驱散=0  ---小法  内丹
    个体.连击=0
    个体.法术暴击伤害= 0
    个体.额外法术伤害=0
    个体.理直气壮=false
    个体.反震=0--.25
    个体.反击=0
    个体.法暴=0
    个体.法连=0
    个体.追击=false
    个体.怒击=false
    个体.法波=0
    个体.魔心=1
    个体.幸运=false
    个体.神佑=0
    个体.复活=0
    个体.冥想=0
    个体.慧根=1
    个体.再生=0
    个体.毒=0
    个体.驱鬼=0
    个体.偷袭=false
    个体.火吸=0
    个体.水吸=0
    个体.雷吸=0
    个体.土吸=0
    个体.夜战=false
    个体.感知=false
    个体.隐身=0
    个体.九黎战鼓 = 0
    个体.盘龙壁  = 0
    个体.神行飞剑 = 0
    个体.汇灵盏 = 0
    个体.织女扇 = 0
    个体.金甲仙衣 = 0
    个体.五彩娃娃 = 0
    个体.嗜血幡 = 0
    个体.混元伞 = 0
    个体.普渡 = 0
    个体.蟠龙玉璧 = 0
    个体.武器伤害=0
    个体.武器宝石=0

    个体.法术状态组 = {}
    for i=1,#法术状态名称 do
        -- 可能不止有无字段
        个体.法术状态组[法术状态名称[i]] = { 有无 = false }
    end
    -- if 个体.特技数据 ==nil then
    --     个体.特技数据={} 
    -- end
    for i=1,#幻化战斗属性 do
        个体[幻化战斗属性[i]]=0
    end
    if 个体.法伤减免==nil then 个体.法伤减免=1 end
    if 个体.物伤减免==nil then 个体.物伤减免=1 end
    -- 个体.主动技能 = {}
    for i=1,#幻化属性值 do
        if 个体[幻化属性值[i]] == nil then 个体[幻化属性值[i]] = 0 end
    end
    -- 个体.治疗能力 = 0 -- 幻化
    -- 个体.固定伤害 = 0 -- 幻化
    个体.封印=0
    个体.神迹=0
    个体.兽王令 = 0
    个体.风灵 = 0
end

function 战斗处理类:重置战斗属性()

  for n=1,#self.参战单位 do
    local 个体 = self.参战单位[n]
    self:resetFight(个体)
    if 个体.主动技能 == nil then 个体.主动技能 = {} end
    if 个体.特技数据 ==nil then 个体.特技数据={} end
  --   for i=1,#幻化战斗属性 do
  --    self.参战单位[n][幻化战斗属性[i]]=0
  --   end
  --  if self.参战单位[n].法伤减免==nil then self.参战单位[n].法伤减免=1 end
  --  if self.参战单位[n].物伤减免==nil then self.参战单位[n].物伤减免=1 end
   if self.参战单位[n].战斗类型~="角色" then
     self:添加技能属性(n,self.参战单位[n].技能)
     if self.参战单位[n].内丹 ~= nil then
      self:添加内丹属性(n,self.参战单位[n].内丹)
      self:添加特性属性(n,self.参战单位[n].特性,self.参战单位[n].特性几率)
    end
    elseif   self.参战单位[n].战斗类型=="角色" and self.参战单位[n].队伍~=0 then
     if self.参战单位[n].变身技能~=0 then
       self:添加技能属性(n,{self.参战单位[n].变身技能})
       end
     --加载幻化属性
      for i=1,#幻化属性值 do
        -- if self.参战单位[n][幻化属性值[i]]==nil then
        --  self.参战单位[n][幻化属性值[i]]=0
        --  end
         if 玩家数据[self.参战单位[n].玩家id].角色.数据.幻化属性[幻化属性值[i]]==nil then
         玩家数据[self.参战单位[n].玩家id].角色.数据.幻化属性[幻化属性值[i]]=0
         end
         self.参战单位[n][幻化属性值[i]]=self.参战单位[n][幻化属性值[i]]+ 玩家数据[self.参战单位[n].玩家id].角色.数据.幻化属性[幻化属性值[i]]
       end
     self.参战单位[n].特技数据=玩家数据[self.参战单位[n].玩家id].角色.数据.特技数据
     if 玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]~=nil and 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]]~=nil and 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]]~=0  then
       self.参战单位[n].武器伤害=玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]].伤害
       if 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]].锻造数据[1] ~= nil then
        self.参战单位[n].武器宝石 = 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[21]].锻造数据.等级
       end
      --  if self.参战单位[n].武器伤害==nil then
      --    self.参战单位[n].武器伤害=0
      --    end
       end

    if self.参战单位[n].门派技能~=nil then
        for i=1,#self.参战单位[n].门派技能 do
            -- __S服务:输出(self.参战单位[n].门派技能[i].名称)
            if self.参战单位[n].门派技能[i].等级>=游戏技能[self.参战单位[n].门派技能[i].名称].等级 then
            self.参战单位[n].主动技能[#self.参战单位[n].主动技能+1]={名称=self.参战单位[n].门派技能[i].名称,等级=self.参战单位[n].门派技能[i].等级}
            end
        end
    end
    if self.参战单位[n].战斗类型 == "角色" then
      for i = 35,38 do
        self.临时格子 = nil
        if self.参战单位[n].法宝数据 ~=nil and self.参战单位[n].法宝数据[i] ~= nil  then

          self.临时格子 = self.参战单位[n].法宝数据[i]

          if 玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子]~=nil and 玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].类型 == "法宝" then

            if 玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 == nil then 玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 = 0 end
            if  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "碧玉葫芦" then
                self.参战单位[n].治疗能力 = self.参战单位[n].治疗能力 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *25 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "飞剑" then
              self.参战单位[n].命中 = self.参战单位[n].命中 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "风袋" then
              self.参战单位[n].速度 = self.参战单位[n].速度 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "拭剑石" then
              self.参战单位[n].伤害 = self.参战单位[n].伤害 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *30 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "五火神焰印" then
              self.参战单位[n].法暴 = self.参战单位[n].法暴 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *3 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "金刚杵" then
              self.参战单位[n].灵力 = self.参战单位[n].灵力 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *20 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "普渡" then
              self.参战单位[n].普渡 =  (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "九黎战鼓" then
              self.参战单位[n].九黎战鼓 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "盘龙壁" then
              self.参战单位[n].盘龙壁 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "神行飞剑" then
              self.参战单位[n].神行飞剑 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "汇灵盏" then
              self.参战单位[n].汇灵盏 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "织女扇" then
              self.参战单位[n].织女扇 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *4 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "金甲仙衣" then
              self.参战单位[n].金甲仙衣 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *4 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "五彩娃娃" then
              self.参战单位[n].五彩娃娃 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *5 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "嗜血幡" then
              self.参战单位[n].嗜血幡 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *6 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "兽王令" then
              self.参战单位[n].兽王令 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *4 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "附灵玉" then
              self.参战单位[n].灵力 = self.参战单位[n].灵力 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "落星飞鸿" then
              self.参战单位[n].伤害 = self.参战单位[n].伤害 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "流影云笛" then
              self.参战单位[n].法术伤害 = self.参战单位[n].法术伤害 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "月影" then
              self.参战单位[n].法连 = self.参战单位[n].法连 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1)*2 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "斩魔" then
              self.参战单位[n].伤害 = self.参战单位[n].伤害 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *7 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "千斗金樽" then
              self.参战单位[n].必杀 = self.参战单位[n].必杀 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1)*2 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "宿幕星河" then
              self.参战单位[n].法暴 = self.参战单位[n].法暴 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1)*2 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "蟠龙玉璧" then
              self.参战单位[n].蟠龙玉璧 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) * 5 / 2-- + 100
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "凌波仙符" then
              self.参战单位[n].速度 = self.参战单位[n].速度 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) * 20 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "河图洛书" then
              self.参战单位[n].织女扇 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *8 / 2
            elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "归元圣印" then
                self.参战单位[n].治疗能力 = self.参战单位[n].治疗能力 + (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) * 35 / 2
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "救命毫毛" and self.参战单位[n].门派 == "方寸山" then
              if 取随机数() <= (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *3 then -- +100
                self.参战单位[n].复活 = self.参战单位[n].复活 + 3
              end
            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "宝烛" and self.参战单位[n].门派 == "无底洞" then
                self.参战单位[n].法术状态组.宝烛.有无 = true
                self.参战单位[n].法术状态组.宝烛.回合 = 999999
                self.参战单位[n].法术状态组.宝烛.伤害 = (玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1) *10

            elseif  玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].名称 == "镇海珠" and self.参战单位[n].门派 == "龙宫" then
              self.参战单位[n].灵力 = self.参战单位[n].灵力 +  self.参战单位[n].灵力* ((玩家数据[self.参战单位[n].玩家id].道具.数据[self.临时格子].层数 +1)/100)
            end
          end
        end
      end
    end
    for i=21,26 do
          if 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]]~=nil and 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]]~=0 then
            if 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石~= nil then
              if 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合~=nil then
                local 临时等级= 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.等级
                if 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="无心插柳" then
                    if 临时等级 == 1 then
                      self.参战单位[n].溅射=0.15
                    elseif  临时等级== 2 then
                      self.参战单位[n].溅射=0.2
                    elseif  临时等级== 3 then
                      self.参战单位[n].溅射=0.25
                    elseif  临时等级==4 then
                      self.参战单位[n].溅射=0.3
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="百步穿杨" then
                    if 临时等级 == 1 then
                      self.参战单位[n].百步穿杨=200 / 4
                    elseif  临时等级== 2 then
                      self.参战单位[n].百步穿杨=450 / 4
                    elseif  临时等级== 3 then
                      self.参战单位[n].百步穿杨=600 / 4
                    elseif  临时等级==4 then
                      self.参战单位[n].百步穿杨=800 / 4
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="隔山打牛" then
                    if 临时等级 == 1 then
                      self.参战单位[n].隔山打牛=80 / 4
                    elseif  临时等级== 2 then
                      self.参战单位[n].隔山打牛=120 / 4
                    elseif  临时等级== 3 then
                      self.参战单位[n].隔山打牛=170 / 4
                    elseif  临时等级==4 then
                      self.参战单位[n].隔山打牛=200 / 4
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="心随我动" then
                    if 临时等级 == 1 then
                      self.参战单位[n].心随我动=250 / 4
                    elseif  临时等级== 2 then
                      self.参战单位[n].心随我动=400 / 4
                    elseif  临时等级== 3 then
                      self.参战单位[n].心随我动=700 / 4
                    elseif  临时等级==4 then
                      self.参战单位[n].心随我动=900 / 4
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="云随风舞" then
                    if 临时等级 == 1 then
                      self.参战单位[n].云随风舞=200 / 4
                    elseif  临时等级== 2 then
                      self.参战单位[n].云随风舞=400 / 4
                    elseif  临时等级== 3 then
                      self.参战单位[n].云随风舞=700 / 4
                    elseif  临时等级==4 then
                      self.参战单位[n].云随风舞=800 / 4
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="网罗乾坤" then
                    if 临时等级 == 1 then
                      self.参战单位[n].网罗乾坤=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].网罗乾坤=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].网罗乾坤=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="石破天惊" then
                    if 临时等级 == 1 then
                      self.参战单位[n].石破天惊=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].石破天惊=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].石破天惊=self.参战单位[n].等级
                    end
                 elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="天雷地火" then
                    if 临时等级 == 1 then
                      self.参战单位[n].天雷地火=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].天雷地火=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].天雷地火=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="烟雨飘摇" then
                    if 临时等级 == 1 then
                      self.参战单位[n].烟雨飘摇=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].烟雨飘摇=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].烟雨飘摇=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="索命无常" then
                    if 临时等级 == 1 then
                      self.参战单位[n].索命无常=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].索命无常=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].索命无常=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="行云流水" then
                    if 临时等级 == 1 then
                      self.参战单位[n].行云流水=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].行云流水=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].行云流水=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="福泽天下" then
                    if 临时等级 == 1 then
                      self.参战单位[n].福泽天下=math.floor(self.参战单位[n].等级/2)
                    elseif  临时等级== 2 then
                      self.参战单位[n].福泽天下=math.floor(self.参战单位[n].等级/1.5)
                    elseif  临时等级== 3 then
                      self.参战单位[n].福泽天下=self.参战单位[n].等级
                    end
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="暗度陈仓" then
                           self.参战单位[n].物伤减免 = self.参战单位[n].物伤减免-0.03
                elseif 玩家数据[self.参战单位[n].玩家id].道具.数据[玩家数据[self.参战单位[n].玩家id].角色.数据.装备数据[i]].符石.组合.名称=="化敌为友" then
                                   self.参战单位[n].法伤减免=self.参战单位[n].法伤减免-0.03
                end
              end
            end
          end
        end
        if self.参战单位[n].经脉有无 ~= nil and self.参战单位[n].经脉有无 then
          self:经脉属性处理(self.参战单位[n])
        end
     end
    if self.参战单位[n].战斗类型 == "bb" then
          if self.参战单位[self.参战单位[n].父编号]~= nil then
            if self.参战单位[self.参战单位[n].父编号].九黎战鼓 ~=nil then
              self.参战单位[n].伤害 = self.参战单位[n].伤害 + self.参战单位[self.参战单位[n].父编号].九黎战鼓
            end
            if self.参战单位[self.参战单位[n].父编号].盘龙壁 ~=nil then
              self.参战单位[n].防御 = self.参战单位[n].防御 + self.参战单位[self.参战单位[n].父编号].盘龙壁
            end
            if self.参战单位[self.参战单位[n].父编号].神行飞剑 ~=nil then
              self.参战单位[n].速度 = self.参战单位[n].速度 + self.参战单位[self.参战单位[n].父编号].神行飞剑
            end
            if self.参战单位[self.参战单位[n].父编号].汇灵盏 ~=nil then
              self.参战单位[n].灵力 = self.参战单位[n].灵力 + self.参战单位[self.参战单位[n].父编号].汇灵盏
            end
            if self.参战单位[self.参战单位[n].父编号].兽王令 ~=nil then
              self.参战单位[n].伤害 = self.参战单位[n].伤害 + self.参战单位[self.参战单位[n].父编号].兽王令
            end
          end
        end
   if self.参战单位[n].队伍~=0 and 昼夜参数==1 and self.参战单位[n].夜战==false and self:取经脉(self.参战单位[n],"夜行") == false then
      self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害*0.85)
      self.参战单位[n].防御=math.floor(self.参战单位[n].防御*0.85)
      self.参战单位[n].法防=math.floor(self.参战单位[n].法防*0.85)--+self.参战单位[n].法术防御
     end
   if  昼夜参数==1 and self:取经脉(self.参战单位[n],"夜之王者") == false then
      self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害*1.2)
      self.参战单位[n].防御=math.floor(self.参战单位[n].防御*1.2)
      self.参战单位[n].法防=math.floor(self.参战单位[n].法防*1.2)--+self.参战单位[n].法术防御
     end

   end
end
function 战斗处理类:召唤流程计算(编号)
 if self.参战单位[编号].参战召唤兽==nil then return 0 end
 self.召唤id=self.参战单位[编号].玩家id

 self.寻找id=0

 for n=1,#self.参战单位 do

   if self.参战单位[n].玩家id==self.召唤id and self.参战单位[n].战斗类型~="角色" then

     self.寻找id=n
     end
   end
 for n=1,#self.参战单位[编号].参战召唤兽 do

   if  self.参战单位[编号].参战召唤兽[n]==self.参战单位[编号].命令数据.目标 then
      self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽不能重复出战")
       return 0
      end

   end
 if 玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].忠诚<=60 then
   self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因忠诚度过低而不愿参战")
   return 0
 elseif 玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].寿命<=50 then
   self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因寿命过低而不愿参战")
   return 0
  elseif 玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].禁止使用 then
   self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽已被禁止使用")
   return 0
  elseif 玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].等级>玩家数据[self.召唤id].角色.数据.等级+10 then

   self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因等级高于角色10级无法参战")
   return 0
  -- elseif 玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].等级>玩家数据[self.召唤id].角色.数据.等级+10 then

  --  self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因等级高于角色10级无法参战")
  --  return 0
  -- elseif 召唤兽[玩家数据[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].造型].参战等级>玩家数据[self.召唤id].角色.数据.等级+10 then

  --  self:添加提示(self.参战单位[编号].数字id,"您的角色当前无法驾驭这样的召唤兽")
  --  return 0

 end
 if self.寻找id==0 then
   self.创建id=#self.参战单位+1
     self:设置队伍区分(self.召唤id)
  else

   self.创建id=self.寻找id
   end
 self.参战单位[self.创建id]={}
 玩家数据[self.召唤id].召唤兽.数据.参战=self.参战单位[编号].命令数据.目标
 self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
  self.参战单位[self.创建id]=table.loadstring(玩家数据[self.召唤id].召唤兽:获取指定数据(玩家数据[self.召唤id].召唤兽.数据.参战))
  玩家数据[self.召唤id].召唤兽.数据[玩家数据[self.召唤id].召唤兽.数据.参战].忠诚=玩家数据[self.召唤id].召唤兽.数据[玩家数据[self.召唤id].召唤兽.数据.参战].忠诚-0.1
  玩家数据[self.召唤id].召唤兽.数据[玩家数据[self.召唤id].召唤兽.数据.参战].寿命=玩家数据[self.召唤id].召唤兽.数据[玩家数据[self.召唤id].召唤兽.数据.参战].寿命-0.5
  self.参战单位[self.创建id].位置=self.参战单位[编号].位置+5
  self.参战单位[self.创建id].队伍id=self.参战单位[编号].队伍id
  self.参战单位[self.创建id].数字id=self.参战单位[编号].数字id
  self.参战单位[self.创建id].玩家id=self.参战单位[编号].玩家id
  self.参战单位[self.创建id].编号=self.创建id
  self.参战单位[self.创建id].父编号 = 编号
  self.参战单位[编号].参战召唤兽[#self.参战单位[编号].参战召唤兽+1]=self.参战单位[编号].命令数据.目标
    if self.参战单位[self.创建id].当前气血<=0 then
      self.参战单位[self.创建id].当前气血=1
    end


    local 个体 = self.参战单位[self.创建id]
    self:resetFight(个体)
    if 个体.主动技能 == nil then 个体.主动技能 = {} end
    if 个体.特技数据 ==nil then 个体.特技数据={} end
    
   self.参战单位[self.创建id].命中=self.参战单位[self.创建id].伤害

  -- self.参战单位[self.创建id].必杀=5
  --   self.参战单位[self.创建id].百步穿杨=0
  -- self.参战单位[self.创建id].福泽天下=0
  -- self.参战单位[self.创建id].行云流水=0
  -- self.参战单位[self.创建id].索命无常=0
  --  self.参战单位[self.创建id].烟雨飘摇=0
  --  self.参战单位[self.创建id].天雷地火=0
  --  self.参战单位[self.创建id].石破天惊=0
  --  self.参战单位[self.创建id].网罗乾坤=0
  --  self.参战单位[self.创建id].云随风舞=0
  --  self.参战单位[self.创建id].心随我动=0
  --  self.参战单位[self.创建id].隔山打牛=0
  --  self.参战单位[self.创建id].灵刃=false
  --  self.参战单位[self.创建id].预知=false
  --  self.参战单位[self.创建id].灵动=false
  --  self.参战单位[self.创建id].瞬击=false
  --  self.参战单位[self.创建id].抗法=false
  --  self.参战单位[self.创建id].阳护=false
  --  self.参战单位[self.创建id].识物=false
  --  self.参战单位[self.创建id].洞察=false
  --  self.参战单位[self.创建id].溅射=0
  --  self.参战单位[self.创建id].弑神=false
  --  self.参战单位[self.创建id].御风=false
  --  self.参战单位[self.创建id].顺势=false
  --  self.参战单位[self.创建id].复仇=false
  --  self.参战单位[self.创建id].自恋=false
  --  self.参战单位[self.创建id].怒吼=false
  --  self.参战单位[self.创建id].暗劲=false
  --  self.参战单位[self.创建id].逆境=false
  --  self.参战单位[self.创建id].瞬法=false
  -- self.参战单位[self.创建id].灵法=false
  -- self.参战单位[self.创建id].灵断=false
  -- self.参战单位[self.创建id].吮吸=false
  -- self.参战单位[self.创建id].识药=0
  -- self.参战单位[self.创建id].护佑=false
  -- self.参战单位[self.创建id].抗物=false
  --  self.参战单位[self.创建id].吸血=0
  --  self.参战单位[self.创建id].狂怒=0
  --  self.参战单位[self.创建id].阴伤=0
  --  self.参战单位[self.创建id].驱散=0  ---小法  内丹
  --  self.参战单位[self.创建id].连击=0
  --  self.参战单位[self.创建id].防御=0
  --  self.参战单位[self.创建id].法术暴击伤害= 0
  --  self.参战单位[self.创建id].额外法术伤害=0
  --  self.参战单位[self.创建id].理直气壮=false
  --  self.参战单位[self.创建id].反震=0.25
  --  self.参战单位[self.创建id].反击=0
  --  self.参战单位[self.创建id].法暴=0
  --  self.参战单位[self.创建id].法连=0
  --  self.参战单位[self.创建id].追击=false
  --  self.参战单位[self.创建id].怒击=false
  --  self.参战单位[self.创建id].法波=0
  --  self.参战单位[self.创建id].魔心=1
  --  self.参战单位[self.创建id].幸运=false
  --  self.参战单位[self.创建id].神佑=0
  --  self.参战单位[self.创建id].复活=0
  --  self.参战单位[self.创建id].冥想=0
  --  self.参战单位[self.创建id].慧根=1
  --  self.参战单位[self.创建id].再生=0
  --  self.参战单位[self.创建id].毒=0
  --  self.参战单位[self.创建id].驱鬼=0
  --  self.参战单位[self.创建id].偷袭=false
  --  self.参战单位[self.创建id].火吸=0
  --  self.参战单位[self.创建id].水吸=0
  --  self.参战单位[self.创建id].雷吸=0
  --  self.参战单位[self.创建id].土吸=0
  --  self.参战单位[self.创建id].夜战=false
  --  self.参战单位[self.创建id].感知=false
  --  self.参战单位[self.创建id].隐身=0
  --  self.参战单位[self.创建id].物伤减免=1
  --  self.参战单位[self.创建id].法伤减免=1
  --  self.参战单位[self.创建id].武器伤害=0
   self.参战单位[self.创建id].攻击封印=false
   self.参战单位[self.创建id].技能封印=false
   self.参战单位[self.创建id].特技封印=false
   self.参战单位[self.创建id].道具封印=false
   self.参战单位[self.创建id].全能封印=false
   self.参战单位[self.创建id].跳过行动=false --横扫千军状态
   self.参战单位[self.创建id].催眠封印=false --催眠符状态
   self.参战单位[self.创建id].封印状态=false
   self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
  --  self.参战单位[self.创建id].法术状态组={}
  --  for i=1,#法术状态名称 do
  --    self.参战单位[self.创建id].法术状态组[法术状态名称[i]]={有无=false}
  --    end
  --  for i=1,#幻化属性值 do
  --         if  self.参战单位[self.创建id][幻化属性值[i]]==nil then
  --        self.参战单位[self.创建id][幻化属性值[i]]=0
  --        end
  --      end
   self.参战单位[self.创建id].主动技能={}
   self:添加技能属性(self.创建id,self.参战单位[self.创建id].技能)
       self:添加内丹属性(self.创建id,self.参战单位[self.创建id].内丹)
           self:添加特性属性(self.创建id,self.参战单位[self.创建id].特性,self.参战单位[self.创建id].特性几率)
     self:添加进场特性属性(self.创建id,self.参战单位[self.创建id].特性,self.参战单位[self.创建id].特性几率)
        self.战斗流程[#self.战斗流程+1]={流程=1,进行=false,id=self.创建id,名称=self.参战单位[self.创建id].名称,数据=table.copy(self.参战单位[self.创建id]),类型="召唤"}

    for i=134,141 do
     if self.参战单位[self.创建id].法术状态组[法术状态名称[i]].有无==true then
      self.战斗流程[#self.战斗流程+1]={流程=108,执行=false,攻击方=self.创建id,结束=false,类型="技能",参数=法术状态名称[i]}

     end
     end




   self.等待结束=self.等待结束+2
   self.参战单位[self.创建id].法防=math.floor(self.参战单位[self.创建id].魔力*0.5)
   self.参战单位[self.创建id].伤害=math.floor(self.参战单位[self.创建id].伤害*1.1)

    self.参战单位[self.创建id].修炼数据={}
    for n=1,#全局变量.修炼名称 do

      self.参战单位[self.创建id].修炼数据[全局变量.修炼名称[n]]=玩家数据[self.召唤id].角色.数据.召唤兽修炼[全局变量.修炼名称[n]].等级
      end
    if self.参战单位[self.创建id].隐身~=nil and self.参战单位[self.创建id].隐身~=0 then
       self:隐身处理(self.创建id)
     end
    if self.参战单位[self.创建id].法术状态组["瞬法"].有无 and self.参战单位[self.创建id].法术状态组["瞬法"].技能~= 0 then
     self:瞬法处理(self.创建id)
      elseif self.参战单位[self.创建id].法术状态组["瞬击"].有无 then
     self:瞬击处理(self.创建id)
     end

  发送数据(玩家数据[self.召唤id].连接id,2005,玩家数据[self.召唤id].召唤兽:取头像数据())
 end
function 战斗处理类:添加进场特性属性(编号,特性,几率)
  if 特性 =="灵法" then
    local 灵法概率 = 0
    if 几率 == 1 then
         self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*33/100)
       self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*10/100)
       灵法概率=33
    elseif 几率 == 2 then
      self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*50/100)
       self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
        灵法概率=50
    elseif 几率 == 3 then
      self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*66/100)
       self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
        灵法概率=66
    elseif 几率 == 4 then
      self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*83/100)
       self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
        灵法概率=83
    elseif 几率 == 5 then
      self.参战单位[编号].法术状态组["灵法"].灵力 =self.参战单位[编号].灵力
       self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
        灵法概率=100
    end
       if 灵法概率~= 0 and 灵法概率 >= 取随机数() then
       self.参战单位[编号].法术状态组["灵法"].有无=true
       self.参战单位[编号].法术状态组["灵法"].回合=4
       self.参战单位[编号].灵力 =self.参战单位[编号].灵力+self.参战单位[编号].法术状态组["灵法"].灵力 / 10
       self.参战单位[编号].防御 =self.参战单位[编号].防御-self.参战单位[编号].法术状态组["灵法"].防御
       self.参战单位[编号].法术状态组["灵法"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
       self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵法"].当前气血

    end


  elseif 特性 =="灵断" then

    local 灵断概率 = 0
    if 几率 == 1 then

       灵断概率=33
    elseif 几率 == 2 then

        灵断概率=50
    elseif 几率 == 3 then

        灵断概率=66
    elseif 几率 == 4 then

        灵断概率=83
    elseif 几率 == 5 then

        灵断概率=100
    end
       if 灵断概率~= 0 and 灵断概率 >= 取随机数()  then
         self.参战单位[编号].法术状态组["灵断"].灵力 = math.floor(self.参战单位[编号].灵力*10/100) --55
       self.参战单位[编号].法术状态组["灵断"].有无=true
       self.参战单位[编号].法术状态组["灵断"].回合=4
    self.参战单位[编号].灵力 =self.参战单位[编号].灵力+self.参战单位[编号].法术状态组["灵断"].灵力
          self.参战单位[编号].法术状态组["灵断"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
       self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵断"].当前气血


    end


  elseif 特性 =="怒吼" then
   self.参战单位[编号].法术状态组["怒吼"].有无=true
       self.参战单位[编号].法术状态组["怒吼"].回合=4

    if 几率 == 1 then
      self.参战单位[编号].法术状态组["怒吼"].伤害 =50
       self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*6/100)
    elseif 几率 == 2 then
      self.参战单位[编号].法术状态组["怒吼"].伤害 =71
       self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*6/100)
    elseif 几率 == 3 then
       self.参战单位[编号].法术状态组["怒吼"].伤害 =109
       self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
    elseif 几率 == 4 then
      self.参战单位[编号].法术状态组["怒吼"].伤害 =160
       self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
    elseif 几率 == 5 then
         self.参战单位[编号].法术状态组["怒吼"].伤害 =180
       self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
    end
       self.参战单位[编号].灵力 = self.参战单位[编号].灵力- self.参战单位[编号].法术状态组["怒吼"].灵力
       self.参战单位[编号].伤害 =self.参战单位[编号].伤害+ self.参战单位[编号].法术状态组["怒吼"].伤害 / 2

  elseif 特性 =="御风" then
   self.参战单位[编号].法术状态组["御风"].有无=true
          self.参战单位[编号].法术状态组["御风"].回合=4

   if 几率 == 1 then
      self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*6/100)
       self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*20/100)
    elseif 几率 == 2 then
      self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*10/100)
       self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*20/100)
    elseif 几率 == 3 then
      self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*14/100)
       self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 4 then
          self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*17/100)
       self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 5 then
         self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*21/100)
       self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
    end
           self.参战单位[编号].防御 = self.参战单位[编号].防御- self.参战单位[编号].法术状态组["御风"].防御
       self.参战单位[编号].速度 =self.参战单位[编号].速度+ self.参战单位[编号].法术状态组["御风"].速度

  elseif 特性 =="灵刃" then
    self.参战单位[编号].法术状态组["灵刃"].有无=true
    self.参战单位[编号].法术状态组["灵刃"].回合=4

   if 几率 == 1 then
      self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*15/100)
       self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*10/100)
              self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 2 then
         self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*25/100)
       self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*10/100)
              self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 3 then
          self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*33/100)
       self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
              self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
    elseif 几率 == 4 then
           self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*43/100)
       self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
              self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
    elseif 几率 == 5 then
            self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*50/100)
       self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
              self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
    end
              self.参战单位[编号].法术状态组["灵刃"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
       self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵刃"].当前气血
          self.参战单位[编号].防御 = self.参战单位[编号].防御- self.参战单位[编号].法术状态组["灵刃"].防御
       self.参战单位[编号].灵力 =self.参战单位[编号].灵力- math.floor (self.参战单位[编号].法术状态组["灵刃"].灵力*0.5)
       self.参战单位[编号].伤害 =self.参战单位[编号].伤害+ self.参战单位[编号].法术状态组["灵刃"].伤害 / 5

  elseif 特性 =="瞬法" then

   if 几率 == 1 then
      self.参战单位[编号].法术状态组["瞬法"].几率 =33
    elseif 几率 == 2 then
      self.参战单位[编号].法术状态组["瞬法"].几率=50
    elseif 几率 == 3 then
       self.参战单位[编号].法术状态组["瞬法"].几率=66
    elseif 几率 == 4 then
      self.参战单位[编号].法术状态组["瞬法"].几率= 83
    elseif 几率 == 5 then
        self.参战单位[编号].法术状态组["瞬法"].几率= 100
    end

    for i=1,#self.参战单位[编号].主动技能  do
      if self.参战单位[编号].主动技能[i].名称 =="九天玄火" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能= "九天玄火"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 =="泰山压顶" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能= "泰山压顶"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 == "水漫金山" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="水漫金山"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 == "地狱烈火" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="地狱烈火"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 =="奔雷咒"  and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="奔雷咒"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
       elseif self.参战单位[编号].主动技能[i].名称 =="雷击" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="雷击"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 =="水攻" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="水攻"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 == "烈火" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="烈火"
          self.参战单位[编号].法术状态组["瞬法"].有无=true
      elseif self.参战单位[编号].主动技能[i].名称 == "落岩" and  取随机数() <= self.参战单位[编号].法术状态组["瞬法"].几率 then
        self.参战单位[编号].法术状态组["瞬法"].技能="落岩"
      end


   end
  elseif 特性 =="瞬击" or self.参战单位[编号].特征 == '精准扶贫' then

   if 几率 == 1 then
      self.参战单位[编号].法术状态组["瞬击"].几率 =33
    elseif 几率 == 2 then
      self.参战单位[编号].法术状态组["瞬击"].几率=50
    elseif 几率 == 3 then
       self.参战单位[编号].法术状态组["瞬击"].几率=66
    elseif 几率 == 4 then
      self.参战单位[编号].法术状态组["瞬击"].几率= 83
    elseif 几率 == 5 then
        self.参战单位[编号].法术状态组["瞬击"].几率= 100
    end


      if  取随机数() <= self.参战单位[编号].法术状态组["瞬击"].几率 then
          self.参战单位[编号].法术状态组["瞬击"].有无=true
      end




  end
end
function 战斗处理类:添加特性属性(编号,特性,几率)
  if 特性 =="力破" then
    if 几率 == 1 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +80 / 2
    elseif 几率 == 2 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +120 / 2
    elseif 几率 == 3 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +160 / 2
    elseif 几率 == 4 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +200 / 2
    elseif 几率 == 5 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +240 / 2
    end
  elseif 特性 =="巧劲" then
    if 几率 == 1 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +2
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*23/100)
    elseif 几率 == 2 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +5
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*20/100)
    elseif 几率 == 3 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +8
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*17/100)
    elseif 几率 == 4 then
      self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +12
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*15/100)
    elseif 几率 == 5 then
       self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +25
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*12/100)
    end
  elseif 特性 =="识药" then
    if 几率 == 1 then
      self.参战单位[编号].识药 = 1.2
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*20/100)
    elseif 几率 == 2 then
            self.参战单位[编号].识药 = 2.4
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 3 then
            self.参战单位[编号].识药 = 3.6
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 4 then
            self.参战单位[编号].识药 = 4.8
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
    elseif 几率 == 5 then
            self.参战单位[编号].识药 = 6.0
      self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
    end
  elseif 特性 =="抗物" then
   if 几率 == 1 then
      self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*15/100)
      self.参战单位[编号].防御 = self.参战单位[编号].防御 +6
    elseif 几率 == 2 then
            self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*18/100)
      self.参战单位[编号].防御 = self.参战单位[编号].防御 +9
    elseif 几率 == 3 then
            self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*22/100)
      self.参战单位[编号].防御 = self.参战单位[编号].防御 +14
    elseif 几率 == 4 then
            self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*26/100)
      self.参战单位[编号].防御 = self.参战单位[编号].防御 +23
    elseif 几率 == 5 then
          self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*30/100)
      self.参战单位[编号].防御 = self.参战单位[编号].防御 +52
    end
  elseif 特性 =="抗法" then
   if 几率 == 1 then
      self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*15/100)
      self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +6
    elseif 几率 == 2 then
            self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*18/100)
      self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +9
    elseif 几率 == 3 then
            self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*22/100)
      self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +14
    elseif 几率 == 4 then
            self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*26/100)
      self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +23
    elseif 几率 == 5 then
          self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*30/100)
      self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +52
    end
  end
end
function 战斗处理类:添加内丹属性(编号,内丹1)

  local 内丹={}
  for n=1,#内丹1 do
   内丹[n]=内丹1[n]
    end



  for i=1,#内丹 do
      local 等级 = 内丹[i].等级
      local 技能 = 内丹[i].技能
      if 技能 == "深思" then
        if self:重复技能判断(编号,"高级冥思") then
          self.参战单位[编号].冥想= 等级 *5 +self.参战单位[编号].冥想
        end
      end
      if 技能 == "淬毒" then
        if self:重复技能判断(编号,"毒") or self:重复技能判断(编号,"高级毒") then
          self.参战单位[编号].毒= math.floor(等级 *5 /100)+self.参战单位[编号].毒
        end
      end
      if 技能 == "连环" then
        if self:重复技能判断(编号,"连击") or self:重复技能判断(编号,"高级连击") then
          self.参战单位[编号].连击= math.floor(等级 *2 /100)+self.参战单位[编号].连击
        end
      end
      if 技能 == "灵光" then
          self.参战单位[编号].灵力= math.floor((等级 * 30 +self.参战单位[编号].魔力/80)*0.5)+self.参战单位[编号].灵力 --*0.7
      end
      if 技能 == "舍身击" then
          self.参战单位[编号].伤害= math.floor(等级*30+self.参战单位[编号].力量/80)+self.参战单位[编号].伤害
      end
      if 技能 == "圣洁" then
         if self:重复技能判断(编号,"驱鬼") or self:重复技能判断(编号,"高级驱鬼") then
          self.参战单位[编号].驱鬼= math.floor(等级 *10 /100)+self.参战单位[编号].驱鬼
        end
      end
      if 技能 == "狙刺" then
          self.参战单位[编号].灵力= math.floor((等级 *35 +self.参战单位[编号].等级/3)*0.4)+self.参战单位[编号].灵力 --*0.7
      end
      if 技能 == "狂怒" then
          self.参战单位[编号].狂怒= 等级*80 / 2 +self.参战单位[编号].狂怒
      end
      if 技能 == "阴伤" then
          self.参战单位[编号].阴伤= 等级*50 / 2 +self.参战单位[编号].阴伤
      end
      if 技能 == "擅咒" then
      self.参战单位[编号].额外法术伤害= self.参战单位[编号].额外法术伤害 + 等级*12
      end
      if 技能 == "钢化" then
         if  self:重复技能判断(编号,"高级防御") or  self:重复技能判断(编号,"防御") then
              self.参战单位[编号].防御=self.参战单位[编号].防御+等级*20
         end
      end
      if 技能 == "生死决" then
      self.参战单位[编号].伤害= self.参战单位[编号].伤害 + self.参战单位[编号].等级*等级 / 2
      self.参战单位[编号].防御= self.参战单位[编号].防御 - self.参战单位[编号].等级*等级 / 2
      end
      if 技能 == "碎甲刃" then
      self.参战单位[编号].伤害= self.参战单位[编号].伤害 + self.参战单位[编号].等级*等级 / 2 --(等级-1)+50
      end
      if 技能 == "撞击" then
      self.参战单位[编号].伤害= self.参战单位[编号].伤害 + 等级*40/2
      end
      if 技能 == "催心浪" then
         if self.参战单位[编号].法波 ~=0 then
      self.参战单位[编号].法波.下限=self.参战单位[编号].法波.下限 + 等级*2
    end
      end
      if 技能 == "通灵法" then
      self.参战单位[编号].额外法术伤害 =self.参战单位[编号].额外法术伤害+ math.floor(self.参战单位[编号].额外法术伤害 * (等级*2/100)) --4
      end
     if 技能 == "隐匿击" then
        if self:重复技能判断(编号,"隐身") then
           local unit = self.参战单位[编号]
           local tempvalue = unit.伤害*(等级/100)
           self.参战单位[编号].伤害 = self.参战单位[编号].伤害 + math.floor(tempvalue) --*2
        end
      end
      if 技能 == "灵身" then
          self.参战单位[编号].法术暴击伤害 = math.floor(self.参战单位[编号].法术暴击伤害+ self.参战单位[编号].灵力*等级*2/100) --/0.7*(等级*7/100)
      end
      if 技能 == "腾挪劲" then
          self.参战单位[编号].防御 = self.参战单位[编号].防御 + 等级*3
      end
      if 技能 == "电魂闪" then
          self.参战单位[编号].驱散 = 等级*8    ----小法
      end
      if 技能 == "坚甲" then
         if  self:重复技能判断(编号,"高级反震") or  self:重复技能判断(编号,"反震") then
              self.参战单位[编号].反震=self.参战单位[编号].反震+等级*2
         end
      end
      if 技能 == "双星爆" then
         if  self:重复技能判断(编号,"高级法术连击") or  self:重复技能判断(编号,"法术连击") then
              self.参战单位[编号].法连=self.参战单位[编号].法连+等级*2
              self.参战单位[编号].额外法术伤害 =self.参战单位[编号].额外法术伤害+ math.floor(self.参战单位[编号].额外法术伤害 * (等级*2/100))
         end
      end
   end
end
function 战斗处理类:添加技能属性(编号,技能组1)
  local 技能组={}
  for n=1,#技能组1 do

   技能组[n]=技能组1[n]

    end

  self.参战单位[编号].已加技能={}
  if self.参战单位[编号].造型=="齐天大圣" then

   技能组[#技能组+1]="大闹天宫"

  elseif self.参战单位[编号].造型=="猪八戒" then

   技能组[#技能组+1]="九天玄火"

  elseif self.参战单位[编号].造型=="超级神猴" then

   技能组[#技能组+1]="大闹天宫"
  elseif self.参战单位[编号].造型=="进阶猫灵" then

   技能组[#技能组+1]="九天玄火"
  end

  --table.print(self.参战单位[编号].技能)
  for n=1,#技能组 do
   if 技能组[n]=="九天玄火" or 技能组[n]=="大闹天宫" or 技能组[n]=="力劈华山" or 技能组[n]=="夜舞倾城" or 技能组[n]=="善恶有报" or 技能组[n]=="雷击" or 技能组[n]=="水攻" or 技能组[n]=="烈火"
    or 技能组[n]=="落岩" or 技能组[n]=="泰山压顶" or 技能组[n]=="水漫金山" or 技能组[n]=="地狱烈火" or 技能组[n]=="奔雷咒"  or 技能组[n]=="法术防御"or 技能组[n]=="剑荡四方" then

      self.参战单位[编号].主动技能[#self.参战单位[编号].主动技能+1]={名称=技能组[n],等级=self.参战单位[编号].等级}
   elseif 技能组[n]=="裂石" or 技能组[n]=="断岳势"  or 技能组[n]=="天崩地裂"   or 技能组[n]=="浪涌"  or 技能组[n]=="惊涛怒"  or 技能组[n]=="翻江搅海"   or 技能组[n]=="腾雷"   then

    if self.参战单位[编号].必杀 == nil then self.参战单位[编号].必杀 = 0 end
    if 取随机数(1,100)<=40 then
       self.参战单位[编号].必杀=self.参战单位[编号].必杀 + 10 --30
    end

   elseif 技能组[n]=="必杀" or 技能组[n]=="高级必杀"  then

      if 技能组[n]=="必杀" then

       if self:重复技能判断(编号,"高级必杀")==false then

          self.参战单位[编号].必杀=self.参战单位[编号].必杀 + 10
         end
     else
       self.参战单位[编号].必杀=self.参战单位[编号].必杀 + 20
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
   elseif 技能组[n]=="强力" or 技能组[n]=="高级强力"  then
      if 技能组[n]=="强力" then
       if self:重复技能判断(编号,"高级强力")==false then
          self.参战单位[编号].伤害=self.参战单位[编号].伤害+math.floor(self.参战单位[编号].等级*0.4)
         end
     else
       self.参战单位[编号].伤害=self.参战单位[编号].伤害+math.floor(self.参战单位[编号].等级*0.4)
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="须弥真言"  then
       if self:重复技能判断(编号,"须弥真言")==false then
          self.参战单位[编号].灵力=self.参战单位[编号].灵力+math.floor(self.参战单位[编号].魔力*0.4)
         end
    elseif 技能组[n]=="防御" or 技能组[n]=="高级防御"  then
      if 技能组[n]=="防御" then
       if self:重复技能判断(编号,"高级防御")==false then

          self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.5)
          self.参战单位[编号].灵力=self.参战单位[编号].灵力-math.floor(self.参战单位[编号].等级*0.1)
         end
     else
       self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.8)
        self.参战单位[编号].灵力=self.参战单位[编号].灵力-math.floor(self.参战单位[编号].等级*0.1)
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

  elseif 技能组[n]=="幸运" or 技能组[n]=="高级幸运"  then

      self.参战单位[编号].幸运=true

  elseif 技能组[n]=="偷袭" or 技能组[n]=="高级偷袭"  then
      self.参战单位[编号].偷袭=true
  elseif 技能组[n]=="感知" or 技能组[n]=="高级感知"  then
      self.参战单位[编号].感知=true
    elseif  技能组[n]=="高级神迹"  then
      self.参战单位[编号].神迹=2
   elseif 技能组[n]=="夜战" or 技能组[n]=="高级夜战"  then
      self.参战单位[编号].夜战=true
  elseif 技能组[n]=="连击" or 技能组[n]=="高级连击"  then
              self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.7)
      if 技能组[n]=="连击" then

       if self:重复技能判断(编号,"高级连击")==false then

          self.参战单位[编号].连击=45
          if self.参战单位[编号].连击减伤==nil then
          -- self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
          -- self.参战单位[编号].连击减伤=1
           end



         end
     else
       self.参战单位[编号].连击=55
       if self.参战单位[编号].连击减伤==nil then
          -- self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
          -- self.参战单位[编号].连击减伤=1
           end
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

  elseif 技能组[n]=="理直气壮"   then
 self.参战单位[编号].理直气壮=true
   self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif  技能组[n]=="嗜血追击"  then
     self.参战单位[编号].追击=true
   self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif  技能组[n]=="苍鸾怒击"  then
     self.参战单位[编号].怒击=true

   self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif 技能组[n]=="隐身" or 技能组[n]=="高级隐身"  then
      if 技能组[n]=="隐身" then
       if self:重复技能判断(编号,"高级隐身")==false then
          self.参战单位[编号].隐身=4
          self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.75)
         end
     else
       self.参战单位[编号].隐身=5
       self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.85)
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="敏捷" or 技能组[n]=="高级敏捷"  then
      if 技能组[n]=="敏捷" then
       if self:重复技能判断(编号,"高级敏捷")==false then
          self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].速度*0.2)
         end
     else
       self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].速度*0.1)
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif 技能组[n]=="吸血" or 技能组[n]=="高级吸血"  then
      if 技能组[n]=="吸血" then
       if self:重复技能判断(编号,"高级吸血")==false then
          self.参战单位[编号].吸血=0.15
         end
     else
       self.参战单位[编号].吸血=0.3
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="毒" or 技能组[n]=="高级毒"  then
      if 技能组[n]=="毒" then
       if self:重复技能判断(编号,"高级毒")==false then
          self.参战单位[编号].毒=10
         end
     else
       self.参战单位[编号].毒=20
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif 技能组[n]=="反震" or 技能组[n]=="高级反震"  then

      if 技能组[n]=="反震" then
       if self:重复技能判断(编号,"高级反震")==false then
          self.参战单位[编号].反震=25
         end

     else
       self.参战单位[编号].反震=30
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
   elseif 技能组[n]=="反击" or 技能组[n]=="高级反击"  then

      if 技能组[n]=="反击" then

       if self:重复技能判断(编号,"高级反击")==false then

          self.参战单位[编号].反击=0
         end
     else
       self.参战单位[编号].反击=0
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]


  elseif 技能组[n]=="神佑复生" or 技能组[n]=="高级神佑复生"  then

      if 技能组[n]=="神佑复生" then

       if self:重复技能判断(编号,"高级神佑复生")==false then
          self.参战单位[编号].神佑=15
         end
     else
       self.参战单位[编号].神佑=25
        end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="鬼魂术" or 技能组[n]=="高级鬼魂术"  then

      if 技能组[n]=="鬼魂术" then
         if self:重复技能判断(编号,"高级鬼魂术")==false then
          self.参战单位[编号].复活=7
         end
     else
       self.参战单位[编号].复活=5
     end
       self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
  elseif 技能组[n]=="驱鬼" or 技能组[n]=="高级驱鬼"  then
      if 技能组[n]=="驱鬼" then
       if self:重复技能判断(编号,"高级驱鬼")==false then
          self.参战单位[编号].驱鬼=2
         end
     else
       self.参战单位[编号].驱鬼=1.5
        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]


  elseif 技能组[n]=="再生" or 技能组[n]=="高级再生"  then

      if 技能组[n]=="再生" then

       if self:重复技能判断(编号,"高级再生")==false then

          self.参战单位[编号].再生=math.floor(self.参战单位[编号].等级)



         end

     else
       self.参战单位[编号].再生=self.参战单位[编号].等级 * 2


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="冥思" or 技能组[n]=="高级冥思"  then

      if 技能组[n]=="冥思" then

       if self:重复技能判断(编号,"高级冥思")==false then

          self.参战单位[编号].冥想=math.floor(self.参战单位[编号].等级*0.25)



         end

     else
       self.参战单位[编号].冥想=math.floor(self.参战单位[编号].等级*0.35)


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]



   elseif 技能组[n]=="慧根" or 技能组[n]=="高级慧根"  then

      if 技能组[n]=="慧根" then

       if self:重复技能判断(编号,"高级慧根")==false then

          self.参战单位[编号].慧根=0.75



         end

     else
       self.参战单位[编号].慧根=0.5


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

   elseif 技能组[n]=="法术暴击" or 技能组[n]=="高级法术暴击"  then

      if 技能组[n]=="法术暴击" then

       if self:重复技能判断(编号,"高级法术暴击")==false then

          self.参战单位[编号].法暴=10
         end

     else
       self.参战单位[编号].法暴=15


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

     elseif 技能组[n]=="法术连击" or 技能组[n]=="高级法术连击"  then

      if 技能组[n]=="法术连击" then

       if self:重复技能判断(编号,"高级法术连击")==false then

          self.参战单位[编号].法连=10



         end

     else
       self.参战单位[编号].法连=20


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

    elseif 技能组[n]=="法术波动" or 技能组[n]=="高级法术波动"  then

      if 技能组[n]=="法术波动" then

       if self:重复技能判断(编号,"高级法术波动")==false then

          self.参战单位[编号].法波={下限=90,上限=115}



         end

     else
       self.参战单位[编号].法波={下限=90,上限=120}


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]

    elseif 技能组[n]=="魔之心" or 技能组[n]=="高级魔之心"  then

      if 技能组[n]=="魔之心" then

       if self:重复技能判断(编号,"高级魔之心")==false then

          self.参战单位[编号].魔心=1.1



         end

     else
       self.参战单位[编号].魔心=1.2


        end
      self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
     end
    end





 end
function 战斗处理类:重复技能判断(编号,名称)

  for n=1,#self.参战单位[编号].已加技能 do

    if self.参战单位[编号].已加技能[n]==名称 then


     return true

      end



    end

 return false

 end

function 战斗处理类:取经脉(玩家,名称)
  if 玩家.经脉数据 ~= nil and 玩家.经脉有无 ~= nil and 玩家.经脉有无 then
    for i,v in pairs(玩家.经脉数据) do
      if i == 名称 and v == 1 then
        return true
      end
    end
  end
  return false
end

function 战斗处理类:经脉回合开始处理(玩家)
  if 玩家.经脉数据 ~= nil then
    if self:取经脉(玩家,"花舞") then
      玩家.速度 = 玩家.速度 + 玩家.速度*0.06
      if 玩家.速度 >= 18888 then
        玩家.速度 = 18888
      end
    end


    if self:取经脉(玩家,"意境") and 玩家.愤怒 ~= nil then
      玩家.愤怒 = 玩家.愤怒 + 1
      玩家.当前魔法 =玩家.当前魔法 +24

      if 玩家.愤怒 >= 150 then
        玩家.速度 = 150
      end
      if 玩家.当前魔法 >= 玩家.魔法上限 then
        玩家.当前魔法 = 玩家.魔法上限
      end
    end

    if self:取经脉(玩家,"回魔") and (玩家.魔法上限-玩家.灵力)>0 then
      玩家.当前魔法 = 玩家.当前魔法 + (玩家.魔法上限-玩家.灵力)*0.05
      if 玩家.当前魔法 >= 玩家.魔法上限 then
        玩家.当前魔法 = 玩家.魔法上限
      end
    end
    if self:取经脉(玩家,"心浪") and 玩家.愤怒 ~= nil and 玩家.愤怒 <= 50 then
      玩家.愤怒 = 玩家.愤怒 + 取随机数(1,15)
    end

    if self:取经脉(玩家,"暗潮") and 玩家.愤怒 <=90 then
      玩家.愤怒 = 玩家.愤怒 + 4
    end

    if self:取经脉(玩家,"突进") then
      if 玩家.突进 ~= nil then
        玩家.伤害 = 玩家.伤害 - 玩家.突进
        玩家.突进 = nil
      end
      if 玩家.突进 == nil and 取随机数() <= 40 then
        玩家.突进 = 玩家.防御*0.08
        玩家.伤害 = 玩家.伤害 + 玩家.突进
      end
    end
    if self:取经脉(玩家,"亢龙归海") then
      if 玩家.亢龙归海 == nil or 玩家.亢龙归海 < 25 then
        if 玩家.亢龙归海  == nil then
          玩家.亢龙归海  = 0
        end
        玩家.灵力 = 玩家.灵力*1.05
        玩家.亢龙归海 = 玩家.亢龙归海  +5
      end
    end
    if self:取经脉(玩家,"爪印") then
      if 玩家.爪印 == nil or 玩家.爪印 < 500 then
        if 玩家.爪印  == nil then
          玩家.爪印  = 0
        end
        玩家.伤害 = 玩家.伤害+20
        玩家.爪印 = 玩家.爪印  +20
      end
    end
    if self:取经脉(玩家,"汲魂") then
      if 玩家.汲魂 == nil or 玩家.汲魂 < 500 then
        if 玩家.汲魂  == nil then
          玩家.汲魂  = 0
        end
        玩家.伤害 = 玩家.伤害+20
        玩家.汲魂 = 玩家.汲魂  +20
      end
    end

    if self:取经脉(玩家,"燃魂") then
      if 玩家.燃魂 == nil then
          玩家.额外法术伤害 = 玩家.额外法术伤害 + 玩家.当前魔法/玩家.魔法上限*0.5
          玩家.燃魂 = 玩家.当前魔法/玩家.魔法上限*0.5
      else
          玩家.额外法术伤害 = 玩家.额外法术伤害 - 玩家.燃魂
          玩家.额外法术伤害 = 玩家.额外法术伤害 + 玩家.当前魔法/玩家.魔法上限*0.5
          玩家.燃魂 = 玩家.当前魔法/玩家.魔法上限*0.5
      end
    end
    if self:取经脉(玩家,"补缺") and 取随机数() <=35 and 玩家.当前魔法 <= 玩家.魔法上限*0.3 then
      玩家.当前魔法 = 玩家.当前魔法 + 玩家.魔法上限*0.1
    end

    if self:取经脉(玩家,"调息") and 玩家.法术状态组.分身术 then
      玩家.当前魔法 = 玩家.当前魔法 + 玩家.魔法上限*0.03
      if 玩家.当前魔法 >= 玩家.魔法上限 then
         玩家.当前魔法 = 玩家.魔法上限
      end
    end
  if self:取经脉(玩家,"狂月") then
      玩家.当前魔法 = 玩家.当前魔法 + 50
      if 玩家.当前魔法 >= 玩家.魔法上限 then
         玩家.当前魔法 = 玩家.魔法上限
      end
    end
  end
end

function 战斗处理类:经脉属性处理(玩家) --if self:取经脉(玩家,"") then
  if 玩家.经脉数据 ~= nil then
    -- if self:取经脉(玩家,"额外能力") then
    --     if 玩家.门派 == "大唐官府" or 玩家.门派 == "化生寺" or 玩家.门派 == "方寸山" or 玩家.门派 == "女儿村" or 玩家.门派 == "神木林" then
    --       玩家.伤害 = 玩家.伤害*1.08
    --       玩家.防御 = 玩家.防御*1.08
    --       玩家.灵力 = 玩家.灵力*1.08
    --       玩家.法防 = 玩家.法防*1.08
    --       玩家.速度 = 玩家.速度*1.08
    --     elseif 玩家.门派 == "狮驼岭" or 玩家.门派 == "魔王寨" or 玩家.门派 == "阴曹地府" or 玩家.门派 == "盘丝洞" or 玩家.门派 == "无底洞" then
    --       玩家.伤害 = 玩家.伤害*1.15
    --       玩家.防御 = 玩家.防御*1.15
    --       玩家.灵力 = 玩家.灵力*1.05
    --       玩家.法防 = 玩家.法防*1.15
    --       玩家.速度 = 玩家.速度*1.05
    --     else
    --       玩家.伤害 = 玩家.伤害*1.05
    --       玩家.防御 = 玩家.防御*1.15
    --       玩家.灵力 = 玩家.灵力*1.15
    --       玩家.法防 = 玩家.法防*1.15
    --       玩家.速度 = 玩家.速度*1.05
    --     end
    -- end
    if self:取经脉(玩家,"洞察") then
        玩家.伤害 = 玩家.伤害+玩家.等级/8
        玩家.防御 = 玩家.防御+玩家.等级/8
        玩家.灵力 = 玩家.灵力+玩家.等级/8
        玩家.法防 = 玩家.法防+玩家.等级/8
    end
    if self:取经脉(玩家,"修身") then
        玩家.伤害 = 玩家.伤害+14
        玩家.防御 = 玩家.防御+14
        玩家.灵力 = 玩家.灵力+14
        玩家.法防 = 玩家.法防+14
    end
    if self:取经脉(玩家,"聚魂") then
        玩家.伤害 = 玩家.伤害+14
        玩家.防御 = 玩家.防御+14
        玩家.灵力 = 玩家.灵力+14
        玩家.法防 = 玩家.法防+14
    end

    if self:取经脉(玩家,"充沛") then
        玩家.防御 = 玩家.防御+玩家.防御*0.1
        玩家.灵力 = 玩家.灵力+玩家.灵力*0.1
    end

    if self:取经脉(玩家,"灵光") and 玩家.武器宝石 ~= nil and 玩家.武器宝石 >0 then
      玩家.灵力 = 玩家.灵力+玩家.武器宝石*5
    end
    if self:取经脉(玩家,"狂狷") then
        玩家.伤害 = 玩家.伤害 + (玩家.力量/玩家.耐力)*16
    end
    if self:取经脉(玩家,"烈焰真诀") then
        玩家.法暴 = 玩家.法暴 + 10
    end
    if self:取经脉(玩家,"化戈") then
        玩家.治疗能力 = 玩家.治疗能力 +玩家.武器伤害*0.18
    end
    if self:取经脉(玩家,"锤炼") then
        玩家.伤害 = 玩家.伤害 +玩家.武器伤害*0.03
    end
    if self:取经脉(玩家,"借灵") then
        玩家.固定伤害 = 玩家.固定伤害 +玩家.武器伤害*0.24
    end
    if self:取经脉(玩家,"真君显灵") then
        玩家.伤害 =  玩家.伤害*1.1
        玩家.防御 =  玩家.防御*1.1
    end
    if self:取经脉(玩家,"驭意") then
        玩家.速度 = 玩家.速度 + 玩家.魔力*0.05
    end
    if self:取经脉(玩家,"神凝") then
        if 玩家.抵抗封印等级 == nil then
          玩家.抵抗封印等级 = 0
        end
        玩家.抵抗封印等级 = 玩家.抵抗封印等级 + 100
    end
    if self:取经脉(玩家,"破击") then
      玩家.必杀 = 玩家.必杀 + 20
    end
    if self:取经脉(玩家,"止戈") then
        玩家.治疗能力 = 玩家.治疗能力 + 玩家.武器伤害*0.18
    end
    if self:取经脉(玩家,"推衍") then
        玩家.固定伤害 = 玩家.固定伤害 + 玩家.敏捷
    end
    if self:取经脉(玩家,"暗伤") then
         if 玩家.固定伤害 == nil then
          玩家.固定伤害 = 0
        end
        玩家.固定伤害 = 玩家.固定伤害 + 玩家.武器伤害*0.18
    end
    if self:取经脉(玩家,"倩影") then
        玩家.速度 = 玩家.速度 + 50
    end
    if self:取经脉(玩家,"风魂") then
        玩家.防御 = 玩家.防御 *1.1
    end
    if self:取经脉(玩家,"念心") and self:取玩家战斗() == false then
       玩家.必杀 = 玩家.必杀 *1.1
    end
    if self:取经脉(玩家,"斗法") then
       玩家.封印命中等级 = 玩家.封印命中等级 + 100
    end
    if self:取经脉(玩家,"自矜") then
       玩家.封印命中等级 = 玩家.封印命中等级 + 100
    end
    if self:取经脉(玩家,"妖气") then
       玩家.封印命中等级 =玩家.封印命中等级 + 玩家.武器伤害*0.12
    end
    if self:取经脉(玩家,"心随意动") then
       玩家.封印命中等级 = 玩家.封印命中等级 + 100
       玩家.伤害 = 玩家.伤害 + 100
    end
    if self:取经脉(玩家,"神附") then
       玩家.伤害 = 玩家.伤害 + 玩家.力量*0.08
    end
    if self:取经脉(玩家,"海沸") then
       玩家.伤害 = 玩家.伤害 + 玩家.力量*0.08
    end
    if self:取经脉(玩家,"趁虚") then
       玩家.封印命中等级 = 玩家.封印命中等级 + 80
    end
    if self:取经脉(玩家,"月桂") then
       玩家.封印命中等级 = 玩家.封印命中等级 + 玩家.武器伤害*0.12
    end
    if self:取经脉(玩家,"顺势而为") then
       -- 玩家.封印必中 = true
    end
    if self:取经脉(玩家,"钟馗论道") then
       玩家.驱鬼 = 2
    end
    if self:取经脉(玩家,"碎玉弄影") then
       玩家.防御 = 玩家.防御*1.05
    end
    if self:取经脉(玩家,"风灵") and 玩家.门派 ~= nil and 玩家.门派 == "神木林" then
       玩家.风灵 = 10
    end
    if self:取经脉(玩家,"风魂") and 玩家.门派 ~= nil and 玩家.门派 == "神木林" then
       玩家.防御 = 玩家.防御 + 100
    end
    if self:取经脉(玩家,"磐石") then
       玩家.防御 = 玩家.防御+50
       玩家.法防 = 玩家.法防+50
    end
    if self:取经脉(玩家,"纯净") then
       玩家.法防 = 玩家.法防+玩家.等级
    end
    if self:取经脉(玩家,"咒法") then
       玩家.额外法术伤害 = 玩家.额外法术伤害 +60
    end
    if self:取经脉(玩家,"踏涛") then
       玩家.额外法术伤害 = 玩家.额外法术伤害 +60
    end
    if self:取经脉(玩家,"天龙") then
       玩家.额外法术伤害 = 玩家.额外法术伤害 +150
    end
    if self:取经脉(玩家,"震天") then
       玩家.额外法术伤害 = 玩家.额外法术伤害 +60
    end
    if self:取经脉(玩家,"护佑") then
       玩家.防御 = 玩家.防御+50
       玩家.法防 = 玩家.法防+50
    end
    if self:取经脉(玩家,"化血") then
       玩家.吸血=0.15
    end

  end
end


function 战斗处理类:加载单个玩家(id,位置)
 --加载单位

 self.参战玩家[#self.参战玩家+1]={玩家id=id,队伍id=self:取队伍id(id),加载=false,数字id=玩家数据[id].数字id,逃跑=false}
 self.参战单位[#self.参战单位+1]=玩家数据[id].角色:获取角色数据()
 self.参战单位[#self.参战单位].坐骑=nil
 self.参战单位[#self.参战单位].位置=位置
 --self.参战单位[#self.参战单位].法防=math.floor(self.参战单位[#self.参战单位].魔力*0.5+self.参战单位[#self.参战单位].灵力*0.3)
 self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
 self.参战单位[#self.参战单位].数字id=玩家数据[id].数字id
 self.参战单位[#self.参战单位].门派=玩家数据[id].角色.数据.门派
 self.参战单位[#self.参战单位].玩家id=id
 self.参战单位[#self.参战单位].付费=false
 self.参战单位[#self.参战单位].编号=#self.参战单位
 self.参战单位[#self.参战单位].战斗类型="角色"
 self.参战单位[#self.参战单位].蚩尤元神=玩家数据[id].角色.数据.蚩尤元神
 if self.参战单位[#self.参战单位].蚩尤元神==1 and self.参战单位[#self.参战单位].门派=="女儿村" and 取随机数()<=35 then
   self.参战单位[#self.参战单位].速度=99999
end
 self.参战单位[#self.参战单位].主动技能={}
 self.参战单位[#self.参战单位].追加技能=玩家数据[id].角色.数据.追加技能
 self.参战单位[#self.参战单位].附加技能=玩家数据[id].角色.数据.附加技能
 self.参战单位[#self.参战单位].特技数据=玩家数据[id].角色.数据.特技数据
 if 玩家数据[id].角色.数据.奇经八脉 ~= nil then
  self.参战单位[#self.参战单位].经脉数据=玩家数据[id].角色.数据.奇经八脉
 end
 if 玩家数据[id].角色.数据.开启奇经八脉 ~= nil then
  self.参战单位[#self.参战单位].经脉有无=玩家数据[id].角色.数据.开启奇经八脉
 end
 self.参战单位[#self.参战单位].修炼数据={}
 self.参战单位[#self.参战单位].参战召唤兽={}
 self.参战单位[#self.参战单位].变身技能=0

 if 玩家数据[id].角色.数据.变身.造型~="" and 玩家数据[id].角色.数据.变身.造型~=nil then
     self.参战单位[#self.参战单位].变身技能=变身卡数据[玩家数据[id].角色.数据.变身.造型].技能
   end
    for n=1,#全局变量.修炼名称 do
      self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=玩家数据[id].角色.数据.人物修炼[全局变量.修炼名称[n]].等级
      end
 self.参战单位[#self.参战单位].参战召唤兽={}
 self:设置队伍区分(id)
 self:增加阵法属性(self:取队伍id(id),位置,#self.参战单位)

 --加载召唤兽
 if 玩家数据[id].召唤兽.数据.参战==0 or 玩家数据[id].召唤兽.数据.参战==nil  then

  elseif  type(玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].当前气血)~= "number" then
    玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].当前气血=0
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽因气血异常而不愿意参战")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].当前气血<=0 then
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽因气血过低而不愿意参战")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].忠诚<=70 and 取随机数()<=50 then
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽因忠诚度过低而不愿意参战")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命<=50  then
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽因寿命过低而无法参战")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].等级 > 玩家数据[id].角色.数据.等级+10 then
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/您的这只召唤兽因等级高于角色10级无法参战")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].禁止使用  then
    玩家数据[id].召唤兽.数据.参战=0
    发送数据(玩家数据[id].连接id,2005,玩家数据[id].召唤兽:取头像数据())
    发送数据(玩家数据[id].连接id,7,"#y/你的召唤兽已经被禁止使用")
  elseif 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战]~=nil then
      if 玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].神兽~=true then
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命=玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].寿命-0.5
      玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].忠诚=玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].忠诚-0.1
      end
    self.参战单位[#self.参战单位].参战召唤兽[#self.参战单位[#self.参战单位].参战召唤兽+1]=玩家数据[id].召唤兽.数据.参战
    self.参战单位[#self.参战单位+1]=table.loadstring(玩家数据[id].召唤兽:获取指定数据(玩家数据[id].召唤兽.数据.参战))
    self.参战单位[#self.参战单位].位置=位置+5
    self.参战单位[#self.参战单位].命中=self.参战单位[#self.参战单位].伤害
    self.参战单位[#self.参战单位].法防=math.floor(self.参战单位[#self.参战单位].魔力*0.5+self.参战单位[#self.参战单位].灵力*0.3)
    self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
    self.参战单位[#self.参战单位].数字id=玩家数据[id].数字id
    self.参战单位[#self.参战单位].召唤兽id=玩家数据[id].召唤兽.数据.参战
    self.参战单位[#self.参战单位].玩家id=id
    self.参战单位[#self.参战单位].战斗类型="bb"
    self.参战单位[#self.参战单位].主动技能={}
    self.参战单位[#self.参战单位].父编号 = #self.参战单位 -1
    self.参战单位[#self.参战单位].技能=玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].技能
    self.参战单位[#self.参战单位].追加技能=玩家数据[id].召唤兽.数据[玩家数据[id].召唤兽.数据.参战].追加技能
    if self.参战单位[#self.参战单位].附加技能~=nil and #self.参战单位[#self.参战单位].附加技能>0 then
      self:添加技能属性(#self.参战单位,self.参战单位[#self.参战单位].附加技能)
    end

    self.参战单位[#self.参战单位].编号=#self.参战单位
    self.参战单位[#self.参战单位].伤害=math.floor(self.参战单位[#self.参战单位].伤害*1.1)
     self.参战单位[#self.参战单位].修炼数据={}
    for n=1,#全局变量.修炼名称 do
      self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=玩家数据[id].角色.数据.召唤兽修炼[全局变量.修炼名称[n]].等级
    end

    if self.参战单位[#self.参战单位-1].战斗类型 == "角色" then
      for i = 35,38 do
        self.临时格子 = nil
        if self.参战单位[#self.参战单位-1].法宝数据 ~=nil and self.参战单位[#self.参战单位-1].法宝数据[i] ~= nil  then
          self.临时格子 = self.参战单位[#self.参战单位-1].法宝数据[i]
          if 玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子]~=nil and 玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].类型 == "法宝" then
            if  玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].名称 == "重明战鼓" then
                self.参战单位[#self.参战单位].伤害 = self.参战单位[#self.参战单位].伤害 + (玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].层数 +1) *10
                self.参战单位[#self.参战单位].灵力 = self.参战单位[#self.参战单位].灵力 + (玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].层数 +1) *10
            elseif 玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].名称 == "梦云幻甲" then
                self.参战单位[#self.参战单位].防御 = self.参战单位[#self.参战单位].防御 + (玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].层数 +1) *10
                self.参战单位[#self.参战单位].法防 = self.参战单位[#self.参战单位].法防 + (玩家数据[self.参战单位[#self.参战单位-1].玩家id].道具.数据[self.临时格子].层数 +1) *10
            end
          end
        end
      end

      if self.参战单位[#self.参战单位].战斗类型 == "bb" and self:取经脉(self.参战单位[#self.参战单位-1],"御兽") and self.参战单位[#self.参战单位-1].速度 >= self.参战单位[#self.参战单位].等级*3 then
          self.参战单位[#self.参战单位-1].速度 = self.参战单位[#self.参战单位-1].速度 + self.参战单位[#self.参战单位].等级*1.6
      end

    end
    if self.参战单位[#self.参战单位].战斗类型 == "bb" then
      if self.参战单位[#self.参战单位].父编号 ~= nil and self.参战单位[self.参战单位[#self.参战单位].父编号] ~= nil and self:取经脉(self.参战单位[self.参战单位[#self.参战单位].父编号],"国色") then
        self.参战单位[#self.参战单位].速度 =self.参战单位[#self.参战单位].速度*1.12
      end
      if self.参战单位[#self.参战单位].父编号 ~= nil and self.参战单位[self.参战单位[#self.参战单位].父编号] ~= nil and self:取经脉(self.参战单位[self.参战单位[#self.参战单位].父编号],"驭兽") then
        self.参战单位[#self.参战单位].伤害 =self.参战单位[#self.参战单位].伤害*1.12
        self.参战单位[#self.参战单位].防御 =self.参战单位[#self.参战单位].防御*1.12
        self.参战单位[#self.参战单位].灵力 =self.参战单位[#self.参战单位].灵力*1.12
      end
      if self.参战单位[#self.参战单位].父编号 ~= nil and self.参战单位[self.参战单位[#self.参战单位].父编号] ~= nil and self:取经脉(self.参战单位[self.参战单位[#self.参战单位].父编号],"御兽") then
        self.参战单位[#self.参战单位].速度 =self.参战单位[#self.参战单位].速度+self.参战单位[#self.参战单位].等级*1.6
      end

      if self.参战单位[#self.参战单位].父编号 ~= nil and self.参战单位[self.参战单位[#self.参战单位].父编号] ~= nil and self:取经脉(self.参战单位[self.参战单位[#self.参战单位].父编号],"肝胆相照") then
        self.参战单位[#self.参战单位].伤害 =self.参战单位[#self.参战单位].伤害+self.参战单位[self.参战单位[#self.参战单位].父编号].伤害*0.1
        self.参战单位[#self.参战单位].防御 =self.参战单位[#self.参战单位].伤害+self.参战单位[self.参战单位[#self.参战单位].父编号].防御*0.1
        self.参战单位[#self.参战单位].灵力 =self.参战单位[#self.参战单位].伤害+self.参战单位[self.参战单位[#self.参战单位].父编号].灵力*0.1

        self.参战单位[self.参战单位[#self.参战单位].父编号].伤害 =self.参战单位[self.参战单位[#self.参战单位].父编号].伤害+self.参战单位[#self.参战单位].伤害*0.1
        self.参战单位[self.参战单位[#self.参战单位].父编号].防御 =self.参战单位[self.参战单位[#self.参战单位].父编号].防御+self.参战单位[#self.参战单位].防御*0.1
        self.参战单位[self.参战单位[#self.参战单位].父编号].灵力 =self.参战单位[self.参战单位[#self.参战单位].父编号].灵力+self.参战单位[#self.参战单位].灵力*0.1
      end
    end

    self:设置队伍区分(id)
    self:增加阵法属性(self:取队伍id(id),位置+5,#self.参战单位)
   end

 end
function 战斗处理类:增加阵法属性(队伍id,位置,编号)
 if 队伍数据[队伍id]~=nil and #队伍数据[队伍id].队员数据==5 then
    self.阵法名称=队伍数据[队伍id].阵法
    if self.阵法使用 then
      self.参战单位[编号].阵法=self.阵法名称
    else
      self.参战单位[编号].阵法="普通"
    end

    if 位置>5 then return 0 end
    self.阵法提升效果 = 1
    if self:取经脉(self.参战单位[1],"扶阵") then
      self.阵法提升效果 = 1.03
    end
    if self.阵法名称=="天覆阵" then

     self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2*self.阵法提升效果)
     self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2*self.阵法提升效果)
     self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.8*self.阵法提升效果)

    elseif self.阵法名称=="地载阵" then

      if 位置<=4 then

       self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.15*self.阵法提升效果)
       self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.15*self.阵法提升效果)
       self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15*self.阵法提升效果)

      else
        self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15*self.阵法提升效果)



        end
     elseif self.阵法名称=="鸟翔阵" then
        self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.2*self.阵法提升效果)

     elseif self.阵法名称=="风扬阵" then

       if 位置==1 then

         self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2*self.阵法提升效果)
         self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2*self.阵法提升效果)
        elseif 位置==2 or 位置==3 then
         self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1*self.阵法提升效果)
         self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1*self.阵法提升效果)
        elseif 位置==4 then
         self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.1*self.阵法提升效果)

        elseif 位置==5 then

          self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.1*self.阵法提升效果)


         end

     elseif self.阵法名称=="云垂阵" then

       if 位置==1 then

         self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.4*self.阵法提升效果)
         self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.7*self.阵法提升效果)
        elseif 位置==2  then
          self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.2*self.阵法提升效果)
          self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.1*self.阵法提升效果)
        elseif  位置==3 then
         self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1*self.阵法提升效果)
         self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1*self.阵法提升效果)
        elseif 位置==4 then
         self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15*self.阵法提升效果)

        elseif 位置==5 then

          self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15*self.阵法提升效果)


         end
     elseif self.阵法名称=="龙飞阵" then
        if 位置==1 then

          self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.2*self.阵法提升效果)
        elseif 位置==2  then
          self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.2*self.阵法提升效果)

        elseif  位置==3 then
         self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.7*self.阵法提升效果)
         self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.3*self.阵法提升效果)
        elseif 位置==4 then
         self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15*self.阵法提升效果)

        elseif 位置==5 then

         self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2*self.阵法提升效果)
         self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2*self.阵法提升效果)


         end
      elseif self.阵法名称=="虎翼阵" then

        self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1*self.阵法提升效果)

      elseif self.阵法名称=="蛇蟠阵" then

        self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.2*self.阵法提升效果)


     end









   end


  end
function 战斗处理类:设置队伍区分(id)

 if 玩家数据[id].队伍==0 then

   self.临时区分id=id

  else
   self.临时区分id=玩家数据[id].队伍


   end

 if self.队伍区分[1]==self.临时区分id then

   self.队伍数量[1]=self.队伍数量[1]+1

  else
   self.队伍数量[2]=self.队伍数量[2]+1
   self.队伍区分[2]=self.临时区分id


   end


 end
function 战斗处理类:攻击流程计算(编号,伤害,连击)-------------------------------------完成
  -----隐身判断
  if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
    self.相关id=self.参战单位[编号].队伍id
    self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
      else
      self.参战单位[编号].命令数据.目标=self.临时目标[1]
      end
  end
  if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
  return 0
  end
 self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型="攻击",反震=false,反击=false,吸血=false,混元伞 = false,混元伞死亡 = 0,五彩娃娃 = false,金甲仙衣 = false,挨打动作="被击中"}--流程=1为从攻击
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.躲避=false
 self.等待结束=self.等待结束+6
 self.临时躲避=self.参战单位[self.参战单位[编号].命令数据.目标].躲闪+self.参战单位[编号].命中
 self.临时比较=self.参战单位[编号].命中/self.临时躲避*100
  if self.临时比较>=50 then
  self.临时比较=50
  end
  -------开始躲避
  if self.临时比较<取随机数(1,200) then
  self.躲避=true
  end
  self.躲避=false
  self.战斗流程[#self.战斗流程].躲避=self.躲避

  -----------------躲避判断
 if self.躲避==false then
   self.临时伤害=self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
   self.保护=false
   self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
    if self.必杀 then
      if self:取经脉(self.参战单位[编号],"杀意") then
        self.临时伤害=self.临时伤害*2.2+self.参战单位[编号].狂怒
      else
        self.临时伤害=self.临时伤害*2+self.参战单位[编号].狂怒
      end
      self.战斗流程[#self.战斗流程].必杀动作=1
      self.苍鸾怒击=true
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.催眠符.有无==false then
          if self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿.有无==false then
          self.临时伤害=math.floor(self.临时伤害*0.5)
          end
          self.战斗流程[#self.战斗流程].挨打动作="防御"
          if self.临时伤害<1 then
          self.临时伤害=1
          end
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.煞气诀.有无==false then
          if self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿.有无==false then
            self.临时伤害=math.floor(self.临时伤害*0.5)
          end
          self.战斗流程[#self.战斗流程].挨打动作="防御"
          if self.临时伤害<1 then
          self.临时伤害=1
          end
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and self:取经脉(self.参战单位[编号],"击破") then
          self.临时伤害=math.floor(self.临时伤害*1.5)
          self.战斗流程[#self.战斗流程].挨打动作="防御"
          if self.临时伤害<1 then
          self.临时伤害=1
          end
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 ~= nil and self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 >0 and 取随机数() <= self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 then

      self.临时伤害 = self.临时伤害 *0.7
      self.战斗流程[#self.战斗流程].金甲仙衣 = true
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 ~= nil and self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 >0 and 取随机数() <= self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 then
      self.临时伤害 = self.临时伤害 *0.5
      self.战斗流程[#self.战斗流程].五彩娃娃 = true
    end
   self.保护=self:取保护单位(self.参战单位[编号].命令数据.目标)
   self.吸血=self:取是否吸血(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
     -----------------吸血伤害计算
      if self.吸血 then
        self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*self.参战单位[编号].吸血),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
        self.战斗流程[#self.战斗流程].吸血=true
      end
     ------------------保护流程
      if self.保护==false then
          self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,"掉血")
          self.战斗流程[#self.战斗流程].挨打数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
          if 取随机数()<=self.参战单位[编号].毒 and self.参战单位[self.参战单位[编号].命令数据.目标].复活==0 and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.百毒不侵.有无==false then
              self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].有无=true
              self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].回合=5
              self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].等级=self.参战单位[编号].等级
              self.战斗流程[#self.战斗流程].毒=true
          end
          if  self:取存活状态(self.参战单位[编号].命令数据.目标) and  self:取存活状态(编号) then
            self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
            if self.反震 then
              self.临时伤害=self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打数据.伤害,self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],0.5,"掉血",1)
              self.战斗流程[#self.战斗流程].反震数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
              self.等待结束=self.等待结束+4
            end
          end

          if self.反震 == false and self:取存活状态(self.参战单位[编号].命令数据.目标)  and self.参战单位[self.参战单位[编号].命令数据.目标].混元伞 ~=nil and self.参战单位[self.参战单位[编号].命令数据.目标].混元伞 > 0 and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.混元伞.有无 then
            self.临时伤害 = self:取物理伤害结果(self.临时伤害.伤害, self.参战单位[self.参战单位[编号].命令数据.目标], self.参战单位[编号], (self.参战单位[self.参战单位[编号].命令数据.目标].混元伞/100), "掉血", 1)
            self.战斗流程[#self.战斗流程].混元伞数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
          end
          if  self:取存活状态(self.参战单位[编号].命令数据.目标) and  self:取存活状态(编号) then
            self.反击=self:取是否反击(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
            if self.反击 then
              self.临时伤害=self:取物理伤害(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1)
              self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],0.5,"掉血",1)
              self.战斗流程[#self.战斗流程].反击数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
              self.等待结束=self.等待结束+4
            end
          end
      else
          self.临时伤害1=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],0.3,"掉血")
          self.战斗流程[#self.战斗流程].挨打数据={伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
          self.临时伤害1=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.保护],0.7,"掉血",1)
          self.战斗流程[#self.战斗流程].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
          self.等待结束=self.等待结束+3
      end
     -----------------
   end
   --------------------躲避判断结束

   if self:取经脉(self.参战单位[编号],"风刃") then
      self.相关id=self.参战单位[编号].队伍id
      self.临时目标=self:取随机单位(编号,self.相关id,1,3)

      if  self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.火甲术.有无 and self:取存活状态(self.参战单位[编号].命令数据.目标) and  self:取存活状态(编号) then --火甲术
     self:单体法术计算(self.参战单位[编号].命令数据.目标,nil,"三昧真火",编号)
      end

      if self.战斗流程[#self.战斗流程].挨打数据.伤害 > self.参战单位[self.参战单位[编号].命令数据.目标].气血上限*0.2 and self:取经脉(self.参战单位[self.参战单位[编号].命令数据.目标],"不舍") and self.参战单位[self.参战单位[编号].命令数据.目标].技能封印 == false and self:取存活状态(self.参战单位[编号].命令数据.目标) then
      self:单体恢复法术计算(self.参战单位[编号].命令数据.目标,nil,1)
      end

      if self.临时目标==0 then
          return 0
      end

      for n=1,#self.临时目标 do
        if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
          table.remove(self.临时目标,n)
        end
      end
       self.战斗流程[#self.战斗流程+1]={流程=24,执行=false,结束=false,类型="攻击",参数="溅射"}--流程=30为从非物理技能开始
       self.战斗流程[#self.战斗流程].攻击方=编号
       self.战斗流程[#self.战斗流程].挨打方={}
       self.等待结束=self.等待结束+5
      for n=1,#self.临时目标 do
          self.等待结束=self.等待结束+2
          self.战斗流程[#self.战斗流程].挨打方[n]={反震=false}
          self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
          self.战斗流程[#self.战斗流程].挨打方[n].伤害= self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
          self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],0.3,"掉血")
          self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
          self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
          self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
      end
   end

   if self.参战单位[编号].溅射~=0  then
     self.相关id=self.参战单位[编号].队伍id
     self.临时目标=self:取随机单位(编号,self.相关id,1,10)
       if self.临时目标==0 then
          return 0
        end
        for n=1,#self.临时目标 do
          if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
            table.remove(self.临时目标,n)
          end
        end
         self.战斗流程[#self.战斗流程+1]={流程=24,执行=false,结束=false,类型="攻击",参数="溅射"}--流程=30为从非物理技能开始
         self.战斗流程[#self.战斗流程].攻击方=编号
         self.战斗流程[#self.战斗流程].挨打方={}
         self.等待结束=self.等待结束+5
         for n=1,#self.临时目标 do
            self.等待结束=self.等待结束+2
            self.战斗流程[#self.战斗流程].挨打方[n]={反震=false}
            self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
            self.战斗流程[#self.战斗流程].挨打方[n].伤害= self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
            self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],self.参战单位[编号].溅射,"掉血")
            self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
            self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
            self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
        end
   end


 end
function 战斗处理类:道具流程计算(编号)
  if self.参战单位[编号].道具封印 then
    self:添加提示(self.参战单位[编号].数字id, "你无法使用道具")
    return 0
   end
self.道具玩家id = self.参战单位[编号].玩家id
  self.参战单位[编号].命令数据.参数 = self.参战单位[编号].命令数据.参数 + 0
  self.命令类型 = self.参战单位[编号].命令数据.类型
  if self.命令类型 == "道具" then
    self.检查关键字 = "包裹"
  else
      self.检查关键字 = "法宝"
  end
  if 玩家数据[self.道具玩家id].角色.数据.道具[self.检查关键字][self.参战单位[编号].命令数据.参数] == nil  then
    self:添加提示(self.参战单位[编号].数字id, "您没有这样的道具")
    return 0
  end
  if self.检查关键字 == "法宝" then
    self.道具使用id = 玩家数据[self.道具玩家id].角色.数据.道具.法宝[self.参战单位[编号].命令数据.参数]
    if self:取经脉(self.参战单位[编号],"神念") then
      self.参战单位[编号].愤怒 = self.参战单位[编号].愤怒 +10
      if self.参战单位[编号].愤怒 >= 150 then
        self.参战单位[编号].愤怒 = 150
       end
    end
  else
    self.道具使用id = 玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]
  end

  self.道具执行流程 = 0
  if 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称=="醉生梦死" and self.参战单位[编号].战斗类型=="角色"  then
    self.参战单位[编号].愤怒=self.参战单位[编号].愤怒+玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质
    self:添加提示(self.参战单位[编号].数字id,"你增加了"..玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质.."点愤怒")
    if self.参战单位[编号].愤怒>150 then

     self.参战单位[编号].愤怒=150
      end

    玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

    return 0
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称=="珍露酒" and self.参战单位[编号].战斗类型=="角色"  then
    self.参战单位[编号].愤怒=self.参战单位[编号].愤怒+math.floor(玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质*0.4+10)
    if self.参战单位[编号].愤怒>150 then

     self.参战单位[编号].愤怒=150
      end
    self:添加提示(self.参战单位[编号].数字id,"你增加了"..玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质.."点愤怒")
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
    return 0
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "混元伞" then
    self.参战单位[编号].混元伞 = (玩家数据[self.道具玩家id].道具.数据[self.道具使用id].层数+1)*10
    self:添加封印状态(self.参战单位[编号], self.参战单位[编号], "混元伞")
    self.道具执行流程 = {
      类型 = "混元伞",
      数额 = 1
    }
   elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "月光宝盒" then
       self:添加提示(self.参战单位[编号].数字id,"该物品为非战斗类法宝")
      return 0
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "分水" then
    if self.参战单位[编号].门派 == "龙宫" then
      self.参战单位[self.参战单位[编号].命令数据.目标].分水 = (玩家数据[self.道具玩家id].道具.数据[self.道具使用id].层数+1)*10
      self:添加封印状态(self.参战单位[编号], self.参战单位[编号], "分水")
      self.道具执行流程 = {
        类型 = "分水",
        数额 = 1
      }
    else
      self:添加提示(self.参战单位[编号].数字id,"你的门派无法使用此法宝")
      return 0
    end
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "赤焰" then
    if self.参战单位[编号].门派 == "魔王寨" then
      self.参战单位[self.参战单位[编号].命令数据.目标].赤焰 = (玩家数据[self.道具玩家id].道具.数据[self.道具使用id].层数+1)*10
      self:添加封印状态(self.参战单位[编号], self.参战单位[编号], "赤焰")
      self.道具执行流程 = {
        类型 = "赤焰",
        数额 = 1
      }
    else
       self:添加提示(self.参战单位[编号].数字id,"你的门派无法使用此法宝")
       return 0
    end
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "神木宝鼎" then
    if self.参战单位[编号].门派 == "神木林" then
      self.参战单位[self.参战单位[编号].命令数据.目标].神木宝鼎 = (玩家数据[self.道具玩家id].道具.数据[self.道具使用id].层数+1)*10
      self:添加封印状态(self.参战单位[编号], self.参战单位[编号], "神木宝鼎")
      self.道具执行流程 = {
        类型 = "神木宝鼎",
        数额 = 1
      }
    else
      self:添加提示(self.参战单位[编号].数字id,"你的门派无法使用此法宝")
      return 0
    end
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称 == "金蟾" then
    if self.参战单位[编号].门派 == "无底洞" then
      self.参战单位[编号].金蟾 = (玩家数据[self.道具玩家id].道具.数据[self.道具使用id].层数+1)*10
      self:群体恢复法术计算(编号,nil,"金蟾")
      return 0
    else
       self:添加提示(self.参战单位[编号].数字id,"你的门派无法使用此法宝")
       return 0
    end

 elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].等级~=3 then


   if self:取存活状态(self.参战单位[编号].命令数据.目标)==false then

     self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
     return 0
    else
     if 游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].气血~=0 then
       self:恢复血魔(self.参战单位[编号].命令数据.目标,2,游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].气血+math.floor(游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].气血*self.参战单位[编号].识药/100))
       self.道具执行流程={类型="加血",数额=游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].气血+math.floor(游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].气血*self.参战单位[编号].识药/100)}

      elseif 游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].魔法~=0 then

       self:恢复血魔(self.参战单位[编号].命令数据.目标,3,游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].魔法+math.floor(游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].魔法*self.参战单位[编号].识药/100))
       self.道具执行流程={类型="加蓝",数额=游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].魔法+math.floor(游戏装备.药品[玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称].魔法*self.参战单位[编号].识药/100)}

       end
     if 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].数量~=nil then
        玩家数据[self.道具玩家id].道具.数据[self.道具使用id].数量=玩家数据[self.道具玩家id].道具.数据[self.道具使用id].数量-1
        if 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].数量<=0 then
          玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=nil
           玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

           end
      else
        玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=nil
        玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
      end

     end
  elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].等级==3 then

   if 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称~="佛光舍利子" and 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称~="九转回魂丹" then

      if self:取存活状态(self.参战单位[编号].命令数据.目标)==false then

       self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
       return 0
     else


      self.名称=玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称
      self.品质=玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质
   if self.名称=="金创药" then

     self:恢复血魔(self.参战单位[编号].命令数据.目标,2,400+math.floor(400*self.参战单位[编号].识药/100))
     self.道具执行流程={类型="加血",数额=400+math.floor(400*self.参战单位[编号].识药/100)}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
    elseif self.名称=="千年保心丹" then
     self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*4+200+self.参战单位[编号].识药)
     self.道具执行流程={类型="加血",数额=self.品质*4+200+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

    elseif self.名称=="金香玉" then
        self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*12+150+self.参战单位[编号].识药)
     self.道具执行流程={类型="加血",数额=self.品质*12+150+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
    elseif self.名称=="五龙丹" then
        self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*3+self.参战单位[编号].识药)
     self.道具执行流程={类型="加血",数额=self.品质*3+self.参战单位[编号].识药,解封=true,目标=self.参战单位[编号].命令数据.目标}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

     self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])
    elseif self.名称=="小还丹" then

     self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*8+100+self.参战单位[编号].识药)
     self.道具执行流程={类型="加血",数额=self.品质*8+100+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil


    elseif self.名称=="风水混元丹" then

     self:恢复血魔(self.参战单位[编号].命令数据.目标,3,self.品质*3+50+self.参战单位[编号].识药)
     self.道具执行流程={类型="加蓝",数额=self.品质*3+50+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil


    elseif self.名称=="蛇蝎美人" then
     self:恢复血魔(self.参战单位[编号].命令数据.目标,3,self.品质*5+100+self.参战单位[编号].识药)
     self.道具执行流程={类型="加蓝",数额=self.品质*5+100+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil


    elseif self.名称=="十香返生丸" then

     self:恢复血魔(self.参战单位[编号].命令数据.目标,3,self.品质*3+50+self.参战单位[编号].识药)
     self.道具执行流程={类型="加蓝",数额=self.品质*3+50+self.参战单位[编号].识药}
     玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
     玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil



      end


        end


    elseif 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称=="佛光舍利子" or 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称=="九转回魂丹" then

      if self:取存活状态(self.参战单位[编号].命令数据.目标) then

       self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
       return 0
      else
        if 玩家数据[self.道具玩家id].道具.数据[self.道具使用id].名称=="佛光舍利子" then

          self:恢复血魔(self.参战单位[编号].命令数据.目标,2,玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质*3)
          self.道具执行流程={类型="加血",数额=玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质*3,复活=true}
          玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
          玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

        else
         self:恢复血魔(self.参战单位[编号].命令数据.目标,2,玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质*5)
         self.道具执行流程={类型="加血",数额=玩家数据[self.道具玩家id].道具.数据[self.道具使用id].品质*5,复活=true}
         玩家数据[self.道具玩家id].道具.数据[self.道具使用id]=0
         玩家数据[self.道具玩家id].角色.数据.道具.包裹[self.参战单位[编号].命令数据.参数]=nil

         end


       end


     end



   end

 if self.道具执行流程~=0 then

   self.战斗流程[#self.战斗流程+1]={流程=105,执行=false,结束=false,类型="技能",参数=self.道具执行流程.类型}--流程=30为从非物理技能开始
   self.战斗流程[#self.战斗流程].攻击方=编号
   self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
   self.战斗流程[#self.战斗流程].数额=self.道具执行流程.数额+self.参战单位[self.参战单位[编号].命令数据.目标].气血回复效果
   self.战斗流程[#self.战斗流程].复活=self.道具执行流程.复活
   self.战斗流程[#self.战斗流程].解封=self.道具执行流程.解封
   self.等待结束=self.等待结束+4

    end

 -- table.print(self.战斗流程[#self.战斗流程])
 end
function 战斗处理类:恢复血魔(编号,类型,数额)

  数额=self.参战单位[编号].气血回复效果+数额
  if 类型==2 then

    if self.参战单位[编号].当前气血<=0 then
       self.参战单位[编号].当前气血=0
    end

   self.参战单位[编号].当前气血=self.参战单位[编号].当前气血+数额
   if self.参战单位[编号].当前气血>self.参战单位[编号].气血上限 then

     self.参战单位[编号].当前气血=self.参战单位[编号].气血上限


     end

  elseif 类型==3 then
    if self.参战单位[编号].当前魔法<=0 then
       self.参战单位[编号].当前魔法=0
    end
   self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+数额
   if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then

     self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限


     end


   end






 end
function 战斗处理类:逃跑流程计算(编号)

 if 取随机数()<=40 then

   self:添加提示(0,self.参战单位[编号].名称.."逃离了战场")

   for n=1,#self.参战玩家 do

     if self.参战玩家[n].玩家id==self.参战单位[编号].玩家id then


        self.逃跑玩家=n
        self.参战玩家[n].逃跑=true

       end

     end
   if 玩家数据[self.参战单位[编号].玩家id].队伍~=0 then

     队伍处理类:离开队伍(玩家数据[self.参战单位[编号].玩家id].队伍,self.参战单位[编号].玩家id)

     end
   self.剩余数量=0
   for n=1,#self.参战玩家 do

     if self.参战玩家[n].逃跑==false then

       self.剩余数量=self.剩余数量+1
       end

     end
   if #self.参战玩家==1 then --直接结束战斗

     self.中断结束=true
      for n=1,#self.参战单位 do

        if self.参战单位[n].玩家id==self.参战单位[编号].玩家id then


          self.参战单位[n].单位消失=1


          end

       end

    else
     地图处理类:更改战斗(self.参战单位[编号].玩家id,false)
     self.接收数量=self.接收数量-1
     self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结果=self.捕捉结果,结束=false,类型="逃跑",逃跑单位={},id=self.参战单位[编号].玩家id}
     for n=1,#self.参战单位 do

        if self.参战单位[n].玩家id==self.参战单位[编号].玩家id then


          self.参战单位[n].单位消失=1
          self.战斗流程[#self.战斗流程].逃跑单位[#self.战斗流程[#self.战斗流程].逃跑单位+1]=n

          end

       end


     end

  else

   self:添加提示(0,self.参战单位[编号].名称.."逃离战场失败")

   end


 end
function 战斗处理类:捕捉流程计算(编号)

  if self.战斗类型~=100001 then


   self:添加提示(self.参战单位[编号].数字id,"当前战斗类型无法使用捕捉指令")
   return 0
  elseif self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
   self:添加提示(self.参战单位[编号].数字id,"目标当前无法捕捉")
   return 0

    end
 self.消耗魔法=15+self.参战单位[self.参战单位[编号].命令数据.目标].等级
 --self.消耗魔法=0
 if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
   self:添加提示(self.参战单位[编号].数字id,"目标当前无法捕捉")
   return 0
  -- elseif self.参战单位[编号].等级<召唤兽[self.参战单位[self.参战单位[编号].命令数据.目标].造型].参战等级 then

  --  self:添加提示(self.参战单位[编号].数字id,"捕捉该目标需要人物等级达到"..召唤兽[self.参战单位[self.参战单位[编号].命令数据.目标].造型].参战等级.."级")
  --  return 0
  elseif  self.参战单位[编号].当前魔法<self.消耗魔法 then

    self:添加提示(self.参战单位[编号].数字id,"你没那么多的魔法")
   return 0
  elseif  #玩家数据[self.参战单位[编号].玩家id].召唤兽.数据>=8 then

    self:添加提示(self.参战单位[编号].数字id,"你当前可携带的召唤兽数量已达上限")
   return 0


   end
 self.等待结束=self.等待结束+3
 self.初始几率=35
 self.初始几率=self.初始几率+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].当前气血/self.参战单位[self.参战单位[编号].命令数据.目标].气血上限*100)
 if self.初始几率>80 then self.初始几率=80 end

 if 取随机数()<=self.初始几率 then

   self.捕捉结果=true
   self.参战单位[self.参战单位[编号].命令数据.目标].单位消失=1
   玩家数据[self.参战单位[编号].玩家id].召唤兽:创建召唤兽(self.参战单位[self.参战单位[编号].命令数据.目标].造型,self.参战单位[self.参战单位[编号].命令数据.目标].宝宝,self.参战单位[self.参战单位[编号].命令数据.目标].变异,1,self.参战单位[self.参战单位[编号].命令数据.目标].等级)
   玩家数据[self.参战单位[编号].玩家id].召唤兽.数据.技能={}
   for n=1,#self.参战单位[self.参战单位[编号].命令数据.目标].技能 do

     玩家数据[self.参战单位[编号].玩家id].召唤兽.数据.技能[n]=self.参战单位[self.参战单位[编号].命令数据.目标].技能[n]

     end
   self:添加提示(self.参战单位[编号].数字id,"捕捉成功")



  else
   self.捕捉结果=false
   self:添加提示(self.参战单位[编号].数字id,"捕捉失败")


   end

  self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-self.消耗魔法
  self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结果=self.捕捉结果,结束=false,类型="捕捉"}
  self.战斗流程[#self.战斗流程].攻击方=编号
  self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标


 end
function 战斗处理类:群体物理法术计算(编号)
  self.保护=false

  -- print("群体物理法术计算",编号)

  self.相关id=self.参战单位[编号].队伍id

  self.临时参数=self.参战单位[编号].命令数据.参数
  self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
  self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
  self.目标存在=false

  if self.临时参数=="鹰击"  then

   if self.参战单位[编号].法术状态组.变身.有无==false then

      self:添加提示(self.参战单位[编号].数字id,"你需要在变身状态下才可使用此技能")
      return 0

     end


   end




  if self.临时目标==0 then

   return 0

    end

  for n=1,#self.临时目标 do

    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

     self.目标存在=true


      end


    end


  if self.目标存在==false then

   if self:取存活状态(self.参战单位[编号].命令数据.目标) and self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then

     self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标

     end


    end



 if #self.临时目标==0 then --找不到可攻击的目标

   return 0
  elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then

   --  if self.临时参数=="破釜沉舟"  then
   --   self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
   --  else
   --   self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
   --    end
   --  return 0


   end



  if self.临时参数=="翻江搅海"  then

   if self.战意点数 < 3 then
     self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.战意点数)
    return 0
  else

    if self.战意点数 < 7 then
       self.战意点数 =self.战意点数 -3

  end
 end

  if self.临时目标==0 then

   return 0

    end

  for n=1,#self.临时目标 do

    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

     self.目标存在=true


      end


    end


  if self.目标存在==false then

   if self:取存活状态(self.参战单位[编号].命令数据.目标) and self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then

     self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标

     end


    end

  end





  if self:取经脉(self.参战单位[编号],"魂聚") and ( self.临时参数 == "浪涌" or self.临时参数 == "惊涛怒" or self.临时参数 == "翻江搅海" ) then
    self.参战单位[编号].愤怒 = self.参战单位[编号].愤怒 + 6
    if self.参战单位[编号].愤怒 >= 150 then
      self.参战单位[编号].愤怒 = 150
    end
  end



  if self.临时参数 == "浪涌" then
    self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)

    elseif self.临时参数 == "裂石" then

     self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)

    elseif self.临时参数 == "断岳势" then

     self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)

    elseif self.临时参数 == "腾雷" then

     self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)
    elseif self.临时参数 == "惊涛怒" then
   if self.战意点数 < 1 then
     self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.战意点数)

    return 0


  else

    if self.战意点数 < 7 then
       self.战意点数 =self.战意点数 -1
       self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)
    end
  end

  end



 self.战斗流程[#self.战斗流程+1]={流程=9,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}

 self.等待结束=self.等待结束+5

  if self.临时参数=="破釜沉舟"  or self.临时参数 == "剑荡四方"then

   self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
   self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
   self.战斗流程[#self.战斗流程].流程=83

    end

 for n=1,#self.临时目标 do
   self.等待结束=self.等待结束+2
   self.战斗流程[#self.战斗流程].挨打方[n]={反震=false,反震死亡=false,必杀动作="",防御动作="",反震动作="",五彩娃娃 = false,金甲仙衣 = false, 混元伞 = false, 混元伞死亡 = 0}
   self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
   self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数,#self.临时目标)
   self.战斗流程[#self.战斗流程].挨打方[n].类型="掉血"
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=0

    --计算是否必杀
   self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.临时目标[n]])
   if self.必杀 then
      if self:取经脉(self.参战单位[编号],"杀意") then
        self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*2.2)
      else
        self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*2)
      end
      self.战斗流程[#self.战斗流程].挨打方[n].必杀动作="必杀"



     end

   --modify by sun 20190729 与下面代码重复

   -- if self.参战单位[self.临时目标[n]].命令数据.类型=="防御" and self.参战单位[self.临时目标[n]].法术状态组.催眠符.有无==false then
   --  if  self.参战单位[self.临时目标[n]].法术状态组.天地同寿~=nil and self.参战单位[self.临时目标[n]].法术状态组.天地同寿.有无==false then
   --      self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.5)
   --     end
   --   --self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.5)
   --  -- self.战斗流程[#self.战斗流程].挨打动作="防御"
   --   self.战斗流程[#self.战斗流程].挨打方[n].防御动作=1
   --   -- print("!1111111111111111")
   --   -- if self.临时伤害<1 then self.临时伤害=1 end

   --   end


   --   self.保护=self:取保护单位(self.临时目标[n])
   -- if self.保护==false then
   --   self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,"掉血")

   --  else

   --    self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],0.3,"掉血")
   --    self.临时伤害1= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.保护],0.7,"掉血")
   --   -- print(self.临时伤害1.伤害)
   --    self.战斗流程[#self.战斗流程].挨打方[n].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
   --    self.等待结束=self.等待结束+3


   --    end
   -- --self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,"掉血")
   -- self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
   -- self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
   -- self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
   --计算是否反震

  --  self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.临时目标[n]])
  -- -- self.反震=true
  --  if self.反震 and self:取存活状态(self.临时目标[n]) and self.保护==false then

  --    self.战斗流程[#self.战斗流程].挨打方[n].反震=true
  --    self.战斗流程[#self.战斗流程].挨打方[n].反震动作="反震"
  --    self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.25)
  --    self.临时伤害=self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].反震伤害,self.参战单位[self.临时目标[n]],self.参战单位[编号],1,"掉血",1)

  --    self.战斗流程[#self.战斗流程].挨打方[n].反震死亡=self.临时伤害.死亡
  --    self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=self.临时伤害.伤害






  --    end

   --计算是否神佑


   if self.参战单位[self.临时目标[n]].命令数据.类型=="防御" and self.参战单位[self.临时目标[n]].法术状态组.煞气诀.有无==false then
    if  self.参战单位[self.临时目标[n]].法术状态组.天地同寿~=nil and self.参战单位[self.临时目标[n]].法术状态组.天地同寿.有无==false then
        self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.5)
       end
     --self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.5)
    -- self.战斗流程[#self.战斗流程].挨打动作="防御"
     self.战斗流程[#self.战斗流程].挨打方[n].防御动作=1


     end
    if self.参战单位[self.临时目标[n]].金甲仙衣 ~= nil and self.参战单位[self.临时目标[n]].金甲仙衣 >0 and 取随机数() <= self.参战单位[self.临时目标[n]].金甲仙衣 then
      self.临时伤害 = self.临时伤害 *0.7
      self.战斗流程[#self.战斗流程].挨打方[n].金甲仙衣 = true
    end
    if self.参战单位[self.临时目标[n]].五彩娃娃 ~= nil and self.参战单位[self.临时目标[n]].五彩娃娃 >0 and 取随机数() <= self.参战单位[self.临时目标[n]].五彩娃娃 then
      self.临时伤害 = self.临时伤害 *0.5
      self.战斗流程[#self.战斗流程].挨打方[n].五彩娃娃 = true
    end
     self.保护=self:取保护单位(self.临时目标[n])
     -- print("!11111111111111aasdfdads")
   if self.保护==false then
     self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,"掉血")

    else

      self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],0.3,"掉血")
      self.临时伤害1= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.保护],0.7,"掉血")
     -- print(self.临时伤害1.伤害)
      self.战斗流程[#self.战斗流程].挨打方[n].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
      self.等待结束=self.等待结束+3


      end
   --self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,"掉血")
   self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
   self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
   --计算是否反震

   self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.临时目标[n]])
  -- self.反震=true
   if self.反震 and self:取存活状态(self.临时目标[n]) and self.保护==false then

     self.战斗流程[#self.战斗流程].挨打方[n].反震=true
     self.战斗流程[#self.战斗流程].挨打方[n].反震动作="反震"
     self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.25)
     self.临时伤害=self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].反震伤害,self.参战单位[self.临时目标[n]],self.参战单位[编号],1,"掉血",1)

     self.战斗流程[#self.战斗流程].挨打方[n].反震死亡=self.临时伤害.死亡
     self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=self.临时伤害.伤害

     end

   --计算是否神佑
    if self.反震 == false and self:取存活状态(self.临时目标[n]) and self.保护 == false and self.参战单位[self.临时目标[n]].混元伞 ~= nil and  self.参战单位[self.临时目标[n]].混元伞 > 0 and self.参战单位[self.临时目标[n]].法术状态组.混元伞.有无  then
      self.战斗流程[#self.战斗流程].挨打方[n].混元伞 = true
      self.战斗流程[#self.战斗流程].挨打方[n].混元伞伤害 = math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害 * (self.参战单位[self.临时目标[n]].混元伞/100))
      self.临时伤害 = self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].混元伞伤害, self.参战单位[self.临时目标[n]], self.参战单位[编号], 1, "掉血", 1)
      self.战斗流程[#self.战斗流程].挨打方[n].混元伞死亡 = self.临时伤害.死亡
      self.战斗流程[#self.战斗流程].挨打方[n].混元伞伤害 = self.临时伤害.伤害
    end
   end

 if self.临时参数=="鹰击"  then
   if self.参战单位[编号].蚩尤元神==1 and 取随机数()<=30 then
       self.战斗流程[#self.战斗流程].状态取消=1
    else
      self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)

   end
 elseif self.临时参数=="破釜沉舟" then

   self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)

 -- elseif self.临时参数=="法术防御" then

 --   self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)


  end


 end
function 战斗处理类:单体物理法术计算(编号,特技)

  --刷新目标单位
 self.开关=1
  if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then

    --二次刷新目标
    self.相关id=self.参战单位[编号].队伍id
    self.临时目标=self:取随机单位(编号,self.相关id,1,1)
    if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
    else
     self.参战单位[编号].命令数据.目标=self.临时目标[1]

      end

    end

 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0


   end

 self.临时参数=self.参战单位[编号].命令数据.参数

 if self.临时参数=="狮搏" or  self.临时参数=="连环击"  or  self.临时参数=="象形"  then

   if self.参战单位[编号].法术状态组.变身.有无==false then

      self:添加提示(self.参战单位[编号].数字id,"你需要在变身状态下才可使用此技能")
      return 0

     end


   end

 self.流程类型="技能"




  if self.参战单位[编号].蚩尤元神==1 and 取随机数()<=35 then
       self.战意点数=6
     end

  if self.临时参数=="天崩地裂"   then
    if self.战意点数 < 3 then
      self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.战意点数)
      return 0
    end
  end

  if self.临时参数=="天崩地裂"   then
    if self.参战单位[编号].当前魔法 < 50 then
       self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
      return 0
    end
  end

   if self.临时参数=="天崩地裂"   then
      if self.战意点数 < 7 then
      self.战意点数 =self.战意点数 -3
      self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-50

    end
  end

   if self.临时参数=="裂石"   then
      if self.战意点数 < 7 then
         self.战意点数 =self.战意点数 +1
         self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-50
         self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.战意点数)
      end
  end


   if self.临时参数=="断岳势"   then
      if self.战意点数 < 1 then
       self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.战意点数)
      return 0

    end
  end


   if self.临时参数=="断岳势"   then
      if self.参战单位[编号].当前魔法 < 50 then
       self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
      return 0

    end
  end
   if self.临时参数=="断岳势"   then
      if self.战意点数 < 7 then
      self.战意点数 =self.战意点数 -1
      self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-50

    end
  end
  if self:取经脉(self.参战单位[编号],"魂聚") and ( self.临时参数 == "断岳势" or self.临时参数 == "裂石" or self.临时参数 == "天崩地裂" ) then
    self.参战单位[编号].愤怒 = self.参战单位[编号].愤怒 + 6
    if self.参战单位[编号].愤怒 >= 150 then
      self.参战单位[编号].愤怒 = 150
    end
  end
   if self.临时参数=="腾雷"   then
      if self.战意点数 < 1 then
       self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.战意点数)
      return 0

    end
  end
  --  if self.临时参数=="腾雷"   then
  --     if self.参战单位[编号].当前魔法 < 60 then
  --      self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
  --     return 0

  --   end
  -- end

 if self.临时参数=="腾雷"   then
    if self.战意点数 < 7 then
    self.战意点数 =self.战意点数 -1
    -- self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-50

  end
 end


  if 特技==nil and self.临时参数~="后发制人" and self:技能消耗(self.参战单位[编号],1) ==false and self.临时参数~="连击" then

   if self.临时参数=="横扫千军" then

      self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
    else
      self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")


     end

    return 0

  elseif 特技~=nil then
    self.流程类型="特技"

    if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then

     self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
     return 0

      end

    end

 self.战斗流程[#self.战斗流程+1]={
    流程=1,
    执行=false,
    结束=false,
    类型=self.流程类型,
    参数=self.临时参数,
    攻击方=编号,
    横扫千军休息 = 0,
    变身解除 = 0
  }
  --流程=1为从物理技能开始
 --self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 if self.临时参数=="横扫千军" then
    self.攻击次数=3
    if self:取经脉(self.参战单位[编号],"无敌") then
      self.攻击次数 = 4
    end
    if self.参战单位[编号].蚩尤元神==1 or (self.参战单位[编号].名称=="四刀结束" and self.参战单位[编号].队伍==0) then
      self.攻击次数 = self.攻击次数 +1
    end
    self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
    self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗

 elseif self.临时参数=="天崩地裂"  then
  self.攻击次数=3
    if self:取经脉(self.参战单位[编号],"天神怒斩") then
      self.攻击次数 = 4
    end
 elseif self.临时参数=="连击"  then
   self.攻击次数=2

 elseif self.临时参数=="血雨"  then

   self.攻击次数=1
   self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
   self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗


  elseif self.临时参数=="烟雨剑法" or self.临时参数=="破血狂攻" or self.临时参数=="断岳势" then
   self.攻击次数=2
    if self.临时参数 == '烟雨剑法' and self:取经脉(self.参战单位[编号],"天命剑法") then
      self.攻击次数 = 取随机数(1,4)
    end
  elseif self.临时参数=="狮搏" or self.临时参数=="弱点击破" or self.临时参数=="善恶有报" or self.临时参数=="象形" or self.临时参数=="裂石" or self.临时参数=="腾雷"  then

   self.攻击次数=1

  elseif self.临时参数=="连环击" then

   self.攻击次数=self:取技能目标数(self.参战单位[编号],self.临时参数)
   if self:取经脉(self.参战单位[编号],"乱击") then
    self.攻击次数 = self.攻击次数 +1
   end
  elseif self.临时参数=="后发制人" then

   self.攻击次数=1
  elseif self.临时参数=="破碎无双" or self.临时参数=="大闹天宫" then

   self.攻击次数=1

   end
  if self:取经脉(self.参战单位[编号],"连破") and 取随机数()<=18 then
    self.战斗流程[#self.战斗流程].横扫千军休息 = 1
  end
  if self.临时参数 == "象形" and self:取经脉(self.参战单位[编号],"怒象") and self.参战单位[编号].当前气血 <= self.参战单位[编号].气血上限*0.3 and 取随机数() <= 70 then
    self.战斗流程[#self.战斗流程].变身解除 = 1
  end
 self.战斗流程[#self.战斗流程].动作={}

 for n=1,self.攻击次数 do

   if self:取存活状态(编号)==false or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
       if self.临时参数=="横扫千军" and self.战斗流程[#self.战斗流程].横扫千军休息 == 0 then

         self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)

        elseif self.临时参数=="连环击" then
         self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"横扫千军")

         self:解除封印状态(self.参战单位[编号],"变身")

        elseif self.临时参数=="天崩地裂" then

        elseif self.临时参数=="血雨" then
         self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)


         end
      return 0
     end


    self.等待结束=self.等待结束+3

   self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作+1]={必杀动作="",挨打动作="被击中",防御动作="",反震动作="",反震=false,死亡=0,反震死亡=0 , 五彩娃娃 = false,金甲仙衣 = false,混元伞 = false, 混元伞死亡 = 0}
   self.临时伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数,n)
   if self.临时参数 == "横扫千军" and self:取经脉(self.参战单位[编号],"破军") then
      self.临时伤害 = self.临时伤害 + self.参战单位[self.参战单位[编号].命令数据.目标].气血上限*0.01*n+200
   end
   if self.临时参数 == "天崩地裂" and  n == 3 and self:取经脉(self.参战单位[编号],"力战") then
      self.临时伤害 = self.临时伤害 + 360
   end

      self.吸血=self:取是否吸血(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
     -----------------吸血伤害计算
      if self.吸血  and  self.临时参数=="连击" then
      self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*self.参战单位[编号].吸血),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
      self.战斗流程[#self.战斗流程].吸血=true
      end
   --计算是否必杀

   self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])


   if self.必杀 then
      if self.临时参数=="连击" and self.开关==1 then
        self.苍鸾怒击=true
        self.开关=2
      end
      if self:取经脉(self.参战单位[编号],"杀意") then
        self.临时伤害=math.floor(self.临时伤害*2.2)+self.参战单位[编号].狂怒
      else
        self.临时伤害=math.floor(self.临时伤害*2)+self.参战单位[编号].狂怒
      end
      self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].必杀动作="必杀"
     end

   if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.催眠符.有无==false then
      if self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿.有无==false then
       self.临时伤害=math.floor(self.临时伤害*0.5)
       end

    -- self.临时伤害=math.floor(self.临时伤害*0.5)
     self.战斗流程[#self.战斗流程].挨打动作="防御"
     self.战斗流程[#self.战斗流程].防御动作=1
     if self.临时伤害<1 then self.临时伤害=1 end

     end
        if self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 ~= nil and self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 >0 and 取随机数() <= self.参战单位[self.参战单位[编号].命令数据.目标].金甲仙衣 then
      self.临时伤害 = self.临时伤害 *0.7
      self.战斗流程[#self.战斗流程].金甲仙衣 = true
    end
    if self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 ~= nil and self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 >0 and 取随机数() <= self.参战单位[self.参战单位[编号].命令数据.目标].五彩娃娃 then
      self.临时伤害 = self.临时伤害 *0.5
      self.战斗流程[#self.战斗流程].五彩娃娃 = true
    end
   self.伤害类型="掉血"
   if self.临时参数=="善恶有报" then

     if 取随机数()<=30 then
         self.伤害类型="加血"
         self.临时伤害=math.floor(self.临时伤害*0.35)

      --self.临时伤害.数额=math.floor(self.临时伤害.数额*0.35)
       --self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].伤害类型="加血"


       end


     end
    self.保护=self:取保护单位(self.参战单位[编号].命令数据.目标)
   if self.保护==false then
     self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.伤害类型)

    else
      self.临时伤害2=self.临时伤害

      self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],0.3,self.伤害类型)
      self.临时伤害1=self:取物理伤害结果(self.临时伤害2,self.参战单位[编号],self.参战单位[self.保护],0.7,self.伤害类型)
     -- print(self.临时伤害1.伤害)
      self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
      self.等待结束=self.等待结束+3

      end

   self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].伤害类型=self.临时伤害.类型
   self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].普通伤害=self.临时伤害.伤害
   self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].死亡=self.临时伤害.死亡

   if self.临时参数=="破碎无双" then

     self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法-math.floor(self.临时伤害.伤害*0.5)
     if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法<0 then self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=0 end


     end
   --print(self.临时伤害.死亡)



  -- print(self.临时伤害)

   --计算是否反震
   self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
   --self.反震=true
   if self.反震 and self:取存活状态(self.参战单位[编号].命令数据.目标) and self.保护==false  then

     self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].反震=true
     self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].反震动作="反震"
     self.临时伤害=self:取物理伤害结果(self.临时伤害.伤害,self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],0.5,"掉血",1)
     self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].反震伤害=self.临时伤害.伤害
     self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].反震伤害
     self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].反震死亡=self.临时伤害.死亡


     end

    if self.反震 == false and self:取存活状态(self.参战单位[编号].命令数据.目标) and self.保护 == false and self.参战单位[self.参战单位[编号].命令数据.目标].混元伞 ~=nil and self.参战单位[self.参战单位[编号].命令数据.目标].混元伞 > 0 and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.混元伞.有无 then
      self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].混元伞 = true
      self.临时伤害 = self:取物理伤害结果(self.临时伤害.伤害, self.参战单位[self.参战单位[编号].命令数据.目标], self.参战单位[编号], (self.参战单位[self.参战单位[编号].命令数据.目标].混元伞/100), "掉血", 1)
      self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].混元伞伤害 = self.临时伤害.伤害
      self.参战单位[编号].当前气血 = self.参战单位[编号].当前气血 - self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].混元伞伤害
      self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].混元伞死亡 = self.临时伤害.死亡
    end

   end
    if self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].普通伤害 > self.参战单位[self.参战单位[编号].命令数据.目标].气血上限*0.2 and self:取经脉(self.参战单位[self.参战单位[编号].命令数据.目标],"不舍") and self.参战单位[self.参战单位[编号].命令数据.目标].技能封印 == false and self:取存活状态(self.参战单位[编号].命令数据.目标) then
      self:单体恢复法术计算(self.参战单位[编号].命令数据.目标,nil,1)
    end


  if self.临时参数=="横扫千军" and self.战斗流程[#self.战斗流程].横扫千军休息 == 0 then

     self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)

    elseif self.临时参数=="连环击" then

     self:解除封印状态(self.参战单位[编号],"变身")

   self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"鹰击")


    elseif self.临时参数=="天崩地裂" then

   elseif self.临时参数=="血雨" then
   self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)

    elseif self.临时参数=="象形" then
      if self.战斗流程[#self.战斗流程].变身解除 ~= 0 then
         self.参战单位[编号].法术状态组.象形.回合 = self.参战单位[编号].法术状态组.象形.回合+1
         self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
         self.参战单位[编号].法术状态组.象形限制.有无=true
         self.参战单位[编号].法术状态组.象形限制.回合=2
      else
       self:解除封印状态(self.参战单位[编号],"变身")
       self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
       self.参战单位[编号].法术状态组.象形限制.有无=true
       self.参战单位[编号].法术状态组.象形限制.回合=2
      end
     end







 end
function 战斗处理类:特殊法术计算(编号)
  self.相关id=self.参战单位[编号].队伍id

  self.临时参数=self.参战单位[编号].命令数据.参数
  self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
  self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
  if self.临时目标==0 then return 0 end
  self.目标存在=false
  --print(self.临时参数)
  --print(self.临时目标)

  for n=1,#self.临时目标 do

    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

     self.目标存在=true


      end


    end


  if self.目标存在==false then

   if self:取存活状态(self.参战单位[编号].命令数据.目标) and self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then

     self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标

     end


    end



 if #self.临时目标==0 then --找不到可攻击的目标

   return 0

  elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0


   end


 self.战斗流程[#self.战斗流程+1]={流程=35,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5


  for n = 1, #self.临时目标 do
    self.战斗流程[#self.战斗流程].挨打方[n] = {}
    self.战斗流程[#self.战斗流程].挨打方[n].编号 = self.临时目标[n]
    self.战斗流程[#self.战斗流程].挨打方[n].伤害 = self:取技能伤害(self.参战单位[编号], self.参战单位[self.临时目标[n]], self.临时参数, #self.临时目标)
    self.战斗流程[#self.战斗流程].挨打方[n].法术暴击=false
    if self.法术暴击 then
      if 取随机数() <= self.参战单位[编号].法暴 + (self.参战单位[编号].法术暴击等级 - self.参战单位[self.临时目标[n]].抗法术暴击等级) / 10 then
        self.战斗流程[#self.战斗流程].挨打方[n].伤害 = self.战斗流程[#self.战斗流程].挨打方[n].伤害 * 1.5
        self.战斗流程[#self.战斗流程].挨打方[n].法术暴击=true
      end
      end
   self.战斗流程[#self.战斗流程].挨打方[n].类型="掉血"
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=0
   self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,self.战斗流程[#self.战斗流程].挨打方[n].类型)
   self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
   self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
   --计算是否神佑

    if self.临时参数 == "落叶萧萧" and self:取经脉(self.参战单位[编号],"咒术") and 取随机数()<=100 then
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].有无=true
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].回合=4
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],"雾杀")
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].攻击方=self.参战单位[编号]
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].蚩尤元神=self.参战单位[编号].蚩尤元神
     self.参战单位[self.临时目标[n]].咒术 = 1
    end

   end



 end
function 战斗处理类:群体恢复法术计算(编号,特技,法宝)
  self.相关id=self.参战单位[编号].队伍id
  if 法宝 == '金蟾' then
    self.临时参数="金蟾"
    self.技能数量=10
  else
    self.临时参数=self.参战单位[编号].命令数据.参数
    self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
  end
 if self.临时参数=="推气过宫" or self.临时参数=="地涌金莲" or self.临时参数 == "金蟾" then
  self.临时目标=self:取随机单位1(编号,self.相关id,2,self.技能数量,self.临时参数)
 else
  self.临时目标=self:取随机单位(编号,self.相关id,2,self.技能数量,self.临时参数)
 end
  self.目标存在=false
  if self.临时目标==0 then return 0 end

  for n=1,#self.临时目标 do

    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

     self.目标存在=true


      end


    end


  if self.目标存在==false then

   if self:取存活状态(self.参战单位[编号].命令数据.目标) then
     if self.临时参数=="推气过宫" or self.临时参数=="地涌金莲"  or self.临时参数 == "金蟾"  then

        self.临时目标[#self.临时目标]=self.参战单位[编号].命令数据.目标

    else
       self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标

      end

     end


    end

 self.流程类型="技能"

 if #self.临时目标==0 then --找不到可攻击的目标

   return 0

  elseif 特技==nil and 法宝 == nil and self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0

  elseif 特技~=nil then
    self.流程类型="特技"

    if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then

     self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
     return 0

      end


   end


 self.战斗流程[#self.战斗流程+1]={流程=49,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5

 for n=1,#self.临时目标 do

   self.战斗流程[#self.战斗流程].挨打方[n]={}
   self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
   --table.print(self.临时目标[n])
  -- print(self.临时目标[n],self.参战单位[self.临时目标[n]],"这是获取到的目标")
   self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数,#self.临时目标)
   self.战斗流程[#self.战斗流程].挨打方[n].类型="加血"
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=0
   self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,self.战斗流程[#self.战斗流程].挨打方[n].类型)
   self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
   self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
   self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡

   --计算是否神佑





     if self.临时参数=="生命之泉" or self.临时参数 == "普渡众生" or self.临时参数=="魔兽之印" or self.临时参数=="圣灵之甲" or self.临时参数=="罗汉金钟" then

        self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数)

      elseif self.临时参数=="玉清诀" or self.临时参数=="晶清诀" then

       self:解除异常状态(self.参战单位[self.临时目标[n]])



        end




   end



 end
function 战斗处理类:群体状态法术计算(编号)
  self.相关id=self.参战单位[编号].队伍id

  self.临时参数=self.参战单位[编号].命令数据.参数
  self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
  if  self.临时参数=="飞花摘叶" or  self.临时参数=="碎甲符" then
    self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
  else
    self.临时目标=self:取随机单位(编号,self.相关id,2,self.技能数量)
  end
  self.目标存在=false
  if  self.临时参数=="变身" and self:取存活状态(编号)==false then

   return 0

    end

  if  self.临时参数=="法术防御" and self:取存活状态(编号)==false then

   return 0

    end

  if self.临时目标==0 then return 0 end

  for n=1,#self.临时目标 do

    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

     self.目标存在=true


      end


    end


  if self.目标存在==false then

   if self:取存活状态(self.参战单位[编号].命令数据.目标) then

     self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标

     end


    end



 if #self.临时目标==0 then --找不到可攻击的目标

   return 0

  elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0


   end


 self.战斗流程[#self.战斗流程+1]={流程=52,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5

 for n=1,#self.临时目标 do

   self.战斗流程[#self.战斗流程].挨打方[n]={}
   self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
  self.战斗流程[#self.战斗流程].挨打方[n].结果 =true

   --计算是否神佑


    if self.临时参数=="后发制人" then
   self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数,self.临时目标[n])
    elseif self.临时参数=="飞花摘叶" then
        if 取随机数()<=50 then
           self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数,self.临时目标[n])
         else
          self.战斗流程[#self.战斗流程].挨打方[n].结果 =false
        end
    else
            self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数)

     end






   end



 end
function 战斗处理类:群体法术计算(编号)
  self.相关id=self.参战单位[编号].队伍id
  self.临时参数=self.参战单位[编号].命令数据.参数
  self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
  self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
  self.目标存在=false
  if self.临时目标==0 then return 0 end
  for n=1,#self.临时目标 do
    if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
     self.目标存在=true
      end
    end
  if self.目标存在==false then
   if self:取存活状态(self.参战单位[编号].命令数据.目标)  and  self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then
     self.临时目标[取随机数(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标
     end
    end
 if #self.临时目标==0 then --找不到可攻击的目标
   return 0
  elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then
    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
    return 0
   end
 self.战斗流程[#self.战斗流程+1]={流程=33,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5

 for n=1,#self.临时目标 do
    self.战斗流程[#self.战斗流程].挨打方[n] = {}
    self.战斗流程[#self.战斗流程].挨打方[n].编号 = self.临时目标[n]
    self.战斗流程[#self.战斗流程].挨打方[n].伤害 = self:取技能伤害(self.参战单位[编号], self.参战单位[self.临时目标[n]], self.临时参数, #self.临时目标)
    if self:取经脉(self.参战单位[编号],"雷波") and self.临时参数 == "雷霆万钧" and n == 1 then
      self.战斗流程[#self.战斗流程].挨打方[n].伤害 =self.战斗流程[#self.战斗流程].挨打方[n].伤害 *1.5
    end
    self.战斗流程[#self.战斗流程].挨打方[n].法术暴击=false
    if self.法术暴击 then
      if 取随机数() <= self.参战单位[编号].法暴 + (self.参战单位[编号].法术暴击等级 - self.参战单位[self.临时目标[n]].抗法术暴击等级) / 10 then
        self.战斗流程[#self.战斗流程].挨打方[n].伤害 = self.战斗流程[#self.战斗流程].挨打方[n].伤害 * 1.5
        self.战斗流程[#self.战斗流程].挨打方[n].法术暴击=true
      end
      end
    self.战斗流程[#self.战斗流程].挨打方[n].类型 = "掉血"
    self.战斗流程[#self.战斗流程].挨打方[n].死亡 = 0
    self.临时伤害 = self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害, self.参战单位[编号], self.参战单位[self.临时目标[n]], 1, self.战斗流程[#self.战斗流程].挨打方[n].类型)
    self.战斗流程[#self.战斗流程].挨打方[n].伤害 = self.临时伤害.伤害
    self.战斗流程[#self.战斗流程].挨打方[n].类型 = self.临时伤害.类型
    self.战斗流程[#self.战斗流程].挨打方[n].死亡 = self.临时伤害.死亡
    if self.临时参数 == "落叶萧萧" and self:取经脉(self.参战单位[编号],"咒术") and 取随机数()<=100 then
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].有无=true
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].回合=4
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],"雾杀")
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].攻击方=self.参战单位[编号]
     self.参战单位[self.临时目标[n]].法术状态组["雾杀"].蚩尤元神=self.参战单位[编号].蚩尤元神
     self.参战单位[self.临时目标[n]].咒术 = 1
    end

  end
  if self.临时参数 == "落雷符" and self:取经脉(self.参战单位[编号],"飞符炼魂") and 取随机数()<=70 then
    self.参战单位[编号].命令数据.目标 = self.临时目标[取随机数(1,#self.临时目标)]
    self.参战单位[编号].命令数据.参数 = "失魂符"
    self:单体封印法术计算(编号,nil,"失魂符")
  end

end

function 战斗处理类:单体持续伤害法术计算(编号)

  if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then

    --二次刷新目标
    self.相关id=self.参战单位[编号].队伍id
    self.临时目标=self:取随机单位(编号,self.相关id,1,1)
    if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
    else
     self.参战单位[编号].命令数据.目标=self.临时目标[1]

      end




    end

 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0

  elseif self:技能消耗(self.参战单位[编号]) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0


   end
 self.临时参数=self.参战单位[编号].命令数据.参数
 self.战斗流程[#self.战斗流程+1]={流程=42,执行=false,结束=false,类型="技能",参数=self.临时参数,死亡=0}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
 self.等待结束=self.等待结束+5

 if self.战斗流程[#self.战斗流程].封印结果 then
     self.战斗流程[#self.战斗流程].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
     self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,"掉血",1)
     self.战斗流程[#self.战斗流程].伤害=self.临时伤害.伤害
     self.战斗流程[#self.战斗流程].死亡=self.临时伤害.死亡


 --if
 --if self.战斗流程[#self.战斗流程].封印结果 then

      self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数)
   end


  -- end

 end
function 战斗处理类:单体封印法术计算(编号,特技,经脉)

    if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then

      --二次刷新目标
      self.相关id=self.参战单位[编号].队伍id
      self.临时目标=self:取随机单位(编号,self.相关id,1,1)
      if self.临时目标==0 then
        self.参战单位[编号].命令数据.目标=0
      else
       self.参战单位[编号].命令数据.目标=self.临时目标[1]

        end
      end
  self.流程类型="技能"
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0

  elseif 特技==nil and 经脉 == nil and self:技能消耗(self.参战单位[编号]) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0

  elseif 特技~=nil then
    self.流程类型="特技"

    if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then

     self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
     return 0

      end


   end
 self.临时参数=self.参战单位[编号].命令数据.参数

 if self.临时参数=="后发制人" then
   self.临时目标=self.参战单位[编号].命令数据.目标
   self.参战单位[编号].命令数据.目标=编号

   end
 self.战斗流程[#self.战斗流程+1]={流程=39,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
  self.等待结束=self.等待结束+5
 if self.战斗流程[#self.战斗流程].封印结果 then
   self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,self.临时目标)
   if self:取经脉(self.参战单位[编号],"机巧") and(self.临时参数 == "如花解语" or self.临时参数 == "似玉生香" or self.临时参数 == "莲步轻舞") then
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].有无=true
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].回合=4
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].等级=self:取技能等级(self.参战单位[编号],self.临时参数)*3
      self.战斗流程[#self.战斗流程].毒=true
      self.参战单位[编号].封印命中等级 = self.参战单位[编号].封印命中等级 +10
      self.参战单位[编号].愤怒 = self.参战单位[编号].愤怒 +5
      if self.参战单位[编号].愤怒 >= 150 then
        self.参战单位[编号].愤怒 = 150
      end
   end
   if self:取经脉(self.参战单位[编号],"不灭") and 取随机数()<= 15  then
      self:单体恢复法术计算(编号,nil,1)
   end
  elseif self.临时参数 == "似玉生香" and self:取经脉(self.参战单位[编号],"碎玉弄影") then
      self:单体封印法术计算(编号)
  end
  if self:取经脉(self.参战单位[编号],"毒雾") and self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].有无 == false and (self.临时参数 == "如花解语" or self.临时参数 == "似玉生香" or self.临时参数 == "莲步轻舞") then
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].有无=true
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].回合=4
      self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"].等级=self:取技能等级(self.参战单位[编号],self.临时参数)*3
      self.战斗流程[#self.战斗流程].毒=true
  end
end
function 战斗处理类:单体封印法术计算1(编号,特技)

  if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then

    --二次刷新目标
    self.相关id=self.参战单位[编号].队伍id
    self.临时目标=self:取随机单位(编号,self.相关id,1,1)
    if self.临时目标==0 then
      self.参战单位[编号].命令数据.目标=0
    else
     self.参战单位[编号].命令数据.目标=self.临时目标[1]

      end




    end



  self.流程类型="技能"
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0

  elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0

  elseif 特技~=nil then
    self.流程类型="特技"

    if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then

     self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
     return 0

      end


   end
 self.临时参数=self.参战单位[编号].命令数据.参数


 self.战斗流程[#self.战斗流程+1]={流程=58,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
  self.等待结束=self.等待结束+5
 if self.战斗流程[#self.战斗流程].封印结果 then

   --self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,self.临时目标)
   self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)

   if self.临时参数=="勾魂" then
      self.参战单位[self.参战单位[编号].命令数据.目标].当前气血=self.参战单位[self.参战单位[编号].命令数据.目标].当前气血-self.战斗流程[#self.战斗流程].法术伤害

      if self.参战单位[self.参战单位[编号].命令数据.目标].当前气血<=0 then


        self.参战单位[self.参战单位[编号].命令数据.目标].当前气血=0
        self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])

      else
        self.战斗流程[#self.战斗流程].死亡=0


        end
     self.战斗流程[#self.战斗流程].恢复伤害=math.floor(self.战斗流程[#self.战斗流程].法术伤害*0.5+1)
     self.参战单位[编号].当前气血=self.参战单位[编号].当前气血+self.战斗流程[#self.战斗流程].恢复伤害
      if self.参战单位[编号].当前气血>self.参战单位[编号].气血上限 then

       self.参战单位[编号].当前气血=self.参战单位[编号].气血上限

        end

    elseif self.临时参数=="摄魄" then
     self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法-self.战斗流程[#self.战斗流程].法术伤害

      if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法<=0 then


        self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=0

       end
     self.战斗流程[#self.战斗流程].恢复伤害=math.floor(self.战斗流程[#self.战斗流程].法术伤害*0.5)
     self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+self.战斗流程[#self.战斗流程].恢复伤害
      self.战斗流程[#self.战斗流程].死亡=0
      if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then

       self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限

        end




      end


   end

 end
function 战斗处理类:修罗隐身计算(编号)


  if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then

    self:添加提示(self.参战单位[编号].数字id,"目标已死亡")
    return 0


    end

 if self.参战单位[编号]==nil then

   return 0

   end


 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0

  elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0



   end



 self.战斗流程[#self.战斗流程+1]={流程=110,执行=false,攻击方=编号,挨打方=self.参战单位[编号].命令数据.目标,结束=false,类型="技能",参数="修罗隐身"}
 self.等待结束=self.等待结束+5
 if self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["隐身"]==nil then self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["隐身"]={有无=false} end
 self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["隐身"].有无=true
 self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["隐身"].回合=math.floor(self:取技能等级(self.参战单位[编号],"隐身")/30)+1



 end
function 战斗处理类:单体恢复法术计算(编号,特技,经脉)
  if 经脉 == nil then
    if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
      self.相关id=self.参战单位[编号].队伍id
      self.临时目标=self:取随机单位(编号,self.相关id,2,1)
      if self.临时目标==0 then
        self.参战单位[编号].命令数据.目标=0
      else
        self.参战单位[编号].命令数据.目标=self.临时目标[1]
      end
    end
    self.流程类型="技能"
    self.临时参数=self.参战单位[编号].命令数据.参数
    if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
      return 0
    elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
      self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
      return 0
    elseif 特技~=nil then
      self.流程类型="特技"
      if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
        self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
      return 0
      end
    end

    self.等待结束=self.等待结束+3
    self.临时参数=self.参战单位[编号].命令数据.参数
    self.战斗流程[#self.战斗流程+1]={流程=46,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}--流程=30为从非物理技能开始
    self.战斗流程[#self.战斗流程].攻击方=编号
    self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
    self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
    self.战斗流程[#self.战斗流程].伤害类型="加血"
    self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.战斗流程[#self.战斗流程].伤害类型)
    self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
    self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
    self.战斗流程[#self.战斗流程].法术死亡=0

    if self.临时参数=="普渡众生" then

     self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数)

    elseif self.临时参数=="冰清诀" then

     self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])

    elseif self.临时参数=="水清诀" then

     self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])
    elseif self.临时参数=="凝气决" then

     self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限*0.1+150)
     if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法>self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限 then

       self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限
       end
    elseif self.临时参数=="凝神决" then

     self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限*0.15+250)
      if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法>self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限 then
        self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限
      end

    elseif self.临时参数=="解毒" then

     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒.有无=false
     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒.有无=false

    elseif self.临时参数=="清心" then

     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒.有无=false
     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒.有无=false
     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.雾杀.有无=false

    elseif self.临时参数=="驱尸" then

     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒.有无=false
     self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒.有无=false
    elseif self.临时参数=="舍身取义" then
     self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*0.95)
     self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*0.97)
       if self:取经脉(self.参战单位[编号],"感念") then
        self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],"舍身取义状态")
       end
    end
  elseif 经脉 == 1 then
    self.流程类型="技能"
    self.等待结束=self.等待结束+3
    self.临时参数="归元咒"
    self.战斗流程[#self.战斗流程+1]={流程=46,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}--流程=30为从非物理技能开始
    self.战斗流程[#self.战斗流程].攻击方=编号
    self.战斗流程[#self.战斗流程].挨打方=编号
    self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[编号],self.临时参数)
    self.战斗流程[#self.战斗流程].伤害类型="加血"
    self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[编号],1,self.战斗流程[#self.战斗流程].伤害类型)
    self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
    self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
    self.战斗流程[#self.战斗流程].法术死亡=0

  end

 end
function 战斗处理类:单体复活法术计算(编号)



 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标

   return 0
  elseif self:取存活状态(self.参战单位[编号].命令数据.目标) then

   self:添加提示(self.参战单位[编号].数字id,"该单位目前无法被复活")
   return 0
  elseif  self.参战单位[self.参战单位[编号].命令数据.目标].战斗类型~="角色" then
   self:添加提示(self.参战单位[编号].数字id,"该单位目前无法被复活")
   return 0

  elseif self:技能消耗(self.参战单位[编号]) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")

    return 0


   end
   self.等待结束=self.等待结束+3
   self.临时参数=self.参战单位[编号].命令数据.参数
   self.战斗流程[#self.战斗流程+1]={流程=55,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
   self.战斗流程[#self.战斗流程].攻击方=编号
   self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
   self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
   self.战斗流程[#self.战斗流程].伤害类型="加血"
   self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.战斗流程[#self.战斗流程].伤害类型)
   self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
   self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
   self.战斗流程[#self.战斗流程].法术死亡=0
   if self.临时参数 == "我佛慈悲" and self:取经脉(self.参战单位[编号],"佛性") then
    self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],"我佛慈悲状态")
   end

 end
function 战斗处理类:取起群体复活单位(编号,相关id,类型,数量) --1为敌 2为队友

  self.单位组={}

  for n=1,#self.参战单位 do
   --print(self:取存活状态(n))

     if self.参战单位[n].战斗类型=="角色" and  self.参战单位[n].队伍id==相关id and self:取存活状态(n)==false  then
       self.单位组[#self.单位组+1]=n
        end
    end

  --这里遍历一次单位组中符合条件的单位
  --table.print(self.单位组)



  if #self.单位组==0 then

   return 0

  else
   return self:取随机数组成员(self.单位组,数量)


    end



 end
function 战斗处理类:群体复活法术计算(编号)
  self.临时参数=self.参战单位[编号].命令数据.参数
 self.临时目标=self:取起群体复活单位(编号,self.参战单位[编号].队伍id,0,5)


 if self.临时目标==0 then --找不到可攻击的目标
    self:添加提示(self.参战单位[编号].数字id,"当前没有队友处于死亡状态")
   return 0


  elseif self:特技消耗(self.参战单位[编号],self.临时参数) ==false then

    self:添加提示(self.参战单位[编号].数字id,"你的当前愤怒不足以施放此技能")

    return 0


   end
   self.等待结束=self.等待结束+5
   self.临时参数=self.参战单位[编号].命令数据.参数
   self.战斗流程[#self.战斗流程+1]={流程=112,执行=false,结束=false,类型="特技",参数=self.临时参数}--流程=30为从非物理技能开始
   self.战斗流程[#self.战斗流程].攻击方=编号
   self.战斗流程[#self.战斗流程].挨打方={}
   self.参战单位[编号].当前魔法=0
   self.气血伤害值=math.floor(self.参战单位[编号].气血上限*0.1)
   if self.参战单位[编号].当前气血<=self.气血伤害值 then

      self.战斗流程[#self.战斗流程].消耗伤害=0


    else

      self.战斗流程[#self.战斗流程].消耗伤害=self.参战单位[编号].当前气血-self.气血伤害值
      self.参战单位[编号].当前气血=self.气血伤害值



     end
   --self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
   --self.战斗流程[#self.战斗流程].伤害类型="加血"
   --self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.战斗流程[#self.战斗流程].伤害类型)
   --self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
   --self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
  -- self.战斗流程[#self.战斗流程].法术死亡=0

   for n=1,#self.临时目标 do

      self.战斗流程[#self.战斗流程].挨打方[n]={}
      self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
      self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数)
      self.参战单位[self.临时目标[n]].当前气血=self.战斗流程[#self.战斗流程].挨打方[n].伤害






     end




 end
function 战斗处理类:单体法术计算(编号,特技,特定,目标)
  self.临时目标 = self.参战单位[编号].命令数据.目标

  if 目标 ~= nil then
    self.临时目标 = 目标
  end

  if self.临时目标 == 0 or self:取存活状态(self.临时目标) == false or self:取隐身判断(编号, self.临时目标) == false then
    if 目标 ~= nil then
      return 0
    end

    self.相关id = self.参战单位[编号].队伍id
    self.临时目标1 = self:取随机单位(编号, self.相关id, 1, 1)

    if self.临时目标1 == 0 then
      self.临时目标 = 0
    else
      self.临时目标 = self.临时目标1[1]
    end
  end

  self.流程类型 = "技能"

  if self.临时目标 == 0 then
    return 0
  elseif 特定 == nil and 特技 == nil and self:技能消耗(self.参战单位[编号]) == false then
    self:添加提示(self.参战单位[编号].数字id, "你的当前魔法不足以施放此技能")

    return 0
  elseif 特技 ~= nil then
    self.流程类型 = "特技"

    if self:特技消耗(self.参战单位[编号], self.临时参数) == false then
      self:添加提示(self.参战单位[编号].数字id, "你没那么多的愤怒")

      return 0
    end
  end

  if 特定 == nil then
    self.临时参数 = self.参战单位[编号].命令数据.参数
  else
    self.临时参数 = 特定
  end

  self.战斗流程[#self.战斗流程 + 1] = {
    执行 = false,
    结束 = false,
    流程 = 30,
    类型 = self.流程类型,
    参数 = self.临时参数
  }
  self.战斗流程[#self.战斗流程].攻击方 = 编号
  self.战斗流程[#self.战斗流程].挨打方 = self.临时目标
  self.战斗流程[#self.战斗流程].法术伤害 = self:取技能伤害(self.参战单位[编号], self.参战单位[self.临时目标], self.临时参数)
    self.战斗流程[#self.战斗流程].法术暴击=false
  if self.法术暴击 then
      if 取随机数() <= self.参战单位[编号].法暴 + (self.参战单位[编号].法术暴击等级 - self.参战单位[self.临时目标].抗法术暴击等级) / 10 then
        self.战斗流程[#self.战斗流程].法术伤害 = self.战斗流程[#self.战斗流程].法术伤害* 1.5
        self.战斗流程[#self.战斗流程].法术暴击=true
      end
      end
  self.战斗流程[#self.战斗流程].伤害类型 = "掉血"
  self.战斗流程[#self.战斗流程].法术死亡 = 0
  self.等待结束 = self.等待结束 + 5

  if self.临时参数 == "水攻" then
    if 取随机数() <= self.参战单位[self.临时目标].水吸 or self.参战单位[self.临时目标].法术状态组.颠倒五行.有无 and 取随机数(1, 1000) <= self.参战单位[self.临时目标].法术状态组.颠倒五行.几率 then
      self.战斗流程[#self.战斗流程].法术伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 0.25)

      if self.战斗流程[#self.战斗流程].法术伤害 <= 1 then
        self.战斗流程[#self.战斗流程].法术伤害 = 1
      end

      self.战斗流程[#self.战斗流程].伤害类型 = "加血"
    end
  elseif self.临时参数 == "烈火" then
    if 取随机数() <= self.参战单位[self.临时目标].火吸 or self.参战单位[self.临时目标].法术状态组.颠倒五行.有无 and 取随机数(1, 1000) <= self.参战单位[self.临时目标].法术状态组.颠倒五行.几率 then
      self.战斗流程[#self.战斗流程].法术伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 0.25)

      if self.战斗流程[#self.战斗流程].法术伤害 <= 1 then
        self.战斗流程[#self.战斗流程].法术伤害 = 1
      end

      self.战斗流程[#self.战斗流程].伤害类型 = "加血"
    end
  elseif self.临时参数 == "落岩" then
    if 取随机数() <= self.参战单位[self.临时目标].土吸 or self.参战单位[self.临时目标].法术状态组.颠倒五行.有无 and 取随机数(1, 1000) <= self.参战单位[self.临时目标].法术状态组.颠倒五行.几率 then
      self.战斗流程[#self.战斗流程].法术伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 0.25)

      if self.战斗流程[#self.战斗流程].法术伤害 <= 1 then
        self.战斗流程[#self.战斗流程].法术伤害 = 1
      end

      self.战斗流程[#self.战斗流程].伤害类型 = "加血"
    end
  elseif self.临时参数 == "雷击" and (取随机数() <= self.参战单位[self.临时目标].雷吸 or self.参战单位[self.临时目标].法术状态组.颠倒五行.有无 and 取随机数(1, 1000) <= self.参战单位[self.临时目标].法术状态组.颠倒五行.几率) then
    self.战斗流程[#self.战斗流程].法术伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 0.25)

    if self.战斗流程[#self.战斗流程].法术伤害 <= 1 then
      self.战斗流程[#self.战斗流程].法术伤害 = 1
    end

    self.战斗流程[#self.战斗流程].伤害类型 = "加血"
  end

  if self.临时参数 == "力劈华山" or self.临时参数 == "满天花雨" then
    self.保护 = self:取保护单位(self.临时目标)
    self.战斗流程[#self.战斗流程].流程 = 64
    self.等待结束 = self.等待结束 + 3
    self.必杀 = self:取是否必杀(self.参战单位[编号], self.参战单位[self.临时目标])

    if self.必杀 then
      if self:取经脉(self.参战单位[编号],"杀意") then
        self.临时伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 1.7)
      else
        self.临时伤害 = math.floor(self.战斗流程[#self.战斗流程].法术伤害 * 1.5)
      end
      self.战斗流程[#self.战斗流程].必杀动作 = "必杀"
    end

    if self.保护 == false then
      self.临时伤害 = self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害, self.参战单位[编号], self.参战单位[self.临时目标], 1, self.战斗流程[#self.战斗流程].伤害类型)
      self.战斗流程[#self.战斗流程].法术伤害 = self.临时伤害.伤害
      self.战斗流程[#self.战斗流程].伤害类型 = self.临时伤害.类型
      self.战斗流程[#self.战斗流程].法术死亡 = self.临时伤害.死亡
    else
      self.临时伤害1 = self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害, self.参战单位[编号], self.参战单位[self.临时目标], 0.3, self.战斗流程[#self.战斗流程].伤害类型)
      self.战斗流程[#self.战斗流程].法术伤害 = self.临时伤害1.伤害
      self.战斗流程[#self.战斗流程].伤害类型 = self.临时伤害1.类型
      self.战斗流程[#self.战斗流程].法术死亡 = self.临时伤害1.死亡
      self.临时伤害1 = self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害, self.参战单位[编号], self.参战单位[self.保护], 0.7, self.战斗流程[#self.战斗流程].伤害类型)
      self.战斗流程[#self.战斗流程].保护数据 = {
        编号 = self.保护,
        伤害 = self.临时伤害1.伤害,
        类型 = self.临时伤害1.类型,
        死亡 = self.临时伤害1.死亡
      }
      self.等待结束 = self.等待结束 + 3
    end
  else
    self.临时伤害 = self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害, self.参战单位[编号], self.参战单位[self.临时目标], 1, self.战斗流程[#self.战斗流程].伤害类型)
    self.战斗流程[#self.战斗流程].法术伤害 = self.临时伤害.伤害
    self.战斗流程[#self.战斗流程].伤害类型 = self.临时伤害.类型
    self.战斗流程[#self.战斗流程].法术死亡 = self.临时伤害.死亡
  end
end
function 战斗处理类:解除门派封印法术(门派,攻击方)

  if 门派=="女儿村" then

   self.解除名称={"如花解语","似玉生香","莲步轻舞"}
  elseif 门派=="方寸山" then

   self.解除名称={"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符"}
  elseif 门派=="天宫" then

   self.解除名称={"百万神兵","镇妖"}

 elseif 门派=="无底洞" then

   self.解除名称={"夺魄令","煞气诀"}

    end

  for n=1,#self.解除名称 do

   if 攻击方.法术状态组[self.解除名称[n]].有无 then
     攻击方.法术状态组[self.解除名称[n]].有无=false
     self:解除封印状态(攻击方,self.解除名称[n])

     end


   end
 end
function 战斗处理类:解除异常状态(攻击方)

  self.异常状态名称={"错乱","含情脉脉","尸腐毒","如花解语","似玉生香","莲步轻舞","离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符","百万神兵","镇妖","日月乾坤","紧箍咒","雾杀","夺魄令","煞气诀"}


 for n=1,#self.异常状态名称 do

   if 攻击方.法术状态组[self.异常状态名称[n]].有无 then
     攻击方.法术状态组[self.异常状态名称[n]].有无=false
     self:解除封印状态(攻击方,self.异常状态名称[n])

     end


   end


 end
function 战斗处理类:催眠状态解除(类型,挨打方)

  if 类型=="掉血" and 挨打方.法术状态组.催眠符.有无 then

   挨打方.法术状态组.催眠符.有无=false
   挨打方.法术状态组.催眠符.回合=0
   self:解除封印状态(挨打方,"催眠符")

    end


  if 类型=="掉血" and 挨打方.法术状态组.煞气诀.有无 then

   挨打方.法术状态组.煞气诀.有无=false
   挨打方.法术状态组.煞气诀.回合=0
   self:解除封印状态(挨打方,"煞气诀")

    end



 end
function 战斗处理类:解除封印状态(攻击方,技能名称)
 -- print("这里是重置战斗属性。。。。。。2222222222")
 -- 攻击方.法术状态组[技能名称].有无=false
 -- 攻击方.法术状态组[技能名称].回合=0

  if 技能名称=="含情脉脉" or 技能名称=="似玉生香" or 技能名称=="威慑"  or 技能名称=="象形" or 技能名称=="飞花摘叶"  then
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
   if 技能名称 == "含情脉脉" and 攻击方.法术状态组[技能名称].防御 ~= nil and self.参战单位[攻击方.法术状态组[技能名称].攻击方] ~= nil then
    self.参战单位[攻击方.法术状态组[技能名称].攻击方].防御 = self.参战单位[攻击方.法术状态组[技能名称].攻击方].防御 - 攻击方.法术状态组[技能名称].防御
   end

    if 技能名称 == "含情脉脉" and  攻击方.法术状态组[技能名称].灵力 ~= nil then
      攻击方.灵力 = 攻击方.灵力 + 攻击方.法术状态组[技能名称].灵力
    end

  elseif 技能名称=="鬼魂术" then
   攻击方.当前气血=攻击方.气血上限
  elseif 技能名称 == "混元伞" then
    攻击方.混元伞 = 0
  elseif 技能名称=="日月乾坤" then

   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
   攻击方.道具封印 = false
  elseif 技能名称=="百万神兵" then

   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="如花解语" then
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="我佛慈悲状态" then
   攻击方.法术状态组[技能名称].有无=false
   攻击方.法术状态组[技能名称].回合=0
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
  elseif 技能名称=="舍身取义状态" then
   攻击方.法术状态组[技能名称].有无=false
   攻击方.法术状态组[技能名称].回合=0
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
  elseif 技能名称=="莲步轻舞" then
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="楚楚可怜" then

   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="失忆符" or 技能名称=="镇妖" then

   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false

  elseif 技能名称=="失魂符" or 技能名称=="追魂符" or 技能名称=="夺魄令" or 技能名称=="错乱"  then
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
   if 技能名称 == "错乱" and 攻击方.法术状态组[技能名称].法防 ~= nil then
    攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
   end
   elseif 技能名称=="一笑倾城" then

   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   if 攻击方.法术状态组[技能名称].法防 ~= nil then
    攻击方.法防= 攻击方.法防 + 攻击方.法术状态组[技能名称].法防
   end
  elseif 技能名称=="碎甲符" then

   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
  elseif 技能名称=="灵动九天" then

   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力

   elseif 技能名称=="蜜润" then

   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力

  elseif 技能名称=="失心符" then

   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="安神诀" then
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
   if 攻击方.法术状态组[技能名称].历战 ~= nil then
    攻击方.法伤减免=攻击方.法伤减免+0.5
   end
  elseif 技能名称=="灵法" then
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
    攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
  elseif 技能名称=="灵断" then
   攻击方.伤害=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   elseif 技能名称=="怒吼" then
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
     elseif 技能名称=="御风" then
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
  elseif 技能名称=="灵刃" then
   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
 elseif 技能名称=="瞬法" then

 elseif 技能名称=="瞬击" then



  elseif 技能名称=="离魂符" then

   攻击方.躲闪=攻击方.躲闪+攻击方.法术状态组[技能名称].躲闪
   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
   elseif 技能名称=="一苇渡江" then

   攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度

 elseif 技能名称=="罗汉金钟" then

    --if 攻击方.法术状态组[技能名称].有无 then return 0 end
   --攻击方.法术状态组[技能名称].有无=true
   --攻击方.法术状态组[技能名称].回合=3
   攻击方.法伤减免=攻击方.法伤减免+0.5


  elseif 技能名称=="催眠符" then
   攻击方.封印开关=false
   elseif 技能名称=="煞气诀" then
   攻击方.封印开关=false
  elseif 技能名称=="盘丝阵" then

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
 elseif 技能名称=="定身符" then

   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   -- 攻击方.攻击封印=false
   -- 攻击方.封印开关=false


   攻击方.攻击封印=false
   攻击方.技能封印=false
   攻击方.特技封印=false
   攻击方.封印开关=false
  elseif 技能名称=="天神护体" then
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
   if 攻击方.法术状态组[技能名称].速度 ~= nil then
     攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
   end
  elseif 技能名称=="牛劲" then

   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   if 攻击方.法术状态组[技能名称].炙烤 ~= nil then
    攻击方.法术状态组[技能名称].炙烤 = 1
    攻击方.感知 = false
   end
  elseif 技能名称=="杀气决" then

   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

  elseif 技能名称=="金刚护体" then

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   if 攻击方.法术状态组[技能名称].诸天看护 ~= nil then
    攻击方.法伤减免=攻击方.法伤减免+0.5
    攻击方.物伤减免=攻击方.物伤减免+0.5
    攻击方.法术状态组[技能名称].诸天看护 = nil
   end
  elseif 技能名称=="金身舍利" then

  攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防


   elseif 技能名称=="明光宝烛" then

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御

   elseif 技能名称=="不动如山" then

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   if 攻击方.法术状态组[技能名称].伤害 ~= nil then
    攻击方.伤害 = 攻击方.伤害 - 攻击方.法术状态组[技能名称].伤害
   end
   elseif 技能名称=="法术防御" then

   攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防


  elseif 技能名称=="神龙摆尾" then

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
  elseif 技能名称=="乘风破浪" then

   攻击方.躲闪=攻击方.躲闪-攻击方.法术状态组[技能名称].躲闪
  elseif 技能名称=="金刚护法" then

   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
   if 攻击方.法术状态组[技能名称].防御 ~= nil then
         攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
    end
    elseif 技能名称=="镇魂诀" then

   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
   elseif 技能名称=="碎星诀" then

   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

  elseif 技能名称=="定心术" then

   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力

  elseif 技能名称=="变身" then

   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

  elseif 技能名称=="后发制人" then
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
  elseif 技能名称=="幽冥鬼眼" then
    if 攻击方.法术状态组[技能名称].魑魅缠身 ~= nil then
    攻击方.驱鬼 =0
   end
   elseif 技能名称=="瘴气" then
   攻击方.气血回复效果=攻击方.气血回复效果+攻击方.法术状态组[技能名称].气血回复效果

   elseif 技能名称=="横扫千军" then
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力

   elseif 技能名称=="血雨" then
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   elseif 技能名称 == "颠倒五行" then

    if 攻击方.法术状态组[技能名称].法连 ~= nil then
    攻击方.法连 = 攻击方.法连 - 20
   end
  elseif 技能名称=="天崩地裂" then
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力

   elseif 技能名称=="金刚镯"  then
   --攻击方.法术状态组[技能名称].有无=true
   --攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   --攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*2)
   if 攻击方.武器伤害~=nil then

     攻击方.武器伤害=攻击方.武器伤害+攻击方.法术状态组[技能名称].伤害

     end


   end




 end
function 战斗处理类:添加封印状态(攻击方,挨打方,技能名称,目标)
  --if 攻击方.法术状态组[技能名称] == nil then return end
 if 攻击方.法术状态组[技能名称].有无 and 技能名称~="分身术" then

   self:解除封印状态(攻击方,技能名称)

   end
  self.临时等级=self:取技能等级(挨打方,技能名称)
  --print(攻击方,挨打方,技能名称,目标)
  if 技能名称=="解封" then

    self:解除门派封印法术("女儿村",攻击方)
    return 0
   elseif 技能名称=="驱魔" then

    self:解除门派封印法术("方寸山",攻击方)
    return 0
     end

  if 技能名称=="含情脉脉" or 技能名称=="似玉生香" or 技能名称=="威慑" or 技能名称=="飞花摘叶" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   if self:取经脉(挨打方,"迷障") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御 + 攻击方.防御*0.04
    攻击方.法术状态组[技能名称].攻击方 = 挨打方.编号
    挨打方.防御 = 挨打方.防御 + 攻击方.法术状态组[技能名称].防御
   end
   if self:取经脉(挨打方,"意乱") then
    攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
    攻击方.灵力 = 攻击方.灵力 - 攻击方.法术状态组[技能名称].灵力
   end

   攻击方.攻击封印=true
   攻击方.技能封印=true
   攻击方.封印开关=true
   if 技能名称 == "似玉生香" and self:取经脉(挨打方,"嫣然") and 取随机数() <= 60 then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
   end
   elseif 技能名称=="火甲术"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3
   if self:取经脉(挨打方,"返火") then
    攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+2
   end
   elseif 技能名称=="我佛慈悲状态"  then
     攻击方.法术状态组[技能名称].有无=true
     攻击方.法术状态组[技能名称].回合=5
     攻击方.法术状态组[技能名称].防御=80
     攻击方.法术状态组[技能名称].法防=80
     攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
     攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
   elseif 技能名称=="舍身取义状态"  then
     攻击方.法术状态组[技能名称].有无=true
     攻击方.法术状态组[技能名称].回合=3
     攻击方.法术状态组[技能名称].防御=攻击方.防御*0.1
     攻击方.法术状态组[技能名称].法防=攻击方.法防*0.1
     攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
     攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
   elseif 技能名称=="分水"  then
     攻击方.法术状态组[技能名称].有无=true
     攻击方.法术状态组[技能名称].回合=3
   elseif 技能名称=="赤焰"  then
     攻击方.法术状态组[技能名称].有无=true
     攻击方.法术状态组[技能名称].回合=3
   elseif 技能名称=="神木宝鼎"  then
     攻击方.法术状态组[技能名称].有无=true
     攻击方.法术状态组[技能名称].回合=3

  elseif 技能名称=="炼气化神"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)
   攻击方.法术状态组[技能名称].魔法=math.floor(self.临时等级)
   攻击方.当前魔法=攻击方.当前魔法+攻击方.法术状态组[技能名称].魔法


   if 攻击方.当前魔法>攻击方.魔法上限 then

     攻击方.当前魔法=攻击方.魔法上限
     end
  elseif 技能名称 == "混元伞" then
    攻击方.法术状态组[技能名称].有无 = true
    攻击方.法术状态组[技能名称].回合 = 3

  elseif 技能名称=="魔息术"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
   攻击方.法术状态组[技能名称].魔法=math.floor(self.临时等级/3)
   if self:取经脉(攻击方,"魔息") then
    攻击方.法术状态组[技能名称].魔法 = 攻击方.法术状态组[技能名称].魔法 *1.5
   end
   攻击方.当前魔法=攻击方.当前魔法+攻击方.法术状态组[技能名称].魔法


   if 攻击方.当前魔法>攻击方.魔法上限 then

     攻击方.当前魔法=攻击方.魔法上限
     end


  elseif 技能名称=="金刚镯"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*2)
   if 攻击方.武器伤害~=nil then

     攻击方.武器伤害=攻击方.武器伤害-攻击方.法术状态组[技能名称].伤害

     end
  -- 攻击方.当前魔法=攻击方.当前魔法+攻击方.法术状态组[技能名称].魔法

  elseif 技能名称=="瘴气"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].气血回复效果=math.floor(self.临时等级*5)
   if self:取经脉(挨打方,"情劫") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合+3
   end

   攻击方.气血回复效果=攻击方.气血回复效果-攻击方.法术状态组[技能名称].气血回复效果



  elseif 技能名称=="佛法无边"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/40)
   攻击方.法术状态组[技能名称].几率=self.临时等级

  elseif 技能名称=="分身术"  then
   if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/30)
   if self:取经脉(挨打方,"化身") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
   end
   攻击方.法术状态组[技能名称].躲避=false
 elseif 技能名称=="灵动九天"  then
     if 攻击方.法术状态组[技能名称].有无 then
      return 0
     end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/40)
   攻击方.法术状态组[技能名称].灵力=self.临时等级
   if self:取经脉(挨打方,"化身") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +3
    攻击方.法术状态组[技能名称].灵力 = 攻击方.法术状态组[技能名称].灵力 +30
   end
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力

 elseif 技能名称=="蜜润"  then
     if 攻击方.法术状态组[技能名称].有无 then
      return 0
     end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/40)
   攻击方.法术状态组[技能名称].灵力=self.临时等级
   攻击方.灵力=攻击方.灵力+self.临时等级


  elseif 技能名称=="颠倒五行"  then
     if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].几率=self.临时等级
   if self:取经脉(挨打方,"波澜不惊") then
    攻击方.法术状态组[技能名称].法连 = 20
    攻击方.法连 = 攻击方.法连 + 20
   end

  elseif 技能名称=="乾坤妙法"  then
     if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1
  elseif 技能名称=="天地同寿"  then
     if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1
   if self:取经脉(挨打方,"同辉") then
    攻击方.法术状态组[技能名称].回合= 攻击方.法术状态组[技能名称].回合+1
   end
 elseif 技能名称=="象形"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2
   攻击方.攻击封印=true
   攻击方.技能封印=true
   攻击方.封印开关=true
  elseif 技能名称=="碎甲符" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
    if 取随机数()>35 then  攻击方.法术状态组[技能名称].有无=false return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/100)
   攻击方.防御=攻击方.防御-math.floor(self.临时等级)
   攻击方.法防=攻击方.法防-math.floor(self.临时等级*0.35)
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级)
   攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.35)
  elseif 技能名称=="野兽之力" then

    if 攻击方.法术状态组[技能名称].有无 == true then
      return 0
    end
   攻击方.法术状态组[技能名称].有无=true
  --  table.print(攻击方.法术状态组)
   攻击方.法术状态组[技能名称].回合=9999
   攻击方.伤害=攻击方.伤害+math.floor(攻击方.伤害*0.1)

  elseif 技能名称=="魔兽之印" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=9999
   攻击方.伤害=攻击方.伤害+math.floor(攻击方.伤害*0.05)
   if self:取经脉(挨打方,"余韵") then
    攻击方.伤害=攻击方.伤害+math.floor(攻击方.伤害*0.05)
   end
  elseif 技能名称=="光辉之甲" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=9999
   攻击方.防御=攻击方.防御+math.floor(攻击方.防御*0.1)

   elseif 技能名称=="安神诀" then
    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=4
   攻击方.法防=攻击方.法防+math.floor(self.临时等级*0.35)
   攻击方.灵力=攻击方.灵力+math.floor(self.临时等级*0.55)
   攻击方.法术状态组[技能名称].灵力= math.floor(self.临时等级*0.55)
   攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.35)

   if self:取经脉(攻击方,"历战") then
    攻击方.法伤减免=攻击方.法伤减免-0.5
    攻击方.法术状态组[技能名称].历战 = 1
   end
  elseif 技能名称=="圣灵之甲" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=9999
   攻击方.防御=攻击方.防御+math.floor(攻击方.防御*0.05)
      if self:取经脉(挨打方,"余韵") then
    攻击方.防御=攻击方.防御+math.floor(攻击方.防御*0.05)
   end
   elseif 技能名称=="神龙摆尾" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=4
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*2)
   if self:取经脉(挨打方,"傲翔") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御*1.1
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
   end
   攻击方.防御=攻击方.防御+math.floor(self.临时等级*2)
   elseif 技能名称=="一笑倾城" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*3)

   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   -- 空
   -- 攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
 elseif 技能名称=="太极护法" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=9999
   攻击方.法伤减免=攻击方.法伤减免-0.5



  elseif 技能名称=="罗汉金钟" then

    if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3
   攻击方.法伤减免=攻击方.法伤减免-0.5
   elseif 技能名称=="幽冥鬼眼" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   if self:取经脉(挨打方,"魑魅缠身") then
    攻击方.法术状态组[技能名称].魑魅缠身 = 1
    攻击方.驱鬼 =2
   end
  elseif 技能名称=="魔王回首" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
  elseif 技能名称=="极度疯狂" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)

  elseif 技能名称=="百毒不侵" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   if 攻击方.法术状态组["毒"].有无 then

     攻击方.法术状态组["毒"].有无=false
     攻击方.法术状态组["毒"].回合=0

     end


    if 攻击方.法术状态组["尸腐毒"].有无 then

     攻击方.法术状态组["尸腐毒"].有无=false
     攻击方.法术状态组["尸腐毒"].回合=0

     end
  elseif 技能名称=="宁心"  then
    攻击方.法术状态组[技能名称].有无=true
    攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
    self:解除门派封印法术("女儿村",攻击方)
  elseif 技能名称=="复苏"  then
    攻击方.法术状态组[技能名称].有无=true
    攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
    self:解除门派封印法术("天宫",攻击方)
  elseif 技能名称=="魔音摄魂"  then
    攻击方.法术状态组[技能名称].有无=true
    攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
  elseif 技能名称=="日月乾坤" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/35)
   if self:取经脉(挨打方,"归本") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
   end
   攻击方.攻击封印=true
   攻击方.技能封印=true
   攻击方.特技封印=true
   if self:取经脉(挨打方,"陌宝") then
    攻击方.道具封印 = true
  end
   攻击方.封印开关=true
  elseif 技能名称=="百万神兵" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
   攻击方.攻击封印=true
   攻击方.封印开关=true
  elseif 技能名称=="如花解语" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
   攻击方.攻击封印=true
   攻击方.封印开关=true
  elseif 技能名称=="莲步轻舞" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
   攻击方.技能封印=true
   攻击方.封印开关=true

  elseif 技能名称=="催眠符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
   if self:取经脉(挨打方,"黄粱") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +2
   end
   攻击方.封印开关=true
  elseif 技能名称=="煞气诀" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
   攻击方.封印开关=true


  elseif 技能名称=="失心符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
   攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
   攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.5)
   if self:取经脉(挨打方,"苦禅") then
    攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级*1.03)
    攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*1.03)
   end
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
   攻击方.技能封印=true
   攻击方.封印开关=true
 elseif 技能名称=="天神护体" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
   攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.5)
   if self:取经脉(挨打方,"神律") then
      攻击方.法术状态组[技能名称].法防=攻击方.法术状态组[技能名称].法防*2
      攻击方.法术状态组[技能名称].速度=self.临时等级*0.3
      攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
   end
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
   --print(攻击方.法术状态组[技能名称].回合)
  elseif 技能名称=="牛劲" then
   if 攻击方.法术状态组[技能名称].有无 then return 0 end
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
   if self:取经脉(挨打方,"威吓") then
    攻击方.法术状态组[技能名称].灵力 = 攻击方.法术状态组[技能名称].灵力 + 攻击方.灵力*0.05
   end
   if self:取经脉(挨打方,"炙烤") then
    攻击方.法术状态组[技能名称].炙烤 = 1
    攻击方.感知 = true
   end
   if self:取经脉(挨打方,"连营") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合+1
   end
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力


  elseif 技能名称=="楚楚可怜" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/25)
   攻击方.技能封印=true
   攻击方.封印开关=true
  elseif 技能名称=="失忆符" or 技能名称=="镇妖" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].前置=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)
   攻击方.特技封印=true
   攻击方.封印开关=true
 elseif 技能名称=="盘丝阵" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
    if self:取经脉(挨打方,"结阵") then
     攻击方.法术状态组[技能名称].回合 =  攻击方.法术状态组[技能名称].回合 -1
     攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御*2
   end
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御

 elseif 技能名称=="失魂符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
   if self:取经脉(挨打方,"苦禅") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御*1.03
   end
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.技能封印=true
   攻击方.封印开关=true

 elseif 技能名称=="错乱" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   if self:取经脉(挨打方,"震慑") then
     攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*1.5)
     攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
   end
   if self:取经脉(挨打方,"画地为牢") then
    攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+2
   end
   攻击方.技能封印=true
   攻击方.封印开关=true


 elseif 技能名称=="夺魄令" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.技能封印=true
   攻击方.封印开关=true
   if self:取经脉(挨打方,"追魂") then
   攻击方.攻击封印 = true
    end
   if self:取经脉(挨打方,"噬魂") then
   攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
    end
  elseif 技能名称=="一苇渡江" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].速度=math.floor(self.临时等级)
   if self:取经脉(挨打方,"流刚") then
    攻击方.法术状态组[技能名称].速度 = 攻击方.法术状态组[技能名称].速度*1.2
   end
    攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
  elseif 技能名称=="追魂符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.攻击封印=true
   攻击方.封印开关=true
  elseif 技能名称=="离魂符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].躲闪=math.floor(self.临时等级)
   攻击方.躲闪=攻击方.躲闪-攻击方.法术状态组[技能名称].躲闪
   攻击方.技能封印=true
   攻击方.封印开关=true
 elseif 技能名称=="定身符" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
   攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级*0.75)
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*0.5)
   if self:取经脉(挨打方,"苦禅") then
    攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级*1.03)
    攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.03)
   end
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.攻击封印=true
   攻击方.封印开关=true
  elseif 技能名称=="尸腐毒" or 技能名称=="紧箍咒" or 技能名称=="雾杀"  then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)+2
   攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
   if 技能名称== "紧箍咒" and self:取经脉(挨打方,"默诵") then
      攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害 *2
      攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+1
   end
   攻击方.法术状态组[技能名称].攻击方=挨打方
   攻击方.法术状态组[技能名称].蚩尤元神=挨打方.蚩尤元神
   elseif 技能名称=="普渡众生" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
   if self:取经脉(挨打方,"劳心") and 挨打方.当前气血 <= 挨打方.气血上限*0.3 then
    攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害*2
   end
   if self:取经脉(挨打方,"普渡")  then
    攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害*1.2
   end
   if self:取经脉(挨打方,"甘露") then
    攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +1
   end
  elseif  技能名称=="生命之泉" then
     攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/30)
   攻击方.法术状态组[技能名称].伤害=(self:取技能等级(攻击方,技能名称)*2 +128)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果
   -- 攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
   -- if self:取经脉(挨打方,"体恤") and 攻击方.当前气血 <= 攻击方.气血上限*0.3 then
   --  攻击方.法术状态组[技能名称].伤害=攻击方.法术状态组[技能名称].伤害+150
   -- end
  elseif 技能名称=="杀气决" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=4
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害

  elseif 技能名称=="金刚护体" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=6
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级 * 1.5)+ 30
   if self:取经脉(挨打方,"流刚") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御*1.2
   end
   if self:取经脉(挨打方,"诸天看护") then
    攻击方.法术状态组[技能名称].诸天看护 = 1
    攻击方.法伤减免=攻击方.法伤减免-0.5
    攻击方.物伤减免=攻击方.物伤减免-0.5
   end
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御

  elseif 技能名称=="明光宝烛" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=4
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级 * 1.5) + 30
   if self:取经脉(挨打方,"由己渡人") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御*2
    挨打方.防御 = 挨打方.防御 - 挨打方.防御*0.02
   end
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御

  elseif 技能名称=="不动如山" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=4
   攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/25)+3
   if self:取经脉(挨打方,"聚气") then
    攻击方.法术状态组[技能名称].防御 = 攻击方.法术状态组[技能名称].防御 *1.5
   end
   if self:取经脉(挨打方,"强袭") then
    攻击方.法术状态组[技能名称].伤害 = 挨打方.伤害 *0.12
    攻击方.伤害 = 攻击方.伤害 + 攻击方.法术状态组[技能名称].伤害
   end

   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御


  elseif 技能名称=="法术防御" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=6

  elseif 技能名称=="乘风破浪" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
   攻击方.法术状态组[技能名称].躲闪=math.floor(self.临时等级*0.5)
   攻击方.躲闪=攻击方.躲闪+攻击方.法术状态组[技能名称].躲闪
  elseif 技能名称=="金刚护法" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=5
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
   if self:取经脉(挨打方,"佛屠") then
    攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害 + 挨打方.武器伤害*0.09
   end
   if self:取经脉(挨打方,"流刚") then
    攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害*1.2
   end
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
   if self:取经脉(挨打方,"映法") and 取随机数() <= 50 then
     攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
     攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   end
   elseif 技能名称=="镇魂诀" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=5
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
    if self:取经脉(挨打方,"斩魔") then
     攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害*1.12
   end
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
    if self:取经脉(挨打方,"战诀") and 攻击方.战诀 == nil then
     攻击方.必杀 =攻击方.必杀 + 10
     攻击方.战诀 = 1
   end
    if self:取经脉(挨打方,"神诀") then
     攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +2
   end
  elseif 技能名称=="碎星诀" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
    if self:取经脉(挨打方,"斩魔") then
     攻击方.法术状态组[技能名称].伤害 = 攻击方.法术状态组[技能名称].伤害*1.12
   end
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
    if self:取经脉(挨打方,"战诀") and 攻击方.战诀 == nil then
     攻击方.必杀 =攻击方.必杀 + 10
     攻击方.战诀 = 1
   end
    if self:取经脉(挨打方,"神诀") then
     攻击方.法术状态组[技能名称].回合 = 攻击方.法术状态组[技能名称].回合 +2
   end
  elseif 技能名称=="定心术" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
   攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力

  elseif 技能名称=="变身" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
   攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*0.7)
   if self:取经脉(挨打方,"宁息")then
    攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+2
    攻击方.法术状态组[技能名称].伤害=攻击方.法术状态组[技能名称].伤害*1.03
   end
   if self:取经脉(挨打方,"兽王") then
    攻击方.法术状态组[技能名称].伤害=攻击方.法术状态组[技能名称].伤害+self.临时等级*2
   end
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
  elseif 技能名称=="后发制人" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2
   攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.5)
   攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
   攻击方.法术状态组[技能名称].速度=math.floor(攻击方.速度*0.5)
   攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
   攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.5)
   攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
   攻击方.法术状态组[技能名称].伤害=math.floor(攻击方.伤害*0.5)
   攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
   攻击方.法术状态组[技能名称].目标=目标
  elseif 技能名称=="横扫千军" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2
   攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.35)
   if self:取经脉(攻击方,"静岳") then
    攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.2)
   end
   攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
   攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.35)
   攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力

  elseif 技能名称=="天崩地裂" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.35)
   攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.35)

 elseif 技能名称=="破釜沉舟" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2

 elseif 技能名称=="血雨" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2


  elseif 技能名称=="鹰击" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2

 elseif 技能名称=="连环击" then
   攻击方.法术状态组[技能名称].有无=true
   攻击方.法术状态组[技能名称].回合=2

   end
 end
function 战斗处理类:添加提示(id,内容)

  self.战斗流程.信息提示[#self.战斗流程.信息提示+1]={id=id,内容="#y/"..内容}
 end
function 战斗处理类:取技能伤害(攻击方,挨打方,技能名称,人数)---------------------------------------完成
  self.临时伤害=0
  self.法术暴击=false
  self.法伤附加=false
  self.天地同寿=false
 if 技能名称=="水攻" or 技能名称=="落岩" or 技能名称=="烈火" or 技能名称=="雷击" then
   self.临时伤害=self:取技能等级(攻击方,技能名称) * 3 + self:取灵力差(攻击方,挨打方) * 1.2
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="龙腾" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+28.5)*(取随机数(99,101)/100) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if self:取经脉(攻击方,"波涛") then
    self.临时伤害 = self.临时伤害 +self:取技能等级(攻击方,技能名称)*1.5
   end
   if self:取经脉(攻击方,"推意") and self.临时伤害 >= 挨打方.气血上限*0.2 then
    挨打方.当前魔法 = 挨打方.当前魔法 -300
    if 挨打方.当前魔法 <= 0 then
      挨打方.当前魔法 = 0
     end
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="荆棘舞" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+28.5)*(取随机数(99,105)/101) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if self:取经脉(攻击方,"鞭挞") and 攻击方.风灵 ~= nil then
    self.临时伤害 = self.临时伤害 + self:取技能等级(攻击方,技能名称)*攻击方.风灵*0.3
   end
    if self:取经脉(攻击方,"蔓延")  then
    self.临时伤害 = self.临时伤害*1.2
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="尘土刃" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+28.5)*(取随机数(99,101)/100) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
     elseif 技能名称=="冰川怒" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+28.5)*(取随机数(99,101)/100) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if self:取经脉(攻击方,"冰锥")  then
    self.临时伤害 = self.临时伤害 + 200
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="二龙戏珠" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+28.5)*(取随机数(99,101)/100) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
    if self:取经脉(攻击方,"汹涌")  then
    self.临时伤害 = self.临时伤害 + (1-挨打方.当前气血/挨打方.气血上限)*1000
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="三昧真火" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.3+攻击方.武器伤害*0.515+28.5)*(取随机数(99,101)/100) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if 攻击方.蚩尤元神==1 then
      self.临时伤害=self.临时伤害*(取随机数(90,150)/100)
     end
     if self:取经脉(攻击方,"魔冥")  then
    攻击方.当前魔法 = 攻击方.当前魔法 + 攻击方.魔法上限*0.05
    if 攻击方.当前魔法 >  攻击方.魔法上限 then
      攻击方.当前魔法  = 攻击方.魔法上限
    end
   end
     if self:取经脉(攻击方,"神炎")  then
    self.临时伤害 = self.临时伤害 + (1-攻击方.当前气血/攻击方.气血上限)*1000
   end
  if self:取经脉(攻击方,"魔焰滔天") then
    self.临时伤害=self.临时伤害*(取随机数(50,200)/100)
  end
     if self:取经脉(攻击方,"崩摧")  then
    self.临时伤害 = self.临时伤害 *1.05
   end
     if self:取经脉(攻击方,"魔心") and 取随机数() <= 10 then
    self.临时伤害 = self.临时伤害 *2
   end

  self.法术暴击=true
  self.法伤附加=true
  self.天地同寿=true
  elseif 技能名称=="归元咒" then
   self.临时伤害=self:取技能等级(攻击方,技能名称) * 3 + 32
   if self:取经脉(攻击方,"吐纳") then
      self.临时伤害 = self.临时伤害 + 攻击方.气血上限*0.1
   end
 elseif 技能名称=="天魔解体" then
   if 挨打方.队伍==0 or 挨打方.队伍==nil then
     self.临时伤害=1
    else
      if self:取技能等级(攻击方,技能名称)+5>=挨打方.等级 and 取随机数()<=30 then
       self.临时伤害=(攻击方.当前气血*0.85)
      else
       self.临时伤害=1
        end
     end
  elseif 技能名称=="五雷轰顶" then

   self.临时伤害=挨打方.当前气血*(取随机数(5,35)/100)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if 攻击方.蚩尤元神==1 then

     self.临时伤害=挨打方.当前气血*(取随机数(10,40)/100)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)

     end

   -- if self.临时伤害>攻击方.等级*攻击方.等级*0.5 then
   --    self.临时伤害=攻击方.等级*攻击方.等级*0.75
   --   end
     if self:取经脉(攻击方,"威吓") then
      self.临时伤害 = self.临时伤害 *1.08
     end
     if self:取经脉(攻击方,"轰鸣") then
      self.临时伤害 = self.临时伤害 +挨打方.防御*0.5
     end
     self.法伤附加=true
     self.天地同寿=true
  elseif 技能名称=="五雷咒" then

   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)*1.3+攻击方.武器伤害*0.515+22.5)--*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   self.法术暴击=true
   self.法伤附加=true
    if 挨打方.队伍==0 then
      self.临时伤害=self.临时伤害*2
     end
     self.天地同寿=true
  elseif 技能名称=="落雷符" then

   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(10-人数)/10 --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
   if 挨打方.队伍==0 then
      self.临时伤害=self.临时伤害*2
      if 攻击方.石破天惊~=0 then
          self.临时伤害=self.临时伤害+石破天惊
      end
     end
    if self:取经脉(攻击方,"雷动") then
      self.临时伤害=self.临时伤害+100
    end
    if self:取经脉(攻击方,"奔雷") then
      self.临时伤害=self.临时伤害*1.1
    end
     if self:取经脉(攻击方,"鬼怮") and 挨打方.复活~=nil and 挨打方.复活~=0 then
      self.临时伤害=self.临时伤害*1.3
    end
      if self:取经脉(攻击方,"钟馗论道") and 挨打方.复活~=nil and 挨打方.复活~=0 then
      self.临时伤害=self.临时伤害*2
    end
  -- self.临时伤害=self.临时伤害*self:取五行克制(攻击方,挨打方)*self:取驱鬼伤害(攻击方,挨打方)*(取随机数(90,105)/100)
  elseif 技能名称=="判官令" then
    self.天地同寿=true
   if 挨打方.队伍==0 then
     self.临时伤害=(self:取技能等级(攻击方,技能名称) * 6 + 64)+攻击方.固定伤害
    else
     self.临时伤害=(self:取技能等级(攻击方,技能名称)* 5 + 32)+攻击方.固定伤害
     end
    if self:取经脉(攻击方,"判官") then
      self.临时伤害=self.临时伤害*1.1
    end
      if self:取经脉(攻击方,"毒印") and 挨打方.法术状态组.尸腐毒.有无 then
        self.临时伤害 = self.临时伤害 *2
      end
    挨打方.当前魔法=挨打方.当前魔法-math.floor(self:取技能等级(攻击方,技能名称) *  1.5+ 64)
    if 挨打方.当前魔法<0 then 挨打方.当前魔法=0 end

  elseif 技能名称=="血雨" then
     self.临时伤害=(self:取技能等级(攻击方,技能名称)*3.5+75)+攻击方.固定伤害+(攻击方.当前气血*0.2)
  elseif 技能名称=="勾魂" then
    self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+挨打方.当前气血*0.05)
    if self:取经脉(攻击方,"倾情") then
      self.临时伤害=self.临时伤害*1.1
    end

    self.法伤附加=true
    self.天地同寿=true

  elseif 技能名称=="飞花摘叶" then
    if self:取玩家战斗() then

     self.临时伤害=1
    else
     self.临时伤害=math.floor(挨打方.当前气血*0.2)
      end
    if self:取经脉(攻击方,"鸿渐于陆") then
      self.临时伤害 = self.临时伤害 + 攻击方.武器伤害*1.3
    end
      self.天地同寿=true

  elseif 技能名称=="摄魄" then
    self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+挨打方.当前魔法*0.05)

  elseif 技能名称=="天罗地网" then

   self.临时伤害=(self:取技能等级(攻击方,技能名称)* 3 +攻击方.伤害 / 5)+攻击方.固定伤害
   self.天地同寿=true
   if 挨打方.队伍==0 then
     self.临时伤害=self.临时伤害 * 1.2
        if 攻击方.网罗乾坤 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.网罗乾坤
        end
     end
    if self:取经脉(攻击方,"落花成泥") then
      self.临时伤害 = self.临时伤害*1.15
    end
    if self:取经脉(攻击方,"粘附") then
      self.临时伤害 = self.临时伤害+攻击方.武器伤害*0.1--8
    end
    if self:取经脉(攻击方,"利刃") and 取随机数() <= 50 then
      self.临时伤害 = self.临时伤害*2
    end
  -- elseif 技能名称=="苍茫树" or 技能名称=="地裂火" or 技能名称=="靛沧海" or 技能名称=="日光华"  or 技能名称=="夺命咒"  or 技能名称=="雨落寒沙" then
  --   if 挨打方.队伍==0 then
  --    self.临时伤害=self:取技能等级(攻击方,技能名称) * 6 + 攻击方.固定伤害
  --      if 攻击方.行云流水 ~=0 then
  --          self.临时伤害=self.临时伤害+攻击方.行云流水
  --       end
  --   else
  --     self.临时伤害=self:取技能等级(攻击方,技能名称)* 4.5 + 64 + 攻击方.固定伤害
  --    end
  --    if 技能名称 == "雨落寒沙" and self:取经脉(攻击方,"杏花") and 取随机数()<= 30 then
  --      self.临时伤害 = self.临时伤害 +math.floor(挨打方.当前气血*0.05)
  --    end
  --    if 技能名称 ~= "雨落寒沙" and 技能名称 ~= "夺命咒" and self:取经脉(攻击方,"秘术") then

  --     self.临时伤害 = self.临时伤害 +150
  --    end
  --    if 技能名称 ~= "雨落寒沙" and 技能名称 ~= "夺命咒" and self:取经脉(攻击方,"莲心剑意") then

  --     self.临时伤害 = self.临时伤害 +self:取灵力差(攻击方,挨打方)*0.45
  --    end
  --    if 技能名称 ~= "雨落寒沙" and 技能名称 ~= "夺命咒" and self:取经脉(攻击方,"法咒") and 取随机数()<=50 then

  --     self.临时伤害 = self.临时伤害 *1.25
  --    end
  --    if 技能名称 ~= "雨落寒沙" and 技能名称 ~= "夺命咒" and self:取经脉(攻击方,"五行制化") then

  --     self.临时伤害 = self.临时伤害 *1.13
  --    end

  --    if 技能名称 == "夺命咒" and self:取经脉(攻击方,"内伤") then

  --     self.临时伤害 = self.临时伤害 +50+self:取技能等级(攻击方,技能名称)*1.5
  --    end
  --      if 技能名称 == "夺命咒" and self:取经脉(攻击方,"陷阱") then

  --     self.临时伤害 = self.临时伤害 *2
  --    end
  --      if 技能名称 == "夺命咒" and self:取经脉(攻击方,"妖风四起") then

  --     self.临时伤害 = self.临时伤害 +挨打方.当前气血*0.01
  --    end
  -- -- self.临时伤害=self.临时伤害*self:取五行克制(攻击方,挨打方)*self:取驱鬼伤害(攻击方,挨打方)*(取随机数(150,200)/100)
  --     self.天地同寿=true
  elseif 技能名称=="苍茫树" or 技能名称=="地裂火" or 技能名称=="靛沧海" or 技能名称=="日光华" then
    if 挨打方.队伍==0 then
     self.临时伤害=self:取技能等级(攻击方,技能名称) * 6 + 攻击方.固定伤害
       if 攻击方.行云流水 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.行云流水
        end
    else
      self.临时伤害=self:取技能等级(攻击方,技能名称)* 4.5 + 64 + 攻击方.固定伤害
     end
     if self:取经脉(攻击方,"五行制化") then

      self.临时伤害 = self.临时伤害 *0.7 --*1.13
     end
     if self:取经脉(攻击方,"法咒") and 取随机数() <= 50 then

      self.临时伤害 = self.临时伤害 *1.25
     end
     if self:取经脉(攻击方,"莲心剑意") then

      self.临时伤害 = self.临时伤害 + self:取灵力差(攻击方,挨打方) * 0.45
     end
     if self:取经脉(攻击方,"秘术") then

      self.临时伤害 = self.临时伤害 + 150
     end
     self.临时伤害 = self.临时伤害 *(24-人数)/20
      self.天地同寿=true
  elseif 技能名称=="夺命咒"  or 技能名称 == "雨落寒沙" then
    if 挨打方.队伍==0 then
     self.临时伤害=self:取技能等级(攻击方,技能名称) * 6 + 攻击方.固定伤害
       if 攻击方.行云流水 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.行云流水
        end
    else
      self.临时伤害=self:取技能等级(攻击方,技能名称)* 4.5 + 64 + 攻击方.固定伤害
     end
     if 技能名称 == "雨落寒沙" and self:取经脉(攻击方,"杏花") and 取随机数()<= 30 then
       self.临时伤害 = self.临时伤害 +math.floor(挨打方.当前气血*0.05)
     end

     if 技能名称 == "夺命咒" and self:取经脉(攻击方,"内伤") then

      self.临时伤害 = self.临时伤害 +50+self:取技能等级(攻击方,技能名称)*1.5
     end
       if 技能名称 == "夺命咒" and self:取经脉(攻击方,"陷阱") then

      self.临时伤害 = self.临时伤害 *2
     end
       if 技能名称 == "夺命咒" and self:取经脉(攻击方,"妖风四起") then

      self.临时伤害 = self.临时伤害 +挨打方.当前气血*0.01
     end
      self.天地同寿=true
  elseif 技能名称=="唧唧歪歪" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.2+攻击方.武器伤害*0.515+22.5)*(10-人数)/10
   if 攻击方.福泽天下 ~=0 then
     self.临时伤害=self.临时伤害+攻击方.福泽天下
   end
   if self:取经脉(攻击方,"销武") then
     self.临时伤害=self.临时伤害+攻击方.等级
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  -- self.临时伤害=self.临时伤害*self:取五行克制(攻击方,挨打方)*self:取驱鬼伤害(攻击方,挨打方)*(取随机数(90,105)/100)
  elseif 技能名称=="阎罗令" then
   if 挨打方.队伍==0 then
     self.临时伤害=(self:取技能等级(攻击方,技能名称)* 6 +64)*(20-人数)/20+攻击方.固定伤害
      if 攻击方.索命无常 ~=0 then
         self.临时伤害=self.临时伤害+攻击方.索命无常
      end
      if self:取经脉(攻击方,"伤魂") and 挨打方.法术状态组.尸腐毒.有无 then
        self.临时伤害 = self.临时伤害 *1.1
      end
      if self:取经脉(攻击方,"百爪狂杀")  then
        self.临时伤害 = self.临时伤害 + self:取物理伤害(攻击方,挨打方)/15
      end
    else
      self.临时伤害=(self:取技能等级(攻击方,技能名称)* 4 + 128)*(20-人数)/20+攻击方.固定伤害
    end
    self.天地同寿=true

  elseif 技能名称=="泰山压顶" or 技能名称=="水漫金山" or 技能名称=="地狱烈火" or 技能名称=="奔雷咒" then

   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3 + self:取灵力差(攻击方,挨打方)*1.3*(10-人数)/10)
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="九天玄火"  then

   self.临时伤害=(self:取技能等级(攻击方,技能名称)*4+self:取灵力差(攻击方,挨打方)+攻击方.魔力*0.35)--*0.45
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true

  elseif 技能名称=="龙卷雨击" then
    self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(1-人数*0.028)*(取随机数(99,102)/100)
    if self:取经脉(攻击方,"逐浪") then
    self.临时伤害 = self.临时伤害 +self:取技能等级(攻击方,技能名称)*1.5
   end
     if self:取经脉(攻击方,"云霄") then
    self.临时伤害 = self.临时伤害 +100
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
  elseif 技能名称=="雷霆万钧" then
    self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.3+攻击方.武器伤害*0.515+22.5)*(1-人数*0.028)*(取随机数(99,101)/100)
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true
     if 攻击方.天雷地火 ~=0 then
     self.临时伤害=self.临时伤害+攻击方.天雷地火
  end
  if self:取经脉(攻击方,"电芒") then
    self.临时伤害=self.临时伤害*1.1
  end
  if self:取经脉(攻击方,"风雷韵动") and 挨打方.愤怒 ~= nil then
    挨打方.愤怒 = 挨打方.愤怒 -15
    if 挨打方.愤怒 <=0 then
      挨打方.愤怒 = 0
    end
  end
  elseif 技能名称=="飞砂走石" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(1-人数*0.018)*(取随机数(99,101)/100)
  -- self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(10-人数)/10
  if self:取经脉(攻击方,"火神") then
    self.临时伤害=self.临时伤害+self:取技能等级(攻击方,技能名称)*1.5
  end
   if self:取经脉(攻击方,"魔冥")  then
    攻击方.当前魔法 = 攻击方.当前魔法 + 攻击方.魔法上限*0.05
    if 攻击方.当前魔法 >  攻击方.魔法上限 then
      攻击方.当前魔法  = 攻击方.魔法上限
    end
   end
  if self:取经脉(攻击方,"魔焰滔天") then
    self.临时伤害=self.临时伤害*(取随机数(50,200)/100)
  end
     if self:取经脉(攻击方,"神焰")  then
    self.临时伤害 = self.临时伤害 + (1-攻击方.当前气血/攻击方.气血上限)*1000
   end
     if self:取经脉(攻击方,"崩摧")  then
    self.临时伤害 = self.临时伤害 *1.05
   end
     if self:取经脉(攻击方,"魔心") and 取随机数() <= 10 then
    self.临时伤害 = self.临时伤害 *2
   end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true

  elseif 技能名称=="落叶萧萧" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(1-人数*0.018)*(取随机数(99,101)/100)
  -- self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.515+22.5)*(10-人数)/10
     if 攻击方.蚩尤元神==1 then
      self.临时伤害=self.临时伤害*(取随机数(90,150)/100)
     end
     if self:取经脉(攻击方,"灵压") then
       self.临时伤害 = self.临时伤害*1.08
     end
     if self:取经脉(攻击方,"风卷残云") then
       self.临时伤害 = self.临时伤害+100
     end
     if self:取经脉(攻击方,"灵能") and 取随机数() <= 30 then
       self.临时伤害 = self.临时伤害+攻击方.灵力/self:取技能等级(攻击方,技能名称)*5
     end
   if self:取经脉(攻击方,"凋零之歌") and 取随机数() <= 50 then
      self.临时伤害 = self.临时伤害 *1.5
     end
   self.法术暴击=true
   self.法伤附加=true
   self.天地同寿=true

  elseif 技能名称=="龙吟" then
    self.临时伤害=(self:取技能等级(攻击方,技能名称))+15
    self.天地同寿=true
    if self:取经脉(攻击方,"清吟") then
      self.临时伤害 = self.临时伤害 +100
    end
    挨打方.当前魔法=挨打方.当前魔法-math.floor(self.临时伤害*0.5)
    if 挨打方.当前魔法<0 then 挨打方.当前魔法=0 end
 elseif 技能名称=="夜舞倾城" then
    self.临时伤害=self:取物理伤害(攻击方,挨打方)*1.55
    self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="力劈华山" then
    self.临时伤害=self:取物理伤害(攻击方,挨打方)*1.55+挨打方.防御*0.75
    self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取是否必杀(攻击方,挨打方) then
      if self:取经脉(攻击方,"杀意") then
        self.临时伤害 = self.临时伤害*2.2
      else
        self.临时伤害=self.临时伤害*2
      end
    end
  elseif 技能名称=="后发制人"   then
    self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.85) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
    self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"勇武") then
      self.临时伤害 = self.临时伤害 +攻击方.防御*0.4
      攻击方.愤怒 = 攻击方.愤怒 +10
      if 攻击方.愤怒 >= 150 then
        攻击方.愤怒 = 150
      end
    end
  elseif 技能名称=="弱点击破" then
   self.临时伤害=math.floor(攻击方.伤害-挨打方.防御*0.35)
   if  self:取经脉(攻击方,"目空") then
    self.临时伤害 = self.临时伤害*1.1
   end
  elseif 技能名称=="破血狂攻" then
   self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*1.15)
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
   if  self:取经脉(攻击方,"目空") then
    self.临时伤害 = self.临时伤害*1.1
   end
  elseif 技能名称=="破碎无双" then
   self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*1.05)
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="善恶有报" then
   self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*2)
    self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="横扫千军"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.1*(1+人数*0.1)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)
   if self:取经脉(攻击方,"勇念") then
    self.临时伤害 = self.临时伤害*1.1
   end
  elseif 技能名称=="天崩地裂"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(0.45+人数*0.65)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)
   self.临时伤害=self.临时伤害*取随机数(90,105)/100
   if self:取经脉(攻击方,"山破") then
    self.临时伤害=self.临时伤害*1.05
   end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
  elseif 技能名称=="破釜沉舟"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.35)--*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)
   self.临时伤害=self.临时伤害*取随机数(90,105)/100
   if self:取经脉(攻击方,"破空") then
    self.临时伤害 = self.临时伤害 + 挨打方.防御*0.3
   end
  elseif 技能名称=="剑荡四方"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.5) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)
   self.临时伤害=self.临时伤害*取随机数(90,105)/100
  elseif 技能名称=="连环击"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15*((取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(1-人数*0.1)))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"乱破") and 取随机数() <= 45 then
      self.临时伤害 = self.临时伤害 +self:取技能等级(攻击方,技能名称)
    end
  elseif 技能名称=="烟雨剑法"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.05) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
              if 攻击方.烟雨飘摇 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.烟雨飘摇
        end
    if self:取经脉(攻击方,"雨杀") then
      self.临时伤害 = self.临时伤害 *1.1
    end
    if self.临时伤害 >= 挨打方.气血上限*0.1 and self:取经脉(攻击方,"致命") then
      self.临时伤害 = self.临时伤害 *1.1
    end
  elseif 技能名称=="断岳势"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
    if self:取经脉(攻击方,"再战") then
      self.临时伤害=self.临时伤害*1.1
    end
  elseif 技能名称=="腾雷"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"雷附") then
      self.临时伤害 = self.临时伤害 *1.1
    end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
   elseif 技能名称=="大闹天宫"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)+攻击方.力量*0.45)*1.25
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
   elseif 技能名称=="连击"   then
   self.临时伤害=self:取物理伤害(攻击方,挨打方)
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
   elseif 技能名称=="夺命蛛丝"   then
   if 挨打方.队伍==0 or 挨打方.队伍==nil then
     self.临时伤害=1
    else
      if 挨打方.战斗类型=="角色" then
       self.临时伤害=1
      else
       self.临时伤害=挨打方.气血上限
      end
     end
   elseif 技能名称=="飘渺式"   then
   if 挨打方.队伍==0 or 挨打方.队伍==nil then
      self.临时伤害=(self:取物理伤害(攻击方,挨打方)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
      self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
        if self:取经脉(攻击方,"傲视") then
          self.临时伤害= self.临时伤害*1.1
        end
        if self.临时伤害 >= 挨打方.气血上限*0.1 and self:取经脉(攻击方,"致命") then
          self.临时伤害 = self.临时伤害 *1.1
        end
        if 攻击方.烟雨飘摇 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.烟雨飘摇
        end
   else
      self.临时伤害=1
    end
   elseif 技能名称=="浪涌"   then
   if 挨打方.队伍==0 or 挨打方.队伍==nil then
      self.临时伤害=(self:取物理伤害(攻击方,挨打方) * 0.8) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
      self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
   else
      self.临时伤害=1
    end
    if self:取经脉(攻击方,"怒涛") and 取随机数()<=70 then
      self.临时伤害=self.临时伤害 + 挨打方.防御/2
    end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
   elseif 技能名称=="断岳势"   then
   if 挨打方.队伍==0 or 挨打方.队伍==nil then
      self.临时伤害=(self:取物理伤害(攻击方,挨打方)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
      self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
   else
      self.临时伤害=1
    end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
   elseif 技能名称=="惊涛怒"   then
    if 挨打方.队伍==0 or 挨打方.队伍==nil then
    self.临时伤害=(self:取物理伤害(攻击方,挨打方)* 1.1) --(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
    self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    else
    self.临时伤害=1
    end
    if self:取经脉(攻击方,"怒涛") and 取随机数()<=70 then
      self.临时伤害=self.临时伤害 + 挨打方.防御/2
    end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
 elseif 技能名称=="满天花雨"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="狮搏"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.55) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"狮吼") then
      self.临时伤害=self.临时伤害*1.15
    end
  elseif 技能名称=="裂石"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.25) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="象形"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))*0.85
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  elseif 技能名称=="天雷斩"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方) * 1.05) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100)*1.1)
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
           if 攻击方.天雷地火 ~=0 then
           self.临时伤害=self.临时伤害+攻击方.天雷地火
        end
    if self:取经脉(攻击方,"疾雷") then
      self.临时伤害 = self.临时伤害 + (攻击方.敏捷-攻击方.等级)*0.6
    end
  elseif 技能名称=="鹰击"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.05) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
    if self:取经脉(攻击方,"背水一战") and 攻击方.当前气血 <= 攻击方.气血上限*0.3 then
      self.临时伤害 = self.临时伤害 *1.5
    end
    if self:取经脉(攻击方,"翼展") then
      self.临时伤害 = self.临时伤害 +self:取技能等级(攻击方,技能名称)*1.2
    end
    if self:取经脉(攻击方,"鹰啸") then
      self.临时伤害 = self.临时伤害 +self:取技能等级(攻击方,技能名称)*1.15
    end
  --elseif 技能名称=="解毒"   then
    --self.临时伤害=0
   elseif 技能名称=="翻江搅海"   then
   self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.05) --*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
   self.临时伤害=self.临时伤害*(取随机数(90,105)/100)
  --elseif 技能名称=="解毒"   then
    --self.临时伤害=0
    if self:取经脉(攻击方,"怒涛") and 取随机数()<=70 then
      self.临时伤害=self.临时伤害 + 挨打方.防御/2
    end
    if self:取经脉(攻击方,"煞气") then
      self.临时伤害=self.临时伤害 + 100
    end
  elseif 技能名称=="紧箍咒" or 技能名称=="尸腐毒"  or 技能名称=="雾杀"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*2+攻击方.固定伤害
   if 技能名称== "紧箍咒" and self:取经脉(挨打方,"默诵") then
      self.临时伤害 = self.临时伤害 *2
   end
   if 技能名称== "尸腐毒" and self:取经脉(挨打方,"入骨") then
      self.临时伤害 = self.临时伤害 *2
   end
  elseif 技能名称=="针灸"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*15+150
   self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.05)+攻击方.治疗能力+挨打方.气血回复效果
   if self:取经脉(攻击方,"佛眷") then
    self.临时伤害 = self.临时伤害*1.2
   end
   if self:取经脉(攻击方,"仁心") and 取随机数() <=20 then
    self.临时伤害 = self.临时伤害*2
   end
  if self:取经脉(攻击方,"归气")  then
    self.临时伤害=self.临时伤害 *1.08
  end
  elseif 技能名称=="波澜不惊"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*12+150
   self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.05)+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="星月之惠"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*6+128
   self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
   if self:取经脉(攻击方,"月影") then
    self.临时伤害 = self.临时伤害 *1.2
   end
   elseif 技能名称=="舍身取义"   then
   self.临时伤害=(挨打方.气血上限-挨打方.当前气血)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
   if self:取经脉(攻击方,"天照") then
    攻击方.当前魔法 = 攻击方.当前魔法 + 75
    if 攻击方.当前魔法 >= 攻击方.魔法上限 then
      攻击方.当前魔法 = 攻击方.魔法上限
    end
   end
  elseif 技能名称=="我佛慈悲"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*10+128
   if self:取经脉(攻击方,"慈针") then
      攻击方.愤怒 = 攻击方.愤怒 + 30
      if 攻击方.愤怒 >= 150 then
        攻击方.愤怒 = 150
      end
   end
  elseif 技能名称=="杨柳甘露"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*12+256
   if 攻击方.蚩尤元神==1 and 取随机数()<=50 then
      self.临时伤害=挨打方.气血上限
     end
     if self:取经脉(攻击方,"莲花心音") and 取随机数()<=50 then
      self.临时伤害=挨打方.气血上限
     end
  elseif 技能名称=="还阳术"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*7+256
   if 挨打方.法术状态组.还阳术.有无==false then
      self.临时伤害=self.临时伤害*5
      挨打方.防御=挨打方.防御-self:取技能等级(攻击方,技能名称)
      挨打方.伤害=挨打方.伤害+self:取技能等级(攻击方,技能名称)
      挨打方.法术状态组.还阳术.回合=9999999
      挨打方.法术状态组.还阳术.有无=true
     end
   elseif 技能名称=="黄泉之息"   then
   self.临时伤害=self:取技能等级(攻击方,技能名称)*2+150
   if 挨打方.法术状态组.黄泉之息.有无==false then
      self.临时伤害=self.临时伤害*5
      挨打方.速度=挨打方.速度-math.floor(self:取技能等级(攻击方,技能名称)/5)
      if self:取经脉(攻击方,"黄泉") then
        挨打方.速度=挨打方.速度-math.floor(self:取技能等级(攻击方,技能名称)/5)
      end
      挨打方.法术状态组.黄泉之息.回合=9999999
      挨打方.法术状态组.黄泉之息.有无=true
     end
   elseif 技能名称=="摧心术"   then
   -- self.临时伤害=self:取技能等级(攻击方,技能名称)*2+150
   if 挨打方.法术状态组.摧心术.有无==false then
      -- self.临时伤害=self.临时伤害*5
      挨打方.速度=挨打方.速度-math.floor(self:取技能等级(攻击方,技能名称)/5)
      挨打方.法术状态组.摧心术.回合=5
      挨打方.法术状态组.摧心术.有无=true
     end
  elseif 技能名称 == "金蟾" then
     self.临时伤害=攻击方.金蟾
  elseif 技能名称=="推气过宫" then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*7+128)*(20-人数)/20
   if 攻击方.蚩尤元神==1 then
      self.临时伤害=self.临时伤害+math.floor((挨打方.气血上限-挨打方.当前气血)*0.4)
     end
    if self:取经脉(攻击方,"止戈") and 攻击方.武器伤害 ~= nil then
      self.临时伤害=self.临时伤害 + 攻击方.武器伤害*0.18
    end
    if self:取经脉(攻击方,"佛显") then
      self.临时伤害=self.临时伤害 *1.1
    end
    if self:取经脉(攻击方,"渡劫金身") and 取随机数()<=10 then
      self.临时伤害=self.临时伤害 *1.5
    elseif self:取经脉(攻击方,"渡劫金身") then
      self.临时伤害=self.临时伤害 *1.15
    end
    if self:取经脉(攻击方,"心韧") and 取随机数()<=15 then
      self.临时伤害=self.临时伤害*2
      攻击方.当前魔法 = 攻击方.当前魔法 - 150
      if 攻击方.当前魔法 <= 0 then
        攻击方.当前魔法 = 0
      end
    end
    if self:取经脉(攻击方,"归气")  then
      self.临时伤害=self.临时伤害 *1.08
    end
    -- print(攻击方,挨打方)
   self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
   if 攻击方.名称=="蜘蛛女王" and 攻击方.队伍==0 then
     self.临时伤害=3000
    elseif 攻击方.名称=="天罡星" and 攻击方.队伍==0 then
     self.临时伤害=5000
    elseif 攻击方.名称=="鬼将6" and 攻击方.队伍==0 then
     self.临时伤害=3000  --100boss
    elseif 攻击方.名称=="香炉精" and 攻击方.队伍==0 then
     self.临时伤害=3000  --上古凶兽
    elseif 攻击方.名称=="噬天虎7" and 攻击方.队伍==0 then
     self.临时伤害=3000  --150boss
     end
  elseif 技能名称=="地涌金莲"   then
    self.临时伤害=(self:取技能等级(攻击方,技能名称)*15 + 256)*(10-人数)/10+攻击方.治疗能力+挨打方.气血回复效果
    if 攻击方.蚩尤元神==1 and 取随机数()<=99 then
      self.临时伤害=挨打方.气血上限
     end
   if self:取经脉(攻击方,"化莲")  then
    self.临时伤害 = self.临时伤害+攻击方.武器伤害*0.30
   end
   if self:取经脉(攻击方,"自愈") and 攻击方.编号 == 挨打方.编号 then
    self.临时伤害 = self.临时伤害*1.5
   end

  elseif 技能名称=="普渡众生"   then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*5+攻击方.治疗能力)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果
   if self:取经脉(攻击方,"劳心") and 攻击方.当前气血 <= 攻击方.气血上限*0.3 then
    self.临时伤害 = self.临时伤害*2
   end
   if self:取经脉(攻击方,"劳心")  then
    self.临时伤害 = self.临时伤害*1.2
   end
  elseif 技能名称=="生命之泉"   then
   self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+攻击方.治疗能力 + 128)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果
   if 攻击方.蚩尤元神==1 then
     self.临时伤害=self.临时伤害*4
     end
   if self:取经脉(攻击方,"体恤") and 挨打方.当前气血 <= 挨打方.气血上限*0.3 then
    self.临时伤害 = self.临时伤害+150
   end
  elseif 技能名称=="气疗术" then
   self.临时伤害=挨打方.气血上限*0.03+200+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="命疗术" then
   self.临时伤害=挨打方.气血上限*0.09+600+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="气归术" or 技能名称=="冰清诀" or  技能名称=="四海升平" then
   self.临时伤害=挨打方.气血上限*0.25+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="命归术" then
   self.临时伤害=挨打方.气血上限*0.5+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="玉清诀" or 技能名称=="凝气决" or 技能名称=="凝神决" then
   self.临时伤害=1
  elseif 技能名称=="慈航普渡" then
    self.临时伤害=挨打方.气血上限
  elseif 技能名称=="晶清诀" then
   self.临时伤害=挨打方.气血上限*0.15+攻击方.治疗能力+挨打方.气血回复效果
  elseif 技能名称=="冥王暴杀" then
   self.临时伤害=攻击方.灵力*取随机数(80,150)/100
   self.天地同寿=true
  elseif 技能名称=="诅咒之伤" then
   self.临时伤害=挨打方.当前气血*0.2
   if self.临时伤害>攻击方.等级*攻击方.等级 then
      self.临时伤害=攻击方.等级*攻击方.等级
     end
   end
  if self.法伤附加 then

   self.临时伤害=self.临时伤害+攻击方.法术伤害结果
     end

 if 攻击方.法波~=0  then
   self.临时伤害=self.临时伤害*取随机数(攻击方.法波.下限,攻击方.法波.上限)/100
   end
   if self.天地同寿 and 挨打方.法术状态组.天地同寿.有无 and 挨打方.命令数据.类型=="防御" then
     self.临时伤害=1
     end
 if self.临时伤害<1 then self.临时伤害=1 end
 return math.floor(self.临时伤害+攻击方.额外法术伤害)
 end
function 战斗处理类:特技消耗(攻击方,特技名称)
  self.临时消耗=装备特技[特技名称].消耗
  if 攻击方.愤怒特效 then
   self.临时消耗=math.floor(self.临时消耗*0.8)
  end
  if self:取经脉(攻击方,"修心") and 特技名称 ~= "慈航普渡" then
    if 攻击方.愤怒<self.临时消耗 then
        if 攻击方.当前魔法 < (self.临时消耗-攻击方.愤怒)*20 then
          return false
        else
          攻击方.当前魔法 = 攻击方.当前魔法 - (self.临时消耗-攻击方.愤怒)*20
          self.临时消耗 = 攻击方.愤怒
        end
    end
  end
  if self:取经脉(攻击方,"慈心") and 特技名称 == "慈航普渡" then
    self.临时消耗 = self.临时消耗 -40
  end
  if self:取经脉(攻击方,"鬼火") then
    self.临时消耗 = self.临时消耗 -10
  end
  if self:取经脉(攻击方,"傲娇") then
    self.临时消耗 = self.临时消耗 -5
  end
   if self:取经脉(攻击方,"花护")  and (特技名称 == "水清诀" or 特技名称 == "冰清诀" or 特技名称 == "晶清诀" or 特技名称 == "玉清诀") then
    self.临时消耗 = self.临时消耗 -8
  end
  if 攻击方.战斗类型~= "角色" then
   return true
  elseif 攻击方.愤怒<self.临时消耗 then
   return false
  else
   攻击方.愤怒=攻击方.愤怒-self.临时消耗

    return true
   end
 end
function 战斗处理类:魔法消耗(攻击方,数值,数量)
 self.临时消耗=math.floor(数值*数量*(攻击方.慧根 or 1))
 if 攻击方.当前魔法<self.临时消耗 then
   return false
  else
    攻击方.当前魔法=攻击方.当前魔法-self.临时消耗

    return true
   end
 end
function 战斗处理类:技能消耗(攻击方,数量)
 if 数量==nil then 数量=1 end
 self.技能名称=攻击方.命令数据.参数
 if 攻击方.队伍id==0 then return true end
 if self.技能名称=="横扫千军" and 攻击方.当前气血>=攻击方.气血上限*0.5 then

   return true
  elseif self.技能名称=="破釜沉舟" and 攻击方.当前气血>=攻击方.气血上限*0.5 then

   return true

  elseif self.技能名称 == "剑荡四方" and 攻击方.当前气血 >= 攻击方.气血上限 * 0.5 then

    return true

  elseif self.技能名称=="血雨" and 攻击方.当前气血>=攻击方.气血上限*0.5 then

   return true

  elseif self.技能名称=="烟雨剑法" then
    if self:取经脉(攻击方,"雨杀") and 攻击方.当前气血>=攻击方.气血上限*0.65 then
      return self:魔法消耗(攻击方,50,1)
    end
    if  攻击方.当前气血>=攻击方.气血上限*0.7 then
      return self:魔法消耗(攻击方,50,1)
    end
  elseif self.技能名称=="后发制人" then
   return true
  elseif self.技能名称=="杀气诀" or self.技能名称=="针灸"  or self.技能名称=="星月之惠" then
   if self:取经脉(攻击方,"滋养") and self.技能名称 == "星月之惠" then
    return self:魔法消耗(攻击方,20,1)
   end
   return self:魔法消耗(攻击方,40,1)
  elseif self.技能名称=="瘴气" or self.技能名称=="魔息术" or self.技能名称=="安神诀" or self.技能名称=="反间之计" or self.技能名称=="定身符" or self.技能名称=="龙吟" or self.技能名称=="连环击" then

   return self:魔法消耗(攻击方,30,1)
  elseif self.技能名称=="推气过宫"  or self.技能名称 == "金蟾"  then
    if self:取经脉(攻击方,"渡劫金身") then
      return self:魔法消耗(攻击方,15,数量)
    end
   return self:魔法消耗(攻击方,10,数量)

  elseif self.技能名称=="地涌金莲" then
    if self:取经脉(攻击方,"回敬") then
      攻击方.当前气血=math.floor(攻击方.当前气血*0.95)
         return self:魔法消耗(攻击方,10,数量)
    end
  攻击方.当前气血=math.floor(攻击方.当前气血*0.95)

   return self:魔法消耗(攻击方,15,数量)


  elseif self.技能名称=="夺命蛛丝" then

   return self:特技消耗(攻击方,150,数量)

  elseif self.技能名称=="金身舍利" then

   return self:魔法消耗(攻击方,100,数量)


  elseif self.技能名称=="天崩地裂" then

   return self:魔法消耗(攻击方,50,1)
   elseif self.技能名称=="天魔解体" then
    if 攻击方.当前魔法<攻击方.魔法上限*0.8 then

      return false
    else

       return self:魔法消耗(攻击方,math.floor(攻击方.魔法上限*0.8),数量)
      end
  elseif self.技能名称=="苍茫树" or self.技能名称=="地裂火" or self.技能名称=="日光华" or self.技能名称=="靛沧海"  or self.技能名称=="夺命咒" then

   return self:魔法消耗(攻击方,20,数量)

  elseif self.技能名称=="金刚护法" then

   return self:魔法消耗(攻击方,10,数量)

  elseif self.技能名称=="镇魂诀" then

  攻击方.当前气血=math.floor(攻击方.当前气血*0.95)

   return self:魔法消耗(攻击方,0,数量)

  elseif self.技能名称=="碎星诀" then

   return self:魔法消耗(攻击方,30,数量)
  elseif self.技能名称=="炼气化神" and 攻击方.当前气血>=30 then
   攻击方.当前气血=攻击方.当前气血-30
    return true
  elseif self.技能名称=="生命之泉" then

   return self:魔法消耗(攻击方,30,数量)
  elseif self.技能名称=="二龙戏珠" then

   return self:魔法消耗(攻击方,70,数量)
  elseif self.技能名称=="神龙摆尾" or self.技能名称=="明光宝烛"   or self.技能名称=="摧心术"  then

   return self:魔法消耗(攻击方,10,数量)

  elseif self.技能名称=="一笑倾城" or self.技能名称=="分身术" or self.技能名称=="颠倒五行"  then

   return self:魔法消耗(攻击方,80,数量)

  elseif self.技能名称=="灵动九天" or self.技能名称=="飞花摘叶" or self.技能名称=="一苇渡江" or self.技能名称=="金刚护体" or self.技能名称=="唧唧歪歪" or self.技能名称=="天雷斩" or self.技能名称=="鹰击" or self.技能名称=="连环击" or self.技能名称=="泰山压顶" or self.技能名称=="奔雷咒" or self.技能名称=="地狱烈火" or self.技能名称=="水漫金山" or self.技能名称=="不动如山" or self.技能名称=="法术防御"then

   return self:魔法消耗(攻击方,20,数量)
  elseif self.技能名称=="似玉生香" or self.技能名称=="乘风破浪" or self.技能名称=="五雷轰顶" or self.技能名称=="判官令"  then

   return self:魔法消耗(攻击方,40,数量)

  elseif self.技能名称=="如花解语" or self.技能名称=="雷霆万钧" then

   return self:魔法消耗(攻击方,35,数量)
   elseif self.技能名称=="摇头摆尾" or self.技能名称=="天地同寿" or self.技能名称=="乾坤妙法" or self.技能名称=="金刚镯" or self.技能名称=="碎甲符" or self.技能名称=="我佛慈悲" or self.技能名称=="阎罗令" or self.技能名称=="杨柳甘露" or self.技能名称=="水攻" or self.技能名称=="落岩" or self.技能名称=="烈火" or self.技能名称=="雷击" then

   return self:魔法消耗(攻击方,50,数量)
  elseif self.技能名称=="莲步轻舞" or self.技能名称=="变身" then

   return self:魔法消耗(攻击方,30,数量)
  elseif self.技能名称=="满天花雨" or self.技能名称=="镇妖"  then

   return self:魔法消耗(攻击方,45,数量)

  elseif self.技能名称=="楚楚可怜" or self.技能名称=="百毒不侵" or self.技能名称=="普渡众生" or self.技能名称=="天罗地网" or self.技能名称=="雨落寒沙"then

   return self:魔法消耗(攻击方,30,数量)
  elseif self.技能名称=="黄泉之息" or self.技能名称=="勾魂" or  self.技能名称=="摄魄" or self.技能名称=="尸腐毒" or self.技能名称=="五雷咒" or self.技能名称=="龙腾" or self.技能名称=="百万神兵" or self.技能名称=="狮搏" or self.技能名称=="含情脉脉" or self.技能名称=="裂石"  or self.技能名称=="荆棘舞"  or self.技能名称=="尘土刃"  or self.技能名称=="冰川怒" then
   return self:魔法消耗(攻击方,50,数量)
  elseif self.技能名称=="火甲术" or self.技能名称=="失心符" or self.技能名称=="失魂符" or self.技能名称=="错乱" or self.技能名称=="三昧真火"  then
   if self.技能名称=="三昧真火" then

       if 攻击方.命令数据.参数.摇头摆尾~=nil then
         攻击方.命令数据.参数.摇头摆尾=nil
         return self:魔法消耗(攻击方,50,1)
        else
          return self:魔法消耗(攻击方,60,数量)

      end

     end
   return self:魔法消耗(攻击方,60,数量)
   elseif self.技能名称=="宁心" or self.技能名称=="落雷符" or self.技能名称=="定身符" or self.技能名称=="紧箍咒" or self.技能名称=="雾杀" or self.技能名称=="夺魄令"  then
   return self:魔法消耗(攻击方,30,数量)
   elseif self.技能名称=="善恶有报" or  self.技能名称=="夜舞倾城"  then
   return self:魔法消耗(攻击方,160,数量)
   elseif self.技能名称=="催眠符" or self.技能名称=="镇妖" or self.技能名称=="九天玄火" then

   return self:魔法消耗(攻击方,45,数量)

  elseif self.技能名称=="大闹天宫" or self.技能名称=="天神护体" or self.技能名称=="离魂符" or self.技能名称=="失忆符" or self.技能名称=="日月乾坤" or self.技能名称=="定心术" or self.技能名称=="煞气诀" then

   return self:魔法消耗(攻击方,50,数量)
  elseif self.技能名称=="龙卷雨击" or self.技能名称=="蜜润"  then

   return self:魔法消耗(攻击方,25,数量)
  elseif self.技能名称=="飞砂走石" then
    if 攻击方.命令数据.参数.摇头摆尾~=nil then
     攻击方.命令数据.参数.摇头摆尾=nil
     return self:魔法消耗(攻击方,50,1)
    else
      return self:魔法消耗(攻击方,30,数量)
      end
  elseif self.技能名称=="落叶萧萧" then
   return self:魔法消耗(攻击方,30,数量)

  elseif self.技能名称=="还阳术" or self.技能名称=="我佛慈悲" or self.技能名称=="舍身取义" or self.技能名称=="佛法无边" then

   return self:魔法消耗(攻击方,150,数量)
  elseif self.技能名称=="杨柳甘露" then

   return self:魔法消耗(攻击方,150,数量)
  elseif self.技能名称=="力劈华山" then

   return self:魔法消耗(攻击方,150,数量)
  elseif self.技能名称=="归元咒" then
   if self:取经脉(攻击方,"吐纳") then
      self:魔法消耗(攻击方,40,数量)
   end
   return self:魔法消耗(攻击方,20,数量)
  elseif self.技能名称=="追魂符" then

   return self:魔法消耗(攻击方,50,数量)
  elseif self.技能名称=="解毒" then

   return self:魔法消耗(攻击方,40,数量)
 elseif self.技能名称=="解封" then

   return self:魔法消耗(攻击方,60,数量)
  elseif self.技能名称=="清心" then

    return self:魔法消耗(攻击方,30,数量)
 elseif self.技能名称=="飘渺式" then
    return self:魔法消耗(攻击方,30,数量)
 elseif self.技能名称=="浪涌" then
    if self.战意点数 < 6 then
       self.战意点数  = self.战意点数 + 1
    end
   return self:魔法消耗(攻击方,30,数量)

 elseif self.技能名称=="惊涛怒" or self.技能名称=="腾雷" or self.技能名称=="断岳势"then

   return self:魔法消耗(攻击方,30,数量)
 elseif self.技能名称=="翻江搅海" then

   return self:魔法消耗(攻击方,20,数量)

 elseif self.技能名称=="驱魔" then

   return self:魔法消耗(攻击方,45,数量)

 elseif self.技能名称=="驱尸" then

   return self:魔法消耗(攻击方,40,数量)
  elseif self.技能名称=="修罗隐身" then

   return self:魔法消耗(攻击方,150,数量)
  elseif self.技能名称=="幽冥鬼眼" then

   return self:魔法消耗(攻击方,20,数量)

  elseif self.技能名称=="牛劲" then

   return self:魔法消耗(攻击方,20,数量)
  elseif self.技能名称=="魔王回首" then

   return self:魔法消耗(攻击方,30,数量)

  elseif self.技能名称=="极度疯狂" then

   return self:魔法消耗(攻击方,30,数量)

   elseif self.技能名称=="威慑" then

   return self:魔法消耗(攻击方,20,数量)

   elseif self.技能名称=="定心术" then

   return self:魔法消耗(攻击方,40,数量)
  elseif self.技能名称=="象形" then

   return self:魔法消耗(攻击方,30,数量)
  elseif self.技能名称=="魔音摄魂" then

   return self:魔法消耗(攻击方,40,数量)
  elseif self.技能名称=="复苏" then

   return self:魔法消耗(攻击方,60,数量)
  elseif self.技能名称=="盘丝阵" then

   return self:魔法消耗(攻击方,40,数量)

  else
   return false

   end
 end
function 战斗处理类:取保护单位(编号)
  for n=1,#self.参战单位 do
    if self.参战单位[n].队伍==self.参战单位[编号].队伍  and self.参战单位[n].命令数据.类型=="保护" and  self.参战单位[n].命令数据.目标==编号 and self:取存活状态(n) then
     self.参战单位[n].命令数据.类型=""
      return n
     end
    end
 return false
 end
function 战斗处理类:取物理伤害结果(数额,攻击方,挨打方,倍数,类型,神佑)
    数额=math.floor(数额*倍数)-挨打方.格挡值
    if 数额<=0 then 数额=1 end
    local 死亡方式=0
    if 类型=="加血" then
        挨打方.当前气血=挨打方.当前气血+数额
        if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
    else
       if 攻击方.百步穿杨~=0 and 20>取随机数() then
            数额=数额+攻击方.百步穿杨
       end
        if 挨打方.心随我动~=0 and 25>取随机数() then
            数额=数额-挨打方.心随我动
       end
       if 挨打方.蟠龙玉璧 ~= nil and 挨打方.蟠龙玉璧>0 and 取随机数()<=挨打方.蟠龙玉璧+1 then
           数额=数额*0.8
       end
       if 挨打方.法术状态组.神木宝鼎.有无 and 挨打方.神木宝鼎 ~= nil then
          数额 = 数额 *0.8
          挨打方.当前魔法 = 挨打方.当前魔法 + 数额 *0.2
          if 挨打方.当前魔法 >= 挨打方.魔法上限 then
            挨打方.当前魔法 = 挨打方.魔法上限
          end
       end
      if 挨打方.法术状态组.分水.有无 and 挨打方.分水 ~= nil and 取随机数()<=挨打方.分水 then
          数额 = 数额 *0.5
      end
      if self:取经脉(挨打方,"鬼念") and 攻击方.复活 ~= nil and 攻击方.复活 ~= 0 then
        数额 = 数额 *0.85
      end
      if self:取经脉(挨打方,"批亢") and 挨打方.法术状态组.分身术.有无 then
        数额 = 数额 *0.85
      end
      if self:取经脉(挨打方,"飞龙") and 取随机数()<=70 then
        数额 = 数额*0.8
      end
      if self:取经脉(挨打方,"化怨") and 挨打方.当前气血<=挨打方.气血上限*0.3 then
        数额 = 数额*0.75
      end
      if self:取经脉(挨打方,"清风望月") then
        数额 = 数额*0.7
      end
      if self:取经脉(挨打方,"养生") and 挨打方.当前气血<=挨打方.气血上限*0.5 then
        数额 = 数额*0.8
      end
      if self:取经脉(攻击方,"强击") and self:取玩家战斗() == false then
        数额 = 数额*1.1
      end
      if self:取经脉(攻击方,"混元") and 攻击方.当前气血>=攻击方.气血上限*0.7 then
        数额 = 数额*1.05
      end
       if self:取经脉(挨打方,"神躯") and 数额>=挨打方.气血上限*0.2 then
        数额 = 数额*0.8
      end
       if self:取经脉(挨打方,"敛恨") then
        数额 = 数额*0.8
      end
       if self:取经脉(挨打方,"安抚") then
        数额 = 数额*0.9
      end
       if self:取经脉(挨打方,"六道无量") then
        数额 = 数额*0.9
      end
       if self:取经脉(挨打方,"回旋") then
        数额 = 数额-60
      end
       if self:取经脉(挨打方,"风墙") then
        数额 = 数额 - 挨打方.等级*16
      end
       if self:取经脉(挨打方,"偷龙转凤") and 挨打方.当前气血<=挨打方.气血上限*0.3 then
        数额 = 数额*0.8
      end
       if self:取经脉(挨打方,"灵身") and 挨打方.当前气血<=挨打方.气血上限*0.5 then
        数额 = 数额*0.8
      end
        if self:取经脉(挨打方,"死地") and 数额>=挨打方.当前气血 and 取随机数()<=20 then
        数额 =1
      end
        if self:取经脉(挨打方,"蚀天") and 数额>=挨打方.气血上限*0.2 and 取随机数()<=20 then
        挨打方.灵力 = 挨打方.灵力+10
      end
      if 神佑==nil and self:取是否神佑(挨打方,数额) then
          类型="加血"
          数额=挨打方.气血上限
          挨打方.当前气血=挨打方.当前气血+数额
          if 挨打方.当前气血>挨打方.气血上限 then
            挨打方.当前气血=挨打方.气血上限
          end
      else
          挨打方.当前气血=挨打方.当前气血-数额
          if 挨打方.当前气血<=0 then
          挨打方.当前气血=0
          死亡方式=self:取死亡状态(攻击方,挨打方)
          end
          self:愤怒处理(挨打方,数额,死亡方式)
      end
    end
   if 数额<=0 then 数额=1 end
   self:催眠状态解除(类型,挨打方)
   return {伤害=数额,类型=类型,死亡=死亡方式}
 end
function 战斗处理类:取法术伤害结果(数额,攻击方,挨打方,倍数,类型,神佑)
  数额=math.floor(数额*倍数)
  local 死亡方式=0
 if 类型=="加血" then

   挨打方.当前气血=挨打方.当前气血+数额
   if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
  else
           if 挨打方.云随风舞~=0 and 25>取随机数() then
            数额=数额-挨打方.云随风舞

       end
   if 神佑==nil and self:取是否神佑(挨打方,数额) then
      类型="加血"
      数额=挨打方.气血上限
      挨打方.当前气血=挨打方.当前气血+数额
      if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end

    else
       if 挨打方.法术状态组.神木宝鼎.有无 and 挨打方.神木宝鼎 ~= nil then
          数额 = 数额 *0.8
          挨打方.当前魔法 = 挨打方.当前魔法 + 数额 *0.2
          if 挨打方.当前魔法 >= 挨打方.魔法上限 then
            挨打方.当前魔法 = 挨打方.魔法上限
          end
       end

      if 挨打方.法术状态组.分水.有无 and 挨打方.分水 ~= nil and 取随机数()<=挨打方.分水 then
          数额 = 数额 *0.5
       end
      if self:取经脉(挨打方,"鬼念") and 攻击方.复活 ~= nil and 攻击方.复活 ~= 0 then
        数额 = 数额 *0.85
      end
      if self:取经脉(挨打方,"批亢") and 挨打方.法术状态组.分身术.有无 then
        数额 = 数额 *0.85
      end
      if self:取经脉(攻击方,"龙慑") then --挨打方
        数额 = 数额+100
      end
      if self:取经脉(攻击方,"破浪") and 取随机数() <= 35  then --挨打方
        数额 = 数额+300
      end
      if self:取经脉(挨打方,"龙骇") then
        数额 = 数额+40
      end
      if self:取经脉(挨打方,"飞龙") and 取随机数()<=70 then
        数额 = 数额*0.8
      end
      if self:取经脉(攻击方,"雷浪穿云")  then --挨打方
        数额 = 数额*1.2
      end
      if self:取经脉(挨打方,"化怨") and 挨打方.当前气血<=挨打方.气血上限*0.3 then
        数额 = 数额*0.75
      end
      if self:取经脉(挨打方,"养生") and 挨打方.当前气血<=挨打方.气血上限*0.5 then
        数额 = 数额*0.8
      end
        if self:取经脉(挨打方,"风墙") then
        数额 = 数额 - 挨打方.等级*16
      end
       if self:取经脉(挨打方,"神躯") and 数额>=挨打方.气血上限*0.2 then
        数额 = 数额*0.8
      end
         if self:取经脉(挨打方,"死地") and 数额>=挨打方.当前气血 and 取随机数()<=20 then
        数额 =1
      end
          if self:取经脉(挨打方,"安抚") then
        数额 = 数额*0.9
      end
       if self:取经脉(挨打方,"六道无量") then
        数额 = 数额*0.9
      end
       if self:取经脉(挨打方,"回旋") then
        数额 = 数额-60
      end
       if self:取经脉(挨打方,"偷龙转凤") and 挨打方.当前气血<=挨打方.气血上限*0.3 then
        数额 = 数额*0.8
      end
       if self:取经脉(挨打方,"灵身") and 挨打方.当前气血<=挨打方.气血上限*0.5 then
        数额 = 数额*0.8
      end

      挨打方.当前气血=挨打方.当前气血-数额
      if 挨打方.当前气血<=0 then
        挨打方.当前气血=0
        死亡方式=self:取死亡状态(攻击方,挨打方)
       end
      self:愤怒处理(挨打方,数额,死亡方式)
     end
   end

  if 类型=="加血" and 挨打方.法术状态组.魔音摄魂.有无 then

   数额=1

    end
 if 数额<=0 then 数额=1 end
   self:催眠状态解除(类型,挨打方)
   return {伤害=数额,类型=类型,死亡=死亡方式}
 end
function 战斗处理类:愤怒处理(攻击方,伤害,死亡方式)
  if 攻击方.愤怒==nil then
   return 0
  elseif 死亡方式>0 and 攻击方.法术状态组.不动如山.有无 == false then
    攻击方.当前愤怒=0
   return 0
  elseif 死亡方式>0 and 攻击方.法术状态组.不动如山.有无 then
    攻击方.当前愤怒=80
   return 0
  else
      local 增加愤怒= 0
       增加愤怒 = 150*(1-(攻击方.最大气血-伤害)/攻击方.最大气血)
       if self:取经脉(攻击方,"怒火") and 增加愤怒 >= 10 then
        增加愤怒 = 增加愤怒 +5
       end
      攻击方.愤怒=攻击方.愤怒+math.floor(增加愤怒)
    if 攻击方.愤怒>150 then 攻击方.愤怒=150 end
    end
 end
function 战斗处理类:取是否吸血(攻击方,挨打方)
 if 攻击方.吸血>0 and 挨打方.复活==0 then
   return true
  else
   return false
   end
  end
function 战斗处理类:取死亡状态(攻击方,挨打方)
 if 挨打方.战斗类型=="角色" and 挨打方.队伍~=0 then
   if 攻击方~=nil and 攻击方~=1 and 攻击方.名称~=nil  then
     挨打方.死亡消息=攻击方.名称
    else
     挨打方.死亡消息="？？？"
    end
    if 攻击方~=nil and 攻击方~=1 and 攻击方.玩家id~=nil and 攻击方.玩家id~=0 then
     挨打方.死亡消息=挨打方.死亡消息.."["..攻击方.玩家id.."]"
    end
    if 挨打方.复活~=0 then
     挨打方.法术状态组.鬼魂术.有无=true
     挨打方.法术状态组.鬼魂术.回合=挨打方.复活
    end
    return 2
  elseif 攻击方~=1 and 攻击方.驱鬼~=0 then
    return 1
  elseif 挨打方.复活~=0 then

   挨打方.法术状态组.鬼魂术.有无=true
   挨打方.法术状态组.鬼魂术.回合=挨打方.复活
   return 2
  else
   return 1
   end
 end
function 战斗处理类:取物理伤害(攻击方,挨打方)
  if 挨打方.物伤减免==nil then 挨打方.物伤减免=1 end
  self.附加值=0
  if 攻击方.玩家id~=0 and 攻击方.战斗类型~="角色" and 玩家数据[攻击方.玩家id]~=nil then
   self.附加值=攻击方.伤害 * 0.35
  end
  if self:取经脉(攻击方,"不惊") and 挨打方.武器宝石 ~= nil and 挨打方.武器宝石 >0 then
    self.附加值 = self.附加值 + 挨打方.武器宝石 * 5
  end
 self.临时伤害=math.floor((攻击方.伤害 + self.附加值+攻击方.穿刺等级-挨打方.防御))
 self.临时伤害 = self.临时伤害 +  (攻击方.等级 - 挨打方.等级) * 9 -- self.临时伤害*self.临时伤害/2200
 self.临时伤害= self.临时伤害*(取随机数(95,105)/100) * 挨打方.物伤减免
 self.临时伤害= self.临时伤害*((1+攻击方.修炼数据.攻击*0.02))
 self.临时伤害= self.临时伤害*((1-挨打方.修炼数据.防御*0.02))
 if self.临时伤害<1 then self.临时伤害=1 end
 if 攻击方.玩家id~=0 and 攻击方.战斗类型=="角色" and 玩家数据[攻击方.玩家id]~=nil then
   玩家数据[攻击方.玩家id].角色:耐久处理(攻击方.玩家id,1)
   end
 if 挨打方.玩家id~=0 and 挨打方.战斗类型=="角色" and 玩家数据[挨打方.玩家id]~=nil then
   玩家数据[挨打方.玩家id].角色:耐久处理(挨打方.玩家id,2)
   end
 return math.floor(self.临时伤害)
 end
function 战斗处理类:取灵力差(攻击方,挨打方,伤害)
  if 挨打方.法伤减免==nil then 挨打方.法伤减免=1 end
   if 攻击方.隔山打牛~=0 and 20>取随机数() then
    self.临时伤害=math.floor(((攻击方.灵力 * 1.1 +攻击方.隔山打牛)*攻击方.魔心+攻击方.法术伤害-挨打方.法防))
   else
      self.临时伤害=math.floor((攻击方.灵力* 1.1 * 攻击方.魔心+攻击方.法术伤害-挨打方.法防))
   end
   self.临时伤害 = self.临时伤害 + (攻击方.等级 - 挨打方.等级) * 6

   -- self.临时伤害=self.临时伤害+self.临时伤害*self.临时伤害/2000
   self.临时伤害=self.临时伤害*挨打方.法伤减免
   self.临时伤害=self.临时伤害*((1+攻击方.修炼数据.法术*0.02))
   self.临时伤害=self.临时伤害*((1-挨打方.修炼数据.法抗*0.02))+攻击方.法术伤害结果
   -- if 挨打方.队伍==0 then
   --   self.临时伤害=math.floor(self.临时伤害*1.35)
   --   end

  if 攻击方.法波~=0  then
   self.临时伤害=self.临时伤害*取随机数(攻击方.法波.下限,攻击方.法波.上限)/100
   end
 return math.floor(self.临时伤害)
 end
function 战斗处理类:取队伍id(id)
 if 玩家数据[id].队伍==0 then
     return id
    else
     return 玩家数据[id].队伍
     end
 end
function 战斗处理类:取隐身判断(攻击方,挨打方)
  if self.参战单位[挨打方].法术状态组.楚楚可怜 and self.参战单位[挨打方].法术状态组.楚楚可怜.有无 then
   return false
  end
  if self.参战单位[挨打方].法术状态组.分身术 and self.参战单位[挨打方].法术状态组.分身术.有无 then
   if self.参战单位[挨打方].法术状态组.分身术.躲避==false   then
     self.参战单位[挨打方].法术状态组.分身术.躲避=true
     self.参战单位[挨打方].法术状态组.分身术.标记=攻击方
     return false
    elseif self.参战单位[挨打方].法术状态组.分身术.标记==攻击方 then
      return false
     end
    end
  if self.参战单位[挨打方].法术状态组.隐身 and self.参战单位[挨打方].法术状态组.隐身.有无 and self.参战单位[攻击方].感知==false and self.参战单位[攻击方].法术状态组.幽冥鬼眼.有无==false  then
    return false
  else
   return true
  end
 end
function 战斗处理类:取存活状态(编号)
 if self.参战单位[编号]==nil or self.参战单位[编号].当前气血==nil then return false end
 if self.参战单位[编号].当前气血<=0 or self.参战单位[编号].单位消失==1  then
    return false
  else
    return true
   end
 end
function 战斗处理类:取是否神佑(挨打方,伤害)
 if 挨打方.复活~=0 then return false end
 if  挨打方.当前气血<=伤害 and (取随机数()<=挨打方.神佑 or (挨打方.门派=="方寸山" and 挨打方.蚩尤元神==1 and 取随机数()<=15)) then
   return true
  else
   return false
   end
  end
function 战斗处理类:取是否吸血(攻击方,挨打方)
 if 攻击方.吸血>0 and 挨打方.复活==0 then
   return true
  else
   return false
   end
  end
function 战斗处理类:取是否反击(攻击方,挨打方)
 if 取随机数()<=挨打方.反击 and 攻击方.偷袭==false then
   return true
 elseif 挨打方.法术状态组.魔王回首.有无 and 攻击方.偷袭==false then
   return true
  elseif 挨打方.法术状态组.极度疯狂.有无 and 攻击方.偷袭==false then
   return true
  else
   return false
   end
 end
function 战斗处理类:取是否反震(攻击方,挨打方)
 if 取随机数(1,100)<=挨打方.反震 and 攻击方.偷袭==false then
   return true
  else
   return false
   end
 end
function 战斗处理类:取是否必杀(攻击方,挨打方)

  if 取随机数()<=(攻击方.必杀+ (攻击方.物理暴击等级-挨打方.抗物理暴击等级)/10) and 挨打方.幸运==false then
   return true
  else
   return false
  end
 end
function 战斗处理类:取死亡状态1(攻击方,挨打方)
 if self.参战单位[挨打方].战斗类型=="角色" and self.参战单位[挨打方].队伍~=0 then
    return 2
 elseif self.参战单位[攻击方].驱鬼~=0 then
    return 1
  elseif self.参战单位[挨打方].复活~=0 then
   self.参战单位[挨打方].法术状态组.鬼魂术.有无=true
   self.参战单位[挨打方].法术状态组.鬼魂术.回合=self.参战单位[挨打方].复活
   return 2
  else
   return 1
   end
 end
function 战斗处理类:取随机单位1(编号,相关id,类型,数量,参数) --1为敌 2为队友
  self.单位组={}
  for n=1,#self.参战单位 do
   if 类型==1 then
     if self.参战单位[n].队伍id~=相关id and self:取存活状态(n) and self:取隐身判断(编号,n) then self.单位组[#self.单位组+1]=n end
    elseif 类型==2 then
     if self.参战单位[n].队伍id==相关id and self:取存活状态(n) then self.单位组[#self.单位组+1]={编号=n,气血=self.参战单位[n].当前气血} end
     end
    end
  if #self.单位组==0 then
   return 0
  else
   return self:取随机数组成员1(self.单位组,数量)
    end
 end
function 战斗处理类:取随机数组成员1(数组,数量)
 if #数组<=数量 then
   self.临时数组={}
  for n=1,#数组 do
      self.临时数组[n]=数组[n].编号
     end
   return self.临时数组
  else
   self.临时数组={}
   table.sort(数组,function(a,b) return a.气血<b.气血 end )
   for n=1,数量 do
      self.临时数组[n]=数组[n].编号
     end
   return self.临时数组
   end
  end
function 战斗处理类:取随机单位(编号,相关id,类型,数量) --1为敌 2为队友
  self.单位组={}
  for n=1,#self.参战单位 do
   if 类型==1 then
     if self.参战单位[n].队伍id~=相关id and self:取存活状态(n) and self:取隐身判断(编号,n) then self.单位组[#self.单位组+1]=n end
    elseif 类型==2 then
     if self.参战单位[n].队伍id==相关id and self:取存活状态(n) then self.单位组[#self.单位组+1]=n end
     end
    end
  if #self.单位组==0 then
   return 0
  else
   return self:取随机数组成员(self.单位组,数量)
    end
 end
function 战斗处理类:取随机数组成员(数组,数量)
 if #数组<=数量 then
   return 数组
  else
   self.临时数组={}
   for n=1,#数组 do
     数组[n]={a=数组[n],b=取随机数(1,10000)}
     end
   table.sort(数组,function(a,b) return a.b>b.b end )
   for n=1,数量 do
      self.临时数组[n]=数组[n].a
     end
   return self.临时数组
   end
  end
function 战斗处理类:取玩家战斗()
 return self.pk战斗
 end
function 战斗处理类:取技能目标数(攻击方,技能名称)
 if self.战斗开始==false then return 1 end --附加状态的人数
 if  技能名称=="金蟾" then
  return 10
 end

 if 装备特技[技能名称]==nil then
   self.临时等级=self:取技能等级(攻击方,技能名称)
 end
 self.临时人数=1
  if 技能名称=="推气过宫" or 技能名称=="鹰击" or 技能名称=="幽冥鬼眼"  or 技能名称=="翻江搅海" then

   self.临时人数=math.floor(self.临时等级/30)+1
   if (攻击方.名称=="天罡星" or 攻击方.名称=="蜘蛛女王" ) and 攻击方.队伍==0 then
      self.临时人数=10
     end
   if ( 攻击方.名称=="香炉精" or 攻击方.名称=="噬天虎7"or 攻击方.名称=="鬼将6") and 攻击方.队伍==0 then
      self.临时人数=10
     end
     if 技能名称=="鹰击" and self:取经脉(攻击方,"失心") then
      self.临时人数 = self.临时人数 +1
     end
  elseif 技能名称=="落雷符" then

   self.临时人数=math.floor(self.临时等级/30)+1
   if self.临时人数>3 then self.临时人数=3 end
   if self:取玩家战斗() then

     self.临时人数=1
    end
    if self:取经脉(攻击方,"奔雷") then
      self.临时人数 = self.临时人数+1
    end
  elseif 技能名称=="碎甲符" then

   self.临时人数=1
   if self:取经脉(攻击方,"碎甲") then

     self.临时人数=3
     end
  elseif 技能名称=="金刚护法" then

   self.临时人数=math.floor((self.临时等级-40)/30)+2

   elseif 技能名称=="镇魂诀" then

   self.临时人数=5


  elseif 技能名称=="碎星诀" or 技能名称=="法术防御" then

   self.临时人数=1

  elseif 技能名称=="地涌金莲" then

   self.临时人数=3
    if self:取经脉(攻击方,"金莲") then
      self.临时人数 = self.临时人数+1
    end

  if  攻击方.蚩尤元神==1 and 取随机数(1,10)<=10 then
       self.临时人数=4

    end



   elseif 技能名称=="金刚护体" or 技能名称=="一苇渡江" or 技能名称=="不动如山"  or 技能名称=="灵动九天"  or 技能名称=="明光宝烛"  then

   self.临时人数=math.floor((self.临时等级-50)/30)+2
   elseif 技能名称=="蜜润"   then

   self.临时人数=10
  elseif 技能名称 == "普渡众生" then
    self.临时人数 = 1
    if 攻击方.普渡 ~= nil and 攻击方.普渡 > 0 and 取随机数()<=攻击方.普渡 then
      self.临时人数 = 2
    end
    if self:取经脉(攻击方,"道衍") then
      self.临时人数 = self.临时人数 +2
    end
  elseif   技能名称=="唧唧歪歪" or 技能名称=="龙卷雨击" or 技能名称=="阎罗令" or 技能名称=="生命之泉" then

   self.临时人数=math.floor(self.临时等级/25)+1
   if 技能名称 == "龙卷雨击" and self:取经脉(攻击方,"龙魄") and 取随机数()<=10 then
    self.临时人数=self.临时人数+3
    if self.临时人数 >= 10 then
      self.临时人数 =10
    end
   end
   if 技能名称 == "阎罗令" and self:取经脉(攻击方,"魂飞") then
    self.临时人数=self.临时人数+1
   end
  elseif  技能名称=="飞花摘叶" then
    self.临时人数=取随机数(2,3) - 1
    if self:取经脉(攻击方,"花殇") and 取随机数() < 50 then
      self.临时人数 = self.临时人数 + 3 - 2
    end
  elseif 技能名称=="飞砂走石"  then

   self.临时人数=math.floor(self.临时等级/35)+1
    if self:取经脉(攻击方,"震怒") and 取随机数() <= 10 then
      self.临时人数 = self.临时人数 +1
    end
   elseif 技能名称=="落叶萧萧"  then

   self.临时人数=math.floor(self.临时等级/35)+1
    if self:取经脉(攻击方,"法身") then
      self.临时人数 = self.临时人数 +1
    end
  elseif 技能名称=="九天玄火"  then

   self.临时人数=math.floor(self.临时等级/30)+1
   if self.临时人数>4 then self.临时人数=4 end
  elseif 技能名称=="苍茫树" or 技能名称=="地裂火" or 技能名称=="靛沧海" or 技能名称=="日光华"  or 技能名称=="夺命咒" or 技能名称=="雨落寒沙"  then

   self.临时人数=math.floor(self.临时等级/40)+2
    if 技能名称 ~= "雨落寒沙" and 技能名称 ~= "夺命咒" and self:取经脉(攻击方,"五行制化") then

      self.临时人数 = 10
     end
     if 技能名称 == "夺命咒" and self:取经脉(攻击方,"内伤") then
      self.临时人数 = self.临时人数 +1
     end
  elseif 技能名称=="天雷斩" or 技能名称=="天罗地网"  then

   self.临时人数=math.floor(self.临时等级/30)+1
    if self:取经脉(攻击方,"迷意") then
      self.临时人数=self.临时人数+1
    end

   if self.临时人数>3 then self.临时人数=3 end
  elseif 技能名称=="破釜沉舟" or 技能名称=="雷霆万钧" or 技能名称 == "剑荡四方"  then
     self.临时人数=3
     if 技能名称 == "破釜沉舟" and self:取经脉(攻击方,"干将") and 取随机数() <= 20 then
      self.临时人数=4
     end
  elseif 技能名称=="二龙戏珠" or 技能名称=="摧心术"  then
     self.临时人数=2
     if 技能名称 == "二龙戏珠" and self:取经脉(攻击方,"龙珠") and 取随机数() <= 8 then
      self.临时人数=3
     end
  elseif 技能名称=="苍鸾怒击"   then
     self.临时人数=2

 elseif 技能名称=="飘渺式"   then

   self.临时人数=math.floor(self.临时等级/30)+1


   if self.临时人数>3 then self.临时人数=3 end

   if self:取玩家战斗() then

     self.临时人数=1
     end
 elseif 技能名称=="浪涌"   then

   self.临时人数=math.floor(self.临时等级/30)+1


   if self.临时人数>3 then self.临时人数=3 end

   if self:取玩家战斗() then

     self.临时人数=1
     end
 elseif 技能名称=="断岳势"   then

   self.临时人数=math.floor(self.临时等级/30)+1
   if self.临时人数>3 then
    self.临时人数=3
   end
    if self:取玩家战斗() then

     self.临时人数=1
     end
  elseif 技能名称=="腾雷"   then
    self.临时人数=1

    if self:取玩家战斗() then

     self.临时人数=1
     end

   elseif 技能名称=="惊涛怒"   then

   self.临时人数=math.floor(self.临时等级/30)+1
   if self.临时人数>3 then
    self.临时人数=3
   end
  elseif 技能名称=="连环击"  then
   self.临时人数=math.floor(self.临时等级/35)+2
  elseif 技能名称=="龙吟" or 技能名称=="四海升平" or 技能名称=="玉清诀" or 技能名称=="晶清诀" or 技能名称=="魔兽之印" or 技能名称=="圣灵之甲" or 技能名称=="罗汉金钟"  then
   self.临时人数=10
  elseif 技能名称=="泰山压顶" or 技能名称=="水漫金山" or 技能名称=="地狱烈火" or 技能名称=="奔雷咒" then
   self.临时人数=math.floor(self.临时等级/30)+1
   if self.临时人数>3 then self.临时人数=3 end
  else
   self.临时人数=1

   end
 return self.临时人数
 end
function 战斗处理类:取封印结果(攻击方,挨打方,技能名称)
  if self:取经脉(挨打方,"忍辱") then
    挨打方.愤怒 = 挨打方.愤怒 +6
    if 挨打方.愤怒>=150 then
      挨打方.愤怒 = 150
    end
  end
  if 技能名称=="反间之计" and 挨打方.允许反间==false then
   return false
  elseif 技能名称=="瘴气" or  技能名称=="魔息术" or 技能名称=="颠倒五行" or 技能名称=="灵动九天"  or 技能名称=="金刚镯" or 技能名称=="神龙摆尾" or 技能名称=="分身术" or 技能名称=="碎甲符" or 技能名称=="一笑倾城" or 技能名称=="后发制人" or 技能名称=="野兽之力" or 技能名称=="光辉之甲" or 技能名称=="太极护法" or 技能名称=="炼气化神" or 技能名称=="安神诀" or 技能名称=="佛法无边" or 技能名称=="蜜润"  then

    return true
  elseif  技能名称=="天地同寿" or 技能名称=="乾坤妙法" or 技能名称=="火甲术" then
     if self:取技能等级(攻击方,技能名称)<挨打方.等级 then
       return false
      else
       return true

       end

  elseif 挨打方.封印开关 then
   return false

 end
  if 技能名称=="含情脉脉" and 攻击方.蚩尤元神==1 and 取随机数()<=20 then

   return true

     end
 if self:取经脉(攻击方,"媚态") and (技能名称=="摄魄" or 技能名称=="勾魂") then
     return true
end
  if 挨打方.不可封印 then
   return false

    end
 self.初始几率=0
 self.等级差=self:取技能等级(攻击方,技能名称)-挨打方.等级

 if 技能名称=="含情脉脉" then

   self.初始几率=45
   if self:取经脉(攻击方,"鼓乐") then
     self.初始几率=55
   end
   if self:取经脉(攻击方,"望川") then
     self.初始几率=self.初始几率+8
   end
  elseif 技能名称=="似玉生香"   then

   self.初始几率=45
      if 攻击方.织女扇 ~= nil then
      self.初始几率 = self.初始几率 + 攻击方.织女扇
    end
  if self:取经脉(攻击方,"嫣然") then
    self.初始几率 =self.初始几率 +12
  end
   if 挨打方.法术状态组.宁心.有无 or 挨打方.法术状态组.解封.有无 then

     return false
     end

  elseif 技能名称=="尸腐毒"  then
   if  挨打方.法术状态组.百毒不侵.有无 or 挨打方.法术状态组.乾坤妙法.有无  then
     return false
    else
     self.初始几率=75
     end
  elseif 技能名称=="雾杀"  then
   if  挨打方.法术状态组.乾坤妙法.有无  then
     return false
    else
     self.初始几率=75
   end

  elseif 技能名称=="日月乾坤" then

   self.初始几率=40
  if self:取经脉(攻击方,"乾坤") then
    self.初始几率 =self.初始几率 +12
  end
  if self:取经脉(攻击方,"强击") then
    self.初始几率 =self.初始几率 -5
  end
  elseif 技能名称=="威慑" then

   self.初始几率=55
    if self:取经脉(攻击方,"威慑") then
    self.初始几率 =self.初始几率+15
  end
   if self:取玩家战斗() or self.战斗类型==100029 or self.战斗类型==100030 or self.战斗类型==100018 or self.战斗类型==100018 or self.战斗类型==100033 or self.战斗类型==100034 or self.战斗类型==100035 or self.战斗类型==100031 or  self.战斗类型==100037 or self.战斗类型==100038 then


     return false

     end

  elseif 技能名称=="百万神兵" then

   self.初始几率=55
   if 挨打方.法术状态组.复苏.有无 then

      return false

     end

  elseif 技能名称=="镇妖" then

   self.初始几率=45
   if 挨打方.法术状态组.复苏.有无 then

      return false

     end
  elseif 技能名称=="莲步轻舞" then

   self.初始几率=45
   if 挨打方.法术状态组.宁心.有无 or 挨打方.法术状态组.解封.有无 then

     return false
     end
  elseif 技能名称=="如花解语" then

   self.初始几率=55
      if 攻击方.织女扇 ~= nil then
      self.初始几率 = self.初始几率 + 攻击方.织女扇
    end
   if 挨打方.法术状态组.宁心.有无 or 挨打方.法术状态组.解封.有无 then

     return false
     end
  elseif 技能名称=="失魂符" then

   self.初始几率=50

  elseif 技能名称=="夺魄令" then

   self.初始几率=50
   if self:取经脉(攻击方,"反先") then
    self.初始几率=55
   end
  elseif 技能名称=="定身符" or 技能名称=="追魂符" or 技能名称=="失心符" or 技能名称=="离魂符" then

   self.初始几率=50
  elseif 技能名称=="失忆符" then

   self.初始几率=50
   elseif 技能名称=="催眠符" then

   self.初始几率=45
   if self:取经脉(攻击方,"不倦") then
    self.初始几率=55
   end
   elseif 技能名称=="煞气诀" then

   self.初始几率=60
   if self:取经脉(攻击方,"救人") then
    self.初始几率=70
   end
  elseif 技能名称=="镇妖" then

   self.初始几率=50
  elseif 技能名称=="天神护体" then
    return true
  elseif 技能名称=="变身" or 技能名称=="后发制人" or 技能名称=="杀气诀" or 技能名称=="楚楚可怜" or 技能名称=="乘风破浪" or 技能名称=="炼气化神" or 技能名称=="普渡众生" or 技能名称=="法术防御" then

   return true

  elseif 技能名称=="错乱" or 技能名称=="摄魄" or 技能名称=="勾魂" then

   self.初始几率=50
   if 技能名称 == "错乱" then
      if self:取经脉(攻击方,"画地为牢") then
          self.初始几率=65
      end
   end
   end

 if 挨打方.法术状态组.乾坤妙法.有无 then
    return false
  end

  self.初始几率 = self.等级差 + self.初始几率 + 攻击方.修炼数据.法术 + 攻击方.封印命中等级 / 10

  if 攻击方.封印必中 then
    self.初始几率 = self.初始几率 + 100
  end

  if 攻击方.抵抗封印等级 ~= nil then
     self.初始几率  =  self.初始几率  - 攻击方.抵抗封印等级 / 10
  end

  if self.初始几率 > 95 then
    self.初始几率 = 95
  end

  if self.初始几率 < 5 then
    self.初始几率 = 5
  end

  -- if self.封印 == nil then self.封印 = 0 end
  if 挨打方.复活 == nil then 挨打方.复活 = 0 end
  if 挨打方.神迹 == nil then 挨打方.神迹 = 0 end
  if 取随机数() <= self.初始几率 and 挨打方.复活 == 0 and 挨打方.神迹 ~= 2 then
    return true
  else
    return false
  end
end

function 战斗处理类:死亡处理()

  for n=1,#self.参战单位 do

    if self.参战单位[n]~=nil and self.参战单位[n].队伍id~=0 and self.参战单位[n].当前气血<=0 then

     if  self.参战单位[n].战斗类型=="角色"  then

        玩家数据[self.参战单位[n].玩家id].角色.数据.当前气血=玩家数据[self.参战单位[n].玩家id].角色.数据.气血上限
        玩家数据[self.参战单位[n].玩家id].角色.数据.当前魔法=玩家数据[self.参战单位[n].玩家id].角色.数据.魔法上限
        玩家数据[self.参战单位[n].玩家id].角色.数据.愤怒=0
        发送数据(玩家数据[self.参战单位[n].玩家id].连接id,1003,玩家数据[self.参战单位[n].玩家id].角色:获取角色气血数据())
       if self.死亡惩罚 then
         玩家数据[self.参战单位[n].玩家id].角色:死亡处理(1,self.参战单位[n].玩家id,self.战斗类型,self.参战单位[n].死亡消息)
         end
        elseif 玩家数据[self.参战单位[n].玩家id]~=nil and 玩家数据[self.参战单位[n].玩家id].召唤兽.数据.参战~=0 then
          --print(玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].神兽)
          self.召唤兽id=玩家数据[self.参战单位[n].玩家id].召唤兽.数据.参战
          玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].当前气血=玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].气血上限
          玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].当前魔法=玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].魔法上限
          if self.死亡惩罚 then
          玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].忠诚=玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].忠诚-5

          if 玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].神兽~=true then

            玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].寿命=玩家数据[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].寿命-25

            end
             end

         self.发送信息={
         当前气血=self.参战单位[n].气血上限,
         气血上限=self.参战单位[n].气血上限,
         当前魔法=self.参战单位[n].魔法上限,
         魔法上限=self.参战单位[n].魔法上限
       }

         发送数据(玩家数据[self.参战单位[n].玩家id].连接id,2015,self.发送信息)
       end
      end
    end
end
function 战斗处理类:强制结束战斗()
  self.死亡惩罚=false
  if self.任务di~=nil and 任务数据[self.任务id]~=nil then

   任务数据[self.任务id].战斗=false
  end
  self:发送结束信息()
 end
function 战斗处理类:结束战斗(队伍id)
 self.回合进程="结束回合"
 if 任务数据[self.任务id]~=nil then

   任务数据[self.任务id].战斗=false
   end
 self.战斗计时=os.time()-self.战斗计时
 self:死亡处理()
 self:回天丹处理()
 self:胜利结算(队伍id)
 self:失败结算(队伍id)
end
function 战斗处理类:回天丹处理()
  if self.战斗类型==100033 or self.战斗类型==100034 or self.战斗类型==100035 or self.战斗类型==100036 or self.战斗类型==100044  then
    return 0
   end

 for n=1,#self.参战玩家 do
    self.回天丹id=玩家数据[self.参战玩家[n].玩家id].角色:取任务id(402)
   if self.回天丹id~=0 and 任务数据[self.回天丹id]~=nil  then


     任务数据[self.回天丹id].次数=任务数据[self.回天丹id].次数-1
     if 任务数据[self.回天丹id].次数<=0 then

        玩家数据[self.参战玩家[n].玩家id].角色:取消任务(self.回天丹id)
        任务数据[self.回天丹id]=nil
        发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,7,"#y/你的回天丹效果到期了")

      else

        发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,7,"#y/你的回天丹还可在#r/"..任务数据[self.回天丹id].次数.."#y/场战斗中生效")

       end
      玩家数据[self.参战玩家[n].玩家id].角色:恢复血魔(self.参战玩家[n].玩家id,1)
      发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,2017,玩家数据[self.参战玩家[n].玩家id].角色:获取角色数据())
      if 玩家数据[self.参战玩家[n].玩家id].召唤兽.数据.参战~=0 then

       玩家数据[self.参战玩家[n].玩家id].召唤兽:恢复状态(3,玩家数据[self.参战玩家[n].玩家id].召唤兽.数据.参战)
       发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,2005,玩家数据[self.参战玩家[n].玩家id].召唤兽:取头像数据())
        end
     end
   end
 end
function 战斗处理类:发送结束信息()
 for n=1,#self.观战玩家 do
   if self.观战玩家[n]~=0 then
     玩家数据[self.观战玩家[n]].战斗=0
     玩家数据[self.观战玩家[n]].观战模式=false
     发送数据(玩家数据[self.观战玩家[n]].连接id,6007,"66")
     end
   end
  for n=1,#self.参战玩家 do
    玩家数据[self.参战玩家[n].玩家id].战斗=0
    地图处理类:更改战斗(self.参战玩家[n].玩家id,false)
    发送数据(玩家数据[self.参战玩家[n].玩家id].连接id,6007,"66")
    玩家数据[self.参战玩家[n].玩家id].遇怪时间={起始=os.time(),间隔=取随机数(8,10)}
    if 玩家数据[self.参战玩家[n].玩家id].下线标记 then
     网络处理类:退出处理(self.参战玩家[n].玩家id)
      end
    end
  if self.飞升序号~=0 then
     发送数据(玩家数据[self.进入战斗玩家id].连接id,7,"#y/您当前正在进行飞升挑战，该挑战将连续进行三场战斗")
     战斗准备类:进入处理(self.进入战斗玩家id,self.飞升序号,"66",1)
  elseif self.战斗类型==100036 and self.战斗失败==false then
      if 任务数据[self.任务id].场次<20 then
        任务数据[self.任务id].场次=任务数据[self.任务id].场次+1
          self.基础积分=任务数据[self.任务id].场次*2
     任务数据[self.任务id].积分=任务数据[self.任务id].积分+self.基础积分*self.死亡人数+math.floor(玩家数据[self.进入战斗玩家id].角色.数据.等级/10)
     广播队伍消息(self.进入战斗玩家id,7,"#y/您当前的积分为#r/"..任务数据[self.任务id].积分.."#y/积分越高获得的奖励也越高")
        广播队伍消息(self.进入战斗玩家id,7,"#y/当前正在进入第#r/"..任务数据[self.任务id].场次.."#y/次战斗")
        战斗准备类:进入处理(self.进入战斗玩家id,100036,"66",self.任务id)
      else
        任务处理类:完成无限轮回任务(self.进入战斗玩家id,self.任务id)
      end
     end
     self.结束条件=true

end
function 战斗处理类:取符合id(id)
  self.返回id={}
  for n=1,#self.参战单位 do

   if self.参战单位[n].战斗类型=="角色" and self.参战单位[n].队伍id==id then

     self.返回id[#self.返回id+1]=self.参战单位[n].玩家id

      end
   end
  return self.返回id
 end
function 战斗处理类:宝图遇怪奖励(id组)
  for n=1,#id组 do
    self.添加经验=取随机数(2000,5000)
    玩家数据[id组[n]].角色:添加银子(id组[n],self.添加经验,5)
    发送数据(玩家数据[id组[n]].连接id,9,"#dq/#w/获得"..self.添加经验.."两银子")
    if 取随机数()<=20 then

      玩家数据[id组[n]].装备:取随机装备(id组[n],取随机数(3,8),true)
      end
    end

 end
function 战斗处理类:添加野外经验(id组)
 self.临时经验=self.地图经验*(self.队伍数量[2]/10)*2
 self.临时经验=self.队伍数量[1]*0.2*self.临时经验
  for n=1,#id组 do
    self.角色等级=玩家数据[id组[n]].角色.数据.等级
    self.等级差=取经验差(self.地图等级,self.角色等级)
   if self.角色等级>self.地图等级 then
     self.等级差=1
     end
    self.添加经验=math.floor(self.临时经验*self.等级差)
    玩家数据[id组[n]].角色:添加经验(self.添加经验,id组[n],1)

    if 取随机数()<=3 then
       玩家数据[id组[n]].装备:取随机装备(id组[n],self.地图物品,false)
      end
    if 玩家数据[id组[n]].召唤兽.数据.参战~=0 then
      self.召唤兽id=玩家数据[id组[n]].召唤兽.数据.参战
      self.角色等级=玩家数据[id组[n]].召唤兽.数据[self.召唤兽id].等级
      self.等级差=取经验差(self.地图等级,self.角色等级)
      self.添加经验=math.floor(self.临时经验*0.75*self.等级差)
      玩家数据[id组[n]].召唤兽:添加经验(self.添加经验,id组[n],self.召唤兽id,1)
      end
    end
 end
function 战斗处理类:胜利结算(队伍id)

 if 队伍id==0 then

   return 0
  else

   self.胜利id组=self:取符合id(队伍id)

   if self.战斗类型==100001 then

      self:添加野外经验(self.胜利id组)
    elseif self.战斗类型==100002 then
      任务处理类:完成治安任务(self.任务id)

    elseif self.战斗类型==100003 then
      任务处理类:完成打图任务(self.任务id)
    elseif self.战斗类型==100004 then

      self:宝图遇怪奖励(self.胜利id组)
    elseif self.战斗类型==100005 then
      任务处理类:完成封妖任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100006 then
      任务处理类:完成抓鬼任务(self.胜利id组,self.任务id)
    elseif self.战斗类型==100007 then
      任务处理类:完成星宿任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100008 then
      任务处理类:完成皇宫飞贼任务(self.胜利id组,self.任务id)
    elseif self.战斗类型==100009 then
      任务处理类:回答科举题目(self.参战玩家[1].玩家id,-1)
    elseif self.战斗类型==100010 then
     任务处理类:完成官职飞贼任务(self.参战玩家[1].玩家id,self.任务id)
    elseif self.战斗类型==100011 then
      任务处理类:完成降妖伏魔任务(self.任务id,self.胜利id组)
    elseif self.战斗类型== 100890 then
     任务处理类:完成天降横财(self.任务id,self.胜利id组)
    elseif self.战斗类型== 20200 then
     任务处理类:完成雪人任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100012 then
      任务处理类:完成门派闯关任务(self.胜利id组,self.任务id)
    elseif self.战斗类型==100013 then

      任务处理类:完成任务链(任务数据[self.任务id].id,self.任务id)
    elseif self.战斗类型==100014 then
     任务数据[self.任务id].次数=任务数据[self.任务id].次数-1
     玩家数据[任务数据[self.任务id].id].遇怪时间.起始=os.time()
     if 任务数据[self.任务id].次数<=0 then

        任务处理类:完成师门任务(任务数据[self.任务id].id)
      else
        发送数据(玩家数据[任务数据[self.任务id].id].连接id,7,"#y/嘿嘿，换个地方我继续捣乱")
        任务数据[self.任务id].起始=os.time()
        任务数据[self.任务id].战斗=取随机数(10,30)


       end

    elseif self.战斗类型==100015 then

      任务处理类:完成铃铛任务(self.胜利id组[1],self.任务id)
    elseif self.战斗类型==100099 then
      玩家数据[self.参战玩家[1].玩家id].角色.数据.突破=0
      广播消息(9,"#xt/#r/天降祥云，勇者归来。恭喜#g/"..玩家数据[self.参战玩家[1].玩家id].角色.数据.名称.."#r/在南极仙翁处完成了100级突破挑战，除了获得丰厚的奖励外，等级上限也上升至89级。")

    elseif self.战斗类型==100017 then
      任务处理类:完成50级boos(self.胜利id组,self.任务id)
    elseif self.战斗类型==100018 then
      任务处理类:完成100级boos(self.胜利id组,self.任务id)
    elseif self.战斗类型==100019 then
      任务处理类:完成大雁塔(self.胜利id组,1,self.战斗计时)
    elseif self.战斗类型==100020 then
      任务处理类:完成大雁塔(self.胜利id组,2,self.战斗计时)
    elseif self.战斗类型==100021 then
      任务处理类:完成大雁塔(self.胜利id组,3,self.战斗计时)
    elseif self.战斗类型==100022 then
      任务处理类:完成大雁塔(self.胜利id组,4,self.战斗计时)
    elseif self.战斗类型==100023 then
      任务处理类:完成大雁塔(self.胜利id组,5,self.战斗计时)
    elseif self.战斗类型==100024 then
      任务处理类:完成大雁塔(self.胜利id组,6,self.战斗计时)
     elseif self.战斗类型==100026 then
      任务处理类:完成劳动节任务(self.胜利id组,self.任务id)
    elseif self.战斗类型==100027 then
      任务处理类:完成劳动节任务2(self.胜利id组,self.任务id)
    elseif self.战斗类型==100028 then
      任务处理类:完成劳动节任务3(self.胜利id组,self.任务id)

    elseif self.战斗类型==200002 then

      if self.观战方==self.胜利id组[1] then

       游戏活动类:首席争霸战斗处理(self.观战方,self.对战方)

      else

        游戏活动类:首席争霸战斗处理(self.对战方,self.观战方)


        end
   elseif self.战斗类型==200004 then

      if self.观战方==self.胜利id组[1] then

       游戏活动类:比武大会战斗处理(self.观战方,self.对战方)

      else

        游戏活动类:比武大会战斗处理(self.对战方,self.观战方)


        end
    elseif self.战斗类型==8002 then
      任务处理类:完成芭蕉木妖(self.胜利id组,self.任务id)

    elseif self.战斗类型==8003 then
      任务处理类:完成三妖挑战(self.胜利id组,1)
    elseif self.战斗类型==8004 then
      任务处理类:完成三妖挑战(self.胜利id组,2)
    elseif self.战斗类型==8005 then
      任务处理类:完成三妖挑战(self.胜利id组,3)
    elseif self.战斗类型==8006 then
      任务处理类:完成鬼祟小妖(self.胜利id组,self.任务id)
    elseif self.战斗类型==8007 then
      任务处理类:完成国王挑战(self.胜利id组,1)
    elseif self.战斗类型==8008 then
      任务处理类:完成国王挑战(self.胜利id组,2)
    elseif self.战斗类型==8009 then
      任务处理类:完成贡品(self.胜利id组,self.任务id)
    elseif self.战斗类型==8010 then
      任务处理类:完成三清天尊(self.胜利id组,self.任务id)
    elseif self.战斗类型==8011 then
      任务处理类:完成双不动(self.胜利id组,self.任务id)
    elseif self.战斗类型==8012 then
      任务处理类:完成三妖大仙(self.胜利id组,self.任务id)
    elseif self.战斗类型==8012 then
      任务处理类:完成三妖大仙(self.胜利id组,self.任务id)

    elseif self.战斗类型==100033 then
       self.飞升序号=100034
     elseif self.战斗类型==100034 then
       self.飞升序号=100035
    elseif self.战斗类型==100035 then
      -- self.飞升序号=100034
      玩家数据[self.进入战斗玩家id].角色:飞升处理(self.进入战斗玩家id)
    elseif self.战斗类型==100029 or self.战斗类型==100030 or self.战斗类型==100031 or self.战斗类型==100041 then
     任务处理类:完成地妖星(self.胜利id组,self.任务id)

    elseif self.战斗类型==100088 then
      任务处理类:完成上古凶兽任务(self.任务id,self.胜利id组)
          elseif self.战斗类型==100089 then
      任务处理类:完成勿忘国耻任务(self.任务id,self.胜利id组)
         elseif self.战斗类型==100889 then
      任务处理类:完成传奇任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==200003 then
      for n=1,#self.胜利id组 do
       if self.发起id==self.胜利id组[n] then
       if 天罚数据表[self.胜利id组[n]]==nil then

         天罚数据表[self.胜利id组[n]]=0
         end
        天罚数据表[self.胜利id组[n]]=天罚数据表[self.胜利id组[n]]+1
        玩家数据[self.胜利id组[n]].角色:添加系统消息(self.胜利id组[n],"#h/你因为参加主动发起的pk战斗而被扣除了阴德值，阴德值过低会遭遇天罚。阴德值在每日中午12点刷新")

        end
        end

    elseif self.战斗类型==100037 then
      任务处理类:完成150级boos(self.胜利id组,self.任务id)

    elseif self.战斗类型==100038 then
      任务处理类:完成天罡星(self.胜利id组,self.任务id)

    elseif self.战斗类型==100039 then

     任务数据[self.任务id].战斗序列[任务数据[self.任务id].次数]=true
    elseif self.战斗类型==100040 then
      任务处理类:完成玄武堂任务(self.任务id)

    elseif self.战斗类型==100042 then
      任务处理类:完成妖魔头领(self.任务id,self.胜利id组)

       elseif self.战斗类型==100043 then
      任务处理类:完成师门守卫(self.任务id,self.胜利id组)
    elseif self.战斗类型==100045 then
      任务处理类:完成天帝任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100046 then
      任务处理类:完成魔帝任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100047 then
      任务处理类:完成超凡仙人任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100048 then
      任务处理类:完成超凡圣人任务(self.任务id,self.胜利id组)
    elseif self.战斗类型==100059 then
      任务处理类:完成天佑星(self.胜利id组,self.任务id)
    elseif self.战斗类型 == 100049 then
      任务处理类:完成渡劫1(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100050 then
      任务处理类:完成渡劫2(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100051 then
      任务处理类:完成渡劫3(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100052 then
      任务处理类:完成渡劫4(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100053 then
      任务处理类:完成渡劫5(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100054 then
      任务处理类:完成渡劫6(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100055 then
      任务处理类:完成渡劫7(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100056 then
      任务处理类:完成渡劫8(self.胜利id组[1],self.任务id)
    elseif self.战斗类型 == 100057 then
      任务处理类:完成渡劫9(self.胜利id组[1],self.任务id)
     end
   end
 self.回合进程="结束回合1"
 end
function 战斗处理类:失败结算(队伍id)
 if self.队伍区分[1]==队伍id then
   self.失败id=self.队伍区分[2]
  else
   self.失败id=self.队伍区分[1]
   end
 if self.失败id==0 then
   return 0
   end
   self.战斗失败=true
 if self.战斗类型==100009 then

    任务处理类:回答科举题目(self.参战玩家[1].玩家id,0)
  elseif self.战斗类型==100014 then
    任务数据[self.任务id].起始=os.time()
    任务数据[self.任务id].战斗=取随机数(10,30)
  elseif self.战斗类型==100043 or self.战斗类型==100038 or  self.战斗类型==100018 or  self.战斗类型==100889 or self.战斗类型==100017 or self.战斗类型==100037 or self.战斗类型==100006 or self.战斗类型==100002 or self.战斗类型==100003 or self.战斗类型==100005 then

    任务数据[self.任务id].战斗=false

    elseif self.战斗类型==100036  then
      任务数据[self.任务id].战斗=false
      if 任务数据[self.任务id].场次> 1 and 玩家数据[self.进入战斗玩家id].队伍~=0 then
        广播队伍消息(self.进入战斗玩家id,7,"#y/当前完成#r/"..任务数据[self.任务id].场次.."#y/次战斗")
         广播队伍消息(self.进入战斗玩家id,7,"#y/当前积分为#r/"..任务数据[self.任务id].积分.."#y/点积分")
        任务处理类:完成无限轮回任务(self.进入战斗玩家id,self.任务id)
      end
  elseif self.战斗类型==100029 or self.战斗类型==100030 or self.战斗类型==100031 or self.战斗类型==100041 then
    任务数据[self.任务id].战斗=false
    -- self.组合名称=""
    -- for n=1,#self.参战玩家 do

    --  self.组合名称=self.组合名称..玩家数据[self.参战玩家[n].玩家id].角色.数据.名称
    --  if n~=#self.参战玩家 then

    --    self.组合名称=self.组合名称.."、"

    --    end
    --   end
    --   广播消息("#xt/#y/据可靠消息#g/"..self.组合名称.."#y/不自量力的去挑战#r/"..任务数据[self.任务id].名称.."#y/，结果被打的爹妈都不认识了")

  end
  self.回合进程="结束回合1"
 end
return 战斗处理类