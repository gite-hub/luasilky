local 角色处理类 = class()

function 角色处理类:初始化(id, ip)
	self.玩家id = id
	self.玩家ip = ip
	self.在线时间 = 0
	self.小时 = 0
	self.分 = 0
	self.秒 = 0
end
function 角色处理类:设置快捷技能(类型,技能,格子)
  if self.数据.快捷技能==nil then
      self.数据.快捷技能={}
  end
   if 类型==2 then
    for i=1,8 do
     if  self.数据.快捷技能[i]~=nil and self.数据.快捷技能[i].名称 == 技能 then
       self.数据.快捷技能[i] =nil
     end
    end
  else
      for i=1,#self.数据.门派技能 do
     if  self.数据.门派技能[i].名称 == 技能 then
       self.数据.快捷技能[格子] =table.copy(self.数据.门派技能[i])
       break
     end
    end


  end
  发送数据(玩家数据[self.数据.id].连接id,2074,self:获取快捷技能数据())
end
function 角色处理类:经脉处理(id)
	if self.数据.奇经八脉 == nil then
		self.数据.奇经八脉 = {}
	end
	if self.数据.乾元丹 == nil then
		self.数据.乾元丹 = {可用乾元丹=0,已用乾元丹=0}
	end
	return {门派 =self.数据.门派 ,乾元丹=self.数据.乾元丹,技能树 = self.数据.技能树,造型 = self.数据.造型,经脉 = self.数据.奇经八脉 }
end

function 角色处理类:清空奇经八脉(id)
	self.数据.奇经八脉 = {}
	self.数据.开启奇经八脉 = nil
	self.数据.技能树 = nil
end


function 角色处理类:增加奇经八脉(位置)
	if self.数据.门派~=nil and self.数据.门派~="无" then
		local 读取 = self:提取奇经八脉(self.数据.门派)
		if 读取~=nil and 读取[位置]~=nil and 读取[位置]~=1 then
			if self.数据.乾元丹~=nil and self.数据.乾元丹.可用乾元丹>0 then
				self.数据.乾元丹.可用乾元丹=self.数据.乾元丹.可用乾元丹-1
				self.数据.乾元丹.已用乾元丹=self.数据.乾元丹.已用乾元丹+1
				self.数据.奇经八脉[读取[位置]]=1
				if 位置 >= 19 then
					self.数据.奇经八脉[读取[20]]=1
				end
				self.数据.开启奇经八脉=true
				self.数据.技能树 = self:技能树(位置) or 1
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/恭喜你又学会一个新的经脉")
			else
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你的乾元丹不够本次学习使用")
			end
		  	return
		end
	end
end


function 角色处理类:技能树(a)
  if a == 1 or a == 4 or a == 7 or a == 10 or a == 13 or a == 16 then
    if a == 16 then
      return {a+3}
    else
      return {a+3,a+4}
    end
  elseif a == 2 or a == 5 or a == 8 or a == 11 or a == 14 or a == 17 then
    if a == 17 then
      return {a+2,a+4}
    else
      return {a+2,a+3,a+4}
    end
  elseif a == 3 or a == 6 or a == 9 or a == 12 or a == 15 or a == 18 then
    if a == 18 then
      return {a+3}
    else
      return {a+2,a+3}
    end
  end
end
function 角色处理类:提取奇经八脉(mp)
  if mp == "方寸山" then
    return {"苦缠","雷动","奔雷","黄粱","不舍","鬼怮","补缺","不倦","不灭","化身","调息","批亢","斗法","吐纳","飞符炼魂","鬼念","碎甲","摧心","顺势而为","额外能力","钟馗论道"}
  elseif mp == "女儿村" then
    return {"自矜","暗伤","杏花","花舞","倩影","淬芒","傲娇","花护","天香","国色","鸿影","轻霜","机巧","毒引","毒雾","嫣然","磐石","花殇","碎玉弄影","额外能力","鸿渐于陆"}
  elseif mp == "神木林" then
    return {"风灵","法身","咒术","灵压","咒法","蔓延","鞭挞","月影","不侵","风魂","冰锥","血契","滋养","纯净","破杀","追击","灵能","寄生","风卷残云","额外能力","凋零之歌"}
  elseif mp == "化生寺" then
    return {"止戈","销武","佛屠","佛誉","仁心","聚气","佛显","心韧","归气","天照","舍利","感念","慈针","佛性","妙悟","慈心","映法","流刚","渡劫金身","额外能力","诸天看护"}
  elseif mp == "大唐官府" then
    return {"目空","风刃","扶阵","翩鸿一击","勇武","长驱直入","杀意","念心","静岳","干将","勇念","神凝","狂狷","不惊","突进","破空","历战","连破","无敌","额外能力","破军"}
  elseif mp == "阴曹地府" then
    return {"判官","回旋","夜行","入骨","聚魂","六道无量","索魂","伤魂","百炼","黄泉","毒印","百爪狂杀","幽光","泉爆","鬼火","魂飞","汲魂","击破","魑魅缠身","额外能力","夜之王者"}
  elseif mp == "盘丝洞" then
    return {"鼓乐","妖气","怜心","迷瘴","粘附","意乱","绝殇","安抚","忘忧","忘川","迷梦","倾情","情劫","脱壳","迷意","结阵","媚态","利刃","落花成泥","额外能力","偷龙转凤"}
  elseif mp == "无底洞" then
    return {"金莲","追魂","回敬","由己渡人","自愈","陷阱","化莲","风墙","御兽","精进","救人","灵身","持戒","内伤","反先","忍辱","暗潮","噬魂","同舟共济","额外能力","妖风四起"}
  elseif mp == "魔王寨" then
    return {"充沛","炙烤","震怒","蚀天","敛恨","赤暖","火神","震天","神焰","神炎","返火","崩摧","魔冥","燃魂","狂月","威吓","连营","魔心","魔焰滔天","额外能力","烈焰真诀"}
  elseif mp == "狮驼岭" then
    return {"爪印","翼展","驭兽","化血","宁息","兽王","狮吼","怒象","鹰啸","乱破","魔息","威慑","狂袭","失心","羁绊","死地","乱击","悲恸","背水一战","额外能力","肝胆相照"}
  elseif mp == "天宫" then
    return {"威吓","疾雷","轰鸣","趁虚","余韵","驭意","震慑","神念","伏魔","苏醒","护佑","电芒","月桂","怒火","灵光","神律","洞察","雷波","画地为牢","额外能力","风雷韵动"}
  elseif mp == "普陀山" then
    return {"化戈","推衍","借灵","莲花心音","生克","默诵","劳心","普渡","化怨","甘露","法印","秘术","灵动","雨润","莲心剑意","玉帛","道衍","法咒","波澜不惊","额外能力","五行制化"}
  elseif mp == "凌波城" then
    return {"山破","战诀","抗击","聚气","神诀","魂聚","神躯","斩魔","不动","力战","破击","怒涛","海沸","怒火","煞气","强袭","雷附","再战","天神怒斩","额外能力","真君显灵"}
  elseif mp == "五庄观" then
    return {"体恤","锤炼","神附","心浪","养生","强击","归本","修心","混元","修身","同辉","雨杀","乾坤","意境","傲视","陌宝","心随意动","致命","清风望月","额外能力","天命剑法"}
  elseif mp == "龙宫" then
    return {"波涛","破浪","云霄","逐浪","踏涛","清吟","天龙","龙珠","龙骇","龙慑","傲翔","飞龙","戏珠","回魔","月魂","汹涌","龙魄","摧意","亢龙归海","额外能力","雷浪穿云"}
  end
end

function 角色处理类:获取快捷技能数据()
  self.发送数据={
  快捷技能 =self.数据.快捷技能
  }
  return self.发送数据
 end
function 角色处理类:获取开运数据()
  self.发送数据={
  银子 = self.数据.道具.货币.银子,
  体力 = self.数据.当前体力
 }
 for n=1,20 do
     if self.数据.道具.包裹[n]~=nil and 玩家数据[self.数据.id].道具.数据[self.数据.道具.包裹[n]]~=nil then
         self.发送数据[n]=玩家数据[self.数据.id].道具.数据[self.数据.道具.包裹[n]]
       end
     end
  return self.发送数据
 end
function 角色处理类:获取称谓数据()
	return self.数据.称谓
end

function 角色处理类:取坐骑数据()
	return {
		坐骑 = self.数据.坐骑数据,
		参战 = self.数据.坐骑编号
	}
end

function 角色处理类:获取种族(q)
	if q == 1 or q == 2 or q == 3 or q == 4 or q == 5 or q == 6 then
		return "人"
	elseif q == 7 or q == 8 or q == 9 or q == 10  or q == 11  or q == 12 or q == 13  then
		return "魔"
	else
		return "仙"
	end
end
function 角色处理类:获取性别(q)
	if q == "剑侠客" or q == "虎头怪" or q == "巨魔王" or q == "神天兵" or q == "逍遥生" or q == "龙太子" or q == "偃无师" or q == "羽灵神" or q == "杀破狼"then
		return "男"
	else
		return "女"
	end
end

function 角色处理类:添加经验(数额, id, 类型, 提示)
	if 类型 ~= 26 and 类型 ~= 8 and 类型 ~= 10 and 类型 ~= 15 and 类型 ~= 7 and 类型 ~= 16 and 类型 ~= 17 and 类型 ~= 999 and 类型 ~= 401 and 类型 ~= 402 and 类型 ~= 405 then
		self.临时数额 = 数额 * 服务端参数.经验获得率
	else
		self.临时数额 = 数额
	end

	if self.数据.双倍  then
		self.临时数额 = self.临时数额 * 2
	end
	if self.数据.通灵宝玉 ~= nil then
		self.临时数额 = self.临时数额*(1 + self.数据.通灵宝玉)
	end
	self.数据.当前经验 = self.数据.当前经验 + self.临时数额
	self.数据.获得经验 = self.数据.获得经验 + self.临时数额

	发送数据(玩家数据[id].连接id, 9, "#dq/#w/ 获得" .. self.临时数额 .. "点经验")
	发送数据(玩家数据[id].连接id, 1003, self:获取角色气血数据())
	发送数据(玩家数据[id].连接id, 2017, "66")
	self:添加消费日志("获得经验：" .. self.临时数额 .. ",来源代号" .. 类型)

	updateRanking(玩家数据[id].数字id, self.数据)
end
function 角色处理类:增加在线时间()
	self.秒 = self.秒 + 1

	if self.秒 >= 60 then
		self.秒 = 0
		self.分 = self.分 + 1

		if 取是否整除(self.分, 5) then
			self.数据.当前体力 = self.数据.当前体力 + math.floor(self.数据.体力上限 * 0.05)
			if self.数据.体力上限 < self.数据.当前体力 then
				self.数据.当前体力 = self.数据.体力上限
			end

			self.数据.当前活力 = self.数据.当前活力 + math.floor(self.数据.活力上限 * 0.05)

			if self.数据.活力上限 < self.数据.当前活力 then
				self.数据.当前活力 = self.数据.活力上限
			end
		end

		if 取是否整除(self.分, 10) then
			self.数据.人气 = self.数据.人气 + 1

			if self.数据.礼包[704] == nil and self.数据.人气 >= 710 then
				self.数据.礼包[704] = 1
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了新手人气奖励")
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了100万经验")
				self:添加系统消息(self.玩家id, "#h/您的新手人气奖励已经发放，本次发放的奖励内容为：1000000点经验")
			elseif self.数据.礼包[705] == nil and self.数据.人气 >= 750 then
				self.数据.礼包[705] = 1
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了新手人气奖励")
				发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了500万经验")
				self:添加系统消息(self.玩家id, "#h/您的新手人气奖励已经发放，本次发放的奖励内容为：5000000点经验")
			elseif self.数据.礼包[706] == nil and self.数据.人气 >= 800 then
				self.数据.礼包[706] = 1
				-- 发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了新手人气奖励")
				-- 发送数据(玩家数据[self.玩家id].连接id, 7, "#y/你获得了120级装备礼包")
				-- self:添加系统消息(self.玩家id, "#h/您的新手人气奖励已经发放，本次发放的奖励内容为：120级装备礼包")
			end

			if self.数据.人气 > 800 then
				self.数据.人气 = 800
			end
		end

		if self.分 >= 60 then
			self.分 = 0
			self.小时 = self.小时 + 1
		end
	end

	self.在线时间 = self.小时 .. "时" .. self.分 .. "分" .. self.秒 .. "秒"
end

function 角色处理类:加载数据(q, w, e)
	self.数据 = table.loadstring(q)
	if self.数据.名称 == "" or self.数据.名称 == nil or string.len(self.数据.名称) > 15 then
		self.数据.名称 = "1名字" .. self.数据.id
	elseif string.find(self.数据.名称, "/") ~= nil then
		self.数据.名称 = "2名字" .. self.数据.id
	elseif string.find(self.数据.名称, "/") ~= nil then
		self.数据.名称 = "3名字" .. self.数据.id
	elseif string.find(self.数据.名称, "@") ~= nil then
		self.数据.名称 = "4名字" .. self.数据.id
	elseif string.find(self.数据.名称, "#") ~= nil then
		self.数据.名称 = "5名字" .. self.数据.id
	elseif string.find(self.数据.名称, "*") ~= nil then
		self.数据.名称 = "6名字" .. self.数据.id
	end
	for n, v in pairs(self.数据.任务数据) do
		if 任务数据[self.数据.任务数据[n]] == nil then
			self.数据.任务数据[n] = nil
		elseif 任务数据[self.数据.任务数据[n]].类型 == 56 and 门派闯关开关 == false then
			self.数据.任务数据[n] = nil
		elseif 任务数据[self.数据.任务数据[n]].类型 == 4660 and os.time() - 任务数据[self.数据.任务数据[n]].起始 >= 7200 then
			self.数据.变身.造型 = nil
			任务数据[self.数据.任务数据[n]] = nil
			self.数据.任务数据[n] = nil
		end
	end

	for n = 21, 26 do
		if self.数据.装备数据[n] ~= nil or self.数据.装备数据[n] == 0 then
			for i = 1, 20 do
				if self.数据.道具.包裹[i] == self.数据.装备数据[n] then
					self.数据.道具.包裹[i] = nil
				end
			end

			for i = 1, 20 do
				if self.数据.道具.行囊[i] == self.数据.装备数据[n] then
					self.数据.道具.行囊[i] = nil
				end
			end
		end
	end

	for n = 1, 20 do
		if self.数据.道具.行囊[n] ~= nil or self.数据.道具.行囊[n] == 0 then
			for i = 1, 20 do
				if self.数据.道具.行囊[i] == self.数据.道具.行囊[n] and i ~= n then
					self.数据.道具.行囊[i] = nil
				end
			end
		end
	end

	for n = 1, 20 do
		if self.数据.道具.包裹[n] ~= nil or self.数据.道具.行囊[n] == 0 then
			for i = 1, 20 do
				if self.数据.道具.包裹[i] == self.数据.道具.包裹[n] and i ~= n then
					self.数据.道具.包裹[i] = nil
				end
			end
		end
	end
	if self.数据.传送地图 == nil then
		self.数据.传送地图 = 1501
	end

	if self.数据.点化次数 == nil then
		self.数据.点化次数 = 0
	end

	if self.数据.礼包记录[1006] == nil then
		-- Nothing
	end

	if self.数据.保护期 == nil then
		self.数据.保护期 = 0
	end

	if (self.数据.备用坐骑 ~= nil or self.数据.坐骑 ~= nil) and self.数据.坐骑异常 == nil then
		self:刷新战斗属性(1)
		self.数据.坐骑异常 = 1
	end

	if self.数据.pk保护 == nil then
		self.数据.pk开关 = false
		self.数据.pk时间 = 0
		self.数据.pk保护 = 0
	end

	self.临时参考 = self.数据.等级 * 5 + 10

	if self.数据.体力上限 ~= self.临时参考 then
		self.数据.体力上限 = self.临时参考
	end

	if self.数据.活力上限 ~= self.临时参考 then
		self.数据.活力上限 = self.临时参考
	end

	if self.数据.充值赠送 == nil then
		self.数据.充值赠送 = {
			坐骑 = 0,
			bb = 0,
			兽诀 = 0,
			武器 = 0
		}
	end
	if self:取任务id(4660) == 0 then
		self.数据.变身.造型 = nil
	end
	if self:取任务id(3819) == 0 then
		self.数据.双倍 = false
	end
	self.称谓序号 = 0

	for n = 1, #self.数据.称谓 do
		if self.数据.称谓[n] == self.数据.门派 .. "首席弟子" and self.数据.数字id ~= 首席资源数据[游戏活动类:取门派编号(self.数据.门派)].玩家id then
			-- Nothing
		end
	end

	if self.称谓序号 ~= 0 then
		-- Nothing
	end

	if self.数据.灵饰 == nil then
		self.数据.灵饰 = {[34.0] = 0,[31.0] = 0,[32.0] = 0,[33.0] = 0}
	end

	if self.数据.锦衣 == nil then
		self.数据.锦衣 = {[35] = 0,[36] = 0,[37] = 0,[38] = 0}
		self.数据.锦衣数据={}
	end

end

function 角色处理类:接受天罚(id)
	if 玩家数据[id].战斗 ~= 0 then
		return 0
	else
		if 玩家数据[id].队伍 ~= 0 then
			队伍处理类:离开队伍(玩家数据[id].队伍, id)
			发送数据(玩家数据[id].连接id, 7, "#y/你被强制离开了队伍")
		end

		广播消息(9, "#xt/#g/ " .. self.数据.名称 .. "#y/终日无所事事，只晓得以杀人为乐。为维护三界秩序，天庭特派执法天兵对其进行惩戒，希望其他玩家引以为戒")
		战斗准备类:进入处理(id, 100032, "66", 11)
	end
end

function 角色处理类:发送欢迎信息()

	发送数据(玩家数据[self.玩家id].连接id, 8)--, {传送数据 = 地图处理类.地图数据, NPC数据 = 地图数据类.npc, 地图数据 = 地图数据类.数据})

	if self.数据.离线ip == 管理ip then
		self.数据.离线ip = "游戏管理员"
	end

	广播消息("#xt/".."#r/"..self.数据.名称.."#y/上线#1")--#y/玩家
	if self.数据.离线时间 ~= nil then
		-- os.date("%Y", self.数据.离线时间) .. "年" ..
		-- self.显示时间 = os.date("%m", self.数据.离线时间) .. "月" .. os.date("%d", self.数据.离线时间) .. "日 " .. os.date("%X", self.数据.离线时间)
		-- 广播消息(self.数据.名称, 210)--#y/玩家
		-- 发送数据(玩家数据[self.玩家id].连接id, 10, os.time())
		-- 发送数据(玩家数据[self.玩家id].连接id, 6, self.数据.离线时间)-- .. "#y/。若非您本人登录，可能您的账号密码已经泄露，请及时修改自己的账号密码。")
		-- 发送数据(玩家数据[self.玩家id].连接id, 8, {传送数据 = 地图处理类.地图数据, NPC数据 = 地图数据类.npc, 地图数据 = 地图数据类.数据})
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#g/".."F1: 回师门")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#g/".."F2: 坐骑")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#g/".."F3: PK开关")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#g/".."F4: 强P开关")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#g/".."F8: 仙灵店铺")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#y/".."快捷键 Alt+A 攻击")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#y/".."快捷键 Alt+X 交易")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#y/".."快捷键 Alt+G 给予")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#y/".."快捷键 Alt+T 组队")
		-- 发送数据(玩家数据[self.玩家id].连接id, 9,"#xt/#y/".."快捷键 Alt+C 快捷键技能栏")
	end

	self.数据.离线时间 = os.time()
	self.数据.离线ip = self.玩家ip

	if 玩家数据[self.玩家id].游戏管理 ~= 0 and self.数据.离线ip ~= nil then
		-- Nothing
	end

	-- 发送时辰(玩家数据[self.玩家id].连接id)
	-- 发送数据(玩家数据[self.玩家id].连接id, 9, 欢迎内容)
	发送数据(0, 11, 游戏时辰.当前)

	if self.数据.法防 == nil then
		self:刷新装备属性(self.玩家id)
		self:刷新战斗属性(1)
	end
	任务处理类:刷新追踪任务信息(self.数据.id)
end

function 角色处理类:取消任务(内容)
	for n, v in pairs(self.数据.任务数据) do
		if self.数据.任务数据[n] == 内容 then
			table.remove(self.数据.任务数据,n)
		end
	end
	任务处理类:刷新追踪任务信息(self.数据.id)
end

function 角色处理类:取任务id(内容)
	for n, v in pairs(self.数据.任务数据) do
		if self.数据.任务数据[n] ~= nil and 任务数据[self.数据.任务数据[n]] ~= nil and 任务数据[self.数据.任务数据[n]].类型 == 内容 then
			return self.数据.任务数据[n]
		end
	end
	return 0
end

function 角色处理类:生成任务id()
	for n, v in pairs(self.数据.任务数据) do
		if self.数据.任务数据[n] == nil then
			return n
		end
	end

	return #self.数据.任务数据 + 1
end

function 角色处理类:加点事件(id, 内容)
	if 内容 ~= "体质" and 内容 ~= "魔力" and 内容 ~= "耐力" and 内容 ~= "敏捷" and 内容 ~= "力量" then
		return 0
	end

	if self.数据.潜能 <= 0 then
		发送数据(id, 7, "#y/你没有那么多可分配的属性点")
		return 0
	end

	self.数据.潜能 = self.数据.潜能 - 1
	self.数据[内容] = self.数据[内容] + 1

	self:刷新战斗属性(1)
	发送数据(玩家数据[id].连接id,2002,self:获取角色数据())
end

function 角色处理类:升级请求(id)
	if self.数据.门派 == "无" and self.数据.等级 >= 10 then
   self.对话内容=[[ 我可以帮助等级低于50级的玩家快速传送至想去的门派地图

  #R/ht|244-9-1/大唐官府    #R/ht|244-9-5/天宫       #R/ht|244-9-9/狮驼岭       #R/ht|244-9-13/凌波城       #R/ht|244-9-2/化生寺

  #R/ht|244-9-6/龙宫        #R/ht|244-9-10/魔王寨    #R/ht|244-9-14/神木林      #R/ht|244-9-3/女儿村        #R/ht|244-9-7/普陀山

  #R/ht|244-9-11/阴曹地府   #R/ht|244-9-15/无底洞    #R/ht|244-9-4/方寸山       #R/ht|244-9-8/五庄观        #R/ht|244-9-12/盘丝洞

  #R/ht|0/我喜欢走路
     ]]

		self.发送信息 = {
			名称 = "门派传送",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id,20, self.发送信息)

		return 0
   elseif self.数据.等级 == 69 then
   	self.对话内容=[[ 是否确认提升等级？

  #R/ht|244-9-16/确认

  #R/ht|0/我考虑考虑
     ]]

		self.发送信息 = {
			名称 = "提升等级",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id,20, self.发送信息)

		return 0
   elseif self.数据.等级 == 89 then
   	self.对话内容=[[ 是否确认提升等级？

  #R/ht|244-9-17/确认

  #R/ht|0/我考虑考虑
     ]]

		self.发送信息 = {
			名称 = "提升等级",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id,20, self.发送信息)

		return 0
   elseif self.数据.等级 == 109 then
   	self.对话内容=[[ 是否确认提升等级？

  #R/ht|244-9-18/确认

  #R/ht|0/我考虑考虑
     ]]

		self.发送信息 = {
			名称 = "提升等级",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id,20, self.发送信息)

		return 0
   elseif self.数据.等级 == 129 then
   	self.对话内容=[[ 是否确认提升等级？

  #R/ht|244-9-19/确认

  #R/ht|0/我考虑考虑
     ]]

		self.发送信息 = {
			名称 = "提升等级",
			对话 = self.对话内容
		}

		发送数据(玩家数据[id].连接id,20, self.发送信息)

		return 0
   elseif    self.数据.等级>= 服务端参数.开放等级 then
	发送数据(玩家数据[id].连接id,7,"#y/当前服务器的开放等级为:#r/"..服务端参数.开放等级.."#y/无法在提升等级!")
 		return 0
   elseif  self.数据.等级>=145 and self.数据.飞升~=true  then
     发送数据(玩家数据[id].连接id,7,"#y/你的等级当前已经达到上限，需要到月宫吴刚处飞升才能提升155")
      return 0

   elseif  self.数据.等级>=160 and self.数据.渡劫~=9  then
     发送数据(玩家数据[id].连接id,7,"#y/你的等级当前已经达到上限，需要到西凉女国渡劫处渡劫才能提升175")
      return 0

    elseif self.数据.渡劫==9 and self.数据.等级>=175 then
       发送数据(玩家数据[id].连接id,7,"#y/你的等级已经达到上限")
      return 0

   end
     if self.数据.当前经验<self.数据.升级经验 then
        发送数据(玩家数据[id].连接id,7,"#y/你当前的经验不足以提升角色等级")
        return 0
     else
        self:升级事件(id)
        发送数据(玩家数据[id].连接id,7,"#y/等级提升成功")
     end
   end

function 角色处理类:道具异常处理(id)
	for i = 1, 20 do
		if self.数据.道具.包裹[i] ~= nil and 玩家数据[id].道具.数据[self.数据.道具.包裹[i]] == nil then
			self.数据.道具.包裹[i] = nil
		end
	end

	for i = 1, 20 do
		if self.数据.道具.行囊[i] ~= nil and 玩家数据[id].道具.数据[self.数据.道具.行囊[i]] == nil then
			self.数据.道具.行囊[i] = nil
		end
	end
end

function 角色处理类:升级事件(id)
	地图处理类:升级事件(id)
	self.数据.等级 = self.数据.等级 + 1
	self.数据.当前经验 = self.数据.当前经验 - self.数据.升级经验
	self.数据.升级经验 = 升级经验[self.数据.等级]
	self.数据.潜能 = self.数据.潜能 + 5
	self.数据.体力上限 = self.数据.体力上限 + 5
	self.数据.活力上限 = self.数据.活力上限 + 5
	for n = 1, #全局变量.基础属性 do
		self.数据[全局变量.基础属性[n]] = self.数据[全局变量.基础属性[n]] + 1
	end
	self:刷新战斗属性()
	发送数据(玩家数据[id].连接id,2002,self:获取角色数据())
	发送数据(玩家数据[id].连接id,1003,self:获取角色气血数据())
	发送数据(玩家数据[id].连接id,2004,"#y/1")
	发送数据(玩家数据[id].连接id,2017,"#y/1")
end

function 角色处理类:门派条件检测(id, 类型)
	门派限制 = false

	if 门派限制 == false then
		return true
	elseif self.数据.种族 == "仙" and 类型 ~= "五庄观" and 类型 ~= "龙宫" and 类型 ~= "普陀山" and 类型 ~= "天宫" then
		发送数据(玩家数据[id].连接id, 7, "#y/本门派不收你这样的弟子")

		return 0
	elseif self.数据.种族 == "人" and 类型 ~= "大唐官府" and 类型 ~= "方寸山" and 类型 ~= "化生寺" and 类型 ~= "女儿村" then
		发送数据(玩家数据[id].连接id, 7, "#y/本门派不收你这样的弟子")

		return 0
	elseif self.数据.种族 == "魔" and 类型 ~= "狮驼岭" and 类型 ~= "魔王寨" and 类型 ~= "盘丝洞" and 类型 ~= "阴曹地府" then
		发送数据(玩家数据[id].连接id, 7, "#y/本门派不收你这样的弟子")

		return 0
	elseif self.数据.性别 == "男" and (类型 == "盘丝洞" or 类型 == "女儿村" or 类型 == "普陀山") then
		发送数据(玩家数据[id].连接id, 7, "#y/本门派只收女性弟子")

		return 0
	elseif self.数据.性别 == "女" and (类型 == "狮驼岭" or 类型 == "五庄观" or 类型 == "化生寺") then
		发送数据(玩家数据[id].连接id, 7, "#y/本门派只收男性弟子")

		return 0
	end
end

function 角色处理类:加入门派(id, 类型)
	if self.数据.门派 ~= "无" then
		发送数据(玩家数据[id].连接id, 7, "#y/您已经加入过门派了，无法再加入另一个门派")

		return 0
	elseif self.数据.等级 < 10 then
		发送数据(玩家数据[id].连接id, 7, "#y/等级达到10级后才可进入门派")

		return 0
	elseif self:门派条件检测(id, 类型) == 0 then
		return 0
	else
		self.数据.门派 = 类型
		self.数据.门派技能 = {}

		for n = 1, #门派技能[类型] do
			self.数据.门派技能[n] = {
				等级 = 79,
				名称 = 门派技能[类型][n]
			}

			发送数据(玩家数据[id].连接id, 7, "#y/你学会了门派技能#r/" .. 门派技能[类型][n])
		end

		self:门派技能补充(id)
		self.数据.快捷技能={}
		发送数据(玩家数据[id].连接id, 7, "#y/您已经成为#g/" .. 类型 .. "#y/弟子")
		广播门派消息(self.数据.门派, "#" .. 门派代号[self.数据.门派] .. "/#g/" .. self.数据.名称 .. "#w/仰慕本门派已久，今日终于拜入本门派。请各位弟子对其多多支持。")
		发送数据(玩家数据[id].连接id, 7, "#y/你可以使用F1迅速返回本门派")
		self:添加经验(1000000, id, 401)
		self:添加储备(id, 500000, 14)
	end
end

function 角色处理类:添加召唤兽修炼经验(数额)
	if self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].上限 <= self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].等级 then
		return 0
	else
		发送数据(玩家数据[self.玩家id].连接id, 9, "#dq/#w/ 你的召唤兽" .. self.数据.召唤兽修炼.当前 .. "修炼增加了" .. 数额 .. "点经验")

		self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].经验 = self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].经验 + 数额
		self.升级经验 = self:计算修炼等级经验(self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].等级, self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].上限)

		if self.升级经验 <= self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].经验 then
			self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].等级 = self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].等级 + 1
			self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].经验 = self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].经验 - self.升级经验

			发送数据(玩家数据[self.玩家id].连接id, 9, "#dq/#w/ 你的召唤兽" .. self.数据.召唤兽修炼.当前 .. "修炼等级提升至" .. self.数据.召唤兽修炼[self.数据.召唤兽修炼.当前].等级 .. "级")
		end
	end
end

function 角色处理类:购买神兽(id, 类型, 仙玉)
	if #玩家数据[id].召唤兽.数据 >= 8 then
		发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
	elseif self:扣除仙玉( 仙玉,"购买神兽") then
		发送数据(玩家数据[id].连接id, 7, "#y/你购买了神兽" .. 类型)
		发送数据(玩家数据[id].连接id, 7, "#y/你的仙玉减少了" .. 仙玉 .. "点")
		self:添加消费日志("花费" .. 仙玉 .. "点仙玉购买" .. 类型)
		玩家数据[id].召唤兽:创建召唤兽(类型, true, false, 3)
	end
end

function 角色处理类:购买神兽1(id, 类型, 仙玉)
	if #玩家数据[id].召唤兽.数据 >= 8 then
		发送数据(玩家数据[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/你获得了神兽" .. 类型)
		self:添加消费日志("通过神兜兜或在线奖励" .. 类型)
		玩家数据[id].召唤兽:创建召唤兽(类型, true, false, 3)
	end
end

function 角色处理类:恢复血魔(id, 类型, 数额)
	if 类型 == 1 then
		self.数据.当前气血 = self.数据.最大气血
		self.数据.气血上限 = self.数据.最大气血
		self.数据.当前魔法 = self.数据.魔法上限
	elseif 类型 == 2 then
		self.数据.当前气血 = self.数据.当前气血 + 数额

		if self.数据.气血上限 < self.数据.当前气血 then
			self.数据.当前气血 = self.数据.气血上限
		end
	elseif 类型 == 3 then
		self.数据.当前魔法 = self.数据.当前魔法 + 数额

		if self.数据.魔法上限 < self.数据.当前魔法 then
			self.数据.当前魔法 = self.数据.魔法上限
		end
	end

	发送数据(玩家数据[id].连接id, 1003, 玩家数据[id].角色:获取角色气血数据())
end

function 角色处理类:提升指定等级(id, 等级)
	self.数据.等级 = 等级
	self.数据.升级经验 = 升级经验[self.数据.等级]

	for i = 1, #self.数据.门派技能 do
		self.数据.门派技能[i].等级 = 等级
	end

	self.数据.体力上限 = 10 + 等级 * 5
	self.数据.活力上限 = 10 + 等级 * 5

	发送数据(玩家数据[id].连接id, 7, "#y/你的等级已经被管理员提升至#r/" .. 等级 .. "#y/级")
	self:重置属性点(id)

	if 玩家数据[id].召唤兽.数据.参战 ~= 0 then
		玩家数据[id].召唤兽:提级处理(玩家数据[id].召唤兽.数据.参战, id)
	end
end
function 角色处理类:提升推广等级(id, 等级)
	self.数据.等级 = 等级
	self.数据.升级经验 = 升级经验[self.数据.等级]

	for i = 1, #self.数据.门派技能 do
		self.数据.门派技能[i].等级 = 等级
	end

	self.数据.体力上限 = 10 + 等级 * 5
	self.数据.活力上限 = 10 + 等级 * 5

	发送数据(玩家数据[id].连接id, 7, "#y/你的等级已经提升至#r/" .. 等级 .. "#y/级")
	self:重置属性点(id)
end

function 角色处理类:门派技能补充(id)
	if 门派主动技能[self.数据.门派] == nil then
		return 0
	end

	for n = 1, #门派主动技能[self.数据.门派] do
		self.是否获得 = false

		for i = 1, #self.数据.门派技能 do
			if self.数据.门派技能[i].名称 == 门派主动技能[self.数据.门派][n] then
				self.是否获得 = true
			end
		end

		if self.是否获得 == false then
			self.数据.门派技能[#self.数据.门派技能 + 1] = {
				等级 = 79,
				名称 = 门派主动技能[self.数据.门派][n]
			}

			发送数据(玩家数据[id].连接id, 7, "#y/你学会了门派技能#r/" .. 门派主动技能[self.数据.门派][n])
		end
	end

	if self.数据.飞升 then
		if Q_飞升法术[self.数据.门派] == nil then
			return 0
		end

		for n = 1, #Q_飞升法术[self.数据.门派] do
			self.是否获得 = false

			for i = 1, #self.数据.门派技能 do
				if self.数据.门派技能[i].名称 == Q_飞升法术[self.数据.门派][n] then
					self.是否获得 = true
				end
			end

			if self.是否获得 == false then
				self.数据.门派技能[#self.数据.门派技能 + 1] = {
					等级 = 120,
					名称 = Q_飞升法术[self.数据.门派][n]
				}

				发送数据(玩家数据[id].连接id, 7, "#y/你学会了门派技能#r/" .. Q_飞升法术[self.数据.门派][n])
			end
		end
	end
end

function 角色处理类:辅助技能等级上限(id)
	if self.数据.帮派 == nil or 帮派数据[self.数据.帮派] == nil then
		return 185
	else
		return 185 + 帮派数据[self.数据.帮派].书院 * 2 + 10
	end
end

function 角色处理类:学习强化技能处理(id, 数据)
	self.技能名称 = 数据.文本
	self.技能类型 = "1"
	self.技能类型 = "辅助技能"
	self.消耗方式 = 5
	self.消耗等级 = self.数据.强化技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")

			return 0
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")

			return 0
		elseif self:辅助技能等级上限(id) <= self.消耗等级 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")

			return 0
		elseif self.消耗等级 >= 60 and self.数据.帮派 == nil then
						发送数据(玩家数据[id].连接id, 7, "#y/加入帮派才能学习！")
			return 0
		elseif self.消耗等级 >= 100 and 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 < 15 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的帮贡")
			return 0
		else
			if self.消耗等级 >= 100 then
				if 帮派数据[self.数据.帮派].资材 < 1 then
					发送数据(玩家数据[id].连接id, 7, "#y/你帮派没有那么多的资材")
					return 0
				end

				帮派数据[self.数据.帮派].资材 = 帮派数据[self.数据.帮派].资材 - 1
				帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 - 15
				玩家数据[id].角色.数据.帮贡 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前
				发送数据(玩家数据[id].连接id, 7, "#y/你减少了15点帮贡")
			end

			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10, 1)
			self.消耗等级 = self.消耗等级 + 1
			self.数据.强化技能[self.技能名称] = self.消耗等级
			玩家数据[id].角色:刷新装备属性(id)

			self.发送信息 = self.数据.强化技能
			self.发送信息.银两 = self.数据.道具.货币.银子
			self.发送信息.储备 = self.数据.道具.货币.储备
			self.发送信息.存银 = self.数据.道具.货币.存银
			self.发送信息.经验 = self.数据.当前经验
			self.发送信息.方式 = 2
			self.发送信息.编号 = 数据.序号
			发送数据(玩家数据[id].连接id, 2019, self.发送信息)
		end
	end
end


function 角色处理类:学习辅助技能处理(id, 数据)
	self.技能名称 = 数据.文本
	self.技能类型 = "1"
	self.技能类型 = "辅助技能"
	self.消耗方式 = 2
	self.消耗等级 = self.数据.辅助技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")
			return 0
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")
			return 0
		elseif self:辅助技能等级上限(id) <= self.消耗等级 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")
			return 0
		elseif self.消耗等级 >= 100 then
			if self.数据.帮派 == 0 then
				发送数据(玩家数据[id].连接id, 7, "#y/进入帮派才可以继续学习")
				return 0
			elseif 帮派数据[self.数据.帮派] == nil or 帮派数据[self.数据.帮派].成员名单[id] == nil then
				发送数据(玩家数据[id].连接id, 7, "#y/无效的帮派")
				return 0
			elseif 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 < 15 then
				发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的帮贡")
				return 0
			elseif 帮派数据[self.数据.帮派].资材 < 1 then
				发送数据(玩家数据[id].连接id, 7, "#y/你帮派没有那么多的资材")
				return 0
			end
			帮派数据[self.数据.帮派].资材 = 帮派数据[self.数据.帮派].资材 - 1
			帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 - 15
			玩家数据[id].角色.数据.帮贡 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前
			发送数据(玩家数据[id].连接id, 7, "#y/你减少了15点帮贡")
		end

		玩家数据[id].角色:扣除经验(id, self.消耗经验)
		玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10, 1)

		self.消耗等级 = self.消耗等级 + 1
		self.数据.辅助技能[self.技能名称] = self.消耗等级

		玩家数据[id].角色:刷新战斗属性(1)

		self.发送信息 = self.数据.辅助技能
		self.发送信息.银两 = self.数据.道具.货币.银子
		self.发送信息.储备 = self.数据.道具.货币.储备
		self.发送信息.存银 = self.数据.道具.货币.存银
		self.发送信息.经验 = self.数据.当前经验
		self.发送信息.方式 = 2
		self.发送信息.编号 = 数据.序号

		发送数据(玩家数据[id].连接id, 2018, self.发送信息)
	end
end
function 角色处理类:购买坐骑(名称)
    if  self:判断坐骑(self.数据.造型,self.数据.种族,名称) ==false  then
        发送数据(玩家数据[self.数据.id].连接id,7,"#y/你的角色当前无法使用改坐骑")
		return 0
	elseif   self:扣除仙玉(10000,"购买坐骑")   then
			self:创建坐骑(self.数据.id,名称)
	else
		发送数据(玩家数据[self.数据.id].连接id,7,"#y/你没有足够的仙玉")
		return 0
	end

end
function 角色处理类:判断坐骑(造型,种族,坐骑)
	 if 坐骑 == "七彩神驴" and 种族 =="人" then
	 		return true
	else
	    return false
	end
end
function 角色处理类:角色改名(名称)
  if   self:扣除仙玉(500,"角色更改名称")   then
    if   网络处理类:名称检查(名称,玩家数据[self.数据.id].连接id) then
      for n = 1, #名称数据 do
        if 名称数据[n] == self.数据.名称 then --.账号 --.账号
           名称数据[n] = 名称 --.名称
          break
        end
      end
      广播消息("#xt/#r/ "..self.数据.名称.."#y/排了三天三夜的队，终于成功改名为#r/"..名称)
      self.数据.名称=名称
      发送数据(玩家数据[self.数据.id].连接id, 7, "#y/恭喜你改名成功!重新启动客户端生效")
      地图处理类:发送数据(self.数据.id, 1016, self:获取角色数据(), 玩家数据[self.数据.id].地图)
    end
  else
    发送数据(玩家数据[id].连接id,7,"#y/你没有足够的仙玉")
    return 0
  end
 end

function 角色处理类:学习生活技能处理(id, 数据)
	self.技能名称 = 数据.文本
	self.技能类型 = "1"
	self.技能类型 = "生活技能"
	self.消耗方式 = 3
	self.消耗等级 = self.数据.生活技能[self.技能名称]

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")

			return 0
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")

			return 0
		elseif self:辅助技能等级上限(id) <= self.消耗等级 then
			发送数据(玩家数据[id].连接id, 7, "#y/该技能等级已达上限")

			return 0
		elseif self.消耗等级 >= 100 and 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 < 15 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有那么多的帮贡")

			return 0
		else
			if self.消耗等级 >= 100 then
				if 帮派数据[self.数据.帮派].资材 < 1 then
					发送数据(玩家数据[id].连接id, 7, "#y/你帮派没有那么多的资材")

					return 0
				end

				帮派数据[self.数据.帮派].资材 = 帮派数据[self.数据.帮派].资材 - 1
				帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前 - 15
				玩家数据[id].角色.数据.帮贡 = 帮派数据[self.数据.帮派].成员名单[id].帮贡.当前
				发送数据(玩家数据[id].连接id, 7, "#y/你减少了15点帮贡")
			end

			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10, 1)

			self.消耗等级 = self.消耗等级 + 1
			self.数据.生活技能[self.技能名称] = self.消耗等级

			玩家数据[id].角色:刷新战斗属性(1)

			self.发送信息 = self.数据.生活技能
			self.发送信息.银两 = self.数据.道具.货币.银子
			self.发送信息.储备 = self.数据.道具.货币.储备
			self.发送信息.存银 = self.数据.道具.货币.存银
			self.发送信息.经验 = self.数据.当前经验
			self.发送信息.方式 = 2
			self.发送信息.编号 = 数据.序号

			发送数据(玩家数据[id].连接id, 2022, self.发送信息)
		end
	end
end

function 角色处理类:学习技能处理(id, 数据)
	self.技能名称 = 数据.文本
	self.技能类型 = ""

	for n = 1, #self.数据.门派技能 do
		if self.数据.门派技能[n].名称 == self.技能名称 then
			self.技能类型 = "门派技能"
			self.消耗方式 = 1
			self.消耗编号 = n
			self.消耗等级 = self.数据.门派技能[n].等级
		end
	end

	if self.技能类型 ~= "" then
		self.消耗经验 = 技能消耗.经验[self.消耗等级 + 1] * self.消耗方式
		self.消耗金钱 = 技能消耗.金钱[self.消耗等级 + 1] * self.消耗方式

		if 取角色银子(id) < self.消耗金钱 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的银子")
		elseif 取角色经验(id) < self.消耗经验 then
			发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的经验")
		elseif self.消耗等级 >= self.数据.等级 + 10 then
			发送数据(玩家数据[id].连接id, 7, "#y/你当前的等级已经无法再提升该技能的等级了")
		else
			玩家数据[id].角色:扣除经验(id, self.消耗经验)
			玩家数据[id].角色:扣除银子(id, self.消耗金钱, 10, 1)

			self.消耗等级 = self.消耗等级 + 1
			self.数据[self.技能类型][self.消耗编号].等级 = self.消耗等级
			self.发送信息 = self.数据.门派技能
			self.发送信息.银两 = self.数据.道具.货币.银子
			self.发送信息.储备 = self.数据.道具.货币.储备
			self.发送信息.存银 = self.数据.道具.货币.存银
			self.发送信息.经验 = self.数据.当前经验
			self.发送信息.方式 = 1
			self.发送信息.序列 = 数据.参数
			self.发送信息.编号 = 数据.序号

			if self.技能类型 == "门派技能" and 游戏技能[self.技能名称].等级 == self.消耗等级 then
				发送数据(玩家数据[id].连接id, 7, "#y/你现在可以在战斗中使用新技能#r/" .. self.技能名称)
			end

			发送数据(玩家数据[id].连接id,2016,self.发送信息)
		end
	end
end

function 角色处理类:取飞行限制(id)
	if 玩家数据[self.玩家id].地图 == 1511 then
		发送数据(玩家数据[self.玩家id].连接id, 7, "#y/本地图不允许使用飞行道具，请找土地公公传回长安城")

		return false
	else
		return true
	end
end

function 角色处理类:添加系统消息(id, 内容)
	if 系统消息数据[id] == nil then
		系统消息数据[id] = {}
	end

	系统消息数据[id][#系统消息数据[id] + 1] = "#r/系统提示 " .. 测试时间(os.time()) .. "\n" .. 内容

	发送数据(玩家数据[id].连接id, 13, "77")
end

function 角色处理类:取指定称谓(称谓)
  for n=1,#self.数据.称谓 do

    if self.数据.称谓[n]==称谓 then
     return true
      end
   end
 return false
 end

function 角色处理类:隐藏称谓(id, 称谓)
	if self.数据.称谓.当前 == 称谓 then
		self.数据.称谓.当前 = ""
		发送数据(玩家数据[id].连接id, 7, "#y/隐藏称谓成功")
		发送数据(玩家数据[id].连接id, 2011, 玩家数据[id].角色:获取称谓数据())
		发送数据(玩家数据[id].连接id, 2012, self.数据.称谓.当前)
		地图处理类:更改称谓(玩家数据[id].地图, id, self.数据.称谓.当前)
	else
		发送数据(玩家数据[id].连接id, 7, "#y/此称谓都没有显示出来怎么隐藏？")
	end
end

function 角色处理类:飞升处理(id)
	if self.数据.飞升 then
		-- Nothing
	else
		self.数据.飞升 = true
		self.临时等级 = self.数据.等级
		self.数据.等级 = self.数据.等级 - 15

		玩家数据[id].角色:重置属性点(id)
		self:门派技能补充(id)
		玩家数据[id].召唤兽:飞升处理(id)
		self:添加系统消息(id, "#y/恭喜你完成了飞升挑战，您的等级由原来的#r/" .. self.临时等级 .. "#y/级下降至#r/" .. self.数据.等级 .. "#y/级。您的属性点已经被重置。")

		if self.数据.种族 == "人" then
			self.临时称谓 = "三届贤者"
		elseif self.数据.种族 == "魔" then
			self.临时称谓 = "混世魔王"
		elseif self.数据.种族 == "仙" then
			self.临时称谓 = "太乙金仙"
		else
			self.临时称谓 = "极乐天人"
		end

		self:添加称谓(id, self.临时称谓)
		广播消息("#xt/#g/" .. self.数据.名称 .. "#y/在月宫吴刚处完成了飞升挑战，用自己的实力证明了自己是三界的强者。获得了玉皇大帝特赐的#r/" .. self.临时称谓 .. "#y/称谓")

		self.修炼关键词 = "人物修炼"

		for n = 1, #全局变量.修炼名称 do
			if self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 20 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 25
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 19 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 25
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 18 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 17 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 16 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 11
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 15 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 10
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 14 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 9
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 13 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 8
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 12 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 7
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 11 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 6
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 10 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 5
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 9 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 4
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 8 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 3
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 7 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 2
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 6 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 1
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 5 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 0
			end
		end
		self.修炼关键词 = "召唤兽修炼"
		for n = 1, #全局变量.修炼名称 do
			if self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 20 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 25
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 19 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 18 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 17 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 16 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 11
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 15 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 10
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 14 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 9
			elseif self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 == 13 then
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
				self.数据[self.修炼关键词][全局变量.修炼名称[n]].等级 = 8
			end
		end
	end
end

function 角色处理类:改变称谓(id, 称谓)
	self.删除序列 = 0

	for n = 1, #self.数据.称谓 do
		if self.数据.称谓[n] == 称谓 then
			self.删除序列 = n
		end
	end

	if self.删除序列 == 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的称谓")
	else
		self.数据.称谓.当前 = 称谓

		发送数据(玩家数据[id].连接id, 7, "#y/更改称谓成功")
		发送数据(玩家数据[id].连接id, 2011, 玩家数据[id].角色:获取称谓数据())
		发送数据(玩家数据[id].连接id, 2012, self.数据.称谓.当前)
		地图处理类:更改称谓(玩家数据[id].地图, id, self.数据.称谓.当前)
	end
end

function 角色处理类:添加称谓(id, 称谓)
	for n = 1, #self.数据.称谓 do
		if self.数据.称谓[n] == 称谓 then
			发送数据(玩家数据[id].连接id, 7, "#y/此称谓你已经拥有了")

			return 0
		end
	end

	if #self.数据.称谓 >= 7 then
		发送数据(玩家数据[id].连接id, 7, "#y/角色拥有的称谓数量不能超过7个")

		return 0
	end

	self.数据.称谓[#self.数据.称谓 + 1] = 称谓
   self.数据.称谓.当前 = 称谓
	发送数据(玩家数据[id].连接id, 7, "#y/你获得了新称谓#r/" .. 称谓)
end

function 角色处理类:添加银子(id, 数额, 类型)
	self.临时数额 = 数额 * 服务端参数.银子获得率
	if 类型 ~= 29 and 类型 ~= 11 and 类型 ~= 6 and 类型 ~= 16 and 类型 ~= 4 and 类型 ~= "银子礼包" and self.数据.聚宝盆 ~= nil then
		self.临时数额 = self.临时数额 * (0.01+self.数据.聚宝盆)
	end
	self.初始余额 = self.数据.道具.货币.银子
	self.数据.道具.货币.银子 = self.数据.道具.货币.银子 + self.临时数额
	self.数据.获得银子 = self.数据.获得银子 + self.临时数额

	updateRanking(id, self.数据)

    发送数据(玩家数据[id].连接id, 9, "#dq/#w/ 获得" .. self.临时数额 .. "两银子")
	self:添加消费日志("余额" .. self.初始余额 .. "，获得银子：" .. self.临时数额 .. ",来源代号" .. 类型 .. "，银子余额" .. self.数据.道具.货币.银子)
end


function 角色处理类:添加储备(id, 数额, 类型)
	self.临时数额 = 数额 * 服务端参数.银子获得率
	self.初始余额 = self.数据.道具.货币.储备
	self.数据.道具.货币.储备 = self.数据.道具.货币.储备 + self.临时数额
	发送数据(玩家数据[id].连接id, 9, "#dq/#w/ 获得" .. self.临时数额 .. "储备金")
	self:添加消费日志("余额" .. self.初始余额 .. "，获得储备：" .. self.临时数额 .. ",来源代号" .. 类型 .. "，储备余额" .. self.数据.道具.货币.储备)
end

function 角色处理类:扣除仙玉(数额,类型)---------------------------
	if 数额 <= 0 then
		return true
	end
	玩家数据[self.数据.id].仙玉 = f函数.读配置(程序目录..data目录 .. 玩家数据[self.数据.id].账号.. _账号txt, "账号信息", "仙玉") + 0
	if 玩家数据[self.数据.id].仙玉 <= 0 then
		发送数据(玩家数据[self.数据.id].连接id, 7, "#y/你没有那么多的仙玉")
		return false
	end
	if 玩家数据[self.数据.id].仙玉 < 数额 then
		发送数据(玩家数据[self.数据.id].连接id, 7, "#y/你没有那么多的仙玉")
		return false
	else
		玩家数据[self.数据.id].仙玉 = 玩家数据[self.数据.id].仙玉 - 数额
		self:添加系统消息(self.数据.id, "#w/您消耗了#y/" .. 数额 .. "#w/点仙玉，当前可用仙玉数额为#r/" .. 玩家数据[self.数据.id].仙玉 .. "#w/点")
		f函数.写配置(程序目录..data目录 .. 玩家数据[self.数据.id].账号.. _账号txt, "账号信息", "仙玉", 玩家数据[self.数据.id].仙玉)
		self:添加消费日志("扣除"..数额.."来源"..类型)
		return true
	end
 end
function 角色处理类:添加仙玉(数额,类型)----------------------
	if 数额 <= 0 then
		return
	end
	玩家数据[self.数据.id].仙玉 = f函数.读配置(程序目录..data目录 .. 玩家数据[self.数据.id].账号.. _账号txt, "账号信息", "仙玉") + 0
	玩家数据[self.数据.id].仙玉=玩家数据[self.数据.id].仙玉+数额
	f函数.写配置(data日录..玩家数据[self.数据.id].账号..[[\账号.txt]],"账号信息","仙玉",玩家数据[self.数据.id].仙玉)
	玩家数据[self.数据.id].仙玉=f函数.读配置(data日录..玩家数据[self.数据.id].账号..[[\账号.txt]],"账号信息","仙玉")+0
	self:添加系统消息(self.数据.id, "#w/您获得了#y/" .. 数额 .. "#w/点仙玉，当前可用仙玉数额为#r/" .. 玩家数据[self.数据.id].仙玉 .. "#w/点")
	发送数据(玩家数据[self.数据.id].连接id,7,"#y/你获得了"..数额.."仙玉")
	self:添加消费日志("增加"..数额.."来源"..类型)
end
function 角色处理类:购买装备(id, 类型, 仙玉)
	if self:扣除仙玉(仙玉,"花费" .. 仙玉 .. "点仙玉购买" .. 类型 .. "级装备") then
		发送数据(玩家数据[id].连接id, 7, "#y/你的仙玉减少了" .. 仙玉 .. "点")
		if 类型 == 120 then
			玩家数据[id].装备:取120级装备礼包(id)
		elseif 类型 == 140 then
			玩家数据[id].装备:取140级装备礼包(id)
		elseif 类型 == 150 then
			玩家数据[id].装备:取150级装备礼包(id)
		elseif 类型 == 160 then
			玩家数据[id].装备:取160级装备礼包(id)
		end
	end
end

function 角色处理类:添加人物修炼经验(数额)
	if self.数据.人物修炼[self.数据.人物修炼.当前].上限 <= self.数据.人物修炼[self.数据.人物修炼.当前].等级 then
		return 0
	else
		发送数据(玩家数据[self.玩家id].连接id, 9, "#dq/#w/ 你的人物" .. self.数据.人物修炼.当前 .. "修炼增加了" .. 数额 .. "点经验")

		self.数据.人物修炼[self.数据.人物修炼.当前].经验 = self.数据.人物修炼[self.数据.人物修炼.当前].经验 + 数额
		self.升级经验 = self:计算修炼等级经验(self.数据.人物修炼[self.数据.人物修炼.当前].等级, self.数据.人物修炼[self.数据.人物修炼.当前].上限)

		if self.升级经验 <= self.数据.人物修炼[self.数据.人物修炼.当前].经验 then
			self.数据.人物修炼[self.数据.人物修炼.当前].等级 = self.数据.人物修炼[self.数据.人物修炼.当前].等级 + 1
			self.数据.人物修炼[self.数据.人物修炼.当前].经验 = self.数据.人物修炼[self.数据.人物修炼.当前].经验 - self.升级经验

			发送数据(玩家数据[self.玩家id].连接id, 9, "#dq/#w/ 你的人物" .. self.数据.人物修炼.当前 .. "修炼等级提升至" .. self.数据.人物修炼[self.数据.人物修炼.当前].等级 .. "级")
		end
	end
end

function 角色处理类:购买染色(id, 仙玉)
	if self:扣除仙玉(仙玉,"购买染色包") then
		发送数据(玩家数据[id].连接id, 7, "#y/你的仙玉减少了" .. 仙玉 .. "点")
		玩家数据[id].道具:给予道具(id, "花豆", "普通", 40)
		玩家数据[id].道具:给予道具(id, "彩果", "普通", 30)
	end
end

function 角色处理类:扣除活力(类型, 数额)
	if 类型 == 1 then
		self.数据.当前活力 = math.floor(self.数据.当前活力 - self.数据.当前活力 * 数额)
	elseif 类型 == 2 then
		self.数据.当前活力 = math.floor(self.数据.当前活力 - 数额)
	end
end

function 角色处理类:处理押镖任务(id, 等级, 活力, 奖励)
	if self.数据.等级 < 等级 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的等级未达到要求")
	elseif self.数据.当前活力 < 活力 then
		发送数据(玩家数据[id].连接id, 7, "#y/你没那么多的活力")
	elseif self.数据.人气 <= 705 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的人气低于705点无法领取押镖任务")
	else
		self.数据.当前活力 = self.数据.当前活力 - 活力
		self.数据.人气 = self.数据.人气 - 15

		玩家数据[id].角色:添加储备(id, 奖励, 6)

		if 取随机数() <= 5 and 等级 >= 69 then
			玩家数据[id].道具:给予道具(id, "鬼谷子", "功能", "魔力")
			发送数据(玩家数据[id].连接id, 7, "#y/你获得了一本鬼谷子")
		end
	end
end

function 角色处理类:扣除经验(id, 数额, 类型)
	self.数据.当前经验 = self.数据.当前经验 - 数额
end

function 角色处理类:取可用道具格子(类型)

	-- table.print(self.数据.道具[类型])

	for n = 1, 20 do
		if self.数据.道具[类型][n] == nil then
			return n
		end
	end

	return 0
end

function 角色处理类:玩家切磋(id, 挑战id)
	if 玩家数据[挑战id] == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/对方并不在线")

		return 0
	elseif 玩家数据[挑战id].摆摊 ~= nil then
		发送数据(玩家数据[id].连接id, 7, "#y/对方正处于摆摊状态，无法发起战斗")

		return 0
	elseif 玩家数据[id].地图 ~= 玩家数据[挑战id].地图 then
		发送数据(玩家数据[id].连接id, 7, "#y/对方与你不在同一地图内")

		return 0
	elseif 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队长 == false then
		发送数据(玩家数据[id].连接id, 7, "#y/只有队长才能发起战斗")

		return 0
	elseif 玩家数据[id].队伍 ~= 0 and 玩家数据[id].队伍 == 玩家数据[挑战id].队伍 then
		发送数据(玩家数据[id].连接id, 7, "#y/您不能对自己的队友发起攻击")

		return 0
	elseif 玩家数据[id].战斗 ~= 0 or 玩家数据[挑战id].战斗 ~= 0 and 玩家数据[挑战id].观战模式 == false then
		if 玩家数据[id].战斗 == 0 and 玩家数据[挑战id].战斗 ~= 0 and 玩家数据[id].队伍 == 0 then
			玩家数据[id].战斗 = 玩家数据[挑战id].战斗
			玩家数据[id].观战模式 = true

			战斗准备类.战斗盒子[玩家数据[挑战id].战斗]:观战数据发送(id, 挑战id)
		else
			发送数据(玩家数据[id].连接id, 7, "#y/对方正处于战斗中，请稍后再试")

			return 0
		end
	elseif 玩家数据[id].地图 == 6001 or 玩家数据[id].地图 == 6002 or 玩家数据[id].地图 == 6003 then
		if 比武大会开始开关 == false then
			发送数据(玩家数据[id].连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")

			return 0
		elseif os.time() - 玩家数据[挑战id].比武保护期 <= 30 then
			发送数据(玩家数据[id].连接id, 7, "#y/对方处于30秒的保护期，暂时无法对其发起攻击")

			return 0
		else
			战斗准备类:创建战斗(id, 200004, 挑战id, 0)
		end
	elseif 玩家数据[id].地图 == 5135 then
		if 首席争霸赛战斗开关 == false then
			发送数据(玩家数据[id].连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")

			return 0
		elseif os.time() - 玩家数据[挑战id].首席保护 <= 30 then
			发送数据(玩家数据[id].连接id, 7, "#y/对方处于保护期，暂时无法对其发起攻击")

			return 0
		else
			战斗准备类:创建战斗(id, 200002, 挑战id, 0)
		end
	elseif 玩家数据[id].地图 == 1001 then
		if 取两点距离({x=8274,y=3426},{x=self.数据.地图数据.x,y=self.数据.地图数据.y})>400 or 取两点距离({x=8274,y=3426},{x=玩家数据[挑战id].角色.数据.地图数据.x,y=玩家数据[挑战id].角色.数据.地图数据.y})>400 then
	      发送数据(玩家数据[id].连接id,7,"#y/只有长安城擂台才可以进行切磋")
	      return 0
	    else
	      战斗准备类:创建战斗(id,200001,挑战id,0)
	     end
    else
    	发送数据(玩家数据[id].连接id,7,"#y/只有长安城擂台才可以进行切磋")
	-- elseif 玩家数据[id].地图 == 1040 then
	-- 	发送数据(玩家数据[id].连接id, 7, "#y/只有西凉才可以进行切磋")
	-- 	战斗准备类:创建战斗(id, 200001, 挑战id, 0)
	end
end

function 角色处理类:添加消费日志(内容)
--	print(内容)
	self.消费日志 = self.消费日志 .. 运行时间 .. " " .. 内容 .. "#换行符"
end

function 角色处理类:扣除银子(id, 数额, 类型, 储备)
	if 数额 < 0 then
		f函数.写配置(data目录 .. 玩家数据[id].账号.. _账号txt, "账号信息", "封禁", "1")

		return 0
	end

	if 储备 == nil then
		self.初始余额 = self.数据.道具.货币.银子
		self.数据.道具.货币.银子 = self.数据.道具.货币.银子 - 数额

		self:添加消费日志("余额" .. self.初始余额 .. "，失去银子：" .. 数额 .. ",来源代号" .. 类型 .. "，银子余额" .. self.数据.道具.货币.银子)

		if 类型 ~= 10 and 类型 ~= 11 and 类型 ~= 29 then
			发送数据(玩家数据[id].连接id, 7, "#y/你失去了" .. 数额 .. "两银子")
		end
	elseif 数额 <= self.数据.道具.货币.储备 then
		self.初始余额 = self.数据.道具.货币.储备
		self.数据.道具.货币.储备 = self.数据.道具.货币.储备 - 数额

		self:添加消费日志("余额" .. self.初始余额 .. "，失去银子：" .. 数额 .. ",来源代号" .. 类型 .. "，储备余额" .. self.数据.道具.货币.储备)

		if 类型 ~= 10 and 类型 ~= 11 then
			发送数据(玩家数据[id].连接id, 7, "#y/你失去了" .. 数额 .. "两储备银子")
		end
	else
		self.扣除余额 = 数额 - self.数据.道具.货币.储备

		self:添加消费日志("余额" .. self.数据.道具.货币.储备 .. "，失去银子：" .. self.数据.道具.货币.储备 .. ",来源代号" .. 类型 .. "，储备余额" .. self.数据.道具.货币.储备)

		if 类型 ~= 10 and 类型 ~= 11 then
			发送数据(玩家数据[id].连接id, 7, "#y/你失去了" .. self.数据.道具.货币.储备 .. "两储备金")
		end

		self.数据.道具.货币.储备 = 0
		self.数据.道具.货币.银子 = self.数据.道具.货币.银子 - self.扣除余额

		self:添加消费日志("余额" .. self.数据.道具.货币.银子 .. "，失去银子：" .. self.扣除余额 .. ",来源代号" .. 类型 .. "，银子余额" .. self.数据.道具.货币.银子)

		if 类型 ~= 10 and 类型 ~= 11 then
			发送数据(玩家数据[id].连接id, 7, "#y/你失去了" .. self.扣除余额 .. "两银子")
		end
	end
end

function 角色处理类:获取角色气血数据()----
	self.发送数据 = {
		当前气血 = self.数据.当前气血,
		气血上限 = self.数据.气血上限,
		最大气血 = self.数据.最大气血,
		当前魔法 = self.数据.当前魔法,
		魔法上限 = self.数据.魔法上限,
		当前经验 = self.数据.当前经验,
		升级经验 = self.数据.升级经验,
		愤怒 = self.数据.愤怒
	}

	return self.发送数据
end
function 角色处理类:转飞燕女(id)
	self.数据.造型 = "飞燕女"
	self.数据.种族 = "人"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end
function 角色处理类:转骨精灵(id)
	self.数据.造型 = "骨精灵"
	self.数据.种族 = "魔"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转狐美人(id)
	self.数据.造型 = "狐美人"
	self.数据.种族 = "魔"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转虎头怪(id)
	self.数据.造型 = "虎头怪"
	self.数据.种族 = "魔"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转剑侠客(id)
	self.数据.造型 = "剑侠客"
	self.数据.种族 = "人"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转巨魔王(id)
	self.数据.造型 = "巨魔王"
	self.数据.种族 = "魔"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转龙太子(id)
	self.数据.造型 = "龙太子"
	self.数据.种族 = "仙"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转神天兵(id)
	self.数据.造型 = "神天兵"
	self.数据.种族 = "仙"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转舞天姬(id)
	self.数据.造型 = "舞天姬"
	self.数据.种族 = "仙"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转逍遥生(id)
	self.数据.造型 = "逍遥生"
	self.数据.种族 = "人"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转玄彩娥(id)
	self.数据.造型 = "玄彩娥"
	self.数据.种族 = "仙"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转英女侠(id)
	self.数据.造型 = "英女侠"
	self.数据.种族 = "人"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转鬼潇潇(id)
	self.数据.造型 = "鬼潇潇"
	self.数据.种族 = "魔"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转偃无师(id)
	self.数据.造型 = "偃无师"
	self.数据.种族 = "人"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转桃夭夭(id)
	self.数据.造型 = "桃夭夭"
	self.数据.种族 = "仙"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转巫蛮儿(id)
	self.数据.造型 = "巫蛮儿"
	self.数据.种族 = "人"
	self.数据.性别 = "女"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转杀破狼(id)
	self.数据.造型 = "杀破狼"
	self.数据.种族 = "魔"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:转羽灵神(id)
	self.数据.造型 = "羽灵神"
	self.数据.种族 = "仙"
	self.数据.性别 = "男"
	self.数据.坐骑 = nil
	self.数据.坐骑数据 = {}
	self.数据.备用坐骑 = nil
	self.数据.坐骑编号 = nil
end

function 角色处理类:检查仙玉(id,数额)------------------------------------------------------
  if 数额<=0 then return true end
 玩家数据[id].仙玉=f函数.读配置(data日录..玩家数据[id].账号..[[\账号.txt]],"账号信息","仙玉")+0
 if 玩家数据[id].仙玉<=0 then
   发送数据(玩家数据[id].连接id,7,"#y/你没有那么多的仙玉")
   return false
   end
 if 玩家数据[id].仙玉<数额 then
   发送数据(玩家数据[id].连接id,7,"#y/你没有那么多的仙玉")
   return false
  else
    return true
   end
 end

function 角色处理类:获取角色数据()
	self.发送数据 = {
		战斗类型 = "角色",
		名称 = self.数据.名称,
		id = self.数据.id,
		门贡 = self.数据.门贡,
		帮贡 = self.数据.帮贡,
		人气 = self.数据.人气,
		愤怒 = self.数据.愤怒,
		门派 = self.数据.门派,
		当前气血 = self.数据.当前气血,
		气血上限 = self.数据.气血上限,
		最大气血 = self.数据.最大气血,
		当前魔法 = self.数据.当前魔法,
		魔法上限 = self.数据.魔法上限,
		当前体力 = self.数据.当前体力,
		体力上限 = self.数据.体力上限,
		当前活力 = self.数据.当前活力,
		活力上限 = self.数据.活力上限,
		坐骑 = self.数据.坐骑,
		伤害 = self.数据.伤害,
		愤怒特效 = self.数据.愤怒特效,
		命中 = self.数据.命中,
		防御 = self.数据.防御,
		灵力 = self.数据.灵力,
		速度 = self.数据.速度,
		锦衣数据 = self.数据.锦衣数据,
		躲闪 = self.数据.躲闪,
		幻化属性 = self.数据.幻化属性,
		潜能 = self.数据.潜能,
		法防 = self.数据.法防,
		法宝数据 = self.数据.法宝,
		变身 = self.数据.变身.造型,
		染色 = self.数据.染色,
		当前经验 = self.数据.当前经验,
		等级 = self.数据.等级,
		升级经验 = self.数据.升级经验,
		变身 = self.数据.变身.造型,
		造型 = self.数据.造型,
		武器数据 = self.数据.武器数据,
		地图数据 = self.数据.地图数据,
		门派技能 = self.数据.门派技能,
		默认法术 = self.数据.默认法术,
		称谓 = self.数据.称谓.当前,

	}

	for n = 1, #全局变量.基础属性 do
		self.发送数据[全局变量.基础属性[n]] = self.数据[全局变量.基础属性[n]]
	end

	if self.数据.帮派 ~= nil and 帮派数据[self.数据.帮派] ~= nil and 帮派数据[self.数据.帮派].成员名单[self.玩家id] ~= nil then
		self.发送数据.帮派 = 帮派数据[self.数据.帮派].名称
		self.发送数据.帮贡 = 帮派数据[self.数据.帮派].成员名单[self.玩家id].帮贡.当前
	else
		self.发送数据.帮派 = "无"
		self.发送数据.帮贡 = 0
	end

	return self.发送数据
end

function 角色处理类:获取地图数据(id)
	self.发送数据 = {
		名称 = self.数据.名称,
		id = self.数据.id,
		武器数据 = self.数据.武器数据,
		地图数据 = self.数据.地图数据,
		造型 = self.数据.造型,
		染色 = self.数据.染色,
		等级 = self.数据.等级,
		门派 = self.数据.门派,
		队伍 = self.队伍,
		战斗开关 = self.战斗开关,
		变身 = self.数据.变身.造型,
		坐骑 = self.数据.坐骑,
		称谓 = self.数据.称谓.当前,
		队长 = 玩家数据[self.玩家id].队长,
		战斗 = 玩家数据[self.玩家id].战斗,
		锦衣数据 = self.数据.锦衣数据,

	}

	if 玩家数据[self.玩家id].摆摊 == nil then
		self.发送数据.摊位名称 = ""
	else
		self.发送数据.摊位名称 = 摊位数据[self.玩家id].名称
	end

	return self.发送数据
end

function 角色处理类:创建新角色(账号, 名称, 造型, 种族, 性别, 角色id)
	种族 = self:获取种族(种族)
	性别 = self:获取性别(造型)
	local lv = 69
	self.数据 = {
		名称 = 名称,
		付费时间 = 0,
		充值赠送 = {坐骑 = 0,bb = 0,兽诀 = 0,武器 = 0},
		id = 角色id,
		造型 = 造型,
		种族 = 种族,
		性别 = 性别,
		礼包记录 = {[10001] = os.time(),[10002] = os.time(),[10003] = os.time()},
		挑战记录 = {},
		染色 = {c = 0,a = 0,b = 0},
		门派 = "无",
		等级 = lv,
		账号 = 账号,
		任务数据 = {},
		当前经验 = 0,--180000000,
		升级经验 = 升级经验[lv],
		当前体力 = 0,
		体力上限 = lv * 5 + 10,
		活力上限 = lv * 5 + 10,
		当前活力 = 0,
		潜能 = lv * 5,
		人气 = 700,
		比武积分 = 0,
		帮贡 = 0,
		门贡 = 0,
		愤怒 = 0,
		副本积分=0,
		锦衣数据 ={},
		道具 = {包裹 = {},行囊 = {},货币 = {银子 = 10000,元宝 = 0,存银 = 0,储备 = 0 }}, --520000000,
		装备数据 = {},
		礼包 = {},
		门派技能 = {},
		辅助技能 = { 冥想 = 0, 强身术 = 0, 中药医理 = 0, 烹饪技巧 = 0 },
		生活技能 = {裁缝技巧 = 0,打造技巧 = 0,炼金术 = 0},
		强化技能 = {灵力强化 = 0,命中强化 = 0,速度强化 = 0,伤害强化 = 0,防御强化 = 0,固伤强化 = 0},
		快捷技能={},
		出生日期 = os.time(),
		离线时间 = os.time(),
		师门时间 = os.time(),
		抓鬼次数 = 1,
		鬼王次数 = 1,
		师门次数 = 20,
		阴德 = 10,
		师门环数 = 0,
		称谓 = {当前 = "新丝滑"},
		伴侣 = 0,
		好友数据 = {},
		祈福 = 0,
		装备属性 = {躲闪=0,力量=0,魔力=0,耐力=0,体质=0,命中 = 0,气血回复效果 = 0,固定伤害 = 0,气血 = 0,法防 = 0,抵抗封印等级 = 0,法术防御 = 0,魔法 = 0,灵力 = 0,速度 = 0,法术伤害结果 = 0,
		格挡值 = 0,狂暴等级 = 0,治疗能力 = 0,穿刺等级 = 0,防御 = 0,抗物理暴击等级 = 0,敏捷 = 0,法术暴击等级 = 0,封印命中等级 = 0,伤害 = 0,物理暴击等级 = 0,法术伤害 = 0,抗法术暴击等级 = 0},
		幻化属性 = {
			命中 = 0,
			气血回复效果 = 0,
			固定伤害 = 0,
			气血 = 0,
			法防 = 0,
			抵抗封印等级 = 0,
			法术防御 = 0,
			抗封印等级 = 0,
			灵力 = 0,
			魔法 = 0,
			法术伤害结果 = 0,
			速度 = 0,
			格挡值 = 0,
			狂暴等级 = 0,
			穿刺等级 = 0,
			防御 = 0,
			治疗能力 = 0,
			抗物理暴击等级 = 0,
			敏捷 = 0,
			法术暴击等级 = 0,
			封印命中等级 = 0,
			伤害 = 0,
			物理暴击等级 = 0,
			法术伤害 = 0,
			抗法术暴击等级 = 0
		},
		装备三围 = {体质 = 0,力量 = 0,敏捷 = 0,耐力 = 0,魔力 = 0},
		变身 = {
			技能 = ""
		},
		双倍=false,
		获得经验 = 0,
		获得银子 = 0,
		传送地图 = 1501
	}

	for n = 1, #全局变量.基础属性 do
		self.数据[全局变量.基础属性[n]] = lv + 10
	end
    self.数据.礼包={新手=0}
	self.数据.装备数据 = {}
	self.数据.道具数据 = {包裹 = {},行囊 = {}}
	self.数据.召唤兽数据 = {参战 = 0,数据 = {}}
	self.数据.人物修炼 = {}
	for n = 1, #全局变量.修炼名称 do
		self.数据.人物修炼[全局变量.修炼名称[n]] = {经验 = 0,上限 = 20,等级 = 9}
	end
	self.数据.人物修炼['猎术'].等级 = 0

	self.数据.人物修炼.当前 = "攻击"
	self.数据.召唤兽修炼 = {}
	for n = 1, #全局变量.修炼名称 do
		self.数据.召唤兽修炼[全局变量.修炼名称[n]] = {经验 = 0,上限 = 20,等级 = 9}
	end
	self.数据.召唤兽修炼.当前 = "攻击"
	self.数据.武器数据 = {名称 = "",强化 = 0,等级 = 0,类别 = 0}
	self.数据.地图数据 = {编号 = 1003,x = 400,y = 500}
	self.数据.押镖次数 = 50
	self.数据.仓库 = {{},{},{}}
	self.数据.pk开关 = false
	self.数据.pk时间 = 0
	self.数据.pk保护 = 0
	self.数据.官职贡献度 = 0
	self.数据.官职任务次数 = 1
	self.数据.飞贼次数 = 0
	self.数据.锦衣 = {[35] = 0,[36] = 0,[37] = 0,[38] = 0}
	self.数据.特技数据 = {}
	self.数据.阵法 = {地载阵 = false,蛇蟠阵 = false,云垂阵 = false,风扬阵 = false,普通 = true,虎翼阵 = false,龙飞阵 = false,鸟翔阵 = false,天覆阵 = false}
	self.数据.点化次数 = 0
	self.数据.坐骑数据 = {}
	self.发送数据 = {}
	self:刷新战斗属性()
end

function 角色处理类:计算修炼等级经验(等级, 上限)
	if 等级 == 0 then
		return 110
	end

	self.临时经验 = 110

	for n = 1, 上限 + 1 do
		self.临时经验 = self.临时经验 + 20 + n * 20

		if n == 等级 then
			return self.临时经验
		end
	end
end

function 角色处理类:获取角色染色数据()
	self.发送数据 = {
		染色 = self.数据.染色,
		造型 = self.数据.造型,
		武器数据 = self.数据.武器数据
	}

	return self.发送数据
end

function 角色处理类:取可用格子数量(类型)
	self.临时数量 = 0

	for n = 1, 20 do
		if self.数据.道具[类型][n] == nil then
			self.临时数量 = self.临时数量 + 1
		end
	end

	return self.临时数量
end

function 角色处理类:技能降级(id)
	self.条件达成 = false
	self.扣除编号 = 0
	self.扣除等级 = 0

	for n = 1, #self.数据.门派技能 do
		if self.扣除等级 < self.数据.门派技能[n].等级 then
			self.扣除编号 = n
			self.扣除等级 = self.数据.门派技能[n].等级
		end
	end

	if self.扣除编号 ~= 0 then
		self.数据.门派技能[self.扣除编号].等级 = self.数据.门派技能[self.扣除编号].等级 - 1

		发送数据(玩家数据[id].连接id, 9, "#xt/#y/你因为死亡导致门派技能" .. self.数据.门派技能[self.扣除编号].名称 .. "#y/等级下降1级")

		self.降级名称 = self.降级名称 .. "#w/你因为死亡导致门派技能" .. self.数据.门派技能[self.扣除编号].名称 .. "#w/等级下降1级" .. "\n"
	end
end

function 角色处理类:清空包裹(id)
	for n = 1, 20 do
		if self.数据.道具.包裹[n] ~= nil then
			玩家数据[id].道具.数据[self.数据.道具.包裹[n]] = nil
			self.数据.道具.包裹[n] = nil
		end
	end

	发送数据(玩家数据[id].连接id, 3006, "88")
end

function 角色处理类:取是否佩戴装备()
	for n = 21, 26 do
		if self.数据.装备数据[n] ~= nil then
			return true
		end
	end

	return false
end

function 角色处理类:清除专用特效(id)
	for n = 21, 26 do
		if self.数据.装备数据[n] ~= nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]] ~= nil then
			玩家数据[id].道具.数据[self.数据.装备数据[n]].专用 = nil
		end
	end
end

function 角色处理类:刷新装备属性new(id)
	 self.数据.锦衣数据 ={}
	--  if first or not self.数据.装备属性 then
	-- 	self.数据.敏捷 = self.数据.敏捷 - self.数据.装备三围.敏捷 --装备属性
	-- 	self.数据.力量 = self.数据.力量 - self.数据.装备三围.力量 --装备属性
	-- 	self.数据.魔力 = self.数据.魔力 - self.数据.装备三围.魔力 --装备属性
	-- 	self.数据.耐力 = self.数据.耐力 - self.数据.装备三围.耐力 --装备属性
	-- 	self.数据.体质 = self.数据.体质 - self.数据.装备三围.体质 --装备属性
	-- else
	-- 	self.数据.敏捷 = self.数据.敏捷 - self.数据.装备属性.敏捷 --装备属性
	-- 	self.数据.力量 = self.数据.力量 - self.数据.装备属性.力量 --装备属性
	-- 	self.数据.魔力 = self.数据.魔力 - self.数据.装备属性.魔力 --装备属性
	-- 	self.数据.耐力 = self.数据.耐力 - self.数据.装备属性.耐力 --装备属性
	-- 	self.数据.体质 = self.数据.体质 - self.数据.装备属性.体质 --装备属性
	-- end
	self.数据.特技数据 = {}
	self.数据.追加技能 = {}
	self.数据.附加技能 = {}
	self.数据.愤怒特效 = nil
	self.临时追加 = {}
	self.临时附加 = {}
	self.数据.装备属性 = {
	    躲闪=0,
	    力量=0,
	    魔力=0,
	    耐力=0,
	    体质=0,
		命中 = 0,
		气血回复效果 = 0,
		固定伤害 = 0,
		气血 = 0,
		法防 = 0,
		抵抗封印等级 = 0,
		法术防御 = 0,
		抗封印等级 = 0,
		灵力 = 0,
		魔法 = 0,
		法术伤害结果 = 0,
		速度 = 0,
		格挡值 = 0,
		狂暴等级 = 0,
		穿刺等级 = 0,
		防御 = 0,
		治疗能力 = 0,
		抗物理暴击等级 = 0,
		敏捷 = 0,
		法术暴击等级 = 0,
		封印命中等级 = 0,
		伤害 = 0,
		物理暴击等级 = 0,
		法术伤害 = 0,
		抗法术暴击等级 = 0
	}
	self.数据.幻化属性 = {
		命中 = 0,
		气血回复效果 = 0,
		固定伤害 = 0,
		气血 = 0,
		法防 = 0,
		抵抗封印等级 = 0,
		法术防御 = 0,
		抗封印等级 = 0,
		灵力 = 0,
		魔法 = 0,
		法术伤害结果 = 0,
		速度 = 0,
		格挡值 = 0,
		狂暴等级 = 0,
		穿刺等级 = 0,
		防御 = 0,
		治疗能力 = 0,
		抗物理暴击等级 = 0,
		敏捷 = 0,
		法术暴击等级 = 0,
		封印命中等级 = 0,
		伤害 = 0,
		物理暴击等级 = 0,
		法术伤害 = 0,
		抗法术暴击等级 = 0
	}

	self.数据.装备三围 = {体质 = 0,力量 = 0,敏捷 = 0,耐力 = 0,魔力 = 0}

	self.数据.武器数据 = {
		名称 = "",
		强化 = 0,
		等级 = 0,
		类别 = ""
	}

	for n = 31, 34 do
		if self.数据.灵饰[n] ~= 0 then
			self.幻化类型 = 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.基础.类型

			if self.幻化类型 == "气血" or self.幻化类型 == "速度" or self.幻化类型 == "防御" or self.幻化类型 == "伤害" then
				self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.基础.数值
				self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.基础.强化
			else
				self.数据.幻化属性[self.幻化类型] = self.数据.幻化属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.基础.数值
				self.数据.幻化属性[self.幻化类型] = self.数据.幻化属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.基础.强化
			end

			for i = 1, #玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加 do
				self.幻化类型 = 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加[i].类型

				if self.幻化类型 == "气血" or self.幻化类型 == "速度" or self.幻化类型 == "防御" or self.幻化类型 == "伤害" then
					self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加[i].数值
					self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加[i].强化
				else
					self.数据.幻化属性[self.幻化类型] = self.数据.幻化属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加[i].数值
					self.数据.幻化属性[self.幻化类型] = self.数据.幻化属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.灵饰[n]].幻化属性.附加[i].强化
				end
			end
		end
	end
	if self.数据.锦衣[35] ~= 0 then
	   self.数据.锦衣数据.锦衣 = 玩家数据[id].道具.数据[self.数据.锦衣[35]].名称
	end
	if self.数据.锦衣[36] ~= 0 then
	   self.数据.锦衣数据.光环 = 玩家数据[id].道具.数据[self.数据.锦衣[36]].名称
	end
	if self.数据.锦衣[37] ~= 0 then
	   self.数据.锦衣数据.足迹 = 玩家数据[id].道具.数据[self.数据.锦衣[37]].名称
	end
	for n = 21, 26 do
		if self.数据.装备数据[n] ~= nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]] ~= nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]].耐久度 > 0 then
			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性 ~= nil then
				self.幻化类型 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.基础.类型
				self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.基础.数值
				self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.基础.强化

				for i = 1, #玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.附加 do
					self.幻化类型 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.附加[i].类型
					self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.附加[i].数值
					self.数据.装备属性[self.幻化类型] = self.数据.装备属性[self.幻化类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].幻化属性.附加[i].强化
				end
			end

			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].套装 ~= nil then
				self.套装名称 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].套装.名称

				if 玩家数据[id].道具.数据[self.数据.装备数据[n]].套装.类型 == "追加法术" then
					if self.临时追加[self.套装名称] == nil then
						self.临时追加[self.套装名称] = 1
					else
						self.临时追加[self.套装名称] = self.临时追加[self.套装名称] + 1
					end
				elseif self.临时附加[self.套装名称] == nil then
					self.临时附加[self.套装名称] = 1
				else
					self.临时附加[self.套装名称] = self.临时附加[self.套装名称] + 1
				end
			end
			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼~=nil then
				for i=1,#玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼 do
					if 玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].正负=="-" then
						self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].类型]=self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].类型]-玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].属性
					else
						self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].类型]=self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].类型]+玩家数据[id].道具.数据[self.数据.装备数据[n]].熔炼[i].属性
					end
				end
			end
			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔~=nil then
				if 玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔.时间 <= os.time() then
					玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔 = nil
				else
				self.数据.幻化属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔.类型]=self.数据.幻化属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔.类型]+玩家数据[id].道具.数据[self.数据.装备数据[n]].附魔.属性

				end

			end
			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].特效 ~= nil then
				for i = 1, #玩家数据[id].道具.数据[self.数据.装备数据[n]].特效 do
					if 玩家数据[id].道具.数据[self.数据.装备数据[n]].特效[i] == "愤怒" then
						self.数据.愤怒特效 = true
					end
				end
			end

			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].特技 ~= nil then
				self.特技重复 = false

				for i = 1, #self.数据.特技数据 do
					if self.数据.特技数据[i] == 玩家数据[id].道具.数据[self.数据.装备数据[n]].特技 then
						self.特技重复 = true
					end
				end

				self.数据.特技数据[#self.数据.特技数据 + 1] = 玩家数据[id].道具.数据[self.数据.装备数据[n]].特技
			end

			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].三围属性 == nil then
				玩家数据[id].道具.数据[self.数据.装备数据[n]].三围属性 = {}
			end

			if 玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果 ~= nil then
				if 玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果.时间 <= os.time() then
					玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果 = nil
				else
					self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果.类型] = self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果.类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].临时效果.数值
				end
			end

			for i = 1, #玩家数据[id].道具.数据[self.数据.装备数据[n]].三围属性 do
				self.临时数值 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].三围属性[i].数值
				self.临时类型 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].三围属性[i].类型
				self.数据.装备三围[self.临时类型] = self.数据.装备三围[self.临时类型] + self.临时数值
			end

			for i = 1, #玩家数据[id].道具.数据[self.数据.装备数据[n]].锻造数据 do
				self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].锻造数据[i].类型] = self.数据.装备属性[玩家数据[id].道具.数据[self.数据.装备数据[n]].锻造数据[i].类型] + 玩家数据[id].道具.数据[self.数据.装备数据[n]].锻造数据[i].数值
			end
            if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石~= nil and #玩家数据[id].道具.数据[self.数据.装备数据[n]].符石>0 then
                if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石.组合~=nil then
                  local 符石名称 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石.组合.名称
                  local 符石等级 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石.组合.等级
                  if 符石名称 == "万丈霞光" then
                          if 符石等级 ==1  then
                            self.数据.幻化属性.气血回复效果=self.数据.幻化属性.气血回复效果+50
                          elseif  符石等级 ==2  then
                             self.数据.幻化属性.气血回复效果=self.数据.幻化属性.气血回复效果+80
                          elseif  符石等级 ==3  then
                              self.数据.幻化属性.气血回复效果=self.数据.幻化属性.气血回复效果+120
                         elseif  符石等级 ==4  then
                              self.数据.幻化属性.气血回复效果=self.数据.幻化属性.气血回复效果+200
                          end
                  elseif 符石名称 == "望穿秋水" then
                    self.数据.装备属性.灵力=self.数据.装备属性.灵力+30
                  elseif 符石名称 == "无懈可击" then
                    self.数据.装备属性.防御=self.数据.装备属性.防御+30
                  elseif 符石名称 == "万里横行" then
                    self.数据.装备属性.伤害=self.数据.装备属性.伤害+40
                  elseif 符石名称 == "日落西山" then
                     self.数据.装备属性.速度=self.数据.装备属性.速度+40
                   elseif 符石名称 == "高山流水" then
                          if 符石等级 ==1  then
                            self.数据.幻化属性.法术伤害=self.数据.幻化属性.法术伤害+math.floor(self.数据.等级/3+30)
                          elseif  符石等级 ==2  then
                             self.数据.幻化属性.法术伤害=self.数据.幻化属性.法术伤害+math.floor(self.数据.等级/2+30)
                          elseif  符石等级 ==3  then
                              self.数据.幻化属性.法术伤害=self.数据.幻化属性.法术伤害+math.floor(self.数据.等级+30)
                          end
                   elseif 符石名称 == "百无禁忌" then
                          if 符石等级 ==1  then
                            self.数据.幻化属性.抵抗封印等级=self.数据.幻化属性.抵抗封印等级+math.floor(4*100*self.数据.等级/1000)
                          elseif  符石等级 ==2  then
                             self.数据.幻化属性.抵抗封印等级=self.数据.幻化属性.抵抗封印等级+math.floor(8*100*self.数据.等级/1000)
                          elseif  符石等级 ==3  then
                              self.数据.幻化属性.抵抗封印等级=self.数据.幻化属性.抵抗封印等级+math.floor(12*100*self.数据.等级/1000)
                          end
                   end
                end

       for i=1,#玩家数据[id].道具.数据[self.数据.装备数据[n]].符石 do
       	if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i]~=nil then
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].气血 ~= nil then
          self.数据.装备属性.气血 = self.数据.装备属性.气血 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].气血
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].魔法 ~= nil then
          self.数据.装备属性.魔法 = self.数据.装备属性.魔法 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].魔法
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].命中 ~= nil then
          self.数据.装备属性.命中 = self.数据.装备属性.命中 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].命中
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].伤害 ~= nil then
          self.数据.装备属性.伤害 = self.数据.装备属性.伤害 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].伤害
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].防御 ~= nil then
          self.数据.装备属性.防御 = self.数据.装备属性.防御 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].防御
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].速度 ~= nil then
          self.数据.装备属性.速度 = self.数据.装备属性.速度 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].速度
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].灵力 ~= nil then
          self.数据.装备属性.灵力 = self.数据.装备属性.灵力 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].灵力
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].敏捷 ~= nil then
          self.数据.装备属性.敏捷 = self.数据.装备属性.敏捷 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].敏捷
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].法术防御 ~= nil then
          self.数据.幻化属性.法术防御 = self.数据.幻化属性.法术防御 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].法术防御
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].法术伤害 ~= nil then
          self.数据.幻化属性.法术伤害 = self.数据.幻化属性.法术伤害 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].法术伤害
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].固定伤害 ~= nil then
          self.数据.幻化属性.固定伤害 = self.数据.幻化属性.固定伤害 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].固定伤害
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].力量 ~= nil then
          self.数据.装备属性.力量 = self.数据.装备属性.力量 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].力量
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].体质 ~= nil then
          self.数据.装备属性.体质 = self.数据.装备属性.体质 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].体质
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].耐力 ~= nil then
          self.数据.装备属性.耐力 = self.数据.装备属性.耐力 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].耐力
         end
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].魔力 ~= nil then
          self.数据.装备属性.魔力 = self.数据.装备属性.魔力 +   玩家数据[id].道具.数据[self.数据.装备数据[n]].符石[i].魔力
         end
     end
       end
     end

			if n == 21 then
				self.数据.装备属性.伤害 = self.数据.装备属性.伤害 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].伤害
				self.数据.装备属性.命中 = self.数据.装备属性.命中 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].命中
				self.数据.武器数据 = {
					强化 = 0,
					名称 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].名称,
					类别 = 武器类型[玩家数据[id].道具.数据[self.数据.装备数据[n]].名称],
					等级 = 玩家数据[id].道具.数据[self.数据.装备数据[n]].等级
				}

				if 玩家数据[id].道具.数据[self.数据.装备数据[n]].强化 == nil then
					self.数据.武器数据.强化 = nil
				else
					self.数据.武器数据.强化 = 1
				end

				if id ~= nil then
					发送数据(玩家数据[id].连接id, 2010, self:获取角色数据())

					self.发送信息 = self.数据.武器数据
					self.发送信息.id = id

					地图处理类:发送数据(id, 1016, self:获取角色数据(), 玩家数据[id].地图)
				end
			elseif n == 22 then
				self.数据.装备属性.魔法 = self.数据.装备属性.魔法 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].魔法
				self.数据.装备属性.防御 = self.数据.装备属性.防御 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].防御
			elseif n == 23 then
				self.数据.装备属性.气血 = self.数据.装备属性.气血 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].气血
				self.数据.装备属性.防御 = self.数据.装备属性.防御 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].防御
			elseif n == 24 then
				self.数据.装备属性.灵力 = self.数据.装备属性.灵力 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].灵力
				self.数据.装备属性.法防 = self.数据.装备属性.灵力
			elseif n == 25 then
				self.数据.装备属性.敏捷 = self.数据.装备属性.敏捷 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].敏捷
				self.数据.装备属性.防御 = self.数据.装备属性.防御 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].防御
			elseif n == 26 then
				self.数据.装备属性.防御 = self.数据.装备属性.防御 + 玩家数据[id].道具.数据[self.数据.装备数据[n]].防御
			end
		end
	end


	for n, v in pairs(self.临时追加) do
		if self.临时追加[n] >= 3 then
			self.数据.追加技能[#self.数据.追加技能 + 1] = n
		end
	end

	for n, v in pairs(self.临时附加) do
		if self.临时附加[n] >= 3 then
			self.数据.附加技能[#self.数据.附加技能 + 1] = n
		end
	end

	-- for n = 1, #全局变量.基础属性 do
	-- 	self.数据[全局变量.基础属性[n]] = self.数据[全局变量.基础属性[n]] + self.数据.装备三围[全局变量.基础属性[n]]
	-- end
	-- self.数据.力量=  self.数据.力量+self.数据.装备属性.力量
	-- self.数据.魔力=  self.数据.魔力+self.数据.装备属性.魔力
	-- self.数据.耐力=  self.数据.耐力+self.数据.装备属性.耐力
	-- self.数据.体质=  self.数据.体质+self.数据.装备属性.体质
	-- self.数据.敏捷 = self.数据.敏捷 + self.数据.装备属性.敏捷
	-- self.数据.幻化属性.固定伤害  = self.数据.幻化属性.固定伤害 +math.floor(self.数据.强化技能.固伤强化 * 2)
	-- self:刷新战斗属性(1)

	-- if id ~= nil then
	-- 	-- Nothing
	-- end

	-- self.发送信息 = self.数据.武器数据
	-- self.发送信息.id = self.数据.id
end

function 角色处理类:刷新装备属性(id)
	local first = not self.数据.装备属性 or not self.数据.幻化属性 or not self.数据.装备三围
	if self.数据.装备属性 then 
		self.数据.敏捷 = self.数据.敏捷 - self.数据.装备属性.敏捷
		self.数据.力量 = self.数据.力量 - self.数据.装备属性.力量
		self.数据.魔力 = self.数据.魔力 - self.数据.装备属性.魔力
		self.数据.耐力 = self.数据.耐力 - self.数据.装备属性.耐力
		self.数据.体质 = self.数据.体质 - self.数据.装备属性.体质
	end

	if self.数据.装备三围 then
		for n = 1, #全局变量.基础属性 do
			self.数据[全局变量.基础属性[n]] = self.数据[全局变量.基础属性[n]] - self.数据.装备三围[全局变量.基础属性[n]]
		end	
	end

	self:刷新装备属性new(id)

	if not first then

		for n = 1, #全局变量.基础属性 do
			self.数据[全局变量.基础属性[n]] = self.数据[全局变量.基础属性[n]] + self.数据.装备三围[全局变量.基础属性[n]]
		end

		self.数据.力量 =  self.数据.力量+self.数据.装备属性.力量
		self.数据.魔力 =  self.数据.魔力+self.数据.装备属性.魔力
		self.数据.耐力 =  self.数据.耐力+self.数据.装备属性.耐力
		self.数据.体质 =  self.数据.体质+self.数据.装备属性.体质
		self.数据.敏捷 = self.数据.敏捷 + self.数据.装备属性.敏捷

		self.数据.幻化属性.固定伤害  = self.数据.幻化属性.固定伤害 + math.floor(self.数据.强化技能.固伤强化 * 2)

	end

	self:刷新战斗属性(1)

	if id ~= nil then
		-- Nothing
	end

	self.发送信息 = self.数据.武器数据
	self.发送信息.id = self.数据.id
end

function 角色处理类:耐久处理(id,类型) --1=物理伤害 2=防具挨打 3=法术伤害
  self.特效永不磨损 = false
  if 类型==1 then
   if self.数据.装备数据[21]~=nil and 玩家数据[id].道具.数据[self.数据.装备数据[21]]~=nil then
      for i=1,2 do
         if 玩家数据[id].道具.数据[self.数据.装备数据[21]].特效~=nil and 玩家数据[id].道具.数据[self.数据.装备数据[21]].特效[i]~= nil and 玩家数据[id].道具.数据[self.数据.装备数据[21]].特效[i] =="永不磨损"  then
            self.特效永不磨损 = true
         end
      end
      if self.特效永不磨损 == false then
        玩家数据[id].道具.数据[self.数据.装备数据[21]].耐久度=玩家数据[id].道具.数据[self.数据.装备数据[21]].耐久度-0.1
        if 玩家数据[id].道具.数据[self.数据.装备数据[21]].耐久度<=0 then
            玩家数据[id].道具.数据[self.数据.装备数据[21]].耐久度=0
            self:刷新装备属性(id)
            发送数据(玩家数据[id].连接id,7,"#y/你的#r/"..玩家数据[id].道具.数据[self.数据.装备数据[21]].名称.."#y/因耐久度过低已无法使用")
       end
      end
   end

  elseif 类型==2 then
    for n=22,26 do
     if self.数据.装备数据[n]~=nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]]~=nil   then
      for i=1,2 do
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].特效~=nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]].特效[i]~= nil and 玩家数据[id].道具.数据[self.数据.装备数据[n]].特效[i] =="永不磨损"  then
            self.特效永不磨损 = true
         end
      end
      if self.特效永不磨损 == false then
         玩家数据[id].道具.数据[self.数据.装备数据[n]].耐久度=玩家数据[id].道具.数据[self.数据.装备数据[n]].耐久度-0.2
         if 玩家数据[id].道具.数据[self.数据.装备数据[n]].耐久度<=0 then
            玩家数据[id].道具.数据[self.数据.装备数据[n]].耐久度=0
            self:刷新装备属性(id)
            发送数据(玩家数据[id].连接id,7,"#y/你的#r/"..玩家数据[id].道具.数据[self.数据.装备数据[n]].名称.."#y/因耐久度过低已无法使用")
         end
      end
     end
    end
  end
end
function 角色处理类:删除称谓(id, 称谓, 系统)
	if 称谓 == self.数据.门派 .. "首席弟子" and 系统 == nil and 玩家数据[id] ~= nil then
		发送数据(玩家数据[id].连接id, 7, "#y/该称谓不允许被删除")

		return 0
	end

	self.删除序列 = 0

	for n = 1, #self.数据.称谓 do
		if self.数据.称谓[n] == 称谓 then
			self.删除序列 = n
		end
	end

	if self.删除序列 == 0 then
		if 玩家数据[id] ~= nil then
			发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的称谓")
		end
	else
		table.remove(self.数据.称谓, self.删除序列)

		if 称谓 == self.数据.称谓.当前 then
			self.数据.称谓.当前 = ""

			if 玩家数据[id] ~= nil then
				发送数据(玩家数据[id].连接id, 2012, self.数据.称谓.当前)
				地图处理类:更改称谓(玩家数据[id].地图, id, self.数据.称谓.当前)
			end
		end

		if 玩家数据[id] ~= nil then
			发送数据(玩家数据[id].连接id, 7, "#y/删除称谓成功")
			发送数据(玩家数据[id].连接id, 2011, 玩家数据[id].角色:获取称谓数据())
		end
	end
end
function 角色处理类:检查称谓(内容)
	for i=1,#self.数据.称谓 do
		if self.数据.称谓[i] == "内容" then
		   return true
		end
	end
	 return false
end


function 角色处理类:回收称谓(内容)
  for i=1,#self.数据.称谓 do
		if self.数据.称谓[i] == "内容" then
		   table.remove(self.数据.称谓,i)
		end
	end
   if 内容 == self.数据.称谓.当前 then
		self.数据.称谓.当前 = ""
		if 玩家数据[self.数据.id] ~= nil then
				发送数据(玩家数据[self.数据.id].连接id, 2012, self.数据.称谓.当前)
				地图处理类:更改称谓(玩家数据[self.数据.id].地图, id, self.数据.称谓.当前)
		end
	end

end
function 角色处理类:重置属性点(id)
	for n = 1, #全局变量.基础属性 do
		self.数据[全局变量.基础属性[n]] = 10 + self.数据.等级
	end

	self.数据.潜能 = self.数据.等级 * 5
	self.数据.装备三围 = {
		体质 = 0,
		力量 = 0,
		敏捷 = 0,
		耐力 = 0,
		魔力 = 0
	}


	self:刷新装备属性(id)
	self:刷新战斗属性()
end

function 角色处理类:死亡处理(类型, id, 战斗类型, 消息)
	if 消息 == nil then
		return 0
	end

	self:添加系统消息(id, "#h/你被" .. 消息 .. "杀死了")

	if self.数据.等级 < 69 and 战斗类型 ~= 200003 and 战斗类型 ~= 100032 then
		return 0
	end

	if 类型 == 1 then
		self.死亡倍率 = 2
	end

	if 战斗类型 == 200003 then
		self.死亡倍率 = 5
		self.扣除银子1 = math.floor(self.数据.道具.货币.银子 * 0.35)
	elseif 战斗类型 == 100032 then
		self.死亡倍率 = 10
		self.扣除银子1 = math.floor(self.数据.道具.货币.银子 * 0.5)
	else
		self.扣除银子1 = math.floor(self.数据.道具.货币.银子 * 0.01)
	end

	self.扣除经验1 = math.floor((self.数据.等级 * self.数据.等级+ 500) * self.死亡倍率) -- * self.数据.等级
	self.组合语句 = ""
	self.降级名称 = ""

	if 银子检查(id, self.扣除银子1) == false then
		发送数据(玩家数据[id].连接id, 9, "#xt/#y/你因为死亡损失了" .. self.数据.道具.货币.银子 .. "两银子")

		self.数据.道具.货币.银子 = 0
		self.组合语句 = "#w/你因为死亡损失了#r/" .. self.数据.道具.货币.银子 .. "#w/两银子"

		self:技能降级(id)
	else
		self:扣除银子(id, self.扣除银子1, 19)
		发送数据(玩家数据[id].连接id, 9, "#xt/#y/你因为死亡损失了" .. self.扣除银子1 .. "两银子")

		self.组合语句 = "#w/你因为死亡损失了#r/" .. self.扣除银子1 .. "#w/两银子"
	end

	self.组合语句 = self.组合语句 .. "\n"

	if self.数据.当前经验 < self.扣除经验1 then
		发送数据(玩家数据[id].连接id, 9, "#xt/#y/你因为死亡损失了" .. self.数据.当前经验 .. "点经验")

		self.组合语句 = self.组合语句 .. "#w/你因为死亡损失了#r/" .. self.数据.当前经验 .. "#w/点经验"
		self.数据.当前经验 = 0

		self:技能降级(id)
	else
		self:扣除经验(id, self.扣除经验1, 19)

		self.组合语句 = self.组合语句 .. "#w/你因为死亡损失了#r/" .. self.扣除经验1 .. "#w/点经验"

		发送数据(玩家数据[id].连接id, 9, "#xt/#y/你因为死亡损失了" .. self.扣除经验1 .. "点经验")
	end

	self:添加系统消息(id, self.组合语句)

	if 战斗类型 == 200003 or 战斗类型 == 100032 then
		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)

		if 玩家数据[id].队伍 ~= 0 then
			队伍处理类:离开队伍(玩家数据[id].队伍, id, 1)
		end

		self.数据.pk保护 = os.time() + 7200
		self.数据.pk时间 = 0

		地图处理类:指定跳转1(id, 1125, 28, 15, 1)
	end

	if self.降级名称 ~= "" then
		self:添加系统消息(id, self.降级名称)
	end

	if 战斗类型 == 100032 then
		self.降级名称 = ""

		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)
		self:技能降级(id)

		if self.降级名称 ~= "" then
			self:添加系统消息(id, self.降级名称)
		end

		self.数据.阴德 = 10
		天罚数据表[id] = 0
		self.掉落序列 = {}

		for n = 21, 26 do
			if self.数据.装备数据[n] ~= nil then
				self.掉落序列[#self.掉落序列 + 1] = n
			end
		end

		if #self.掉落序列 ~= 0 then
			self.掉落序号 = self.掉落序列[取随机数(1, #self.掉落序列)]

			self:添加系统消息(id, "#w/你因为死亡损失了#r/" .. 玩家数据[id].道具.数据[self.数据.装备数据[self.掉落序号]].名称 .. "#w/装备")

			玩家数据[id].道具.数据[self.数据.装备数据[self.掉落序号]] = nil
			self.数据.装备数据[self.掉落序号] = nil

			玩家数据[id].角色:刷新装备属性(id)

			if self.掉落序号 == 21 then
				发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())

				self.发送信息 = self.数据.武器数据
				self.发送信息.id = id

				地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
			end
		end
	end
end

function 角色处理类:取是否佩戴装备()
	for n = 21, 26 do
		if self.数据.装备数据[n] ~= nil then
			return true
		end
	end

	return false
end

function 角色处理类:创建坐骑(id, 类型)
	if #self.数据.坐骑数据 >= 12 then
		发送数据(玩家数据[id].连接id, 7, "#y/你已经无法携带更多的坐骑了")

		return 0
	else
		self.数据.坐骑数据[#self.数据.坐骑数据 + 1] = {
			敏捷 = 0,
			体质 = 0,
			当前经验 = 0,
			环境度 = 100,
			魔力 = 0,
			耐力 = 0,
			好感度 = 100,
			饱食度 = 100,
			力量 = 0,
			潜能 = 0,
			等级 = 0,
			升级经验 = 400,
			类型 = 类型
		}

		for n = 1, 50 do
			self.临时类型 = 全局变量.基础属性[取随机数(1, #全局变量.基础属性)]
			self.数据.坐骑数据[#self.数据.坐骑数据][self.临时类型] = self.数据.坐骑数据[#self.数据.坐骑数据][self.临时类型] + 1
		end
	end

	self.临时成长 = 1.2
	self.临时成长 = 玩家数据[id].召唤兽:保留成长(self.临时成长) / 1
	self.数据.坐骑数据[#self.数据.坐骑数据].成长 = self.临时成长

	发送数据(玩家数据[id].连接id, 7, "#y/你获得了新坐骑#r/" .. 类型)
end

function 角色处理类:添加随机坐骑(id)
	if self.数据.种族 == "人" then
		self.随机坐骑 = {
			"汗血宝马",--
			"宝贝葫芦",--
			"欢喜羊羊",--
			"神气小龟"--
		}
	elseif self.数据.种族 == "魔" then
		self.随机坐骑 = {
			"魔力斗兽",--
			"宝贝葫芦",
			"披甲战狼",
			"神气小龟"
		}
	elseif self.数据.种族 == "仙" then
		self.随机坐骑 = {
			"云魅仙鹿",
			"宝贝葫芦",
			"闲云野鹤",
			"神气小龟"
		}
	end

	self:创建坐骑(id, self.随机坐骑[取随机数(1, #self.随机坐骑)])
end

function 角色处理类:添加随机祥瑞(id)
	if self.数据.种族 == "人" then
		self.随机坐骑 = {
            "九尾神狐",
			"粉红小驴",
			"天使猪猪",
			"金麟仙子"
		}
	elseif self.数据.种族 == "魔" then
		self.随机坐骑 = {
			"战火穷奇",
			"银色穷奇",
			"暗影战豹",
			"金麟仙子"
		}
	elseif self.数据.种族 == "仙" then
		self.随机坐骑 = {
			"金麟仙子",
			"九霄冰凤",
			"幽骨战龙",
			"玉瓷葫芦"
		}
	end

	self:创建坐骑(id, self.随机坐骑[取随机数(1, #self.随机坐骑)])
end

function 角色处理类:坐骑加点处理(id, 编号, 标识)
	if self.数据.坐骑数据[编号].潜能 <= 0 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只坐骑当前没有可分配的属性点")

		return 0
	end

	self.数据.坐骑数据[编号].潜能 = self.数据.坐骑数据[编号].潜能 - 1
	self.临时类型 = 全局变量.基础属性[标识]
	self.数据.坐骑数据[编号][self.临时类型] = self.数据.坐骑数据[编号][self.临时类型] + 1

	玩家数据[id].角色:刷新战斗属性(1)
	发送数据(玩家数据[id].连接id, 20031, {
		坐骑 = self.数据.坐骑数据[编号],
		编号 = 编号
	})
end

function 角色处理类:坐骑驯养处理(id, 编号)
	if self.数据.坐骑数据[编号].等级 >= self.数据.等级 + 15 then
		发送数据(玩家数据[id].连接id, 7, "#y/你的坐骑等级已经超过人物15级了")

		return 0
	elseif self.数据.当前经验 < 20000000 then
		发送数据(玩家数据[id].连接id, 7, "#y/每次驯养都需要消耗2000万人物经验")

		return 0
	elseif self.数据.当前体力 < 50 then
		发送数据(玩家数据[id].连接id, 7, "#y/每次驯养都需要消耗50点体力")

		return 0
	else
		self.数据.当前体力 = self.数据.当前体力 - 50
		self.数据.当前经验 = self.数据.当前经验 - 20000000
		self.数据.坐骑数据[编号].当前经验 = self.数据.坐骑数据[编号].当前经验 + 15000000

		while self.数据.坐骑数据[编号].升级经验 <= self.数据.坐骑数据[编号].当前经验 do
			self.数据.坐骑数据[编号].当前经验 = self.数据.坐骑数据[编号].当前经验 - self.数据.坐骑数据[编号].升级经验
			self.数据.坐骑数据[编号].等级 = self.数据.坐骑数据[编号].等级 + 1
			self.数据.坐骑数据[编号].升级经验 = 升级经验[self.数据.坐骑数据[编号].等级] * 10

			for n = 1, 5 do
				self.临时类型 = 全局变量.基础属性[n]
				self.数据.坐骑数据[编号][self.临时类型] = self.数据.坐骑数据[编号][self.临时类型] + 1
			end

			self.数据.坐骑数据[编号].潜能 = self.数据.坐骑数据[编号].潜能 + 5

			玩家数据[id].角色:刷新战斗属性(1)
		end

		发送数据(玩家数据[id].连接id, 7, "#y/驯养成功")
		发送数据(玩家数据[id].连接id, 7, "#y/你的体力减少了50点")
		发送数据(玩家数据[id].连接id, 7, "#y/你的人物经验减少了2000万")
		发送数据(玩家数据[id].连接id, 7, "#y/你的这只坐骑获得了1500万经验")
		发送数据(玩家数据[id].连接id, 20031, {
			坐骑 = self.数据.坐骑数据[编号],
			编号 = 编号
		})
	end
end

function 角色处理类:坐骑骑乘处理(id, 编号)
	if 编号 == self.数据.坐骑编号 then
		self.数据.坐骑 = nil
		self.数据.坐骑编号 = nil

		玩家数据[id].角色:刷新战斗属性(1)
		发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
		地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
		发送数据(玩家数据[id].连接id, 2001, 玩家数据[id].角色:获取角色数据())
		发送数据(玩家数据[id].连接id, 20030, self:取坐骑数据())
	else
		self.数据.坐骑编号 = 编号
		self.数据.坐骑 = self.数据.坐骑数据[编号].类型

		玩家数据[id].角色:刷新战斗属性(1)
		发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
		地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
		发送数据(玩家数据[id].连接id, 2001, 玩家数据[id].角色:获取角色数据())
		发送数据(玩家数据[id].连接id, 20030, self:取坐骑数据())
	end
end

function 角色处理类:坐骑处理(id)
	if self.数据.坐骑 ~= nil then
		self.数据.备用坐骑 = self.数据.坐骑
		self.数据.坐骑 = nil

		玩家数据[id].角色:刷新战斗属性(1)
		发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
		地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
		发送数据(玩家数据[id].连接id, 2001, 玩家数据[id].角色:获取角色数据())
		发送数据(玩家数据[id].连接id, 7, "#y/按下F2后可骑乘坐骑")
	elseif self.数据.备用坐骑 == nil then
		发送数据(玩家数据[id].连接id, 7, "#y/你现在好像还没有坐骑")
	else
		发送数据(玩家数据[id].连接id, 7, "#y/你骑乘了坐骑后气血、魔法上限提高了10%")
		发送数据(玩家数据[id].连接id, 7, "#y/按下F2后可收回坐骑")

		self.数据.坐骑 = self.数据.备用坐骑
		self.数据.备用坐骑 = nil

		玩家数据[id].角色:刷新战斗属性(1)
		发送数据(玩家数据[id].连接id, 2010, 玩家数据[id].角色:获取角色数据())
		地图处理类:发送数据(id, 1016, 玩家数据[id].角色:获取角色数据(), 玩家数据[id].地图)
		发送数据(玩家数据[id].连接id, 2001, 玩家数据[id].角色:获取角色数据())
	end
end

function 角色处理类:刷新战斗属性(恢复)

	if self.数据.种族 == "人" then
		self.数据.伤害 = math.floor(self.数据.力量 * 0.67) + 20
		self.数据.命中 = math.floor(self.数据.力量 * 2.01) + 20
		self.数据.灵力 = math.floor(self.数据.力量 * 0.4 + self.数据.魔力 * 0.7 + self.数据.体质 * 0.3 + self.数据.耐力 * 0.2)
		self.数据.速度 = math.floor(self.数据.敏捷 * 0.7 + self.数据.力量 * 0.1 + self.数据.体质 * 0.1 + self.数据.耐力 * 0.1)
		self.数据.躲闪 = math.floor(self.数据.敏捷)
		self.数据.魔法上限 = math.floor(self.数据.魔力 * 3)
		self.数据.最大气血 = math.floor(self.数据.体质 * 5)
		self.数据.防御 = math.floor(self.数据.耐力 * 1.5)
	elseif self.数据.种族 == "魔" then
		self.数据.伤害 = math.floor(self.数据.力量 * 0.67) + 20
		self.数据.命中 = math.floor(self.数据.力量 * 2.01) + 20
		self.数据.防御 = math.floor(self.数据.耐力 * 1.5)
		self.数据.灵力 = math.floor(self.数据.力量 * 0.4 + self.数据.魔力 * 0.7 + self.数据.体质 * 0.3 + self.数据.耐力 * 0.2)
		self.数据.速度 = math.floor(self.数据.敏捷 * 0.7 + self.数据.力量 * 0.1 + self.数据.体质 * 0.1+ self.数据.耐力 * 0.1)
		self.数据.躲闪 = math.floor(self.数据.敏捷)
		self.数据.魔法上限 = math.floor(self.数据.魔力 * 3)
		self.数据.最大气血 = math.floor(self.数据.体质 * 5)
	elseif self.数据.种族 == "仙" then
		self.数据.伤害 = math.floor(self.数据.力量 * 0.67) + 20
		self.数据.命中 = math.floor(self.数据.力量 * 2.01) + 20
		self.数据.防御 = math.floor(self.数据.耐力 * 1.5)
		self.数据.灵力 = math.floor(self.数据.力量 * 0.4 + self.数据.魔力 * 0.7 + self.数据.体质 * 0.3 + self.数据.耐力 * 0.2)
		self.数据.速度 = math.floor(self.数据.敏捷 * 0.7 + self.数据.力量 * 0.1 + self.数据.体质 * 0.1+ self.数据.耐力 * 0.1)
		self.数据.躲闪 = math.floor(self.数据.敏捷)
		self.数据.魔法上限 = math.floor(self.数据.魔力 * 3)
		self.数据.最大气血 = math.floor(self.数据.体质 * 5)
	end

	if self.数据.飞升 then
		self.飞升附加 = 0
	else
		self.飞升附加 = 0
	end

	if self.数据.坐骑编号 ~= nil and self.数据.坐骑数据[self.数据.坐骑编号] ~= nil then
		self.飞升附加 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].体质 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 5)
		self.坐骑伤害 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].力量 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 10)
		self.坐骑灵力 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].魔力 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 10)
		self.坐骑防御 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].耐力 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 10)
		self.坐骑速度 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].敏捷 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 10)
		self.坐骑魔法 = math.floor(self.数据.坐骑数据[self.数据.坐骑编号].魔力 * self.数据.坐骑数据[self.数据.坐骑编号].成长 / 5)
	else
		self.坐骑伤害 = 0
		self.坐骑灵力 = 0
		self.坐骑防御 = 0
		self.坐骑速度 = 0
	end


    self.数据.躲闪=self.数据.装备属性.躲闪+self.数据.躲闪
	self.数据.命中 = self.数据.命中 + self.数据.装备属性.命中 + self.数据.幻化属性.命中  + math.floor(self.数据.强化技能.命中强化 * 5)

	self.数据.伤害 = self.数据.伤害 + self.数据.装备属性.伤害 + math.floor(self.数据.强化技能.伤害强化 * 3) + math.floor((self.数据.命中 + self.数据.装备属性.命中 + self.数据.幻化属性.命中) * 0.35) + self.坐骑伤害+self.数据.幻化属性.伤害
	self.数据.防御 = self.数据.防御 + self.数据.装备属性.防御 + 5 + math.floor(self.数据.强化技能.防御强化 * 2) + self.坐骑防御+self.数据.幻化属性.防御
	self.数据.灵力 = self.数据.灵力 + self.数据.装备属性.灵力 + math.floor(self.数据.强化技能.灵力强化 * 2) + self.坐骑灵力+self.数据.幻化属性.灵力
	-- self.数据.法防 = self.数据.装备属性.法防 + math.floor(self.数据.魔力 * 0.35) + math.floor(self.数据.灵力 * 0.25)+self.数据.幻化属性.法防
	self.数据.法防 = self.数据.装备属性.法防 + self.数据.灵力 + self.数据.幻化属性.法防
	self.数据.速度 = self.数据.速度 + self.数据.装备属性.速度 + math.floor(self.数据.强化技能.速度强化 * 1.5) + self.坐骑速度+self.数据.幻化属性.速度
	self.数据.魔法上限 = self.数据.魔法上限 + self.数据.装备属性.魔法+self.数据.幻化属性.魔法
	self.数据.最大气血 = self.数据.最大气血 + self.数据.装备属性.气血+self.数据.幻化属性.气血
	self.数据.最大气血 = math.floor((self.数据.最大气血 + self.飞升附加) * (1 + 0.01 * self.数据.辅助技能.强身术)) + 3000
	self.数据.气血上限 = self.数据.最大气血
	self.数据.魔法上限 = math.floor(self.数据.魔法上限 * (1 + 0.01 * self.数据.辅助技能.冥想))
    --  if  self.数据.锦衣数据.锦衣 then
    -- 	self.数据.最大气血 = math.floor(self.数据.最大气血 * 1.1)
    -- 	if self.数据.锦衣数据.锦衣 == "夜影"or self.数据.锦衣数据.锦衣 == "夏日清凉" or self.数据.锦衣数据.锦衣 == "萌萌小厨" or self.数据.锦衣数据.锦衣 == "华风汉雅"  then
	   --      self.数据.魔法上限 = self.数据.魔法上限+50
	   --      self.数据.伤害 = self.数据.伤害+50
	   --      self.数据.命中 = self.数据.命中+50
	   --      self.数据.防御 = self.数据.防御+50
	   --      self.数据.灵力 = self.数据.灵力+50
	   --      self.数据.速度 = self.数据.速度+50
	   --      self.数据.速度 = self.数据.速度+50
	   --  end
	   --  if self.数据.锦衣数据.锦衣 == "尖叫服"or self.数据.锦衣数据.锦衣 == "锦绣幻梦" or self.数据.锦衣数据.锦衣 == "萤雪轻裘" or self.数据.锦衣数据.锦衣 == "铃儿叮当" or self.数据.锦衣数据.锦衣 == "顽皮小恶魔"  then
	   --      self.数据.魔法上限 = self.数据.魔法上限+100
	   --      self.数据.伤害 = self.数据.伤害+100
	   --      self.数据.命中 = self.数据.命中+100
	   --      self.数据.防御 = self.数据.防御+100
	   --      self.数据.灵力 = self.数据.灵力+100
	   --      self.数据.速度 = self.数据.速度+100
	   --  end

    -- end
    --     if  self.数据.锦衣数据.光环 then
    -- 	self.数据.伤害 = math.floor(self.数据.伤害 * 1.1)
    -- 	self.数据.灵力 = math.floor(self.数据.灵力 * 1.1)
    -- end
    -- if  self.数据.锦衣数据.足迹 then
    -- 	self.数据.防御 = math.floor(self.数据.防御 * 1.1)
    -- 	self.数据.速度 = math.floor(self.数据.速度 * 1.1)
    -- end
	if self.数据.变身.造型 ~= "" and self.数据.变身.造型 ~= nil and 变身卡数据[self.数据.变身.造型].属性 ~= 0 then
		if 变身卡数据[self.数据.变身.造型].类型 == "气血" then
			self.增加类型 = "最大气血"
		else
			self.增加类型 = 变身卡数据[self.数据.变身.造型].类型
		end

		if 变身卡数据[self.数据.变身.造型].单独 == 1 then
			if 变身卡数据[self.数据.变身.造型].正负 == 1 then
				self.数据[self.增加类型] = self.数据[self.增加类型] + 变身卡数据[self.数据.变身.造型].属性
			else
				self.数据[self.增加类型] = self.数据[self.增加类型] - 变身卡数据[self.数据.变身.造型].属性
			end
		elseif 变身卡数据[self.数据.变身.造型].正负 == 1 then
			self.数据[self.增加类型] = self.数据[self.增加类型] + math.floor(self.数据[self.增加类型] * 变身卡数据[self.数据.变身.造型].属性 / 100)
		else
			self.数据[self.增加类型] = self.数据[self.增加类型] - math.floor(self.数据[self.增加类型] * 变身卡数据[self.数据.变身.造型].属性 / 100)
		end
	end

	if self.数据.气血上限 == nil then
		self.数据.气血上限 = self.数据.最大气血
	end

	if self.数据.当前气血 == nil then
		self.数据.当前气血 = self.数据.气血上限
	end

	if self.数据.当前魔法 == nil then
		self.数据.当前魔法 = self.数据.魔法上限
	end

	-- if self.数据.坐骑 ~= nil then
	-- 	self.数据.最大气血 = math.floor(self.数据.最大气血 * 1.1)
	-- 	self.数据.气血上限 = self.数据.最大气血
	-- 	self.数据.魔法上限 = math.floor(self.数据.魔法上限 * 1.1)
	-- 	if self.数据.坐骑 == "九尾神狐" or self.数据.坐骑 == "粉红小驴"or self.数据.坐骑 == "天使猪猪"or self.数据.坐骑 == "金麟仙子" or self.数据.坐骑 == "战火穷奇"or self.数据.坐骑 == "银色穷奇"or self.数据.坐骑 == "暗影战豹" or self.数据.坐骑 == "九霄冰凤"or self.数据.坐骑 == "幽骨战龙"or self.数据.坐骑 == "玉瓷葫芦"then
	--         self.数据.伤害 = math.floor(self.数据.伤害 * 1.1)
	--         self.数据.命中 = math.floor(self.数据.命中 * 1.1)
	--         self.数据.防御 = math.floor(self.数据.防御 * 1.1)
	--         self.数据.灵力 = math.floor(self.数据.灵力 * 1.1)
	--         self.数据.速度 = math.floor(self.数据.速度 * 1.1)
	--     end
	-- end

	if self.数据.最大气血 < self.数据.气血上限 then
		self.数据.气血上限 = self.数据.最大气血
	end

	if self.数据.气血上限 < self.数据.当前气血 then
		self.数据.当前气血 = self.数据.气血上限
	end

	if self.数据.魔法上限 < self.数据.当前魔法 then
		self.数据.当前魔法 = self.数据.魔法上限
	end

	if 恢复 == nil then
		self.数据.气血上限 = self.数据.最大气血
		self.数据.当前气血 = self.数据.最大气血
		self.数据.当前魔法 = self.数据.魔法上限
	end

	if 玩家数据[self.玩家id] ~= nil then
		发送数据(玩家数据[self.玩家id].连接id, 1003, self:获取角色气血数据())
	end




end

function 角色处理类:存档(id)
	self.数据.离线ip = self.玩家ip

	if 玩家数据[id].账号 == nil or 目录是否存在(data目录 .. 玩家数据[id].账号) == false then
		return 0
	end

	local temp = {}
	for k, v in pairs(self.数据) do
		if k == '蚩尤元神' then
		elseif k == '装备属性' then
		elseif k == '幻化属性' then
		elseif k == '装备三围' then
		else 
			temp[k] = v 
		end
	end
	写出文件(data目录 .. 玩家数据[id].账号 .. _角色txt, table.tostring(temp))

	if 玩家数据[id].召唤兽 == nil or 玩家数据[id].道具 == nil then
		return 0
	end

	if 玩家数据[id].召唤兽.存档 == nil or 玩家数据[id].道具.存档 == nil then
		return 0
	end

	玩家数据[id].召唤兽:存档(id)
	玩家数据[id].道具:存档(id)
	写出文件(data目录 .. 玩家数据[id].账号 .. _日志目录 .. self.数据.日志编号 .. ".txt", self.消费日志)

	if string.len(self.消费日志) >= 10240 then
		self.数据.日志编号 = self.数据.日志编号 + 1 .. ""
		self.消费日志 = "日志创建"

		写出文件(data目录 .. 玩家数据[id].账号 .. _日志目录 .. self.数据.日志编号 .. ".txt", "日志创建#换行符")
		写出文件(data目录 .. 玩家数据[id].账号 .. "/日志编号.txt", self.数据.日志编号)
	end
end
return 角色处理类
