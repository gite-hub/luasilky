
__gge.safecall = function (func,...)
    local args = { ... };
    local ret = {xpcall(function() return func(unpack(args)) end, __gge.traceback)}

    if ret[1] and ret[2] then
        return unpack(ret, 2)
    end
    return false
end

require("gge函数")
do
	-- local file = io.open('media.cao/client')
	-- if file ~= nil then
	-- 	local v = io.read("*a")
	-- 	ffi函数.信息框(v, "资料片DLC")
	-- 	io.close(file)
	-- 	file = io.open(tostring(v) .. 'dlc.exe')
	-- 	if file ~= nil then
	-- 		local scene = 'scene.'
	-- 		资料片 = v
	-- 		资料片wd = v .. scene .. "wd/"
	-- 		资料片ui = v .. scene .. "ui/"
	-- 		资料片ori = v .. scene .. "ori/"
	-- 		资料片jd = v .. scene .. "jd/"
	-- 		资料片it = v .. scene .. "it/"
	-- 		io.close(file)
	-- 	end
	-- end

	local name = "资料片DLC/"
	local exe = "dlc.exe"
	local up = "../"
	local scene = "scene."
	local paths =
	{
		[1] = name,
		[2] = name .. name,
		[3] = up .. name,
		[4] = up .. name .. name,
		[5] = up .. up .. name,
		[6] = up .. up .. name .. name,
		[7] = up .. up .. up .. name,
		[8] = up .. up .. up .. name .. name,
		[9] = up .. up .. up .. up .. name,
		[10] = up .. up .. up .. up .. name .. name,
	}
	for k,v in pairs(paths) do
		local file = io.open(v .. exe)
		if file ~= nil then
			资料片 = v
			资料片wd = v .. scene .. "wd/"
			资料片ui = v .. scene .. "ui/"
			资料片ori = v .. scene .. "ori/"
			资料片jd = v .. scene .. "jd/"
			资料片it = v .. scene .. "it/"
			io.close(file)
		end
	end

end

ffi = require("ffi")
ffi函数 = require("ffi函数")

if not 资料片 then
	ffi函数.信息框("未找到资料片DLC", "资料片DLC")
	os.exit()
end


全局变量 = {
	游戏目录 = "wdf/",
	地图开关 = true
}

require("Script/通用函数")
require("Script/对象/锦衣模型")
require("Script/对象/锦衣身体模型")
动画类锦衣=require("script/资源类/动画类锦衣")
require("Script/坐骑ini/坐骑拦截")
游戏公告=[[

#1 #y2023.05.05 #1
 1.去掉锦衣，较小体积，爱玩不玩
 2.欲言又止...

#1 #y2023.04.28 #1
 1.依赖改为 资料片DLC
	
#1 #y2020.08.20 #1
 1.性能测试,爱玩不玩
 2.各种闪退,电容击穿,被攻击,被拉闸,概不负责

#1 #y2020.05.01 #1
 1.优化底层和内存
 2.不定期拉闸
 3.爱玩不玩

#1 #y2020.04.01 #1
 1.调整 DT PT LG 经脉
 2.调整生命之泉问题
 3.修正法宝碎片出售价格
 4.开放等级上限

#1 #y2020.03.27 #1
 1.调整地煞星
 2.修复无底洞经脉化莲

#1 #y2020.03.26 #1
 1.减少帮派仓库和厢房的升级进度需求
 2.修复龙宫被龙吟炸干的问题
 3.尝试修复脚本刷鬼的问题(嗯,尝试...)

#1 #y2020.03.25 #1
 1.加强保驾护航,深入贯彻海毛虫的扶贫方针
 2.修复乾元丹兑换丢失问题
 3.师门抓宠直接跳过
 4.修复打造资金显示不一致的问题

#1 #y2020.03.23 #1
 1.调整部分活动的刷新频率
 2.修复西凉女果/宝象国/朱紫国背景音乐
 3.去掉中场召唤宝宝的参战等级限制

#1 #y2020.03.18 #1
 1.调整新手满技满修
 2.优化游戏容错弹框提示
 3.调整游戏物价及NPC服务费用
 4.调整物品回收价格,大部分可原价回收
 5.去掉飞升的技能等级要求
 6.优化敌人封系太多的问题
 7.减少死亡损失
 8.修复宝宝中途进场无法隐身的问题
 9.去掉法宝携带限制
10.修复亲儿子魔王BUG

#1 #y2020.03.17 #1
 1.优化队伍界面体验,按鼠标区域选取队员
 2.修复建邺城副本积分兑换卡屏或者闪退问题
 3.调整内丹, 6格全开
 4.调整伤害算法(测试阶段)
 5.去掉部分活动的等级限制和队伍人物限制
 6.调整坐骑携带上限
 7.调整长安城的传送NPC到宝象国
 8.调整飞机师的价格
 9.调整法术暴击动画
10.调整飞砂走石动画
11.调整人物/宝宝界面,加点按住CTRL键可以连点
12.修复入帮申请名单的包围盒问题
13.超级巫医增加重置召唤兽等级的选项
14.减少化生寺的推气过宫耗蓝过大的问题
15.野外随便抓不分携带等级
16.减少强化打造的强化石消耗
17.调整宝宝初始属性点10,多出50可分配点数
18.优化服务端,利用队列结构,减少发送高并发

#1 #y2020.03.16 #1
 0.加入全家桶, 依赖浪西游资料片, 减少游戏体积
 1.优化游戏公告 支持鼠标滚轮浏览
 2.优化登录 可以按 TAB 或者 ENTER 回车键 快捷登录
 3.优化部分UI体验,尤其是添加右键关闭
 4.修复某些界面按钮点击 误触地图寻路问题
 5.修复头顶对话边框大小显示(结果还是没解决)
 6.优化对话框选项(太难了,只能调一下字体大小)
 7.优化关闭回调, 添加提示, 再按一次退出游戏
 8.修复战斗中 请等待 一直显示的问题
 9.修复战斗中无法打开表情包的问题
10.优化摆摊的价格输入和显示的体验
以上
]]

print("记得改ip, 改日期, 改公告, 删掉id截图, 删掉配置里的账号, 去掉多余的print")
simsun = "C:/Windows/Fonts/simsun.ttc"
引擎("新丝滑", 800, 600, 60, true)
require("script/对象/战斗模型库")
require("script/对象/普通模型库")
require("Script/对象/技能库")
require("Script/对象/头像库")
require("Script/对象/物品库")
require("Script/对象/音效库")
require("Script/对象/特效库")
-- WDF类 = require("Glow/WDF类")
-- WAS类 = require("Glow/WAS类")
图像类 = require("gge图像类")
-- 动画类 = require("Script/核心/动画类")
WAS动画类 = require("Script/核心/WAS动画类")
按钮类 = require("Script/核心/按钮类")
包围盒 = require("Script/核心/包围盒类")
-- FMOD类2 = require("FMOD类")
-- FMOD类3 = FMOD类2
-- ffi = require("ffi")
-- f函数 = require("ffi函数")
-- ffi函数 = require("ffi函数")
gge文字 = require("gge文字类")
文字类 = gge文字
输入框 = require("ggeui/输入类")


引擎.场景 = require("script/场景")()

窗口标题 = {名称 = "",服务器 = "",时间 = 0,id = 0}
require("Script/地图/遮罩类")

传送圈类 = require("Script/地图/传送圈类").创建()
-- 路径类 = require("Script/地图/路径类")
信息提示 = require("Script/核心/游戏提示类").创建()
连接参数 = 1
认证号 = 9235
同步时间 = os.time()
玩家屏蔽 = false
玩家寻路表 = {}
玩家寻路开关 = false
名称文字 = 文字类.创建(simsun,16,false,false,true)
字体 = require("gge文字类")(simsun, 14, true, true)
文字 = require("gge文字类")():置颜色(4278518274.0)
文字:置颜色(白色)
文字1 = require("gge文字类")():置颜色(4278518274.0)
文字1:置颜色(白色)
道具名称 = 文字类.创建(simsun, 25):置颜色(黄色)
推广文字 = 文字类.创建(simsun, 20):置颜色(绿色)
道具文本 = require("丰富文本类")(190, 270)
数量字体 = {
	文字类.创建(simsun, 15):置颜色(黑色),
	文字类.创建(simsun, 15)
}
角色移动 = 0
物品数据类 = require("Script/物品数据类")

账号调试 = false
组队开关 = false
jt开关 = false
调试战斗 = false
战斗模式 = false
玩家给予开关 = false
系统给予开关 = false
交易开关 = false
pk开关 = false
消息开关 = false
游戏时间 = 0
时间差 = 0
鼠标 = {
	x = 0,
	y = 0
}
系统处理类 = {
	数字id = 0
}
重连状态 = false
连接状态 = false
连接状态 = true
自动回合 = 100
禁止重连 = false
使用重连 = false
自动战斗 = false
xtjc = {
	time = os.time()
}

--锦衣DLL = loadfile("galaxy2d.dll")()


-- local img = ffi.C.LoadImageA(0,'xyq.ico',1,32,32,16) -- IMAGE_BITMAP,LR_LOADFROMFILE
-- ffi.C.SendMessageA(引擎.取窗口句柄(),128,0,img) -- WM_SETICON

关闭延迟 = 0
function 客户端连接断开()
	连接状态 = false
	使用重连 = true
end

local delta = 0
function 更新函数(dt, x, y)
	delta = delta + dt
	if delta > 1 then
		delta = delta - 1
		更改窗口标题(os.time() + 时间差)
	end
	if os.time() - 数据包时间 >= 1 then
		数据包流 = 0
		数据包流1 = 0
		数据包时间 = os.time()
	end

	if 使用重连 and 禁止重连 == false and 停止连接 == false then
		停止连接 = true

		客户端:重新连接()
	end
	随机序列 = 随机序列 + 1
	if 随机序列 >= 1000 then
		随机序列 = 0
	end
	全局dt = dt
	鼠标.y = y
	鼠标.x = x
	外部聊天框渲染函数(dt,x,y)

	关闭延迟 = 关闭延迟 + dt
end

local tick = 0
function 外部聊天框渲染函数(dt,x,y)
	tick = tick + 1
	if tick % 30 == 29 then
		tick = 0
		引擎.child:setTitle('v20230505')
	    if 引擎.child:renderBegin() then
			引擎.场景.窗口.聊天框类.丰富文本外框:显示(0, 2)
			引擎.child:renderEnd()
		end
	end
 -- 	local wbgb
	-- local wbgb2
	-- if 引擎.外部 ~= nil then
	-- 	if 引擎.外部.渲染开始() then
	-- 		引擎.场景.外部聊天框:显示(0,0)
	-- 		local xxx,xxy = 17,0
	-- 		local kg = false
	-- 		local xx,yy = 引擎.外部.取鼠标坐标()
	-- 		local xxk =引擎.场景.窗口.聊天框类
	-- 		xxk.按钮_左拉:更新(xx,yy)
	-- 		xxk.按钮_上拉:更新(xx,yy)
	-- 		xxk.按钮_下拉:更新(xx,yy)
	-- 		xxk.按钮_移动:更新(xx,yy)
	-- 		xxk.按钮_查询:更新(xx,yy)
	-- 		xxk.按钮_禁止:更新(xx,yy)
	-- 		xxk.按钮_锁定:更新(xx,yy)
	-- 		xxk.按钮_清屏:更新(xx,yy)
	-- 		if xxk.按钮_左拉:是否选中(xx,yy) and 引擎.外部.鼠标按住(0) then
	-- 			wbgb2 = true
	-- 		elseif xxk.按钮_下拉:事件判断() then
	-- 			if xxk.丰富文本外框.滚动值 > 0 then
	-- 				xxk.丰富文本外框:滚动(-1)
	-- 			end
	-- 		elseif xxk.按钮_上拉:事件判断() then
	-- 			if xxk.丰富文本外框.滚动值 < #xxk.丰富文本外框.显示表 - 24 then
	-- 				xxk.丰富文本外框:滚动(1)
	-- 			end
	-- 		elseif xxk.按钮_禁止:事件判断() then
	-- 			xxk.禁止 = xxk.禁止 == false
	-- 		elseif xxk.按钮_清屏:事件判断() then
	-- 			xxk:清空内容()
	-- 		end
	-- 		xxk.丰富文本外框:显示(xxx -17,xxy + 2)
	-- 		xxk.按钮_左拉:显示(xxx + 13,xxy +引擎.高度-30,true)
	-- 		xxk.按钮_上拉:显示(xxx + 36,xxy + 引擎.高度-30,true)
	-- 		xxk.按钮_下拉:显示(xxx + 59,xxy + 引擎.高度-30,true)
	-- 		xxk.按钮_移动:显示(xxx + 83,xxy + 引擎.高度-30,true)
	-- 		xxk.按钮_查询:显示(xxx + 106,xxy +引擎.高度-30,true)
	-- 		xxk.按钮_禁止:显示(xxx + 129,xxy + 引擎.高度-30,true)
	-- 		xxk.按钮_锁定:显示(xxx + 153,xxy +引擎.高度-30,true)
	-- 		xxk.按钮_清屏:显示(xxx + 175,xxy + 引擎.高度-30,true)
	--         引擎.外部.渲染结束()
	--         if 引擎.是否在窗口内() then
	-- 	        引擎.外部.置鼠标按住(false)
	--         end
	--     end
 --    end

	-- if wbgb then
	-- 	临时高度=1
	-- end
	-- if wbgb2 then
	-- 	外部聊天框窗口关闭(wbgb2)
	-- end
end

function 外部聊天框窗口关闭(wbgb)
	if wbgb then --引擎.外部 ~= nil and
		local xxk = 引擎.场景.窗口.聊天框类
		xxk.外部聊天 = xxk.外部聊天 == false
		引擎.child:close()
		-- 引擎.外部.关闭()
		-- 引擎.外部 = nil
		-- xxk.丰富文本:置宽度(450)
		xxk.丰富文本:置高度(xxk:取高度())
		if xxk.丰富文本.滚动值 > 0 then
			xxk.丰富文本:滚动(-xxk.丰富文本.滚动值)
		end
		if xxk.丰富文本.滚动值 < #xxk.丰富文本.显示表 - 24 then
			xxk.丰富文本:滚动(xxk.丰富文本.滚动值)
		end
		xxk.按钮_左拉.外部按钮 = nil
		xxk.按钮_上拉.外部按钮 = nil
		xxk.按钮_下拉.外部按钮 = nil
		xxk.按钮_移动.外部按钮 = nil
		xxk.按钮_查询.外部按钮 = nil
		xxk.按钮_禁止.外部按钮 = nil
		xxk.按钮_锁定.外部按钮 = nil
		xxk.按钮_清屏.外部按钮 = nil
		xxk.按钮_移动.确定按下 = false
		引擎.在外部 = nil
		collectgarbage("collect")
	end
end
function 渲染函数(dt,x, y)

	引擎.渲染开始()
	引擎.渲染清除(4278190080.0)
    引擎.场景:显示(dt,x,y)
	引擎.渲染结束()

end

local function 退出函数()
	if tp.进程 == 6 and 信息提示 ~= nil then
		if 关闭延迟 > 2 then
	   		信息提示:加入提示("再次点击退出")
	   		关闭延迟 = 0
	   		外部聊天框窗口关闭(true)
	   		return false
	   	end
	end
	客户端:断开连接()
	return true
end
引擎.置退出函数(退出函数)
function 回调函数(q, w)
	if q == "0" then
		tp.主界面.界面数据[8].本类开关 = false
	else
		tp.主界面.界面数据[8].本类开关 = false

		客户端:发送数据(0, 2, 6, q .. "*-*" .. w, 1)
	end
end

-- function 回调函数1(q, w)
-- 	客户端:发送数据(q, 2, 46, q .. "*-*" .. w, 1)
-- end

function 取硬盘特征字()
	return "745445356535563564"
end



-- local str = "_._123456"
-- str = string.sub(str, 4, #str)
-- print(str)


-- local xiaofan = __gge.readfile('../服务端源码/data/wxiaohao4/道具.txt')
-- xiaofan = table.loadstring(xiaofan)

-- local xf = {}
-- local index = 1
-- for k = 1, 9999 do
--   if(xiaofan[k])then
--     local name = xiaofan[k].名称;
--     if(name ~= "青龙石" and name ~= "白虎石" and name ~= "朱雀石" and name ~= "玄武石" and name ~= "飞行符") then  --and name ~= "吸附石"
--       if(true or index >= 68)then
--         xf[index] = xiaofan[k]
--       end
--       index = index + 1
--     end
--   end
-- end


-- xf[index] = {数量=999,名称="吸附石",类型="炼妖"}
-- xf[index + 1] = {数量=9999990,名称="青龙石",价格=90000,类型="普通"}
-- xf[index + 2] = {数量=9999990,名称="白虎石",价格=90000,类型="普通"}
-- xf[index + 3] = {数量=9999990,名称="朱雀石",价格=90000,类型="普通"}
-- xf[index + 4] = {数量=9999990,名称="玄武石",价格=90000,类型="普通"}


-- print(table.tostring(xf))

-- local cks = {}
-- local idx = 1
-- index = 1
-- for k = 1, 999 do
--   if(xf[k])then
--       if (index == 1 and not cks[idx]) then
--         cks[idx] = {}
--       end
--       cks[idx][index] = k;
--       index = index + 1
--       if index == 21 then
--         idx = idx + 1
--         index = 1
--       end
--   end
-- end

-- for k = 1, idx do
--   	print(table.tostring(cks[k]));
-- end

-- print(table.tostring(cks));

-- loadstring('local myc = \'local myc = class() function myc:初始化(a) self.a = a end function myc:print() print(self.a) end return myc\' myc = loadstring(myc)();myc(666):print()')()
