local 战斗单位类 = class()

function 战斗单位类:初始化(数据, 敌我, 位置, 编号)
	self.血条文字 = 文字类(simsun, 12)
	self.特技文字 = 文字类(simsun, 16)
	self.特技文字:置颜色(4287045496.0)
	self.血条 = 图像类("imge/001/28-1.png")
	self.怒条 = 图像类("imge/001/556-2.png")
	self.蓝条 = 图像类("imge/001/554-2.png")
	self.空条 = 图像类("imge/001/27-1.png")
	self.隐身 = false
	self.特效动作 = {}
	self.移动上限 = 90
	self.单位消失 = false
	self.移动频率 = 35
	self.分身 = false
	self.动作复位 = false
	self.音效动作 = {"防御","施法","攻击","倒地","被击中"}
	self.伤害图片 = {}
	for n = 1, 11, 1 do
		self.伤害图片[n] = 图像类("Dat/num/" .. tostring(n - 1) .. ".png")
	end
	self.回复图片 = {}
	for n = 1, 11, 1 do
		self.回复图片[n] = 图像类("Dat/num/22-" .. tostring(n) .. ".png")
	end
	self.数据 = 数据
	self.高亮显示 = false
	self.移动开关 = false
	self.躲避开关 = false
	self.返回开关 = false
	self.击飞开关 = false
	self.倒地开关 = false
	self.击飞开关 = false
	self.掉血开关 = false
	self.特技内容开关 = false
	self.法术状态组 = {}
	self.死亡开关=false
	self.动作 = "待战"
	self:创建动画(数据)
	self.方向 = 0
	self.坐标抖动 = {
		y = 0,
		进程 = 1,
		x = 0,
		开关 = false
	}
	self.停止更新 = false
	self.敌我 = 敌我
	self.移动坐标 = {}
	if self.数据.阵法 ~= "普通" and self.数据.阵法 ~= nil then
		位置 = table.loadstring(阵法位置[self.数据.阵法])
	end

	if 敌我 == 1 then
		self.初始方向 = 2
		self.初始xy = {
			x = 位置[self.数据.位置].x,
			y = 位置[self.数据.位置].y
		}
		self.显示xy = {
			x = 位置[self.数据.位置].x,
			y = 位置[self.数据.位置].y
		}
		self.躲避参数 = 8
		self.躲避坐标 = 2.5
		self.反震误差 = {
			x = 50,
			y = 10
		}
	else
		if self.数据.阵法 ~= "普通" and self.数据.阵法 ~= nil then
			位置[self.数据.位置] = {
				x = 阵法位置1[self.数据.阵法][self.数据.位置].x,
				y = 阵法位置1[self.数据.阵法][self.数据.位置].y
			}
		end
		self.初始xy = {
			x = 位置[self.数据.位置].x,
			y = 位置[self.数据.位置].y
		}
		self.显示xy = {
			x = 位置[self.数据.位置].x,
			y = 位置[self.数据.位置].y
		}
		self.反震误差 = {
			x = -50,
			y = -10
		}
		self.初始方向 = 0
		self.躲避坐标 = -2.5
		self.躲避参数 = -8
	end

	self.方向 = self.初始方向
	self.动画:换方向(self.动作, self.初始方向)
	self:创建音效()
	self.技能动画 = {
		开关 = false
	}
	self.文字 = 文字类(simsun, 14)
	self.超丰富文本 = require("丰富文本类")(100, 100):置文字(self.文字)
	self.超丰富文本:添加元素("w", 4294967295.0)
	self.超丰富文本:添加元素("h", 4278190080.0)
	self.超丰富文本:添加元素("y", 4294967040.0)
	self.超丰富文本:添加元素("r", 红色)
	self.超丰富文本:添加元素("g", 4278255360.0)

	local bqs =
{1295847816,1853525647,1076954610,3552721044,3990239921,3366615112,2782337201,3771134163,1462708813,
	2715893287,3002376600,3991654351,993860032,2666294756,454787878,1382010105,2912848813,3919700593,477926852,352380776,
	811687356,915719759,4249060934,1336751228,1950021903,2235513801,3594739784,1960900090,2382390242,1537855326,3907030953,
	2290431679,3202252097,4033571742,1492820992,655187057,2901001332,3190903022,298183232,1945354141,3436623848,1724964988,
	2592865169,3393696884,1494002331,1310769894,1152800681,2542768010,1378600591,2829693277,3846038890,3901444948,68667698,
	3331959614,432707334,4241851683,3448703702,2542506275,241536076,314448958,2242677963,1887092560,1489631920,2860202818,
	4076591726,4232189420,3498505860,4241140149,3858705890,2271353759,3452523393,963399171,2959831232,2917226350,4215743335,
	3987108486,365569753,3701218951,3044567112,3265766525,2129343522,2401287726,2716317030,292723042,4014574629,4183102172,
	4115700508,2139528734,276624883,4099788650,2578443618,3367759523,576638850,3586214754,2830927389,1915332364,1341579386,
	3743372972,1217313750,1208397371,156713767,2057251015,3084935361,1485268859,470535714,2868631088,3835251562,2896374671,
	3982186902,1708428735,2448085336,1354708809,943667221,1146784672,3592760724,611393967,1688780051,2042812914,1466206851,
	3612578974}
	self.临时表情 = {}

	for i = 0, 119, 1 do
		self.临时表情[i] = 引擎.场景.资源:载入('ZY.FT',"网易WDF动画",bqs[i+1])
		self.超丰富文本:添加元素(i, self.临时表情[i])
	end

	self.发言内容 = {}
end

function 战斗单位类:创建音效()
	self.音效动作 = {
		"防御",
		"施法",
		"攻击",
		"倒地",
		"被击中"
	}
	if self.数据.战斗类型 == "角色" then
		if 武器 == "" then
			武器 = 全局变量.角色武器类型[造型].主武器
		end

		if self.数据.武器数据.名称 == "" then
			武器 = 全局变量.角色武器类型[self.数据.造型].主武器
		else
			武器 = self.数据.武器数据.类别
		end

		self.名称后缀 = "_" .. 武器
	else
		self.名称后缀 = ""
	end
	self.音效数据 = {}
	for n = 1, #self.音效动作, 1 do
				local yx=引擎.取音效(self.数据.造型..self.名称后缀)
		    self.音效数据[self.音效动作[n]] = yx[self.音效动作[n]] --创建音效(yx[self.音效动作[n]], false)
	end
end

function 战斗单位类:添加发言内容(内容)


	self.发言内容[#self.发言内容 + 1] = {
		起始 = 0,
		开关 = false,
		长度 = string.len(内容)
	}
	self.发言内容[#self.发言内容].开关 = true
	self.发言内容[#self.发言内容].起始 = os.time()

	self.超丰富文本:清空()
	self.超丰富文本:添加文本(内容)
end

function 战斗单位类:创建动画()
	self.闪光模式 = {
		计次 = 0,
		上次 = 0,
		开关 = false,
		间隔 = 30,
	}

   self.动画 = require("script/战斗类/战斗动画类")()
   self.动画:创建动画(self.数据.造型, self.数据.战斗类型,self.数据.染色, self.数据.武器数据,self.数据.变身,self.数据.变异,self.数据.饰品,self.数据.锦衣数据)
   if self.数据.锦衣数据~=nil and self.数据.锦衣数据.锦衣 ~= nil and (self.数据.锦衣数据.锦衣 == "华风汉雅" or self.数据.锦衣数据.锦衣 == "夏日清凉" or self.数据.锦衣数据.锦衣 == "萌萌小厨" or self.数据.锦衣数据.锦衣 == "夜影") then
   		self.动画.锦衣 = true
   end
end

function 战斗单位类:更新(dt)
	if self.单位消失 then
		return 0
	end

	if self.停止更新 == false then
		if self.动作复位 and self.动画:取当前帧("攻击") == self.动画:取结束帧("攻击") then
			self.动作复位 = false
		end

		self.动画:更新(dt, self.动作)
	end

	if self.移动开关 then
		self:移动事件()
	end

	if self.返回开关 then
		self:返回事件()
	end

	if self.击退开关 then
		self:击退处理()
	end

	if self.击飞开关 then
		self:击飞处理()
	end

	if self.倒地开关 then
		self:倒地处理()
	end

	if self.坐标抖动.开关 then
		if self.坐标抖动.进程 == 1 then
			self.坐标抖动.y = self.坐标抖动.y + 0.55

			if self.坐标抖动.y >= 1.3 then
				self.坐标抖动.进程 = 2
			end
		elseif self.坐标抖动.进程 == 2 then
			self.坐标抖动.y = self.坐标抖动.y - 0.55

			if self.坐标抖动.y <= -1.3 then
				self.坐标抖动.进程 = 1
			end
		end

		self.显示xy.y = self.显示xy.y + self.坐标抖动.x
		self.显示xy.x = self.显示xy.x + self.坐标抖动.y
	end

	if self.技能动画.开关 then
		self.技能动画.动画:更新(dt)
	end

	for n, v in pairs(self.法术状态组) do
		self.法术状态组[n]:更新(dt)
	end

	if self.击退延迟 and self.动画:取当前帧(self.动作) == self.动画:取结束帧(self.动作) then
		self.击退延迟 = false
		self.击退开关 = true
		self.动画.按帧更新 = true
		if self.动画.锦衣~=nil and self.动作 ~= "倒地" then
	        self.动画.按帧更新 = false
	    end
	end

	if self.闪光模式.开关 then
		self.闪光模式.计次 = self.闪光模式.计次 + 1

		if self.闪光模式.间隔 <= self.闪光模式.计次 then
			self.闪光模式.计次 = 0

			if self.闪光模式.上次 == 0 then
				self.闪光模式.上次 = 1
              self.动画[self.动作]:置高亮模式(-10855936)
				--self.动画:置颜色(self.动作, self.闪光模式.闪光)
			else
				self.闪光模式.上次 = 0

				--self.动画:置颜色(self.动作, self.闪光模式.复原)
				self.动画[self.动作]:取消高亮()
			end
		end
	end
end

function 战斗单位类:播放音效(类型)
	if self.音效数据[类型] ~= nil then
		-- print("播放音效")
		-- self.音效数据[类型]:置音量(1000)
		-- self.音效数据[类型]:播放()
		创建音效(self.音效数据[类型])
	end
end

function 战斗单位类:移动事件()
	if self.移动上限 <= 取两点距离(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动坐标.x, self.移动坐标.y)) then
		self.移动距离 = 取移动坐标(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动坐标.x, self.移动坐标.y), self.移动频率)
		self.角度 = 取两点角度(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动距离.x, self.移动距离.y))
		self.方向 = 角度算四方向(self.角度)
		self.动画:换方向(self.动作, 角度算四方向(self.角度))
		self.显示xy.y = self.移动距离.y
		self.显示xy.x = self.移动距离.x
	else
		self.移动开关 = false
	end
end

function 战斗单位类:返回事件()
	if 取两点距离(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动坐标.x, self.移动坐标.y)) >= 20 then
		self.移动距离 = 取移动坐标(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动坐标.x, self.移动坐标.y), 20)
		self.角度 = 取两点角度(生成XY(self.显示xy.x, self.显示xy.y), 生成XY(self.移动距离.x, self.移动距离.y))
		self.动画:换方向(self.动作, 角度算四方向(self.角度))
		self.显示xy.y = self.移动距离.y
		self.显示xy.x = self.移动距离.x
	else
		self.返回开关 = false
		self.动作 = "待战"
		self.动画:换方向(self.动作, self.初始方向)
		self.显示xy.y = self.初始xy.y
		self.显示xy.x = self.初始xy.x
	end
end

function 战斗单位类:操作抖动(逻辑)
	if 逻辑 then
		self.坐标抖动.进程 = 1
	end
end
function 战斗单位类:取前置状态(动作)
 if 动作=="瘴气" or 动作=="火甲术" or 动作=="乾坤妙法" or 动作=="天地同寿" or 动作=="灵动九天" or 动作=="神龙摆尾" or 动作=="舍身取义" or 动作=="佛法无边"  or 动作=="魔息术" or 动作=="安神诀" or 动作=="炼气化神" or 动作=="复苏" or 动作=="盘丝阵" or 动作=="极度疯狂" or 动作=="魔王回首" or 动作=="幽冥鬼眼" or 动作=="天神护体" or 动作=="宁心" or 动作=="一苇渡江" or 动作=="百毒不侵" or 动作=="紧箍咒" or 动作=="毒" or 动作=="光辉之甲" or 动作=="圣灵之甲" or  动作=="鹰击" or 动作=="日月乾坤" or 动作=="楚楚可怜" or 动作=="尸腐毒" or 动作=="变身" or 动作=="金刚护体" or 动作=="定心术" or 动作=="金刚护法"    then
    return false
  else
    return true
  end
 end

function 战斗单位类:解除异常状态()
	self.异常状态名称 = {
		"含情脉脉",
		"尸腐毒",
		"如花解语",
		"似玉生香",
		"莲步轻舞",
		"定身符",
		"失魂符",
		"失忆符",
		"百万神兵",
		"镇妖",
		"日月乾坤",
		"紧箍咒",
		"尸腐毒"
	}

	for n = 1, #self.异常状态名称, 1 do
		if self.法术状态组[self.异常状态名称[n]] ~= nil then
			self.法术状态组[self.异常状态名称[n]] = nil
		end
	end
end
function 战斗单位类:添加状态动画(动作)
 local txk=引擎.特效库("状态_"..动作)
  if #txk ==0 then
     return
 end
	self.法术状态组[动作] = 引擎.场景.资源:载入(txk[2],"网易WDF动画",txk[1])

	if 动作 == "百毒不侵" then
		self.法术状态组.毒 = nil
		self.法术状态组.尸腐毒 = nil
	elseif 动作 == "宁心" or 动作 == "解封" then
		self.解除名称 = {
			"如花解语",
			"似玉生香",
			"莲步轻舞"
		}

		for n = 1, #self.解除名称, 1 do
			self.法术状态组[self.解除名称[n]] = nil
		end
	elseif 动作 == "驱魔" then
		self.解除名称 = {
			"离魂符",
			"失心符",
			"追魂符",
			"催眠符",
			"定身符",
			"失魂符",
			"失忆符"
		}

		for n = 1, #self.解除名称, 1 do
			self.法术状态组[self.解除名称[n]] = nil
		end
	elseif 动作 == "复苏" then
		self.解除名称 = {
			"百万神兵",
			"镇妖"
		}

		for n = 1, #self.解除名称, 1 do
			self.法术状态组[self.解除名称[n]] = nil
		end
	end
end

function 战斗单位类:增加技能动画(动作)

 local txk=引擎.特效库(动作)
 if #txk ==0 then
     return
 end
	self.技能动画.动画 = 引擎.场景.资源:载入(txk[2],"网易WDF动画",txk[1])

  if 动作 == "唧唧歪歪" then
    txz = 1.15
  elseif 动作 == "横扫千军"   then
    txz = 1.05
  elseif 动作 == "反震" or 动作 == "防御" then
    txz = 1.6
  elseif 动作 == "捕捉开始"or 动作 == "暴击"  then
    txz = 1.55
  elseif   动作 == "地裂火"  then
    txz = 1.2
  elseif 动作 == "裂石"  then
    txz = 1.2
  elseif 动作 == "浪涌"   then
    txz = 1.4
  elseif 动作 == "惊涛怒"  then
    txz = 5
  elseif 动作 == "泰山压顶" or 动作 == "满天花雨"  then
    txz = 1.4
  elseif  动作 == "烟雨剑法" or  动作 == "飘渺式" or 动作 == "金刚护体"
   or 动作 == "日月乾坤" then
    txz = 2.0
  elseif 动作 == "推气过宫" or 动作 == "活血" then
    txz = 0.7
  elseif 动作 == "天雷斩" or 动作 == "地狱烈火" then
    txz = 1.1
  elseif 动作 == "水漫金山" or 动作 == "鹰击" or 动作 == "上古灵符" then
    txz = 1.48
  elseif 动作 == "五雷轰顶" then
    txz = 1
  elseif 动作 == "天罗地网" then
    txz = 0.85
  elseif 动作 == "被击中"or 动作 == "飞砂走石" or 动作 == "二龙戏珠"  then
    txz = 1
  elseif 动作 == "翻江搅海" or 动作 == "五雷咒" or 动作 == "落雷符" then
    txz = 3
  elseif   动作 == "血雨" then
    txz = 3
    self.特效偏移 = {30,65}
  elseif 动作 == "后发制人" or 动作 =="冰川怒" or 动作 == "月光"  then
    txz = 1.1
  elseif 动作 == "泰山压顶1" then
    txz = 1.25
  elseif 动作 == "泰山压顶2" then
    txz = 1.4
  elseif 动作 == "泰山压顶3" then
    txz = 1.1
  elseif  动作 == "逆鳞" or 动作 == "清心" or 动作 == "牛劲" or 动作 == "魔王回首" or 动作 == "变身" or
    动作 == "定心术" or 动作 =="归元咒" or 动作 == "极度疯狂" or 动作 == "杀气诀" or 动作 == "韦陀护法" or 动作 == "天神护法"  then
    txz = 1.1
  elseif  动作 == "地涌金莲" or 动作 == "夺命咒" then
    txz = 1.1
    elseif  动作 == "荆棘舞" or 动作 == "尘土刃"  then
    	 txz = 1.6
  elseif 动作 == "归元咒" or 动作 == "乾天罡气" or 动作 == "巨岩破" or 动作 == "推拿" or 动作 == "活血"  then
    txz = 0.95
  elseif 动作 == "惊心一剑" or 动作 == "牛刀小试" then
    txz = 1.05
  else
    txz = 1.15
  end
  self.技能动画.动画:置提速(txz+0.5)
	self.技能动画.开关 = true
	self.技能动画.名称 = 动作
	self.技能文本 = 动作

	if 动作 == "解毒" or 动作 == "清心" or 动作 == "驱尸" then
		self.法术状态组.毒 = nil
		self.法术状态组.尸腐毒 = nil
	elseif 动作 == "驱魔" then
		self.解除名称 = {
			"离魂符",
			"失心符",
			"追魂符",
			"催眠符",
			"定身符",
			"失魂符",
			"失忆符"
		}

		for n = 1, #self.解除名称, 1 do
			self.法术状态组[self.解除名称[n]] = nil
		end
	end
end

function 战斗单位类:添加特效动作(动作)
	self.特效动作[#self.特效动作 + 1] = {}
	self.特效动作[#self.特效动作].动作 = 动作

 local txk=引擎.特效库(动作)
    if #txk ==0 then
     return
 end
	self.特效动作[#self.特效动作].动画 =引擎.场景.资源:载入(txk[2],"网易WDF动画",txk[1])

	if 动作 == "防御" then
		self.特效动作[#self.特效动作].动画:置提速(1.4)
	elseif 动作 == "反震" then
		self.特效动作[#self.特效动作].动画:置提速(1.2)
	elseif 动作 == "法暴" then
		self.特效动作[#self.特效动作].动画:置提速(2)
	end
end

function 战斗单位类:添加特技内容(q)
	self.特技文本 = 分割字符(q)
	self.特技总数 = #self.特技文本
	self.特技序列 = {}
	self.特技次数 = 20
	self.特技间隔 = 4

	for n = 1, #self.特技文本, 1 do
		self.特技序列[n] = {
			文本 = self.特技文本[n],
			高度 = 0,
			x = self.显示xy.x - self.特技总数 * 4,
			y = self.显示xy.y
		}
	end

	self.特技顺序 = 1
	self.特技内容开关 = true
	self.关闭计次 = 0
end

function 战斗单位类:开启击退(死亡)
	self.击退开关 = true
	self.击退延迟 = true
	self.偏移类型 = 1
	self.停止偏移 = 30
	self.死亡参数 = 死亡

	if self.动作 ~= "防御" then
		self.动作 = "被击中"
	end

	self.动画:换方向(self.动作, self.初始方向)

	self.击退坐标 = {
		y = self.显示xy.y,
		x = self.显示xy.x
	}

	if self.敌我 == 2 then
		self.偏移坐标 = -1

		if self.死亡参数 ~= 0 then
			self.偏移坐标 = -0.4
		end
	else
		self.偏移坐标 = 1

		if self.死亡参数 ~= 0 then
			self.偏移坐标 = 0.4
		end
	end
end

function 战斗单位类:死亡处理()
	self.动画:置帧率(self.动作, 0.1)
	self.死亡开关 = true
	if self.死亡参数 == 1 then
		self.击飞开关 = true
		self.击飞数据 = {}
		self.击飞流程 = 0
		self.击飞方向 = 0
		self.击飞开关 = true
		self.击飞流程 = 1
		self.击飞时间 = 引擎.取游戏时间()

		if self.敌我 == 2 then
			self.击飞数据[1] = {
				终止 = 600,
				x = 15,
				y = 3
			}
			self.击飞数据[2] = {
				终止 = 900,
				x = 15,
				y = 1
			}
		else
			self.击飞数据[1] = {
				终止 = 600,
				x = -15,
				y = -3
			}
			self.击飞数据[2] = {
				终止 = 900,
				x = -15,
				y = -1
			}
		end
	elseif self.死亡参数 == 2 then
		self.动作 = "倒地"

		self:播放音效("倒地")

		self.动画.按帧更新 = false

		self.动画:换方向(self.动作, self.方向)

		self.倒地开关 = true
	end
end

function 战斗单位类:倒地处理()
	if self.动画[self.动作].当前帧 == self.动画[self.动作].结束帧 then
		self.动作 = "倒地"
		self.动画.按帧更新 = true
		self.倒地开关 = false
	end
end
function 战斗单位类:击飞处理()
	if self.击飞流程 == 1 then
		self.显示xy.y = self.显示xy.y - self.击飞数据[self.击飞流程].y
		self.显示xy.x = self.显示xy.x - self.击飞数据[self.击飞流程].x

		if 引擎.取游戏时间() - self.击飞时间 >= 0.005 then
			self.方向 = self.方向 - 1

			if self.方向 < 0 then
				self.方向 = 3
			end

			self.击飞时间 = 引擎.取游戏时间()

			self.动画:换方向(self.动作, self.方向)
		end

		if 取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) > 300 then
			self.击飞流程 = 2
		end
	elseif self.击飞流程 == 2 then
		self.显示xy.y = self.显示xy.y + self.击飞数据[self.击飞流程].y
		self.显示xy.x = self.显示xy.x + self.击飞数据[self.击飞流程].x

		if 引擎.取游戏时间() - self.击飞时间 >= 0.005 then
			self.方向 = self.方向 - 1

			if self.方向 < 0 then
				self.方向 = 3
			end

			self.击飞时间 = 引擎.取游戏时间()

			self.动画:换方向(self.动作, self.方向)
		end

		if 取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) > 900 then
			self.击飞流程 = 0
			self.击飞开关 = false
		end
	end
end

function 战斗单位类:复位处理()
	if self.敌我 == 1 and 取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) < 900 then
		self.显示xy.y = self.初始xy.y
		self.显示xy.x = self.初始xy.x
	end
end

function 战斗单位类:击退处理()
	if self.偏移类型 == 1 then
		self.显示xy.y = self.显示xy.y + self.偏移坐标
		self.显示xy.x = self.显示xy.x + self.偏移坐标

		if self.停止偏移 < 取两点距离(生成XY(self.击退坐标.x, self.击退坐标.y), 生成XY(self.显示xy.x, self.显示xy.y)) then
			self.偏移类型 = 2

			if self.死亡参数 ~= 0 then
				self:死亡处理()

				self.显示xy.y = self.击退坐标.y
				self.显示xy.x = self.击退坐标.x
				self.偏移类型 = 0
				self.击退开关 = false
				self.偏移类型 = 0
			else
				self.偏移类型 = 2
			end
		end
	elseif self.偏移类型 == 2 then
		self.显示xy.y = self.显示xy.y - self.偏移坐标
		self.显示xy.x = self.显示xy.x - self.偏移坐标

		if 取两点距离(生成XY(self.击退坐标.x, self.击退坐标.y), 生成XY(self.显示xy.x, self.显示xy.y)) <= self.停止偏移 then
			self.显示xy.y = self.击退坐标.y
			self.显示xy.x = self.击退坐标.x
			self.偏移类型 = 0
			self.击退开关 = false
			self.动画.按帧更新 = false
			self.显示xy.y = self.击退坐标.y
			self.显示xy.x = self.击退坐标.x

			if self.死亡参数 == 0 then
				self.动作 = "待战"

				self.动画:换方向(self.动作, self.初始方向)
			end
		end
	end
end

function 战斗单位类:设置掉血(数额, 类型)
	if 数额 == nil then
		数额 = 1
	end

	self.伤害数额 = 数额
	self.伤害数额 = math.floor(self.伤害数额)
	self.伤害总数 = string.len(tostring(self.伤害数额))
	self.伤害序列 = {}
	self.弹跳次数 = 12
	self.弹跳间隔 = 2
	self.伤害类型 = 类型
	self.掉血开关 = true
	self.加血顺序 = 0

	if 类型 == "掉血" then
		self.数据.当前气血 = self.数据.当前气血 - 数额

		if self.数据.当前气血 <= 0 then
			self.数据.当前气血 = 1
		end

		if self.法术状态组.催眠符 ~= nil then
			self.法术状态组.催眠符 = nil
		end

	else
		self.数据.当前气血 = self.数据.当前气血 + 数额

		if self.数据.气血上限 < self.数据.当前气血 then
			self.数据.当前气血 = self.数据.气血上限
		end
	end

	for n = 1, self.伤害总数, 1 do
		self.伤害序列[n] = {
			数字 = string.sub(self.伤害数额, n, n),
			高度 = 0,
			x = self.显示xy.x - self.伤害总数 * 2 - self.伤害总数 * 1.5,
			y = self.显示xy.y
		}
	end

	self.弹跳顺序 = 1
end

function 战斗单位类:加血显示()
	for n = 1, self.伤害总数, 1 do
		self.加血顺序 = self.加血顺序 + 1

		if n == self.弹跳顺序 then
			self.伤害序列[n].高度 = self.伤害序列[n].高度 + self.弹跳间隔

			if self.弹跳次数 <= self.伤害序列[n].高度 then
				self.弹跳顺序 = self.弹跳顺序 + 1
			end
		elseif self.伤害序列[n].高度 > 0 then
			self.伤害序列[n].高度 = self.伤害序列[n].高度 - 1
		end

		if self.敌我 == 2 then
			self.回复图片[self.伤害序列[n].数字 + 1]:显示(self.伤害序列[n].x - 20 + n * 12, self.伤害序列[n].y - 30 - self.伤害序列[n].高度)
		else
			self.回复图片[self.伤害序列[n].数字 + 1]:显示(self.伤害序列[n].x - 20 + n * 12, self.伤害序列[n].y - 30 - self.伤害序列[n].高度)
		end

		if self.加血顺序 >= #self.伤害序列 * 40 then
			self.掉血开关 = false
		end
	end
end

function 战斗单位类:掉血显示()
	for n = 1, #self.伤害序列, 1 do
		self.加血顺序 = self.加血顺序 + 1

		if n == self.弹跳顺序 then
			self.伤害序列[n].高度 = self.伤害序列[n].高度 + self.弹跳间隔

			if self.弹跳次数 <= self.伤害序列[n].高度 then
				self.弹跳顺序 = self.弹跳顺序 + 1
			end
		elseif self.伤害序列[n].高度 > 0 then
			self.伤害序列[n].高度 = self.伤害序列[n].高度 - 1
		end

		if self.敌我 == 2 then
			if self.伤害序列[n] == nil or self.伤害序列[n].数字 == nil then
				return 0
			end

			self.伤害图片[self.伤害序列[n].数字 + 1]:显示(self.伤害序列[n].x - 20 + n * 12, self.伤害序列[n].y - 30 - self.伤害序列[n].高度)
		else
			if self.伤害序列[n] == nil or self.伤害序列[n].数字 == nil then
				return 0
			end

			if self.伤害序列[n].数字 == nil then
				self.伤害序列[n].数字 = 0
			end

			self.伤害图片[self.伤害序列[n].数字 + 1]:显示(self.伤害序列[n].x - 10 + n * 12, self.伤害序列[n].y - 30 - self.伤害序列[n].高度)
		end
	end

	if self.加血顺序 >= #self.伤害序列 * 40 then
		self.掉血开关 = false
	end
end

function 战斗单位类:开启躲避()
	self.躲避类型 = 1
	self.停止躲避 = 75
	self.躲避开关 = true
	self.躲避数量 = 0
	self.躲避xy = {
		y = self.显示xy.y,
		x = self.显示xy.x
	}
end

function 战斗单位类:躲避事件()
	if self.躲避类型 == 1 then
		self.显示xy.y = self.显示xy.y + self.躲避坐标
		self.显示xy.x = self.显示xy.x + self.躲避坐标
		self.躲避数量 = math.floor(取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) / 15)

		if self.停止躲避 <= 取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) then
			self.躲避类型 = 2
		end
	elseif self.躲避类型 == 2 then
		self.显示xy.y = self.显示xy.y - self.躲避坐标
		self.显示xy.x = self.显示xy.x - self.躲避坐标
		self.躲避数量 = math.floor(取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) / 15)

		if 取两点距离(生成XY(self.初始xy.x, self.初始xy.y), 生成XY(self.显示xy.x, self.显示xy.y)) <= 5 then
			self.显示xy.y = self.初始xy.y
			self.显示xy.x = self.初始xy.x
			self.躲避类型 = 0
			self.躲避开关 = false
		end
	end

	self:躲避处理()
end

function 战斗单位类:躲避处理()
	for n = 1, self.躲避数量, 1 do
		self.动画:显示(self.动作, self.躲避xy.x + self.躲避参数 * n, self.躲避xy.y + self.躲避参数 * n)
	end
end

function 战斗单位类:显示(x, y, 进程)
	if self.单位消失 then
		return 0
	end

	引擎.场景.影子:显示(self.显示xy.x, self.显示xy.y)

 for n, v in pairs(self.法术状态组) do
    if self:取前置状态(n) then
     self.显示高度 = 0
     if n=="普渡众生" or n=="生命之泉" or n=="紧箍咒" or n=="天神护体" or n=="炼气化神" or n =="魔息术" then
       self.显示高度=80
       elseif n=="明光宝烛" or n == "雾杀"then -- or n == "金身舍利"
       self.显示高度=90
       end
      self.法术状态组[n]:显示(self.显示xy.x,self.显示xy.y-self.显示高度)
     end
   end

	self.动画:显示(self.动作, self.显示xy.x, self.显示xy.y)

  for n, v in pairs(self.法术状态组) do
    if self:取前置状态(n)==false then
      self.显示高度=0
     if n=="变身" or n=="幽冥鬼眼" or n=="魔王回首" or n=="极度疯狂"  then
       self.显示高度=25
     elseif n =="灵动九天" then
		self.显示高度=50
     elseif  n=="紧箍咒" or n=="炼气化神"  or n=="魔息术" or n=="天神护体"   then
       self.显示高度=80
     elseif n=="明光宝烛" or n=="雾杀"  then
       self.显示高度=90
     elseif n=="金身舍利" then
     	self.显示高度=-60
     end

     self.法术状态组[n]:显示(self.显示xy.x,self.显示xy.y-self.显示高度)
     end
   end

	if self.躲避开关 then
		self:躲避事件()
	end
	if self.动画[self.动作]:是否选中(鼠标.x,鼠标.y) and 进程 == "命令回合" then
		if self.高亮显示 == false then
			self.高亮显示 = true
			if self.动画[self.动作].身体 ~= nil then
				self.动画[self.动作].身体.允许显示 = false
			end
			self.动画:置颜色(self.动作, ARGB(105, 170, 170, 170))
		end
	elseif self.高亮显示 then

		self.高亮显示 = false
		if self.动画[self.动作].身体 ~= nil and self.动画[self.动作].身体.允许显示 ~= nil then
			self.动画[self.动作].身体.允许显示 = nil
		end
		if self.隐身 == false then
			self.动画:置颜色(self.动作, ARGB(255, 255, 255, 255))
		else
			self.动画:置颜色(self.动作, ARGB(105, 170, 170, 170))
		end
	end

	显示人物平常名称(self.数据.名称, self.显示xy.x, self.显示xy.y)

	for n = 1, #self.特效动作, 1 do
		if self.特效动作[n] ~= nil then
			self.特效动作[n].动画:更新(全局dt)

			self.临时坐标 = {
				x = self.显示xy.x,
				y = self.显示xy.y
			}
			self.结束帧 = self.特效动作[n].动画.结束帧

			if self.特效动作[n].动作 == "反震" then
				self.临时坐标.y = self.临时坐标.y - self.反震误差.y
				self.临时坐标.x = self.临时坐标.x - self.反震误差.x
				self.结束帧 = 9
			elseif self.特效动作[n].动作 == "被击中" then
				self.临时坐标 = {
					x = self.显示xy.x,
					y = self.显示xy.y - 10
				}
			end

			self.特效动作[n].动画:显示(self.临时坐标.x, self.临时坐标.y)

			if self.特效动作[n].动画.当前帧 == self.结束帧 then
				table.remove(self.特效动作, n)
			end
		end
	end

	if self.技能动画.开关 then

  if self.技能动画.名称 == "天罗地网" then
    self.技能动画.动画:显示(self.显示xy.x-170, self.显示xy.y - 220)
  elseif   self.技能动画.名称 == "血雨" then
    self.技能动画.动画:显示(self.显示xy.x+30, self.显示xy.y +65)
  elseif  self.技能动画.名称 == "逆鳞" or self.技能动画.名称 == "清心" or self.技能动画.名称 == "牛劲" or self.技能动画.名称 == "魔王回首" or self.技能动画.名称 == "变身" or
    self.技能动画.名称 =="归元咒" or self.技能动画.名称 == "极度疯狂" or self.技能动画.名称 == "杀气诀" or self.技能动画.名称 == "韦陀护法" or self.技能动画.名称 == "天神护法" then
    self.技能动画.动画:显示(self.显示xy.x+2, self.显示xy.y -31)
  elseif self.技能动画.名称 == "定心术" then
    self.技能动画.动画:显示(self.显示xy.x+2, self.显示xy.y -21)
  elseif  self.技能动画.名称 == "地涌金莲" or self.技能动画.名称 == "夺命咒" or self.技能动画.名称 == "荆棘舞" or self.技能动画.名称 == "尘土刃"  then
    self.技能动画.动画:显示(self.显示xy.x+2, self.显示xy.y +45)

	-- if self.技能动画.名称 == "生命之泉" or self.技能动画.名称 == "紧箍咒" or self.技能动画.名称 == "炼气化神" or self.技能动画.名称 == "魔息术" then
	-- 	self.技能动画.动画:显示(self.显示xy.x, self.显示xy.y - 80)
	-- elseif self.技能动画.名称 == "变身" then
	-- 	self.技能动画.动画:显示(self.显示xy.x, self.显示xy.y - 25)
	else
		self.技能动画.动画:显示(self.显示xy.x, self.显示xy.y)
	end

		if self.技能文本 == "弱点击破" then
			self.结束帧 = self.技能动画.动画.开始帧 + 4
		else
			self.结束帧 = self.技能动画.动画.结束帧
		end

		if self.技能动画.动画.当前帧 == self.结束帧 then
			self.技能动画.开关 = false
			self.技能动画.动画 = nil
		end
	end

	if self.特技内容开关 then
		self:特技文本显示()
	end
end

function 战斗单位类:特技文本显示()
	self.关闭计次 = self.关闭计次 + 1

	for n = 1, self.特技总数, 1 do
		if n == self.特技顺序 then
			self.特技序列[n].高度 = self.特技序列[n].高度 + self.特技间隔

			if self.特技次数 <= self.特技序列[n].高度 then
				self.特技顺序 = self.特技顺序 + 1
			end
		elseif self.特技序列[n].高度 > 0 then
			self.特技序列[n].高度 = self.特技序列[n].高度 - 1
		end

		self.特技文字:显示(self.特技序列[n].x - 20 + n * 12, self.特技序列[n].y - 30 - self.特技序列[n].高度, self.特技序列[n].文本)
	end

	if self.关闭计次 >= 50 then
		self.特技内容开关 = false
	end
end

function 战斗单位类:血条显示(x, y)
	if self.单位消失 then
		return 0
	end

	if self.敌我 == 1 then
		if self.数据.战斗类型 == "角色" then
			self.血条区域 = 37 * 取百分比(self.数据.当前气血, self.数据.最大气血) / 100

			self.血条:置区域(0, 0, self.血条区域 - 1, 5 - 1)

			self.临时高度 = 100

			self.空条:显示(self.显示xy.x - 15, self.显示xy.y - self.临时高度)
			self.血条:显示(self.显示xy.x - 14, self.显示xy.y - self.临时高度 + 1)
		else
			self.血条区域 = 37 * 取百分比(self.数据.当前气血, self.数据.气血上限) / 100

			self.血条:置区域(0, 0, self.血条区域 - 1, 5 - 1)

			self.临时高度 = 100

			self.空条:显示(self.显示xy.x - 15, self.显示xy.y - self.临时高度)
			self.血条:显示(self.显示xy.x - 14, self.显示xy.y - self.临时高度 + 1)
		end
	end

	if #self.发言内容 > 0 then
		self.临时宽度 = math.floor(self.发言内容[1].长度) - 2

		if self.临时宽度 > 12 then
			self.临时宽度 = 12
		end

		self.偏差宽度 = self.发言内容[1].长度 * 3 + 5

		if self.偏差宽度 > 60 then
			self.偏差宽度 = 60
		end

		self.偏差高度 = math.floor(self.发言内容[1].长度 * 0.1 + 1) * 10 + 100

		信息提示:显示(2, self.临时宽度, math.floor(self.发言内容[1].长度 * 0.1) + 2, self.显示xy.x - self.偏差宽度, self.显示xy.y - self.偏差高度)

		if self.偏差宽度 > 60 then
			self.偏差宽度 = 60
		end

		self.偏差高度 = math.floor(self.发言内容[1].长度 * 0.1 + 1) * 10 + 100 - 5

		self.超丰富文本:显示(self.显示xy.x - self.偏差宽度 + 5, self.显示xy.y - self.偏差高度)

		if os.time() - self.发言内容[1].起始 >= 5 then
			table.remove(self.发言内容, 1)
		end
	end
end

return 战斗单位类
