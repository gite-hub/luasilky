local 战斗类 = class()

local collectgarbage = collectgarbage
local sort = table.sort
local floor = math.floor
local insert = table.insert
local min = math.min
local random = 引擎.取随机整数
local pairs = pairs
local format = string.format
local tp;
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起
local tp
local sort = table.sort
local itemwp = 引擎.取物品
local jl = require("gge精灵类")
local 战斗单位类 = require("Script/战斗类/战斗单位类")
function 战斗类:初始化(根)
    tp=根
    --Script/资源类/
      self.过度纹理 = require("gge纹理类")():渲染目标(800,600)
  self.过度进度 = 155
  self.过度时间 = 30
  self.选中窗口 = 0
    self.背景 = 图像类("imge/001/0012-1.png")
    self.特技背景 = 图像类("imge/001/423-1.png")
    self.背景显示 = true
    self.法术背景 = 图像类("imge/001/2945-1.png")
    self.默认法术字体 = 文字类(simsun, 16)
    self.战斗信息提示 = {
        起始时间 = 0,
        停止时间 = 0,
        开关 = false,
        内容 = "",
        字体 = 文字类(simsun, 16)
    }

    self.默认法术字体:置颜色(黄色)

    self.禁止动画 = tp.资源:载入('JM.FT',"网易WDF动画", 389276475)
    self.禁止字体 = 文字类(simsun, 14)
    self.战斗信息提示 = {
        起始时间 = 0,
        停止时间 = 0,
        开关 = false,
        内容 = "",
        字体 = 文字类(simsun, 16)
    }

    self.禁止字体:置颜色(红色)

    self.战斗次数 = 0
    self.数字图片 = {}
    self.退出观战 = 按钮类.创建("imge/001/0015.png", 2, "退出观战", 3)

    for n = 1, 10, 1 do
        self.数字图片[n] = 图像类("imge/001/23-" .. n .. ".png")
    end
    self.窗口 = {
        -- 技能栏 = require("script/场景类/战斗技能栏").创建(根),
        -- 道具栏 = require("script/场景类/战斗道具栏").创建(根),
        -- 召唤栏 = require("script/场景类/战斗召唤栏").创建(根),
        自动栏 = require("script/战斗类/战斗自动栏").创建(根),
        -- 头像栏 = require("script/场景类/战斗头像栏").创建(根),
        -- 特技栏 = require("script/场景类/战斗特技栏").创建(根),
        -- 法宝栏 = require("script/场景类/战斗法宝栏").创建(根),
    }
    self.窗口_ = {
        -- [1] = self.窗口.技能栏,
        -- [2] = self.窗口.道具栏,
        -- [3] = self.窗口.召唤栏,
        [1] = self.窗口.自动栏,
        -- [5] = self.窗口.头像栏,
        -- [6] = self.窗口.特技栏,
        -- [7] = self.窗口.法宝栏,
    }
    self.请等待 = 图像类("imge/001/25-1.png")
    法术 = 按钮类.创建("imge/001/法术.png", 1, 1, 3)
    法宝 = 按钮类.创建("imge/001/法宝.png", 1, 1, 3)
    特技 = 按钮类.创建("imge/001/特技.png", 1, 1, 3)
    道具 = 按钮类.创建("imge/001/道具.png", 1, 1, 3)
    防御 = 按钮类.创建("imge/001/防御.png", 1, 1, 3)
    保护 = 按钮类.创建("imge/001/保护.png", 1, 1, 3)
    召唤 = 按钮类.创建("imge/001/召唤.png", 1, 1, 3)
    召还 = 按钮类.创建("imge/001/召还.png", 1, 1, 3)
    自动 = 按钮类.创建("imge/001/自动.png", 1, 1, 3)
    捕捉 = 按钮类.创建("imge/001/捕捉.png", 1, 1, 3)
    逃跑 = 按钮类.创建("imge/001/逃跑.png", 1, 1, 3)
    指挥 = 按钮类.创建("imge/001/指挥.png", 1, 1, 3)
    self.命令背景 = 图像类("imge/001/2946-1.png")
    self.道具背景 = 图像类("imge/001/414-1.png")
    self.法宝背景 = 图像类("imge/001/414-1.png")
    self.命令背景1 = 图像类("imge/001/424-1.png")
    self.召唤命令背景 = 图像类("imge/001/425-1.png")
    self.召唤文字 = 文字类.创建(simsun, 14)
    self.名称文字 = 文字类.创建(simsun, 14)
    self.召唤按钮 = 按钮类.创建("imge/001/0016.png", 2, "召唤", 3)
    self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
    self.命令坐标 = 100
    -- self.敌方位置 = {
    --  {
    --      x = 175,
    --      y = 170
    --  },
    --  {
    --      x = 115,
    --      y = 200
    --  },
    --  {
    --      x = 235,
    --      y = 140
    --  },
    --  {
    --      x = 55,
    --      y = 230
    --  },
    --  {
    --      x = 295,
    --      y = 110
    --  },
    --  {
    --      x = 220,
    --      y = 210
    --  },
    --  {
    --      x = 160,
    --      y = 240
    --  },
    --  {
    --      x = 280,
    --      y = 180
    --  },
    --  {
    --      x = 100,
    --      y = 270
    --  },
    --  {
    --      x = 340,
    --      y = 150
    --  }
    -- }
       self.敌方位置={
      [1]={x=175+10+60,y=170-10+20},
      [2]={x=115+60,y=200+20},
      [3]={x=235+20+60,y=140-20+20},
      [4]={x=55-10+60,y=230+10+20},
      [5]={x=295+30+60,y=110-30+20},
      [6]={x=220+20+60,y=210+20},
      [7]={x=160+10+60,y=240+10+20},
      [8]={x=280+30+60,y=180-10+20},
      [9]={x=100+60,y=270+20+20},
      [10]={x=340+30+60,y=150+20-20}}
    self.我方位置 = {
        {
            x = 4615,
            y = 3115
        },
        {
            x = 4005,
            y = 3405
        },
        {
            x = 5215,
            y = 2185
        },
        {
            x = 3415,
            y = 3715
        },
        {
            x = 5815,
            y = 2515
        },
        [6] = {
            x = 4115,
            y = 2715
        },
        [7] = {
            x = 3515,
            y = 3015
        },
        [8] = {
            x = 4715,
            y = 2415
        },
        [9] = {
            x = 2915,
            y = 3315
        },
        [10] = {
            x = 5315,
            y = 2115
        }
    }
    self.我方位置 = table.loadstring("do local ret={[7]={[\"y\"]=425,[\"x\"]=515},[1]={[\"y\"]=435,[\"x\"]=625},[2]={[\"y\"]=465,[\"x\"]=565},[4]={[\"y\"]=495,[\"x\"]=505},[8]={[\"y\"]=365,[\"x\"]=635},[9]={[\"y\"]=455,[\"x\"]=455},[5]={[\"y\"]=375,[\"x\"]=745},[10]={[\"y\"]=335,[\"x\"]=695},[3]={[\"y\"]=405,[\"x\"]=685},[6]={[\"y\"]=404,[\"x\"]=581}} return ret end")
    self.角色相关字体 = 文字类(simsun, 14)
    self.回合文字 = 文字类(simsun, 16)
    self.操作文字 = 文字类(simsun, 14)
    self.超丰富文本 = require("丰富文本类")(210, 270)

    self.超丰富文本:添加元素("w", 4294967295.0)
    self.超丰富文本:添加元素("h", 4278190080.0)
    self.超丰富文本:添加元素("y", 4294967040.0)
    self.超丰富文本:添加元素("r", 4294901760.0)
    self.超丰富文本:添加元素("g", 4278255360.0)

    self.参战数据 = {}
    self.回合进程 = ""
    self.命令数据 = {
        秒 = 0,
        分 = 3,
        计时 = 游戏时间
    }
end

function 战斗类:准备战斗(观战)
    tp.屏幕.移动xy.y = tp.屏幕.主角.xy.y
    tp.屏幕.移动xy.x = tp.屏幕.主角.xy.x
    tp.屏幕.xy = 取画面坐标(tp.屏幕.主角.xy.x, tp.屏幕.主角.xy.y,tp.地图.宽度,tp.地图.高度)



  引擎.截图("Dat/" .. 窗口标题.id .. ".jpg")
  -- print("。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。战斗截图")
  self.过度精灵 = jl("Dat/" .. 窗口标题.id .. ".jpg")
  self.过度进度 = 255
  self.过度时间 = 3
  self.隐藏UI = true
  self.恢复UI = true

  tp.隐藏UI = false



    yxjc = 3

    if 观战 == nil then
        self.观战模式 = false
    else
        self.观战模式 = true
    end

    self.参战单位 = {}
    self.参战数据 = {}
    self.单位总数 = 0
    self.我方数量 = 0
    self.敌方数量 = 0
    self.回合进程 = "加载回合"
    self.加载结束 = false
    self.群体法术 = {
        开关 = false
    }
    self.道具开关 = false
    self.法宝开关 = false
    self.命令数据 = {
        秒 = 0,
        分 = 3,
        计时 = 游戏时间
    }
    self.保护开关 = false
    self.捕捉开关 = false
    self.召唤开关 = false
    self.召唤编号 = 0
    self.战斗音乐 = 创建音乐(0x845DC12C, true)

    if self.战斗音乐 then self.战斗音乐:置音量(全局变量.音量) self.战斗音乐:播放() end --*10

    if tp.地图.地图音乐 ~= nil then
        tp.地图.地图音乐:停止()
    end

    if tp.主界面.界面数据[8].本类开关 then
        tp.主界面.界面数据[8].本类开关 = false
    end

    tp.屏幕.主角:停止移动()
end

function 战斗类:添加参战单位1(数据, id)
    -- local 战斗单位类 = require("Script/战斗类/战斗单位类")
    self.召唤数据 = {
        数据 = 数据,
        id = id
    }
    self.参战数据[self.召唤数据.id] = table.loadstring(table.tostring(self.召唤数据.数据))

    if self.参战数据[self.召唤数据.id].队伍id == 队伍id then
        self.我方数量 = self.我方数量 + 1
        self.参战数据[self.召唤数据.id].我方 = true
        self.参战数据[self.召唤数据.id].单位 = 战斗单位类.创建(self.参战数据[self.召唤数据.id], 1, self.我方位置)
    else
        self.敌方数量 = self.敌方数量 + 1
        self.参战数据[self.召唤数据.id].我方 = false
        self.参战数据[self.召唤数据.id].单位 = 战斗单位类.创建(self.参战数据[self.召唤数据.id], 2, self.敌方位置)
    end
        self.显示表 = {}
    for k,v in pairs(self.参战数据) do
        table.insert(self.显示表,v.单位)
    end
end


local 法术状态名称={ [1] ="横扫千军",[2]  ="后发制人",[3] ="似玉生香",[4] ="杀气诀",[5] ="催眠符",[6] ="失魂符",[7] ="定身符",[8] ="佛法无边",[9] ="金刚护体",[10] ="一苇渡江",[11] ="乘风破浪",[12] ="魔王回首",[13] ="百毒不侵2",[14] ="烟雨剑法",[15] ="百万神兵",[16] ="镇妖",[17] ="含情脉脉",[18] ="如花解语",[19] ="莲步轻舞",[20] ="日月乾坤",[21] ="连环击",[22] ="鹰击",[23] ="变身",[24] ="天雷斩",[25] ="杀气诀",[26] ="针灸",[27] ="推气过宫",[28] ="普渡众生",[29] ="生命之泉",[30] ="炼气化神",[31] ="驱魔",[32] ="尸腐毒",[33] ="幽冥鬼眼",[34] ="天神护体",[35] ="定心术",[36] ="杨柳甘露",[37] ="紧箍咒",[38] ="天罗地网",[39] ="摄魄",[40] ="勾魂",[41] ="解封",[42] ="清心",[43] ="解封",[44] ="我佛慈悲",[45] ="归元咒",[46] ="碎甲符",[47] ="明光宝烛",[48] ="解毒",[50] ="极度疯狂",[51] ="楚楚可怜",[53] ="反间之计",[54] ="失心符",[55] ="威慑",[56] ="天罗地网",[57] ="推气过宫",[58] ="金刚护法",[59] ="百毒不侵",[60] ="寡欲令",[61] ="宁心",[62] ="极度疯狂",[63] ="满天花雨",[49] ="象形",[64] ="放下屠刀",[65] ="野兽之力",[66] ="魔兽之印",[67] ="光辉之甲",[68] ="圣灵之甲",[69] ="流云诀",[70] ="啸风诀",[71] ="失忆符",[72] ="乘风破浪",[73] ="地涌金莲",[74] ="太极护法",[75] ="罗汉金钟",[76] ="鬼魂术",[77] ="隐身",[78] ="失忆符",[79] ="夜舞倾城",[80] ="错乱",[81] ="四海升平",[82] ="玉清诀",[83] ="晶清诀",[84] ="水清诀",[85] ="冰清诀",[86] ="勾魂",[87] ="摄魄",[88] ="推气过宫",[89] ="高级鬼魂术",[90] ="毒",[91] ="失忆符",[92] ="魔王回首",[93] ="离魂符",[94] ="追魂符",[95] ="一苇渡江",[96] ="宁心",[97] ="牛劲",[52] ="幽冥鬼眼",[98] ="象形限制",[99] ="盘丝阵",[100] ="魔音摄魂",[101] ="复苏",[102] ="破釜沉舟",[103] ="安神诀",[104] ="佛法无边",[105] ="一笑倾城",[106] ="飞花摘叶",[107] ="分身术",[108] ="碎甲符",[109] ="神龙摆尾",[110] ="金刚镯",[111] ="乾坤妙法",[112] ="天地同寿",[113] ="颠倒五行",[114] ="灵动九天",[115] ="火甲术",[116] ="黄泉之息",[117] ="还阳术",[118] ="魔息术",[119] ="瘴气",[120] ="翻江搅海",[121] ="天崩地裂",[122] ="断岳势",[123] ="惊涛怒",[124] ="碎星诀",[125] ="不动如山",[126] ="夺魄令",[127] ="煞气诀",[128] ="摧心术",[129] ="雾杀",[130] ="金身舍利",[131] ="蜜润",[132] ="血雨",[133] ="镇魂诀",[134] ="腾雷",[135] ="灵法",[136] ="灵断",[137] ="护佑",[138] ="御风",[139] ="怒吼",[140] ="阳护",[141] ="灵刃",[142] ="瞬击",[143] ="瞬法",[144] ="混元伞",[145] ="宝烛",[146] = "分水",[147] = "赤焰",[148] = "神木宝鼎",[149] = "我佛慈悲状态",[150] = "舍身取义状态"}
local 幻化属性值 = {"伤害","防御","气血","速度","法术伤害","抗封印等级","法术伤害结果","法术暴击等级","抗法术暴击等级","治疗能力","抗物理暴击等级","格挡值","气血回复效果","封印命中等级","固定伤害","狂暴等级","穿刺等级","物理暴击等级"}
local 幻化战斗属性 = {"抵抗封印等级","抗封印等级","法术伤害","法术伤害结果","法术暴击等级","抗法术暴击等级","治疗能力","抗物理暴击等级","格挡值","气血回复效果","封印命中等级","固定伤害","狂暴等级","穿刺等级","物理暴击等级"}
function 战斗类:添加单位(数据)

    self.临时数据 = {}
    self:resetFight(self.临时数据)
    for k, v in pairs(数据) do
        if type(v) == 'table' then
            if k == '法术状态组' then
                for kk, vv in pairs(v) do
                    if vv.有无 then self.临时数据.法术状态组[kk] = vv end
                end
            else
                self.临时数据[k] = v --table.copy(v)
            end
        else
            self.临时数据[k] = v
        end
    end

    self.单位总数 = self.单位总数 - 1
    -- self.临时数据 = 数据
    self.参战数据[self.临时数据.编号] = self.临时数据

    if self.参战数据[self.临时数据.编号].队伍id == 队伍id then
        self.我方数量 = self.我方数量 + 1
        self.参战数据[self.临时数据.编号].我方 = true
        self.参战数据[self.临时数据.编号].单位 = 战斗单位类.创建(self.参战数据[self.临时数据.编号], 1, self.我方位置, self.临时数据.编号)
    else
        self.敌方数量 = self.敌方数量 + 1
        self.参战数据[self.临时数据.编号].我方 = false
        self.参战数据[self.临时数据.编号].单位 = 战斗单位类.创建(self.参战数据[self.临时数据.编号], 2, self.敌方位置, self.临时数据.编号)
    end

    if self.单位总数 <= 0 then
        self.显示表 = {}
        for k,v in pairs(self.参战数据) do
            table.insert(self.显示表,v.单位)
        end
        self.加载结束 = true

        客户端:发送数据(3, 232, 24, "77", 0)
    end

end

function 战斗类:添加发言内容(id, 内容)
    for n = 1, #self.参战数据, 1 do
        if self.参战数据[n].单位.数据.数字id == id and self.参战数据[n].单位.数据.战斗类型 == "角色" then
            self.参战数据[n].单位:添加发言内容(内容)
        end
    end
end

function 战斗类:设置执行回合(数据)
    self.战斗流程 = 数据

    self.回合进程 = "执行回合"

    for n = 1, #self.战斗流程.信息提示, 1 do
        if self.战斗流程.信息提示[n].id == 系统处理类.数字id or self.战斗流程.信息提示[n].id == 0 then
            tp.提示:写入(self.战斗流程.信息提示[n].内容)
        end
    end

    if #self.战斗流程 == 0 then
        self.回合进程 = "等待回合"

        客户端:发送数据(5, 234, 24, "1A")
    end
end

function 战斗类:设置命令回合(数据)
    self.回合进程 = "命令回合"
    self.命令类型 = "攻击"
    self.命令对象 = 2
    self.命令结束 = {}
    self.群体法术.开关 = false
    全局变量.地图开关 = true
    self.临时数据 = 数据
    self.自动起始 = os.time()
    self.操作单位 = {}
    self.当前单位 = 1
    self.命令版面 = true
    self.捕捉开关 = false
    self.法术开关 = false
    self.召唤开关 = false
    self.自动减少 = true
    self.特技开关 = false
    self.道具开关 = false
    self.法宝开关 = false
    for n = 1, #self.参战数据, 1 do
        self.参战数据[n].单位:复位处理()
    end

    for n = 1, #self.临时数据, 1 do
        if self.参战数据[self.临时数据[n]] ~= nil then
            self.操作单位[#self.操作单位 + 1] = {
                参数 = 0,
                类型 = "",
                目标 = "",
                附加 = 0,
                编号 = self.临时数据[n]
            }

            if self.参战数据[self.操作单位[#self.操作单位].编号].单位.数据.付费 then
                self.自动减少 = false
            end
        end
    end

    self.命令数据 = {
        秒 = 0,
        分 = 3,
        计时 = 游戏时间
    }
end


function 战斗类:更新状态组(数据)
    self.临时数据1 = 数据

    for n = 1, #self.临时数据1, 1 do
        --if not self.临时数据1[n].id and not self.参战数据[self.临时数据1[n].id] and not self.参战数据[self.临时数据1[n].id].单位 then
            self.参战数据[self.临时数据1[n].id].单位.法术状态组[self.临时数据1[n].名称] = nil
            if self.临时数据1[n].名称 == "鬼魂术" then
                self.参战数据[self.临时数据1[n].id].单位.动作 = "待战"
                self.参战数据[self.临时数据1[n].id].单位.动画.按帧更新 = false
                self.参战数据[self.临时数据1[n].id].单位.动画:换方向("待战", self.参战数据[self.临时数据1[n].id].单位.方向)
                self.参战数据[self.临时数据1[n].id].单位:设置掉血(self.临时数据1[n].气血, "加血")
            elseif self.临时数据1[n].名称 == "隐身" or self.临时数据1[n].名称 == "分身术" then
                self.参战数据[self.临时数据1[n].id].单位.隐身 = false
                self.参战数据[self.临时数据1[n].id].单位.动画:置颜色("待战", ARGB(255, 255, 255, 255))
            elseif self.临时数据1[n].名称 == "金刚护法"  then
                self.参战数据[self.临时数据1[n].id].单位.闪光开关 = false
                self.参战数据[self.临时数据1[n].id].单位.动画[self.参战数据[self.临时数据1[n].id].单位.动作]:取消高亮()
            --end
        end
    end
end

function 战斗类:刷新召唤数据(数据)
    if self.回合进程 ~= "命令回合" then
        return 0
    elseif self.参战数据[self.操作单位[self.当前单位].编号].战斗类型 ~= "角色" then
        return 0
    else
        self.召唤数据 = {}
        self.加载文本 = {
            "参战",
            "等级",
            "名称",
            "当前气血",
            "气血上限",
            "忠诚",
            "当前魔法",
            "魔法上限"
        }
        self.临时数据 = 数据

        for n = 1, #self.临时数据, 1 do
            self.召唤数据[n] = {}

            for i = 1, #self.加载文本, 1 do
                self.召唤数据[n][self.加载文本[i]] = self.临时数据[n][self.加载文本[i]]
            end

            self.召唤数据[n].包围盒 = 包围盒.创建(323, 140 + n * 20, 64, 18)

            self.召唤数据[n].包围盒:置坐标(323, 140 + n * 20)
            self.召唤数据[n].包围盒:置宽高(64, 18)
        end

        self.召唤开关 = true
        self.召唤编号 = 0
    end
end

function 战斗类:刷新道具数据(数据)
    if self.回合进程 ~= "命令回合" then
        return 0
    end

    self.物品数据 = {}

    for n = 1, 20, 1 do
        if 数据[n] ~= nil then
            self.物品数据[n] = 物品数据类.创建(数据[n], 3, "包裹", n)
            self.物品数据[n].编号 = 数据[n].编号
        end
    end

    self.道具开关 = true
end
function 战斗类:刷新法宝数据(数据)
    if self.回合进程 ~= "命令回合" then
        return 0
    end
    self.物品数据 = {}
    for n = 1, 20, 1 do
        if 数据[n] ~= nil then
            self.物品数据[n] = 物品数据类.创建(数据[n], 3, "法宝", n)
            self.物品数据[n].编号 = 数据[n].编号
        end
    end
    self.法宝开关 = true
end
function 战斗类:按键事件()
    if self.命令版面 == false then
        return 0
    end

    if 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.A) then
        self.操作单位[self.当前单位].类型 = "攻击"
        self.操作单位[self.当前单位].目标 = 0

        if self.当前单位 == #self.操作单位 then
            鼠标动画.显示序列 = 1
            self.回合进程 = "等待回合"

            客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
        else
            self.当前单位 = self.当前单位 + 1
        end
    elseif 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.Q) then
        self:使用默认法术S()
    elseif 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.S) then
        self:使用默认法术S()
    elseif 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.W) then
        self:设置法术数据(self.操作单位[self.当前单位].编号)
    elseif 防御:取是否单击() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.D) then
        self.操作单位[self.当前单位].类型 = "防御"
        self.操作单位[self.当前单位].目标 = 0

        if self.当前单位 == #self.操作单位 then
            鼠标动画.显示序列 = 1
            self.回合进程 = "等待回合"

            客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
        else
            self.当前单位 = self.当前单位 + 1
        end
    end
end

function 战斗类:使用默认法术S()

  if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术=="" or self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术==nil then
    tp.提示:写入("#y/请先设置好默认法术")

  else
    self.临时法术名称=self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术
   if 取技能信息(self.临时法术名称,"对象")==3  then


         self.命令类型="技能"
         self.命令对象=2
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false

        elseif 取技能信息(self.临时法术名称,"对象")==2  then

         self.命令类型="技能"
         self.命令对象=1
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false

         elseif 取技能信息(self.临时法术名称,"对象")==1  then

         self.命令类型="技能"
         self.命令对象=1
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false
         self.操作单位[self.当前单位].类型=self.命令类型
         self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
         self.操作单位[self.当前单位].参数=self.命令参数

          if self.当前单位==#self.操作单位 then
              鼠标动画.显示序列=1
              self.回合进程="等待回合"
              客户端:发送数据(1,235,24,self:转文本(self.操作单位))
          else
            self.当前单位=self.当前单位+1
            self.命令版面=true
            self.捕捉开关=false
            self.法术开关=false
            self.命令类型="攻击"
            self.命令对象=2
            鼠标动画.显示序列=1


            end



         end



    end
end
function 战斗类:使用默认法术()

  if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术=="" or self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术==nil then
    tp.提示:写入("#y/请先设置好默认法术")

  else
    self.临时法术名称=self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术
   if 取技能信息(self.临时法术名称,"对象")==3  then


         self.命令类型="技能"
         self.命令对象=2
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false

        elseif 取技能信息(self.临时法术名称,"对象")==2  then

         self.命令类型="技能"
         self.命令对象=1
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false

         elseif 取技能信息(self.临时法术名称,"对象")==1  then

         self.命令类型="技能"
         self.命令对象=1
         self.命令参数=self.临时法术名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.法术开关=false
         self.操作单位[self.当前单位].类型=self.命令类型
         self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
         self.操作单位[self.当前单位].参数=self.命令参数

          if self.当前单位==#self.操作单位 then
              鼠标动画.显示序列=1
              self.回合进程="等待回合"
              客户端:发送数据(1,235,24,self:转文本(self.操作单位))
          else
            self.当前单位=self.当前单位+1
            self.命令版面=true
            self.捕捉开关=false
            self.法术开关=false
            self.命令类型="攻击"
            self.命令对象=2
            鼠标动画.显示序列=1


            end



         end



    end
end
function 战斗类:单位选择()
    self.选中单位 = 0
    self.选择结果 = false

    if (self.命令对象 == 2 or self.命令对象 == 1) and tp.主界面.队伍数据 ~= nil then
        for i = 1, #tp.主界面.队伍数据, 1 do
            if tp.主界面.队伍数据[i].头像:是否选中(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
                self.队友编号 = 0

                for w = 1, #self.参战数据, 1 do


                    if self.队友编号 == 0 and self.参战数据[w].单位.敌我 == 1 and self.参战数据[w].单位.数据.名称 == tp.主界面.队伍数据[i].名称 and self.参战数据[w].单位.数据.战斗类型 == "角色" then
                        self.队友编号 = w
                    end
                end

                if self.队友编号 == 0 then
                    tp.提示:写入("#y/目标无法被选择")

                    return 0
                else
                    if self.命令类型 == "保护" and self.参战数据[self.队友编号].单位.数据.编号 == self.操作单位[self.当前单位].编号 then
                        tp.提示:写入("#y/你无法选择自己作为保护目标")

                        return 0
                    end

                    self.操作单位[self.当前单位].类型 = self.命令类型
                    self.操作单位[self.当前单位].目标 = self.参战数据[self.队友编号].单位.数据.编号
                    self.操作单位[self.当前单位].参数 = self.命令参数

                    if self.当前单位 == #self.操作单位 then
                        鼠标动画.显示序列 = 1
                        self.回合进程 = "等待回合"

                        客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
                    else
                        self.当前单位 = self.当前单位 + 1
                        self.命令版面 = true
                        self.捕捉开关 = false
                        self.法术开关 = false
                        self.保护开关 = false
                        self.命令类型 = "攻击"
                        self.命令对象 = 2
                        鼠标动画.显示序列 = 1
                    end
                end
            end
        end
    end

    for n = 1, #self.参战数据, 1 do
        if self.参战数据[n].单位.动画[self.参战数据[n].单位.动作]:是否选中(鼠标.x,鼠标.y) then
            if self.命令类型 == "攻击" then
                鼠标动画.显示序列 = 2
                self.选中单位 = n
            end

            if 引擎.鼠标弹起(左键) and self.选择结果 == false and self:取界面重叠() == false then
                self.选择结果 = true
                self.界面判断 = false


                if self.命令对象 == self.参战数据[n].单位.敌我 then
                    if self.命令类型 == "保护" and self.参战数据[n].单位.数据.编号 == self.操作单位[self.当前单位].编号 then
                        tp.提示:写入("#y/你无法选择自己作为保护目标")

                        return 0
                    end

                    self.操作单位[self.当前单位].类型 = self.命令类型
                    self.操作单位[self.当前单位].目标 = self.参战数据[n].单位.数据.编号
                    self.操作单位[self.当前单位].参数 = self.命令参数
                    if self.当前单位 == #self.操作单位 then
                        鼠标动画.显示序列 = 1
                        self.回合进程 = "等待回合"

                        客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
                    else
                        self.当前单位 = self.当前单位 + 1
                        self.命令版面 = true
                        self.捕捉开关 = false
                        self.法术开关 = false
                        self.保护开关 = false
                        self.命令类型 = "攻击"
                        self.命令对象 = 2
                        鼠标动画.显示序列 = 1
                    end
                else
                    tp.提示:写入("#y/你无法选择这样的单位作为目标")
                end
            end
        end
    end

    if self.选中单位 == 0 and self.命令类型 == "攻击" then
        鼠标动画.显示序列 = 1
    end
end

function 战斗类:转文本(q)
    self.组合文本 = ""

    for n, v in pairs(q) do
        if q[n].参数 == nil then
            q[n].参数 = "1"
        end

        if q[n].附加 == nil then
            q[n].附加 = "1"
        end

        self.组合文本 = self.组合文本 .. q[n].编号 .. "@-@" .. q[n].目标 .. "@-@" .. q[n].类型 .. "@-@" .. q[n].参数 .. "@-@" .. q[n].附加 .. "@-@*-*"
    end

    return self.组合文本
end

function 战斗类:添加战斗提醒文字(q)
    self.战斗信息提示.开关 = true
    self.战斗信息提示.文字 = q
    self.战斗信息提示.起始时间 = os.time()
end

function 战斗类:执行回合更新(dt)
    if self.战斗流程[1] == nil then
        return 0
    end

    if self.战斗流程[1].执行 == false then
        self.战斗流程[1].执行 = true
        self.执行流程 = self.战斗流程[1].流程
        if self.战斗流程[1].类型 == "技能" and self.战斗流程[1].流程 ~= 100 and self.战斗流程[1].流程 ~= 102 and self.战斗流程[1].流程 ~= 104 then
            if self.参战数据[self.战斗流程[1].攻击方] == nil then
                tp.提示:写入("#y/错误的技能参数#r/" .. self.战斗流程[1].参数)
                table.remove(self.战斗流程, 1)
                elseif self.战斗流程[1].参数=="连击" then
            else
                self:添加战斗提醒文字(self.参战数据[self.战斗流程[1].攻击方].单位.数据.名称 .. "使用了" .. self.战斗流程[1].参数)
            end
        elseif self.战斗流程[1].类型 == "特技" then
            self.参战数据[self.战斗流程[1].攻击方].单位:添加特技内容(self.战斗流程[1].参数)
        end
    end

    if self.战斗流程[1].类型 == "攻击" then
        self:攻击事件()
    elseif self.战斗流程[1].类型 == "捕捉" then
        self:捕捉事件()
    elseif self.战斗流程[1].类型 == "技能" then
        self:技能事件()
    elseif self.战斗流程[1].类型 == "特技" and self.参战数据[self.战斗流程[1].攻击方].单位.特技内容开关 == false then
        self:技能事件()
    elseif self.战斗流程[1].类型 == "逃跑" then
        for n = 1, #self.战斗流程[1].逃跑单位, 1 do
            self.参战数据[self.战斗流程[1].逃跑单位[n]].单位.单位消失 = true
        end

        if self.战斗流程[1].id == 系统处理类.数字id then
            self:退出战斗()
        end

     table.remove(self.战斗流程, 1)

        if #self.战斗流程 == 0 then
            self.回合进程 = "等待回合"
             客户端:发送数据(5, 234, 24, "1A")
        end
    elseif self.战斗流程[1].类型 == "召唤" and self.战斗流程[1].进行 == false then
        self:添加参战单位1(self.战斗流程[1].数据, self.战斗流程[1].id)
        self:添加战斗提醒文字(self.战斗流程[1].名称 .. "加入了战斗")

        self.战斗流程[1].进行 = false

        table.remove(self.战斗流程, 1)

        if #self.战斗流程 == 0 then
            self.回合进程 = "等待回合"

             客户端:发送数据(5, 234, 24, "1A")
        end
    end

    if self.群体法术.开关 then
        for n = 1, #self.群体法术.动画, 1 do
            if self.群体法术.动画[n].更新 then
                self.群体法术.动画[n].动画:更新(dt)
            end
        end

        self:群体法术动画显示()
    end
end

function 战斗类:自动使用法术()
    self.敌方单位组 = {}
    self.我方单位组 = {}

    for n = 1, #self.参战数据, 1 do
        if self.参战数据[n].单位.敌我 == 2 then
            self.敌方单位组[#self.敌方单位组 + 1] = n
        elseif self.参战数据[n].单位.敌我 == 1 then
            self.我方单位组[#self.我方单位组 + 1] = n
        end
    end

    for n = 1, #self.操作单位, 1 do
        self.当前单位 = n

        if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术 == "" or self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术 == nil then
            self.操作单位[self.当前单位].类型 = "攻击"
            self.操作单位[self.当前单位].参数 = ""
            self.操作单位[self.当前单位].目标 = self.敌方单位组[取随机数(1, #self.敌方单位组)]
        else
            self.临时法术名称 = self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术
            self.操作单位[self.当前单位].类型 = "技能"
            self.操作单位[self.当前单位].参数 = self.临时法术名称

            if 取技能信息(self.临时法术名称,"对象") == 3 then
                self.操作单位[self.当前单位].目标 = self.敌方单位组[取随机数(1, #self.敌方单位组)]
            elseif 取技能信息(self.临时法术名称,"对象") == 2 then
                self.操作单位[self.当前单位].目标 = self.我方单位组[取随机数(1, #self.我方单位组)]
            else
                self.操作单位[self.当前单位].目标 = self.操作单位[self.当前单位].编号
            end
        end
    end

    鼠标动画.显示序列 = 1
    self.回合进程 = "等待回合"

    客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
end
function 战斗类:取鼠标所在窗口(x,y)
    if not tp.隐藏UI then
        for n=#self.窗口_, 1,-1 do
            if self.窗口_[n]:检查点(x,y)  then
                tp.选中UI = true
                return n
            end
        end
    end
    return 0
end

local function UI排序(a,b)
    return a.窗口时间 < b.窗口时间
end
function 战斗类:捕捉事件()
    if self.执行流程 == 1 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("捕捉2")
        self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("施法")

        self.执行流程 = 2
    elseif self.执行流程 == 2 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 3
        end
    elseif self.执行流程 == 3 then
        if self.战斗流程[1].结果 == false then
            self.执行流程 = 4
        else
            self.执行流程 = 5
        end
    elseif self.执行流程 == 4 then
        table.remove(self.战斗流程, 1)

        if #self.战斗流程 == 0 then
            self.回合进程 = "等待回合"

            客户端:发送数据(5, 234, 24, "1A")
        end
    elseif self.执行流程 == 5 then
        self.闪烁次数 = 3
        self.闪烁间隔 = 5
        self.闪烁回来 = 0
        self.闪烁起始 = 引擎.取游戏时间()
        self.临时xy = {
            x = self.参战数据[self.战斗流程[1].攻击方].单位.显示xy.x,
            y = self.参战数据[self.战斗流程[1].攻击方].单位.显示xy.y
        }
        self.执行流程 = 6
    elseif self.执行流程 == 6 and self.闪烁间隔 <= 引擎.取游戏时间() - self.闪烁起始 then
        self.闪烁起始 = 引擎.取游戏时间()
        self.闪烁次数 = self.闪烁次数 - 1

        if self.闪烁回来 == 0 then
            self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y = 9999
            self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x = 9999
        else
            self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y = self.临时xy.y
            self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x = self.临时xy.x
        end

        if self.闪烁次数 <= 0 then
            self.执行流程 = 4
        end
    end
end

function 战斗类:命令回合更新(dt)
    if self.参战数据[self.操作单位[self.当前单位].编号].战斗类型 == "角色" and self.命令版面 then
        法术:更新()
        法宝:更新()
        特技:更新()
        道具:更新()
        防御:更新()
        保护:更新()
        指挥:更新()
        召唤:更新()
        召还:更新()
        自动:更新()
        捕捉:更新()
        逃跑:更新()

        if 捕捉:取是否单击() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.G) then
            if self.捕捉开关 then
                self.捕捉开关 = false
                鼠标动画.显示序列 = 1
                self.命令版面 = true
            else
                self.捕捉开关 = true
                self.命令类型 = "捕捉"
                self.命令对象 = 2
                鼠标动画.显示序列 = 3
                self.命令版面 = false

                tp.提示:写入("#y/再次按下alt+G可解除捕捉指令")
            end
        elseif 逃跑:取是否单击() then
            self.操作单位[self.当前单位].类型 = "逃跑"
            self.操作单位[self.当前单位].目标 = 0

            if self.当前单位 == #self.操作单位 then
                鼠标动画.显示序列 = 1
                self.回合进程 = "等待回合"

                客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
            else
                self.当前单位 = self.当前单位 + 1
            end
        elseif 保护:取是否单击() or (引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.T)) then
            if self.保护开关 then
                self.保护开关 = false
                鼠标动画.显示序列 = 1
                self.命令版面 = true
            else
                self.保护开关 = true
                self.命令类型 = "保护"
                self.命令对象 = 1
                鼠标动画.显示序列 = 5
                self.命令版面 = false

                tp.提示:写入("#y/再次按下alt+T可解除保护指令")
            end
        elseif 法术:取是否单击() then
            self:设置法术数据(self.操作单位[self.当前单位].编号)
        elseif 2 == 1 and 特技:取是否单击() then
            if self.特技开关 then
                self.特技开关 = false
            else
                self.特技开关 = true
            end
        elseif 召唤:取是否单击() then
            if self.召唤开关 then
                self.召唤开关 = false
            else
                客户端:发送数据(1, 237, 24, "91")
            end
        elseif 特技:取是否单击() then
            if self.特技开关 then
                self.特技开关 = false
            else
                self.特技开关 = true
            end
        elseif 道具:取是否单击() or (引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.E) ) then
            if self.道具开关 then
                self.道具开关 = false
            else
                客户端:发送数据(1, 236, 24, "91")
            end
        elseif 法宝:取是否单击() then
            if self.法宝开关 then
                self.法宝开关 = false
            else
                客户端:发送数据(1, 240, 24, "91")
            end
        elseif 召唤:取是否单击() then
            if self.召唤开关 then
                self.召唤开关 = false
            else
                客户端:发送数据(1, 237, 24, "91")
            end
        elseif 自动:取是否单击() then
                自动战斗 = true
                自动回合 = 9999
                tp.提示:写入("#y/你开启了自动战斗，在命令回合开始5秒后自动下达指令")
        end
    elseif self.命令版面 then
        法术:更新()
        道具:更新()
        保护:更新()
        防御:更新()

        if 法术:取是否单击() then
            self:设置法术数据(self.操作单位[self.当前单位].编号)
        elseif 道具:取是否单击() then
            if self.道具开关 then
                self.道具开关 = false
            else
                客户端:发送数据(1, 236, 24, "91")
            end
        elseif 保护:取是否单击() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.T) then
            if self.保护开关 then
                self.保护开关 = false
                鼠标动画.显示序列 = 1
                self.命令版面 = true
            else
                self.保护开关 = true
                self.命令类型 = "保护"
                self.命令对象 = 1
                鼠标动画.显示序列 = 5
                self.命令版面 = false

                tp.提示:写入("#y/再次按下alt+T可解除保护指令")
            end
        end
    end

    self:单位选择()
    self:按键事件()
    if 自动战斗  then
        self.窗口.自动栏.可视化=true
        if os.time()-self.自动起始>=3 then
        self:自动使用法术()
        self.自动起始=os.time()
            if self.自动减少 then
                自动回合=自动回合-1
                if 自动回合<5 then
                tp.提示:写入("#y/您当前的自动回合数还剩"..自动回合.."回合")
                end
                if 自动回合<=0 then
                自动战斗=false
                tp.提示:写入("#Y/你的自动战斗功能已经被取消")
                end
          end
        end
    end

    if 引擎.鼠标弹起(右键) and 鼠标动画.显示序列 == 4 then
        self.命令版面 = true
        self.捕捉开关 = false
        self.法术开关 = false
        self.命令类型 = "攻击"
        self.命令对象 = 2
        鼠标动画.显示序列 = 1
    end
end

function 战斗类:设置法术数据(编号)
    if self.法术开关 then
        self.法术开关 = false

        return 0
    end

    self.法术数据 = {}

    for n = 1, #self.参战数据[编号].主动技能, 1 do
        self.法术数据[n] = {
            名称 = self.参战数据[编号].主动技能[n].名称,
            等级 = self.参战数据[编号].主动技能[n].等级
        }
    end

    self.法术开关 = true
end

local function 显示排序1(a,b)
    return a.显示xy.y  < b.显示xy.y
end
function 战斗类:显示(dt,x, y)

    -----------------------
    if 全局变量.地图开关 then
        self.背景:显示(0, 0)
    end
    if self.过度精灵 ~= nil then
            self.过度时间 = self.过度时间 - 0.55
            if self.过度时间 <= 0 then
                self.过度进度 = self.过度进度 - (self.过度减少 or 10)
                self.过度时间 = 0
                if self.过度进度 <= 0 then
                    self.过度进度 = 0
                end
            end
            if self.过度进度 <= 240 then
                if self.恢复UI then
                    self.隐藏UI = false
                    self.恢复UI = false
                end
            end
            self.过度精灵:置颜色(ARGB(self.过度进度,255,255,255))
            self.过度精灵:显示()
            if self.过度进度 <= 0 then
                self.过度精灵:释放()
                self.过度精灵 = nil
                self.过度减少 = nil
            end

    else
        if self.加载结束 == false then
            return 0
        end
        if self.回合进程 == "命令回合" and self.观战模式 == false then
            self:命令回合更新(dt)
        elseif self.回合进程 == "执行回合" then
            self:执行回合更新(dt)
        end

        if self.观战模式 then
            self.退出观战:更新(720, 510)

            if self.退出观战:取是否单击() then
                客户端:发送数据(1, 239, 24, "77")
            end
        end
        sort(self.窗口_,UI排序)
        if mousea(0) and self.选中窗口 ~= 0  and self.窗口_[self.选中窗口].可移动 and not tp.消息栏焦点 then
            self.窗口_[self.选中窗口]:初始移动(x,y)
        end
        if mouseb(0)  or self.隐藏UI or self.消息栏焦点 then
            self.移动窗口 = false
        end
        if self.移动窗口 then
            self.窗口_[#self.窗口_]:开始移动(x,y)
        end
        if mouseb(0) then
            self.移动窗口 = false
        end
        for n=1,#self.窗口_ do
            if self.选中窗口 ~= 0 and self.窗口_[self.选中窗口].ID == self.窗口_[n].ID then
                self.窗口_[n].鼠标 = true
            else
                self.窗口_[n].鼠标 = false
            end
            if not tp.隐藏UI then
                self.窗口_[n]:更新(dt,x,y)
            else
                if self.窗口_[n].ID == 1 then
                    self.窗口_[n]:更新(dt,x,y)
                end
            end
        end
        sort(self.显示表,显示排序1)
        for i=1,#self.显示表 do
              self.显示表[i]:显示(x, y, self.回合进程)
            self.显示表[i]:更新(dt)
        end
        for i=1,#self.显示表 do
            self.显示表[i]:血条显示()
        end


        if self.回合进程 ~= "命令回合" and self.群体法术.开关 then
            self:群体法术动画显示()
        end
        for i=1,#self.显示表 do
            if self.显示表[i].掉血开关 then
                if self.显示表[i].伤害类型 == "掉血" then
                    self.显示表[i]:掉血显示()
                else
                    self.显示表[i]:加血显示()
                end
                -- 分不清谁
                -- print("引擎.场景.窗口.人物框:显示(dt, x, y)")
                -- 引擎.场景.窗口.人物框:显示(dt, x, y)

            end
        end



        if self.回合进程 == "命令回合" then
            self:命令回合显示()
        end

        if self.回合进程 ~= "命令回合" then

            if self.回合进程 ~= "执行回合" then
                self.请等待:显示(320, 60)
            end

            if self.战斗信息提示.开关 then
                self.战斗信息提示.字体:显示(300, 500, self.战斗信息提示.文字)

                if os.time() - self.战斗信息提示.起始时间 >= 2 then
                    self.战斗信息提示.开关 = false
                end
            end
        end


        if tp.窗口.消息框窗口.可视 then
            tp.窗口.消息框窗口.鼠标 = true
            tp.窗口.消息框窗口:显示(dt, 鼠标.x, 鼠标.y)
        else
            tp.窗口.消息框窗口.鼠标 = false
        end
    end

    if self.观战模式 then
        self.退出观战:显示(720, 510)
    end
end

function 战斗类:命令回合结束()
    self.回合进程 = "等待回合"
end

function 战斗类:命令回合显示()
    if self.加载结束 == false then
        return 0
    end

    self.秒显示 = 0
    self.分显示 = 0
    self.结果 = 游戏时间 - self.命令数据.计时
    self.显示时间 = 0

    if self.结果 >= 1 then
        self.命令数据.计时 = 游戏时间
        self.命令数据.秒 = self.命令数据.秒 - 1

        if self.命令数据.秒 < 0 then
            if self.命令数据.分 <= 0 and self.命令数据.秒 <= 0 then
                self:命令回合结束()

                self.显示时间 = 1
            elseif self.命令数据.秒 <= 0 then
                self.命令数据.秒 = 9
                self.命令数据.分 = self.命令数据.分 - 1
            end
        end
    end

    if self.显示时间 == 0 then
        self.分显示 = self.命令数据.分 + 1

        if self.分显示 > 10 then
            self.分显示 = 1
        end

        self.秒显示 = self.命令数据.秒 + 1

        if self.秒显示 > 10 then
            self.秒显示 = 1
        end

        self.数字图片[self.分显示]:显示(350, 60)
        self.数字图片[self.秒显示]:显示(400, 60)
    end

    self.命令坐标 = 100

    if self.参战数据[self.操作单位[self.当前单位].编号].战斗类型 == "角色" and self.命令版面 then
        self.命令背景:显示(567 + self.命令坐标, 82)
        法术:显示(570 + self.命令坐标, 86)
        法宝:显示(570 + self.命令坐标, 110)
        特技:显示(570 + self.命令坐标, 134)
        道具:显示(570 + self.命令坐标, 158)
        防御:显示(570 + self.命令坐标, 182)
        保护:显示(570 + self.命令坐标, 206)
        指挥:显示(570 + self.命令坐标, 230)
        召唤:显示(570 + self.命令坐标, 254)
        召还:显示(570 + self.命令坐标, 278)
        自动:显示(570 + self.命令坐标, 302)
        捕捉:显示(570 + self.命令坐标, 326)
        逃跑:显示(570 + self.命令坐标, 350)
    elseif self.命令版面 then
        self.命令背景1:显示(567 + self.命令坐标, 82)
        法术:显示(570 + self.命令坐标, 86)
        道具:显示(570 + self.命令坐标, 110)
        保护:显示(570 + self.命令坐标, 134)
        防御:显示(570 + self.命令坐标, 158)
        逃跑:显示(570 + self.命令坐标, 182)
    end

    if self.法术开关 then
        self:法术界面显示()
    elseif self.召唤开关 then
        self:召唤界面显示()
    elseif self.特技开关 then
        self:特技界面显示()
    elseif self.道具开关 then
        self:道具界面显示()
    elseif self.法宝开关 then
        self:法宝界面显示()
    elseif self.召唤开关 then
        self:召唤界面显示()
    end
end

function 战斗类:召唤界面显示()
    self.召唤命令背景:显示(300, 100)
    self.召唤按钮:更新()
    self.召唤按钮:显示(350, 405)
    self.取消按钮:更新()
    self.取消按钮:显示(400, 405)

    if self.取消按钮:取是否单击() then
        self.召唤开关 = false
    end

    for n = 1, #self.召唤数据, 1 do
        if n == self.召唤编号 then
            self.召唤文字:置颜色(红色):显示(323, 140 + n * 20, self.召唤数据[n].名称)
        elseif self.召唤数据[n].参战 then
            self.召唤文字:置颜色(绿色):显示(323, 140 + n * 20, self.召唤数据[n].名称)
        else
            self.召唤文字:置颜色(黑色):显示(323, 140 + n * 20, self.召唤数据[n].名称)
        end

        if self.召唤数据[n].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(键盘符号.左键) then
            self.召唤编号 = n
        end
    end

    if self.召唤编号 ~= 0 then
        self.召唤文字:置颜色(黑色):显示(363, 280, self.召唤数据[self.召唤编号].名称)
        self.召唤文字:置颜色(黑色):显示(377, 305, self.召唤数据[self.召唤编号].等级)
        self.召唤文字:置颜色(黑色):显示(377, 330, math.floor(self.召唤数据[self.召唤编号].忠诚))
        self.召唤文字:置颜色(黑色):显示(370, 355, self.召唤数据[self.召唤编号].当前气血 .. "/" .. self.召唤数据[self.召唤编号].气血上限)
        self.召唤文字:置颜色(黑色):显示(370, 380, self.召唤数据[self.召唤编号].当前魔法 .. "/" .. self.召唤数据[self.召唤编号].魔法上限)

        if self.召唤按钮:取是否单击() then
            if self.召唤数据[self.召唤编号].参战 then
                tp.提示:写入("#y/这只召唤兽已经出战过了")
            else
                self.操作单位[self.当前单位].类型 = "召唤"
                self.操作单位[self.当前单位].目标 = self.召唤编号
                self.操作单位[self.当前单位].参数 = self.命令参数
                self.召唤开关 = false

                if self.当前单位 == #self.操作单位 then
                    鼠标动画.显示序列 = 1
                    self.回合进程 = "等待回合"

                    客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
                else
                    self.当前单位 = self.当前单位 + 1
                    self.命令版面 = true
                    self.捕捉开关 = false
                    self.法术开关 = false
                    self.命令类型 = "攻击"
                    self.命令对象 = 2
                    鼠标动画.显示序列 = 1
                end
            end
        end
    end
end

function 战斗类:取界面重叠()
    if self.道具开关 and self.道具背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
        return true
    elseif self.召唤开关 and self.召唤命令背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
        return true
    elseif self.特技开关 and self.特技背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
        return true
    elseif self.法术开关 and self.法术背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
        return true
    elseif self.法宝开关 and self.法宝背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
        return true
    else
        return false
    end
end
function 战斗类:法宝界面显示()
    self.法宝背景:显示(250, 40)
    self.a = 0
    self.b = 0
    self.选中道具 = 0
    for n = 1, 20, 1 do
        self.a = n / 5
        if self.a < 1 then
            self.a = 1
        elseif self.a > 1 and self.a < 2 then
            self.a = 2
        elseif self.a > 2 and self.a < 3 then
            self.a = 3
        elseif self.a > 3 and self.a < 4 then
            self.a = 4
        end
        self.b = self.b + 1
        if self.b == 6 then
            self.b = 1
        end
        if self.物品数据[n] ~= nil then
            self.物品数据[n]:显示(270 + self.b * 50 - 50, 77 + self.a * 50 - 50)

            if self.物品数据[n].类型 ~= "法宝" then
                self.禁止字体:显示(270 + self.b * 50 - 50, 77 + self.a * 50 - 30, "不可用")
            elseif self.物品数据[n].类型 == "法宝" and itemwp(self.物品数据[n].名称).主动被动 == "被动" then
                self.禁止字体:显示(270 + self.b * 50 - 50, 77 + self.a * 50 - 30, "不可用")
            elseif self.物品数据[n]:获取焦点() and self.物品数据[n].类型 == "法宝" and itemwp(self.物品数据[n].名称).主动被动 == "主动"  then
                self.选中道具 = n
            end
        end
    end
    if self.选中道具 ~= 0 then
        self.物品数据[self.选中道具]:显示事件(2)
        if 引擎.鼠标弹起(左键) then
            self.命令类型 = "法宝"
            self.命令对象 = 1
            self.命令参数 = self.选中道具
            鼠标动画.显示序列 = 4
            self.命令版面 = false
            self.法宝开关 = false
        end
    elseif self.法宝背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(KEY.RBUTTON) then
        self.法宝开关 = false
    end
end
function 战斗类:道具界面显示()
    self.道具背景:显示(250, 40)

    self.a = 0
    self.b = 0
    self.选中道具 = 0

    for n = 1, 20, 1 do
        self.a = n / 5

        if self.a < 1 then
            self.a = 1
        elseif self.a > 1 and self.a < 2 then
            self.a = 2
        elseif self.a > 2 and self.a < 3 then
            self.a = 3
        elseif self.a > 3 and self.a < 4 then
            self.a = 4
        end

        self.b = self.b + 1

        if self.b == 6 then
            self.b = 1
        end

        if self.物品数据[n] ~= nil then
            self.物品数据[n]:显示(270 + self.b * 50 - 50, 77 + self.a * 50 - 50)

            if self.物品数据[n].类型 ~= "药品" and self.物品数据[n].名称 ~= "醉生梦死" and self.物品数据[n].名称 ~= "珍露酒" then
                self.禁止字体:显示(270 + self.b * 50 - 50, 77 + self.a * 50 - 30, "不可用")
            end

            if self.物品数据[n]:获取焦点() and (self.物品数据[n].类型 == "药品" or self.物品数据[n].名称 == "醉生梦死" or self.物品数据[n].名称 == "珍露酒") then
                self.选中道具 = n
            end
        end
    end

    if self.选中道具 ~= 0 then
        self.物品数据[self.选中道具]:显示事件(2)

        if 引擎.鼠标弹起(左键) then
            self.命令类型 = "道具"
            self.命令对象 = 1
            self.命令参数 = self.选中道具
            鼠标动画.显示序列 = 4
            self.命令版面 = false
            self.道具开关 = false
        end
    elseif self.道具背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(KEY.RBUTTON) then
        self.道具开关 = false
    end
end

function 战斗类:法术界面显示()
  self.法术背景:显示(370,90)
  if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术~=nil then
    self.默认法术字体:置颜色(黄色):显示(420,365,self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术)
  else
    self.默认法术字体:置颜色(红色):显示(380,365,"（尚未设置默认法术）")
  end
  self.临时x=0
  self.临时y=0
  self.法术对象=0
  for n=1,#self.法术数据 do
    local 临时技能  = 引擎.取技能(self.法术数据[n].名称)
    local 法术图标={}
    法术图标[n]=tp.资源:载入(临时技能[6],"网易WDF动画",临时技能[7])
      法术图标[n]:显示(400+self.临时x*88,120+ self.临时y*43)
    self.临时x=self.临时x+1
      if(self.临时x==2)then
        self.临时x=0
        self.临时y=self.临时y+1
      end
      if 法术图标[n]:是否选中(鼠标.x,鼠标.y)then
          self.法术对象=n
          self.超丰富文本:清空()
          self.超丰富文本:添加文本([[#r/             ]]..self.法术数据[n].名称)
          self.超丰富文本:添加文本([[#y/]]..临时技能[1])
          self.超丰富文本:添加文本([[#g/消耗:]]..临时技能[4])
          if 临时技能[5]~=nil  then
              self.超丰富文本:添加文本([[#g/使用条件:]]..临时技能[5])
          end

          self.超丰富文本:添加文本([[#y/技能等级:]]..self.法术数据[n].等级)
          if 引擎.鼠标弹起(KEY.LBUTTON )  then
              if 取技能信息(self.法术数据[n].名称,"对象")==3  then
                  self.命令类型="技能"
                  self.命令对象=2
                  self.命令参数=self.法术数据[n].名称
                  鼠标动画.显示序列=4
                  self.命令版面=false
                  self.法术开关=false
              elseif 取技能信息(self.法术数据[n].名称,"对象")==2  then
                  self.命令类型="技能"
                  self.命令对象=1
                  self.命令参数=self.法术数据[n].名称
                  鼠标动画.显示序列=4
                  self.命令版面=false
                  self.法术开关=false
              elseif 取技能信息(self.法术数据[n].名称,"对象")==1  then
                  self.命令类型="技能"
                  self.命令对象=1
                  self.命令参数=self.法术数据[n].名称
                  鼠标动画.显示序列=4
                  self.命令版面=false
                  self.法术开关=false
                  self.操作单位[self.当前单位].类型=self.命令类型
                  self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
                  self.操作单位[self.当前单位].参数=self.命令参数
                  if self.当前单位==#self.操作单位 then
                      鼠标动画.显示序列=1
                      self.回合进程="等待回合"
                      客户端:发送数据(1,235,24,self:转文本(self.操作单位))
                  else
                      self.当前单位=self.当前单位+1
                      self.命令版面=true
                      self.捕捉开关=false
                      self.法术开关=false
                      self.命令类型="攻击"
                      self.命令对象=2
                      鼠标动画.显示序列=1
                  end
              end
          elseif 引擎.鼠标弹起(右键)  then
              if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术~=self.法术数据[n].名称 then
                  self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术=self.法术数据[n].名称
                  客户端:发送数据(self.操作单位[self.当前单位].编号,238,24,self.法术数据[n].名称)
              else
                  self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术=nil
                  客户端:发送数据(self.操作单位[self.当前单位].编号,238,24,"0")
              end
          end
      end
  end
  if self.法术对象~=0 then
    信息提示:显示(2,25,15,鼠标.x-80,鼠标.y+30)
    self.超丰富文本:显示(鼠标.x-77,鼠标.y+35)
  else
    if self.法术背景:取包围盒():检查点(鼠标.x,鼠标.y) and 引擎.鼠标弹起(KEY.RBUTTON) then
    self.法术开关=false
    end
  end
  end

function 战斗类:特技界面显示()
 local 装备特技={}
    装备特技.弱点击破={}
    装备特技.弱点击破.说明="以临时提高伤害力攻击对手,适于对付防御高者"
    装备特技.弱点击破.消耗=50
    装备特技.弱点击破.编号=37
    装备特技.弱点击破.对象=3
    装备特技.破血狂攻={}
    装备特技.破血狂攻.说明="狂性大发，可以连续攻击两次 "
    装备特技.破血狂攻.消耗=80
    装备特技.破血狂攻.编号=36
    装备特技.破血狂攻.对象=3
    装备特技.冥王暴杀={}
    装备特技.冥王暴杀.说明="减少对方HP，最小伤害为50 "
    装备特技.冥王暴杀.消耗=60
    装备特技.冥王暴杀.编号=40
    装备特技.冥王暴杀.对象=3
    装备特技.诅咒之伤={}
    装备特技.诅咒之伤.说明="减少敌方单人当前20%的HP "
    装备特技.诅咒之伤.消耗=40
    装备特技.诅咒之伤.编号=16
    装备特技.诅咒之伤.对象=3
    装备特技.太极护法={}
    装备特技.太极护法.说明="受到的法术伤害减半，效果为3回合"
    装备特技.太极护法.消耗=90
    装备特技.太极护法.编号=26
    装备特技.太极护法.对象=2
    装备特技.罗汉金钟={}
    装备特技.罗汉金钟.说明="全体队友受到的法术伤害减半，效果为3回合"
    装备特技.罗汉金钟.消耗=150
    装备特技.罗汉金钟.编号=27
    装备特技.罗汉金钟.对象=1
    装备特技.慈航普渡={}
    装备特技.慈航普渡.说明="复活我方全体队友"
    装备特技.慈航普渡.消耗=150
    装备特技.慈航普渡.编号=28
    装备特技.慈航普渡.对象=1
    装备特技.放下屠刀={}
    装备特技.放下屠刀.说明="降低敌人的物理伤害力，效果持续到战斗结束 "
    装备特技.放下屠刀.消耗=30
    装备特技.放下屠刀.编号=30
    装备特技.放下屠刀.对象=3
    装备特技.碎甲术 ={}
    装备特技.碎甲术.说明="降低敌方全体的物理防御力，效果持续到战斗结束 "
    装备特技.碎甲术.消耗=30
    装备特技.碎甲术.编号=30
    装备特技.碎甲术.对象=3
    装备特技.命归术={}
    装备特技.命归术.说明="恢复自身1/2的气血（最高不超过人物等级*20） "
    装备特技.命归术.消耗=130
    装备特技.命归术.编号=7
    装备特技.命归术.对象=1
    装备特技.气归术={}
    装备特技.气归术.说明="恢复自身1/4的气血（最高不超过人物等级*12） "
    装备特技.气归术.消耗=60
    装备特技.气归术.编号=6
    装备特技.气归术.对象=1
    装备特技.凝气决 ={}
    装备特技.凝气决.说明="恢复自身魔法(魔法上限*10%+150) "
    装备特技.凝气决 .消耗=60
    装备特技.凝气决 .编号=4
    装备特技.凝气决 .对象=1
    装备特技.凝神决 ={}
    装备特技.凝神决 .说明="恢复自身魔法(魔法上限*15%+250) "
    装备特技.凝神决 .消耗=90
    装备特技.凝神决 .编号=5
    装备特技.凝神决 .对象=1
    装备特技.命疗术 ={}
    装备特技.命疗术 .说明="恢复（上限9%+600）的气血，对单人使用 "
    装备特技.命疗术 .消耗=90
    装备特技.命疗术 .编号=3
    装备特技.命疗术 .对象=2
    装备特技.流云诀 ={}
    装备特技.流云诀 .说明="提升队友的速度，效果持续到战斗结束"
    装备特技.流云诀 .消耗=40
    装备特技.流云诀 .编号=43
    装备特技.流云诀 .对象=2
    装备特技.啸风诀 ={}
    装备特技.啸风诀 .说明="全体队友速度上升，效果持续到战斗结束"
    装备特技.啸风诀 .消耗=80
    装备特技.啸风诀 .编号=44
    装备特技.啸风诀 .对象=1
    装备特技.起死回生={}
    装备特技.起死回生 .说明="复活队友并回复其一半气血"
    装备特技.起死回生 .消耗=120
    装备特技.起死回生 .编号=45
    装备特技.起死回生 .对象=2
    装备特技.气疗术 ={}
    装备特技.气疗术 .说明="恢复（上限3%+200）的气血，对单人使用 "
    装备特技.气疗术 .消耗=30
    装备特技.气疗术 .编号=1
    装备特技.气疗术 .对象=2
    装备特技.野兽之力 ={}
    装备特技.野兽之力 .说明="提升队友的伤害力，效果持续到战斗结束 "
    装备特技.野兽之力 .消耗=40
    装备特技.野兽之力 .编号=20
    装备特技.野兽之力 .对象=2
    装备特技.魔兽之印 ={}
    装备特技.魔兽之印 .说明="提升我方全体的伤害力，效果持续到战斗结束 "
    装备特技.魔兽之印 .消耗=70
    装备特技.魔兽之印 .编号=21
    装备特技.魔兽之印 .对象=1
    装备特技.光辉之甲 ={}
    装备特技.光辉之甲 .说明="提升队友的防御，效果持续到战斗结束 "
    装备特技.光辉之甲 .消耗=40
    装备特技.光辉之甲 .编号=22
    装备特技.光辉之甲 .对象=2
    装备特技.圣灵之甲 ={}
    装备特技.圣灵之甲 .说明="提升我方全体的防御，效果持续到战斗结束 "
    装备特技.圣灵之甲 .消耗=80
    装备特技.圣灵之甲 .编号=23
    装备特技.圣灵之甲 .对象=1
    装备特技.四海升平 ={}
    装备特技.四海升平 .说明="恢复全体队友1/4的气血（最高不超过被恢复目标的等级*12） "
    装备特技.四海升平 .消耗=135
    装备特技.四海升平 .编号=8
    装备特技.四海升平 .对象=1
    装备特技.水清诀 ={}
    装备特技.水清诀 .说明="解除目标异常状态 "
    装备特技.水清诀 .消耗=50
    装备特技.水清诀 .编号=12
    装备特技.水清诀 .对象=2
    装备特技.玉清诀 ={}
    装备特技.玉清诀 .说明="解除全体队员异常状态 "
    装备特技.玉清诀 .消耗=125
    装备特技.玉清诀 .编号=14
    装备特技.玉清诀 .对象=2
    装备特技.冰清诀 ={}
    装备特技.冰清诀 .说明="解除目标当前异常状态并恢复目标4分之1的气血 "
    装备特技.冰清诀 .消耗=100
    装备特技.冰清诀 .编号=13
    装备特技.冰清诀 .对象=2
    装备特技.晶清诀 ={}
    装备特技.晶清诀 .说明="解除全体队员异常状态并恢复全体队员15%的气血 "
    装备特技.晶清诀 .消耗=150
    装备特技.晶清诀 .编号=15
    装备特技.晶清诀 .对象=2
 local 特技精灵={}
  for n=1,50 do
    特技精灵[n]=图像类("imge/008/"..944+n.."-1.png")
  end
 self.特技背景:显示(370,110)
 self.临时x=0
 self.临时y=0
 self.特技对象=0
 for n=1,#self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.特技数据 do
   self.特技名称=self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.特技数据[n]
   if self.特技名称==nil then return 0 end
   特技精灵[装备特技[self.特技名称].编号]:显示(405+self.临时x*61,142+self.临时y*60)
   if 特技精灵[装备特技[self.特技名称].编号]:取包围盒():检查点(鼠标.x,鼠标.y) then
     self.临时文本=[[
          #r/]]..self.特技名称..[[
  #y/]]..装备特技[self.特技名称].说明..[[
  #g/消耗愤怒:]]..装备特技[self.特技名称].消耗..[[
  #g/当前愤怒:]]..tp.屏幕.主角.数据.愤怒
      self.超丰富文本:清空()
      self.超丰富文本:添加文本(self.临时文本)
      self.特技对象=n
      if 引擎.鼠标弹起(KEY.LBUTTON )  then
       if 装备特技[self.特技名称].对象==3  then
         self.命令类型="特技"
         self.命令对象=2
         self.命令参数=self.特技名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.特技开关=false
        elseif 装备特技[self.特技名称].对象==2  then
         self.命令类型="特技"
         self.命令对象=1
         self.命令参数=self.特技名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.特技开关=false
         elseif 装备特技[self.特技名称].对象==1  then
         self.命令类型="特技"
         self.命令对象=1
         self.命令参数=self.特技名称
         鼠标动画.显示序列=4
         self.命令版面=false
         self.特技开关=false
         self.操作单位[self.当前单位].类型=self.命令类型
         self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
         self.操作单位[self.当前单位].参数=self.命令参数
          if self.当前单位==#self.操作单位 then
              鼠标动画.显示序列=1
              self.回合进程="等待回合"
              客户端:发送数据(1,235,24,self:转文本(self.操作单位))
          else
            self.当前单位=self.当前单位+1
            self.命令版面=true
            self.捕捉开关=false
            self.法术开关=false
            self.命令类型="攻击"
            self.命令对象=2
            鼠标动画.显示序列=1
            end
         end
         end
     end
    self.临时x=self.临时x+1
    if(self.临时x==2)then
      self.临时x=0
      self.临时y=self.临时y+1
      end
   end
  if self.特技对象~=0 then
    信息提示:显示(2,27,9,鼠标.x-80,鼠标.y+30)
    self.超丰富文本:显示(鼠标.x-77,鼠标.y+35)
  else
      if self.特技背景:取包围盒():检查点(鼠标.x,鼠标.y) and 引擎.鼠标弹起(KEY.RBUTTON) then
        self.特技开关=false
      end
  end
 end

function 战斗类:攻击事件()
    if self.执行流程 == 1 then
        self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x

        if self.战斗流程[1].保护数据 == nil then
            self.执行流程 = 2
        else
            self.执行流程 = 18
        end
    elseif self.执行流程 == 2 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 3

        if self.战斗流程[1].躲避 == false then
            self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(true)
        end
    elseif self.执行流程 == 3 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            if self.战斗流程[1].躲避 then
                self.参战数据[self.战斗流程[1].挨打方].单位:开启躲避()

                self.执行流程 = 4
            else
                if self.战斗流程[1].毒 then
                    self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画("毒")
                end

                if self.战斗流程[1].挨打数据.死亡 ~= 0 then
                    self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.2)
                    self.参战数据[self.战斗流程[1].挨打方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].挨打方].单位.动作, 0.2)
                end

                self.执行流程 = 8
            end
        end
    elseif self.执行流程 == 4 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 5
        end
    elseif self.执行流程 == 5 and self.参战数据[self.战斗流程[1].挨打方].单位.躲避开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
        self.执行流程 = 6
    elseif self.执行流程 == 6 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        self.执行流程 = 7
    elseif self.执行流程 == 7 then
        table.remove(self.战斗流程, 1)

        if #self.战斗流程 == 0 then
            self.回合进程 = "等待回合"

            客户端:发送数据(5, 234, 24, "1A")
        end
    elseif self.执行流程 == 8 then
        self.参战数据[self.战斗流程[1].挨打方].单位.动作 = self.战斗流程[1].挨打动作
        self.参战数据[self.战斗流程[1].挨打方].单位:播放音效(self.战斗流程[1].挨打动作)
        self.参战数据[self.战斗流程[1].挨打方].单位.动画:换方向(self.参战数据[self.战斗流程[1].挨打方].单位.动作, self.参战数据[self.战斗流程[1].挨打方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].挨打数据.死亡)
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].挨打数据.伤害, self.战斗流程[1].挨打数据.类型)

        if self.战斗流程[1].挨打动作 == "防御" then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("防御")
        end

        if self.战斗流程[1].必杀动作 ~= nil then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("必杀")
        end

        if self.战斗流程[1].金甲仙衣 or self.战斗流程[1].五彩娃娃 then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("金甲仙衣")
        end
        self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("被击中")

        if self.战斗流程[1].吸血 then
            self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].吸血伤害.伤害, "加血")
        end

        if self.战斗流程[1].反震数据 ~= nil then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "被击中"

            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("被击中")
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].反震数据.死亡)
            self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].反震数据.伤害, self.战斗流程[1].反震数据.类型)
            self.参战数据[self.战斗流程[1].攻击方].单位:添加特效动作("反震")

            self.执行流程 = 12
        elseif  self.战斗流程[1].混元伞数据 ~= nil then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "被击中"
            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("被击中")
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].混元伞数据.死亡)
            self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].混元伞数据.伤害, self.战斗流程[1].混元伞数据.类型)
            self.执行流程 = 12
        else
            self.执行流程 = 9
        end
    elseif self.执行流程 == 9 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 13
        end
    elseif self.执行流程 == 10 then
        if self.参战数据[self.战斗流程[1].挨打方].单位.击退开关 == false then
             if self.参战数据[self.战斗流程[1].攻击方].单位.死亡开关 == false then
                self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
                self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
                self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
                self.执行流程 = 11
            else
                self.执行流程 = 7
             end
        end
    elseif self.执行流程 == 11 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        self.执行流程 = 7
    elseif self.执行流程 == 12 and self.参战数据[self.战斗流程[1].攻击方].单位.击退开关 == false then
        self.执行流程 = 13
    elseif self.执行流程 == 13 then
        if self.战斗流程[1].反击数据 == nil then
            self.执行流程 = 10
        else
            self.执行流程 = 14
        end
    elseif self.执行流程 == 14 then
        self.参战数据[self.战斗流程[1].挨打方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].挨打方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].挨打方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].挨打方].单位.动画:换方向(self.参战数据[self.战斗流程[1].挨打方].单位.动作, self.参战数据[self.战斗流程[1].挨打方].单位.方向)

        if self.战斗流程[1].反击数据.死亡 ~= 0 then
            -- Nothing
        end

        self.执行流程 = 15
    elseif self.执行流程 == 15 then
        if self.参战数据[self.战斗流程[1].挨打方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].挨打方].单位.动作) == self.参战数据[self.战斗流程[1].挨打方].单位.动画[self.参战数据[self.战斗流程[1].挨打方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].挨打方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].挨打方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("攻击")

            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "被击中"

            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("被击中")
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].反击数据.死亡)
            self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].反击数据.伤害, self.战斗流程[1].反击数据.类型)
            self.参战数据[self.战斗流程[1].攻击方].单位:添加特效动作("被击中")

            self.执行流程 = 16
        end
    elseif self.执行流程 == 16 then
        if self.参战数据[self.战斗流程[1].挨打方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].挨打方].单位.动作) == self.参战数据[self.战斗流程[1].挨打方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].挨打方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].挨打方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].挨打方].单位.方向)

            self.执行流程 = 17
        end
    elseif self.执行流程 == 17 and self.参战数据[self.战斗流程[1].攻击方].单位.击退开关 == false then
        self.执行流程 = 10
    elseif self.执行流程 == 18 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动上限 = 20
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.x
        self.执行流程 = 19
    elseif self.执行流程 == 19 and self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 20
    elseif self.执行流程 == 20 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.执行流程 = 21
        end
    elseif self.执行流程 == 21 then
        self.参战数据[self.战斗流程[1].挨打方].单位.动作 = self.战斗流程[1].挨打动作

        self.参战数据[self.战斗流程[1].挨打方].单位:播放音效(self.战斗流程[1].挨打动作)
        self.参战数据[self.战斗流程[1].挨打方].单位.动画:换方向(self.参战数据[self.战斗流程[1].挨打方].单位.动作, self.参战数据[self.战斗流程[1].挨打方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].挨打数据.死亡)
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].挨打数据.伤害, self.战斗流程[1].挨打数据.类型)

        if self.战斗流程[1].挨打动作 == "防御" then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("防御")
        end

        if self.战斗流程[1].必杀动作 ~= nil then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("必杀")
        end

        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作 = "被击中"

        self.参战数据[self.战斗流程[1].保护数据.编号].单位:播放音效("被击中")
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动画:换方向(self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作, self.参战数据[self.战斗流程[1].保护数据.编号].单位.方向)
        self.参战数据[self.战斗流程[1].保护数据.编号].单位:开启击退(self.战斗流程[1].保护数据.死亡)
        self.参战数据[self.战斗流程[1].保护数据.编号].单位:设置掉血(self.战斗流程[1].保护数据.伤害, self.战斗流程[1].保护数据.类型)

        self.执行流程 = 22
    elseif self.执行流程 == 22 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 23
        end
    elseif self.执行流程 == 23 and self.参战数据[self.战斗流程[1].保护数据.编号].单位.击退开关 == false then
        if self.参战数据[self.战斗流程[1].保护数据.编号].单位.死亡开关 == false then
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.返回开关 = true
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].保护数据.编号].单位.初始xy.y
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].保护数据.编号].单位.初始xy.x
        end

        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动上限 = 90
        self.执行流程 = 10
        elseif self.执行流程==24 then
       for i=1,#self.战斗流程[1].挨打方 do
          self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:开启击退(self.战斗流程[1].挨打方[i].死亡)
           self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:设置掉血(self.战斗流程[1].挨打方[i].伤害,self.战斗流程[1].挨打方[i].类型)
           self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:播放音效("被击中")
       end
         self.执行流程=25
    elseif self.执行流程==25  then
        local js =0
       for i=1,#self.战斗流程[1].挨打方 do
            if self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.击退开关==false then
              js = js+1
            end
      end
       if js ==#self.战斗流程[1].挨打方 then
                table.remove(self.战斗流程,1)
               if #self.战斗流程==0 then
                    self.回合进程="等待回合"
                    客户端:发送数据(5,234,24,"1A")
             end
       end

    end
end

function 战斗类:群体法术动画设置(攻击方, 技能名称)

    if 技能名称 == "龙卷雨击" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "龙卷雨击"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xd61e2519)--3592299801.0)
            },
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xe926e4d8)--3911640280.0)
            },
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x56f9a205)--1459200517)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0xd1772aed)--3514247917.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        for n = 1, #self.群体法术.动画, 1 do
            self.群体法术.动画[n].动画.帧率 = 0.04
        end

        全局变量.地图开关 = false

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 300,
                y = 100
            }
            self.群体法术.坐标[2] = {
                x = 170,
                y = 230
            }
            self.群体法术.坐标[3] = {
                x = 270,
                y = 180
            }
            self.群体法术.坐标[4] = {
                x = 200,
                y = 250
            }
            self.群体法术.坐标[5] = {
                x = 300,
                y = 270
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 280
            }
            self.群体法术.坐标[2] = {
                x = 430,
                y = 380
            }
            self.群体法术.坐标[3] = {
                x = 670,
                y = 380
            }
            self.群体法术.坐标[4] = {
                x = 400,
                y = 350
            }
            self.群体法术.坐标[5] = {
                x = 500,
                y = 370
            }
        end
    elseif 技能名称 == "龙吟" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "龙吟"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xa5199709)--2769917705.0)
            },
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x5ec2d5bb)--1589827003)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0x3d74be12)--1031061010)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0x63605c8d)--1667259533)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0x9fe48849)--2682554441.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        for n = 1, #self.群体法术.动画, 1 do
            self.群体法术.动画[n].动画.帧率 = 0.04
        end

        全局变量.地图开关 = false

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 200,
                y = 200
            }
            self.群体法术.坐标[2] = {
                x = 400,
                y = 100
            }
            self.群体法术.坐标[3] = {
                x = 250,
                y = 200
            }
            self.群体法术.坐标[4] = {
                x = 200,
                y = 250
            }
            self.群体法术.坐标[5] = {
                x = 350,
                y = 100
            }
            self.群体法术.坐标[6] = {
                x = 300,
                y = 150
            }
            self.群体法术.坐标[7] = {
                x = 50,
                y = 300
            }
        else
            self.群体法术.坐标[1] = {
                x = 570,
                y = 350
            }
            self.群体法术.坐标[2] = {
                x = 600,
                y = 200
            }
            self.群体法术.坐标[3] = {
                x = 550,
                y = 300
            }
            self.群体法术.坐标[4] = {
                x = 570,
                y = 400
            }
            self.群体法术.坐标[5] = {
                x = 600,
                y = 300
            }
            self.群体法术.坐标[6] = {
                x = 670,
                y = 300
            }
            self.群体法术.坐标[7] = {
                x = 550,
                y = 350
            }
        end
    elseif 技能名称 == "飞砂走石" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "飞砂走石"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x4DB67680) --0x335A5A78
            },
            -- {
            --     更新 = false,
            --     动画 = WAS动画类.创建(magic, 0x335A5A78)
            -- },
            -- {
            --     更新 = false,
            --     动画 = WAS动画类.创建(magic, 0x335A5A78)
            -- }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        for n = 1, #self.群体法术.动画, 1 do
            self.群体法术.动画[n].动画.帧率 = 0.06
        end

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 300 - 150,
                y = 300 - 250
            }
            -- self.群体法术.坐标[2] = {
            --     x = 300,
            --     y = 180
            -- }
            -- self.群体法术.坐标[3] = {
            --     x = 200,
            --     y = 300
            -- }
        else
            self.群体法术.坐标[1] = {
                x = 500,
                y = 400 - 100
            }
            -- self.群体法术.坐标[2] = {
            --     x = 600,
            --     y = 470
            -- }
            -- self.群体法术.坐标[3] = {
            --     x = 670,
            --     y = 350
            -- }
            -- self.群体法术.坐标[4] = {
            --     x = 400,
            --     y = 350
            -- }
            -- self.群体法术.坐标[5] = {
            --     x = 500,
            --     y = 370
            -- }
        end
    elseif 技能名称 == "泰山压顶" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "泰山压顶"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic2, 0x783f9d20)--2017434912) --0x783f9d20
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic2, 0x783f9d20)--2017434912) --0x783f9d20 0xf54c4025
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 200,
                y = 100
            }
            self.群体法术.坐标[2] = {
                x = 200,
                y = 220
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 200
            }
            self.群体法术.坐标[2] = {
                x = 600,
                y = 400
            }
        end
    elseif 技能名称 == "二龙戏珠" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "二龙戏珠"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xa680a821)--2793449505.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 200,
                y = 220
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
        end
    elseif 技能名称 == "落叶萧萧" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "落叶萧萧"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x335A5A78)--2793449505.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 200,
                y = 220
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
        end
    elseif 技能名称 == "破釜沉舟" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "破釜沉舟"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x78d6fd06)--2027355398)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 220,
                y = 200
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
        end
    elseif 技能名称 == "奔雷咒" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "奔雷咒"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xf54c4025)--4115415077.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 220,
                y = 200
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
        end
          elseif 技能名称 == "九天玄火" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "九天玄火"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xf54c4025)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 220,
                y = 200
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
        end
    elseif 技能名称 == "地狱烈火" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "地狱烈火"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0xbe325d99)--3190971801.0)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0xbe325d99)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0xbe325d99)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}
        self.群体法术.动画[1].动画.帧率 = 0.08
        self.群体法术.动画[2].动画.帧率 = 0.08
        self.群体法术.动画[3].动画.帧率 = 0.08

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 200,
                y = 250
            }
            self.群体法术.坐标[2] = {
                x = 380,
                y = 200
            }
            self.群体法术.坐标[3] = {
                x = 560,
                y = 170
            }
        else
            self.群体法术.坐标[1] = {
                x = 400,
                y = 450
            }
            self.群体法术.坐标[2] = {
                x = 580,
                y = 400
            }
            self.群体法术.坐标[3] = {
                x = 700,
                y = 370
            }
        end
    elseif 技能名称 == "水漫金山" then
        self.群体法术.开关 = true
        self.群体法术.名称 = "水漫金山"
        self.群体法术.类型 = 2
        self.群体法术.时间 = 引擎:取游戏时间()
        self.群体法术.动画 = {
            {
                更新 = true,
                动画 = WAS动画类.创建(magic, 0x97311bad)--2536577965.0)
            },
            {
                更新 = false,
                动画 = WAS动画类.创建(magic, 0x90e8589f)--2431146143.0)
            }
        }
        self.群体法术.伤害 = false
        self.群体法术.坐标 = {}
        self.群体法术.动画[1].动画.帧率 = 0.08
        self.群体法术.动画[2].动画.帧率 = 0.08

        if 攻击方.单位.敌我 == 1 then
            self.群体法术.坐标[1] = {
                x = 250,
                y = 130
            }
            self.群体法术.坐标[2] = {
                x = 250,
                y = 130
            }
        else
            self.群体法术.坐标[1] = {
                x = 600,
                y = 400
            }
            self.群体法术.坐标[2] = {
                x = 400,
                y = 450
            }
        end
    end
end

function 战斗类:群体法术动画显示()
    if self.群体法术.名称 == "龙卷雨击" and 1 == 1 then
        self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)
        self.群体法术.动画[1].动画:显示(self.群体法术.坐标[2].x, self.群体法术.坐标[2].y)
        self.群体法术.动画[2].动画:显示(self.群体法术.坐标[3].x, self.群体法术.坐标[3].y)

        if self.群体法术.动画[3].动画.当前帧 == 32 then
            self.群体法术.动画[4].更新 = true
        elseif self.群体法术.动画[3].动画.当前帧 == self.群体法术.动画[3].动画.结束帧 and self.群体法术.动画[3].更新 then
            self.群体法术.动画[3].更新 = false
        elseif self.群体法术.动画[3].更新 then
            self.群体法术.动画[3].动画:显示(self.群体法术.坐标[4].x, self.群体法术.坐标[4].y)
        end

        if self.群体法术.动画[4].更新 then
            self.群体法术.动画[4].动画:显示(self.群体法术.坐标[5].x, self.群体法术.坐标[5].y)

            if self.群体法术.动画[4].动画.当前帧 == 25 then
                self.群体法术.伤害 = true
            elseif self.群体法术.动画[4].动画.当前帧 == self.群体法术.动画[4].动画.结束帧 then
                self.群体法术.开关 = false
                self.群体法术.动画 = {}
                全局变量.地图开关 = true
            end
        end
    elseif self.群体法术.名称 == "龙吟" then
        self.群体法术.动画[2].动画:显示(self.群体法术.坐标[2].x, self.群体法术.坐标[2].y)
        self.群体法术.动画[2].动画:显示(self.群体法术.坐标[4].x, self.群体法术.坐标[4].y)
        self.群体法术.动画[2].动画:显示(self.群体法术.坐标[7].x, self.群体法术.坐标[7].y)

        if self.群体法术.动画[1].更新 then
            self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

            if self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
                self.群体法术.动画[1].更新 = false
                self.群体法术.动画[3].更新 = true
            end
        elseif self.群体法术.动画[3].更新 then
            self.群体法术.动画[3].动画:显示(self.群体法术.坐标[4].x, self.群体法术.坐标[4].y)

            if self.群体法术.动画[3].动画.当前帧 == 18 then
                self.群体法术.动画[4].更新 = true
                self.群体法术.动画[5].更新 = true
            elseif self.群体法术.动画[3].动画.当前帧 == self.群体法术.动画[3].动画.结束帧 then
                self.群体法术.动画[3].更新 = false
            end
        end

        if self.群体法术.动画[4].更新 then
            self.群体法术.动画[4].动画:显示(self.群体法术.坐标[4].x, self.群体法术.坐标[4].y)
            self.群体法术.动画[4].动画:显示(self.群体法术.坐标[5].x, self.群体法术.坐标[5].y)
        end

        if self.群体法术.动画[5].更新 then
            self.群体法术.动画[5].动画:显示(self.群体法术.坐标[6].x, self.群体法术.坐标[6].y)

            if self.群体法术.动画[5].动画.当前帧 == 10 then
                self.群体法术.伤害 = true
            elseif self.群体法术.动画[5].动画.当前帧 == 20 then
                self.群体法术.开关 = false
                self.群体法术.动画 = {}
                全局变量.地图开关 = true
            end
        end
    elseif self.群体法术.名称 == "飞砂走石" then
        -- if self.群体法术.动画[1].更新 then
            self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)
        -- end

        -- if self.群体法术.动画[1].动画.当前帧 == 5 then
        --     self.群体法术.动画[2].更新 = true
        -- elseif self.群体法术.动画[1].动画.当前帧 == 7 then
        --     self.群体法术.动画[3].更新 = true
        -- elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 and self.群体法术.动画[1].更新 then
        --     self.群体法术.动画[1].更新 = false
        -- end

        -- if self.群体法术.动画[2].更新 then
        --     self.群体法术.动画[2].动画:显示(self.群体法术.坐标[2].x, self.群体法术.坐标[2].y)
        -- end

        -- if self.群体法术.动画[3].更新 then
        --     self.群体法术.动画[3].动画:显示(self.群体法术.坐标[3].x, self.群体法术.坐标[3].y)

        --     if self.群体法术.动画[3].动画.当前帧 == 20 then
        --         self.群体法术.伤害 = true
        --     elseif self.群体法术.动画[3].动画.当前帧 == self.群体法术.动画[3].动画.结束帧 then
        --         self.群体法术.开关 = false
        --         self.群体法术.动画 = {}
        --     end
        -- end
        if self.群体法术.动画[1].动画.当前帧 == 10 then
            self.群体法术.伤害 = true
        elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
            self.群体法术.动画 = {}
            self.群体法术.开关 = false
        end
    elseif self.群体法术.名称 == "二龙戏珠" then
        self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

        if self.群体法术.动画[1].动画.当前帧 == 15 then
            self.群体法术.伤害 = true
        elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
            self.群体法术.动画 = {}
            self.群体法术.开关 = false
        end
    elseif self.群体法术.名称 == "落叶萧萧" then
        self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

        if self.群体法术.动画[1].动画.当前帧 == 10 then
            self.群体法术.伤害 = true
        elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
            self.群体法术.动画 = {}
            self.群体法术.开关 = false
        end
    elseif self.群体法术.名称 == "泰山压顶" then
        -- print("泰山压顶 群体法术动画显示")
        if self.群体法术.动画[1].更新 then
            if self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 and self.群体法术.动画[1].更新 then
                self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

                self.群体法术.动画[1].更新 = false
                self.群体法术.动画[2].更新 = true
                self.临时时间 = os.time()
            end
        elseif self.群体法术.动画[2].更新 then
            if os.time() - self.临时时间 < 0.3 then
                self.群体法术.动画[2].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)
            else
                self.群体法术.动画[2].动画:显示(self.群体法术.坐标[2].x, self.群体法术.坐标[2].y)

                self.群体法术.伤害 = true

                if os.time() - self.临时时间 > 1 then
                    self.群体法术.动画 = {}
                    self.群体法术.开关 = false
                end
            end
        end
    elseif self.群体法术.名称 == "奔雷咒" then
        if self.群体法术.动画[1].更新 then
            self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

            if self.群体法术.动画[1].动画.当前帧 == 10 then
                self.群体法术.伤害 = true
            elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
                self.群体法术.动画 = {}
                self.群体法术.开关 = false
            end
        end
    elseif self.群体法术.名称 == "破釜沉舟" then
        if self.群体法术.动画[1].更新 then
            self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

            if self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
                self.群体法术.动画 = {}
                self.群体法术.开关 = false
            end
        end
    elseif self.群体法术.名称 == "地狱烈火" then
        if self.群体法术.动画[1].更新 then
            self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

            if self.群体法术.动画[1].动画.当前帧 == 5 then
                self.群体法术.动画[2].更新 = true
            elseif self.群体法术.动画[1].动画.当前帧 == 10 then
                self.群体法术.动画[3].更新 = true
            elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
                self.群体法术.动画[1].更新 = false
            end
        end

        if self.群体法术.动画[2].更新 then
            self.群体法术.动画[2].动画:显示(self.群体法术.坐标[2].x, self.群体法术.坐标[2].y)

            if self.群体法术.动画[2].动画.当前帧 == self.群体法术.动画[2].动画.结束帧 then
                self.群体法术.动画[2].更新 = false
            end
        end

        if self.群体法术.动画[3].更新 then
            self.群体法术.动画[3].动画:显示(self.群体法术.坐标[3].x, self.群体法术.坐标[3].y)

            if self.群体法术.动画[3].动画.当前帧 == 5 then
                self.群体法术.伤害 = true
            elseif self.群体法术.动画[3].动画.当前帧 == self.群体法术.动画[3].动画.结束帧 then
                self.群体法术.动画 = {}
                self.群体法术.开关 = false
            end
        end
    elseif self.群体法术.名称 == "水漫金山" and self.群体法术.动画[1].更新 then
        self.群体法术.动画[1].动画:显示(self.群体法术.坐标[1].x, self.群体法术.坐标[1].y)

        if self.群体法术.动画[1].动画.当前帧 == 25 then
            self.群体法术.伤害 = true
        elseif self.群体法术.动画[1].动画.当前帧 == 30 then
            -- Nothing
        elseif self.群体法术.动画[1].动画.当前帧 == self.群体法术.动画[1].动画.结束帧 then
            self.群体法术.动画[1].更新 = false
            self.群体法术.动画 = {}
            self.群体法术.开关 = false
        end
    end
end

function 战斗类:退出战斗()
 if self.窗口.自动栏.可视化 then
        self.窗口.自动栏:打开()
 end

  self.单位总数=nil
  self.临时数据=nil
  self.参战数据=nil
    全局变量.地图开关 = true
    yxjc = 2

    if self.战斗音乐 ~= nil then
        self.战斗音乐:停止()
    end

    if tp.地图.临时数据.音乐 then
        创建音乐(tp.地图.临时数据.音乐)
    end
    -- if tp.地图.地图音乐 ~= nil then
    --     tp.地图.地图音乐:播放()
    -- end
    collectgarbage("collect")
end

function 战斗类:技能事件()
    if self.执行流程 == 1 then
        self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x
        self.执行流程 = 2
    elseif self.执行流程 == 2 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        if self.战斗流程[1].动作[1].保护数据 == nil then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

            self.执行流程 = 3
        else
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动开关 = true
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动上限 = 20
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.y
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.x
            self.执行流程 = 201
        end
    elseif self.执行流程 == 201 and self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 3
    elseif self.执行流程 == 3 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            播放技能音效(self.战斗流程[1].参数)

            if self.战斗流程[1].参数 ~= "后发制人" then
                self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)

                if self.战斗流程[1].参数 == "象形" then
                    self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画(self.战斗流程[1].参数)
                    if self.战斗流程[1].变身解除 ~= 1 then
                     self.参战数据[self.战斗流程[1].攻击方].单位.法术状态组.变身 = nil
                    end
                end
            end

            self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].动作[1].死亡)
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].动作[1].普通伤害, self.战斗流程[1].动作[1].伤害类型)
            self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("被击中")
          if self.战斗流程[1].吸血 then


         self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].吸血伤害.伤害,"加血")

         end
            if self.战斗流程[1].动作[1].保护数据 ~= nil then
                self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.动作 = "被击中"

                self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位:播放音效("被击中")
                self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.动画:换方向(self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.动作, self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.方向)
                self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位:开启击退(self.战斗流程[1].动作[1].保护数据.死亡)
                self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位:设置掉血(self.战斗流程[1].动作[1].保护数据.伤害, self.战斗流程[1].动作[1].保护数据.类型)
            end

            if self.战斗流程[1].动作[1].必杀动作 ~= "" then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作(self.战斗流程[1].动作[1].必杀动作)
            end

            if self.战斗流程[1].动作[1].金甲仙衣 or self.战斗流程[1].动作[1].五彩娃娃 then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("金甲仙衣")
            end
            if self.战斗流程[1].动作[1].反震动作 ~= "" then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作(self.战斗流程[1].动作[1].反震动作)
            end

            if self.战斗流程[1].防御动作 ~= nil then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作(self.战斗流程[1].挨打动作)
                self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("防御")
            end

            if self.战斗流程[1].动作[1].反震 then
                self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].动作[1].反震死亡)
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].动作[1].反震伤害, "掉血")
                self.执行流程 = 4
            elseif self.战斗流程[1].动作[1].混元伞 ~= nil and self.战斗流程[1].动作[1].混元伞 then
                self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].动作[1].混元伞死亡)
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].动作[1].混元伞伤害, "掉血")
                self.执行流程 = 4
            else
                self.执行流程 = 5
            end
        end
    elseif self.执行流程 == 4 and self.参战数据[self.战斗流程[1].攻击方].单位.击退开关 == false then
        if self.战斗流程[1].动作[1].反震死亡 ~= 0 then
            self.执行流程 = 29
        elseif self.战斗流程[1].动作[1].混元伞死亡 ~= nil and self.战斗流程[1].动作[1].混元伞死亡 ~= 0 then
            self.执行流程 = 29
        else
            self.执行流程 = 5
        end
    elseif self.执行流程 == 5 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位.技能动画.开关 = false
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            if self.战斗流程[1].动作[1].保护数据 == nil then
                self.执行流程 = 6
            else
                self.执行流程 = 202
            end
        end
    elseif self.执行流程 == 202 and self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.击退开关 == false then
        self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动上限 = 90

        if self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.死亡开关 == false then
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.返回开关 = true
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.初始xy.y
            self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.初始xy.x
            self.执行流程 = 203
        else
            self.执行流程 = 6
        end
    elseif self.执行流程 == 203 and self.参战数据[self.战斗流程[1].动作[1].保护数据.编号].单位.返回开关 == false then
        self.执行流程 = 6
    elseif self.执行流程 == 6 then
        table.remove(self.战斗流程[1].动作, 1)

        if #self.战斗流程[1].动作 == 0 then
            self.执行流程 = 7

            if self.战斗流程[1].消耗 ~= nil then
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].消耗, "掉血")
            end
        else
            self.执行流程 = 2
        end
    elseif self.执行流程 == 7 and self.参战数据[self.战斗流程[1].攻击方].单位.掉血开关 == false then
        if self.战斗流程[1].参数 == "后发制人" or self.战斗流程[1].参数 == "连环击" then
            self.参战数据[self.战斗流程[1].攻击方].单位.法术状态组[self.战斗流程[1].参数] = nil
        elseif self.战斗流程[1].参数 == "横扫千军" and self.战斗流程[1].横扫千军休息 ~= 1 then
            self.参战数据[self.战斗流程[1].攻击方].单位:添加状态动画(self.战斗流程[1].参数)
        end

        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
        self.执行流程 = 8
    elseif self.执行流程 == 8 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 9 then
        self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.显示xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.显示xy.x
        self.执行流程 = 10
    elseif self.执行流程 == 10 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        if self.战斗流程[1].挨打方[1].保护数据 == nil then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

            self.参战数据[self.战斗流程[1].攻击方].单位.动作复位 = true
            self.执行流程 = 11
        else
            self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动开关 = true
            self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动上限 = 20
            self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.显示xy.y
            self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.显示xy.x
            self.执行流程 = 204
        end
    elseif self.执行流程 == 204 and self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 11
    elseif self.执行流程 == 11 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            播放技能音效(self.战斗流程[1].参数)
            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")
            self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:增加技能动画(self.战斗流程[1].参数)
            self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:开启击退(self.战斗流程[1].挨打方[1].死亡)
            self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:设置掉血(self.战斗流程[1].挨打方[1].伤害, self.战斗流程[1].挨打方[1].类型)
            self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:播放音效("被击中")
            self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

            if self.战斗流程[1].挨打方[1].必杀动作 ~= "" then
                self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:添加特效动作(self.战斗流程[1].挨打方[1].必杀动作)
            end

            if self.战斗流程[1].挨打方[1].反震动作 ~= "" then
                self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:添加特效动作(self.战斗流程[1].挨打方[1].反震动作)
            end
            if self.战斗流程[1].挨打方[1].五彩娃娃 or self.战斗流程[1].挨打方[1].金甲仙衣 then
                self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:添加特效动作("金甲仙衣")
            end
            if self.战斗流程[1].挨打方[1].防御动作 ~= "" then
                self.参战数据[self.战斗流程[1].挨打方[1].编号].单位:添加特效动作("金甲仙衣")
            end

            if self.战斗流程[1].挨打方[1].保护数据 ~= nil then
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.动作 = "被击中"

                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位:播放音效("被击中")
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.动画:换方向(self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.动作, self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.方向)
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位:开启击退(self.战斗流程[1].挨打方[1].保护数据.死亡)
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位:设置掉血(self.战斗流程[1].挨打方[1].保护数据.伤害, self.战斗流程[1].挨打方[1].保护数据.类型)
            end

            if self.战斗流程[1].挨打方[1].反震 then
                self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].挨打方[1].反震死亡)
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].挨打方[1].反震伤害, "掉血")

                self.执行流程 = 12
            elseif self.战斗流程[1].挨打方[1].混元伞 ~= nil and  self.战斗流程[1].挨打方[1].混元伞 then
                self.参战数据[self.战斗流程[1].攻击方].单位:开启击退(self.战斗流程[1].挨打方[1].混元伞死亡)
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].挨打方[1].混元伞伤害, "掉血")
                self.执行流程 = 12
            else
                self.执行流程 = 205
            end
        end
    elseif self.执行流程 == 12 and self.参战数据[self.战斗流程[1].攻击方].单位.击退开关 == false then
        if self.战斗流程[1].挨打方[1].反震死亡 ~= 0 then
            self.执行流程 = 29
        elseif self.战斗流程[1].挨打方[1].混元伞死亡 ~= nil and  self.战斗流程[1].挨打方[1].混元伞死亡 ~= 0 then
            self.执行流程 = 29
        else
            self.执行流程 = 13
        end
    elseif self.执行流程 == 205 and self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.击退开关 == false then
        if self.战斗流程[1].挨打方[1].保护数据 == nil then
            self.执行流程 = 13
        else
            self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动上限 = 90

            if self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.死亡开关 == false then
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.返回开关 = true
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.初始xy.y
                self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.初始xy.x
                self.执行流程 = 206
            else
                self.执行流程 = 13
            end
        end
    elseif self.执行流程 == 206 and self.参战数据[self.战斗流程[1].挨打方[1].保护数据.编号].单位.返回开关 == false then
        self.执行流程 = 13
    elseif self.执行流程 == 13 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.死亡开关 == false then
            -- Nothing
        end

        table.remove(self.战斗流程[1].挨打方, 1)

        if #self.战斗流程[1].挨打方 == 0 then
            if self.战斗流程[1].参数 == "破釜沉舟" then
                self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].消耗, "掉血")
            end

            self.执行流程 = 14
        else
            self.执行流程 = 16
        end
    elseif self.执行流程 == 14 and self.参战数据[self.战斗流程[1].攻击方].单位.掉血开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
        self.执行流程 = 15
    elseif self.执行流程 == 15 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        if self.战斗流程[1].参数 == "鹰击" and self.战斗流程[1].状态取消 ~= 1 then
            self.参战数据[self.战斗流程[1].攻击方].单位:添加状态动画(self.战斗流程[1].参数)
        elseif self.战斗流程[1].参数 == "破釜沉舟" then
            self.参战数据[self.战斗流程[1].攻击方].单位:添加状态动画("破釜沉舟")
        end

        self.执行流程 = 29
    elseif self.执行流程 == 16 then
        if self.战斗流程[1].参数 == "鹰击" then
            self.执行流程 = 17
        else
            self.执行流程 = 9
        end
    elseif self.执行流程 == 17 then
        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
        self.执行流程 = 18
    elseif self.执行流程 == 18 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        self.执行流程 = 9
    elseif self.执行流程 == 29 then
        table.remove(self.战斗流程, 1)

        if #self.战斗流程 == 0 then
            self.回合进程 = "等待回合"

            客户端:发送数据(5, 234, 24, "1A")
        end
    elseif self.执行流程 == 30 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(true)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        if self.战斗流程[1].法术暴击 then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("法暴")
        end

        self.执行流程 = 31
    elseif self.执行流程 == 31 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(false)
            self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].法术死亡)
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, self.战斗流程[1].伤害类型)
            self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("被击中")

            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 32
        end
    elseif self.执行流程 == 32 and self.参战数据[self.战斗流程[1].挨打方].单位.技能动画.开关 == false and self.参战数据[self.战斗流程[1].挨打方].单位.击退开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 33 then
        self.执行流程 = 34
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        for n = 1, #self.战斗流程[1].挨打方, 1 do
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:增加技能动画(self.战斗流程[1].参数)
        end
    elseif self.执行流程 == 34 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            for n = 1, #self.战斗流程[1].挨打方, 1 do
                self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:开启击退(self.战斗流程[1].挨打方[n].死亡)
                self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:设置掉血(self.战斗流程[1].挨打方[n].伤害, self.战斗流程[1].挨打方[n].类型)
                self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:播放音效("被击中")
                if self.战斗流程[1].挨打方[n].法术暴击 then
              self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:添加特效动作("法暴")
             end
                 if self.参战数据[self.战斗流程[1].挨打方[n].编号].咒术 ~= nil then
                     self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:添加状态动画("毒")
                end
            end

            self.执行流程 = 38
        end
    elseif self.执行流程 == 35 then
        self.执行流程 = 36
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        self:群体法术动画设置(self.参战数据[self.战斗流程[1].攻击方], self.战斗流程[1].参数)
    elseif self.执行流程 == 36 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 37
        end
    elseif self.执行流程 == 37 and self.群体法术.伤害 then
        for n = 1, #self.战斗流程[1].挨打方, 1 do
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:开启击退(self.战斗流程[1].挨打方[n].死亡)
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:设置掉血(self.战斗流程[1].挨打方[n].伤害, self.战斗流程[1].挨打方[n].类型)
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:播放音效("被击中")
         if self.战斗流程[1].挨打方[n].法术暴击 then
          self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:添加特效动作("法暴")
         end
        end

        self.执行流程 = 38
    elseif self.执行流程 == 38 then
        self.击退结束 = true

        for n = 1, #self.战斗流程[1].挨打方, 1 do
            if self.参战数据[self.战斗流程[1].挨打方[n].编号].单位.击退开关 then
                self.击退结束 = false
            end
        end

        if self.击退结束 then
            self.执行流程 = 299
        end
    elseif self.执行流程 == 299 and self.群体法术.开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 39 then
        self.执行流程 = 40
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        播放技能音效(self.战斗流程[1].参数)
    elseif self.执行流程 == 40 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 41
        end
    elseif self.执行流程 == 41 and self.参战数据[self.战斗流程[1].挨打方].单位.技能动画.开关 == false then
        if self.战斗流程[1].封印结果 then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画(self.战斗流程[1].参数)
            if self.战斗流程[1].毒 then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画("毒")
            end
        end

        self.执行流程 = 29
    elseif self.执行流程 == 42 then
        self.执行流程 = 43
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(true)
        播放技能音效(self.战斗流程[1].参数)
    elseif self.执行流程 == 43 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            if self.战斗流程[1].封印结果 then
                self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(false)
                self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].死亡)
                self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].伤害, "掉血")
            end

            self.执行流程 = 45
        end
    elseif self.执行流程 == 45 and self.参战数据[self.战斗流程[1].挨打方].单位.技能动画.开关 == false then
        if self.战斗流程[1].封印结果 then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画(self.战斗流程[1].参数)
        end

        self.执行流程 = 29
    elseif self.执行流程 == 46 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        self.执行流程 = 47
    elseif self.执行流程 == 47 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, self.战斗流程[1].伤害类型)

            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 48
        end
    elseif self.执行流程 == 48 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        if self.战斗流程[1].参数 == "普渡众生" then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画(self.战斗流程[1].参数)
        elseif self.战斗流程[1].参数 == "冰清诀" then
            self.参战数据[self.战斗流程[1].挨打方].单位:解除异常状态()
        end

        self.执行流程 = 29
    elseif self.执行流程 == 49 then
        self.执行流程 = 50
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)

        for n = 1, #self.战斗流程[1].挨打方, 1 do
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:增加技能动画(self.战斗流程[1].参数)
        end
    elseif self.执行流程 == 50 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            for n = 1, #self.战斗流程[1].挨打方, 1 do
                if self.战斗流程[1].挨打方[n].伤害 ~= 0 then
                    self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:设置掉血(self.战斗流程[1].挨打方[n].伤害, self.战斗流程[1].挨打方[n].类型)
                end
            end

            self.执行流程 = 51
        end
    elseif self.执行流程 == 51 and self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.掉血开关 == false then
        if self.战斗流程[1].参数 == "生命之泉" or self.战斗流程[1].参数 == "圣灵之甲" or self.战斗流程[1].参数 == "罗汉金钟" then
            for n = 1, #self.战斗流程[1].挨打方, 1 do
                self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:添加状态动画(self.战斗流程[1].参数)
            end
        elseif self.战斗流程[1].参数 == "玉清诀" or self.战斗流程[1].参数 == "晶清诀" then
            for n = 1, #self.战斗流程[1].挨打方, 1 do
                self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:解除异常状态()
            end
        end

        self.执行流程 = 29
    elseif self.执行流程 == 52 then
        self.执行流程 = 53
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        for n = 1, #self.战斗流程[1].挨打方, 1 do
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:增加技能动画(self.战斗流程[1].参数)
        end
    elseif self.执行流程 == 53 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.执行流程 = 54
        end
    elseif self.执行流程 == 54 and self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.技能动画.开关 == false then
        for n = 1, #self.战斗流程[1].挨打方, 1 do
           if self.战斗流程[1].参数 == "金刚护法"  then
               self.参战数据[self.战斗流程[1].挨打方[n].编号].单位.闪光模式.开关=true
          elseif  self.战斗流程[1].挨打方[n].结果 then
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:添加状态动画(self.战斗流程[1].参数)
            end

        end
        if self.战斗流程[1].参数 == "分身术" then
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:置颜色(self.参战数据[self.战斗流程[1].攻击方].单位.动作, ARGB(105, 170, 170, 170))
            self.参战数据[self.战斗流程[1].攻击方].单位.分身 = true

        end

        self.执行流程 = 29
    elseif self.执行流程 == 55 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)

        self.执行流程 = 56
    elseif self.执行流程 == 56 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, self.战斗流程[1].伤害类型)

            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"
            self.参战数据[self.战斗流程[1].挨打方].单位.动作 = "待战"
            self.参战数据[self.战斗流程[1].挨打方].单位.动画.按帧更新 = false
            self.参战数据[self.战斗流程[1].挨打方].单位.倒地开关 = false

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 57
        end
    elseif self.执行流程 == 57 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 58 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)

        self.执行流程 = 59
    elseif self.执行流程 == 59 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            if self.战斗流程[1].封印结果 then
                if self.战斗流程[1].参数 == "勾魂" then
                    self.执行流程 = 60
                elseif self.战斗流程[1].参数 == "摄魄" then
                    self.执行流程 = 62
                end
            else
                self.执行流程 = 29
            end
        end
    elseif self.执行流程 == 60 then
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].死亡)
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, "掉血")
        self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].恢复伤害, "加血")

        self.执行流程 = 61
    elseif self.执行流程 == 61 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 62 then
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].死亡)

        self.执行流程 = 63
    elseif self.执行流程 == 63 and self.参战数据[self.战斗流程[1].挨打方].单位.击退开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 64 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)

        self.执行流程 = 66
    elseif self.执行流程 == 66 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 68
        end
    elseif self.执行流程 == 68 then
        if self.战斗流程[1].保护数据 == nil then
            self.执行流程 = 69
        else
            self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 = true
            self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y
            self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x
            self.执行流程 = 75
        end
    elseif self.执行流程 == 69 then
        self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.显示xy.x
        self.执行流程 = 70
    elseif self.执行流程 == 70 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 71
    elseif self.执行流程 == 71 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].法术死亡)
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, "掉血")
            self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("被击中")

            self.执行流程 = 72

            if self.战斗流程[1].必杀动作 ~= nil then
                self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("必杀")
            end
        end
    elseif self.执行流程 == 72 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 73
        end
    elseif self.执行流程 == 73 then
        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x
        self.执行流程 = 74
    elseif self.执行流程 == 74 and self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 75 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动开关 = true
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动上限 = 20
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].挨打方].单位.初始xy.x
        self.执行流程 = 76
    elseif self.执行流程 == 76 and self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "攻击"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:置帧率(self.参战数据[self.战斗流程[1].攻击方].单位.动作, 0.1)
        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("攻击")

        self.执行流程 = 77
    elseif self.执行流程 == 77 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画[self.参战数据[self.战斗流程[1].攻击方].单位.动作]:取中间() + self.参战数据[self.战斗流程[1].攻击方].单位.动画:取开始帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.执行流程 = 78
        end
    elseif self.执行流程 == 78 then
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].法术死亡)
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].法术伤害, "掉血")
        self.参战数据[self.战斗流程[1].挨打方].单位:播放音效("被击中")
        self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("被击中")

        if self.战斗流程[1].必杀动作 ~= nil then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加特效动作("必杀")
        end

        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作 = "被击中"

        self.参战数据[self.战斗流程[1].保护数据.编号].单位:播放音效("被击中")
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动画:换方向(self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作, self.参战数据[self.战斗流程[1].保护数据.编号].单位.方向)
        self.参战数据[self.战斗流程[1].保护数据.编号].单位:开启击退(self.战斗流程[1].保护数据.死亡)
        self.参战数据[self.战斗流程[1].保护数据.编号].单位:设置掉血(self.战斗流程[1].保护数据.伤害, self.战斗流程[1].保护数据.类型)

        self.执行流程 = 79
    elseif self.执行流程 == 79 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 80
        end
    elseif self.执行流程 == 80 and self.参战数据[self.战斗流程[1].保护数据.编号].单位.击退开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.返回开关 = true
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.y = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.y
        self.参战数据[self.战斗流程[1].攻击方].单位.移动坐标.x = self.参战数据[self.战斗流程[1].攻击方].单位.初始xy.x

        if self.参战数据[self.战斗流程[1].保护数据.编号].单位.死亡开关 == false then
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.返回开关 = true
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.y = self.参战数据[self.战斗流程[1].保护数据.编号].单位.初始xy.y
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动坐标.x = self.参战数据[self.战斗流程[1].保护数据.编号].单位.初始xy.x
            self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动上限 = 90
            self.执行流程 = 81
        else
            self.执行流程 = 82
        end
    elseif self.执行流程 == 81 and self.参战数据[self.战斗流程[1].保护数据.编号].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作 = "待战"

        self.参战数据[self.战斗流程[1].保护数据.编号].单位.动画:换方向(self.参战数据[self.战斗流程[1].保护数据.编号].单位.动作, self.参战数据[self.战斗流程[1].保护数据.编号].单位.方向)

        self.执行流程 = 82
    elseif self.执行流程 == 82 and self.参战数据[self.战斗流程[1].攻击方].单位.移动开关 == false then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

        self.执行流程 = 29
    elseif self.执行流程 == 83 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)
        self:群体法术动画设置(self.参战数据[self.战斗流程[1].攻击方], self.战斗流程[1].参数)

        self.执行流程 = 84
    elseif self.执行流程 == 84 and self.群体法术.开关 == false then
        self.执行流程 = 9
    elseif self.执行流程 == 100 then
        self.参战数据[self.战斗流程[1].挨打方].单位:操作抖动(false)
        self.参战数据[self.战斗流程[1].挨打方].单位:开启击退(self.战斗流程[1].死亡)
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].伤害, "掉血")

        self.执行流程 = 101
    elseif self.执行流程 == 101 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 102 then
        self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].伤害, "加血")

        self.执行流程 = 103
    elseif self.执行流程 == 103 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 104 then
        self.参战数据[self.战斗流程[1].挨打方].单位.法术状态组[self.战斗流程[1].参数] = nil
        self.执行流程 = 29
    elseif self.执行流程 == 105 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].挨打方].单位:增加技能动画(self.战斗流程[1].参数)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")
        播放技能音效(self.战斗流程[1].参数)

        self.执行流程 = 106
        if self.战斗流程[1].参数 ~= "加血" and self.战斗流程[1].参数 ~= "加蓝" then
            self.参战数据[self.战斗流程[1].挨打方].单位:添加状态动画(self.战斗流程[1].参数)
        end
        if self.战斗流程[1].参数 == "加血" then
            self.参战数据[self.战斗流程[1].挨打方].单位:设置掉血(self.战斗流程[1].数额, "加血")
        end

        if self.战斗流程[1].解封 then
            self.参战数据[self.战斗流程[1].挨打方].单位:解除异常状态()
        end
    elseif self.执行流程 == 106 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)

            self.执行流程 = 107

            if self.参战数据[self.战斗流程[1].攻击方].复活 ~= nil then
                self.参战数据[self.战斗流程[1].挨打方].单位.动作 = "待战"
                self.参战数据[self.战斗流程[1].挨打方].单位.动画.按帧更新 = false
                self.参战数据[self.战斗流程[1].挨打方].单位.倒地开关 = false
                self.参战数据[self.战斗流程[1].挨打方].单位.死亡开关  = false
            end
        end
    elseif self.执行流程 == 107 and self.参战数据[self.战斗流程[1].挨打方].单位.掉血开关 == false then
        self.执行流程 = 29
    elseif self.执行流程 == 108 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")

        self.执行流程 = 109
    elseif self.执行流程 == 109 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位.动画:置颜色(self.参战数据[self.战斗流程[1].攻击方].单位.动作, ARGB(105, 170, 170, 170))

            self.参战数据[self.战斗流程[1].攻击方].单位.隐身 = true
            self.执行流程 = 29
        end
    elseif self.执行流程 == 110 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")

        self.执行流程 = 111
    elseif self.执行流程 == 111 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].挨打方].单位.动画:置颜色(self.参战数据[self.战斗流程[1].挨打方].单位.动作, ARGB(105, 170, 170, 170))

            self.参战数据[self.战斗流程[1].挨打方].单位.隐身 = true
            self.执行流程 = 29
        end
    elseif self.执行流程 == 112 then
        self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "施法"

        self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
        self.参战数据[self.战斗流程[1].攻击方].单位:播放音效("施法")

        self.执行流程 = 113
    elseif self.执行流程 == 113 then
        for n = 1, #self.战斗流程[1].挨打方, 1 do
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:增加技能动画(self.战斗流程[1].参数)

            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位.动作 = "待战"
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位.动画.按帧更新 = false
            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位.倒地开关 = false

            self.参战数据[self.战斗流程[1].挨打方[n].编号].单位:设置掉血(self.战斗流程[1].挨打方[n].伤害, "加血")
        end

        self.执行流程 = 114
    elseif self.执行流程 == 114 then
        if self.参战数据[self.战斗流程[1].攻击方].单位.动画:取当前帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) == self.参战数据[self.战斗流程[1].攻击方].单位.动画:取结束帧(self.参战数据[self.战斗流程[1].攻击方].单位.动作) then
            self.参战数据[self.战斗流程[1].攻击方].单位.动作 = "待战"

            self.参战数据[self.战斗流程[1].攻击方].单位.动画:换方向(self.参战数据[self.战斗流程[1].攻击方].单位.动作, self.参战数据[self.战斗流程[1].攻击方].单位.方向)
            self.参战数据[self.战斗流程[1].攻击方].单位:设置掉血(self.战斗流程[1].消耗伤害, "掉血")

            self.执行流程 = 115
        end
    elseif self.执行流程 == 115 and self.参战数据[self.战斗流程[1].攻击方].单位.掉血开关 == false then
        self.执行流程 = 29
    end
end

function 战斗类:快捷技能(名称)

 if self.回合进程 == "命令回合" and self.参战数据[self.操作单位[self.当前单位].编号].战斗类型=="角色" and self.命令版面 then
  if 取技能信息(名称,"对象")==3  then
    self.命令类型="技能"
    self.命令对象=2
    self.命令参数=名称
    鼠标动画.显示序列=4
    self.命令版面=false
    self.法术开关=false
  elseif 取技能信息(名称,"对象")==2  then
    self.命令类型="技能"
    self.命令对象=1
    self.命令参数=名称
    鼠标动画.显示序列=4
    self.命令版面=false
    self.法术开关=false
  elseif 取技能信息(名称,"对象")==1  then
    self.命令类型="技能"
    self.命令对象=1
    self.命令参数=名称
    鼠标动画.显示序列=4
    self.命令版面=false
    self.法术开关=false
    self.操作单位[self.当前单位].类型=self.命令类型
    self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
    self.操作单位[self.当前单位].参数=self.命令参数
    if self.当前单位==#self.操作单位 then
        鼠标动画.显示序列=1
        self.回合进程="等待回合"
        客户端:发送数据(1,235,24,self:转文本(self.操作单位))
    else
        self.当前单位=self.当前单位+1
        self.命令版面=true
        self.捕捉开关=false
        self.法术开关=false
        self.命令类型="攻击"
        self.命令对象=2
        鼠标动画.显示序列=1
    end
  end
end
end


function 战斗类:resetFight(个体)
    个体.攻击封印=false
    个体.技能封印=false
    个体.特技封印=false
    个体.道具封印=false
    个体.全能封印=false
    个体.跳过行动=false --横扫千军状态
    个体.催眠封印=false --催眠符状态
    个体.封印状态=false

    个体.必杀=5
    个体.百步穿杨=0
    个体.福泽天下=0
    个体.行云流水=0
    个体.索命无常=0
    个体.烟雨飘摇=0
    个体.天雷地火=0
    个体.石破天惊=0
    个体.网罗乾坤=0
    个体.云随风舞=0
    个体.心随我动=0
    个体.隔山打牛=0
    个体.灵刃=false
    个体.预知=false
    个体.灵动=false
    个体.瞬击=false
    个体.抗法=false
    个体.阳护=false
    个体.识物=false
    个体.洞察=false
    个体.弑神=false
    个体.御风=false
    个体.顺势=false
    个体.复仇=false
    个体.自恋=false
    个体.怒吼=false
    个体.暗劲=false
    个体.逆境=false
    个体.瞬法=false
    个体.灵法=false
    个体.灵断=false
    个体.吮吸=false
    个体.识药=0
    个体.护佑=false
    个体.抗物=false
    个体.吸血=0
    个体.溅射=0
    个体.狂怒=0
    个体.阴伤=0
    个体.驱散=0  ---小法  内丹
    个体.连击=0
    个体.法术暴击伤害= 0
    个体.额外法术伤害=0
    个体.理直气壮=false
    个体.反震=0--.25
    个体.反击=0
    个体.法暴=0
    个体.法连=0
    个体.追击=false
    个体.怒击=false
    个体.法波=0
    个体.魔心=1
    个体.幸运=false
    个体.神佑=0
    个体.复活=0
    个体.冥想=0
    个体.慧根=1
    个体.再生=0
    个体.毒=0
    个体.驱鬼=0
    个体.偷袭=false
    个体.火吸=0
    个体.水吸=0
    个体.雷吸=0
    个体.土吸=0
    个体.夜战=false
    个体.感知=false
    个体.隐身=0
    个体.九黎战鼓 = 0
    个体.盘龙壁  = 0
    个体.神行飞剑 = 0
    个体.汇灵盏 = 0
    个体.织女扇 = 0
    个体.金甲仙衣 = 0
    个体.五彩娃娃 = 0
    个体.嗜血幡 = 0
    个体.混元伞 = 0
    个体.普渡 = 0
    个体.蟠龙玉璧 = 0
    个体.武器伤害=0
    个体.武器宝石=0

    个体.法术状态组 = {}
    for i=1,#法术状态名称 do
        -- 可能不止有无字段
        个体.法术状态组[法术状态名称[i]] = { 有无 = false }
    end
    -- if 个体.特技数据 ==nil then
    --     个体.特技数据={}
    -- end
    for i=1,#幻化战斗属性 do
        个体[幻化战斗属性[i]]=0
    end
    if 个体.法伤减免==nil then 个体.法伤减免=1 end
    if 个体.物伤减免==nil then 个体.物伤减免=1 end
    -- 个体.主动技能 = {}
    for i=1,#幻化属性值 do
        if 个体[幻化属性值[i]] == nil then 个体[幻化属性值[i]] = 0 end
    end
    -- 个体.治疗能力 = 0 -- 幻化
    -- 个体.固定伤害 = 0 -- 幻化
    个体.封印=0
    个体.神迹=0
    个体.兽王令 = 0
    个体.风灵 = 0
end

return 战斗类
