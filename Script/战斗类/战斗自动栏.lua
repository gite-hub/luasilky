--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2019-09-24 02:05:06
--======================================================================--
local 场景类_战斗自动栏 = class()
local 开启自动 = false
local format = string.format
local tp
function 场景类_战斗自动栏:初始化(根)
	tp = 根
	self.ID = 102
	self.x = 258
	self.y = 260
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.可视化 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.窗口时间 = 0
	local 资源 = tp.资源
	local 按钮 = tp._按钮
	local 自适应 = tp._自适应
	self.资源组 = {
		[0] = 自适应.创建(1,1,204,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,220,139,3,9),
		[2] = 按钮.创建(tp.资源:载入('JM.FT',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"重置"),
		[3] = 按钮.创建(tp.资源:载入('JM.FT',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"取消"),
		[4] = 按钮.创建(tp.资源:载入('JM.FT',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"暂离"),
		[5] = 按钮.创建(tp.资源:载入('JM.FT',"网易WDF动画",0xF11233BB),0,0,4,true,true),
	}
	for i=2,5 do
		self.资源组[i]:绑定窗口_(102)
	end
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体
	self.介绍文本 = 根._丰富文本(130,70,根.字体表.普通字体)
	self.介绍文本:清空()
	self.第一次 = false
end

function 场景类_战斗自动栏:打开()
	if self.可视化 then
		self.可视化 = false
	else

		self.可视化 = true
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	end
end
function 场景类_战斗自动栏:更新(dt,x,y)
	end
function 场景类_战斗自动栏:显示(dt,x,y)
	if not self.可视化 then
		return
	end
       if 自动战斗  then
		self.状态 = "取消"
		else
		self.状态 = "开启"
		end
	self.资源组[3]:置文字(self.状态)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[0]:显示(self.x+6,self.y+3)
	self.资源组[5]:显示(self.x+200,self.y+3)
	tp.窗口标题背景_:置区域(0,0,74,16)
	tp.窗口标题背景_:显示(self.x+72,self.y+3)
	zts2:置字间距(2)
	zts2:显示(self.x+78,self.y+3,"自动战斗")
	zts2:置字间距(0)
	tp.字体表.普通字体:置颜色(-16777216)

	if self.状态 == "取消" then
		self.介绍文本:清空()
		self.介绍文本:添加文本("#W/  剩余  #R/"..自动回合.." #W/回合")
		if tp.战斗类.参战数据[tp.战斗类.操作单位[1].编号].单位.数据.默认法术 ~= nil then
		self.介绍文本:添加文本("#W/ 人物 ：#G/"..tp.战斗类.参战数据[tp.战斗类.操作单位[1].编号].单位.数据.默认法术)
		else
		self.介绍文本:添加文本("#W/ 人物 ：#G/物理攻击")
		end

		if tp.战斗类.操作单位[2]~=nil then
			if tp.战斗类.参战数据[tp.战斗类.操作单位[2].编号].单位.数据.默认法术 ~= nil then
			self.介绍文本:添加文本("#W/召唤兽：#G/"..tp.战斗类.参战数据[tp.战斗类.操作单位[2].编号].单位.数据.默认法术)
			else
			self.介绍文本:添加文本("#W/召唤兽：#G/物理攻击")
			end
	    end
	end
	--tp.字体表.普通字体:显示(self.x + 16,self.y + 55,format("点击按钮%s自动",self.状态))
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
	          自动回合=9999
	     elseif self.资源组[5]:事件判断() or (引擎.鼠标弹起(1)and self:检查点(x,y)) then
	     	  self.介绍文本:清空()
	            自动战斗=false
				self.状态 = "取消"
				self.可视化 = false
				tp.提示:写入("#Y/你取消了自动战斗")
			return
		elseif self.资源组[3]:事件判断() then
			if self.状态 == "取消" then
				 self.介绍文本:清空()
	            自动战斗=false
				self.状态 = "取消"
				self.可视化 = false
				tp.提示:写入("#y/你取消了自动战斗")
			else
				自动战斗=true
                自动回合=9999
				self.状态 = "开启"
		        tp.提示:写入("#Y/你开启了自动战斗，在命令回合开始3秒后自动下达指令")
				for n=1,#tp.战斗类.窗口_ do
					if tp.战斗类.窗口_[n].可视 then
						tp.战斗类.窗口_[n]:打开()
					end
				end
				tp.战斗类.提示 = ""
			end
			self.资源组[3]:置文字(self.状态)
		end
	end
	self.资源组[4]:显示(self.x+20,self.y+105)
	self.资源组[2]:显示(self.x+148,self.y+105)
	self.资源组[3]:显示(self.x+83,self.y+105,true)
	self.介绍文本:显示(self.x+60,self.y+30)
	if tp.按钮焦点 then
		self.焦点 = true
	end
end
function 场景类_战斗自动栏:检查点(x,y)
	if self.可视化 and self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_战斗自动栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not self.第一次 then
		tp.运行时间 = tp.运行时间 + 2
		self.第一次 = true
	end
	if not 引擎.场景.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.战斗类.移动窗口 = true
	end
	if self.可视化  and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_战斗自动栏:开始移动(x,y)
	if self.可视化 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗自动栏