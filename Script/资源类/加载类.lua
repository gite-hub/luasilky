--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-05-05 04:12:39
-- 梦幻西游游戏资源破解 baidwwy@vip.qq.com(313738139) 老毕   和 C++PrimerPlus 717535046 这俩位大神破解所以资源
--======================================================================--
local 资源类_加载 = class()
--Script/资源类/
local 纹理类 = require("gge纹理类")
local 精灵类 = require("gge精灵类")
-- local Fmod类 = require("Fmod类")
local 动画类 = require("Script/资源类/动画类")
require("Script/lxy_translate_table")
local yq = 引擎

function 资源类_加载:初始化()
	local folder = "wdf/"
	self.folder = folder
	self.names = {
		[1] = { name = "shape.wdf", path = 资料片wd .. "shape.wdf"},
		[2] = { name = "shape.wd2", path = 资料片wd .. "shape.wd2"},
		[3] = { name = "baby.wdf", path = 资料片wd .. "baby.wdf"},
		[4] = { name = "uijd.wdf", path = 资料片ui .. "uijd.wdf"},
		[5] = { name = "ui.wdf", path = 资料片ui .. "ui.wdf"},

        [6] = { name = "JM.FT", path = folder .. "JM.FT"},
		[7] = { name = "ZY.FT", path = folder .. "ZY.FT"},
		[8] = { name = "ZHS.FT", path = 资料片wd .. "shape.wdf"}, --全覆盖
		[9] = { name = 'WP.FT', path = folder .. 'WP.FT'},
		[10] = { name = 'WE.dll', path = folder .. 'WE.dll'},
		[11] = { name = "smap.wdf", path = 资料片wd .. "smap.wdf"},
	    [12] = { name = "WP1.dll", path = folder .. "WP1.dll"},
	    [13] = { name = "wzife.wdf", path = folder .. "wzife.wdf"},
		[14] = { name = "common/item.wdf", path = folder .. "item.wdf"},
		[15] = { name = "common/shape.wda", path = 资料片wd .. "shape.wdf"}, --全覆盖
		[16] = { name = "common/shape.wdb", path = 资料片wd .. "shape.wdf"}, --全覆盖
		[17] = { name = "common/shape.wdc", path = folder .. "shape.wdc"},
		[18] = { name = "wzife.wd1", path = folder .. "wzife.wd1"},
		[19] = { name = "wzife.wd2", path = folder .. "wzife.wd2"},
		[20] = { name = "wzife.wd3", path = folder .. "wzife.wd3"},
		[21] = { name = "wzife.wd4", path = folder .. "wzife.wd4"},
		[22] = { name = "ui.wd1", path = 资料片ui .. "ui.wd1"},
	}
	self.wdfs = {}

end

function 资源类_加载:打开()

		local openwdf = require("script/资源类/WDF")

		-- ffi函数.信息框("-10", "0")

		for k =1,#self.names do
			if k ~= 8 and k ~= 15 and k ~= 16 then
				-- print(self.names[k].name, "\t", self.names[k].path)
				self.wdfs[self.names[k].name] = openwdf(self.names[k].path)
			end
		end
		-- local wdf1 = self.wdfs[self.names[1].name]
		-- self.wdfs[self.names[8].name] = wdf1
		-- self.wdfs[self.names[15].name] = wdf1
		-- self.wdfs[self.names[16].name] = wdf1
		-- self.wdfs[self.names[17].name] = wdf1

		-- ffi函数.信息框("-11", "0")

		-- openwdf = require("script/资源类/锦衣wdf")

		-- local fileso = {
		-- 	"shape2.npk",
		-- 	"shape1.npk",
		-- 	"shape0.npk"
		-- }
		-- for n=1,#fileso do
		-- 	self.wdfs[fileso[n]] = openwdf(self.folder .. fileso[n])
		-- end

		-- ffi函数.信息框("-12", "0")
end

-- function 资源类_加载:取偏移(file,id)
-- 	return self.wdf[file]:读偏移(id)
-- end

-- function 资源类_加载:读数据(file,id)
-- 	return self.wdf[file]:读数据(id)
-- end

function 资源类_加载:载入(文件,类型,文件号,音量,附加,fs,fs1)
	-- if 文件号 then
	-- 	print(string.format("was   %s   0x%08X   %u",文件,文件号,文件号))
	-- end
	if 文件 and 文件号 and 文件 ~= "" and lxy_translate_table[文件] then
		local i = lxy_translate_table[文件][文件号 + 0]
		if i ~= nil then
			文件 = i.file
			文件号 = i.id or 文件号
			-- print(string.format("转换   %s   0x%08X   %u", 文件,文件号,文件号))
		end
	end
	if 类型 == "网易WDF动画" then
		-- if （(not 文件) or 文件 == "") and ((not 文件号) or 文件号 == 0) then
		if not 文件号 then
			__gge.messagebox("无效WAS","不丝滑", 16)
			return
		end
		local a
		local b
		文件号 = 文件号 + 0
		if 文件 and self.wdfs[文件] then
			a, b = self.wdfs[文件]:读数据(文件号)
		end
		if a ~= nil and b ~= nil then return 动画类(a,b,音量,附加,fs) end
	    if a == nil then
			for n = 1, 5 do
				a, b = self.wdfs[self.names[n].name]:读数据(文件号)
				if a ~= nil and b ~= nil then return 动画类(a,b,音量,附加,fs) end
			end
	    end
	    __gge.messagebox(string.format("无效WAS   %s   0x%08X   %u",文件,文件号,文件号),"不丝滑", 16)
		-- return bbbb(a,b,音量,附加,fs)--987841DE
	elseif 类型 == "图片" then
		return 精灵类(纹理类(文件))
	-- elseif 类型 == "音乐" then -- 没有这个类型
	-- 	return Fmod类(文件,2,nil,nil,0 or 0)
	-- elseif 类型 == "网易WDF动画s" then -- 没这个类型
	-- 	return 动画类(文件)
	elseif 类型 == "网易锦衣动画" then
		if 文件号 then return 动画类锦衣(self.wdfs[文件]:取文件(文件号 + 0)) end
		return 动画类锦衣(self.wdfs[文件]:取文件(0xEC4818D8))
	end
end

return 资源类_加载