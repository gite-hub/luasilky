
local ggeobj  = class()
rawset(ggeobj, '__new__', require("__ggewdf__"))

function ggeobj:初始化(filename)
    self._obj = ggeobj.__new__()
    self._ok = self._obj:load(filename)
end

function ggeobj:读数据(cid)
    if not self._ok then return nil, nil end
    local t = self._obj:getData(cid)
    if t.ptr == 0 or t.size == 0 then return nil, nil end
    return t.ptr, t.size
end

function ggeobj:取文件(cid)
    return self:读数据(cid)
end

function ggeobj:读偏移(Hash)
    if not self._ok then return nil, nil end
    local t = self._obj:getIndex(cid)
    if t.size == 0 then return nil, nil end
    return t.offest, t.size
end

return ggeobj