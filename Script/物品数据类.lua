local 物品数据类 = class()
local itemwp
local itemwpw = 引擎.取物品
function 物品数据类:初始化(数据, 来源, 背包类型, 格子id)
	if 数据 == nil then
		return nil
	end

	self.鉴定按钮=引擎.场景.资源:载入("JM.FT","网易WDF动画",0x4A51901C)
	self.背包类型 = 背包类型
	self.格子id = 格子id
	self.背包编号 = 包裹转换(背包类型)
	self.名称 = 数据.名称
  itemwp = 引擎.取物品(self.名称)
	self.类型 = 数据.类型
	self.数量 = 数据.数量
	self.来源 = 来源
	self.数据 = 数据
	self.等级 = 数据.等级
	self.高度 = 20
    if self.名称~="怪物卡片" then
        self.大动画= 引擎.场景.资源:载入(itemwp.资源,"网易WDF动画",itemwp.大模型资源)
        self.小动画= 引擎.场景.资源:载入(itemwp.资源,"网易WDF动画",itemwp.小模型资源)
    end
  self.说明=itemwp.说明

	if self.类型 == "武器" then
		self.角色 = itemwp.角色限制
		self.条件 = itemwp.等级
		self.分类 = "武器"
		self:高度增加()
  elseif self.名称 == "月光宝盒" then
    self.高度 = 28
	elseif self.类型 == "衣服" or self.类型 == "头盔" then
        self.角色 = itemwp.性别限制
        self.条件 = itemwp.等级
		self.分类 = "防具"
		self:高度增加()
	elseif self.类型 == "宝石" then
	self.效果 = itemwp.效果
	self.部位=	itemwp.部位
		elseif self.类型 == "药品" then
	self.功效 = itemwp.功效
	self.等级=	itemwp.等级

    elseif self.类型 == "鞋子" or self.类型 == "腰带" or self.类型 == "项链" then
        self.角色 = "无"
        self.条件 = itemwp.等级
        self.分类 = "防具"
        self:高度增加()
    elseif self.类型=="召唤兽道具"  or self.类型 == "炼妖" or self.类型== "符石" or self.类型 == "未激活符石" or self.名称 == "符石卷轴"  then
                    if self.类型== "炼妖" and  self.名称 ~= "吸附石" then
                       if 数据.技能==nil then
                        self.技能 = "购买随机生成内丹"
                      else
                         self.技能=数据.技能
                       end
                    elseif self.类型 =="符石" then
                            self.颜色 = 数据.颜色
                            self.耐久 = 500
                            if 数据.体质 then
                            self.体质 = 数据.体质
                            end
                            if 数据.耐力 then
                            self.耐力 = 数据.耐力
                            end
                            if 数据.敏捷 then
                            self.敏捷 = 数据.敏捷
                            end
                            if 数据.法术防御 then
                            self.法术防御 = 数据.法术防御
                            end
                            if 数据.魔力 then
                            self.魔力 = 数据.魔力
                            end
                            if 数据.力量 then
                            self.力量 = 数据.力量
                            end
                            if 数据.法术伤害 then
                            self.法术伤害 = 数据.法术伤害
                            end
                            if 数据.气血 then
                            self.气血 = 数据.气血
                            end
                            if 数据.防御 then
                            self.防御 = 数据.防御
                            end
                            if 数据.命中 then
                            self.命中 = 数据.命中
                            end
                            if 数据.伤害 then
                            self.伤害 = 数据.伤害
                            end
                            if 数据.灵力 then
                            self.灵力 = 数据.灵力
                            end
                            if 数据.魔法 then
                            self.魔法 = 数据.魔法
                            end
                            if 数据.速度 then
                            self.速度 = 数据.速度
                            end
                            if 数据.躲避 then
                            self.躲避 = 数据.躲避
                            end
                            if 数据.固定伤害 then
                            self.固定伤害 = 数据.固定伤害
                            end
                    end
                    if 数据.灵气~=nil   then
                    self.灵气=数据.灵气
                   end
	elseif self.名称 == "怪物卡片" then
		if self.数据.等级 == 1 then
			self.大写数字 = "一"
		elseif self.数据.等级 == 2 then
			self.大写数字 = "二"
		elseif self.数据.等级 == 3 then
			self.大写数字 = "三"
		elseif self.数据.等级 == 4 then
			self.大写数字 = "四"
		elseif self.数据.等级 == 5 then
			self.大写数字 = "五"
		elseif self.数据.等级 == 6 then
			self.大写数字 = "六"
		elseif self.数据.等级 == 7 then
			self.大写数字 = "七"
    elseif self.数据.等级 == 8 then
      self.大写数字 = "八"
    elseif self.数据.等级 == 9 then
      self.大写数字 = "九"
		end
        local bsk= 引擎.取变身卡(self.数据.类型)
            self.大动画 = 引擎.场景.资源:载入(bsk.资源,"网易WDF动画",bsk.大模型)
            self.小动画 = 引擎.场景.资源:载入(bsk.资源,"网易WDF动画",bsk.小模型)
     end
	道具文本:清空()
	道具文本:添加文本(self.说明)

	self.显示高度 = 道具文本.数量
	self.高度 = self.高度 + 道具文本.数量 * 1.5

	if math.floor(self.高度) < self.高度 then
		self.高度 = math.floor(self.高度)
	end

	if (self.类型 == "佩饰" or self.类型 == "戒指" or self.类型 == "耳饰" or self.类型 == "手镯") and self.数据.幻化属性 ~= nil then
		self.高度 = self.高度 + 2
		self.高度 = self.高度 + #self.数据.幻化属性.附加 * 2
		if self.数据.特效 ~= nil then
			self.高度 = self.高度 + 2
		end
	end
end

function 物品数据类:置颜色(颜色)
	return self.小动画.精灵:置颜色(颜色)
end

function 物品数据类:高度增加()
   if  self.数据.鉴定 then
     if #self.数据.锻造数据>0 then
         self.高度=self.高度+2
        end
     end
  if #self.数据.三围属性>0 then
      self.高度=self.高度+2
     end
         if  self.数据.熔炼~=nil then
       self.高度=self.高度+4
     end
     if  self.数据.附魔~=nil then
      self.高度=self.高度+2
     end
   if  self.数据.特技~=nil then
      self.高度=self.高度+2
     end
   if  self.数据.特效~=nil then
      self.高度=self.高度+#self.数据.特效*2
     end
   if  self.数据.制造者~=nil then
      self.高度=self.高度+2
     end
  if  self.数据.临时效果~=nil then
      self.高度=self.高度+2
     end
   if  self.数据.套装~=nil then
      self.高度=self.高度+2
     end
     if  self.数据.专用~=nil then
      self.高度=self.高度+2
     end
    if  self.数据.符石~=nil then
       self.高度=self.高度+#self.数据.符石*2+2
           if  self.数据.符石.组合~=nil then
       self.高度=self.高度+8
     end
 end
    if self.数据.幻化属性~=nil then
       self.高度=self.高度+2
       self.高度=self.高度+#self.数据.幻化属性.附加*2
      end

 end

function 物品数据类:更新(dt)
end


function 物品数据类:获取焦点()
  if self.小动画==nil then return false end
    if self.小动画:取包围盒():检查点(鼠标.x,鼠标.y) then
      return true
    else
      return false
     end
 end
function 物品数据类:显示(x, y)
	if self.小动画 == nil then
		return 0
	end
  if self.名称 == "水墨游龙" then
     self.小动画:显示(x+25, y+25)
  elseif self.名称 == "齐天大圣" or  self.名称 == "浪淘纱·月白" or  self.名称 == "锦绣幻梦"or  self.名称 == "萤雪轻裘"or  self.名称 == "尖叫服" or self.名称 == "浪淘纱" or self.名称 == "冰灵蝶翼·月笼" or self.名称 == "华风汉雅" or self.名称 == "夜影" or self.名称 == "夏日清凉" or self.名称 == "萌萌小厨" then
     self.小动画:显示(x+3, y+3)
  else
    self.小动画:显示(x, y)
  end

	if self.数量 ~= nil and self.来源 ~= 2 then
		self:数量显示(self.数量, x, y - 2)
	end
end

function 物品数据类:数量显示(文本, x, y)
	if 文本 == nil or 文本 <= 1 then
		return 0
	end
	数量字体[1]:显示(x + 1, y + 1, 文本)
	数量字体[2]:显示(x, y, 文本)
end

function 物品数据类:显示事件()
	if self.来源 == 2 or self.来源 == 1 or self.来源 == 99 then
		self.y = 74
		self.x = 240
	elseif self.来源 == 3 then
		self.y = 74
		self.x = 440
	end
	信息提示:显示(2, 40, self.高度, self.x, self.y)
	self.大动画:显示(self.x, self.y + 20)
	道具名称:显示(self.x + 118, self.y + 10, self.名称)
	道具文本:清空()
	道具文本:添加文本(self.说明)
	道具文本:显示(self.x + 120, self.y + 45)
	if self.类型 == "武器" or self.类型 == "衣服" or self.类型 == "头盔" or self.类型 == "鞋子" or self.类型 == "腰带" or self.类型 == "项链" then
		self:装备显示()
	elseif self.类型 == "功能" then
		self:功能显示()
	elseif self.类型 == "普通" or self.名称=="符石卷轴"then
		self:普通显示()
	elseif self.类型 == "药品" then
		self:药品显示()
	elseif self.类型 == "杂货" then
		self:杂货显示()
  elseif self.类型 == "法宝" then
    self:法宝显示()
	elseif self.类型 == "宝石" then
		self:宝石显示()
	elseif self.类型 == "打造" then
		self:打造显示()
	elseif self.类型 == "烹饪" then
		self:烹饪显示()
	elseif self.类型 == "bb装备" then
		self:bb装备显示()
	elseif self.名称 == "怪物卡片" then
		self:变身卡显示()
	elseif self.类型 == "佩饰" or self.类型 == "戒指" or self.类型 == "手镯" or self.类型 == "耳饰" then
		self:灵饰显示()
    elseif self.类型 == "锦衣"  then
    self:锦衣显示()
    elseif self.类型=="召唤兽道具" or self.类型== "炼妖" then
     self:进阶显示()
   elseif self.类型=="符石" then
     self:符石显示()
      elseif self.类型=="未激活符石" then
        self:符石显示1()
          elseif self.类型=="召唤兽饰品" then
        self:召唤兽饰品显示()

	end
end
function 物品数据类:符石显示1()


    tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFFFF):显示(self.x+120,self.y+self.显示高度*20+35,"【激活方式】鼠标右键激活")
    tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF00):显示(self.x+120,self.y+self.显示高度*20+50,"未激活")
      if 引擎.鼠标弹起(键盘符号.右键) and self.来源==1  then
    self.发送信息={类型=self.背包类型,格子=self.格子id}
    客户端:发送数据(self.格子id,186,13,self.背包类型)

    end
end

function 物品数据类:符石显示()
 tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF00):显示(self.x+120,self.y+self.显示高度*20+35,"等级 "..self.等级.." "..self.颜色)
            local sx = ""
            local 气血 = self.气血
            local 魔法 = self.魔法
            local 命中 = self.命中
            local 伤害 = self.伤害
            local 防御 = self.防御
            local 速度 = self.速度
            local 躲避 = self.躲避
            local 灵力 = self.灵力
            local 敏捷 = self.敏捷
            local 体质 = self.体质
            local 耐力 = self.耐力
            local 法术防御 = self.法术防御
            local 魔力 = self.魔力
            local 力量 = self.力量
            local 法术伤害 = self.法术伤害
            local 固定伤害 = self.固定伤害
            if 体质 ~= 0 and 体质 ~= nil  then
                sx = sx.."体质 +"..体质.." "
            end
            if 耐力 ~= 0 and 耐力 ~= nil  then
                sx = sx.."耐力 +"..耐力.." "
            end
            if 法术防御 ~= 0 and 法术防御 ~= nil  then
                sx = sx.."法术防御 +"..法术防御.." "
            end
            if 魔力 ~= 0 and 魔力 ~= nil  then
                sx = sx.."魔力 +"..魔力.." "
            end
            if 力量 ~= 0 and 力量 ~= nil  then
                sx = sx.."力量 +"..力量.." "
            end
            if 法术伤害 ~= 0 and 法术伤害 ~= nil  then
                sx = sx.."法术伤害 +"..法术伤害.." "
            end
            if 固定伤害 ~= 0 and 固定伤害 ~= nil  then
                sx = sx.."固定伤害 +"..固定伤害.." "
            end
            if 伤害 ~= 0 and 伤害 ~= nil  then
                sx = sx.."伤害 +"..伤害.." "
            end
            if 命中 ~= 0  and 命中 ~= nil then
                sx = sx.."命中 +"..命中.." "
            end
            if 防御 ~= 0 and 防御 ~= nil  then
                sx = sx.."防御 +"..防御.." "
            end
            if 气血 ~= 0 and 气血 ~= nil then
                sx = sx.."气血 +"..气血.." "
            end
            if 魔法 ~= 0 and 魔法 ~= nil  then
                sx = sx.."魔法 +"..魔法.." "
            end
            if 速度 ~= 0 and 速度 ~= nil  then
                sx = sx.."速度 +"..速度.." "
            end
            if 躲避 ~= 0 and 躲避 ~= nil  then
                sx = sx.."躲避 +"..躲避.." "
            end
            if 灵力 ~= 0 and 灵力 ~= nil  then
                sx = sx.."灵力 +"..灵力.." "
            end
            if 敏捷 ~= 0 and 敏捷 ~= nil  then
                sx = sx.."敏捷 +"..敏捷.." "
            end
            if sx ~= "" then
            tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF00):显示(self.x+120,self.y+self.显示高度*20+35+15,sx)
            end
  tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF00):显示(self.x+120,self.y+self.显示高度*20+35+30,"耐久 "..self.耐久)

end


function 物品数据类:进阶显示()
  if self.技能 ~= nil then
 tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF80):显示(self.x+120,self.y+self.显示高度*20+32,"所带内丹技能: "..self.技能)
 end
   if self.灵气 ~= nil then
 tp.主界面.界面数据[3].道具参数:置颜色(0xFFFFFF80):显示(self.x+120,self.y+self.显示高度*20+32,"灵气: "..self.灵气)
 end
  if 引擎.鼠标弹起(键盘符号.右键) and self.来源==1  then
    if self.名称 == "清灵净瓶"  then
      self.发送信息={类型=self.背包类型,格子=self.格子id}
       客户端:发送数据(self.格子id,79,13,self.背包类型,1)

    elseif self.名称 =="初级清灵仙露"  or self.名称 =="中级清灵仙露"  or self.名称 =="高级清灵仙露" then
            self.发送信息={类型=self.背包类型,格子=self.格子id}
       客户端:发送数据(self.格子id,78,13,self.背包类型,1)
        elseif self.名称 =="易经丹" then
            self.发送信息={类型=self.背包类型,格子=self.格子id}
       客户端:发送数据(self.格子id,77,13,self.背包类型,1)
          elseif self.名称 =="玉葫灵髓" then
            self.发送信息={类型=self.背包类型,格子=self.格子id}
       客户端:发送数据(self.格子id,80,13,self.背包类型,1)
    end
  end
end

function 物品数据类:变身卡显示()
	local 变身卡数据 = {
巨蛙={技能="",属性=0,类型="",正负=0}
,大海龟={技能="",属性=0,类型="",正负=0}
,护卫={技能="",属性=0,类型="",正负=0}
,树怪={技能="",属性=0,类型="",正负=0}
,强盗={技能="",属性=0,类型="",正负=0}
,海毛虫={技能="",属性=0,类型="",正负=0}
,大蝙蝠={技能="",属性=0,类型="",正负=0}
,山贼={技能="",属性=0,类型="",正负=0}
,野猪={技能="",属性=0,类型="",正负=0}
,骷髅怪={技能="夜战",属性=20,类型="气血",正负=2}
,羊头怪={技能="幸运",属性=10,类型="气血",正负=2}
,蛤蟆精={技能="毒",属性=15,类型="防御",正负=2}
,狐狸精={技能="感知",属性=5,类型="气血",正负=2}
,老虎={技能="强力",属性=10,类型="速度",正负=2}
,黑熊={技能="防御",属性=5,类型="灵力",正负=2}
,花妖={技能="慧根",属性=0,类型="",正负=0}
,牛妖={技能="反击",属性=5,类型="防御",正负=2}
,小龙女={技能="驱鬼",属性=5,类型="伤害",正负=2}
,野鬼={技能="夜战",属性=5,类型="灵力",正负=2}
,狼={技能="偷袭",属性=10,类型="气血",正负=2}
,虾兵={技能="必杀",属性=10,类型="伤害",正负=2}
,蟹将={技能="连击",属性=12,类型="速度",正负=2}
,龟丞相={技能="",属性=7,类型="气血",正负=1}
,蜘蛛精={技能="毒",属性=10,类型="防御",正负=2}
,黑熊精={技能="反震",属性=10,类型="防御",正负=2}
,僵尸={技能="弱点雷",属性=10,类型="气血",正负=1}
,牛头={技能="招架",属性=0,类型="",正负=0}
,马面={技能="必杀",属性=8,类型="伤害",正负=2}
,雷鸟人={技能="飞行",属性=10,类型="气血",正负=2}
,蝴蝶仙子={技能="魔之心",属性=10,类型="防御",正负=2}
,古代瑞兽={技能="反震",属性=10,类型="气血",正负=2}
,白熊={技能="迟钝",属性=0,类型="",正负=0}
,黑山老妖={技能="偷袭",属性=10,类型="速度",正负=2}
,天兵={技能="防御",属性=0,类型="",正负=0}
,天将={技能="强力",属性=8,类型="灵力",正负=2}
,地狱战神={技能="连击",属性=5,类型="气血",正负=2}
,风伯={技能="敏捷",属性=5,类型="防御",正负=2}
,凤凰={技能="飞行",属性=0,类型="",正负=0}
,蛟龙={技能="永恒",属性=0,类型="",正负=0}
,芙蓉仙子={技能="再生",属性=30,类型="防御",正负=1,单独=1}
,巡游天神={技能="必杀",属性=20,类型="速度",正负=2}
,星灵仙子={技能="慧根",属性=10,类型="灵力",正负=1,单独=1}
,如意仙子={技能="慧根",属性=10,类型="灵力",正负=1,单独=1}
,幽灵={技能="高级夜战",属性=0,类型="",正负=0}
,吸血鬼={技能="偷袭",属性=5,类型="速度",正负=2}
,鬼将={技能="高级必杀",属性=10,类型="伤害",正负=2}
,净瓶女娲={技能="高级永恒",属性=5,类型="防御",正负=2}
,律法女娲={技能="高级反击",属性=10,类型="速度",正负=2}
,灵符女娲={技能="魔之心",属性=0,类型="",正负=0}
,大力金刚={技能="高级防御",属性=5,类型="灵力",正负=2}
,雾中仙={技能="高级敏捷",属性=10,类型="防御",正负=2}
,灵鹤={技能="高级魔之心",属性=10,类型="防御",正负=2}
,炎魔神={技能="高级魔之心",属性=15,类型="气血",正负=2}
,进阶阴阳伞={技能="高级魔之心",属性=15,类型="气血",正负=2}
,进阶画魂={技能="高级魔之心",属性=15,类型="气血",正负=2}
,夜罗刹={技能="高级夜战",属性=5,类型="速度",正负=1}
,噬天虎={技能="高级强力",属性=10,类型="速度",正负=2}
,龙龟={技能="高级防御",属性=5,类型="灵力",正负=2}
,进阶幽灵={技能="高级防御",属性=5,类型="灵力",正负=2}
,进阶灵鹤={技能="高级法术暴击",属性=5,类型="灵力",正负=2}
,进阶琴仙={技能="高级法术暴击",属性=5,类型="灵力",正负=2}
,进阶毗舍童子={技能="高级必杀",属性=5,类型="伤害",正负=2}
,长眉灵猴={技能="法术暴击",属性=5,类型="气血",正负=2}
,进阶长眉灵猴={技能="高级法术暴击",属性=10,类型="气血",正负=2}
,进阶持国巡守={技能="迟钝",属性=1,类型="灵力",正负=1}
,进阶混沌兽={技能="高级魔之心",属性=10,类型="气血",正负=2}
,金身罗汉={技能="高级防御",属性=10,类型="气血",正负=1}
,进阶金身罗汉={技能="迟钝",属性=15,类型="气血",正负=1}
,巨力神猿={技能="高级强力",属性=5,类型="气血",正负=2}
,进阶巨力神猿={技能="高级强力",属性=2,类型="灵力",正负=1}
,进阶狂豹人形={技能="高级连击",属性=8,类型="速度",正负=1}
,狂豹兽形={技能="敏捷",属性=5,类型="速度",正负=1}
,进阶狂豹兽形={技能="高级敏捷",属性=10,类型="气血",正负=2}
,曼珠沙华={技能="高级敏捷",属性=2,类型="灵力",正负=2}
,进阶曼珠沙华={技能="高级敏捷",属性=5,类型="防御",正负=2}
,进阶蝎子精={技能="高级反震",属性=5,类型="气血",正负=1}
,修罗傀儡鬼={技能="夜战",属性=2,类型="速度",正负=1}
,进阶修罗傀儡鬼={技能="高级夜战",属性=5,类型="气血",正负=1}
,修罗傀儡妖={技能="高级偷袭",属性=1,类型="灵力",正负=1}
,进阶修罗傀儡妖={技能="高级偷袭",属性=2,类型="灵力",正负=1}
,增长巡守={技能="须弥真言",属性=10,类型="气血",正负=2}
,进阶增长巡守={技能="须弥真言",属性=1,类型="灵力",正负=2}
,进阶真陀护法={技能="高级偷袭",属性=5,类型="防御",正负=2}
,蜃气妖={技能="高级法术暴击",属性=1,类型="灵力",正负=1}
}
	tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 38, "卡片类型: " .. self.数据.类型)
  tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 54, "卡片等级: " .. self.大写数字 .. "级")
  tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 70, "技能要求: " .. "无")

  if 变身卡数据[self.数据.类型].技能 == "" then
    tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 84, "【附加技能】: " .. "无")
  else
    tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + 88, "【附加技能】: " .. 变身卡数据[self.数据.类型].技能)
  end

  if 变身卡数据[self.数据.类型].属性 == 0 then
    tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x + 120, self.y + self.显示高度 * 20 + 104, "【属性影响】: " .. "无")
  else
    if 变身卡数据[self.数据.类型].正负 == 1 then
      self.正负符号 = "+"
    else
      self.正负符号 = "-"
    end

    if 变身卡数据[self.数据.类型].单独 == 1 then
      self.百分符号 = ""
    else
      self.百分符号 = "%"
    end

    tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x + 120, self.y + self.显示高度 * 20 + 104, "【属性影响】: " .. 变身卡数据[self.数据.类型].类型 .. self.正负符号 .. 变身卡数据[self.数据.类型].属性 .. self.百分符号)
  end

  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 120, "等级: " .. self.数据.等级 .. " 剩余使用次数:" .. self.数据.次数)
  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 134, "持续时间:120分钟")

  if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
    客户端:发送数据(self.格子id, 47, 13, self.背包类型)
  end
end
function 物品数据类:打造显示()
	if self.名称 == "制造指南书" or self.名称 == "灵饰指南书" then
		self.临时武器名称 = self.数据.制造
		self.制造 = self.数据.制造

		if self.制造 == "枪" then
			self.临时武器名称 = "枪矛"
		elseif self.制造 == "斧" then
			self.临时武器名称 = "斧钺"
		elseif self.制造 == "双" then
			self.临时武器名称 = "双短剑"
		elseif self.制造 == "环" then
			self.临时武器名称 = "环圈"
		elseif self.制造 == "爪" then
			self.临时武器名称 = "爪刺"
		elseif self.制造 == "棒" then
			self.临时武器名称 = "魔棒"
		elseif self.制造 == "飘" then
			self.临时武器名称 = "飘带"
		end

		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, "种类 " .. self.临时武器名称 .. ", 等级 " .. (self.数据.参数 or 'nil'))
	else
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, "等级 " .. (self.数据.参数 or 'nil'))
	end
end

function 物品数据类:bb装备显示()
	tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 45, "【装备对象】召唤兽")
	tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 65, "【装备条件】等级" .. self.数据.等级)
	tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 85, "等级 " .. self.数据.等级)
	tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 100, self.数据.主属性.名称 .. " +" .. self.数据.主属性.数值)

	if self.数据.附属性 ~= nil then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 195, self.y + self.显示高度 * 20 + 100, self.数据.附属性.名称 .. " +" .. self.数据.附属性.数值)
	end

	if self.数据.单加 ~= nil then
		tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + 115, self.数据.单加.名称 .. " +" .. self.数据.单加.数值)

		if self.数据.双加 ~= nil then
			tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 195, self.y + self.显示高度 * 20 + 115, self.数据.双加.名称 .. " +" .. self.数据.双加.数值)
		end
	end
		if self.数据.套装效果 ~= nil then
	  if self.数据.单加 == nil and self.数据.双加 == nil then
        tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255, 80, 200, 255)):显示(self.x + 120, self.y + self.显示高度 * 20 + 115,  self.数据.套装效果.类型.."："..self.数据.套装效果.名称)

	  else
        tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255, 80, 200, 255)):显示(self.x + 120, self.y + self.显示高度 * 20 + 130,  self.数据.套装效果.类型.."："..self.数据.套装效果.名称)
      end
    end

	if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 26, 13, self.背包类型)
	elseif 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 99 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 27, 13, self.背包类型)
	end
end

function 物品数据类:宝石显示()
	if self.名称 ~= "星辉石" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, "【镶嵌装备】" .. self.部位)
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 55, "【镶嵌效果】" .. self.效果)
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 73, "等级 " .. self.数据.参数)
	else
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, "等级 " .. self.数据.参数)
	end

	if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 14, 13, self.背包类型)
	end
end
function 物品数据类:法宝显示()
  if self.数据.层数 == nil  then
    tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, self.数据.等级.."级法宝")
  else
    tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + 37, self.数据.等级.."级法宝,修炼境界:第".. self.数据.层数 .."层")
    tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 55, 法宝层数[self.数据.层数+1])
    tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + 73, "灵气：" .. self.数据.灵气 .." 五行："..self.数据.五行)--
    if 法宝限制[self.数据.名称] ~= nil then
      tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 91, 法宝限制[self.数据.名称])
    end
    tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x + 185, self.y + self.显示高度 * 20 + 55, 法宝使用限制[self.数据.名称])
    tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 122, 法宝类型[self.数据.名称])
    tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 107, 法宝门派限制[self.数据.名称])
  end--颜色自己改

  if self.数据.名称 == "月光宝盒" then
    if self.数据.地图数据 == nil then
      tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 137, "未定位")
    else
      tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 115, self.y + self.显示高度 * 20 + 137, self.数据.地图数据.名称 .. "坐标X："..math.floor(self.数据.地图数据.坐标x/20).."坐标Y："..math.floor(self.数据.地图数据.坐标y/20))
    end
  end
  if 引擎.鼠标弹起(键盘符号.右键) and self.背包类型 == "法宝" and itemwpw(self.数据.名称).主动被动 == "被动" then
    客户端:发送数据(self.格子id, 206, 13, self.背包编号)
  elseif 引擎.鼠标弹起(键盘符号.右键) and self.背包类型 == "法宝" and self.数据.名称 == "月光宝盒" then
    if self.数据.地图数据 == nil then
      客户端:发送数据(self.格子id, 209, 13, self.背包类型)
    else
      客户端:发送数据(self.格子id, 210, 13, self.背包类型)
    end
  elseif 引擎.鼠标弹起(键盘符号.右键) and self.背包类型 == "法宝" and itemwpw(self.数据.名称).主动被动 == "主动" then
    信息提示:加入提示("#y/该法宝无需佩戴！")
  elseif 引擎.鼠标弹起(键盘符号.右键) then
    信息提示:加入提示("#y/法宝只能在法宝栏内使用！")
  end
end
function 物品数据类:杂货显示()
	if self.名称 == "白色导标旗" or self.名称 == "绿色导标旗" or self.名称 == "红色导标旗" or self.名称 == "黄色导标旗" or self.名称 == "蓝色导标旗" then
		if self.数据.地图名称 == nil then
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 39, "未定标")

			if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
				self.发送信息 = {
					类型 = self.背包类型,
					格子 = self.格子id
				}

				客户端:发送数据(self.格子id, 8, 13, self.背包类型)
			end
		else
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 39, self.数据.地图名称 .. "(" .. self.数据.x .. "," .. self.数据.y .. ")")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 59, "还可以使用" .. self.数据.次数 .. "次")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "右击将直接传送至定标处")

			if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
				self.发送信息 = {
					类型 = self.背包类型,
					格子 = self.格子id
				}

				客户端:发送数据(self.格子id, 9, 13, self.背包类型)
			end
		end
	elseif self.名称 == "白色合成旗" or self.名称 == "绿色合成旗" or self.名称 == "红色合成旗" or self.名称 == "黄色合成旗" or self.名称 == "蓝色合成旗" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 39, "场景:" .. self.数据.地图 .. ",还能使用" .. self.数据.次数 .. "次")

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 58, 13, self.背包类型)
		end

    elseif self.名称 == "金银宝盒"  then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 87, 13, self.背包类型)
    end
    elseif self.名称 == "小雪块"  then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 88, 13, self.背包类型)
    end
     elseif self.名称 == "大雪块"  then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 89, 13, self.背包类型)
    end
	elseif self.名称 == "飞行符" and 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		tp.主界面.界面数据[10]:刷新(self.背包类型, self.格子id)
	end
end

function 物品数据类:烹饪显示()
	if self.名称 == "包子" then
		-- Nothing
	elseif self.名称 == "佛跳墙" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复魔法=品质*10+100")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	elseif self.名称 == "女儿红" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复愤怒值=20")
	elseif self.名称 == "烤鸭" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复气血=品质*10+100")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	elseif self.名称 == "珍露酒" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复愤怒值=品质*0.4+10")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	elseif self.名称 == "醉生梦死" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复愤怒值=品质*1")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	elseif self.名称 == "桂花丸" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复召唤兽寿命=品质*0.5")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	elseif self.名称 == "长寿面" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, "恢复召唤兽寿命=品质*2+50")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 79, "品质 " .. self.数据.品质)
	end

	if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 30, 13, self.背包类型)
	end
end

function 物品数据类:药品显示()
	if self.等级 ~= 3 then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "【功效】" .. self.功效)
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 62, self.功效 .. ",等级 " .. self.等级)
	else
		self.品质 = self.数据.品质

		if self.名称 == "千年保心丹" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复气血=品质*4+200")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复气血" .. self.品质 * 4 + 200 .. " 等级: 3")
		elseif self.名称 == "蛇蝎美人" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复魔法=品质*5+100")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复魔法" .. self.品质 * 5 + 100 .. " 等级: 3")
		elseif self.名称 == "金香玉" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复气血=品质*12+150")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复气血" .. self.品质 * 12 + 150 .. " 等级: 3")
		elseif self.名称 == "小还丹" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复气血=品质*8+100")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复气血" .. self.品质 * 8 + 100 .. " 等级: 3")
		elseif self.名称 == "佛光舍利子" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 100, self.y + self.显示高度 * 20 + 43, "【功效】复活并恢复气血=品质*3")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 等级: 3")
		elseif self.名称 == "九转回魂丹" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 100, self.y + self.显示高度 * 20 + 43, "【功效】复活并恢复气血=品质*5")
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 100, self.y + self.显示高度 * 20 + 60, "100")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 77, "品质 " .. self.品质 .. " 等级: 3")
		elseif self.名称 == "风水混元丹" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复魔法=品质*3+50")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复魔法" .. self.品质 * 3 + 50 .. " 等级: 3")
		elseif self.名称 == "十香返生丸" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复魔法=品质*3+50")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "品质 " .. self.品质 .. " 恢复魔法" .. self.品质 * 3 + 50 .. " 等级: 3")
		elseif self.名称 == "金创药" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】恢复气血400点")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "恢复气血400, 等级: 3")
		elseif self.名称 == "五龙丹" then
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 43, "【功效】解除异常状态并")
			tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + 60, "恢复气血=品质*3")
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 77, "品质 " .. self.品质 .. " 恢复气血" .. self.品质 * 3 .. " 等级: 3")
		end
	end

	if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 31, 13, self.背包类型)
	end
end

function 物品数据类:普通显示()
	if self.名称 == "魔兽要诀" or self.名称 == "高级魔兽要诀" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 119, self.y + self.显示高度 * 20 + 47, "携带技能：" .. self.数据.参数)
	elseif self.名称 == "珍珠" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 125, self.y + self.显示高度 * 20 + 20, "等级：" .. self.数据.等级)
	elseif self.名称 == "避水珠" or self.名称 == "金刚石" or self.名称 == "定魂珠" or self.名称 == "龙鳞" or self.名称 == "夜光珠" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 20, 13, self.背包类型, 1)
		end
	elseif self.名称 == "神兜兜" and 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 45, 13, self.背包类型, 1)
  elseif self.名称 == "法宝碎片" and 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
    self.发送信息 = {
      类型 = self.背包类型,
      格子 = self.格子id
    }
    客户端:发送数据(self.格子id, 207, 13, self.背包类型, 1)
	end
end

function 物品数据类:功能显示()
	if self.名称 == "藏宝图" or self.名称 == "高级藏宝图" then
		if self.名称 == "藏宝图" then
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 100, self.y + self.显示高度 * 20 + 47, "【宝图类型】：普通藏宝图")
		else
			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 100, self.y + self.显示高度 * 20 + 47, "【宝图类型】：高级藏宝图")
		end

		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 100, self.y + self.显示高度 * 20 + 67, "【宝图地点】：" .. self.数据.地图名称 .. "(" .. self.数据.x .. "," .. self.数据.y .. ")")

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			if self.数据.地图名称 ~= tp.地图.地图名称 then
				信息提示:加入提示("#y/藏宝图记载的藏宝地点是在#r/" .. self.数据.地图名称 .. "#y/，你好像走错路了")
			elseif 取两点距离({
				x = tp.屏幕.主角.xy.x / 20,
				y = tp.屏幕.主角.xy.y / 20
			}, {
				x = self.数据.x,
				y = self.数据.y
			}) > 5 then
				信息提示:加入提示("#y/宝藏就在(" .. self.数据.x .. "，" .. self.数据.y .. ")附近")
			else
				self.发送信息 = {
					类型 = self.背包类型,
					格子 = self.格子id
				}

				客户端:发送数据(self.格子id, 19, 13, self.背包类型, 1)
			end
		end
	elseif self.名称 == "元宵" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 135, self.y + self.显示高度 * 20 + 47, self.数据.参数 .. "提升")

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 37, 13, self.背包类型, 1)
		end
  elseif self.名称 == "九转金丹" then
    tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 135, self.y + self.显示高度 * 20 + 47, "【品质】 "..self.数据.品质)

    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 205, 13, self.背包类型, 1)
    end
  	elseif self.名称 == "钨金" then
  		if self.数据.等级==nil   then
  			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 27, "等级：随机获得60-160级")
  		 else
  		 	tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 27, "等级：" ..self.数据.等级)
  		end
  elseif self.名称 == "点化石" then
      tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 27, "技能："..self.数据.技能)
	elseif self.名称 == "新手玩家礼包" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,187, 13, self.背包类型, 1)
		end
	elseif self.名称 == "玄天残卷·上卷" or self.名称 == "玄天残卷·中卷" or self.名称 == "玄天残卷·下卷" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,193, 13, self.背包类型, 1)
		end
	elseif self.名称 == "双倍经验丹" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,188, 13, self.背包类型, 1)
		end
	elseif self.名称 == "120级玩家礼包" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,189, 13, self.背包类型, 1)
		end
	elseif self.名称 == "150级玩家礼包" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,190, 13, self.背包类型, 1)
		end
	elseif self.名称 == "银币礼包" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,191, 13, self.背包类型, 1)
		end
	elseif self.名称 == "储备礼包" then


		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id,192, 13, self.背包类型, 1)
		end
    elseif self.名称 == "160级玩家礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,195, 13, self.背包类型, 1)
    end
       elseif self.名称 == "三十礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,196, 13, self.背包类型, 1)
    end
       elseif self.名称 == "八十八礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,197, 13, self.背包类型, 1)
    end
       elseif self.名称 == "一八八礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,198, 13, self.背包类型, 1)
    end
         elseif self.名称 == "星辉石礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,199, 13, self.背包类型, 1)
    end
    elseif self.名称 == "太阳石礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,200, 13, self.背包类型, 1)
    end
    elseif self.名称 == "月亮石礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,216, 13, self.背包类型, 1)
    end
    elseif self.名称 == "舍利子礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,217, 13, self.背包类型, 1)
    end
    elseif self.名称 == "黑宝石礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,218, 13, self.背包类型, 1)
    end
    elseif self.名称 == "光芒石礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,219, 13, self.背包类型, 1)
    end
    elseif self.名称 == "红玛瑙礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,220, 13, self.背包类型, 1)
    end

    elseif self.名称 == "坐骑灵丹" then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 221, 13, self.背包类型, 1)
    end

    elseif self.名称 == "图册礼包" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,201, 13, self.背包类型, 1)
    end
      elseif self.名称 == "孤儿名册" then


    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id,2000, 13, self.背包类型, 1)
    end
	elseif self.名称 == "蟠桃" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 38, 13, self.背包类型, 1)
		end
	elseif self.名称 == "鬼谷子" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 135, self.y + self.显示高度 * 20 + 47, "阵型：" .. self.数据.参数)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 10, 13, self.背包类型, 1)
		end
	elseif self.名称 == "摄妖香" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 15, 13, self.背包类型, 1)
		end
	elseif self.名称 == "洞冥草" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 16, 13, self.背包类型, 1)
		end
	elseif self.名称 == "月华露" then
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 94, self.y + self.显示高度 * 20 + 35, "【功效】增加召唤兽经验=品质*2*服")
		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 100, self.y + self.显示高度 * 20 + 55, "用召唤兽等级+100")
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 100, self.y + self.显示高度 * 20 + 70, " 品质：" .. self.数据.参数)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			客户端:发送数据(self.格子id, 18, 13, self.背包类型)
		end
	elseif self.名称 == "无名秘籍" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 94, self.y + self.显示高度 * 20 + 35, "     等级：" .. self.数据.等级)
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 100, self.y + self.显示高度 * 20 + 55, "   部位：" .. self.数据.部位)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			客户端:发送数据(self.格子id, 13, 13, self.背包类型)
		end
	elseif self.名称 == "天眼通符" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 39, 13, self.背包类型, 1)
		end
	elseif self.名称 == "修炼果" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 40, 13, self.背包类型, 1)
		end
	elseif self.名称 == "回天丹" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 41, 13, self.背包类型, 1)
		end
	elseif self.名称 == "超级回天丹" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 76, 13, self.背包类型, 1)
		end
	elseif self.名称 == "新手礼包" then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 788, 13, self.背包类型, 1)
		end
	elseif self.名称 == "新玩家礼包" and self.来源 == 1 then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 132, self.y + self.显示高度 * 20 + 47, "打开等级:" .. self.数据.等级)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(4047, table.tostring(self.发送信息))
		end
	elseif self.名称 == "小铲子" and self.来源 == 1 then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 132, self.y + self.显示高度 * 20 + 47, "每次使用消耗20点体力")

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 17, 13, self.背包类型)
		end
	elseif self.名称 == "人参果" and self.来源 == 1 then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 132, self.y + self.显示高度 * 20 + 47, "消除属性：" .. self.数据.参数)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(4051, table.tostring(self.发送信息))
			客户端:发送数据(self.格子id, 42, 13, self.背包类型, 1)
		end
	elseif self.名称 == "海马" and self.来源 == 1 then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 21, 13, self.背包类型)
		end
	elseif self.名称 == "如意棒" and self.来源 == 1 then
		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 43, 13, self.背包类型, 1)
		end
  elseif self.名称 == "飞行卷" and self.来源 == 1 then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 44, 13, self.背包类型, 1)
    end
  elseif self.名称 == "分解符" and self.来源 == 1 then
    if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
      self.发送信息 = {
        类型 = self.背包类型,
        格子 = self.格子id
      }

      客户端:发送数据(self.格子id, 204, 13, self.背包类型, 1)
    end
	elseif self.名称 == "上古锻造图策" then
		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + 45, "种类 " .. self.数据.参数 .. ", 等级 " .. self.数据.等级)

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 25, 13, self.背包类型)
		end
	elseif self.名称 == "摇钱树树苗" and 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
		self.发送信息 = {
			类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 60, 13, self.背包类型)
	end
end
function 物品数据类:召唤兽饰品显示()

		if 引擎.鼠标弹起(键盘符号.右键) and self.来源 == 1 then
			self.发送信息 = {
				类型 = self.背包类型,
				格子 = self.格子id
			}

			客户端:发送数据(self.格子id, 194, 13, self.背包类型, 1)
		end

end
function 物品数据类:装备显示()

 tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x+120,self.y+self.显示高度*20+60,"【装备角色】 "..self.角色)
 tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x+120,self.y+self.显示高度*20+45,"【装备条件】 等级"..self.数据.等级)

 self.临时高度=105
 if self.数据.鉴定 then
      if self.类型=="武器" then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级.." 五行 "..self.数据.五行)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"伤害 +"..self.数据.伤害.." 命中 +"..self.数据.命中)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))
        elseif self.类型=="男衣" or self.类型=="女衣" or self.类型=="衣服" then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级.." 五行 "..self.数据.五行)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"防御 +"..self.数据.防御)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))

       elseif self.类型=="项链"  then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"灵力 +"..self.数据.灵力)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))
       elseif self.类型=="头盔" or self.类型=="发钗" then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级.." 五行 "..self.数据.五行)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"魔法 +"..self.数据.魔法.." 防御 +"..self.数据.防御)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))
        elseif self.类型=="鞋子"  then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级.." 五行 "..self.数据.五行)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"防御 +"..self.数据.防御.." 敏捷 +"..self.数据.敏捷)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))
        elseif self.类型=="腰带"  then
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+75,"等级 "..self.数据.等级)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+90,"气血 +"..self.数据.气血.." 防御 +"..self.数据.防御)
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+105,"耐久度 "..math.floor(self.数据.耐久度))
         end


         if self.数据.修理失败~=nil  then

           tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+210,self.y+self.显示高度*20+105+self.临时高度,"修理失败"..self.数据.修理失败.."次")
           end



       if 引擎.鼠标弹起(右键) and 引擎.按键按住(0x10) and self.来源==1  then

        tp.主界面.界面数据[48]:打开(self.数据,self.说明,self.大动画,self.格子id)
     elseif 引擎.鼠标弹起(右键) and self.来源==1  then
         self.发送信息={当前类型=self.背包类型,格子=self.格子id}
         客户端:发送数据(self.格子id,2,13,self.背包编号)

         end
   --显示三围

   self.锻造内容=""

   if #self.数据.锻造数据>0 then

       self.临时高度=self.临时高度+18
       if #self.数据.锻造数据==1 then

         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"锻炼等级 "..self.数据.锻造数据.等级.." 镶嵌宝石 "..self.数据.锻造数据[1].名称)
        else
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"锻炼等级 "..self.数据.锻造数据.等级.." 镶嵌宝石 "..self.数据.锻造数据[1].名称)
         self.临时高度=self.临时高度+15
         tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"、"..self.数据.锻造数据[2].名称)



         end

       for n=1,#self.数据.锻造数据 do

         if self.数据.锻造数据[n].数值>0 then

           self.锻造内容=self.锻造内容.." "..self.数据.锻造数据[n].类型.." +"..self.数据.锻造数据[n].数值

           end


         end

         if self.锻造内容~="" then

           self.临时高度=self.临时高度+15

           tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.锻造内容)


           end


     end
     self.锻造内容=""

       if self.数据.临时效果~=nil then
         self.临时高度=self.临时高度+15

         tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度," 临时"..self.数据.临时效果.类型.." "..self.数据.临时效果.数值.."  "..临时时间(self.数据.临时效果.时间))


         end




         for n=1,#self.数据.三围属性 do

             if self.数据.三围属性[n].数值~=0 then

                 self.锻造内容=self.锻造内容.." "..self.数据.三围属性[n].类型.." +"..self.数据.三围属性[n].数值

               end


           end

          if self.锻造内容~="" then
             self.临时高度=self.临时高度+15

              tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+113,self.y+self.显示高度*20+self.临时高度,self.锻造内容)
          end

    --
    if self.数据.特技~=nil then
             self.临时高度=self.临时高度+15

              tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255,80,200,255)):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"特技 "..self.数据.特技)
          end

          if self.数据.特效~=nil  then

             for n=1,#self.数据.特效 do
                self.临时高度=self.临时高度+15

                tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255,80,200,255)):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"特效 "..self.数据.特效[n])
              end
          end

           if self.数据.专用~=nil then
             self.临时高度=self.临时高度+15

              tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255,80,200,255)):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"玩家"..self.数据.专用.."专用")
          end

          if self.数据.制造者~=nil then
             self.临时高度=self.临时高度+15

              tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.制造者)
          end
        if self.数据.套装~=nil then
             self.临时高度=self.临时高度+15

              tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255,80,200,255)):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"套装效果:"..self.数据.套装.类型..self.数据.套装.名称)
          end
               if  self.数据.附魔~=nil then
             self.临时高度=self.临时高度+15
              tp.主界面.界面数据[3].道具参数:置颜色(0xFFFF8040):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.附魔.类型.."+"..self.数据.附魔.属性.."  "..临时时间(self.数据.附魔.时间))
          end

       if  self.数据.熔炼~=nil then
             self.临时高度=self.临时高度+15
            tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"熔炼效果:")
            self.临时高度=self.临时高度+15
        if #self.数据.熔炼==1 then
           tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[1].正负..self.数据.熔炼[1].属性..self.数据.熔炼[1].类型.." ")
         elseif #self.数据.熔炼==2 then
                       tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[1].正负..self.数据.熔炼[1].属性..self.数据.熔炼[1].类型.." "..self.数据.熔炼[2].正负..self.数据.熔炼[2].属性..self.数据.熔炼[2].类型.." ")

         elseif #self.数据.熔炼==3 then

                       tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[1].正负..self.数据.熔炼[1].属性..self.数据.熔炼[1].类型.." "..self.数据.熔炼[2].正负..self.数据.熔炼[2].属性..self.数据.熔炼[2].类型.." ")
                         self.临时高度=self.临时高度+15
                       tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[3].正负..self.数据.熔炼[3].属性..self.数据.熔炼[3].类型.." ")

         elseif #self.数据.熔炼==4 then

                       tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[1].正负..self.数据.熔炼[1].属性..self.数据.熔炼[1].类型.." "..self.数据.熔炼[2].正负..self.数据.熔炼[2].属性..self.数据.熔炼[2].类型.." ")
                         self.临时高度=self.临时高度+15
                       tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.熔炼[3].正负..self.数据.熔炼[3].属性..self.数据.熔炼[3].类型.." "..self.数据.熔炼[4].正负..self.数据.熔炼[4].属性..self.数据.熔炼[4].类型.." ")

        end

       end

         if self.数据.符石~=nil and self.数据.符石.最小孔数 ~= 0  then
               self.临时高度=(self.临时高度+15 )*(#self.数据+1)
               tp.主界面.界面数据[3].道具参数:置颜色(0xFF00FF00):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"开运孔数："..self.数据.符石.最小孔数.."/"..self.数据.符石.最大孔数)
               for i=1,5 do
                      if self.数据.符石[i]~= nil then
                                self.临时高度=self.临时高度+15
                                 local sx ={}
                                 sx[i] = ""
                                local 气血={}
                                气血[i] = self.数据.符石[i].气血
                                local 魔法={}
                                魔法[i] = self.数据.符石[i].魔法
                                local 命中={}
                                命中[i] = self.数据.符石[i].命中
                                local 伤害={}
                                伤害[i] = self.数据.符石[i].伤害
                                local 防御={}
                                防御[i] = self.数据.符石[i].防御
                                local 速度={}
                                速度[i] = self.数据.符石[i].速度
                                local 躲避={}
                                躲避[i] = self.数据.符石[i].躲避
                                local 灵力={}
                                灵力[i] = self.数据.符石[i].灵力
                                local 敏捷={}
                                敏捷[i] = self.数据.符石[i].敏捷
                                local 体质={}
                                体质[i] = self.数据.符石[i].体质
                                local 耐力={}
                                耐力[i] = self.数据.符石[i].耐力
                                local 法术防御={}
                                法术防御[i] = self.数据.符石[i].法术防御
                                local 魔力={}
                                魔力[i] = self.数据.符石[i].魔力
                                local 力量={}
                                力量[i] = self.数据.符石[i].力量
                                local 法术伤害={}
                                法术伤害[i] = self.数据.符石[i].法术伤害
                                local 固定伤害={}
                                固定伤害[i] = self.数据.符石[i].固定伤害

                                if 体质[i] ~= 0 and 体质[i] ~= nil  then
                                    sx[i] = sx[i].."体质 +"..体质[i].." "
                                end
                                if 耐力[i] ~= 0 and 耐力[i] ~= nil  then
                                    sx[i] = sx[i].."耐力 +"..耐力[i].." "
                                end
                                if 法术防御[i] ~= 0 and 法术防御[i] ~= nil  then
                                    sx[i] = sx[i].."法术防御 +"..法术防御[i].." "
                                end
                                if 魔力[i] ~= 0 and 魔力[i] ~= nil  then
                                    sx[i] = sx[i].."魔力 +"..魔力[i].." "
                                end
                                if 力量[i] ~= 0 and 力量[i] ~= nil  then
                                    sx[i] = sx[i].."力量 +"..力量[i].." "
                                end
                                if 法术伤害[i] ~= 0 and 法术伤害[i] ~= nil  then
                                    sx[i] = sx[i].."法术伤害 +"..法术伤害[i].." "
                                end
                                if 固定伤害[i] ~= 0 and 固定伤害[i] ~= nil  then
                                    sx[i] = sx[i].."固定伤害 +"..固定伤害[i].." "
                                end
                                if 伤害[i] ~= 0 and 伤害[i] ~= nil  then
                                    sx[i] = sx[i].."伤害 +"..伤害[i].." "
                                end
                                if 命中[i] ~= 0  and 命中[i] ~= nil then
                                    sx[i] = sx[i].."命中 +"..命中[i].." "
                                end
                                if 防御[i] ~= 0 and 防御[i] ~= nil  then
                                    sx[i] = sx[i].."防御 +"..防御[i].." "
                                end
                                if 气血[i] ~= 0 and 气血[i] ~= nil then
                                    sx[i] = sx[i].."气血 +"..气血[i].." "
                                end
                                if 魔法[i] ~= 0 and 魔法[i] ~= nil  then
                                    sx[i] = sx[i].."魔法 +"..魔法[i].." "
                                end
                                if 速度[i] ~= 0 and 速度[i] ~= nil  then
                                    sx[i] = sx[i].."速度 +"..速度[i].." "
                                end
                                if 躲避[i] ~= 0 and 躲避[i] ~= nil  then
                                    sx[i] = sx[i].."躲避 +"..躲避[i].." "
                                end
                                if 灵力[i] ~= 0 and 灵力[i] ~= nil  then
                                    sx[i] = sx[i].."灵力 +"..灵力[i].." "
                                end
                                if 敏捷[i] ~= 0 and 敏捷[i] ~= nil  then
                                    sx[i] = sx[i].."敏捷 +"..敏捷[i].." "
                                end
                                if sx[i] ~= "" then
                                             tp.主界面.界面数据[3].道具参数:置颜色(0xFF00FF00):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"符石："..sx[i])

                                end
                      end
               end
                if  self.数据.符石.组合~=nil then
                 self.临时高度=self.临时高度+15
                tp.主界面.界面数据[3].道具参数:置颜色(0xFFFF80FF):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"符石组合："..self.数据.符石.组合.名称)
                 self.临时高度=self.临时高度+15
                tp.主界面.界面数据[3].道具参数:置颜色(0xFFFF80FF):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"门派条件：无")
                 self.临时高度=self.临时高度+15
                tp.主界面.界面数据[3].道具参数:置颜色(0xFFFF80FF):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"部位条件：无")
                 self.临时高度=self.临时高度+15
                tp.主界面.界面数据[3].道具参数:置颜色(0xFFFF80FF):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self:取符石介绍(self.数据.符石.组合))
                end
          end

       if self.数据.幻化属性~=nil then
          self.临时高度=self.临时高度+15
           tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,"幻化效果：")

           self.临时高度=self.临时高度+15


             tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.幻化属性.基础.类型.." +"..self.数据.幻化属性.基础.数值)


         for n=1,#self.数据.幻化属性.附加 do
            self.临时高度=self.临时高度+15
            tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.幻化属性.附加[n].类型.." +"..self.数据.幻化属性.附加[n].数值)
             if self.数据.幻化属性.附加[n].强化==0 then

             tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.幻化属性.附加[n].类型.." +"..self.数据.幻化属性.附加[n].数值)

            else
              self.字符长度=string.len(tostring(self.数据.幻化属性.附加[n].类型))+string.len(tostring(self.数据.幻化属性.附加[n].数值))+3
              self.字符长度=self.字符长度*7
              tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x+120,self.y+self.显示高度*20+self.临时高度,self.数据.幻化属性.附加[n].类型.." +"..self.数据.幻化属性.附加[n].数值)
              tp.主界面.界面数据[3].道具参数:置颜色(ARGB(250,250,180,220)):显示(self.x+120+self.字符长度,self.y+self.显示高度*20+self.临时高度," [+"..self.数据.幻化属性.附加[n].强化.."]")

             end

           end
         end



    else
    	--self.鉴定按钮:更新(鼠标.x,鼠标.y)
       self.鉴定按钮:显示(self.x+50,self.y+180)
       self.鉴定按钮:更新(全局dt)
       tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x+120,self.y+self.显示高度*20+80,"等级 "..self.数据.等级)
        if self.数据.制造者~=nil then

          tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x+120,self.y+self.显示高度*20+98,self.数据.制造者)
          tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+116,"未鉴定物品 ")
        else
          tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x+120,self.y+self.显示高度*20+98,"未鉴定物品 ")

          end


    end




 end
function 物品数据类:取符石介绍(数据)
  local jies = ""
    if 数据.名称=="无心插柳" then
        if 数据.等级 == 1 then
            jies="目标的15%受到伤害,溅射的随机\n目标。"
        elseif 数据.等级 == 2 then
            jies="目标的20%受到伤害,溅射的随机\n目标。"
        elseif 数据.等级 == 3 then
          jies="目标的25%受到伤害,溅射的随机\n目标。"
        elseif 数据.等级 == 4 then
          jies="目标的30%受到伤害,溅射的随机\n目标。"
        end
    elseif 数据.名称=="万丈霞光" then
        if 数据.等级 == 1 then
            jies="增加50点恢复气血效果，包括师\n门技能和特技"
        elseif 数据.等级 == 2 then
            jies="增加80点恢复气血效果，包括师\n门技能和特技"
        elseif 数据.等级 == 3 then
          jies="增加120点恢复气血效果，包括师\n门技能和特技"
        elseif 数据.等级 == 4 then
          jies="增加200点恢复气血效果，包括师\n门技能和特技"
        end
    elseif 数据.名称=="百步穿杨" then
        if 数据.等级 == 1 then
            jies="物理类攻击时有20%的几率给目\n标额外造成200点伤害"
        elseif 数据.等级 == 2 then
            jies="物理类攻击时有20%的几率给目\n标额外造成450点伤害"
        elseif 数据.等级 == 3 then
          jies="物理类攻击时有20%的几率给目\n标额外造成600点伤害"
        elseif 数据.等级 == 4 then
          jies="物理类攻击时有20%的几率给目\n标额外造成800点伤害"
        end
    elseif 数据.名称=="隔山打牛" then
        if 数据.等级 == 1 then
            jies="法术攻击时有20%的几率临时提\n升自身80点灵力"
        elseif 数据.等级 == 2 then
            jies="法术攻击时有20%的几率临时提\n升自身120点灵力"
        elseif 数据.等级 == 3 then
          jies="法术攻击时有25%的几率临时提\n升自身170点灵力"
        elseif 数据.等级 == 4 then
          jies="法术攻击时有25%的几率临时提\n升自身200点灵力"
        end
    elseif 数据.名称=="心随我动" then
        if 数据.等级 == 1 then
            jies="遭受物理类攻击时有25%几率抵\n挡250点伤害"
        elseif 数据.等级 == 2 then
            jies="遭受物理攻击时有25%几率抵\n挡400点伤害"
        elseif 数据.等级 == 3 then
          jies="遭受物理类攻击时有25%几率抵\n挡700点伤害"
        elseif 数据.等级 == 4 then
          jies="遭受物理类攻击时有25%几率抵\n挡900点伤害"
        end
    elseif 数据.名称=="云随风舞" then
        if 数据.等级 == 1 then
            jies="遭受法术攻击时有20%几率抵挡\n200点伤害"
        elseif 数据.等级 == 2 then
            jies="遭受法术攻击时有20%几率抵挡\n400点伤害"
        elseif 数据.等级 == 3 then
          jies="遭受法术攻击时有20%几率抵挡\n700点伤害"
        elseif 数据.等级 == 4 then
          jies="遭受法术攻击时有20%几率抵挡\n800点伤害"
        end
    elseif 数据.名称=="无懈可击" then
        jies="提升自身30点防御力"
    elseif 数据.名称=="望穿秋水" then
        jies="提升自身30点灵力"
    elseif 数据.名称=="万里横行" then
        jies="提升自身40点伤害"
    elseif 数据.名称=="日落西山" then
        jies="提升自身40点速度"
    elseif 数据.名称=="网罗乾坤" then
        if 数据.等级 == 1 then
        jies="使用天罗地网时，增加人物等级\n/2的伤害"
        elseif 数据.等级 == 2 then
        jies="使用天罗地网时，增加人物等级\n/1.5的伤害"
        elseif 数据.等级 == 3 then
        jies="使用天罗地网时，增加人物等级\n的伤害"
        end
    elseif 数据.名称=="石破天惊" then
        if 数据.等级 == 1 then
            jies="使用落雷符时增加人物等级/2\n的伤害，装备该组合时降低5%的\n防御"
        elseif 数据.等级 == 2 then
            jies="使用落雷符时增加人物等级/1.5\n的伤害，装备该组合时降低5%的\n防御"
        elseif 数据.等级 == 3 then
          jies="使用落雷符时增加人物等级\n的伤害，装备该组合时降低5%的\n防御"
        end
    elseif 数据.名称=="天雷地火" then
        if 数据.等级 == 1 then
            jies="使用天雷斩、雷霆万钧时增加人\n物等级/2的伤害，装备该组合时\n降低5%的防御"
        elseif 数据.等级 == 2 then
            jies="使用天雷斩、雷霆万钧时增加人\n物等级/1.5的伤害，装备该组合时\n降低5%的防御"
        elseif 数据.等级 == 3 then
          jies="使用天雷斩、雷霆万钧时增加人\n物等级的伤害，装备该组合时\n降低5%的防御"
        end
    elseif 数据.名称=="烟雨飘摇" then
        if 数据.等级 == 1 then
            jies="使用烟雨剑法、飘渺式时增加人\n物等级/2的伤害，装备该组合时\n降低5%的防御"
        elseif 数据.等级 == 2 then
            jies="使用烟雨剑法、飘渺式时增加人\n物等级/1.5的伤害，装备该组合时\n降低5%的防御"
        elseif 数据.等级 == 3 then
          jies="使用烟雨剑法、飘渺式时增加人\n物等级的伤害，装备该组合时\n降低5%的防御"
        end
    elseif 数据.名称=="索命无常" then
        if 数据.等级 == 1 then
            jies="使用阎罗令时，增加人物等级/2\n的伤害，装备该组合时降低5%的\n防御"
        elseif 数据.等级 == 2 then
            jies="使用阎罗令时，增加人物等级/\n1.5的伤害，装备该组合时降低5%的\n防御"
        elseif 数据.等级 == 3 then
          jies="使用阎罗令时，增加人物等级的\n伤害，装备该组合时降低5%的\n防御"
        end
    elseif 数据.名称=="行云流水" then
        if 数据.等级 == 1 then
            jies="使用五行法术时，增加人物等级\n/2的伤害，装备该组合时降低\n5%的防御"
        elseif 数据.等级 == 2 then
            jies="使用五行法术时，增加人物等级\n/1.5的伤害，装备该组合时降低\n5%的防御"
        elseif 数据.等级 == 3 then
          jies="使用五行法术时，增加人物等级\n的伤害，装备该组合时降低\n5%的防御"
        end
    elseif 数据.名称=="福泽天下" then
        if 数据.等级 == 1 then
            jies="使用唧唧歪歪时，增加人物等级\n/2的伤害，装备该组合时降低5%\n的防御"
        elseif 数据.等级 == 2 then
            jies="使用唧唧歪歪时，增加人物等级\n/2的伤害，装备该组合时降低5%\n的防御"
        elseif 数据.等级 == 3 then
          jies="使用唧唧歪歪时，增加人物等级\n/2的伤害，装备该组合时降低5%\n的防御"
        end
    elseif 数据.名称=="暗度陈仓" then
        jies="受到物理攻击时，降低3%的所受\n伤害。"
    elseif 数据.名称=="点石成金" then
        jies="防御时，遭受物理攻击所受到的\n伤害降低20%"
    elseif 数据.名称=="化敌为友" then
        jies="受到法术攻击时，降低3%的所受\n伤害。"
    elseif 数据.名称=="凤舞九天" then
        jies="提升自身500点灵力"
    elseif 数据.名称=="君临天下" then
        jies="提升自身500点伤害"
    elseif 数据.名称=="势如破竹" then
        jies="提升自身500点敏捷"
    elseif 数据.名称=="不动明王" then
        jies="提升自身300点防御和500点体质"
    elseif 数据.名称=="高山流水" then
        if 数据.等级 == 1 then
            jies="增加人物等级/3+30的法术伤害。\n(该组合全身只有一件装备起效)"
        elseif 数据.等级 == 2 then
            jies="增加人物等级/2+30的法术伤害。\n(该组合全身只有一件装备起效)"
        elseif 数据.等级 == 3 then
          jies="增加人物等级+30的法术伤害。\n(该组合全身只有一件装备起效)"
        end
    elseif 数据.名称=="百无禁忌" then
        if 数据.等级 == 1 then
            jies="提高自身4%对抗封印类技能的能\n力"
        elseif 数据.等级 == 2 then
            jies="提高自身8%对抗封印类技能的能\n力"
        elseif 数据.等级 == 3 then
          jies="提高自身12%对抗封印类技能的能\n力"
        end
   end
  return jies
end

function 物品数据类:灵饰显示()
	self.临时高度 = self.高度
	self.临时高度 = self.临时高度 + 15

	tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "【装备条件】等级" .. self.数据.等级)

	self.临时高度 = self.临时高度 + 15

	tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "【灵饰类型】" .. self.数据.类型)

	self.临时高度 = self.临时高度 + 15

	tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "等级 " .. self.数据.等级)

	if self.数据.幻化属性 ~= nil then
		self.临时高度 = self.临时高度 + 15

		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, self.数据.幻化属性.基础.类型 .. " +" .. self.数据.幻化属性.基础.数值)

		self.临时高度 = self.临时高度 + 15

		tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "耐久度 " .. self.数据.耐久)

		if self.数据.幻化等级 > 0 then
			self.临时高度 = self.临时高度 + 15

			tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "精练等级 " .. self.数据.幻化等级)
		end

		if self.数据.特效 ~= nil then
			self.临时高度 = self.临时高度 + 15

			tp.主界面.界面数据[3].道具参数:置颜色(ARGB(255, 80, 200, 255)):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "特效:" .. self.数据.特效)
		end

		for n = 1, #self.数据.幻化属性.附加, 1 do
			self.临时高度 = self.临时高度 + 15

			tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, self.数据.幻化属性.附加[n].类型 .. " +" .. self.数据.幻化属性.附加[n].数值)

			if self.数据.幻化属性.附加[n].强化 == 0 then
				tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, self.数据.幻化属性.附加[n].类型 .. " +" .. self.数据.幻化属性.附加[n].数值)
			else
				self.字符长度 = string.len(tostring(self.数据.幻化属性.附加[n].类型)) + string.len(tostring(self.数据.幻化属性.附加[n].数值)) + 3
				self.字符长度 = self.字符长度 * 7

				tp.主界面.界面数据[3].道具参数:置颜色(绿色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, self.数据.幻化属性.附加[n].类型 .. " +" .. self.数据.幻化属性.附加[n].数值)
				tp.主界面.界面数据[3].道具参数:置颜色(ARGB(250, 250, 180, 220)):显示(self.x + 120 + self.字符长度, self.y + self.显示高度 * 20 + self.临时高度, " [+" .. self.数据.幻化属性.附加[n].强化 .. "]")
			end
		end
	end

	if self.数据.制造者 ~= nil then
		self.临时高度 = self.临时高度 + 15

		tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "制造者： " .. self.数据.制造者)
	end

	if 引擎.鼠标弹起(右键) and self.来源 == 1 then
		self.发送信息 = {
			当前类型 = self.背包类型,
			格子 = self.格子id
		}

		客户端:发送数据(self.格子id, 75, 13, self.背包编号)
	end
end
function 物品数据类:锦衣显示()
  self.临时高度 = self.高度
  if self.数据.名称 == "浪淘纱·月白" or self.数据.名称 == "齐天大圣"or self.数据.名称 == "浪淘纱"or self.数据.名称 == "冰灵蝶翼·月笼"or self.数据.名称 == "碧华锦·凌雪"  then
   self.临时高度 = self.临时高度 + 15
  tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "没什么, 就是帅") --【效果】增加10%的气血上限
  elseif  self.数据.名称 == "华风汉雅" or self.数据.名称 == "夜影"or self.数据.名称 == "夏日清凉"or self.数据.名称 == "萌萌小厨" then
    self.临时高度 = self.临时高度 + 15
  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "没什么, 就是酷") --【效果】增加10%的气血上限\n        且全属性增加50点
  elseif  self.数据.名称 == "尖叫服" or self.数据.名称 == "锦绣幻梦" or self.数据.名称 == "萤雪轻裘"or self.数据.名称 == "铃儿叮当"or self.数据.名称 == "顽皮小恶魔" then
    self.临时高度 = self.临时高度 + 15
  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 20 + self.临时高度, "没什么, 就是拽") --【效果】增加10%的气血上限\n        且全属性增加100点
  elseif  self.数据.部位 == "光环"  then
     self.临时高度 = self.临时高度 + 15
  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 40 + self.临时高度, "没什么, 就是炫") --增加10%的伤害和灵力
  tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x + 120, self.y + self.显示高度 * 30 + self.临时高度, "部位：光环")
  elseif  self.数据.部位 == "足迹" then
     self.临时高度 = self.临时高度 + 15
  tp.主界面.界面数据[3].道具参数:置颜色(黄色):显示(self.x + 120, self.y + self.显示高度 * 40 + self.临时高度, "没什么, 就是爽") --增加10%的防御和速度
  tp.主界面.界面数据[3].道具参数:置颜色(白色):显示(self.x + 120, self.y + self.显示高度 * 30 + self.临时高度, "部位：足迹")
  tp.主界面.界面数据[3].道具参数:置颜色(红色):显示(self.x + 120, self.y + self.显示高度 * 50 + self.临时高度, "装备后, 只有走动时\n才在身后出现。")

  end



  if 引擎.鼠标弹起(右键) and self.来源 == 1 then
    self.发送信息 = {
      当前类型 = self.背包类型,
      格子 = self.格子id
    }

    客户端:发送数据(self.格子id, 203, 13, self.背包编号)
  end
end
return 物品数据类
