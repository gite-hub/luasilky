
local 地图类 = class()
local jl = require("gge精灵类")
local xdt = require("Script/地图/小地图类")
local tp
function 生成XY(x,y)
  local f ={}
  f.x = tonumber(x) or 0
  f.y = tonumber(y) or 0
  setmetatable(f,{
  __add = function (a,b)
    return 生成XY(a.x + b.x,a.y + b.y)
  end,
  __sub = function (a,b)
    return 生成XY(a.x - b.x,a.y - b.y)
  end
  })
  return f
end
function 地图类:初始化(根)
 self.创建=false
 self.宽度 		= 800
 self.高度 		= 600
 self.地图单位={}
 --Script/资源类/
  self.过度纹理 = require("gge纹理类")():渲染目标(800,600)
  self.过度进度 = 155
  self.过度时间 = 30
  self.战斗动画 = 根.资源:载入('ZY.FT',"网易WDF动画",0x3D3AA29E)

 tp= 根

  -- self.posx = 0
  -- self.posy = 0
end

function 地图类:创建坐标()

 self.地图id={6001,6002,6003,4131,4132,4133,3131,3132,5135,5134,1125,1004,1005,1006,1007,1008,1090,5131,1501,1504,1505,1511,1506,1503,1507,1508,1509,1524,1532,1537,1126,1092,1142,1514,1174,1177,1178,1179,1180,1181,1182,1183,1186,1187,1188,1189,1190,1191,1192,1091,1111,1070,1135,1173,1131,1512,
1513,1146,1201,1202,1203,1204,1205,1110,1140,1122,1127,1128,1129,1130,1202,1001,1198,1002,1193,1116,1117,1118,1119,1120,1121,1505,1504,1112,1114,1117,1141,1147,1054,1043,1143,1137,1134,1144,1145,1123,1124,1123,1041,1042,1228,1229,1003,1235,1232,1242,1210,1211,1218,1221,1138,1139,1223,1216}
 self.地图坐标={}
 for n=1,#self.地图id do
   if self.地图id[n]~=5135 and self.地图id[n]~=6001 and self.地图id[n]~=6002 and self.地图id[n]~=6003 then
     self.地图坐标[self.地图id[n]]=地图坐标类.创建(self.地图id[n])
     end
   end

end

-- require"lfs"
-- function 取当前目录(dirname)--全部目录
--     return lfs.currentdir(dirname)
-- end


local maper = require("__ggemaper__")(800, 600)
function 地图类:跳转地图(编号)
  -- print('跳转地图')
   self.重叠文字=文字类(simsun,14)
 self.重叠文字:置颜色(黄色)
 玩家寻路表={}
	-- self.创建中=true
	self.npc={}
	self.npc选中=0
    self.传递数据=编号

    if self.传递数据.编号==5135 or self.传递数据.编号==6001 or self.传递数据.编号==6002 or self.传递数据.编号==6003 then

      self.传递数据.编号=5131
    elseif self.传递数据.编号>7000 and self.传递数据.编号<=7020 then
      self.传递数据.编号=1193
	 end
	 self.临时数据=self.传递数据
    -- self.char = ffi.new('char[2097152]')


  if not self.paths then
     self.paths =
     {
        [1] = 资料片it,
        [2] = 资料片ori,
        [3] = 资料片jd,
        [4] = "scene/"
     }
  end


  for k,v in ipairs(self.paths) do
    self.临时文件 = v .. self.临时数据.编号 .. ".map"
    local file = io.open(self.临时文件)
    if file ~= nil then
      self.搜索路径 = v
      io.close(file)
      break
    end
  end

  -- self.临时文件=取当前目录()..[[\wdf\scene\]]..self.临时数据.编号..".map"--全局变量.游戏目录..[[/scene/]]
	-- self.p = tonumber(ffi.cast("intptr_t",self.char))
  -- self.map = require("glow/MAP类")(self.临时文件,self.p,2097152)


  local map = maper:load(self.搜索路径, tonumber(self.临时数据.编号))
	self.宽度 		= map.mw --宽度
	self.高度 		= map.mh --高度
	self.行数 		= map.bw --行数
	self.列数 		= map.bh --列数
	self.遮罩数量   = 0 --self.map.遮罩数量 -1 --从0开始
	self.地表组 	= {}
	-- self.创建=true
	--刷新坐标
	if self.传递数据.跳转数据~=nil then
     tp.屏幕.主角.xy.x,tp.屏幕.主角.xy.y=self.传递数据.x,self.传递数据.y
     tp.屏幕.主角:停止移动()
	   end
	self.地图名称=self.临时数据.名称
	self.地图编号=self.临时数据.编号
  self.小地图=xdt.创建(self.地图编号,self.宽度,self.高度)
  -- print('小地图')
	if self.地图音乐~=nil then self.地图音乐:停止() end
 if self.临时数据.音乐~=0 then
	   self.地图音乐 = 创建音乐(self.临时数据.音乐,true)
  end
  if self.地图音乐 then self.地图音乐:置音量(全局变量.音量) self.地图音乐:播放() end --*10
  传送圈类:更新数据(self.临时数据.传送圈)

-- 	local id = 0
-- 	for h = 1 , self.行数  do
-- 		self.地表组[h] = {}
-- 		for l = 1 , self.列数  do
-- 			self.地表组[h][l] =
-- 			{
-- 				xy = 生成XY((l-1)*320,(h-1)*240) ,
-- 				id=id,
-- 				遮罩	={}
-- 			}
-- 			id=id+1
-- 		end
-- 	end

-- print('跳转地图3')
-- 	self.精灵 = self.map:取精灵(0)

-- 	self.地表组[1][1].纹理 = self.精灵:取纹理()
--   print('跳转地图3.1')
-- 	local 障碍宽度,障碍高度 = self.列数*16,self.行数*12
--   print('跳转地图3.2')
--   self.寻路 = require("Astart类")(障碍宽度,障碍高度,self.map:取障碍())
--   print('跳转地图3.3')
--   self.路径类 = 路径类:创建(障碍宽度,障碍高度,self.map:取障碍())
--   print('跳转地图3.4')
--   self.路径类:载入路径(障碍宽度,障碍高度,self.map:取障碍())

-- 	for i=0,self.遮罩数量 do
-- 		local v =  self.map:取遮罩信息(i)
-- 		local h,l = math.ceil(v.Y/240),math.ceil(v.X/320)
-- 		l = l<1 and 1  or l
-- 		h = h<1 and 1  or h
-- 		table.insert(self.地表组[h][l].遮罩,遮罩类(i,v))
-- 	end
--   print('跳转地图4')
	-- self.开始位置 	= 生成XY(1,1) --生成XY() 是GGE函数，生成的{x=0,y=0},而且重载了相加和相减
	-- self.结束位置 	= 生成XY(1,1)
	-- self.开始位置1 	= 生成XY(1,1)
	-- self.结束位置1 	= 生成XY(1,1)
	-- self.障碍精灵  	= require("gge精灵类")(0,0,0,20,20):置颜色(ARGB(80,255,0,0))
  -- self.创建中=false
  self.地图单位={}
  self.单位选中=0
  --检查是否处于障碍位置
 self.地图单位={}
 yxjc=2

 tp.屏幕:同步坐标(dt,x,y)
 tp.屏幕.玩家类.玩家组={}
 tp.屏幕.主角:停止移动()
 if tp.主界面~=nil and tp.主界面.界面数据[8]~=nil then
     tp.主界面.界面数据[8].本类开关=false
 	 end
 if tp.主界面~=nil and tp.主界面.界面数据[34]~=nil then
     tp.主界面.界面数据[34].本类开关=false
  end
 self.重叠数据={}
 self.选中数据={}
 self.重叠开关=false
 self.单位重叠=false
 self.名称重叠=false

  引擎.截图("Dat/" .. 窗口标题.id .. ".jpg")
  self.过度精灵 = jl("Dat/" .. 窗口标题.id .. ".jpg")
  self.过度进度 = 255
  self.过度时间 = 3
  tp.隐藏UI = true
  tp.恢复UI = true
  collectgarbage("collect")
end
function 地图类:添加单位(数据)
 if 数据==nil then return 0 end
 for n, v in pairs(数据) do
     if 数据[n]~=nil then
          self:添加单位2(数据[n])
         end
   end
 end


function 地图类:添加单位1(数据)
   self:添加单位2(数据)
 end
function 地图类:移除单位(数据)
 self.移除序列=0
 for n, v in pairs(self.地图单位) do
     if self.地图单位[n]~=nil and (self.地图单位[n].id+0==数据 or self.地图单位[n].id..""==数据 ) then self.移除序列=n end
     end
 if self.移除序列~=0 then
     self.地图单位[self.移除序列]=nil
     end
 end
function 地图类:添加单位2(数据)
 self.临时编号=0
 if 数据.造型==nil then return 0 end
 for n, v in pairs(self.地图单位) do
     if self.地图单位[n]==nil and self.临时编号==0 then self.临时编号=n end
     end
 if self.临时编号==0 then
     self.临时编号=#self.地图单位+1
     end
  self.地图单位[self.临时编号]={}
  if 数据.武器==nil then
  	 if 数据.变异 then
         self.变异=true
        else
         self.变异=false
  	 	 end
     self.地图单位[self.临时编号].动画=引擎.场景.召唤兽动画类.创建(数据.造型,self.变异)
     self.地图单位[self.临时编号].动画:换方向(数据.方向)
    else
     self.地图单位[self.临时编号].动画=引擎.场景.角色动画类.创建(数据.造型,数据.染色,数据.武器.名称,数据.武器.等级,数据.武器.类别,数据.武器.强化,nil,nil)
     self.地图单位[self.临时编号].动画:换方向("静立",数据.方向)
     end
  self.地图单位[self.临时编号].名称=数据.名称
  self.地图单位[self.临时编号].包围盒=包围盒:创建(150,192,64,18)
  self.地图单位[self.临时编号].包围盒:置坐标(18,30)
  self.地图单位[self.临时编号].包围盒:更新宽高(string.len(数据.名称)*9,18)
  self.地图单位[self.临时编号].坐标=数据.坐标
  self.地图单位[self.临时编号].标识=数据.标识
  self.地图单位[self.临时编号].类型=数据.类型
  self.地图单位[self.临时编号].id=数据.id
   self.地图单位[self.临时编号].称谓=数据.称谓

 end

function 地图类:添加npc(数据)
 -- print('添加NCC')
 if #数据~=0 then
     self.npc={}
     for n=1,#数据 do
          self.npc[n]={}
            local ls= 引擎.取模型(数据[n].动画)
             self.npc[n].动画=引擎.场景.资源:载入(ls[3],"网易WDF动画",ls[1])
            self.npc[n].名称=数据[n].名称
            self.npc[n].包围盒=包围盒:创建(150,192,64,18)
            self.npc[n].包围盒:置坐标(18,30)
            self.npc[n].包围盒:更新宽高(string.len(数据[n].名称)*9,18)
            self.npc[n].x=数据[n].x
            self.npc[n].y=数据[n].y
            self.npc[n].动画:置方向(数据[n].方向)
            self.npc[n].称谓=数据[n].称谓
     	 end
     end
 end


function 地图类:重叠检测()


 if self.单位重叠 or self.名称重叠 then

   return false

   end
 return true
 end



function 地图类:更新(pos,dt)
  maper:step(-tp.屏幕.xy.x, -tp.屏幕.xy.y)
	传送圈类:更新(dt)
  self.战斗动画:更新(dt)
  self.单位重叠=false
	-- if self.创建==false  then return 0 end
	local 主角位置  = 生成XY(math.ceil(pos.x/320), math.ceil(pos.y/240))
  -- 	if(主角位置.x == 1)then
	-- 	self.开始位置.x 	= 1
	-- 	self.结束位置.x 	= 3
	-- elseif(主角位置.x >= self.列数)then
	-- 	self.开始位置.x 	= 主角位置.x - 2
	-- 	self.结束位置.x 	= 主角位置.x
	-- else
	-- 	self.开始位置.x 	= 主角位置.x - 2
	-- 	self.结束位置.x 	= 主角位置.x + 2

	-- 	if(self.开始位置.x <=0)then

  --         self.开始位置.x =1
	-- 	  end
  --      if(self.结束位置.x >self.列数)then
  --         self.结束位置.x =self.列数

	-- 	  end
	-- end

	-- if(主角位置.y == 1)then
	-- 	self.开始位置.y 	= 1
	-- 	self.结束位置.y 	= 3
	-- elseif(主角位置.y >= self.行数)then
	-- 	self.开始位置.y 	= 主角位置.y -2
	-- 	self.结束位置.y 	= 主角位置.y
	-- else
	-- 	self.开始位置.y 	= 主角位置.y - 2
	-- 	if(self.开始位置.y<=0)then

  --             self.开始位置.y=1

	-- 		  end
	-- 	self.结束位置.y 	= 主角位置.y + 2

	-- 	 if(self.结束位置.y>self.行数)then
  --             self.结束位置.y=self.行数
  --             self.开始位置.y=self.行数-10
	-- 		  end
	-- end
 self.点击动作=false
 self.右键点击=false
 self.重叠数据={}
 self.起始xy={x=鼠标.x,y=鼠标.y}
 self.坐标重叠=nil
 if self.npc then
 for n=1,#self.npc do
     if 取两点距离(tp.屏幕.主角.xy,self.npc[n])<=800 then
      self.npc[n].动画:更新(dt)
      if self.npc[n].动画:是否选中(鼠标.x,鼠标.y) and tp.主界面:界面重叠()==false and self.小地图:界面重叠()==false then
          self.单位重叠=true
         if 组队开关 then
            else
             self.npc选中=n
             鼠标动画.显示序列=7
             self.重叠数据[#self.重叠数据+1]={编号=n,类型=1}
             if self.坐标重叠==nil then

               self.坐标重叠={x=self.npc[n].x,y=self.npc[n].y}
               end

             if 引擎.鼠标弹起(左键) and self.点击动作==false then
                 客户端:发送数据(n,1,6,n+1,n+2)
                 self.点击动作=true
                 self.重叠开关=false
              elseif 引擎.鼠标弹起(右键) then
                 self.右键点击=true

                 end

         	 end
        elseif self.npc选中==n then
         鼠标动画.显示序列=1
         self.npc选中=0

      	 end
        end
 	 end
end
 for n, v in pairs(self.地图单位) do
     if self.地图单位[n]~=nil then
         self.地图单位[n].动画:更新(dt,"静立")
         if self.地图单位[n].动画["静立"]:是否选中(鼠标.x,鼠标.y) and tp.主界面:界面重叠()==false and self.地图单位[n].类型~=401 then
             self.单位重叠=true
            if 组队开关 then
               else
                self.单位选中=n
                鼠标动画.显示序列=7
                self.重叠数据[#self.重叠数据+1]={编号=n,类型=2}
                if self.坐标重叠==nil then
               self.坐标重叠={x=self.地图单位[n].坐标.x,y=self.地图单位[n].坐标.y}
               end
                if 引擎.鼠标弹起(左键) and self.点击动作==false then
                   -- 客户端:发送数据(8003,self.地图单位[n].标识)
                    客户端:发送数据(0,3,6,self.地图单位[n].标识,1)
                    self.点击动作=true
                    self.重叠开关=false
                  elseif 引擎.鼠标弹起(右键) then
                    self.右键点击=true
                    end
         	    end
         	elseif self.地图单位[n].类型==401 and self.地图单位[n].动画:取包围盒("静立"):检查点(tp.屏幕.主角.xy.x+tp.屏幕.xy.x,tp.屏幕.主角.xy.y+tp.屏幕.xy.y) then
                客户端:发送数据(0,4,6,self.地图单位[n].标识,1)
            elseif self.单位选中==n  then
             鼠标动画.显示序列=1
             self.单位选中=0
      	     end
     	 end
 	 end
  if self.右键点击 and #self.重叠数据>1 then

      self.选中数据={}
      for n=1,#self.重叠数据 do

      if self.重叠数据[n].类型==1 then

        self.选中数据[n]={名称=self.npc[self.重叠数据[n].编号].名称,类型=1,编号=self.重叠数据[n].编号,x=self.坐标重叠.x+30,y=self.坐标重叠.y+n*25-25}
        self.选中数据[n].包围盒=包围盒:创建(150,192,64,18)
        self.选中数据[n].包围盒:置坐标(self.选中数据[n].x+tp.屏幕.xy.x,self.选中数据[n].y+tp.屏幕.xy.y)
        self.选中数据[n].包围盒:更新宽高(string.len(self.选中数据[n].名称)*9,18)
      else
        self.选中数据[n]={名称=self.地图单位[self.重叠数据[n].编号].名称,类型=2,编号=self.重叠数据[n].编号,x=self.坐标重叠.x+30,y=self.坐标重叠.y+n*25-25}
        self.选中数据[n].包围盒=包围盒:创建(150,192,64,18)
        self.选中数据[n].包围盒:置坐标(self.选中数据[n].x+tp.屏幕.xy.x,self.选中数据[n].y+tp.屏幕.xy.y)
        self.选中数据[n].包围盒:更新宽高(string.len(self.选中数据[n].名称)*9,18)
        end
        end
     self.重叠开关=true
     end

 end


function 地图类:显示(偏移)

  if 全局变量.地图开关 == false then return 0 end
  -- if self.创建==false  then return 0 end
  maper:render() ---偏移.x, -偏移.y
  	-- for h = self.开始位置.y, self.结束位置.y do
  	-- 	for  l = self.开始位置.x, self.结束位置.x do
  	-- 		if  self.地表组[h]~=nil then
    --         local t = self.地表组[h][l]
    --         if t~=nil then
    -- 			   if  (not t.纹理)then
    -- 				   t.纹理 =self.map:取纹理(t.id)
    -- 			   end
    -- 			   self.精灵:置纹理(t.纹理)
    -- 			   self.精灵:显示(t.xy + 偏移)
    -- 			   for i,v in ipairs(t.遮罩) do
    -- 				   tp.屏幕:加入(v:载入(self.map))
    -- 			   end
    --         end
  	-- 		 end
  	-- 	end
  	-- end

		-- local 障碍宽度,障碍高度 = math.ceil(self.宽度/20)-1,math.ceil(self.高度/20)-1
		-- local 障碍位置  = 生成XY(math.ceil(tp.屏幕.主角.xy.x/20), math.ceil(tp.屏幕.主角.xy.y/20))
		-- if(障碍位置.x < 16)then
		-- 	self.开始位置1.x= 0
		-- 	self.结束位置1.x = 32
		-- elseif(障碍位置.x >= 障碍宽度- 16)then
		-- 	self.开始位置1.x = 障碍位置.x - 32
		-- 	self.结束位置1.x = 障碍宽度
		-- else
		-- 	 self.开始位置1.x = 障碍位置.x - 16
		-- 	 self.结束位置1.x = 障碍位置.x + 16
		-- end

		-- if(障碍位置.y < 12)then
		-- 	self.开始位置1.y = 0
		-- 	self.结束位置1.y = 24
		-- elseif(障碍位置.y >= 障碍高度-12 )then
		-- 	self.开始位置1.y = 障碍高度 - 24
		-- 	self.结束位置1.y = 障碍高度
		-- else
		-- 	self.开始位置1.y = 障碍位置.y - 12
		-- 	self.结束位置1.y = 障碍位置.y + 12
		-- end
 传送圈类:显示()
 if yxjc==2 then
    if self.npc then
      for n=1,#self.npc do
          if 取两点距离(tp.屏幕.主角.xy,self.npc[n])<=800 then
             引擎.场景.影子:显示(self.npc[n].x+tp.屏幕.xy.x,self.npc[n].y+tp.屏幕.xy.y)
             self.npc[n].动画:显示(self.npc[n].x+tp.屏幕.xy.x,self.npc[n].y+tp.屏幕.xy.y)
               if self.npc[n].称谓~=nil then
                  显示npc名称(self.npc[n].名称,self.npc[n].x+tp.屏幕.xy.x-1,self.npc[n].y+12+tp.屏幕.xy.y)
                  显示人物称谓(self.npc[n].称谓,self.npc[n].x+tp.屏幕.xy.x-10,self.npc[n].y-10+tp.屏幕.xy.y)
                else
                    显示npc名称(self.npc[n].名称,self.npc[n].x+tp.屏幕.xy.x,self.npc[n].y+tp.屏幕.xy.y)
                end
            end
 	      end
      end
      for n, v in pairs(self.地图单位) do
         if self.地图单位[n]~=nil then
             self.地图单位[n].动画:显示("静立",self.地图单位[n].坐标.x+tp.屏幕.xy.x,self.地图单位[n].坐标.y+tp.屏幕.xy.y)


        if self.地图单位[n].称谓~=nil then
           显示npc名称(self.地图单位[n].名称,self.地图单位[n].坐标.x+tp.屏幕.xy.x,self.地图单位[n].坐标.y+8+tp.屏幕.xy.y)
          显示人物称谓(self.地图单位[n].称谓,self.地图单位[n].坐标.x+tp.屏幕.xy.x-5,self.地图单位[n].坐标.y-10+tp.屏幕.xy.y)
      else
           显示npc名称(self.地图单位[n].名称,self.地图单位[n].坐标.x+tp.屏幕.xy.x,self.地图单位[n].坐标.y+tp.屏幕.xy.y)
       end

           self.战斗动画:显示(self.地图单位[n].坐标.x+tp.屏幕.xy.x,self.地图单位[n].坐标.y-self.地图单位[n].动画.静立.高度*0.8+tp.屏幕.xy.y)
     	     end
 	     end
 	 end
  self.名称重叠=false

  if self.重叠开关 then
    信息提示:显示(2,8,#self.选中数据*3,self.选中数据[1].x+tp.屏幕.xy.x-10,self.选中数据[1].y+tp.屏幕.xy.y-10)
    self.允许点击=true
   for n=1,#self.选中数据 do
      self.重叠文字:显示(self.选中数据[n].x+tp.屏幕.xy.x,self.选中数据[n].y+tp.屏幕.xy.y,self.选中数据[n].名称)
      self.选中数据[n].包围盒:置坐标(self.选中数据[n].x+tp.屏幕.xy.x,self.选中数据[n].y+tp.屏幕.xy.y)
      self.选中数据[n].包围盒:显示()
     if self.选中数据[n].包围盒:检查点(鼠标.x,鼠标.y) then  self.名称重叠=true end
      if self.允许点击 and self.选中数据[n].包围盒:检查点(鼠标.x,鼠标.y) and 引擎.鼠标弹起(左键) then

        self.允许点击=false
        self.重叠开关=false

        if self.选中数据[n].类型==1 then
          客户端:发送数据(self.选中数据[n].编号,1,6,self.选中数据[n].编号+1,self.选中数据[n].编号+2)
        else

           客户端:发送数据(0,3,6,self.地图单位[self.选中数据[n].编号].标识,1)
          end
        end
     end
  end

end

function 地图类:显示遮罩(偏移)
  maper:renderAlpha() -- -偏移.x, -偏移.y
end


function 地图类:检查点(x,y)
    return not maper:isObstacle(x,y)
end

function 地图类:寻路(xy,txy)
    xy=require("gge坐标类")(xy.x,xy.y)
    txy=require("gge坐标类")(txy.x,txy.y)
    local x,y       = self:坐标算格子(xy)
    local tx,ty     = self:坐标算格子(txy)

    if self:判断直线障碍(xy,txy) then
        if not self:检查点(tx,ty) then--目标是障碍
            tx,ty = self:坐标算格子(self:最近坐标(txy) or xy)
        end
        if not self:检查点(x,y) then--卡障碍
            x,y = self:坐标算格子(self:最近坐标(xy))
        end
        --self.l_A:取路径(x,y,tx,ty)
        -- maper:search(math.floor(v.x / 20), math.floor(v.y / 20), math.floor(t.x / 20), math.floor(t.y / 20))
        self.l_路径 =  maper:search(x,y,tx,ty)
    else
        self.l_路径 = {require("gge坐标类")(txy.x/20,txy.y/20)}
    end

    return self.l_路径
end


function 地图类:判断直线障碍(xy,txy)
    local 距离 = xy:取距离(txy)-10
    local 孤度 = xy:取弧度(txy)

    local 坐标
    repeat
        坐标 = xy:取距离坐标(距离,孤度)
        if not self:检查点(self:坐标算格子(坐标))then
            return true
        end
        距离 = 距离 -10
    until 距离<0
    return false
end
function 地图类:最近坐标(xy)--最近可以行走的坐标
    local 距离 = 20
    local 坐标
    repeat
        for i=0,6,0.5 do
            坐标 = xy:取距离坐标(距离,i)
            if self:检查点(self:坐标算格子(坐标))then
                return 坐标
            end
        end
        距离 = 距离+20
    until 距离>500
end
function 地图类:坐标算格子(x,y)

    if type(x) == 'table' then
        y,x = x.y,x.x
    else
       if x==nil or y==nil then return {x=1,y=1} end
    end
    local rx,ry = x/20,y/20
    return math.floor(x/20),math.floor(y/20)
end
function 地图类:格子算坐标(x,y)
    if type(x) == 'table' then
        y,x = x.y,x.x
    end
    local rx,ry = x*20,y*20
    return require("gge坐标类")(rx,ry)
end

return 地图类