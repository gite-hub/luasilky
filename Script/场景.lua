--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19//
-- @Last Modified time: 2020-05-05 05:45:49
--======================================================================--
local 场景类_场景 = class()
local ceil = math.ceil
local floor = math.floor
local sort = table.sort
-- local require = require
-- local pairs = pairs
local insert = table.insert
local txk = 引擎.特效库
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起
local ARGB = ARGB
-- local type = type
local remove = table.remove
local xys = 生成XY

local 杀星出现计时 = 0
local tp
function 场景类_场景:qfjmc(子类,级别限制,名称)
	if 子类 == "枪" then
		if 级别限制 < 21 then
			return "红缨枪"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "乌金三叉戟"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "玄铁矛"
		end
	elseif 子类 == "斧" then
		if 级别限制 < 21 then
			return "青铜斧"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "双弦钺"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "乌金鬼头镰"
		end
	elseif 子类 == "剑" then
		if 级别限制 < 21 then
			return "青铜短剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "青锋剑"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "游龙剑"
		end
	elseif 子类 == "双" then
		if 级别限制 < 21 then
			return "双短剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "竹节双剑"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "赤焰双剑"
		end
	elseif 子类 == "飘" then
		if 级别限制 < 21 then
			return "五色缎带"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "无极丝"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "七彩罗刹"
		end
	elseif 子类 == "爪" then
		if 级别限制 < 21 then
			return "铁爪"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "青龙牙"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "青刚刺"
		end
	elseif 子类 == "扇" then
		if 级别限制 < 21 then
			return "折扇"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "铁面扇"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "神火扇"
		end
	elseif 子类 == "棒" then
		if 级别限制 < 21 then
			return "细木棒"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "点金棒"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "满天星"
		end
	elseif 子类 == "锤" then
		if 级别限制 < 21 then
			return "松木锤"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "狼牙锤"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "震天锤"
		end
	elseif 子类 == "鞭" then
		if 级别限制 < 21 then
			return "牛皮鞭"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "钢结鞭"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "青藤柳叶鞭"
		end
	elseif 子类 == "环" then
		if 级别限制 < 21 then
			return "黄铜圈"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "金刺轮"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "蛇形月"
		end
	elseif 子类 == "刀" then
		if 级别限制 < 21 then
			return "柳叶刀"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "金背大砍刀"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "狼牙刀"
		end
	elseif 子类 == "法杖" then
		if 级别限制 < 21 then
			return "曲柳杖"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "墨铁拐"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "腾云杖"
		end
	elseif 子类 == "弓弩" then
		if 级别限制 < 21 then
			return "硬木弓"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "宝雕长弓"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "连珠神弓"
		end
	elseif 子类 == "宝珠" then
		if 级别限制 < 21 then
			return "琉璃珠"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "翡翠珠"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "如意宝珠"
		end
	elseif 子类 == "巨剑" then
		if 级别限制 < 21 then
			return "钝铁重剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "壁玉长铗"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "惊涛雪"
		end
	elseif 子类 == "伞" then
		if 级别限制 < 21 then
			return "红罗伞"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "琳琅盖"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "金刚伞"
		end
	elseif 子类 == 18 then
		if 级别限制 < 21 then
			return "素纸灯"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			return "如意宫灯"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			return "玉兔盏"
		end
	end
	return 名称
end

function 场景类_场景:可通行()
	return not self.人物选中 and not self.选中UI and not self.按钮焦点 and not self.消息栏焦点 and  not self.禁止通行 and not self.下一次确定 and not self.按下中 and not self.第一窗口移动中 and not self.第二窗口移动中
end
function 场景类_场景:初始化()
   -- self.内存计次=0
   -- self.项目目录  = 取当前目录()
   -- -- self.内存 = require "xyxy"
   -- -- 置当前目录(self.项目目录)
   -- self.多标签=require "xymh"
   --  置当前目录(self.项目目录)
   --  print(require "xymh")
    -- self.多标签.初始载入(self.项目目录,tonumber(引擎.取窗口句柄()))--多标签启
    -- self.内存.系统内存优化()

	黄色 = 4294967040.0
	黑色 = 4278190080.0
	黄色 = 4294967040.0
	绿色 = 4278255360.0
	红色 = 4294901760.0
	白色 = 4294967295.0
	紫色 = 4285211534.0
	蓝色 = 4278190335.0

	队伍id = 0
	游戏时间 = 0
	回收时间 = 0
	调试模式 = false
	门派限制 = false
	客户端连接 = 0
	停止连接 = false
	连接状态 = false
	数据输出 = false
	随机序列 = 0
	数据包流 = 0
	数据包流1 = 0
	数据包时间 = os.time()

	-- 测试文本 = "a我啊"
    客户端 = require("Script/客户端类").创建()
	self.资源缓存 = {} -- 已加载的was资源将保存在这里，避免重复读取

    self._滑块 =  require("script/系统类/滑块")
	self._自适应 = require("script/系统类/自适应")
	self._按钮 = require("script/系统类/按钮")
	self._小型选项栏 = require("script/系统类/小型选项栏")
	self._丰富文本 = require("script/系统类/丰富文本")
	self._列表 = require("script/系统类/列表")
	self._物品格子 = require("script/系统类/物品_格子")
	self._技能格子 = require("script/系统类/技能_格子")
	self._内丹格子 = require("script/系统类/内丹_格子")
	self.资源 = require("script/资源类/加载类")(self)


	self.资源:打开()

	-- ffi函数.信息框("-1", "0")

	local 资源 = self.资源
	local wz = require("gge文字类")

	-- ffi函数.信息框("0", "0")

	self.字体表 = {
		普通字体 = wz.创建(simsun,14,false,false,false),
		普通字体1 = wz.创建(simsun,16,false,false,false),
		普通字体_ = wz.创建(simsun,13,false,false,false),
		普通字体__ = wz.创建(simsun,12.5,false,false,false),
		描边字体 = wz.创建(simsun,16,false,true,false),
		道具字体 = wz.创建(simsun,21,true,false,true),
		人物字体 = wz.创建(simsun,16,false,false,true),
		人物字体_ = wz.创建(simsun,16,false,false,false),
		文书字体 = wz.创建(simsun,18,false,false,true),
		窗口字体 = wz.创建(simsun,16,false,true,false),
		登入字体 =wz.创建(simsun,14,false,false,false),

	};wz=nil
	self.字体表.道具字体:置字间距(3.8)
	self.字体表.描边字体:置描边颜色(-16777216)
	self.字体表.描边字体:置颜色(4294967295)
	-- 测试
	local fc = self._自适应.创建()
	fc:加载(self)
		yxjc = 1
	 	local bqs = {1295847816,1853525647,1076954610,3552721044,3990239921,3366615112,2782337201,3771134163,1462708813,
	2715893287,3002376600,3991654351,993860032,2666294756,454787878,1382010105,2912848813,3919700593,477926852,352380776,
	811687356,915719759,4249060934,1336751228,1950021903,2235513801,3594739784,1960900090,2382390242,1537855326,3907030953,
	2290431679,3202252097,4033571742,1492820992,655187057,2901001332,3190903022,298183232,1945354141,3436623848,1724964988,
	2592865169,3393696884,1494002331,1310769894,1152800681,2542768010,1378600591,2829693277,3846038890,3901444948,68667698,
	3331959614,432707334,4241851683,3448703702,2542506275,241536076,314448958,2242677963,1887092560,1489631920,2860202818,
	4076591726,4232189420,3498505860,4241140149,3858705890,2271353759,3452523393,963399171,2959831232,2917226350,4215743335,
	3987108486,365569753,3701218951,3044567112,3265766525,2129343522,2401287726,2716317030,292723042,4014574629,4183102172,
	4115700508,2139528734,276624883,4099788650,2578443618,3367759523,576638850,3586214754,2830927389,1915332364,1341579386,
	3743372972,1217313750,1208397371,156713767,2057251015,3084935361,1485268859,470535714,2868631088,3835251562,2896374671,
	3982186902,1708428735,2448085336,1354708809,943667221,1146784672,3592760724,611393967,1688780051,2042812914,1466206851,
	3612578974}

	-- ffi函数.信息框("1", "1")

	self.包子表情动画 = {}
	for i=0,119 do
	    self.包子表情动画[i] = 资源:载入('ZY.FT',"网易WDF动画",bqs[i+1])
	end
	self.提示框 = self._自适应.创建(6,1,1,1,3,9)
   	self._技能 = require("script/系统类/技能")
    self.外部聊天框    = 资源:载入("Dat/pic/wb.png","图片")
	self.物品格子焦点_ = 资源:载入('JM.FT',"网易WDF动画",0x6F88F494)
	self.物品格子确定_ = 资源:载入('JM.FT',"网易WDF动画",0x10921CA7)
	self.物品格子禁止_ = 资源:载入('JM.FT',"网易WDF动画",0x4138B067)
	self.队伍格子焦点_ = 资源:载入('JM.FT',"网易WDF动画",0x1ADE7867)
	self.队伍格子确定_ = 资源:载入('JM.FT',"网易WDF动画",0xC540D7A7)
	self.窗口标题背景_ = 资源:载入('JM.FT',"网易WDF动画",0xC69BF9F3).精灵
	self.物品界面背景_ = 资源:载入('JM.FT',"网易WDF动画",0xF70725E9).精灵
	self.物品格子背景_ = 资源:载入('JM.FT',"网易WDF动画",0xB17505CF).精灵
	self.竖排花纹背景_ = 资源:载入('JM.FT',"网易WDF动画",0x4E5F661E).精灵
	self.竖排花纹背景1_ = 资源:载入('JM.FT',"网易WDF动画",0xADC83326).精灵
	self.横排花纹背景_ = 资源:载入('JM.FT',"网易WDF动画",0xEA6D0A4D).精灵
	self.宽竖排花纹背景_ = 资源:载入('JM.FT',"网易WDF动画",0x2D0F136C).精灵
	self.宠物头像背景_ = 资源:载入('JM.FT',"网易WDF动画",0x363AAF1B).精灵
	self.人物头像背景_ = 资源:载入('JM.FT',"网易WDF动画",0x360B8373).精灵
	self.符石边框 = 资源:载入('JM.FT',"网易WDF动画",0x7F09EF0A)--边框
	self.经验背景_ = self._自适应.创建(7,1,186,22,1,3)
	self.影子 = 资源:载入('JM.FT',"网易WDF动画",0xDCE4B562)
	self.底图 = 资源:载入('JM.FT',"网易WDF动画",0xA393A808)
	self.传送点 =  资源:载入('JM.FT',"网易WDF动画",0x7F4CBC8C)
	self.画线 = 资源:载入('JM.FT',"网易WDF动画",0xA1442425)
	self.窗口_ = {}
	-- 初始化标题
    self.标题 = require("script/登入类/标题").创建(self)
    self.读取 = require("script/登入类/读取").创建(self)
    self.登入 = require("script/登入类/登入").创建(self)
    self.创建 = require("script/登入类/创建").创建(self)
    self.注册 = require("script/登入类/注册").创建(self)
	-- 初始化鼠标
    self.进程=0
	self.提示 = require("script/系统类/提示").创建(self)

	-- ffi函数.信息框("2", "2")

	加载游戏资源()

	-- ffi函数.信息框("3", "3")

   self.角色坐标 = xys()
	self.渐变音量 = 40
	self.任务追踪 = true
	self.战斗中 = false
	self.音乐开启 = true
	self.音效开启 = true
	self.人物选中  = false
	self.选中UI  = false
	self.隐藏UI = false
	self.第二场景开启 = false
	self.下一次确定 = false
	self.按钮焦点  = false
	self.禁止通行 = false
	self.消息栏焦点 = false
	self.快捷技能显示 = false
	self.按下中 = false
	self.第一窗口移动中 = false
	self.第二窗口移动中 = false
	self.第二次删除 = 0
	self.运行时间 = 0
	self.恢复UI = false

	self.登录处理类 = require("Script/登录系统").创建(self)
    self.角色动画类 = require("Script/主角类/角色动画类")
    self.召唤兽动画类 = require("Script/主角类/召唤兽动画类")
    self.地图 = require("Script/地图/地图类").创建(self)
    self.屏幕=require("Script/地图/屏幕").创建(self)
    鼠标动画  = require("Script/核心/鼠标动画类").创建(self)
     self.战斗类 = require("Script/战斗类/战斗类").创建(self)

     self.主界面 = require("Script/界面/主界面类").创建(self)
	  tp = self
     self.窗口_={}
		self.窗口 = {
		消息框窗口 = require("script/界面/消息框窗口").创建(self),
        聊天框类 = require("Script/界面/聊天框类").创建(self),
        人物框 = require("Script/系统类/人物框").创建(self),
        底图框 = require("Script/系统类/底图框").创建(self),
        动作界面 = require("Script/界面/动作界面").创建(self),
        任务追踪 = require("Script/界面/任务追踪").创建(self),
        任务栏 = require("script/界面/任务栏").创建(self),
        染色 = require("script/界面/染色").创建(self),
        商城 = require("script/界面/商城").创建(self),
        幻化属性 = require("script/界面/幻化属性").创建(self),
	    时辰= require("Script/系统类/时辰").创建(self),
	    -- 赌博 = require("script/界面/炼丹炉").创建(self),
	    奇经八脉 = require("script/界面/奇经八脉").创建(self),
	    符石合成 = require("script/界面/符石合成").创建(self),

	}
--self.窗口.赌博:打开()
end

function 场景类_场景:关闭窗口()
	for i,v in pairs(self.窗口_) do
		if v.可视 then
			if v.ID ~= 3 then
				if v.ID == 4 or v.ID == 7 then
					v:打开()
				else
					v.可视 = false
				end
			end
		end
	end
	self.选中窗口 = 0
	self.选中UI  = false
end

function 场景类_场景:取鼠标所在窗口(x,y)
	if not self.隐藏UI and yxjc==2 then
		for n=#self.窗口_, 1,-1 do
	        if self.窗口_[n]:检查点(x,y)  then
	        	self.选中UI = true
				return n
	        end
		end
	end
	return 0
end

function UI排序(a,b)
 	return a.窗口时间 < b.窗口时间
end

function 场景类_场景:显示(dt,x,y)
	--   self.内存计次 =self.内存计次+1
	-- if self.内存计次>1200 then
	-- 	self.内存.系统内存优化()
	--     self.内存计次=0
	-- end
	for i=0,119 do
			self.包子表情动画[i]:更新(dt)
	end
	self.登录处理类:显示(dt,x,y)
	if self.隐藏UI then
		self.选中UI = false
	end

	if yxjc == 1 then
		 if self.进程 == 1 or self.进程 == 2 or self.进程 == 3 or self.进程 == 4 or self.进程 == 5 then
		 	-- self.登录处理类.背景音乐:播放()
		    self.标题:显示(dt,x,y)
			if self.进程 == 2 then
				self.读取:显示(dt,x,y)
			elseif self.进程 == 3 then
				self.登入:显示(dt,x,y)
			elseif self.进程 == 4 then
				self.创建:显示(dt,x,y)
			elseif self.进程 == 5 then
				self.注册:显示(dt,x,y)
			end
	   end
	elseif yxjc == 2 then
		self.屏幕:显示(dt, x, y)
		-- if self.地图.小地图 ~= nil then
		-- 	self.地图.小地图:显示()
		-- 	self.地图.小地图:更新(dt)
		-- end
		if 引擎.鼠标弹起(右键) then
			if 组队开关 then
			组队开关 = false
			鼠标动画.显示序列 = 1
			elseif 玩家给予开关 then
			玩家给予开关 = false
			鼠标动画.显示序列 = 1
			elseif 交易开关 then
			交易开关 = false
			鼠标动画.显示序列 = 1
			elseif pk开关 then
			pk开关 = false
			鼠标动画.显示序列 = 1
			end
		end
		if self.地图.过度精灵 ~= nil then
			self.地图.过度时间 = self.地图.过度时间 - 0.55
			if self.地图.过度时间 <= 0 then
				self.地图.过度进度 = self.地图.过度进度 - (self.地图.过度减少 or 10)
				self.地图.过度时间 = 0
				if self.地图.过度进度 <= 0 then
					self.地图.过度进度 = 0
				end
			end
			if self.地图.过度进度 <= 240 then
				if self.恢复UI then
					self.隐藏UI = false
					self.恢复UI = false
				end
			end
			self.地图.过度精灵:置颜色(ARGB(self.地图.过度进度,255,255,255))
			self.地图.过度精灵:显示()
			if self.地图.过度进度 <= 0 then
				self.地图.过度精灵:释放()
				self.地图.过度精灵 = nil
				self.地图.过度减少 = nil
			end
		end
	elseif yxjc == 3 then
        self.地图:显示(self.屏幕.xy)
		self.战斗类:显示(dt,x,y)
		local 快捷组 = self.主界面.快捷技能栏数据
		if 快捷组~=nil  then
		  if 快捷组[1]~=nil and 引擎.按键按下(KEY.F1)  then
		    self.战斗类:快捷技能(快捷组[1].名称)
		  elseif 快捷组[2]~=nil and 引擎.按键按下(KEY.F2)  then
		    self.战斗类:快捷技能(快捷组[2].名称)
		  elseif 快捷组[3]~=nil and 引擎.按键按下(KEY.F3)  then
		   self.战斗类:快捷技能(快捷组[3].名称)
		  elseif 快捷组[4]~=nil and 引擎.按键按下(KEY.F4)  then
		   self.战斗类:快捷技能(快捷组[4].名称)
		  elseif 快捷组[5]~=nil and 引擎.按键按下(KEY.F5)  then
		   self.战斗类:快捷技能(快捷组[5].名称)
		  elseif 快捷组[6]~=nil and 引擎.按键按下(KEY.F6)  then
		    self.战斗类:快捷技能(快捷组[6].名称)
		  elseif 快捷组[7]~=nil and 引擎.按键按下(KEY.F7) then
		   self.战斗类:快捷技能(快捷组[7].名称)
		  elseif 快捷组[8]~=nil and 引擎.按键按下(KEY.F8) then
		   self.战斗类:快捷技能(快捷组[8].名称)
		  end
		end
		self.主界面:显示(dt)
	end
	if not self.隐藏UI and yxjc~=1 then
		-- print("self.隐藏UI   " .. (self.隐藏UI and "true" or "false"))
		self.队伍格子焦点_:更新(dt)
		self.物品格子焦点_:更新(dt)
		 self.窗口.时辰:显示(dt,x,y)
		 self.窗口.人物框:显示(dt,x,y)
		 self.窗口.底图框:显示(dt,x,y)
		-- self.窗口.快捷技能栏:显示(dt,x,y)
        self.窗口.聊天框类:显示(dt,x,y)
		if yxjc==3 then
			for n=1,#self.战斗类.窗口_ do
				if not self.隐藏UI then
					self.战斗类.窗口_[n]:显示(dt,x,y)
					if self.按钮焦点 then
						self.战斗类.窗口_[n].焦点 = true
					end
				end
			end
		elseif yxjc==2 then
			self.主界面:更新(dt, x, y)
			 self.窗口.任务追踪:显示(dt,x,y)
			self.主界面:显示(dt, x, y)
			sort(self.窗口_,UI排序)
			for n=1,#self.窗口_ do
				if self.窗口_[n].可视 then
					self.窗口_[n]:显示(dt,x,y)
					self.窗口_[n].焦点 = self.窗口_[n].焦点 or self.按钮焦点
					if self.窗口_[self.选中窗口] ~= nil and self.窗口_[self.选中窗口].ID == self.窗口_[n].ID and not self.按下中 then
						self.窗口_[n].鼠标 = true
						if mouseb(1) and self.抓取物品 == nil and self.抓取技能==nil and not self.禁止关闭 and not self.消息栏焦点 and not self.窗口_[n].焦点1 then
							self.窗口_[n]:打开()
						end
					else
						if self.第一窗口移动中 == false then
							self.窗口_[n].鼠标 = false
						end
					end
				end
			end
			if mousea(0) and self.选中窗口 ~= 0  and self.窗口_[self.选中窗口].可移动 and not self.消息栏焦点 then
				self.窗口_[self.选中窗口]:初始移动(x,y)
			elseif mouseb(0)  or self.隐藏UI or self.消息栏焦点 then
				self.移动窗口 = false
				self.第一窗口移动中 = false
			end
			if self.移动窗口 and not self.隐藏UI and not self.消息栏焦点 and self.窗口_[#self.窗口_] then
				self.第一窗口移动中 = true
				self.窗口_[#self.窗口_]:开始移动(x,y)
			end
			for n=1,#self.窗口_ do
				if self.窗口_[n] and not self.窗口_[n].可视 then
					remove(self.窗口_,n)
				end
			end
		end
	end
	if #self.提示.寄存内容 > 0 then
		if self.提示.寄存内容.开启提示 then
		self.提示框:置宽高(self.提示.寄存内容.提示坐标[3]+15,self.提示.寄存内容.提示坐标[4]+12)
		self.提示框:显示(self.提示.寄存内容.提示坐标[1],self.提示.寄存内容.提示坐标[2])
		end
		for i=1,#self.提示.寄存内容 do
			if self.提示.寄存内容[i].内容 ~= nil then
			self.提示.寄存内容[i].内容:显示(self.提示.寄存内容[i].x,self.提示.寄存内容[i].y)
			else
				if self.提示.寄存内容[i].文字 ~= nil then
				self.提示.寄存内容[i].文字:置颜色(self.提示.寄存内容[i].颜色):显示(self.提示.寄存内容[i].坐标[1],self.提示.寄存内容[i].坐标[2],self.提示.寄存内容[i].文本)
				end
			end
		end
		self.提示:清空寄存()
	end
	if yxjc == 2 and self.地图.小地图 ~= nil then
		self.地图.小地图:显示()
		self.地图.小地图:更新(dt)
	end
	self.提示:显示()
	信息提示:显示(1)
	鼠标动画:显示(x, y)
	鼠标动画:更新(dt)
	信息提示:更新(dt)
	--self.字体表.道具字体:显示(50,140,string.format('%d,%d',x,y))
	self.选中UI = false
	if self.下一次确定 then
		self.选中UI = true
	end
	if self.选项栏选中 then
		self.第二次删除 = self.第二次删除 + 1
		if self.第二次删除 == 2 then
			self.第二次删除 = 0
			self.选项栏选中 = false
		end
	end
	self.禁止通行 = false
	self.按钮焦点 = false
	self.禁止关闭 = false
	self.消息栏焦点 = false

	if yxjc == 2 then
		if not self.隐藏UI then
			-- if self.窗口.文本栏.可视 or self.窗口.对话栏.可视  then
			-- 	self.消息栏焦点 = true
			-- end
		    self.选中窗口 = self:取鼠标所在窗口(x,y)

		end
	elseif  yxjc == 3 then
	 	self.战斗类.选中窗口 = self.战斗类:取鼠标所在窗口(x,y)
	end
end













return 场景类_场景