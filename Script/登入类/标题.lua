--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-04-27 20:30:05
--======================================================================--
local 场景类_标题 = class()
local tp
function 场景类_标题:初始化(根)
	local xx = {{"1989C5FC",0xEB3FD8C3,0xEC1A0419},{"66279210",0x16E9D48F,0x79560528},{"07B8C541",0x7BBB735E,nil}}
	local sj = 引擎.取随机整数(1,3)
	local 资源 = 根.资源
	self.标题背景 = 资源:载入("Dat/pic/"..xx[sj][1]..".jpg","图片")
	self.场景覆盖 = 资源:载入('JM.FT',"网易WDF动画",xx[sj][2])
	self.蓝框= 资源:载入('JM.FT',"网易WDF动画",0x37B6FAA5)
	self.场景计次 = self.场景覆盖.宽度
	self.场景覆盖:置区域(self.场景计次,0,800,600)
	if xx[sj][3] ~= nil then
		self.特效覆盖 = 资源:载入('JM.FT',"网易WDF动画",xx[sj][3])
		self.特效计次 = self.特效覆盖.宽度/2+450
		self.特效覆盖:置区域(self.特效计次,0,800,600)
	end
	local dh = {0xDC739617,0x22E6E35C,0x16CC1B46,0xD8632D20}
    self.动画 = {}
	for n=1,4 do
	    self.动画[n] = 资源:载入('JM.FT',"网易WDF动画",dh[n])
	end
	self.进入游戏 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0x0A247197),0,0,3,true,true)
	self.注册账号 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0x072DD907),0,0,3,true,true)
	self.游戏充值 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0x499A35BB),0,0,3,true,true)
	self.退出游戏 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0xD139A8FE),0,0,3,true,true)
    self.上翻 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0xCB50AB1D),0,0,3,true,true)
	self.下翻 = 根._按钮(资源:载入('JM.FT',"网易WDF动画",0x7AB5584C),0,0,3,true,true)
	self.游戏公告=资源:载入('JM.FT',"网易WDF动画",0xBD827FF7)
	self.超丰富文本 = require("Script/系统类/丰富文本")(480,395)
	 for n=0,119 do
    self.超丰富文本:添加元素(n,根.包子表情动画[n])
    end
     self.超丰富文本:添加文本("                       #1 #y公告也丝滑 #1" )
     self.超丰富文本:添加文本(游戏公告)
     -- print(self.超丰富文本.行数量)
     if self.超丰富文本.行数量 > 22 then
     	self.超丰富文本:滚动(self.超丰富文本.行数量 - 22)
     end
	tp = 根
	-- 客户端:连接处理()


    self.服务器状态=资源:载入('JM.FT',"网易WDF动画",0x0E3A7EE6)
    self.新区动画=资源:载入('JM.FT',"网易WDF动画",0xFB057B3D)
    self.服务器背景=资源:载入('JM.FT',"网易WDF动画",0xD330CE3F)
    self.服务器选中=资源:载入('JM.FT',"网易WDF动画",0x5286B0B5)
    self.服务器下划线=资源:载入('JM.FT',"网易WDF动画",0x635B8925)
end

function 场景类_标题:显示(dt,x,y)
	self.标题背景:显示(0,-120)
	self.场景计次 = self.场景计次 - 0.3
	self.场景覆盖:置区域(self.场景计次,0,800,600)
	self.场景覆盖:显示(0,600)

	for n=1,4 do
		self.动画[n]:更新(dt)
	    self.动画[n]:显示(200 + (n-1) *110,520)
	end
	if self.特效覆盖 ~= nil then
		self.特效计次 = self.特效计次 - 0.7
		self.特效覆盖:置区域(self.特效计次,0,800,600)
		self.特效覆盖:显示(0,600)
	end
	
	if tp.进程 == 1 then
		if not self.start then
			self.start = true
			客户端:连接处理()
			创建音乐(0x2F1EC70A)
		end
		self.进入游戏:更新(x,y)
		self.注册账号:更新(x,y)
		self.游戏充值:更新(x,y)
		self.退出游戏:更新(x,y)
		 self.上翻:更新(x,y,self.超丰富文本.滚动值 ~= 0)
         self.下翻:更新(x,y,self.超丰富文本.行数量 > 22 and self.超丰富文本.滚动值 < self.超丰富文本.行数量 - 22)
		if self.进入游戏:事件判断() then
			tp.进程= 3
			tp.选中窗口 = nil
		elseif self.注册账号:事件判断() then
			tp.进程= 5
		elseif self.退出游戏:事件判断() then
			引擎.关闭()
			return false
		elseif self.下翻:事件判断() then
			self.超丰富文本:滚动(5)
		elseif self.上翻:事件判断() then
			self.超丰富文本:滚动(-5)
		end
		local scroll = -引擎.取鼠标滚轮()
		if scroll < 0 and self.超丰富文本.行数量 > 22 and self.超丰富文本.滚动值 < self.超丰富文本.行数量 - 22 then
			self.超丰富文本:滚动(3)
			-- print(self.超丰富文本.滚动值)
		elseif scroll > 0 and self.超丰富文本.滚动值 ~= 0 then
			self.超丰富文本:滚动(-3)
			-- print(self.超丰富文本.滚动值)
		end
	end
	if tp.进程 == 1 or tp.进程== 5 then
		self.蓝框:显示(100,100)
		-- self.游戏公告:显示(268,40)
		self.超丰富文本:显示(110,110)
	    self.上翻:显示(570,465)
		if self.超丰富文本.行数量 > 22 then
		    self.下翻:显示(570,110)
		end
		self.进入游戏:显示(620,250 + 105)
		self.注册账号:显示(620,310 + 90)
		-- self.游戏充值:显示(640,370)
		self.退出游戏:显示(620,430 + 15)

		self.服务器状态:显示(620,125 - 10)
	    self.新区动画:显示(640,307 - 10)
	    self.新区动画:更新(dt)

		self.服务器背景:显示(102,70)
		self.服务器下划线:显示(105,88)
		tp.字体表.普通字体__:显示(116,73, "新丝滑")
		self.服务器选中:显示(132,90)
		self.服务器选中:更新(dt)
		if 引擎.按键弹起(KEY.ENTER) then tp.进程 = 3 end
	end
end

return 场景类_标题