--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-04-27 13:59:26
--======================================================================--
local 场景类_创建 = class()

local tp
local mouseb = 引擎.鼠标按下
local qtx = 引擎.取头像
local qmx = 引擎.取模型
local qzd = 引擎.取战斗模型

function 场景类_创建:初始化(根)

	tp = 根
	self.rwlb = {}
	self.rwlb[1] = {模型="飞燕女",x=230,y=65,染色方案=3,介绍="深山有佳人,灼灼芙蓉姿,飞燕女轻盈飘逸,灵慧动人,自幼怜爱弱小,嫉恶如仇,一生自由自在,是大自然骄纵的宠儿\n#Z/擅使兵器：双剑、环圈",兵器="双剑\n环圈",门派="大唐官府\n方寸山\n女儿村\n神木林"}
	self.rwlb[2] = {模型="英女侠",x=303,y=65,染色方案=4,介绍="兰心惠质出名门,英姿飒爽自芳华,英女侠天资聪颖,精通琴棋书画,心怀仁爱,行善不落人后,是位侠骨柔情的奇女子\n#Z/擅使兵器：双剑、鞭",兵器="双剑\n长鞭",门派="大唐官府\n方寸山\n女儿村\n神木林"}
	self.rwlb[3] = {模型="巫蛮儿",x=376,y=65,染色方案=nil,介绍="嫣然巧笑踏绿萝,一路银铃一路歌,巫蛮儿质朴单纯,灵动可人,生性善良,活泼可爱,花盈翠影出神木,环佩婉转披香来\n#Z/擅使兵器：宝珠、法杖",兵器="宝珠\n法杖",门派="大唐官府\n方寸山\n女儿村\n神木"}
	self.rwlb[5] = {模型="逍遥生",x=522,y=65,染色方案=1,介绍="快意恩仇事,把酒踏歌行,一袭白衫,一纸折扇,逍遥生风流倜傥,潇洒自如,行事光明磊落,是世人乐于结交的谦谦君子\n#Z/擅使兵器：剑、扇",兵器="剑\n扇",门派="大唐官府\n化生寺\n方寸山\n神木林"}
	self.rwlb[6] = {模型="剑侠客",x=595,y=65,染色方案=2,介绍="霜刃露锋芒,飒沓如流星,剑侠客率情任性,狂放不羁,一生淡泊名利,嗜武如痴,英雄意,儿女情,独闯江湖半生醉,举杯邀月最销魂\n#Z/擅使兵器：刀、剑",兵器="刀\n剑",门派="大唐官府\n化生寺\n方寸山\n神木林"}
	self.rwlb[7] = {模型="狐美人",x=230,y=142,染色方案=7,介绍="修眉连娟,斜挑入眉,媚眼如丝,含娇含笑,狐美人柔情绰态,胜似海棠醉日,风情万种,颠倒众生\n#Z/擅使兵器：爪刺、鞭",兵器="爪刺\n鞭",门派="盘丝洞\n阴曹地府\n魔王寨\n无底洞"}
	self.rwlb[8] = {模型="骨精灵",x=303,y=142,染色方案=8,介绍="眉黛春山秀,横波剪秋水,骨精灵娇妍俏皮,顾盼神飞,机敏聪慧,好打不平,对世间万物充满好奇\n#Z/擅使兵器：爪刺、魔棒",兵器="爪刺\n魔棒",门派="盘丝洞\n阴曹地府\n魔王寨\n无底洞"}
	self.rwlb[10] = {模型="杀破狼",x=449,y=142,染色方案=nil,介绍="一啸生风雪,长歌动寒霜,杀破狼飘逸潇洒,气宇轩昂,能文能武,卓尔不群,身具的神秘天狼血统,纵横骄天下,傲立三界间.\n#Z/擅使兵器：弓弩、宝珠",兵器="弓弩\n宝珠",门派="狮驼岭\n魔王寨\n阴曹地府\n无底洞"}
	self.rwlb[11] = {模型="巨魔王",x=522,y=142,染色方案=5,介绍="一怒震乾坤,杀气凝如山,巨魔王力拔山气兮盖世,肩负魔族神秘使命,叱咤风云,威风凛凛\n#Z/擅使兵器：斧钺、刀",兵器="斧钺\n刀",门派="狮驼岭\n魔王寨\n阴曹地府\n无底洞"}
	self.rwlb[12] = {模型="虎头怪",x=595,y=142,染色方案=6,介绍="戏谑犹可爱,虽有神力不欺人,虎头怪弯弧五百步,长戟八十斤,勇武过人,生性耿直豁达,对朋友忠肝义胆,是顶天立地的大丈夫\n#Z/擅使兵器：斧钺、锤",兵器="斧钺\n锤",门派="狮驼岭\n魔王寨\n阴曹地府\n无底洞"}
	self.rwlb[13] = {模型="舞天姬",x=230,y=219,染色方案=11,介绍="霓裳曳广带,飘拂升天行,舞天姬明眸珠辉,瑰姿艳逸,生性善解人意,令人如沐春风.一舞绡丝动四方,观之心魂俱醉\n#Z/擅使兵器：飘带、环圈",兵器="飘带\n环圈",门派="天宫\n普陀山\n龙宫\n凌波城"}
	self.rwlb[14] = {模型="玄彩娥",x=303,y=219,染色方案=12,介绍="桂露对仙娥,星星下云逗,玄彩娥在花从中蝶翼翩翩,婀娜曼妙,犹聚晨露新聚,奇花初蕊,是集天地灵气于一身的百花仙子\n#Z/擅使兵器：飘带、魔棒",兵器="飘带\n魔棒",门派="龙宫\n普陀山\n天宫\n凌波城"}
	self.rwlb[16] = {模型="羽灵神",x=449,y=219,染色方案=nil,介绍="游侠红尘里,豪情动九天.羽灵神热情正直,率性豁达,游侠三界间,交友遍天下;乐见四海尽升平,愿引凤鸣遍九州\n#Z/擅使兵器：弓弩、法杖",兵器="弓弩\n法杖",门派="天宫\n龙宫\n普陀山\n凌波城"}
	self.rwlb[17] = {模型="神天兵",x=522,y=219,染色方案=9,介绍="金甲腾云受天命,神枪破逆卫灵霄,神天兵风采鹰扬,锋芒毕露,守护天庭立天威,所向披靡,妖魔皆闻风丧胆\n#Z/擅使兵器：枪矛、锤",兵器="枪矛\n锤",门派="龙宫\n天宫\n五庄观\n凌波城"}
	self.rwlb[18] = {模型="龙太子",x=595,y=219,染色方案=10,介绍="乘风破浪翔碧海,腾云架雾上青天,龙太子凭借天生的优势领悟仙法精髓,是当之无愧的龙族骄子,身经百战的天界战将\n#Z/擅使兵器：枪矛、扇",兵器="枪矛\n扇",门派="龙宫\n天宫\n五庄观\n凌波城"}
	self.rwlb[4] = {模型="偃无师",x=449,y=65,染色方案=nil,介绍="铁手隐机枢，巧夺天工，猛力执巨剑，志敌万均。偃无师性情冷厉，疏狂不羁，亦有奇谋满腹，铮铮傲骨。\n#Z/擅使兵器：剑、巨剑",兵器="巨剑\n剑",门派="大唐官府\n化生寺\n方寸山\n神木林"}
	self.rwlb[9] = {模型="鬼潇潇",x=376,y=142,染色方案=nil,介绍="寒眸印秋水，魂隐三生途，素手执竹伞，影馀幽冥路。鬼潇潇青丝如墨，红杉如火，一对异色瞳里似乎藏着无尽的忧愁和神秘。\n#Z/擅使兵器：爪刺、伞",兵器="爪刺\n伞",门派="狮驼岭\n魔王寨\n阴曹地府\n无底洞"}
	self.rwlb[15] = {模型="桃夭夭",x=376,y=219,染色方案=nil,介绍="桃夭柳媚梦酣眠，笑语嫣然化春风。一朝春近晴光好，清波潋滟映芳菲，桃夭夭是蟠桃园含花吐蕊的花苞，历经三千毓秀钟灵，化身一个机灵爽朗，骄憨顽皮的少女。\n#Z/擅使兵器：飘带、灯笼",兵器="飘带\n灯笼",门派="天宫\n龙宫\n普陀山\n凌波城"}
	self.xzrw = 0
	self.选择人物 = false
	self.说明框状态 = "人物介绍"
	self.dz = "静立"
	self.fx = 4
	self.染色ID = 0
	self.jswb = 根._丰富文本(250,81)
	local 资源 = 根.资源
	self.jswb:置行度(-1)
	self.文字创建角色 = 资源:载入('JM.FT',"网易WDF动画",0xB3E8376C)
	self.文字人魔仙 = 资源:载入('JM.FT',"网易WDF动画",0xA4B96C8E)
	self.文字未选择人物背景 = 资源:载入('JM.FT',"网易WDF动画",0x35BE402C)
	self.文字未选择人物 = 资源:载入('JM.FT',"网易WDF动画",0xA0087F62)
	self.人物已选择头像背景 = 资源:载入('JM.FT',"网易WDF动画",0x820CE56F)
	self.人物头像框大图背景 = 资源:载入('JM.FT',"网易WDF动画",0x00A12153)
	self.xmsrk = 资源:载入('JM.FT',"网易WDF动画",0x1BA268A5)
	self.染色切换 = 资源:载入('JM.FT',"网易WDF动画",0x5C939428)
	self.ztx = {}
	self.dtx = {}
	self.dh = {}

	for n=1,18 do
		local s = qtx(self.rwlb[n].模型)
		local q = qmx(self.rwlb[n].模型,nil,根)
		local w = qzd(self.rwlb[n].模型,nil,根)
		self.ztx[n] = 资源:载入(s[4],"网易WDF动画",s[5])
		self.dtx[n] = 资源:载入(s[7],"网易WDF动画",s[8])
		self.dh[n] = {}
		self.dh[n]["静立"] = 资源:载入(q[3],"网易WDF动画",q[1])
		self.dh[n]["行走"] = 资源:载入(q[3],"网易WDF动画",q[2])
		self.dh[n]["攻击"] = 资源:载入(w[10],"网易WDF动画",w[1])
		self.dh[n]["施法"] = 资源:载入(w[10],"网易WDF动画",w[7])
		self.rwlb[n].x = self.rwlb[n].x +50
		self.rwlb[n].y = self.rwlb[n].y + 70
	end
	local 按钮 = tp._按钮
	self.影子 = 资源:载入('JM.FT',"网易WDF动画",0xDCE4B562)
	self.选择框 = 资源:载入('JM.FT',"网易WDF动画",0x37DD63ED)
	self.一号染色 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xE328CB14),0,0,3,true,true)
	self.二号染色 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x57DAD188),0,0,3,true,true)
	self.三号染色 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x185D8D97),0,0,3,true,true)
	self.人物站立 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x74706375),0,0,3,true,true)
	self.人物奔跑 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x5CB66E72),0,0,3,true,true)
	self.人物攻击 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xD27DAAD2),0,0,3,true,true)
	self.人物施法 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xB8EBD1E5),0,0,3,true,true)
	self.后退一个方向 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x15B09F21),0,0,3,true,true)
	self.前进一个方向 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x2AF9D941),0,0,3,true,true)
	self.人物介绍 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x9EBCAA98),0,0,3,true,true)
	self.可用兵器 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x1559A775),0,0,3,true,true)
	self.可选门派 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xF0B715C0),0,0,3,true,true)
	self.人物说明框 = 资源:载入('JM.FT',"网易WDF动画",0x9DCA2A15)
	self.人物说明框上翻 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xFD3D61F2),0,0,3,true,true)
	self.人物说明框下翻 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x09217E13),0,0,3,true,true)
	-- 公用
	self.上一步 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x611107AA),0,0,3,true,true)
	self.下一步 =  按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x51A45362),0,0,3,true,true)
	-- 控件
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.mc = 总控件:创建输入("创建输入",81,388,120,14,0xFFFFFFFF)
	self.mc:置可视(false,false)
	self.mc:置限制字数(10)


end

function 场景类_创建:置方向(方向,n)
	self.dh[n]["静立"]:置方向(方向)
	self.dh[n]["行走"]:置方向(方向)
	self.dh[n]["攻击"]:置方向(取四至八方向(方向))
	self.dh[n]["施法"]:置方向(取四至八方向(方向))
end

function 场景类_创建:置染色(id,染色方案,染色ID,方向)
	self.dh[id]["静立"]:置染色(染色方案,染色ID,染色ID,染色ID)
	self.dh[id]["行走"]:置染色(染色方案,染色ID,染色ID,染色ID)
	self.dh[id]["攻击"]:置染色(染色方案,染色ID,染色ID,染色ID)
	self.dh[id]["施法"]:置染色(染色方案,染色ID,染色ID,染色ID)
	self:置方向(方向,self.xzrw)
end

function 场景类_创建:后退方向()
	if self.fx ~= 7 then
		self.fx = self.fx + 1
		self:置方向(self.fx,self.xzrw)
	end
end

function 场景类_创建:前进方向()
	if self.fx ~= 0 then
		self.fx = self.fx - 1
		self:置方向(self.fx,self.xzrw)
	end
end



function 场景类_创建:显示(dt,x,y)
	if self.xzrw ~= 0 then
		self.dh[self.xzrw][self.dz]:更新(dt)
	end

	self.后退一个方向:更新(x,y)
	self.前进一个方向:更新(x,y)
	self.一号染色:更新(x,y)
	self.二号染色:更新(x,y)
	self.三号染色:更新(x,y)
	self.人物站立:更新(x,y)
	self.人物奔跑:更新(x,y)
	self.人物攻击:更新(x,y)
	self.人物施法:更新(x,y)
	self.人物介绍:更新(x,y)
	self.可用兵器:更新(x,y)
	self.可选门派:更新(x,y)
	self.人物说明框上翻:更新(x,y,false)
	self.人物说明框下翻:更新(x,y,false)
	self.上一步:更新(x,y)
	self.下一步:更新(x,y)
	if self.xzrw ~= 0 then
		self.控件类:更新(dt,x,y)
		if self.后退一个方向:事件判断() then
			self:后退方向()
		elseif self.前进一个方向:事件判断() then
			self:前进方向()
		elseif self.人物站立:事件判断() then
			self.dz = "静立"
		elseif self.人物奔跑:事件判断() then
			self.dz = "行走"
		elseif self.人物攻击:事件判断() then
			self.dz = "攻击"
		elseif self.人物施法:事件判断() then
			self.dz = "施法"
		elseif self.一号染色:事件判断() then
			if self.rwlb[self.xzrw].染色方案 ~= nil then
				if self.染色ID ~= 0 then
					self.染色ID = 0
					self:置方向(0,self.xzrw)
					self:置染色(self.xzrw,self.rwlb[self.xzrw].染色方案,self.染色ID,self.fx)
				end
			else
				tp.提示:写入("#Y/该角色暂未开放染色")
			end
		elseif self.二号染色:事件判断() then
			if self.rwlb[self.xzrw].染色方案 ~= nil  then
				if self.染色ID ~= 1 then
					self.染色ID = 1
					self:置方向(0,self.xzrw)
					self:置染色(self.xzrw,self.rwlb[self.xzrw].染色方案,self.染色ID,self.fx)
				end
			else
				tp.提示:写入("#Y/该角色暂未开放染色")
			end
		elseif self.三号染色:事件判断() then
			if self.rwlb[self.xzrw].染色方案 ~= nil then
				if self.染色ID ~= 2 then
					self.染色ID = 2
					self:置方向(0,self.xzrw)
					self:置染色(self.xzrw,self.rwlb[self.xzrw].染色方案,self.染色ID,self.fx)
				end
			else
				tp.提示:写入("#Y/该角色暂未开放染色")
			end
		elseif self.人物介绍:事件判断() and self.说明框状态 ~= "人物状态" then
			self.jswb:清空()
			self.jswb:添加文本(self.rwlb[self.xzrw].介绍)
			self.说明框状态 = "人物状态"
		elseif self.可用兵器:事件判断() and self.说明框状态 ~= "可用兵器"  then
			self.jswb:清空()
			self.jswb:添加文本(self.rwlb[self.xzrw].兵器)
			self.说明框状态 = "可用兵器"
		elseif self.可选门派:事件判断() and self.说明框状态 ~= "可选门派"  then
			self.jswb:清空()
			self.jswb:添加文本(self.rwlb[self.xzrw].门派)
			self.说明框状态 = "可选门派"
		end
	end
	if self.xzrw == 0 then
		self.文字未选择人物背景:显示(15,100)
		self.文字未选择人物:显示(15,100)
	else
		self.人物已选择头像背景:显示(15,100)
	end
	self.人物头像框大图背景:显示(257,117)
	self.文字人魔仙:显示(750,145)
	for n=1,18 do
		self.ztx[n]:显示(self.rwlb[n].x,self.rwlb[n].y)
		if self.ztx[n]:是否选中(x,y) then
			self.选择框:更新(dt)
			self.选择框:显示(self.rwlb[n].x,self.rwlb[n].y)
			if mouseb(0) and self.xzrw ~= n then
				self.xzrw = n
				self.dz = "静立"
				self.fx = 4
				self.染色ID = 0
				if self.rwlb[self.xzrw].染色方案 ~= nil  then
					self:置染色(self.xzrw,self.rwlb[self.xzrw].染色方案,self.染色ID,self.fx)
				end
				self:置方向(self.fx,self.xzrw)
				self.jswb:清空()
				self.jswb:添加文本(self.rwlb[self.xzrw].介绍)
			end
		end
	end
	self.xmsrk:显示(28,373)
	self.后退一个方向:显示(45,440)
	self.前进一个方向:显示(155,440)
	self.染色切换:显示(34,493)
	self.一号染色:显示(74,505)
	self.二号染色:显示(109,505)
	self.三号染色:显示(147,505)
	self.人物站立:显示(188,420)
	self.人物奔跑:显示(188,445)
	self.人物攻击:显示(188,470)
	self.人物施法:显示(188,495)
	if self.xzrw ~= 0 then
		self.控件类:显示(x,y)
		self.mc:置可视(true,true)
		self.影子:显示(108,493)
		self.dh[self.xzrw][self.dz]:显示(108,493)
	else
		self.mc:置可视(false,false)
	end
	self.人物介绍:显示(298,405)
	self.可用兵器:显示(373,405)
	self.可选门派:显示(450,405)
	self.上一步:显示(617,415)
	self.下一步:显示(617,457)
	self.文字创建角色:显示(295,85)
	self.人物说明框:显示(296,436)
	if self.xzrw ~= 0 then
		self.dtx[self.xzrw]:显示(120,346)
		self.jswb:显示(298,439)
	end
	self.人物说明框上翻:显示(546,439)
	self.人物说明框下翻:显示(546,496)
	if self.xzrw ~= 0 then
		self.dtx[self.xzrw]:显示(120,346)
		self.jswb:显示(298,439)
	end
	if self.上一步:事件判断() then
		self.xzrw = 0
		self.dz = "静立"
		self.fx = 4
		self.染色ID = 0
		self.说明框状态 = "人物状态"
		self.jswb:清空()
		self.mc:置可视(false,false)
		tp.进程 = 3
	elseif self.下一步:事件判断() then
		if self.xzrw == 0 then
			tp.提示:写入("#Y/请选择一个人物进入游戏")
		else
			客户端:发送数据(0, 0, 2, { 账号=游戏账号, 密码=游戏密码, 名称=self.mc:取文本(), 编号=self.xzrw, 卡洛 = 1 })
			 --    local fun  = require("ffi函数")

				-- self.发送信息 = {
				-- 账号=游戏账号,
				-- 名称=self.mc:取文本(),
				-- 密码=游戏密码,
				-- 卡洛=fun.取MD5("???"..游戏密码.."jsiosom"),
				-- 编号=self.xzrw
				-- }
				-- 连接参数 = 1
				-- -- 客户端:发送(2,table.tostring(self.发送信息))
				-- local info = table.tostring(self.发送信息)
				-- info = (require("zlib").deflate()(info,'finish'))
				-- 客户端:发送(2, info)
		end
	end
end

return 场景类_创建