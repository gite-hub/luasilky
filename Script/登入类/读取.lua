--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-03-17 04:39:38
--======================================================================--
local 场景类_读取 = class()
local tp
function 场景类_读取:初始化(根)
  tp=根
  local 资源 = 根.资源
  self.服务器选中=资源:载入('JM.FT',"网易WDF动画",0x5286B0B5)
  self.服务器背景=资源:载入('JM.FT',"网易WDF动画",0x37B6FAA5)
  self.服务器状态=资源:载入('JM.FT',"网易WDF动画",0x0E3A7EE6)
  self.新区动画=资源:载入('JM.FT',"网易WDF动画",0xFB057B3D)
  self.服务器编号=1
  local 按钮 = 根._按钮
  self.上一步 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x611107AA),0,0,3,true,true)
  self.下一步 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x51A45362),0,0,3,true,true)
  self.服务器数据={}
  self.服务器数据[1]={名称="新丝滑",状态=1,x=0,y=0,背景=资源:载入('JM.FT',"网易WDF动画",0xD330CE3F)}
  self.服务器字体=根.字体表.普通字体__
  self.服务器状态栏={}
  for n=1,5 do
  self.服务器状态栏[n]=资源:载入('JM.FT',"网易WDF动画",0x635B8925)
  end
end
function 场景类_读取:显示(dt,x,y)
      self.服务器背景:显示(100,70)
      self.服务器状态:显示(620,125)
      self.新区动画:显示(630,310)
      self.新区动画:更新(dt)
      self.服务器选中:更新(dt)
      self.上一步:显示(610,380)
      self.上一步:更新(x,y)
      self.下一步:显示(610,425)
      self.下一步:更新(x,y)
       for n=1,#self.服务器数据 do
         self.服务器数据[n].x=10+n*80+30
         self.服务器数据[n].y=90
         self.服务器数据[n].背景:显示(self.服务器数据[n].x,self.服务器数据[n].y)
         self.服务器字体:显示(self.服务器数据[n].x+7,self.服务器数据[n].y+2,self.服务器数据[n].名称)
         self.服务器状态栏[self.服务器数据[n].状态]:显示(self.服务器数据[n].x+4,self.服务器数据[n].y+16)
        end
       if self.服务器编号~=0 then
         self.服务器选中:显示(self.服务器数据[self.服务器编号].x+30,self.服务器数据[self.服务器编号].y+20)
         end
    if self.上一步:事件判断() then
			tp.进程= 1
			tp.选中窗口 = nil
		elseif self.下一步:事件判断() then
			tp.进程= 3
		end
end

return 场景类_读取