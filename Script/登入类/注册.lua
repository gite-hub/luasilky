-- @作者: baidwwy
-- @邮箱:  313738139@qq.com
-- @创建时间:   2019-05-16 00:51:30
-- @最后修改来自: baidwwy
-- @Last Modified time: 2020-04-13 09:56:14

local 注册 = class()
local 临时 = 2

function 注册:初始化(根)
local 按钮 = 根._按钮
local 资源 = 根.资源
local 自适应 =根._自适应

	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.zczh = 总控件:创建输入("注册账号",356,153,120,16,0xFF000000)
	self.zczh:置可视(false,false)
	self.zczh:置限制字数(10)
	-- self.zczh:置文字颜色(0xFF000000)
	self.zcmm = 总控件:创建输入("注册密码",356,203,120,16,0xFF000000)
	self.zcmm:置可视(false,false)
	self.zcmm:置限制字数(10)
	self.zcmm:置密码模式()
	-- self.zcmm:置文字颜色(0xFF000000)
	self.qrmm = 总控件:创建输入("确认密码",356,253,120,16,0xFF000000)
	self.qrmm:置可视(false,false)
	self.qrmm:置限制字数(10)
	self.qrmm:置密码模式()
	-- self.qrmm:置文字颜色(0xFF000000)
	self.aqm = 总控件:创建输入("安全码",356,303,120,16,0xFF000000)
	self.aqm:置可视(false,false)
	self.aqm:置限制字数(10)
    -- self.aqm:置文字颜色(0xFF000000)


		self.资源组 = {
		[0] = 自适应.创建(1,1,257,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,283,353,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"注册"),
		[4] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"取消"),
		[5]  = 自适应.创建(3,1,150,19,1,3)

	}

		tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体

	self.注册间隔 = 0
end


function 注册:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end


function 注册:显示(dt,x,y)
	local bbs = {"账号输入：","密码输入：","确认密码：","*安全码："}
	if  引擎.按键弹起(0x9) then
    if 临时 == 1  then
    	self.zczh:置焦点(true)
    	临时 =2
 	elseif 临时==2 and  self.zczh:取焦点() then
 		self.zcmm:置焦点(true)
 		临时 = 3
 	elseif 临时==3 and self.zcmm:取焦点() then
 		self.qrmm:置焦点(true)
 		临时 = 4
 	elseif 临时==4 and self.qrmm:取焦点() then

 		self.aqm:置焦点(true)
 		临时 = 1
 	end
   end
        self.资源组[1]:显示(240,100)
        self.资源组[0]:显示(246,103)
        self.资源组[2]:显示(503,106)
        self.资源组[3]:显示(290,400)
        self.资源组[4]:显示(440,400)
         self.资源组[2]:更新(x,y)
        self.资源组[3]:更新(x,y)
        self.资源组[4]:更新(x,y)

   for i=0,3 do
      self.资源组[5]:显示(350,150+i*50)
      end
        for i=1,#bbs do
      zts:显示(265,103+i*50,bbs[i])

      end
	tp.窗口标题背景_:置区域(0,0,72,16)
	tp.窗口标题背景_:显示(350,103)
	zts2:置字间距(2)
	zts2:显示(355,103,"账号注册")
	zts2:置字间距(0)
	zts2:置颜色(0xFFFF0000)
	zts2:显示(265,353,"*请牢记安全码可以找回账号和修改密码！")
	zts2:置颜色(0xFFFFFFFF)
	self.控件类:显示(x,y)
	self.控件类:更新(dt,x,y)
	self.zczh:置可视(true,true)
	self.zcmm:置可视(true,true)
	self.aqm:置可视(true,true)
	self.qrmm:置可视(true,true)

	if self.注册间隔 > 0 then
		self.注册间隔 = self.注册间隔 - 1
	end
    if (self.资源组[2]:事件判断() or self.资源组[4]:事件判断()) or (self:检查点(x,y)and 引擎.鼠标弹起(0x01)) then
		tp.进程= 1
	elseif  self.资源组[3]:事件判断()  then
		if self.注册间隔 > 0 then
			tp.提示:写入("#Y/注册过于频繁")
        elseif self.zcmm:取文本() ~= self.qrmm:取文本() then
        	tp.提示:写入("#Y/请确认密码重新输入")
        elseif   self.zcmm:取文本() =="" or self.qrmm:取文本() =="" or self.zczh:取文本() ==""   or self.aqm:取文本()== "" then
            tp.提示:写入("#Y/请输入完整的账号密码以及安全码")
		else
			self.注册间隔 = 300
			ccc_registe(314159, self.zczh:取文本(), self.zcmm:取文本(), self.aqm:取文本())
				-- local fun  = require("ffi函数")
				-- local 发送信息 = {
				-- -- 账号=self.zczh:取文本(),
				-- -- 密码=self.zcmm:取文本(),
				-- 卡洛=fun.取MD5("草儿"..self.zcmm:取文本().."丝滑不"),
				-- 	-- a=0.66,
				-- 	-- b=12,
				-- 	c=self.zczh:取文本(),
				-- 	d=self.zcmm:取文本(),
				-- 	e=self.aqm:取文本()
				-- }
				--   -- 客户端:发送(314159,table.tostring(发送信息))
				--   local info = table.tostring(发送信息)
				--   info = (require("zlib").deflate()(info,'finish'))
				--   客户端:发送(314159, info)
	            -- tp.进程= 1
        end
    end
end

return 注册