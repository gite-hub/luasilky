
 local 登入 = class()
 local 临时 =false
 local zt
 local tp
function 登入:初始化(根)
  tp=根
  local 按钮 = 根._按钮
  local 资源 = 根.资源
  zt=根.字体表.描边字体
  zt1=根.字体表.登入字体
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.zhsr = 总控件:创建输入("账号输入",290,230 - 40,190,16,0xFFFFFFFF)
	self.zhsr:置可视(false,false)
  self.zhsr:置文本(引擎.读配置项([[\配置.ini]],"账号信息","账号"))
	self.zhsr:置限制字数(10)
	self.mmsr = 总控件:创建输入("密码输入",290,270 - 40,190,16,0xFFFFFFFF)
	self.mmsr:置可视(false,false)
	self.mmsr:置限制字数(10)
	self.mmsr:置密码模式()
  self.mmsr:置文本(引擎.读配置项([[\配置.ini]],"账号信息","密码"))
	self.上一步 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0X08349957),0,0,3,true,true)
	self.下一步 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0XD3E2202B),0,0,3,true,true)
	self.账号密码=资源:载入('JM.FT',"网易WDF动画",0xCB788484)---CB788484 51F941C4
  self.蓝框= 资源:载入('JM.FT',"网易WDF动画",0x2B64A696)
  self.普通登入= 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xCD1DE5B1),0,0,3,true,true)
  self.二维码登入= 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xE71947F7),0,0,3,true,true)
  self.内部测试 =根.资源:载入("Dat/pic/logo1.png","图片")
  self.普通显示=false
  self.普通显示1 = 引擎.读配置项([[\配置.ini]],"账号信息","状态")
  if self.普通显示1 =="true" then
     self.普通显示=true
  end
  self.普通 = 根._按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xFF205590),0,0,1,true,true)
  self.普通:置根(根)
  self.普通:置偏移(-3,2)
end
function 登入:账号检查()
   if self.zhsr:取文本()=="" or self.mmsr:取文本()=="" then
      return false
    else
     游戏账号=self.zhsr:取文本()
     游戏密码=self.mmsr:取文本()
      return true
    end
   end
function 登入:显示(dt,x,y)
    self.蓝框.精灵:置拉伸(190,200 - 50,612,388 - 20)
    self.蓝框.精灵:置颜色(0x99072c8d)
    self.蓝框.精灵:显示(1,1)
    self.账号密码:显示(218,224 - 40)
    self.普通:置打勾框(self.普通显示)
    self.普通:显示(280,303 - 40)
    self.普通:显示(425,303 - 40)
    -- self.普通登入:显示(279,150,true,nil,nil,true,2)
    -- self.二维码登入:显示(419,150)
    -- self.普通登入:更新(x,y)
    -- self.二维码登入:更新(x,y)
    -- zt1:置颜色(0xFFf7d710)
    -- zt1:显示(535,232,"忘记账号")
    -- zt1:显示(535,235,"________")
    -- zt1:显示(535,272,"账号服务")
    -- zt1:显示(535,275,"________")
    zt:置颜色(0xFFffffff)
    -- zt:显示(309,308 - 40,"密码保护")
    zt:显示(452,308 - 40,"记住账号")
    self.普通:更新(x,y)
    -- self.内部测试:显示(200,0)
    self.上一步:显示(278,331 - 30)
    self.下一步:显示(419,331 - 30)
    self.上一步:更新(x,y)
    self.下一步:更新(x,y)
    self.控件类:显示(x,y)
    self.控件类:更新(dt,x,y)
    self.zhsr:置可视(true,true)
    self.mmsr:置可视(true,true)
    if self.zhsr._输入焦点==true then
      if 引擎.按键弹起(KEY.TAB) or 引擎.按键弹起(KEY.ENTER) then
          self.mmsr._输入焦点=true
          self.zhsr._输入焦点=false
          self.zhsr._光标可视=false
      end
    elseif self.mmsr._输入焦点==true then
      if 引擎.按键弹起(KEY.TAB) then
        self.zhsr._输入焦点=true
        self.mmsr._输入焦点=false
        self.mmsr._光标可视=false

      elseif 引擎.按键弹起(KEY.ENTER) then
          self:回车()
      end
    else
      self.zhsr._输入焦点=true
    end
	  if self.上一步:事件判断() then
		tp.进程= 1
		tp.选中窗口 = nil
    elseif self.普通:事件判断() then
      if self.普通显示 then
        self.普通显示=false
        引擎.写配置项([[\配置.ini]],"账号信息","状态","false")
      else
        self.普通显示=true
        引擎.写配置项([[\配置.ini]],"账号信息","状态","true")
      end
	   elseif self.下一步:事件判断() then
        self:回车()
    end
end

function 登入:回车()
    if self.普通显示 then
      引擎.写配置项([[\配置.ini]],"账号信息","账号",self.zhsr:取文本())
      引擎.写配置项([[\配置.ini]],"账号信息","密码",self.mmsr:取文本())
    end
    if self.zhsr:取文本()=="" or self.mmsr:取文本()=="" then
      tp.提示:写入("#Y/请输入完整的账号密码")
    else
        if  self:账号检查() then
            -- local fun  = require("ffi函数")
            local 发送信息 = {
            账号=self.zhsr:取文本(),
            密码=self.mmsr:取文本(),
            卡洛=1 --fun.取MD5("草儿"..self.mmsr:取文本().."丝滑不")
            }
            客户端:发送数据(0, 0, 1, 发送信息)
            -- 客户端:发送(1, table.tostring(发送信息))
            -- local info = table.tostring(发送信息)
            -- info = (require("zlib").deflate()(info,'finish'))
            -- 客户端:发送(1, info)
        end
   end
end

return 登入