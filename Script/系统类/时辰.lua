

local 系统类_时辰 = class()

local floor = math.floor
-- local ARGB = ARGB
-- local require = require
local 矩阵 = require("gge包围盒")(0,0,115,95)
local tp
local zt
local format = string.format
local keytq = 引擎.按键弹起
function 系统类_时辰:初始化(根)
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.背景 = 资源:载入('JM.FT',"网易WDF动画",0xDE3F48B7)
	self.白昼 = 资源:载入('JM.FT',"网易WDF动画",0x9DF6DEBC).精灵
	self.假人 = 资源:载入('JM.FT',"网易WDF动画",0xC7BEBF45)
	self.假人1 =资源:载入('JM.FT',"网易WDF动画",2888856949)
	self.时辰 = {[1]=资源:载入('JM.FT',"网易WDF动画",0x361FA820),--
	[2]=资源:载入('JM.FT',"网易WDF动画",0xC0A66903),--
	[3]=资源:载入('JM.FT',"网易WDF动画",0xD1D11294),--
	[4]=资源:载入('JM.FT',"网易WDF动画",0xAA7DEB05),--
	[5]=资源:载入('JM.FT',"网易WDF动画",0x21274A87),
	[6]=资源:载入('JM.FT',"网易WDF动画",0x09C4978D),--
	[7]=资源:载入('JM.FT',"网易WDF动画",0xC9E2F072),--
	[8]=资源:载入('JM.FT',"网易WDF动画",0x2ACB36B2),--
	[9]=资源:载入('JM.FT',"网易WDF动画",0xC26BF189),--
	[10]=资源:载入('JM.FT',"网易WDF动画",0x1AA170AE),--
	[11]=资源:载入('JM.FT',"网易WDF动画",0x7921D3A3),--
	[12]=资源:载入('JM.FT',"网易WDF动画",0xEA7CAB84)--
    }
	资源:载入('JM.FT',"网易WDF动画",0xC9E2F072)
	self.梦幻指引 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xF102F42D),0,0,4)
	self.收缩 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x6EDD4D71),0,0,4)
	self.灯笼 = {}
	for n=1,3 do
	   self.灯笼[n] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xBAF6A95D),0,0,2)
	end
	self.地图 = nil
	self.偏移 = nil
	self.白昼:置区域(self.计次,0,80,30)
	self.偏移坐标 = 0
	tp = 根
	zt = tp.字体表.描边字体
	self.当前时辰 = 1

end
function 系统类_时辰:显示(dt,x,y)

	if 矩阵:检查点(x,y) then
		tp.选中UI = true
	end
	self.地图 = tp.地图.地图名称
    self.偏移  = 60-string.len(tostring(tp.地图.地图名称)) *3.72
	self.白昼:置区域(self.计次,0,80,30)
	self.白昼:显示(14,32 + self.偏移坐标)
	self.背景:显示(0,0 + self.偏移坐标)
	self.假人:更新(dt)
	self.假人1:更新(dt)
	if tp.屏幕.主角.移动开关 == false then
		self.假人:显示(58,60 + self.偏移坐标)
	else
		self.假人1:显示(58,60 + self.偏移坐标)
	end

	if self.偏移坐标 == 0 then
		for n=1,3 do
		   self.灯笼[n]:更新(x,y)
		   self.灯笼[n]:显示(93,17+((n-1)*18))
		end
		self.梦幻指引:更新(x,y)
		if self.梦幻指引:事件判断() and yxjc==2 then
			-- tp.窗口.梦幻指引:打开()
			客户端:发送数据(2, 6, 13, "9", 1)
		end
		self.梦幻指引:显示(1,78 + self.偏移坐标)
		if self.灯笼[1]:事件判断() and yxjc == 2 and not tp.消息栏焦点 then --and 游戏进程==2
			-- tp.窗口.小地图:打开(tp.场景.数据.地图数据.编号)
			tp.地图.小地图.是否显示 = not tp.地图.小地图.是否显示
		-- elseif (self.灯笼[2]:事件判断() or keytq(KEY.TAB)) and 游戏进程==2 and not tp.消息栏焦点 then
		  -- 引擎.置全屏()
		end
	end

	self.收缩:更新(x,y)
	if self.收缩:事件判断() then
		if self.偏移坐标 == 0 then
			self.偏移坐标 = -64
			矩阵:置坐标(0,-64)
		else
		    self.偏移坐标 = 0
		    矩阵:置坐标(0,0)
		end
	end
	self.收缩:显示(3,64 + self.偏移坐标)


	self.时辰[self.当前时辰]:显示(1.3,21.5 + self.偏移坐标)
	zt:显示(self.偏移-1,13.5 + self.偏移坐标,self.地图)
	local xy = "X:"..floor(tp.屏幕.主角.格子坐标.x).." Y:"..floor(tp.屏幕.主角.格子坐标.y)
	zt:显示(floor(61 - zt:取宽度(xy) / 2)-1.5,62 + self.偏移坐标,xy)
end



function 系统类_时辰:检查点(x,y)
	if  矩阵:检查点(x,y) then
		return  true
	end
end

return 系统类_时辰