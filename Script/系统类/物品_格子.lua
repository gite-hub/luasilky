--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2019-09-18 21:39:14
--======================================================================--
local 系统类_物品格子 = class()
local tp,zt
local mouse = 引擎.鼠标弹起

function 系统类_物品格子:初始化(x,y,ID,注释,遮挡)
	self.ID = ID
	self.注释 = 注释
	self.物品 = nil
	self.事件 = false
	self.焦点 = false
	self.右键 = false
	self.遮挡 = 遮挡
	self.确定 = false

end

function 系统类_物品格子:置根(根)
	tp = 根
	zt = tp.字体表.描边字体
end



function 系统类_物品格子:置物品(物品)
	if 物品 ~= nil then
		local item =引擎.取物品(物品.名称)
		if 物品.名称 ~="怪物卡片" and 物品.名称 ~=0 then
                self.物品 = 物品
                self.物品.说明 = item.说明
                if 物品.类型=="武器" or 物品.类型=="衣服"or  物品.类型=="头盔" or 物品.类型=="项链" or 物品.类型=="腰带" or 物品.类型=="鞋子" or 物品.类型=="召唤兽装备" then
                 self.物品.角色限制 =item.角色限制
                 self.物品.性别限制 =item.性别限制
                elseif 物品.类型== "药品" then
                	self.物品.功效 = item.功效
                	if 物品.等级 ==3 then
	                	if 物品.名称 == "九转回魂丹" then
	                		self.物品.气血=物品.品质*5+100
	                        self.物品.伤势=物品.品质*5+100
	                	elseif 物品.名称 == "十香返生丸" then
	                		self.物品.魔法=物品.品质*3+50
						elseif 物品.名称 == "金香玉" then
							self.物品.气血=物品.品质*12+150
						elseif 物品.名称 =="小还丹" then
							self.物品.气血=物品.品质*8+100
							self.物品.伤势=物品.品质+80
						elseif 物品.名称 == "红雪散" then
							self.物品.气血=物品.品质*4
						elseif 物品.名称 == "千年保心丹" then
							self.物品.气血=物品.品质*4+200
							self.物品.伤势=物品.品质*4+100
						elseif 物品.名称 == "风水混元丹" then
							self.物品.魔法=物品.品质*3+50
						elseif 物品.名称 == "定神香" then
							self.物品.魔法=物品.品质*5+50
						elseif 物品.名称 == "蛇蝎美人" then
							self.物品.魔法=物品.品质*5+100
						elseif 物品.名称 == "佛光舍利子" then
	                         self.物品.气血 =物品.品质*3
	                         self.物品.伤势 =物品.品质*7+100
						elseif 物品.名称 == "五龙丹" then
							self.物品.气血= 物品.品质*3
	                	 end
                	end
                elseif 物品.类型== "烹饪" then
                 	self.物品.功效 =item.功效
                 	self.物品.类别 =item.类别
                elseif 物品.类型== "宝石" then
                  self.物品.部位 =item.部位
                  self.物品.效果 =item.效果
                elseif 物品.类型== "炼妖" then
                 if 物品.名称 == "金柳露" or 物品.名称 == "超级金柳露" then
                 	self.物品.功效 =item.功效
                 end
                end
				self.物品.小模型 = tp.资源:载入(item.资源 ,"网易WDF动画",item.小模型资源)
				self.物品.大模型 = tp.资源:载入(item.资源 ,"网易WDF动画",item.大模型资源)
				self.物品.可叠加= item.叠加
        elseif 物品.名称 == "怪物卡片" then
        	self.物品 = 物品
        	self.物品.说明 = item.说明
        	local 卡片 = 引擎.取变身卡(物品.类型)
        	self.物品.变化之术等级= 卡片.变化之术等级
        	self.物品.属性= 卡片.属性
             if self.物品.等级 ==9 and 卡片.小模型==nil then
        		self.物品.小模型 = tp.资源:载入("WP.FT" ,"网易WDF动画",0x76B5210F)
        		   if 卡片.资源 ==nil then
        		   	self.物品.大模型 = tp.资源:载入("WP.FT","网易WDF动画",卡片.大模型)
        		   else
        		  	self.物品.大模型 = tp.资源:载入(卡片.资源,"网易WDF动画",卡片.大模型)
                  end

        	else
        	     self.物品.小模型 = tp.资源:载入(卡片.资源 ,"网易WDF动画",卡片.小模型)
                 self.物品.大模型 = tp.资源:载入(卡片.资源 ,"网易WDF动画",卡片.大模型)
        	end

				self.物品.可叠加= false

		end
	else
		self.物品 = nil
	end
end

function 系统类_物品格子:显示(dt,x,y,条件,总类,xx,yy,abs,类别,符石)

	self.事件 = false
	self.焦点 = false
	self.右键 = false
	if self.物品 ~= nil then
		if self.遮挡 ~= nil then
			self.遮挡:显示(self.x-1+(xx or 0),self.y+4+(yy or 0))
		end
		self.物品.小模型:显示(self.x-1,self.y+2)
		if ((总类 and 总类 ~= false) and self.物品.类型 ~= 总类[1] and self.物品.类型 ~= 总类[2] and self.物品.类型 ~= 总类[3] and self.物品.类型 ~= 总类[4]
		and self.物品.类型 ~= 总类[5] and self.物品.类别 ~=类别 and self.物品.类型 ~= 总类[6]  and self.物品.类型 ~= 总类[7] and self.物品.类型 ~= 总类[8])   then
			tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
		else
			if self.物品.可叠加  then
			zt:置颜色(4294967295)
			zt:置描边颜色(-16777216)
			zt:显示(self.x + 3,self.y + 3,self.物品.数量)
		   end
		end
		  if 符石 then
	     tp.符石边框:显示(self.x+23,self.y+26)
		   if 符石 == 2 then
		   	     tp.符石边框:更新(dt/2)
			elseif 符石 == 3 then
				  tp.符石边框:更新(dt/3)
			elseif 符石 == 4 then
				  tp.符石边框:更新(dt/4)
			elseif 符石 == 5 then
				   tp.符石边框:更新(dt/5)
			else
                 tp.符石边框:更新(dt)
		   end
	    end
	end
	if self.确定 then
		tp.物品格子确定_:显示(self.x+1,self.y+2)
	end

	if not tp.消息栏焦点 then
		if x>=self.x and x<=self.x+50 and y>=self.y and y<=self.y+50 and 条件 then
			tp.按钮焦点 = true
			tp.禁止关闭 = true
			if mouse(0) then
				self.事件 = true
			elseif mouse(1) then
				self.右键 = true
			end
			if self.格子显示 == nil then
				if abs == nil and  符石==nil   then
					tp.物品格子焦点_:显示(self.x+1,self.y+1)
				end
			end
			self.焦点 = true
		end
	end
end

function 系统类_物品格子:置坐标(x,y)
	self.x,self.y = x,y
end

return 系统类_物品格子