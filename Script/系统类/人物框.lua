--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-05-01 10:45:35
--======================================================================--
local 系统类_人物框 = class()
local keyaz = 引擎.按键按住
local keyax = 引擎.按键按下
local floor = math.floor
local ceil = math.ceil
-- local 矩阵 = require("Script/通用方法/gge包围盒")(0,0,115,95)
local tp
local insert = table.insert
local remove = table.remove
local mouseb = 引擎.鼠标弹起
local format = string.format
local tx = 引擎.取头像
local min = math.min
-- local 矩阵 = require("Script/通用方法/gge包围盒")(670,1,228,50)
local 矩阵 = require("gge包围盒")(570,1,228,50)
local 按钮

function 系统类_人物框:刷新角色头像(根)
  		if tp.屏幕.主角.数据.变身~=nil then
			local s = tx(tp.屏幕.主角.数据.变身)
			self.人物头像背景[1] = 按钮(tp.资源:载入(s[4],"网易WDF动画",s[2]),0,0,1)
		else
			local s = tx(tp.屏幕.主角.数据.造型)
			self.人物头像背景[1] = 按钮(tp.资源:载入(s[4],"网易WDF动画",s[2]),0,0,1)
		end
end
function 系统类_人物框:初始化(根)
	tp = 根
	local 资源 = 根.资源
	 按钮 = 根._按钮
	self.背景 = 资源:载入('JM.FT',"网易WDF动画",0x2E8758EE)
	self.气血 = 资源:载入('JM.FT',"网易WDF动画",0xAAD44583)
	self.魔法 = 资源:载入('JM.FT',"网易WDF动画",0xCE4D3C2D)
	self.经验 = 资源:载入('JM.FT',"网易WDF动画",0x7B3C08E4)
	self.愤怒 = 资源:载入('JM.FT',"网易WDF动画",0xBAF8009F)
	self.气血底图 = 资源:载入('JM.FT',"网易WDF动画",0x008C2611)
	self.记忆角色 = {}
	self.人物头像背景 = {}
	self.小图标 = {资源:载入('JM.FT',"网易WDF动画",0x24901650),资源:载入('JM.FT',"网易WDF动画",0x09E2B01B)}
	self.图标组 = {}
	self.焦点 = false
	 self.队伍数据={}
	   self.bb数据 ={}
   self.双倍动画 = 资源:载入('JM.FT',"网易WDF动画",0x19B90B9D)
   self.攻击动画 = 资源:载入('JM.FT',"网易WDF动画",0x4A5DD98C)
   self.交易动画 = 资源:载入('JM.FT',"网易WDF动画",0xABB8DCAA)
   self.铃铛动画 = 资源:载入('JM.FT',"网易WDF动画",0x397B8CF6)
   self.红丹动画 = 资源:载入('JM.FT',"网易WDF动画",0x24901650)
   self.盾牌动画 = 资源:载入('JM.FT',"网易WDF动画",0x8C6D00DC)
   self.蓝丹动画 = 资源:载入('JM.FT',"网易WDF动画",0x09E2B01B)
end
function 系统类_人物框:添加队伍(内容)
 self.队伍数据={}
	self.队伍数据 = {}
	self.临时数据 = 内容
	if self.临时数据 ~= nil and self.临时数据 ~= "66" then
		for n = 1, #self.临时数据, 1 do

			self.队伍数据[n] = self.临时数据[n]

			--    if self.队伍数据[i].变身 ~=nil then
			-- 	local x = tx(self.队伍数据[i].变身)
			-- 	self.人物头像背景[i+1] = 按钮(tp.资源:载入(x[4],"网易WDF动画",x[2]),0,0,1)
			-- else
				local x = tx(self.队伍数据[n].造型)
				self.人物头像背景[n+1] = 按钮(tp.资源:载入(x[4],"网易WDF动画",x[2]),0,0,1)
			-- end
		end
	end

 end


function 系统类_人物框:刷新召唤兽气血(数据)
	self.临时数据 = 数据

	self.bb数据.当前气血 = self.临时数据.当前气血
	self.bb数据.气血上限 = self.临时数据.气血上限
	self.bb数据.当前魔法 = self.临时数据.当前魔法
	self.bb数据.魔法上限 = self.临时数据.魔法上限
end

function 系统类_人物框:刷新召唤兽头像(数据)
  local 资源 = tp.资源
  self.bb数据=数据

  if self.bb数据==0 then
   self.bb头像=nil
  else
   if self.bb数据.造型~=nil then
        local n = 引擎.取头像(self.bb数据.造型)
         self.bb头像 = tp.资源:载入(n[4],"网易WDF动画",n[3])
 end
end
end

function 系统类_人物框:加入小图标信息(方式,提示信息,变量)
	for n=1,#self.图标组 do
		if self.图标组[n][4] == 方式 then
			return
		end
	end
	insert(self.图标组,{self.小图标[方式],提示信息,变量,方式})
end

function 系统类_人物框:删除小图标信息(方式)
	for n=1,#self.图标组 do
		if self.图标组[n][4] == 方式 then
			remove(self.图标组,n)
			break
		end
	end
end
function 系统类_人物框:显示(dt,x,y)
	self.焦点 = false

	if self.队伍数据 ~=nil and yxjc==2 then
		for i=1,#self.队伍数据 do
			tp.人物头像背景_:显示(555-i*51,2)
			self.人物头像背景[i+1]:更新(x,y)
			self.人物头像背景[i+1]:显示(555-i*51+3,5,true)
			if self.人物头像背景[i+1]:事件判断() and 游戏进程==2 then
			  ---未来加好友功能
			end
		end
		for i=1,#self.队伍数据 do
			if self.人物头像背景[i+1]:是否选中(x,y) then
				tp.提示:队伍(x,y,self.队伍数据[i])
				self.焦点 = true
				break
			end
		end
	end
    tp.人物头像背景_:显示(680,0)
	self.背景:显示(732,0)
	self.背景:显示(732,13)
	self.背景:显示(732,26)
	self.背景:显示(732,39)
	self.气血:置区域(0,0,min(floor(tp.屏幕.主角.数据.当前气血 / tp.屏幕.主角.数据.最大气血 * 50),50),8)
	self.魔法:置区域(0,0,min(floor(tp.屏幕.主角.数据.当前魔法 / tp.屏幕.主角.数据.魔法上限 * 50),50),8)
	self.愤怒:置区域(0,0,min(floor(tp.屏幕.主角.数据.愤怒 / 150 * 50),50),8)
	self.经验:置区域(0,0,min(floor(tp.屏幕.主角.数据.当前经验 / tp.屏幕.主角.数据.升级经验 * 50),50),8)

    self.人物头像背景[1]:更新(x,y)
	if self.人物头像背景[1]:事件判断() or ((引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.W)) and not tp.消息栏焦点) and yxjc==2  then
		if tp.主界面.界面数据[1].本类开关 == false then
			-- 客户端:发送数据2(5,{序号=1,数字id=窗口标题.id})
			客户端:发送数据(0, 1, 5)
		else
			tp.主界面.界面数据[1].本类开关 = false
		end

  end

	self.人物头像背景[1]:显示(683,3,true)
	self.气血底图:显示(744,3)
	self.气血:显示(744,3)
	self.魔法:显示(744,16)
	self.愤怒:显示(744,29)
	self.经验:显示(744,42)


	if self.气血:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/气血：%d/%d/%d",tp.屏幕.主角.数据.当前气血,tp.屏幕.主角.数据.气血上限,tp.屏幕.主角.数据.最大气血))
		if mouseb(1) and 游戏进程==2 and not tp.消息栏焦点 or 引擎.鼠标弹起(键盘符号.右键) then
			客户端:发送数据(88,34,13,"S7")
		end
	elseif self.魔法:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/魔法：%d/%d",tp.屏幕.主角.数据.当前魔法,tp.屏幕.主角.数据.魔法上限))
		if mouseb(1) and 游戏进程==2 and not tp.消息栏焦点 or 引擎.鼠标弹起(键盘符号.右键) then
			客户端:发送数据(88,35,13,"S7")
		end
	elseif self.愤怒:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/愤怒：%d/150",tp.屏幕.主角.数据.愤怒))
	elseif self.经验:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/经验：%d/%d",tp.屏幕.主角.数据.当前经验,tp.屏幕.主角.数据.升级经验))
	end
	local xx = 0
	local yy = 0
	for n=1,#self.图标组 do
		self.图标组[n][1]:显示(682+xx*30,58)
		if self.图标组[n][1]:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/%s%d",self.图标组[n][2],tp.剧情进度[self.图标组[n][3]]))
		end
		xx = xx + 1
	end
	tp.宠物头像背景_:显示(570,0)
	self.背景:显示(610,0)
	self.背景:显示(610,13)
	self.背景:显示(610,26)
	local cz = self.bb数据
	if self.bb头像 ~= nil then
		if self.记忆角色[7] ~= cz.造型 then
			local n = tx(cz.造型)
			self.宝宝头像背景 = 按钮(tp.资源:载入(n[4],"网易WDF动画",n[3]),0,0,1)
			self.记忆角色[7] = cz.造型
		end
		self.气血:置区域(0,0,min(floor(cz.当前气血 / cz.气血上限 * 50),50),8)
		self.魔法:置区域(0,0,min(floor(cz.当前魔法 / cz.魔法上限 * 50),50),8)
		self.经验:置区域(0,0,min(floor(cz.当前经验 / cz.升级经验 * 50),50),8)
		self.气血底图:显示(822,3)
		self.气血:显示(622,3)
		self.魔法:显示(622,16)
		self.经验:显示(622,29)
		if self.气血:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/气血：%d/%d",cz.当前气血,cz.气血上限))
			if mouseb(1) and yxjc==2 and not tp.消息栏焦点 or 引擎.鼠标弹起(键盘符号.右键) then
				客户端:发送数据(88,32,13,"S7")
			end
		elseif self.魔法:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/魔法：%d/%d",cz.当前魔法,cz.魔法上限))
			if mouseb(1) and yxjc==2 and not tp.消息栏焦点 or 引擎.鼠标弹起(键盘符号.右键)then
					客户端:发送数据(88,33,13,"S7")
			end
		elseif self.经验:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/经验：%d/%d",cz.当前经验,cz.升级经验))
		end
	else
		if self.记忆角色[7] ~= false then
			self.宝宝头像背景 = 按钮(tp.资源:载入("JM.FT","网易WDF动画",0xCEC838D7),0,0,1)
			self.记忆角色[7] = false
		end
	end
	self.宝宝头像背景:更新(x,y)
	if self.宝宝头像背景:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.O)) and not tp.消息栏焦点) and yxjc==2 then
		if tp.主界面.界面数据[2].本类开关 then
			tp.主界面.界面数据[2].本类开关 = false
		else
			客户端:发送数据(6, 1, 22, "6A", 1)
		end
	end
	self.宝宝头像背景:显示(573,3,true)
	if #tp.窗口.任务追踪.数据 > 0 then
		local 偏移= 0
		for i=1,#tp.窗口.任务追踪.数据 do

			if tp.窗口.任务追踪.数据[i].名称=="双倍经验" then
			     self.双倍动画:显示(680+偏移,58)
				if  self.双倍动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		    if tp.窗口.任务追踪.数据[i].名称=="摄妖香" then
			     self.铃铛动画:显示(680+偏移,58)
				if  self.铃铛动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		    if tp.窗口.任务追踪.数据[i].名称=="回天丹" then
			     self.红丹动画:显示(680+偏移,58)
				if  self.红丹动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		end

	end

end



function 系统类_人物框:检查点(x,y)
	return 矩阵:检查点(x,y) or self.焦点
end

return 系统类_人物框









