--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-03-16 10:02:49
--======================================================================--
local 系统类_底图框 = class()
local 矩阵 = require("gge包围盒")(450,564,350,38)
local tp;
local keyaz = 引擎.按键按住
local keyax = 引擎.按键按下
local keytq = 引擎.按键弹起
local KEY = KEY

function 系统类_底图框:初始化(根)
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	self.UI_底图 = 资源:载入('JM.FT',"网易WDF动画",0x3D1FA249)
	self.UI_攻击 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x6BBC42FA),0,0,4,true)
	self.UI_道具 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x0E53F705),0,0,4,true)
	self.UI_给予 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x7E4DE3DE),0,0,4,true)
	self.UI_交易 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xCAB0B8B4),0,0,4,true)
	self.UI_队伍 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x0D3EA20B),0,0,4,true)
	self.UI_宠物 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x187ABFC8),0,0,4,true)
	self.UI_任务 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xA15292B2),0,0,4,true)
	self.UI_帮派 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xC35B2EC3),0,0,4,true)
	self.UI_快捷 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xBB6E607E),0,0,4,true)
	self.UI_好友 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x7C7A64D9),0,0,4,true)
	self.UI_成就 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x8B3AADDA),0,0,4,true)
	self.UI_动作 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x548156A0),0,0,4,true)
	self.UI_系统 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x5116F7DF),0,0,4,true)
	self.UI_梦幻 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xB4092D66),0,0,4,true)
	self.UI_排行 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x3D5F03B3),0,0,1,true)
    --self.UI_排行1 = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x5829CF13),0,0,1,true)
	self.好友闪烁 =资源:载入('JM.FT',"网易WDF动画",0x7C7A64D9)
	tp = 根

end

function 系统类_底图框:显示(dt,x,y)
	self.UI_攻击:更新(x,y,yxjc==2)
	self.UI_道具:更新(x,y,yxjc==2)
	self.UI_给予:更新(x,y,yxjc==2)
	self.UI_交易:更新(x,y,yxjc==2)
	self.UI_队伍:更新(x,y,yxjc==2)
	self.UI_宠物:更新(x,y,yxjc==2)
	self.UI_任务:更新(x,y,yxjc==2)
	self.UI_帮派:更新(x,y,yxjc==2)
	self.UI_快捷:更新(x,y,yxjc==2)
	self.UI_成就:更新(x,y,yxjc==2)
	self.UI_动作:更新(x,y,yxjc==2)
	self.UI_系统:更新(x,y,yxjc==2)
	-- self.UI_梦幻:更新(x,y,yxjc==2)
	-- self.UI_排行:更新(x,y,yxjc==2)
	--self.UI_排行1:更新(x,y,yxjc==2)

	if yxjc==2 then
		if self.UI_攻击:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.A)) and not tp.消息栏焦点) then
					鼠标动画.显示序列 = 2
		pk开关 = true
		elseif self.UI_道具:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.E)) and not tp.消息栏焦点)then
				if tp.主界面.界面数据[3].本类开关 then
					tp.主界面.界面数据[3].本类开关 = false
				else
					客户端:发送数据(1, 1, 13, "9", 1)
				end

		elseif self.UI_给予:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.G)) and not tp.消息栏焦点) then
			 鼠标动画.显示序列 = 8
		      玩家给予开关 = true
		elseif self.UI_交易:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.X)) and not tp.消息栏焦点)  then
			鼠标动画.显示序列 = 9
		交易开关 = true
		elseif 引擎.按键弹起(KEY.F4) then
			客户端:发送数据(854, 24, 43, "14", 1)
	    elseif 引擎.按键弹起(KEY.F8) then
			客户端:发送数据(50, 50, 13, "52", 1)
		elseif self.UI_队伍:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.T)) and not tp.消息栏焦点)  then
		if tp.主界面.界面数据[7].本类开关 then
			tp.主界面.界面数据[7].本类开关 = false
		else
			if 组队开关 then
				组队开关 = false
				鼠标动画.显示序列 = 1
				return 0
			end
			客户端:发送数据(0, 1, 11, "66", 1)
		end
		elseif self.UI_宠物:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.P)) and not tp.消息栏焦点)  then
		if tp.主界面.界面数据[4].本类开关 then
			tp.主界面.界面数据[4].本类开关 = false
		else
			客户端:发送数据(6321, 22, 13, "9s", 1)
		end
		elseif self.UI_任务:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.Q)) and not tp.消息栏焦点) then
		if tp.主界面.界面数据[13].本类开关 then
			tp.主界面.界面数据[13].本类开关 = false
		else
			客户端:发送数据(12, 26, 25, "93", 1)
		end

		elseif 引擎.按键按住(KEY.ALT) and 引擎.按键弹起(键盘符号.回车) then
			引擎.置全屏()
		elseif 引擎.按键弹起(KEY.F1) then
				客户端:发送数据(6321, 57, 40, "61", 1)
	    elseif 引擎.按键弹起(KEY.F2) then
				客户端:发送数据(6940, 16, 42, "61", 1)
		elseif 引擎.按键弹起(KEY.F3) then
			if 战斗模式 then
				战斗模式 = false
				信息提示:加入提示("#y/你退出了战斗模式")
			else
				战斗模式 = true
				信息提示:加入提示("#y/你开启了战斗模式，战斗模式下将显示所有玩家id")
				信息提示:加入提示("#y/在聊天框内输入pk玩家id即可对对方发起攻击")
			end
		elseif self.UI_快捷:事件判断() and not tp.消息栏焦点 then -- or ((keyaz(KEY.ALT) and keyax(KEY.C))
	       if tp.主界面.快捷技能显示 then
           tp.主界面.快捷技能显示 =false
         else
           tp.主界面.快捷技能显示 =true
           客户端:发送数据(1, 1, 9961, "kjl", 1)
       end
		elseif self.UI_好友:事件判断() or (keyaz(KEY.ALT) and keyax(KEY.F)) then
		客户端:发送数据(51, 51, 13, "9s", 1)
		elseif self.UI_排行:事件判断() then
			-- if tp.主界面.界面数据[52].本类开关 then
			-- 	tp.主界面.界面数据[32].本类开关 = false
			-- else
			--      客户端:发送数据(50,888,13,"52",1)
			-- end
        elseif self.UI_帮派:事件判断() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.B) then
			if tp.主界面.界面数据[37].本类开关 then
				tp.主界面.界面数据[37].本类开关 = false
			else
			    客户端:发送数据(1, 61, 13, "9", 1)
			 end
        elseif self.UI_成就:事件判断() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.N) then
     		if tp.主界面.界面数据[52].本类开关 then
				tp.主界面.界面数据[32].本类开关 = false
			else
			     客户端:发送数据(50,888,13,"52",1)
			end
		elseif self.UI_动作:事件判断() or 引擎.按键按住(KEY.ALT) and 引擎.按键按下(KEY.D) then
			-- tp.窗口.动作界面:打开()
			if tp.主界面.界面数据[45].本类开关 then
				tp.主界面.界面数据[45].本类开关 = false
			else
			    客户端:发送数据(50,81,13,"52",1)
			end
		elseif self.UI_系统:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.S)) and not tp.消息栏焦点) then
			-- tp.主界面.界面数据[35].本类开关 = not tp.主界面.界面数据[35].本类开关
			tp.窗口.动作界面:打开()
		elseif self.UI_梦幻:事件判断() then
			  -- 客户端:发送数据(50,81,13,"52",1)
		end
        if self.UI_攻击:是否选中(x,y) then
        	tp.提示:自定义(x+40,y,"快捷键:ALT+A")
		elseif	self.UI_道具:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+E")
		elseif	self.UI_给予:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+G")
		elseif	self.UI_交易:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+X")
		elseif	self.UI_队伍:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+T")
		elseif	self.UI_宠物:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+O")
		elseif	self.UI_任务:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+Q")
		elseif	self.UI_帮派:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+B")
		elseif	self.UI_快捷:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+C")
		elseif	self.UI_好友:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+F")
		elseif	self.UI_成就:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+N")
		elseif	self.UI_动作:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+D")
		elseif	self.UI_系统:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+S")
		elseif	self.UI_梦幻:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"商城内有你想要的东西哦！")
			elseif	self.UI_排行:是否选中(x,y) then
			tp.提示:自定义(x+40,y-15,"点击查看全区实力榜单！")
        end
	end
	if keyaz(KEY.ALT) and keyax(KEY.C) then
       if tp.主界面.快捷技能显示 then
       tp.主界面.快捷技能显示 =false
	     else
	       tp.主界面.快捷技能显示 =true
	       客户端:发送数据(1, 1, 9961, "kjl", 1)
	   end
	end
	self.UI_底图:显示(450,537)
	self.UI_攻击:显示(454,566,true)
	self.UI_道具:显示(480,566,true)
	self.UI_给予:显示(506,566,true)
	self.UI_交易:显示(531,566,true)
	self.UI_队伍:显示(557,566,true)
	self.UI_宠物:显示(582,566,true)
	self.UI_任务:显示(609,566,true)
	self.UI_帮派:显示(636,566,true)
	self.UI_快捷:显示(663,566,true)
	self.UI_好友:显示(690,566,true)
	if 消息开关 then
		--self.好友闪烁:更新(dt)
		self.好友闪烁:显示(690,566,yxjc==2)
	else
	 self.UI_好友:显示(690,566,true)
	 self.UI_好友:更新(x,y,yxjc==2)
	end
	self.UI_成就:显示(719,569,true)
	self.UI_动作:显示(747,566,true)
	self.UI_系统:显示(774,566,true)
	-- self.UI_梦幻:显示(145,10,true)
	   -- self.UI_排行1:显示(240,50,true)
    -- self.UI_排行:显示(120,10,true)
	if self:检查点(x,y) then
		tp.按钮焦点 = true
	end

end

function 系统类_底图框:检查点(x,y)
	return 矩阵:检查点(x,y) or self.UI_梦幻:是否选中(x,y)or self.UI_排行:是否选中(x,y)
end

return 系统类_底图框