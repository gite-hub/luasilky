--[[
    0  只有平时可用 只能对自己使用
	1  只有平时可用 能对我方使用
	2  只有战斗可用 只能对自已使用
	3  只有战斗可用 对我方使用
	4  只有战斗可用 对敌人使用
	5  平时战斗都可用 对自已使用
	6  平时战斗都可用 对我方使用
	7  被动
	8  未定义
	9  只有战斗可用,对我方使用,但不能对自己使用
	10 只有战斗可用,目标死亡可用
--]]
local 系统类_技能格子 = class()
local tp;
local mouse = 引擎.鼠标弹起

function 系统类_技能格子:初始化(x,y,ID,注释)
	self.x = x
	self.y = y
	self.注释 = 注释
	self.技能 = nil
	self.事件 = false
	self.焦点 = false
end

function 系统类_技能格子:置技能(技能,小模型,大模型)

if 技能 ~= nil and 技能.名称 ~= nil then
	    self.技能 = 技能

	local n = 引擎.取技能(技能.名称)
	 self.技能.大模型资源 = n[7] self.技能.小模型资源 = n[8] self.技能.资源 = n[6]
	self.技能.介绍 = n[1]
	if self.技能.种类 ~= nil then
		self.技能.消耗说明 = n[4]
	end
	if self.技能.种类 ~= nil then
		self.技能.使用条件 = n[5]
	end
	self.技能.冷却 = n[12]
		if tp == nil then
			tp = 引擎.场景
		end

		self.技能.模型 = tp.资源:载入(self.技能.资源,"网易WDF动画",self.技能.大模型资源)

          self.技能.小模型 = tp.资源:载入(self.技能.资源,"网易WDF动画",self.技能.小模型资源)

	else
		self.技能 = nil
	end
end

function 系统类_技能格子:显示(x,y,条件,模型)
	if self.技能 == nil then
		return
	end
	self.右键 = false
	self.事件 = false
	self.焦点 = false
	if 条件 and self.技能.模型:是否选中(x,y) then
		tp.按钮焦点 = true
		tp.禁止关闭 = true
		self.焦点 = true
		self.技能.模型:置高亮()
		if mouse(0) then
			self.事件 = true
		elseif 	mouse(1) then
			self.右键 = true
		end
	else
		self.技能.模型:取消高亮()
	end
	if self.剩余冷却回合 ~= nil then
		self.技能.模型:灰度级()
	end
	if 模型 ~=nil then
    	self.技能.模型=self.技能.小模型
    end
    self.技能.模型:显示(self.x,self.y)
end

function 系统类_技能格子:置坐标(x,y)
	self.x = x
	self.y = y
end


return 系统类_技能格子