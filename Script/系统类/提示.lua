--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2019-08-22 11:13:14
--======================================================================--
local 系统类_提示框 = class()
local floor = math.floor
local format = string.format
local remove = table.remove
local insert = table.insert
local jls = nil
local min = math.min
local tp = nil
local ARGB = ARGB
local pairs = pairs
local ff

function 系统类_提示框:初始化(根)
	ff = 根._丰富文本
	self.介绍文本 = ff(246,480,根.字体表.普通字体)
	self.道具文本 = ff(187,480,根.字体表.普通字体)
	self.文本 = {}
	self.寄存内容 = {}
	self.寄存高度 = {}
	self.最高 = 0
	jls = 根.提示框
	tp = 根
end

function 系统类_提示框:显示()
	if #self.文本 == 0 then
		return false
	end
	for n=1,#self.文本 do
		if self.文本[n] ~= nil then
			self.文本[n].tiem = self.文本[n].tiem - 1
			if self.文本[n].tiem <= 0 then
				table.remove(self.文本, n)
			end
		end
	end
	for n=1,#self.文本 do
		if self.文本[n] ~= nil then
			local h=self.文本[n].h
			jls:置宽高(394,h+16)
			jls:显示(self.文本[n].x,self.文本[n].y)
			self.文本[n].ffs:显示(self.文本[n].x+6.5,self.文本[n].y+6.5)
		end
	end

end

function 系统类_提示框:写入(txt)
	local yy = 60
	local x = 0
	if #self.文本 > 8 then
		remove(self.文本, 1)
	end
	local txts = {
		x = 210,
		y = 300,
		tiem = 150+#self.文本*7,
		ffs =  ff(210,300)
	}
	local ab = txts.ffs:添加文本(txt)
	txts.h = ab-6
	if #self.文本 > 0 then
		local cs = 1
		local zgd = 300
		for i=1,#self.文本 do
			zgd = zgd - self.文本[i].h - 18
		end
		while true do
			self.文本[cs].y = zgd
			zgd = zgd + self.文本[cs].h +18
			cs = cs + 1
			if cs > #self.文本 then
				break
			end
		end
	end
	insert(self.文本,txts)
end

function 系统类_提示框:小地图(x,y,text)
	local x = x + 28
	local y = y
	local w = 86
	local h = 16
	if y - h <= 0 then
		y = 0
	end
	if x + w+18 >= 992 then
		x = 992-w
	end
	if y + h >= 612 then
		y = 612 - h
	end
	jls:置宽高(w+15,h+16)
	jls:显示(x,y)
	tp.字体表.普通字体:置颜色(-256):显示(x + 16,y + 8,text)
end

function 系统类_提示框:战斗提示(x,y,text,f)
	local x = x
	local y = y
	local w = floor(tp.字体表.普通字体:取宽度(text))-1
	local h = 12
	if x + w >= 780 then
		x = 780-w
		if f then
			x = x - (w/1.3)
		end
	end
	if y+30 >= 580 then
		y = 580-h-18
	end
	if y <= 2 then
		y = 2
	end
	jls:置宽高(w+15,h+16)
	jls:显示(x,y)
	tp.字体表.普通字体:置颜色(-256):显示(x + 7,y + 7,text)
end

function 系统类_提示框:其他提示(x,y,f)
	local x = x+25
	local y = y-10
	local w = 125
	local h = 43

	if x + w >= 780 then
		x = 780-w
		if f then
			x = x - (w/1.8)
		end
	end
	jls:置宽高(w+15,h+16)
	jls:显示(x,y)
	local zt = tp.字体表.普通字体
	zt:置颜色(-256)
		zt:显示(x + 7,y + 7,format("气血:%d/%d/%d",f.当前气血,f.气血上限,f.最大气血))
		zt:显示(x + 7,y + 23,format("魔法:%d/%d",f.当前魔法,f.魔法上限))
		zt:显示(x + 7,y + 39,format("愤怒:%d/150",f.愤怒))
end

function 系统类_提示框:自定义(x,y,txt)
	local x = x-80
	local y = y
	self.介绍文本:清空()
	self.介绍文本:添加文本(txt)
	local w = self.介绍文本.显示表.宽度
	local h = self.介绍文本.显示表.高度
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h >= 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = self.介绍文本
	self.寄存内容[1].x = x + 6
	self.寄存内容[1].y = y + 6

	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end
function 系统类_提示框:队伍(x,y,skill)
	local x = x - 60
	local y = y + 30
	local w = 120
	self.介绍文本:清空()
	self.介绍文本:添加文本("#W/ ID : #Y"..skill.id)
	self.介绍文本:添加文本("#W/名称: #Y"..skill.名称)
	self.介绍文本:添加文本("#W/等级: #Y"..skill.等级)
	self.介绍文本:添加文本("#W/门派: #Y"..skill.门派)
	local h = self.介绍文本.显示表.高度
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h >= 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	tp.字体表.人物字体_:置阴影颜色(nil)
	self.寄存内容[1].文字 = tp.字体表.人物字体_
	self.寄存内容[1].内容 = self.介绍文本
	self.寄存内容[1].x = x + 10
	self.寄存内容[1].y = y + 5
	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end

function 系统类_提示框:技能(x,y,skill,冷却,ps)
	local x = x + 30
	local y = y
	local w = 243
	self.介绍文本:清空()
	self.介绍文本:添加文本("")
	self.介绍文本:添加文本("")
	self.介绍文本:添加文本(skill.介绍)
	if skill.消耗说明 ~= nil then
		self.介绍文本:添加文本("#G/消耗："..skill.消耗说明)
	end
	if skill.使用条件 ~= nil and skill.学会 == false and not tp.战斗中 and not ps then
		self.介绍文本:添加文本(format("#G/条件：%s",skill.使用条件))
	end
	if skill.冷却 ~= nil then
		self.介绍文本:添加文本(format("#G/冷却：%s",skill.冷却))
	end
	if 冷却 ~= nil then
		self.介绍文本:添加文本(format("#R/剩余冷却回合：%d回合",冷却))
	end
	local h = self.介绍文本.显示表.高度-10
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h >= 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = self.介绍文本
	self.寄存内容[1].x = x + 9
	self.寄存内容[1].y = y - 5

	self.寄存内容[2] = {}
	tp.字体表.人物字体_:置阴影颜色(nil)
	self.寄存内容[2].文字 = tp.字体表.人物字体_
	self.寄存内容[2].颜色 = ARGB(255,0,255,0)
	self.寄存内容[2].文本 = skill.名称
	self.寄存内容[2].坐标 = {[1]=x+10,[2] =y+10}
	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end
function 系统类_提示框:BB技能(x,y,skill)
	local x = x + 30
	local y = y
	local w = 273
	self.介绍文本:清空()
	self.介绍文本:添加文本("")
	self.介绍文本:添加文本("")
	self.介绍文本:添加文本("#G/"..skill.介绍)
	if skill.消耗说明 ~= nil then
		self.介绍文本:添加文本("#G/消耗："..skill.消耗说明)
	end
	if skill.使用条件 ~= nil and skill.学会 == false and not tp.战斗中  then
		self.介绍文本:添加文本(format("#G/条件：%s",skill.使用条件))
	end

	local h = self.介绍文本.显示表.高度-10
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h >= 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = self.介绍文本
	self.寄存内容[1].x = x + 9
	self.寄存内容[1].y = y - 5
 --    self.寄存内容[2] = {}
	-- self.寄存内容[2].内容 = skill.小模型
	-- self.寄存内容[2].x = x+5
	-- self.寄存内容[2].y = y+10
	self.寄存内容[2] = {}
	tp.字体表.人物字体_:置阴影颜色(nil)
	self.寄存内容[2].文字 = tp.字体表.人物字体_
	self.寄存内容[2].颜色 = 0xFFFFFF00
	self.寄存内容[2].文本 = skill.名称
	self.寄存内容[2].坐标 = {[1]=x+10,[2] =y+10}

	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end
function 系统类_提示框:印记(x,y,yj)
	local x = x + 25
	local y = y + 25
	local xs = 0
	self.道具文本:清空()
    for i,v in pairs(yj) do
		self.道具文本:添加文本(format("#Y/%s印记#R/%d#Y/层",i,v))
		xs = 1
	end
	if xs == 0 then
		return false
	end
	local h = self.道具文本.显示表.高度
	if x >= 900 then
		x = 900
	end
	if y + h > 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}

	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = self.道具文本
	self.寄存内容[1].x = x+7
	self.寄存内容[1].y = y+8

	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=80,[4]=h}
	self.寄存内容.开启提示 = true
end

function 系统类_提示框:内丹提示(x,y,ad)
    local x = x +30
	local y = y
	local w = 238
	h = self.介绍文本.显示表.高度+38
	if h < 80 then
	h = 80
	end
	self.介绍文本:清空()
	self.介绍文本:添加文本("#G/"..ad.等级.."层/5层")
	self.介绍文本:添加文本(ad.说明)
	self.介绍文本:添加文本("#Y/"..ad.效果)

	if x < 3 then
		x = 3
	end
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h > 580 then
		y = y - h - 30
	end
     self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = ad.小模型
	self.寄存内容[1].x = x+5
	self.寄存内容[1].y = y+5
	self.寄存内容[2] = {}
	self.寄存内容[2].内容 = self.介绍文本
	self.寄存内容[2].x = x + 5
	self.寄存内容[2].y = y + 45
	self.寄存内容[3] = {}
	self.寄存内容[3].文字 = tp.字体表.人物字体_
	self.寄存内容[3].颜色 = -256
	self.寄存内容[3].文本 = ad.技能
	self.寄存内容[3].坐标 = {[1]=x+45,[2] =y+18}
	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true

end

function 系统类_提示框:红尘试炼(x,y,ad)
	local x = x + 30
	local y = y
	local w = 135
	local h = 15
	self.道具文本:清空()
	self.道具文本:添加文本("#Y/红尘试炼·"..(ad[5] or ad[1]))
	if x < 3 then
		x = 3
	end
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h > 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = png
	self.寄存内容[1].x = x+10
	self.寄存内容[1].y = y+10

	self.寄存内容[2] = {}
	self.寄存内容[2].内容 = self.道具文本
	self.寄存内容[2].x = x + 5
	self.寄存内容[2].y = y + 5

	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end

local function 取境界(类型,j)
	if 类型 == 1 then
		if j == 0 then
			return "#C/初具法力"
		elseif j == 1 then
			return "#W/略晓变化"
		elseif j == 2 then
			return "#S/驾轻就熟"
		elseif j == 3 then
			return "#S/心领神会"
		elseif j == 4 then
			return "#S/出类拔萃"
		elseif j == 5 then
			return "#S/腾云驾雾"
		elseif j == 6 then
			return "#S/降龙伏虎"
		elseif j == 7 then
			return "#S/神乎其技"
		elseif j == 8 then
		    return "#S/纵横三界"
		elseif j == 9 then
		    return "#S/不堕轮回"
		end
	end
  if 类型 == 2 then
		if j == 0 then
		    return "#S/心法初成"
		elseif j == 1 then
		    return "#S/了然于胸"
		elseif j == 2 then
		    return "#S/妙领天机"
		elseif j == 3 then
		    return "#S/渐入佳境"
		elseif j == 4 then
		    return "#S/预知福祸"
		elseif j == 5 then
		    return "#S/脱胎换骨"
		elseif j == 6 then
		    return "#S/出神入化"
		elseif j == 7 then
		    return "#S/呼风唤雨"
		elseif j == 8 then
		    return "#S/随心所欲"
		elseif j == 9 then
		    return "#S/登峰造极"
		elseif j == 10 then
		    return "#S/道满根归"
		elseif j == 11 then
		    return "#S/不堕轮回"
		elseif j == 12 then
		    return "#S/法力无边"
		end
	end
  if 类型 == 3 then
		if j == 0 then
		    return "#S/元神初具"
		elseif j == 1 then
		    return "#S/一日千里"
		elseif j == 2 then
		    return "#S/妙领天机"
		elseif j == 3 then
		    return "#S/负海担山"
		elseif j == 4 then
		    return "#S/霞举飞升"
		elseif j == 5 then
		    return "#S/移星换斗"
		elseif j == 6 then
		    return "#S/变幻莫测"
		elseif j == 7 then
		    return "#S/擎日挽月"
		elseif j == 8 then
		    return "#S/道满根归"
		elseif j == 9 then
		    return "#S/不堕轮回"
		elseif j == 10 then
		    return "#S/举世无双"
		elseif j == 11 then
		    return "#S/纵横三界"
		elseif j == 12 then
		    return "#S/笑傲西游"
		elseif j == 13 then
		    return "#S/法力无边"
		elseif j == 14 then
		    return "#S/反璞归真"
		elseif j == 15 then
		    return "#S/天人合一"
		end
	end
end
function 系统类_提示框:特性(x,y,ad,ac,aa)
 local xa ={}
	local x = x-80
	local y = y+15
	self.介绍文本:清空()

 if ad == "力破" and aa == 1 then
        xa ={"#G/80#Y/","#R/10#Y/"}
	elseif ad == "力破" and aa == 2 then
         xa ={"#G/120#Y/","#R/10#Y/"}
	elseif ad == "力破" and aa == 3 then
		xa ={"#G/160#Y/","#R/5%#Y/"}
	elseif ad == "力破" and aa == 4 then
		xa ={"#G/200#Y/","#R/5%#Y/"}
	elseif ad == "力破" and aa == 5 then
		xa ={"#G/240#Y/","#R/5%#Y/"}
    elseif ad == "识物" and aa == 1 then
        xa ={"#G/5%#Y/","#R/8#Y/"}
	elseif ad == "识物" and aa == 2 then
		 xa ={"#G/8%#Y/","#R/8#Y/"}
	elseif ad == "识物" and aa == 3 then
		 xa ={"#G/10%#Y/","#R/4#Y/"}
	elseif ad == "识物" and aa == 4 then
		 xa ={"#G/13%#Y/","#R/4#Y/"}
    elseif ad == "识物" and aa == 5 then
    	 xa ={"#G/16%#Y/","#R/4#Y/"}
	elseif ad == "灵刃" and aa == 1 then
		 xa ={"#G/33%#Y/","#R/10%#Y/"}
	elseif ad == "灵刃" and aa == 2 then
		 xa ={"#G/50%#Y/","#R/10%#Y/"}
	elseif ad == "灵刃" and aa == 3 then
		 xa ={"#G/66%#Y/","#R/5%#Y/"}
    elseif ad == "灵刃" and aa == 4 then
    	 xa ={"#G/83%#Y/","#R/5%#Y/"}
	elseif ad == "灵刃" and aa == 5 then
		 xa ={"#G/100%#Y/","#R/5%#Y/"}
	elseif ad == "瞬击" and aa == 1 then
		 xa ={"#G/33%#Y/","#R/10%#Y/"}
	elseif ad == "瞬击" and aa == 2 then
		 xa ={"#G/50%#Y/","#R/10%#Y/"}
    elseif ad == "瞬击" and aa == 3 then
    	 xa ={"#G/66%#Y/","#R/5%#Y/"}
	elseif ad == "瞬击" and aa == 4 then
		 xa ={"#G/83%#Y/","#R/5%#Y/"}
	elseif ad == "瞬击" and aa == 5 then
		 xa ={"#G/100%#Y/","#R/5%#Y/"}
	elseif ad == "复仇" and aa == 1 then
		 xa ={"#G/33%#Y/","#R/10%#Y/"}
	elseif ad == "复仇" and aa == 2 then
		 xa ={"#G/50%#Y/","#R/10%#Y/"}
    elseif ad == "复仇" and aa == 3 then
    	 xa ={"#G/66%#Y/","#R/5%#Y/"}
	elseif ad == "复仇" and aa == 4 then
		 xa ={"#G/83%#Y/","#R/5%#Y/"}
	elseif ad == "复仇" and aa == 5 then
		 xa ={"#G/100%#Y/","#R/5%#Y/"}
	elseif ad == "弑神" and aa == 1 then
		 xa ={"#G/60#Y/","#R/40%#Y/"}
	elseif ad == "弑神" and aa == 2 then
		 xa ={"#G/90#Y/","#R/40%#Y/"}
    elseif ad == "弑神" and aa == 3 then
    	 xa ={"#G/120#Y/","#R/20%#Y/"}
	elseif ad == "弑神" and aa == 4 then
		 xa ={"#G/150#Y/","#R/20%#Y/"}
	elseif ad == "弑神" and aa == 5 then
		 xa ={"#G/180%#Y/","#R/20%#Y/"}
	elseif ad == "吮魔" and aa == 1 then
		 xa ={"#G/1%#Y/","#R/1%#Y/"}
	elseif ad == "吮魔" and aa == 2 then
		 xa ={"#G/2%#Y/","#R/1%#Y/"}
    elseif ad == "吮魔" and aa == 3 then
    	 xa ={"#G/3%#Y/","#R/5%#Y/"}
	elseif ad == "吮魔" and aa == 4 then
		 xa ={"#G/4%#Y/","#R/5%#Y/"}
	elseif ad == "吮魔" and aa == 5 then
		 xa ={"#G/5%#Y/","#R/5%#Y/"}
	elseif ad == "自恋" and aa == 1 then
        xa ={"#G/33%#Y/","#R/8#Y/"}
    elseif ad == "自恋" and aa == 2 then
    	 xa ={"#G/50%#Y/","#R/8#Y/"}
	elseif ad == "自恋" and aa == 3 then
		 xa ={"#G/66%#Y/","#R/4#Y/"}
	elseif ad == "自恋" and aa == 4 then
		 xa ={"#G/83%#Y/","#R/4#Y/"}
	elseif ad == "自恋" and aa == 5 then
		 xa ={"#G/100%#Y/","#R/4#Y/"}
    elseif ad == "乖巧" and aa == 1 then
        xa ={"#G/6%#Y/","#R/8#Y/"}
	elseif ad == "乖巧" and aa == 2 then
		xa ={"#G/10%#Y/","#R/8#Y/"}
	elseif ad == "乖巧" and aa == 3 then
		xa ={"#G/13%#Y/","#R/4#Y/"}
	elseif ad == "乖巧" and aa == 4 then
		xa ={"#G/16%#Y/","#R/4#Y/"}
    elseif ad == "乖巧" and aa == 5 then
    	xa ={"#G/20%#Y/","#R/4#Y/"}
	elseif ad == "灵动" and aa == 1 then
	    xa ={"#G/10%#Y/","#R/8#Y/"}
	elseif ad == "灵动" and aa == 2 then
		xa ={"#G/16%#Y/","#R/8#Y/"}
	elseif ad == "灵动" and aa == 3 then
		xa ={"#G/21%#Y/","#R/4#Y/"}
    elseif ad == "灵动" and aa == 4 then
    	xa ={"#G/26%#Y/","#R/4#Y/"}
	elseif ad == "灵动" and aa == 5 then
		xa ={"#G/32%#Y/","#R/4#Y/"}
	elseif ad == "逆境" and aa == 1 then
       xa ={"#G/33%#Y/","#R/30%#Y/"}
	elseif ad == "逆境" and aa == 2 then
		xa ={"#G/50%#Y/","#R/30%#Y/"}
    elseif ad == "逆境" and aa == 3 then
    	xa ={"#G/66%#Y/","#R/15%#Y/"}
	elseif ad == "逆境" and aa == 4 then
		xa ={"#G/83%#Y/","#R/15%#Y/"}
	elseif ad == "逆境" and aa == 5 then
		xa ={"#G/100%#Y/","#R/10%#Y/"}
	elseif ad == "灵断" and aa == 1 then
        xa ={"#G/33%#Y/","#R/5%#Y/"}
    elseif ad == "灵断" and aa == 2 then
        xa ={"#G/50%#Y/","#R/10%#Y/"}
	elseif ad == "灵断" and aa == 3 then
		xa ={"#G/66%#Y/","#R/5%#Y/"}
	elseif ad == "灵断" and aa == 4 then
		xa ={"#G/83%#Y/","#R/5%#Y/"}
	elseif ad == "灵断" and aa == 5 then
		xa ={"#G/100%#Y/","#R/5%#Y/"}
    elseif ad == "争锋" and aa == 1 then
        xa ={"#G/6%#Y/","#R/90#Y/"}
	elseif ad == "争锋" and aa == 2 then
		xa ={"#G/10%#Y/","#R/90#Y/"}
	elseif ad == "争锋" and aa == 3 then
		xa ={"#G/13%#Y/","#R/90#Y/"}
	elseif ad == "争锋" and aa == 4 then
		xa ={"#G/16%#Y/","#R/180#Y/"}
    elseif ad == "争锋" and aa == 5 then
    	xa ={"#G/20%#Y/","#R/180#Y/"}
	elseif ad == "暗劲" and aa == 1 then
       xa ={"#G/6%#Y/","#R/40%#Y/"}
	elseif ad == "暗劲" and aa == 2 then
		xa ={"#G/10%#Y/","#R/20%#Y/"}
	elseif ad == "暗劲" and aa == 3 then
		xa ={"#G/13%#Y/","#R/20%#Y/"}
    elseif ad == "暗劲" and aa == 4 then
    	xa ={"#G/16%#Y/","#R/20%#Y/"}
	elseif ad == "暗劲" and aa == 5 then
		xa ={"#G/20%#Y/","#R/20%#Y/"}
	elseif ad == "顺势" and aa == 1 then
       xa ={"#G/60#Y/","#R/180#Y/"}
	elseif ad == "顺势" and aa == 2 then
		xa ={"#G/90#Y/","#R/90#Y/"}
	elseif ad == "顺势" and aa == 3 then
		xa ={"#G/120#Y/","#R/90#Y/"}
    elseif ad == "顺势" and aa == 4 then
    	xa ={"#G/150#Y/","#R/90#Y/"}
	elseif ad == "顺势" and aa == 5 then
		xa ={"#G/180#Y/","#R/90#Y/"}
	elseif ad == "阳护" and aa == 1 then
       xa ={"#G/33%#Y/","#R/30%#Y/"}
	elseif ad == "阳护" and aa == 2 then
		xa ={"#G/50%#Y/","#R/15%#Y/"}
	elseif ad == "阳护" and aa == 3 then
		xa ={"#G/66%#Y/","#R/15%#Y/"}
    elseif ad == "阳护" and aa == 4 then
    	xa ={"#G/83%#Y/","#R/15%#Y/"}
	elseif ad == "阳护" and aa == 5 then
		xa ={"#G/100%#Y/","#R/15%#Y/"}
	elseif ad == "预知" and aa == 1 then
       xa ={"#G/10%#Y/","#R/8#Y/"}
	elseif ad == "预知" and aa == 2 then
		xa ={"#G/16%#Y/","#R/4#Y/"}
	elseif ad == "预知" and aa == 3 then
		xa ={"#G/21%#Y/","#R/4#Y/"}
    elseif ad == "预知" and aa == 4 then
    	xa ={"#G/26%#Y/","#R/4#Y/"}
	elseif ad == "预知" and aa == 5 then
		xa ={"#G/32%#Y/","#R/4#Y/"}
	elseif ad == "识药" and aa == 1 then
       xa ={"#G/1.2%#Y/","#R/20%#Y/"}
	elseif ad == "识药" and aa == 2 then
		xa ={"#G/2.4%#Y/","#R/10%#Y/"}
	elseif ad == "识药" and aa == 3 then
		xa ={"#G/3.6%#Y/","#R/10%#Y/"}
    elseif ad == "识药" and aa == 4 then
    	xa ={"#G/4.8%#Y/","#R/10%#Y/"}
	elseif ad == "识药" and aa == 5 then
		xa ={"#G/6.0%#Y/","#R/10%#Y/"}
	elseif ad == "御风" and aa == 1 then
       xa ={"#G/7#Y/","#R/20%#Y/"}
	elseif ad == "御风" and aa == 2 then
		xa ={"#G/10#Y/","#R/10%#Y/"}
	elseif ad == "御风" and aa == 3 then
		xa ={"#G/14#Y/","#R/10%#Y/"}
    elseif ad == "御风" and aa == 4 then
    	xa ={"#G/17#Y/","#R/10%#Y/"}
	elseif ad == "御风" and aa == 5 then
		xa ={"#G/21#Y/","#R/10%#Y/"}
	elseif ad == "瞬法" and aa == 1 then
       xa ={"#G/33%#Y/","#R/20%#Y/"}
	elseif ad == "瞬法" and aa == 2 then
		xa ={"#G/50%#Y/","#R/10%#Y/"}
	elseif ad == "瞬法" and aa == 3 then
		xa ={"#G/66%#Y/","#R/10%#Y/"}
    elseif ad == "瞬法" and aa == 4 then
    	xa ={"#G/83%#Y/","#R/10%#Y/"}
	elseif ad == "瞬法" and aa == 5 then
		xa ={"#G/100%#Y/","#R/10%#Y/"}
	elseif ad == "灵法" and aa == 1 then
       xa ={"#G/33%#Y/","#R/10%#Y/"}
	elseif ad == "灵法" and aa == 2 then
		xa ={"#G/50%#Y/","#R/5%#Y/"}
	elseif ad == "灵法" and aa == 3 then
		xa ={"#G/66%#Y/","#R/5%#Y/"}
    elseif ad == "灵法" and aa == 4 then
    	xa ={"#G/83%#Y/","#R/5%#Y/"}
	elseif ad == "灵法" and aa == 5 then
		xa ={"#G/100%#Y/","#R/5%#Y/"}
	elseif ad == "怒吼" and aa == 1 then
       xa ={"#G/50#Y/","#R/6%#Y/"}
	elseif ad == "怒吼" and aa == 2 then
		xa ={"#G/71#Y/","#R/6%#Y/"}
	elseif ad == "怒吼" and aa == 3 then
		xa ={"#G/109#Y/","#R/10%#Y/"}
    elseif ad == "怒吼" and aa == 4 then
    	xa ={"#G/160#Y/","#R/10%#Y/"}
	elseif ad == "怒吼" and aa == 5 then
		xa ={"#G/186#Y/","#R/10%#Y/"}
	elseif ad == "抗法" and aa == 1 then
       xa ={"#G/6#Y/","#R/15%#Y/"}
	elseif ad == "抗法" and aa == 2 then
		xa ={"#G/9#Y/","#R/18%#Y/"}
	elseif ad == "抗法" and aa == 3 then
		xa ={"#G/14#Y/","#R/22%#Y/"}
    elseif ad == "抗法" and aa == 4 then
    	xa ={"#G/23#Y/","#R/26%#Y/"}
	elseif ad == "抗法" and aa == 5 then
		xa ={"#G/52#Y/","#R/30%#Y/"}
	elseif ad == "洞察" and aa == 1 then
       xa ={"#G/10%#Y/","#R/8#Y/"}
	elseif ad == "洞察" and aa == 2 then
		xa ={"#G/16%#Y/","#R/7#Y/"}
	elseif ad == "洞察" and aa == 3 then
		xa ={"#G/21%#Y/","#R/6#Y/"}
    elseif ad == "洞察" and aa == 4 then
    	xa ={"#G/26%#Y/","#R/5#Y/"}
	elseif ad == "洞察" and aa == 5 then
		xa ={"#G/32%#Y/","#R/4#Y/"}
	elseif ad == "抗物" and aa == 1 then
       xa ={"#G/6#Y/","#R/15%#Y/"}
	elseif ad == "抗物" and aa == 2 then
		xa ={"#G/9#Y/","#R/18%#Y/"}
	elseif ad == "抗物" and aa == 3 then
		xa ={"#G/14#Y/","#R/22%#Y/"}
    elseif ad == "抗物" and aa == 4 then
    	xa ={"#G/23#Y/","#R/26%#Y/"}
	elseif ad == "抗物" and aa == 5 then
		xa ={"#G/52#Y/","#R/30%#Y/"}
	elseif ad == "抗法" and aa == 1 then
       xa ={"#G/6#Y/","#R/15%#Y/"}
	elseif ad == "抗法" and aa == 2 then
		xa ={"#G/9#Y/","#R/18%#Y/"}
	elseif ad == "抗法" and aa == 3 then
		xa ={"#G/14#Y/","#R/22%#Y/"}
    elseif ad == "抗法" and aa == 4 then
    	xa ={"#G/23#Y/","#R/26%#Y/"}
	elseif ad == "抗法" and aa == 5 then
		xa ={"#G/52#Y/","#R/30%#Y/"}
	elseif ad == "护佑" and aa == 1 then
       xa ={"#G/13#Y/","#R/10%#Y/"}
	elseif ad == "护佑" and aa == 2 then
		xa ={"#G/29#Y/","#R/8%#Y/"}
	elseif ad == "护佑" and aa == 3 then
		xa ={"#G/42#Y/","#R/7%#Y/"}
    elseif ad == "护佑" and aa == 4 then
    	xa ={"#G/69#Y/","#R/6%#Y/"}
	elseif ad == "护佑" and aa == 5 then
		xa ={"#G/76#Y/","#R/5%#Y/"}
	elseif ad == "巧劲" and aa == 1 then
       xa ={"#G/2#Y/","#R/23%#Y/"}
	elseif ad == "巧劲" and aa == 2 then
		xa ={"#G/5#Y/","#R/20%#Y/"}
	elseif ad == "巧劲" and aa == 3 then
		xa ={"#G/8#Y/","#R/17%#Y/"}
    elseif ad == "巧劲" and aa == 4 then
    	xa ={"#G/12#Y/","#R/15%#Y/"}
	elseif ad == "巧劲" and aa == 5 then
		xa ={"#G/25#Y/","#R/12%#Y/"}
	end
	local a
	if ad == "灵刃" then
	  a ="#Y/第二回合以后进场时，"..xa[1].."概率对自身造成#R/30%#Y/气血上限的伤害并且物理伤害结果提高#R/50%#Y/，持续4回合，防御力，法术防御力降低"..xa[2].."(带有鬼混和神佑类技能，伤害结果只提高10%)"
	elseif ad == "预知" then
	　a = "#Y/感受到危险时有"..xa[1].."概率喊话,每场战斗最多3次,全属性降低"..xa[2]
	elseif ad == "灵动" then
		a= "#Y/行动时有"..xa[1].."概率与队友进行交流，每场战斗最多3次，全属性降低"..xa[2]
	elseif ad == "瞬击" then
		a= "#Y/第二回合以后进场时，"..xa[1].."概率对气血百分比最低的单位发动一次攻击，气血上限降低"..xa[2]
	elseif ad == "抗法" then
		a="#Y/在场时，我方所有单位法术防御增加"..xa[1].."，自身受到物理伤害增加"..xa[2]
	elseif ad == "阳护" then
	    a="#Y/第二回合以后进场时，"..xa[1].."减少所有我方单位身上“死亡禁锢”状态2回合，防御力下降"..xa[2]
	   elseif ad == "识物" then
	   a="#Y/召唤兽套装附带的技能为法术技能时，触发概率提高"..xa[1].."全属性降低"..xa[2]
	elseif ad == "洞察" then
	a="#Y/PVP战斗中行动时有"..xa[1].."概率发现什么，全属性降低"..xa[2]
	elseif ad == "弑神" then
	a="#Y/对已经神佑过的召唤兽法术伤害增加"..xa[1].."，普通攻击命中率下降"..xa[2]
	elseif ad == "御风" then
		a="#Y/进场时提高主人的速度"..xa[1].."，倒地或者死亡后无效，持续3回合，受到所有伤害增加"..xa[2]
	elseif ad == "顺势" then
		a="#Y/对气血百分低于#G/70%#Y/的单位法术伤害增加"..xa[1].."，对其余单位法术伤害力降低"..xa[2]
	elseif ad == "复仇" then
		a="#Y/主人被击倒时，有"..xa[1].."概率向敌方单位发动一次攻击，最多生效3次。减少所有防御力"..xa[2]
	elseif ad == "自恋" then
		a="#Y/普通物理攻击击杀任意单位时有"..xa[1].."概率进行炫耀，每场战斗最多1次，全属性降低"..xa[2]
	elseif ad == "怒吼" then
		a="#Y/第二回合以后进场时，我方伤害最高的单位提高"..xa[1].."伤害力，持续2回合，法术伤害力降低"..xa[2]
	elseif ad == "暗劲" then
		　a="#Y/属性法术被吸收时，有"..xa[1].."概率使恢复量不超过1,普通物理攻击命中率下降"..xa[2]
	elseif ad == "逆境" then
		a="#Y/进场时，若已方四个以上单位处于被封印状态，则"..xa[1].."解除我方所有单位异常状态，防御力下降"..xa[2]
	elseif ad == "瞬法" then
		a="#Y/第2回合以后进场时，"..xa[1].."概率对血量百分比最低的单位使用随机法术，气血上限降低"..xa[2]
	elseif ad == "乖巧" then
		a="#Y/使用超级巫医时，耗费的银两降低"..xa[1].."，全属性降低"..xa[2]
	elseif ad == "抗物" then
		a="#Y/在场时，我方所有单位物理防御增加"..xa[1].."，自身受到法术伤害增加"..xa[2]
	elseif ad == "护佑" then
		a="#Y/第2回合进场，我方百分比最低的单位受到的伤害降低"..xa[1].."持续2回合，法术伤害力降低#R/"..xa[2]
	elseif ad == "识药" then
		a="#Y/药物效果提高"..xa[1].."受到的所有伤害增加"..xa[2]
	elseif ad == "吮魔" then
		a="#Y/普通物理攻击时吸取"..xa[1].."的魔法，对当前魔法高于#R/50%#Y/的单位伤害结果少"..xa[2]
	elseif ad == "灵断" then
		a="#Y/第二回合以后进场是，"..xa[1].."的几率队自身造成#R/30%#Y/气血上限的伤害并且无视地方神佑鬼魂类技能，持续#R/4#Y/回合，气血上限降低#G/5%#Y/(带有鬼魂类或者神佑技能时，进场对自己造成"..xa[2].."气血上限伤害)"
	elseif ad == "争锋" then
		a="#Y/对召唤兽物理伤害结果增加"..xa[1].."对人物伤害结果减少"..xa[2]
	elseif ad == "力破" then
		a="#Y/忽略人物角色#R/"..xa[1].."#Y/防御力".."减少对召唤兽的伤害结果"..xa[2]
	elseif ad == "灵法" then
		a="#Y/第二回合进场后，提高"..xa[1].."自身气血法术伤害，并提高50%法术伤害结果。持续#R/4#Y/回合,减少所有防御力"..xa[2]
	elseif ad == "巧劲" then
	　　a="#Y/普通攻击增加伤害结果"..xa[1].."受到的所有伤害增加"..xa[2]
 end

	self.介绍文本:添加文本(a)
	local w = self.介绍文本.显示表.宽度
	local h = self.介绍文本.显示表.高度
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h >= 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 =self.介绍文本
	self.寄存内容[1].x = x + 10
	self.寄存内容[1].y = y + 10
	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true

end


function 系统类_提示框:道具行囊(x,y,item)
	if item == nil then
		return
	end
	local x = x + 30
	local y = y
	local w = 300
	local h = 18
	 zf = "#Y/"
	self.道具文本:清空()
	self.道具文本:添加文本("")
	self.道具文本:添加文本(item.说明)
	local zls = item.类型
	if zls == "药品" then
		local gs = ""
		self.道具文本:添加文本("#W".."【功效】"..item.功效)
		if item.气血 ~= nil then
		gs = gs..zf..zf.."恢复气血"..item.气血..", "
		end
		if item.魔法 ~= nil then
		gs = gs..zf..zf.."恢复魔法"..item.魔法..", "
		end
		if item.伤势 ~= nil then
		gs = gs..zf..zf.."治疗伤势"..item.伤势..", "
		end
		if item.品质 ~= 0 and item.品质 ~= nil then
		self.道具文本:添加文本(zf.."品质 "..item.品质..", "..gs.."等级:"..item.等级)
		else
		self.道具文本:添加文本(zf..gs.."等级:"..item.等级)
		end
	elseif zls == "烹饪"	 then
		self.道具文本:添加文本("#W/".."【功效】"..item.功效)
		if item.类别 == "酒"then
			self.道具文本:添加文本("#W/只能在战斗中对自己使用")
		end
		if item.品质 ~= 0 and item.品质 ~= nil then
		self.道具文本:添加文本(zf.."品质 "..item.品质)
		end
	elseif item.名称 == "鬼谷子" then
		self.道具文本:添加文本(zf.."阵法: "..item.类型)
	elseif zls == "杂货" then
		if item.名称 == "符石卷轴" then
		self.道具文本:添加文本("【说明】需要加入此卷轴才可以合成高级符石,在合成过程中会消耗一定的体力和金钱")
	   end
	elseif zls == "宝石"	 then
		self.道具文本:添加文本("#W/".."【镶嵌装备】"..item.部位)
		self.道具文本:添加文本("#W/".."【镶嵌效果】"..item.效果)
		if item.等级 ==nil then
		 self.道具文本:添加文本("#Y/随机获得1-8级宝石")
		else
	    self.道具文本:添加文本(zf.."等级 "..item.等级)
	    end
	elseif zls == "鞋子" or zls == "腰带"  or zls == "项链"  or zls =="头盔"  or zls == "衣服" or zls == "武器"then -- 判断装备药品
		 if item.角色限制 ~= nil then
			if #item.角色限制 > 1 then
			self.道具文本:添加文本("#W/【装备角色】"..item.角色限制[1].."、"..item.角色限制[2])
			else
			self.道具文本:添加文本("#W/【装备角色】"..item.角色限制[1])
			end
		end
		if item.性别限制 ~= nil then
		self.道具文本:添加文本("#W/【装备角色】"..item.性别限制)
	    end
	    self.道具文本:添加文本("#Y/等级 "..item.等级.." 五行 "..item.五行)
    	local sx = ""
		local 气血 = item.气血
		local 魔法 = item.魔法
		local 命中 = item.命中
		local 伤害 = item.伤害
		local 防御 = item.防御
		local 速度 = item.速度
		local 躲避 = item.躲避
		local 灵力 = item.灵力
		local 敏捷 = item.敏捷
		if 伤害 ~= 0 and 伤害 ~= nil  then
			sx = sx.."伤害 +"..伤害.." "
		end
		if 命中 ~= 0  and 命中 ~= nil then
			sx = sx.."命中 +"..命中.." "
		end
		if 防御 ~= 0 and 防御 ~= nil  then
			sx = sx.."防御 +"..防御.." "
		end
		if 气血 ~= 0 and 气血 ~= nil then
			sx = sx.."气血 +"..气血.." "
		end
		if 魔法 ~= 0 and 魔法 ~= nil  then
			sx = sx.."魔法 +"..魔法.." "
		end
		if 速度 ~= 0 and 速度 ~= nil  then
			sx = sx.."速度 +"..速度.." "
		end
		if 躲避 ~= 0 and 躲避 ~= nil  then
			sx = sx.."躲避 +"..躲避.." "
		end
		if 灵力 ~= 0 and 灵力 ~= nil  then
			sx = sx.."灵力 +"..灵力.." "
		end
		if 敏捷 ~= 0 and 敏捷 ~= nil  then
			sx = sx.."敏捷 +"..敏捷.." "
		end
		if sx ~= "" then
			self.道具文本:添加文本("#Y/"..sx)
		end
	    self.道具文本:添加文本("#Y/耐久度 "..floor(item.耐久度))
		if item.锻造数据 ~= nil then
			--	local bsz = ""
				--for i=1,#item.镶嵌宝石 do
				--	local sw = "、"
				--	if i == #item.镶嵌宝石 then
				--		sw = ""
				--	end
				--	bsz = bsz..item.镶嵌宝石[i]..sw
			--	end
			--	self.道具文本:添加文本(format("#Y/锻炼等级 %s   镶嵌宝石 %s",item.锻炼等级,bsz))
		end
   		if #item.三围属性 ==2 then
   			self.道具文本:添加文本("#G/"..item.三围属性[1].类型.."+"..item.三围属性[1].数值.." "..item.三围属性[2].类型.."+"..item.三围属性[2].数值)
   		elseif #item.三围属性 ==1 then
			self.道具文本:添加文本("#G/"..item.三围属性[1].类型.."+"..item.三围属性[1].数值)
		end
		if item.特技 ~= nil then
			self.道具文本:添加文本(format("#S/特技：%s",item.特技))
		end
		if item.特效 ~= nil then
			for i=1,#item.特效 do
				self.道具文本:添加文本(format("#S/特效：%s",item.特效[i]))
			end
		end
		if item.套装 ~= nil then
			self.道具文本:添加文本(format("#S/套装效果：%s",item.套装.类型..item.套装.名称))
		end
		if item.专用 ~= nil then
			self.道具文本:添加文本("#S/玩家"..item.专用.."专用")
		end
       if  item.符石.最小孔数 > 0  then
          	self.道具文本:添加文本("#G/开运孔数: "..item.符石.最小孔数.."/"..item.符石.最大孔数)
         for i=1,5 do
            if item.符石[i]~= nil then
		        local sx ={}
		         sx[i] = ""
		        local 气血={}
		        气血[i] = item.符石[i].气血
		        local 魔法={}
		        魔法[i] = item.符石[i].魔法
		        local 命中={}
		        命中[i] = item.符石[i].命中
		        local 伤害={}
		        伤害[i] = item.符石[i].伤害
		        local 防御={}
		        防御[i] = item.符石[i].防御
		        local 速度={}
		        速度[i] = item.符石[i].速度
		        local 躲避={}
		        躲避[i] = item.符石[i].躲避
		        local 灵力={}
		        灵力[i] = item.符石[i].灵力
		        local 敏捷={}
		        敏捷[i] = item.符石[i].敏捷
		        local 体质={}
		        体质[i] = item.符石[i].体质
		        local 耐力={}
		        耐力[i] = item.符石[i].耐力
		        local 法术防御={}
		        法术防御[i] = item.符石[i].法术防御
		        local 魔力={}
		        魔力[i] = item.符石[i].魔力
		        local 力量={}
		        力量[i] = item.符石[i].力量
		        local 法术伤害={}
		        法术伤害[i] = item.符石[i].法术伤害
		        local 固定伤害={}
		        固定伤害[i] = item.符石[i].固定伤害
		        if 体质[i] ~= 0 and 体质[i] ~= nil  then
		            sx[i] = sx[i].."体质 +"..体质[i].." "
		        end
		        if 耐力[i] ~= 0 and 耐力[i] ~= nil  then
		            sx[i] = sx[i].."耐力 +"..耐力[i].." "
		        end
		        if 法术防御[i] ~= 0 and 法术防御[i] ~= nil  then
		            sx[i] = sx[i].."法术防御 +"..法术防御[i].." "
		        end
		        if 魔力[i] ~= 0 and 魔力[i] ~= nil  then
		            sx[i] = sx[i].."魔力 +"..魔力[i].." "
		        end
		        if 力量[i] ~= 0 and 力量[i] ~= nil  then
		            sx[i] = sx[i].."力量 +"..力量[i].." "
		        end
		        if 法术伤害[i] ~= 0 and 法术伤害[i] ~= nil  then
		            sx[i] = sx[i].."法术伤害 +"..法术伤害[i].." "
		        end
		        if 固定伤害[i] ~= 0 and 固定伤害[i] ~= nil  then
		            sx[i] = sx[i].."固定伤害 +"..固定伤害[i].." "
		        end
		        if 伤害[i] ~= 0 and 伤害[i] ~= nil  then
		            sx[i] = sx[i].."伤害 +"..伤害[i].." "
		        end
		        if 命中[i] ~= 0  and 命中[i] ~= nil then
		            sx[i] = sx[i].."命中 +"..命中[i].." "
		        end
		        if 防御[i] ~= 0 and 防御[i] ~= nil  then
		            sx[i] = sx[i].."防御 +"..防御[i].." "
		        end
		        if 气血[i] ~= 0 and 气血[i] ~= nil then
		            sx[i] = sx[i].."气血 +"..气血[i].." "
		        end
		        if 魔法[i] ~= 0 and 魔法[i] ~= nil  then
		            sx[i] = sx[i].."魔法 +"..魔法[i].." "
		        end
		        if 速度[i] ~= 0 and 速度[i] ~= nil  then
		            sx[i] = sx[i].."速度 +"..速度[i].." "
		        end
		        if 躲避[i] ~= 0 and 躲避[i] ~= nil  then
		            sx[i] = sx[i].."躲避 +"..躲避[i].." "
		        end
		        if 灵力[i] ~= 0 and 灵力[i] ~= nil  then
		            sx[i] = sx[i].."灵力 +"..灵力[i].." "
		        end
		        if 敏捷[i] ~= 0 and 敏捷[i] ~= nil  then
		            sx[i] = sx[i].."敏捷 +"..敏捷[i].." "
		        end
		        if sx[i] ~= "" then
		        	self.道具文本:添加文本("#G/符石："..sx[i])
		        end
            end
         end
         if item.符石.组合 ~=nil then
		 	self.道具文本:添加文本("#F/符石组合："..item.符石.组合.名称)
		 	self.道具文本:添加文本("#F/门派条件：无")
            self.道具文本:添加文本("#F/部位条件：无")
		 	self.道具文本:添加文本("#F/"..取符石介绍(item.符石.组合))
	    end


      end
		if item.制造者 ~= nil then
			 if item.强化 ~= nil then
				 self.道具文本:添加文本(format("#W/制造者：%s",item.制造者.."强化打造"))
			else
				self.道具文本:添加文本(format("#W/制造者：%s",item.制造者))
			end
	    end
		 if item.鉴定==false then
			self.道具文本:添加文本("#W/未鉴定物品")
		 end
    elseif zls == "符石" then
     	   self.道具文本:添加文本("#Y/等级 "..item.等级.." "..item.颜色)
		    local sx = ""
			local 气血 = item.气血
			local 魔法 = item.魔法
			local 命中 = item.命中
			local 伤害 = item.伤害
			local 防御 = item.防御
			local 速度 = item.速度
			local 躲避 = item.躲避
			local 灵力 = item.灵力
			local 敏捷 = item.敏捷
			local 体质 = item.体质
			local 耐力 = item.耐力
			local 法术防御 = item.法术防御
			local 魔力 = item.魔力
			local 力量 = item.力量
			local 法术伤害 = item.法术伤害
			local 固定伤害 = item.固定伤害
			if 体质 ~= 0 and 体质 ~= nil  then
				sx = sx.."体质 +"..体质.." "
			end
			if 耐力 ~= 0 and 耐力 ~= nil  then
				sx = sx.."耐力 +"..耐力.." "
			end
			if 法术防御 ~= 0 and 法术防御 ~= nil  then
				sx = sx.."法术防御 +"..法术防御.." "
			end
			if 魔力 ~= 0 and 魔力 ~= nil  then
				sx = sx.."魔力 +"..魔力.." "
			end
			if 力量 ~= 0 and 力量 ~= nil  then
				sx = sx.."力量 +"..力量.." "
			end
			if 法术伤害 ~= 0 and 法术伤害 ~= nil  then
				sx = sx.."法术伤害 +"..法术伤害.." "
			end
			if 固定伤害 ~= 0 and 固定伤害 ~= nil  then
				sx = sx.."固定伤害 +"..固定伤害.." "
			end
			if 伤害 ~= 0 and 伤害 ~= nil  then
				sx = sx.."伤害 +"..伤害.." "
			end
			if 命中 ~= 0  and 命中 ~= nil then
				sx = sx.."命中 +"..命中.." "
			end
			if 防御 ~= 0 and 防御 ~= nil  then
				sx = sx.."防御 +"..防御.." "
			end
			if 气血 ~= 0 and 气血 ~= nil then
				sx = sx.."气血 +"..气血.." "
			end
			if 魔法 ~= 0 and 魔法 ~= nil  then
				sx = sx.."魔法 +"..魔法.." "
			end
			if 速度 ~= 0 and 速度 ~= nil  then
				sx = sx.."速度 +"..速度.." "
			end
			if 躲避 ~= 0 and 躲避 ~= nil  then
				sx = sx.."躲避 +"..躲避.." "
			end
			if 灵力 ~= 0 and 灵力 ~= nil  then
				sx = sx.."灵力 +"..灵力.." "
			end
			if 敏捷 ~= 0 and 敏捷 ~= nil  then
				sx = sx.."敏捷 +"..敏捷.." "
			end
			if sx ~= "" then
				self.道具文本:添加文本("#Y/"..sx)
			end
			self.道具文本:添加文本(format("#Y/耐久 "..item.耐久))
	elseif zls == "手镯" or zls == "戒指"  or zls == "佩饰"  or zls =="耳饰" then
			self.道具文本:添加文本("#W/【装备条件】等级"..item.等级)
			self.道具文本:添加文本("#W/【灵饰类型】"..item.类型)
			self.道具文本:添加文本("#Y/等级 "..item.等级)
            self.道具文本:添加文本("#Y/"..item.幻化属性.基础.类型.." +"..item.幻化属性.基础.数值)
			self.道具文本:添加文本("#Y/耐久度 "..item.耐久)
            for i=1,#item.幻化属性.附加 do
            	self.道具文本:添加文本("#G/"..item.幻化属性.附加[i].类型.." +"..item.幻化属性.附加[i].数值)
            end
			self.道具文本:添加文本("#W/制造者： "..item.制造者)
	elseif zls == "炼妖" then
		if item.名称 == "召唤兽内丹"  or item.名称 == "高级召唤兽内丹" then
			if item.技能 == nil then
				self.道具文本:添加文本("#Y/随机生成内丹")
			else
			self.道具文本:添加文本("#Y/所带内丹技能:"..item.技能)
			self.道具文本:添加文本(self:取内丹介绍(item.技能))
		    end
		elseif item.名称 =="魔兽要诀" or item.名称 =="高级魔兽要诀" then
			if item.技能 == nil then
			self.道具文本:添加文本("#Y/随机生成技能")
			else
			self.道具文本:添加文本("#Y/所带技能:"..item.技能)
		   end
		elseif item.名称 =="超级金柳露" or item.名称 =="金柳露" then
		   self.道具文本:添加文本("#w/【功效】"..item.功效)
		end
	elseif zls == 5 then
		if item.分类 == 1 then
			local it = tp:取武器子类(item.特效)
			self.道具文本:添加文本(format("#Y/等级 %d 种类 %s",item.子类,it))
		elseif item.分类 == 2 then
			self.道具文本:添加文本(format("#Y/等级 %d",item.子类))
		elseif item.分类 == 3 then
			self.道具文本:添加文本("#Y/功效 用于分解装备获得宝石")
		elseif item.分类 == 4 then
			self.道具文本:添加文本(format("#Y/功效 %s",item.特效))
		elseif item.分类 == 6 then
			self.道具文本:添加文本(format("#W/【镶嵌装备】%s",item.角色限制))
			self.道具文本:添加文本(format("#W/【镶嵌效果】%s",item.特效))
			self.道具文本:添加文本(format("#Y/等级 %s",item.级别限制))
		end
	elseif zls == 1 then
		if item.分类 == 1 then
			self.道具文本:添加文本("#Y/到达指定地点右键挖宝")
			self.道具文本:添加文本(format("#Y/场景 %s",item.伤害))
			self.道具文本:添加文本(format("#Y/坐标 X：%d Y：%d",item.气血,item.魔法))
		end
	elseif zls == "召唤兽道具" then
		if item.名称 =="高级清灵仙露" or item.名称 =="中级清灵仙露"  or item.名称 =="初级清灵仙露"  then
			self.道具文本:添加文本("#Y/"..item.灵气)
		end
	elseif item.名称 == "怪物卡片" then
		self.道具文本:添加文本("#W/卡片类型："..item.类型)
		self.道具文本:添加文本("#W/技能要求："..item.变化之术等级)
		self.道具文本:添加文本("#W/【附加技能】"..item.技能)
		self.道具文本:添加文本("#W/【属性影响】"..item.属性)
		self.道具文本:添加文本("#Y/等级："..item.等级.."，使用次数："..item.使用次数.."，")
		self.道具文本:添加文本("#Y/持续时间：15分钟*（1+变化之术等级）")
	elseif zls == "剧情道具" then
		if item.名称 =="特赦令牌"  then
			self.道具文本:添加文本("#W/【用途】交给地府迷宫三层的无名野鬼可以换取一张稀世的高级藏宝图")
			self.道具文本:添加文本("#Y/特赦令牌")
		end
	elseif zls == 11 then
		if item.伤害 == nil then
			self.道具文本:添加文本(format("#Y/右键双击之后在该地点定下坐标，总共可以使用%d次",item.子类))
		else
			local map = tp:取地图名称(item.伤害)
			self.道具文本:添加文本(format("#Y/场景：%s\n#Y/坐标x：%d y：%d",map,item.气血,item.魔法))
			self.道具文本:添加文本(format("#Y/剩余次数：%s",item.子类))
		end
	elseif zls == "合成旗"  then
		if item.地图编号 ==0 then
			self.道具文本:添加文本("#Y/未定标")
		else
			self.道具文本:添加文本("#Y/场景:"..item.地图.."，还能使用"..item.次数.."次")
		end
	elseif zls == "导标旗"  then
		if item.地图编号 ==0 then
			self.道具文本:添加文本("#Y/未定标")
		else
			self.道具文本:添加文本("#Y/记录坐标是"..item.地图名称.."("..item.x..","..item.y..")")
			self.道具文本:添加文本("#Y/还能使用"..item.次数.."次")
		end
	elseif zls == "暗器"  then
		self.道具文本:添加文本("#W/【基本伤害】"..item.等级)
		self.道具文本:添加文本("#Y/命中 +"..item.等级.."，装备等级 1，耐久度 "..item.耐久)
	elseif zls == 13 then
		self.道具文本:添加文本(format("#Y/阵型 %s",tp.阵型表[item.子类][1]))
	elseif zls == "消耗道具" then
		if item.名称 == "九转金丹" then
		self.道具文本:添加文本("#W/【功效】增加人物修炼经验=品质*0.5")
		self.道具文本:添加文本("#Y/大于等于40级才可以使用")
		self.道具文本:添加文本("#Y/品质："..item.品质)
	   elseif item.名称 =="天眼通符" then
	   	    self.道具文本:添加文本("#W/【功能】可以传送部分任务要求的NPC准确坐标。")
	   elseif item.名称 =="藏宝图" then
	   	    self.道具文本:添加文本("#Y/【类型】普通藏宝图")
	   elseif item.名称 =="高级藏宝图" then
	   	    self.道具文本:添加文本("#Y/【类型】高级藏宝图")
	   	    self.道具文本:添加文本("#Y/【坐标】#G/"..item.地图名称.."("..item.x.."，"..item.y..")")
       elseif item.名称 =="修炼果" then
          self.道具文本:添加文本("#W/【功能】提高某种修炼经验，经验达到要求后可提供修炼等级，从而控制召唤兽发挥更强的潜能。人物等级必须达到65级方可使用。")
        elseif item.名称 =="一级未激活符石" or item.名称 =="二级未激活符石" or item.名称 =="三级未激活符石"then
          	self.道具文本:添加文本("#W/【激活方式】鼠标右键点击使用激活")
	    end
	elseif zls == "打造" then  --打造
     if item.名称 == "元灵晶石" then
		self.道具文本:添加文本("#Y/装备等级 "..item.等级)
     elseif item.名称 == "灵饰指南书" then
     	self.道具文本:添加文本("#Y/种类 "..item.种类.."，等级 "..item.等级)
     end
	elseif zls == "法宝" then----法宝
		self.道具文本:添加文本("#W/【法宝类型】："..item.法宝类型)
				local xz;
		if item.佩戴  then
			xz = ",需要佩戴才可在战斗中发挥效用"
		else
			if item.回合限制 ==nil then
			xz = (",无需佩戴，在战斗中选择使用,")
			else
		    xz = format(",无需佩戴，在战斗中选择使用,使用回合限制：#G/%d#Y/",item.回合限制)
		   end
		end
		self.道具文本:添加文本(format("#G/%d#Y/级法宝,修炼境界：第#G/%d#Y/层 %s",item.等级,item.层数,取境界(item.等级,item.层数))..format("#Y/,灵气:#G/%d#Y/,五行:#G/%s",item.灵气,item.五行)..format("#Y/%s",xz)..format("#Y/使用装备限制：#G/人物等级≥%d",item.等级限制))
	end
	h = self.道具文本.显示表.高度+10
	if h < 130 then
		h = 130
	end
	if x + w >= 780 then
		x = 780 - w
	end
	if y + h > 580 then
		y = y - h - 30
	end
	self.寄存内容 = {}
	self.寄存内容[1] = {}
	self.寄存内容[1].内容 = item.大模型
	self.寄存内容[1].x = x+3
	self.寄存内容[1].y = y+5
	self.寄存内容[2] = {}
	self.寄存内容[2].内容 = self.道具文本
	self.寄存内容[2].x = x + 128
	self.寄存内容[2].y = y + 15
	self.寄存内容[3] = {}
	self.寄存内容[3].文字 = tp.字体表.道具字体
	self.寄存内容[3].颜色 = -256
	self.寄存内容[3].文本 = item.名称
	self.寄存内容[3].坐标 = {[1]=x+128.8,[2] =y+4}
	self.寄存内容.提示坐标 = {[1]=x,[2]=y,[3]=w,[4]=h}
	self.寄存内容.开启提示 = true
end
function 系统类_提示框:取内丹介绍(a)
	if a =="迅敏" then
		return "#Y/拥有此技能会提高自己的伤害力与速度。"
	elseif a =="狂怒" then
		return "#Y/在此狂乱状态下必杀的伤害更高，但是受到水、土系法术攻击时承受额外的伤害。"
	elseif a =="阴伤" then
		return "#Y/疯狂攻击的第二次伤害更高，但是受到火、雷系法术攻击时承受额外的伤害。"
	elseif a =="静岳" then
	   return "#Y/拥有此技能会提高自己的灵力与气血。"
	elseif a =="擅咒" then
	   return "#Y/你对目标的法术伤害得到提升。"
	elseif a =="灵身" then
	   return "#Y/法术带来的爆发性更强烈，只是受到强力与高强力技能打击时，所受到的伤害增加。"
	elseif a =="矫健" then
	   return "#Y/拥有此技能会提高自身的气血与速度。"
	elseif a =="深思" then
	   return "#Y/高冥思的效果得到加强。"
	elseif a =="坚甲" then
	   return "#Y/拥有此技能后对敌人造成的反震伤害得到加强。"
	elseif a =="钢化" then
	   return "#Y/带有高级防御、防御技能时的防御值增加，但是所带来的代价是受到法术伤害额外打击。"
	elseif a =="慧心" then
	   return "#Y/只要集中精神，抵御封印的能力就会加强。"
	elseif a =="撞击" then
	   return "#Y/物理攻击时命中率得到增加，效果与体质点成正比，同时增加一定的伤害结果。"
	elseif a =="无畏" then
	   return "#Y/拥有此技能能够更好的突破反震、高级反震技能，对目标造成更大的物理伤害。"
	elseif a =="愤恨" then
	   return "#Y/拥有此技能能够更好的突破幸运、高级幸运技能，对目标造成更大的物理伤害。"
	elseif a =="淬毒" then
	   return "#Y/满淬毒汁的毒牙使对手更加胆寒。"
	elseif a =="狙刺" then
	   return "#Y/该召唤兽对第一目标造成的法术伤害更大。"
	elseif a =="连环" then
		return "#Y/拥有此技能时，召唤兽触发连击的几率增加。"
	elseif a =="圣洁" then
	    return "#Y/拥有此技能后，你的召唤兽使用法术攻击时驱鬼和高驱鬼效果得到提升。"
	elseif a =="灵光" then
	    return "#Y/法术的精修必然会为你带来更多好处。"
	elseif a =="神机步" then
	    return "#Y/入场时你总是活力无限，3回合内极大的增加你的躲避力。"
	elseif a =="腾挪劲" then
	    return "#Y/神奇的护盾，有一定几率能够将你所受的一部分物理伤害腾挪到另一个世界。"
	elseif a =="玄武躯" then
	    return "#Y/你放弃了伤害，得到的是气血的大幅度提升。"
	elseif a =="龙胄铠" then
	    return "#Y/你放弃了伤害，得到的是防御的大幅度提升。"
	elseif a =="玉砥柱" then
	    return "#Y/最尖锐的矛也遇到了一点麻烦。"
	elseif a =="碎甲刃" then
	    return "#Y/以千钧之力击碎目标的护甲，2回合内降低目标一定防御值，效果与自身力量点相关。"
	elseif a =="阴阳护" then
	     return "#Y/激发潜力，保护他人！你在保护其他目标的时候所承受的伤害减少。"
	elseif a =="凛冽气" then
	     return "#Y/霸气外露，你的在场是本方所有召唤兽的强心剂，逃跑的几率也减少了。"
	elseif a =="舍身击" then
	     return "#Y/置之死地而后生，你对目标造成的物理伤害值受到力量点的加成。"
	elseif a =="电魂闪" then
	     return "#Y/神奇的法术，总在不经意间给你惊喜，有可能将目标携带的增益状态驱散。"
	elseif a =="通灵法" then
	     return "#Y/敏锐的洞察力！能够找出目标法术防御的漏洞，对处在法术减免状态的目标造成更大的伤害。"
	elseif a =="双星爆" then
	     return "#Y/强大的法术攻击接踵而来，法术连击的威力更强大。"
	elseif a =="催心浪" then
	     return "#Y/虽然带有法术波动技能时法术力量无法控制，但是总会向更好的方向发展。"
	elseif a =="隐匿击" then
	     return "#Y/专家级隐身。"
	elseif a =="生死决" then
	     return "#Y/此刻你如天神下凡。"
    elseif a =="血债偿" then
	     return "#Y/战友的离去反而激起了你的斗志。"

	end

end
function 系统类_提示框:清空寄存()
	if #self.寄存内容 ~= 0 then
		self.寄存内容 = {}
	end
end

return 系统类_提示框