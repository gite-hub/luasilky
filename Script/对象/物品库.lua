

function 引擎.取物品(wd)
    local wds = {}
 ----------------------------------------------------飞升任务
        if wd == "避火诀" then
            wds.说明 = "仙界的五行秘宝之一，拈此诀处在烈火中也安然无恙"
            wds.类型 = "飞升道具"
            wds.类别 = 1
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x04694A50
            wds.大模型资源 = 0xC3A8BEA9
        elseif wd == "烈焰澜翻" then
            wds.说明 = "烈焰澜翻为说时，百千诸佛尽攒眉。"
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.属性 = "固定伤害"
            wds.小模型资源 = 0xB585CF35
            wds.大模型资源 = 0xFBBA047B
        elseif wd == "双鲤寄情" then
            wds.说明 = "鸿雁在云鱼在水，惆怅此情难寄。"
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.属性 = "气血"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x81E3D00A
            wds.大模型资源 = 0x48A7F40C
        elseif wd == "凌波微步" then
            wds.说明 = "体迅飞凫，飘忽若神。凌波微步，罗袜生尘。"
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.属性 = "速度"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0xF2D53378
            wds.大模型资源 = 0x6E786275
        elseif wd == "水墨游龙" then
            wds.说明 = "翩若惊鸿，婉若游龙。                   "
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.属性 = "伤害"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x3AA1A218
            wds.大模型资源 = 0x61053102
        elseif wd == "星光熠熠" then
            wds.说明 = "天接云涛连晓雾，星河欲转千帆舞。"
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.属性 = "魔法"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0xC72D37F6
            wds.大模型资源 = 0xED812000
        elseif wd == "浩瀚星河" then
            wds.说明 = "天河夜转漂回星,银浦流云学水声。"
            wds.类型 = "锦衣"
            wds.部位 = "光环"
            wds.属性 = "灵力"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x4DEE1719
            wds.大模型资源 = 0x7DF7F4C3
        elseif wd == "荷塘涟漪" then
            wds.说明 = "池面风来波潋潋,波间露下叶田田。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x9597FC1F
            wds.大模型资源 = 0x4906ED3E
        elseif wd == "桃花飞舞" then
            wds.说明 = "桃花流水暮然去,别有天地非人间。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000007
            wds.大模型资源 = 0x00000008
        elseif wd == "雪花飘落" then
            wds.说明 = "白雪却嫌春色晚,故穿庭树作飞花。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000011
            wds.大模型资源 = 0x00000012
        elseif wd == "月影婆娑" then
            wds.说明 = "今人不见古时月,今月曾经照古人。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000013
            wds.大模型资源 = 0x00000014
        elseif wd == "祥云瑞气" then
            wds.说明 = "日日祥云瑞气连,农家应作大神仙。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000009
            wds.大模型资源 = 0x00000010
        elseif wd == "花的海洋" then
            wds.说明 = "无意苦争春,一任群芳妒。           "
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000005
            wds.大模型资源 = 0x00000006
        elseif wd == "爱的光影" then
            wds.说明 = "爱心闪动,光影年华。            "
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000001
            wds.大模型资源 = 0x00000002
        elseif wd == "珠落玉盘" then
            wds.说明 = "嘈嘈切切错杂弹，大珠小珠落玉盘。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000015
            wds.大模型资源 = 0x00000016
        elseif wd == "红叶随风" then
            wds.说明 = "晚风岸抚柳,看红叶秋色然。           "
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "光环"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000003
            wds.大模型资源 = 0x00000004
        elseif wd == "碧华锦·凌雪" then
            wds.说明 = "碧华锦·凌雪。情深似海翻澜起，心悦君兮浪淘纱。织女素手织就浪淘纱，大浪淘纱，海枯石烂，此情不泯。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x02000109
            wds.大模型资源 = 0x02000110
        elseif wd == "浪淘纱·月白" then
            wds.说明 = "夏日绮梦，雪浪淘纱。情深似海翻澜起，心悦君兮浪淘纱。织女素手织就浪淘纱，大浪淘纱，海枯石烂，此情不泯。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x01100009
            wds.大模型资源 = 0x01100003
        elseif wd == "月白" then
            wds.说明 = "夏日绮梦，雪浪淘纱。情深似海翻澜起，心悦君兮浪淘纱。织女素手织就浪淘纱，大浪淘纱，海枯石烂，此情不泯。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "asd.wdf"
            wds.小模型资源 = 0x00000004
            wds.大模型资源 = 0x00000003
        elseif wd == "龙卷风足迹" then
            wds.说明 = "龙卷风来啦！大家都快闪开，被刮跑了可不是玩的。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000020
            wds.大模型资源 = 0x00000021
        elseif wd == "皮球足迹" then
            wds.说明 = "真皮球是一种材质的球，适合在木地板上打。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000023
            wds.大模型资源 = 0x00000022
        elseif wd == "雀屏足迹" then
            wds.说明 = "孔雀开屏，骄傲的尾巴闪耀着灼灼光华。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000025
            wds.大模型资源 = 0x00000024
        elseif wd == "旋律足迹" then
            wds.说明 = "生动的语言节奏和丰富的生活节奏是旋律节奏的自然基础。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000027
            wds.大模型资源 = 0x00000026
        elseif wd == "枫叶足迹" then
            wds.说明 = "秋风扫落叶，行走时犹如带着秋风扫起一片片枫叶。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000029
            wds.大模型资源 = 0x00000028
        elseif wd == "飞天足迹" then
            wds.说明 = "多种颜色至于身后，犹如飞天般的感觉"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000031
            wds.大模型资源 = 0x00000030
        elseif wd == "两心相悦足迹" then
            wds.说明 = "从我的第一眼看到你，我就爱上你了。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000033
            wds.大模型资源 = 0x00000032
        elseif wd == "心花怒放" then
            wds.说明 = "故～，髓海难枯，而杰构鸿篇，大率超群出类。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000035
            wds.大模型资源 = 0x00000034
        elseif wd == "雷电足迹" then
            wds.说明 = "电闪雷鸣，莫装逼，装逼被雷劈。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000037
            wds.大模型资源 = 0x00000036
        elseif wd == "寒冰足迹" then
            wds.说明 = "行走间，犹如冬天来到。          "
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000039
            wds.大模型资源 = 0x00000038
        elseif wd == "星星祈愿" then
            wds.说明 = "花一些时间向星星许愿吧。        "
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000041
            wds.大模型资源 = 0x00000040
        elseif wd == "地狱焰火" then
            wds.说明 = "那闪动的火光肆无忌惮燃烧着，跳跃着。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000043
            wds.大模型资源 = 0x00000042
        elseif wd == "元宝足迹" then
            wds.说明 = "我是一个财迷，有钱我就会出现。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000045
            wds.大模型资源 = 0x00000044
        elseif wd == "光剑足迹" then
            wds.说明 = "背后出现了神秘的光晕，有锐利的光芒投射过来。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000047
            wds.大模型资源 = 0x00000046
        elseif wd == "雪花足迹" then
            wds.说明 = "雪花纷飞，犹如大雪即将来临。"
            wds.类型 = "锦衣"
            wds.属性 = "法术伤害"
            wds.部位 = "足迹"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x00000049
            wds.大模型资源 = 0x00000048
        elseif wd == "华风汉雅" then
            wds.说明 = "华风整体的感觉就是宽袖长袍，衣袂飘飘（有战斗锦衣）。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WE.dll"
            wds.小模型资源 = 0x00000078
            wds.大模型资源 = 0x00000077
        elseif wd == "夜影" then
            wds.说明 = "如鬼魅般隐藏在夜色之中，出其不意给予敌方致命一击（有战斗锦衣）。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0xFFED8420
            wds.大模型资源 = 0xFFED8419
        elseif wd == "夏日清凉" then
            wds.说明 = "夏日专属的清凉套装，一起享受夏日的阳光（有战斗锦衣）。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0xFFED8421
            wds.大模型资源 = 0xFFED8422
        elseif wd == "萌萌小厨" then
            wds.说明 = "萌萌小厨巧手巧作千人食，五味调料和百味香（有战斗锦衣）。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WE.dll"
            wds.小模型资源 = 0x00000079
            wds.大模型资源 = 0x00000090
        elseif wd == "尖叫服" then
            wds.说明 = "吸血鬼的造型 。（黑色小猪）      "
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WE.dll"
            wds.小模型资源 = 0x00000038
            wds.大模型资源 = 0x00000037
        elseif wd == "锦绣幻梦" then
            wds.说明 = "春宵夜沉，一尚服局女官于梦中魂游，至不知何处仙境。（深海狂鲨）"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WE.dll"
            wds.小模型资源 = 0x00000039
            wds.大模型资源 = 0x00000040
        elseif wd == "萤雪轻裘" then
            wds.说明 = "萤雪微霜寒，早春轻裘暖。细雨纷飞，正是春寒料峭时节，着萤雪轻裘在身，如暖阳。（蝠翼冥骑）"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "WE.dll"
            wds.小模型资源 = 0x00000091
            wds.大模型资源 = 0x00000092
        elseif wd == "点化石" then
            wds.说明 = "吸附石在成功吸取召唤兽身上的特色能力后转变而成的打造材料,与召唤兽装备合成后即可成为套装之一。（找宝石商人镶嵌）"
            wds.类型 = "消耗道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xFBFCFCF2
            wds.大模型资源 = 0x1FB5F6AD
        elseif wd == "吸附石" then
            wds.说明 = "精铁铸造的装备中分解而出的特殊材料，与召唤兽进行炼妖会有一定几率获取召唤兽身上的特殊能力。"
            wds.类型 = "炼妖"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x4C07009E
            wds.大模型资源 = 0xBF944057
            wds.叠加 =true
        elseif wd == "齐天大圣" then
            wds.说明 = "金甲玄绫慑九霄，降妖除魔显神通。穿上齐天大圣锦衣，心有热血，我便齐天！"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x01100007
            wds.大模型资源 = 0x01100001
        elseif wd == "浪淘纱" then
            wds.说明 = "夏日绮梦，雪浪淘纱。情深似海翻澜起，心悦君兮浪淘纱。织女素手织就浪淘纱，大浪淘纱，海枯石烂，此情不泯。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x3E968F51
            wds.大模型资源 = 0x3E968F50
        elseif wd == "冰灵蝶翼·月笼" then
            wds.说明 = "穿花蛱蝶深深见，点水蜻蜓款款飞。蝶翼做衣，比翼双飞。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x1824AF19
            wds.大模型资源 = 0x1824AF18
        elseif wd == "铃儿叮当" then
            wds.说明 = "蒸糕包、蒸糕包，蒸个大包，穿戴后会有祥瑞效果哦。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x00000077
            wds.大模型资源 = 0x00000078
        elseif wd == "顽皮小恶魔" then
            wds.说明 = "顽皮小恶魔,有多顽皮你穿上不就知道咯？，穿戴后会有祥瑞效果哦。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x00000079
            wds.大模型资源 = 0x00000080
        elseif wd == "蜜蜂嗡嗡" then
            wds.说明 = "小蜜蜂嗡嗡，我们一起飞到花丛中去吧？，穿戴后会有祥瑞效果哦。"
            wds.类型 = "锦衣"
            wds.属性 = "气血"
            wds.部位 = "衣服"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x00000081
            wds.大模型资源 = 0x00000082
        elseif wd == "修篁斧" then
            wds.说明 = "仙界的五行秘宝之一，为普陀山砍伐紫竹之物。"
            wds.类型 = "飞升道具"
            wds.类别 = 1
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x25F0FF04
            wds.大模型资源 = 0x34EF726D
                    elseif wd == "小铲子" then
            wds.说明 = "挖宝。"
            wds.类型 = "挖宝带锯"
            wds.类别 = 1
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x25F0FF04
            wds.大模型资源 = 0x34EF726D
        elseif wd == "炼金鼎" then
            wds.说明 = "仙界的五行秘宝之一，为五庄观修炼金丹之物。 "
            wds.类型 = "飞升道具"
            wds.类别 = 1
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6BEC70D0
            wds.大模型资源 = 0x45968E54
        elseif wd == "定海针" then
            wds.说明 = "仙界的五行秘宝之一，为龙宫的镇海之宝。"
            wds.类型 = "飞升道具"
            wds.类别 = 1
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xF524FB0F
            wds.大模型资源 = 0xD75D34DE
 ----------------------消耗道具
            elseif wd == "蟠桃" then
                wds.说明 = "来自于蟠桃园的仙桃，使用后可增加当前参战召唤兽0.001点成长。"
                wds.类型 = "儿童用品"
                wds.类别 = 4
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x58849704
                wds.大模型资源 = 0x140D0428
                  elseif wd == "孤儿名册" then
                wds.说明 = "依照天神所授意的神奇符石，利用人物经验激活后可以镶嵌到开过运的装备上,按照一定顺序,将正确的符石放入装备的位置中,可被天神附身获得神奇的效果。激活后为专用,无法转移给他人。"
                wds.资源 = "ZY.FT"
                wds.类型 = "消耗道具"
                wds.小模型资源 = 0x6F2F8C9D
                wds.大模型资源 = 0x110859B6
            elseif wd == "一级未激活符石" then
                wds.说明 = "依照天神所授意的神奇符石，利用人物经验激活后可以镶嵌到开过运的装备上,按照一定顺序,将正确的符石放入装备的位置中,可被天神附身获得神奇的效果。激活后为专用,无法转移给他人。"
                wds.资源 = "ZY.FT"
                wds.类型 = "消耗道具"
                wds.等级 = 1
                wds.小模型资源 = 0x580E0DE1
                wds.大模型资源 = 0xC45E19C4
             elseif wd == "二级未激活符石" then
                wds.说明 = "依照天神所授意的神奇符石，利用人物经验激活后可以镶嵌到开过运的装备上,按照一定顺序,将正确的符石放入装备的位置中,可被天神附身获得神奇的效果。激活后为专用,无法转移给他人。"
                wds.资源 = "WP.FT"
                wds.类型 = "消耗道具"
                wds.等级 = 2
                wds.小模型资源 = 0x58DEAA8F
                wds.大模型资源 = 0xCC937790
             elseif wd == "三级未激活符石" then
                wds.说明 = "依照天神所授意的神奇符石，利用人物经验激活后可以镶嵌到开过运的装备上,按照一定顺序,将正确的符石放入装备的位置中,可被天神附身获得神奇的效果。激活后为专用,无法转移给他人。"
                wds.资源 = "WP.FT"
                wds.类型 = "消耗道具"
                wds.等级 = 3
                wds.小模型资源 = 0xB39A2CAB
                wds.大模型资源 = 0x2675DF2C
            elseif wd == "新手玩家礼包" then
                wds.说明 = "新手扶持礼包，等级大于50级才能使用，每隔10级可以开一次。"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
            elseif wd == "120级玩家礼包" then
                wds.说明 = "120级礼包，能开根据自身角色的120级无级别套装，满属性"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
             elseif wd == "150级玩家礼包" then
                wds.说明 = "150级礼包，能开根据自身角色的150级无级别套装，满属性"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
             elseif wd == "160级玩家礼包" then
                wds.说明 = "150级礼包，能开根据自身角色的160级无级别套装，满属性"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
            elseif wd == "银币礼包" then
                wds.说明 = "使用这个礼包可以获得2E银币，从此富甲天下"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
                  elseif wd == "三十礼包" then
                wds.说明 = "使用这个礼包可以获得30元首充礼包"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
                  elseif wd == "八十八礼包" then
                wds.说明 = "使用这个礼包可以获得88元首充礼包"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
                  elseif wd == "一八八礼包" then
                wds.说明 = "使用这个礼包可以获得188元首充礼包"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
                elseif wd == "星辉石礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级星辉石"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "太阳石礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级太阳石。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "月亮石礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级月亮石。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "舍利子礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级舍利子。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "黑宝石礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级黑宝石。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "光芒石礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级光芒石。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "红玛瑙礼包" then
                wds.说明 = "使用这个礼包可以获得1-10级红玛瑙。"
                wds.类型 = "消耗道具"
                wds.资源 = "common/item.wdf"
                wds.小模型资源 = 0x317EDC7E
                wds.大模型资源 = 0x1AFF14C4
                elseif wd == "图册礼包" then
                wds.说明 = "使用这个礼包可以获得多种上古锻造图册"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
            elseif wd == "储备礼包" then
                wds.说明 = "使用这个礼包可以获得2E储备金，从此富甲天下"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA3DB661B
                wds.大模型资源 = 0x5AAF94C0
            elseif wd == "修炼果" then
                wds.说明 = "具有灵气的果子，为修炼之士所常用"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x19CEE771
                wds.大模型资源 = 0x0F2E3E3F
                wds.价格 = 500000
                wds.叠加 =true
            elseif wd == "双倍经验丹" then
                wds.说明 = "使用后能获得2小时双倍时间"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xD6684CA7
                wds.大模型资源 = 0x9D8591D8
                wds.价格 = 500000
                wds.叠加 =true
            elseif wd == "月华露" then
                wds.说明 = "昆仑山顶采集的皓月光华，是增加修为的仙露。由于月有阴晴圆缺，所以不同时间采集的光华效果也不尽相同。"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x016F4E03
                wds.大模型资源 = 0x026E613B
                wds.价格 = 500000
                wds.叠加 =true
             elseif wd == "天眼通符" then
                wds.说明 = "天界的灵符，具有洞察世间万物的神奇作用。"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xDF37C485
                wds.大模型资源 = 0x7FA4203F
                wds.叠加 =true
                wds.价格=500
            elseif wd == "九转金丹" then
                wds.说明 = "太上老君八卦炉中炼制的灵丹，服用之后可以增加人物的修炼经验。不通火候炼制的丹药，效果自然也不同。"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x5A0E9307
                wds.大模型资源 = 0x6EB440C8
                wds.价格 = 300000
            elseif wd == "海马" then
                wds.说明 = "生活在热带及温带深海，头部弯曲，形状与马头相似，使用后可以增加体力"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xACEE1AE4
                wds.大模型资源 = 0x98CA1487
                wds.价格 = 500000
            elseif wd == "清灵净瓶" then
                wds.说明 = "自上古流传至今的神奇净瓶，注以灵力之后可以汇聚清灵仙露"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xBB62F78D
                wds.大模型资源 = 0x455BD7CF
                wds.价格 = 400000
                wds.叠加 = true
            elseif wd == "飞行符" then
                wds.说明 = "一种神奇的符咒，使用后可以穿梭于各大陆之间"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 3496860737
                wds.大模型资源 = 2592521580
                wds.叠加 = true
                wds.价格 = 500-----------------------------------------
            elseif wd == "摇钱树树苗" then
                wds.说明 = "摇钱树的树苗，在特定地方种下，可能有意想不到的收获。"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x2D1399E3
                wds.大模型资源 = 0x82254575
                wds.叠加 = true
                wds.价格 = 500-----------------------------------------
            elseif wd == "洞冥草" then
                wds.说明 = "山洞中生长的一种阴暗的小草，可以解除摄妖香效果"
                wds.类型 = "消耗道具"
                wds.叠加 = true
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 3286276407
                wds.大模型资源 = 3672031387
                wds.价格 = 500---------------------
            elseif wd == "摄妖香" then
                wds.说明 = "人间流行的一种驱逐妖魔的异香，时效为三个时辰"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 3553509277
                wds.大模型资源 = 4033100487
                wds.叠加 = true
                wds.价格 =1000---------------------
             elseif wd == "藏宝图" then
                wds.说明 = "一张记着藏宝地点的地图"
                wds.类型 = "消耗道具"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 211127435
                wds.大模型资源 = 1954996989-----------------
            elseif wd == "高级藏宝图" then
                 wds.说明 = "一张记着藏宝地点的地图"
                  wds.类型 = "消耗道具"
                 wds.资源 = "ZY.FT"
                 wds.小模型资源 = 0x0C958C8B
                 wds.大模型资源 = 0x7486E2FD
            elseif wd == "如意棒" then
                 wds.说明 = "使用后可以让迷宫内的玩家进入战斗，请注意，使用后也可能会让自己进入战斗。使用后积分增加3点，所有进入战斗的玩家会获得1点积分。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0xF3BD3928
                 wds.大模型资源 = 0x29850387
            elseif wd == "如意棒1" then
                 wds.说明 = "使用后可以让迷宫内的玩家进入战斗，请注意，使用后也可能会让自己进入战斗。使用后积分增加3点，所有进入战斗的玩家会获得1点积分。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0xA545ABC8
                 wds.大模型资源 = 0xD05BA36C
            elseif wd == "圣兽丹" then
                 wds.说明 = "与特定召唤兽炼妖合成，讲丹内上古圣兽之态与召唤兽融合，合成出该召唤兽的饰品，饰品能增加新的形态装饰效果。【说明】炼妖过程中使用的召唤兽会被消耗。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0x8029B90A
                 wds.大模型资源 = 0x80237D0F
            elseif wd == "玄天残卷·上卷" then
                 wds.说明 = "上古神力编撰而成的玄天卷法了凑齐上中下三卷可以右键兑换一颗圣兽丹。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0xC3AF882B
                 wds.大模型资源 = 0xCFB770DD
            elseif wd == "玄天残卷·中卷" then
                 wds.说明 = "上古神力编撰而成的玄天卷法了凑齐上中下三卷可以右键兑换一颗圣兽丹。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0x559D05B9
                 wds.大模型资源 = 0x95C4E6EC
            elseif wd == "玄天残卷·下卷" then
                 wds.说明 = "上古神力编撰而成的玄天卷法了凑齐上中下三卷可以右键兑换一颗圣兽丹。"
                  wds.类型 = "消耗道具"
                 wds.资源 = "WP.FT"
                 wds.小模型资源 = 0xF91379E1
                 wds.大模型资源 = 0xEC1B5658
 ------------------------------召唤兽用品
            elseif wd == "坐骑灵丹" then
                wds.说明 = "坐骑的道具，能够唤醒坐骑成长,使坐骑的成长得到提升"
                wds.类型 = "消耗道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x2A59F2A1
                wds.大模型资源 = 0x77F1A658
                wds.价格 = 50000
                wds.叠加 =true
            elseif wd == "宠物口粮" then
                wds.说明 = "宠物非常喜欢的食物，可以恢复宠物耐力。"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xB864511E
                wds.大模型资源 = 0x33BBE002
                wds.价格 = 50000
                wds.叠加 =true
            elseif wd == "神兜兜" then
                wds.说明 = "三界之内的神兽们最喜欢的玩具，积攒99个，神兽就会跟你回家哦。"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x50808141
                wds.大模型资源 = 0x65756770
                wds.价格 = 50000
                wds.叠加 =true
            elseif wd == "高级宠物口粮" then
                wds.说明 = "宠物非常喜欢的食物，可以大量恢复宠物耐力。"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x73080A8C
                wds.大模型资源 = 0xDBB55CC4
                wds.价格 = 50000
                wds.叠加 =true
            elseif wd == "玉葫灵髓" then
                wds.说明 = "《博物录》之中记载的玄妙丹药，可以将召唤兽已使用的清灵仙露的状态回归原始。"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x3FE08883
                wds.大模型资源 = 0x05104141
                wds.价格 = 5000000
                wds.叠加 =true
             elseif wd == "易经丹" then
                wds.说明 = "《异兽录》之中记载的奇妙药丸，使用得当可助召唤兽打通筋骨达到形易经之境。"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xD9BBCB07
                wds.大模型资源 = 0x8C34FD39
                wds.价格 = 1500000
                wds.叠加 =true------------------------
            elseif wd == "高级清灵仙露" then
                wds.说明 = "由清灵净瓶所汇聚的神奇仙露，可以用于提升召唤兽的灵性。（只对50≤灵性≤100的召唤兽使用）"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xE0D95C88
                wds.大模型资源 = 0x253B7A83
            elseif wd == "中级清灵仙露" then
                wds.说明 = "由清灵净瓶所汇聚的神奇仙露，可以用于提升召唤兽的灵性。（只对50≤灵性≤100的召唤兽使用）"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xFD5F44C0
                wds.大模型资源 = 0xC8F9A4D3
            elseif wd == "初级清灵仙露" then
                wds.说明 ="由清灵净瓶所汇聚的神奇仙露，可以用于提升召唤兽的灵性。（只对50≤灵性≤100的召唤兽使用）"
                wds.类型 = "召唤兽道具"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x6F4A548E
                wds.大模型资源 = 0x1287E12D
 -----------------------导标旗
            elseif wd == "白色导标旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "导标旗"
                wds.价格 = 7000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 2217728758
                wds.大模型资源 = 1751361785
            elseif wd == "黄色导标旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "导标旗"
                wds.价格 = 7000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x746BEC9C
                wds.大模型资源 = 1011605557
            elseif wd == "红色导标旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "导标旗"
                wds.价格 = 7000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 2856215948
                wds.大模型资源 = 3379177744
            elseif wd == "绿色导标旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "导标旗"
                wds.价格 = 7000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 1060779522
                wds.大模型资源 = 662138631
            elseif wd == "蓝色导标旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "导标旗"
                wds.价格 = 7000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 53543608
                wds.大模型资源 = 340145338
            elseif wd == "蓝色合成旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "合成旗"
                wds.价格 = 60000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x78C6828D
                wds.大模型资源 = 0x4D27A80E
            elseif wd == "绿色合成旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "合成旗"
                wds.价格 = 60000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xC87EF093
                wds.大模型资源 = 0x498354A0
            elseif wd == "黄色合成旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "合成旗"
                wds.价格 = 60000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x409FA7D1
                wds.大模型资源 = 0x3D64E066
            elseif wd == "白色合成旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "合成旗"
                wds.价格 = 60000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x306FE9B9
                wds.大模型资源 = 0xB06EE4C6
            elseif wd == "红色合成旗" then
                wds.说明 = "集导标旗之大成，江湖遁形所用，奇人异士必备之物。"
                wds.类型 = "合成旗"
                wds.价格 = 60000
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x0DE87173
                wds.大模型资源 = 0x4F8723DA
 ------------------钓鱼
            elseif wd == "鱼竿" then
                wds.说明 = "用于垂钓的工具"
                wds.类型 = "钓鱼"
                wds.类别 = 4
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x9667DBC6
                wds.大模型资源 = 0x7331E121
            elseif wd == "海星" then
                wds.说明 = "生活在深海，因身型呈五角星状而得名"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x131DE253
                wds.大模型资源 = 0x4B11034E
            elseif wd == "小黄鱼" then
                wds.说明 = "也称黄花鱼，喜欢在近海底层结群活动，是常见的海产鱼类"
                wds.类型 = "钓鱼"
                wds.类别 = 2
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xBC1A72C9
                wds.大模型资源 = 0x739C6D76
            elseif wd == "大黄鱼" then
                wds.说明 = "常见于近海深水区域，身型修长"
                wds.类型 = "钓鱼"
                wds.类别 = 2
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x8C1E2281
                wds.大模型资源 = 0xAD9A1C65
            elseif wd == "大闸蟹" then
                wds.说明 = "体型较大的淡水蟹，常见于深水湖泊之中"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xA2245FD3
                wds.大模型资源 = 0xB11DA9DF
            elseif wd == "鲫鱼" then
                wds.说明 = "体态雍容,有鱼中君子之称"
                wds.类型 = "钓鱼"
                wds.类别 = 2
                wds[4] = 6
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x176448AE
                wds.大模型资源 = 0xDDCD0D58
            elseif wd == "泥鳅" then
                wds.说明 = "常见淡水鱼类，肉质细嫩，味道鲜美"
                wds.类型 = "钓鱼"
                wds.类别 = 1
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x53BA9EAB
                wds.大模型资源 = 0xAEBD20FC
            elseif wd == "对虾" then
                wds.说明 = "常栖于沿岸浅海，肉质鲜嫩，营养丰富"
                wds.类型 = "钓鱼"
                wds.类别 = 1
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x661007AA
                wds.大模型资源 = 0x832E423E
            elseif wd == "草鱼" then
                wds.说明 = "淡水鱼类，因喜食水草而得名"
                wds.类型 = "钓鱼"
                wds.类别 = 2
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xEB87ED4C
                wds.大模型资源 = 0x709318D2
            elseif wd == "沙丁鱼" then
                wds.说明 = "常见海洋鱼类，身型小巧，行动敏捷，喜好群居于暖水海区"
                wds.类型 = "钓鱼"
                wds.类别 = 1
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x48B1CD34
                wds.大模型资源 = 0xC23E4BBE
            elseif wd == "河蟹" then
                wds.说明 = "体型较大的淡水蟹，常见于深水湖泊之中"
                wds.类型 = "钓鱼"
                wds.类别 = 2
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x4D678B2E
                wds.大模型资源 = 0xB54EA7CC
            elseif wd == "娃娃鱼" then
                wds.说明 = "生活在山区的溪流湖泊中，形象可爱"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds[4] = 6
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xABEB5CF1
                wds.大模型资源 = 0x56BFCF4D
            elseif wd == "河豚" then
                wds.说明 = "肉食鱼类，主要捕食小鱼小虾和贝类，部分内脏有毒，但肉味非常鲜美，是难得的佳肴"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x8AC31DBD
                wds.大模型资源 = 0x81169897
            elseif wd == "甲鱼" then
                wds.说明 = "两栖爬行动物，有坚硬厚实的甲壳，行动缓慢但寿命比较长"
                wds.类型 = "钓鱼"
                wds.类别 = 3

                wds.资源 = "item.wdf"
                wds.小模型资源 = 174833964--------------
                wds.大模型资源 = 0xE446A3C7-----小没有
            elseif wd == "鲤鱼" then
                wds.说明 = "淡水鱼类，肉质坚实，味道鲜美"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds.资源 = "item.wdf"
                wds.小模型资源 = 3865632193
                wds.大模型资源 = 2442761904-----------------------
            elseif wd == "金枪鱼" then
                wds.说明 = "温热带海洋鱼类，身型细长，游速非常快"
                wds.类型 = "钓鱼"
                wds.类别 = 3
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x4226E620
                wds.大模型资源 = 0xE3D265E9
            elseif wd == "河虾" then
                wds.说明 = "生长在河水中的小虾"
                wds.类型 = "钓鱼"
                wds.类别 = 1
                wds.资源 = "item.wdf"
                wds.小模型资源 = 0x6531F84E
                wds.大模型资源 = 1542469648---------大没有
            elseif wd == "毛蟹" then
                wds.说明 = "常见于近海深水区域，身小壳薄，肉质细嫩而有香味"
                wds.类型 = "钓鱼"
                wds.类别 = 2

                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x23E3771B
                wds.大模型资源 = 0xD3F240C9
 -----------------打造
            elseif wd == "钨金" then
                wds.说明 = "一种十分稀有的矿产，其坚硬的性能受广大铸造师的喜爱，是熔炼装备的主要配料。（找宝石商人镶嵌）"
                wds.类型 = "功能"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x18EAE6A5
                wds.大模型资源 = 0x248E4364-----------------
            elseif wd == "附魔宝珠" then
                wds.说明 = "蕴含天地精华的宝珠，可为一件人物装备随机附加一种特殊效果。（找宝石商人镶嵌）"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x002A5EAF
                wds.大模型资源 = 0x575EE64D-----------------
            elseif wd == "炼妖石" then
                wds.说明 = "从仙界散落到人间的“灵”石，拥有强烈的吸附能量的能力。"
                wds.类型 = "打造"
                wds.类别 = "召唤兽"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 94724302
                wds.大模型资源 = 164529735-----------------
            elseif wd == "制造指南书" then
                wds.说明 = "记载着各种装备武器制造方法的书卷。"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 1102456535
                wds.大模型资源 = 3855535092-------------
            elseif wd == "百炼精铁" then
                wds.说明 = "精铁百炼而成，这种材料为神兵而存在。是用来合成装备的神奇矿石"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 3971456754
                wds.大模型资源 = 2585045399---------------
            elseif wd == "上古锻造图策" then
                wds.说明 = "古代的典籍，详细记录了各种护具的锻造方法。"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x9F6993FA
                wds.大模型资源 = 0xF2379F81
            elseif wd == "九眼天珠" then
                wds.说明 = "完全吸收了生物的潜能，在经过特别的处理后可将此潜能转换到召唤兽装备上"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x50E14899
                wds.大模型资源 = 0x8AABE686
             elseif wd == "三眼天珠" then
                wds.说明 = "吸收了较多的生物潜能，再通过特别的方式将此潜能转化到召唤兽装备上。"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x03C1E325
                wds.大模型资源 = 0x4E9B7820
              elseif wd == "天眼珠" then
                wds.说明 = "吸收了一定的生物潜能，再通过特别的方式将此潜能转化到召唤兽装备上。"
                wds.类型 = "打造"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xD29E5ECE
                wds.大模型资源 = 0x859C0204
            elseif wd == "元灵晶石" then
                wds.说明 = "灵山晶石经千锤百炼而成，为灵饰淬灵所必须材料。"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x321116AF
                wds.大模型资源 = 0x13A717B5
            elseif wd == "灵饰指南书" then
                wds.说明 = "记载各种灵饰制造方法的古老书卷。"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xAEB5E00E
                wds.大模型资源 = 0xD7E29586
            elseif wd == "珍珠" then
                wds.说明 = "光彩夺目的珍珠，可镶嵌在武器装备上，恢复装备一定的耐久度，每件装备镶嵌珍珠的数量没有限制，但装备等级高于珍珠等级时无法镶嵌。可在宝石商人处进行镶嵌。"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x611926C4
                wds.大模型资源 = 0x58C5D6AC
            elseif wd == "巨剑·元身" then
                wds.说明 = "等待被唤醒的洪荒巨剑，与战魄一起锻造为[长息]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0xD5E5ADD3
                wds.大模型资源 = 0x43CC0E3B
            elseif wd == "伞·元身" then
                wds.说明 = "等待被唤醒的风云之伞，与战魄一起锻造成为[晴雪]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0xCB5C6EFD
                wds.大模型资源 = 0x47EA4C41
            elseif wd == "灯笼·元身" then
                wds.说明 = "等待被唤醒的河汉之灯，与战魄一起锻造为[荒尘]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0x7AE2641F
                wds.大模型资源 = 0x4DCB3823
            elseif wd == "长杖·元身" then
                wds.说明 = "等待被唤醒的天地之杖，与战魄一起锻造为[弦月]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0x3221FB78
                wds.大模型资源 = 0x7023E521
            elseif wd == "宝珠·元身" then
                wds.说明 = "等待被唤醒的盘古之珠，与战魄一起锻造为[赤明]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0xAED3678B
                wds.大模型资源 = 0x9381644E
            elseif wd == "弓·元身" then
                wds.说明 = "等待被唤醒的射日之弓，与战魄一起锻造为[若木]。"--
                wds.类型 = "打造"
                wds.资源 = "JM.FT"
                wds.小模型资源 = 0xCE170DFA
                wds.大模型资源 = 0x157E1425
            elseif wd == "扇·元身" then
                wds.说明 = "等待被唤醒的玄冥之扇，与战魄一起锻造为[星瀚]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x6DF2FD31
                wds.大模型资源 = 0x4370EED8
            elseif wd == "刀·元身" then
                wds.说明 = "等待被唤醒的复仇之刀，与战魄一起锻造为[鸣鸿]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x42313640
                wds.大模型资源 = 0xE8E78B92
            elseif wd == "剑·元身" then
                wds.说明 = "等待被唤醒的宿敌之剑，与战魄一起锻造为[擒龙]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xAD7B3E5B
                wds.大模型资源 = 0xE0BB9E80
            elseif wd == "斧·元身" then
                wds.说明 = "等待被唤醒的魔君之斧，与战魄一起锻造为[裂天]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xFBEC3D5B
                wds.大模型资源 = 0xE5709BCF
            elseif wd == "锤·元身" then
                wds.说明 = "等待被唤醒的盘古龙齿，与战魄一起锻造为[碎寂]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x397D0A2C
                wds.大模型资源 = 0x4A0E1D74
            elseif wd == "枪·元身" then
                wds.说明 = "等待被唤醒的王者之枪，与战魄一起锻造为[弑皇]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xAA64F92D
                wds.大模型资源 = 0x4A0E1D74
            elseif wd == "飘带·元身" then
                wds.说明 = "等待被唤醒的九霄澄云，与战魄一起锻造为[九霄]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x4F4AD0CD
                wds.大模型资源 = 0xDF316EF7
            elseif wd == "魔棒·元身" then
                wds.说明 ="等待被唤醒的混沌异果，与战魄一起锻造为[醍醐]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x8D1F0679
                wds.大模型资源 = 0xAB525F51
            elseif wd == "双环·元身" then
                wds.说明 ="等待被唤醒的朝夕霞雾，与战魄一起锻造为[朝夕]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x111D93EB
                wds.大模型资源 = 0x7D9D4D19
            elseif wd == "双剑·元身" then
                wds.说明 = "等待被唤醒的双飞利刃，与战魄一起锻造为[浮犀]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xEB9EDC89
                wds.大模型资源 = 0x5EDAC2DE
            elseif wd == "长鞭·元身" then
                wds.说明 = "等待被唤醒的九曲月华，与战魄一起锻造为[霜陨]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x94B20072
                wds.大模型资源 = 0x1C30212D
            elseif wd == "爪刺·元身" then
                wds.说明 = "等待被唤醒的天地离愁，与战魄一起锻造为[离钩]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x0F776C4A
                wds.大模型资源 = 0x9CE1A36C
            elseif wd == "头盔·元身" then
                wds.说明 = "等待被唤醒的浑天玄火，与战魄一起锻造为[浑天玄火盔]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x764A4001
                wds.大模型资源 = 0x80644D9A
            elseif wd == "冠冕·元身" then
                wds.说明 = "等待被唤醒的乾元鸣凤，与战魄一起锻造为[乾元鸣凤冕]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xCD93E6F7
                wds.大模型资源 = 0xD97AD016
            elseif wd == "坚甲·元身" then
                wds.说明 ="等待被唤醒的混元一气，与战魄一起锻造为[混元一气甲]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xD08B5887
                wds.大模型资源 = 0x5FC5E447
            elseif wd == "纱衣·元身" then
                wds.说明 = "等待被唤醒的鎏金浣月，与战魄一起锻造为[鎏金浣月衣]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x99EDA596
                wds.大模型资源 = 0x4A94E5EC
            elseif wd == "挂坠·元身" then
                wds.说明 = "等得被唤醒的落霞陨星，与战魄一起锻造为[落霞陨星坠]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x2365F2C0
                wds.大模型资源 = 0xC31B0927
            elseif wd == "束带·元身" then
                wds.说明 = "等待被唤醒的紫霄云芒，与战魄一起锻造为[紫霄云芒带]。"--
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x83D8E526
                wds.大模型资源 = 0xDA79287A
            elseif wd == "鞋履·元身" then
                wds.说明 = "等待被唤醒的红尘光阴，与战魄一起锻造为[辟尘分光履]。"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x2D4D76E4
                wds.大模型资源 = 0x2E7EEDEF
            elseif wd == "战魄" then
                wds.说明 = "十二神兵精魄所凝的玄彩战魄，不屈的杀意仍在其间咆哮呐喊，他们誓要铸躯重生，再续与魔气这万年未竟之战，与元身一同锻造则可制作160级装备。"
                wds.类型 = "打造"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xF87C4047
                wds.大模型资源 = 0x54382E1C
 ------------杂货
        elseif wd =="符石卷轴"  then
            wds.小模型资源 = 0xF9DD83C6
            wds.大模型资源 = 0xDF3E0DDE
            wds.类型 = "杂货"
            wds.资源 ="WP.FT"
            wds.说明 ="一张记载高级符石合成方式的卷轴"
        elseif wd =="金银宝盒"  then
            wds.小模型资源 = 0xA8BB247D
            wds.大模型资源 = 0xF1AB952C
            wds.类型 = "杂货"
            wds.资源 ="WP.FT"
            wds.说明 ="上交给帮派白虎堂总管后可以增加50000的帮派资金，同时还可以获得5点帮贡。"
        elseif wd =="小雪块"  then
            wds.小模型资源 = 0x00000033
            wds.大模型资源 = 0x00000031
            wds.类型 = "杂货"
            wds.资源 ="WP.FT"
            wds.说明 ="直接使用可随机获得积分、储备、兽诀等物品。"
        elseif wd =="大雪块"  then
            wds.小模型资源 = 0x00000034
            wds.大模型资源 = 0x00000032
            wds.类型 = "杂货"
            wds.资源 ="WP.FT"
            wds.说明 ="直接使用可随机获得积分、银子、兽诀等物品。"

 --------------------??

        elseif wd == "分解符" then
            wds.说明 = "用于分解装备"
            wds.类型 = "宝石"
            wds.类别 = 3
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xEBA131CF
            wds.大模型资源 = 0xBE268B81
        elseif wd == "心魔宝珠" then
            wds.说明 = "汲取野妖族精华而成，蕴含着无穷魔力的宝珠"
            wds.类型 = "心魔宝珠"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x44C5892B
            wds.大模型资源 = 0x507C3D83
 --------------------------------刀-------------- 武器
        elseif wd == "柳叶刀" then
            wds.说明 = "形如柳叶的细弯刀"
            wds.类型 = "武器"
            wds.等级 = 0
            wds.价格 = 500
            wds.类别 = "刀"
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1159059801
            wds.大模型资源 = 388221160
        elseif wd == "苗刀" then
            wds.说明 = "苗疆常用的兵器，极细极尖锐"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.等级 = 10
            wds.价格 = 1000
            wds.价格 = 1000
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 183813784
            wds.大模型资源 = 3883026774------------
        elseif wd == "夜魔弯刀" then
            wds.说明 = "精钢制成的细弯刀，刀背密布细齿，如夜魔之牙。"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2386679785
            wds.大模型资源 = 2763619781---------
        elseif wd == "金背大砍刀" then
            wds.说明 = "刀身挺直，以黄金镶背，刀道沉猛"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.价格 = 9000
            wds.小模型资源 = 1741978907
            wds.大模型资源 = 3104242640--------
        elseif wd == "雁翅刀" then
            wds.说明 = "刀背布满粗大的锯齿，上挂银环，铿锵入耳如雁过九天"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.价格 = 14000
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1719719282
            wds.大模型资源 = 3277123413------------
        elseif wd == "破天宝刀" then
            wds.说明 = "刀光八面，劈风破天"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.等级 = 50
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1201880064
            wds.大模型资源 = 3191505206-----------
        elseif wd == "狼牙刀" then
            wds.说明 = "精钢打造，刀背密布狼牙锯齿"
            wds.类型 = "武器"
            wds.类别 = "刀"
            wds.等级 = 60
            wds.角色限制 = "剑侠客,巨魔王"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2655796221
            wds.大模型资源 = 295875160------------
        elseif wd == "龙鳞宝刀" then
           wds.说明 = "百练寒钢打造，以龙鳞装饰，身份的象征"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 70
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 668589754
           wds.大模型资源 = 2944368377------------
        elseif wd == "黑炎魔刀" then
           wds.说明 = "刀身透出森森鬼气，有着摄人心魄的魔力"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 80
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 162972978
           wds.大模型资源 = 279098730-----------
        elseif wd == "冷月" then
           wds.说明 = "夜清冷，月露眉尖，笑引幽冥路"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 90
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 603925505
           wds.大模型资源 = 407135953---------------
        elseif wd == "屠龙" then
           wds.说明 = "上古神兵，宝刀屠龙"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 =  100
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1961749850
           wds.大模型资源 = 155455945---------------
        elseif wd == "血刃" then
           wds.说明 = "血红的刃，血红的光，以血洗天地"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 =  100
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1138002761
           wds.大模型资源 = 3952677662---------------
        elseif wd == "偃月青龙" then
           wds.说明 = "关公之配刀，青龙化刀柄，刀身如半月，百万军中取上将首级如探囊取物。关公死，刀柄不知去向，唯余此刀身。"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 120
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 241494943
           wds.大模型资源 = 2525360226-----------
        elseif wd == "晓风残月" then
           wds.说明 = "迅如晓风，刃如残月。此非中土大唐之物，乃海外大食国宝刀，其上蛩伏可实现愿望之神明。但如何召唤已不可知。"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 130
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 293054687
           wds.大模型资源 = 3069402462-----------
        elseif wd == "斩妖泣血" then
           wds.说明 = "大唐名匠半生心血所成的宝刀，因其妻子被妖魔所害，故将满腔愤恨倾注于刀中。谁能为他复仇，便以此刀相赠。"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 140
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2680057538
           wds.大模型资源 = 1425981787-----------
        elseif wd == "业火三灾" then
           wds.说明 = "周身暗红如浴血池，刃裂三线引为三灾，中有赤瞳勾人心火。敌心神既失，十方俱灭，惟余红莲：“汝一念起，业火炽然，非人燔汝，乃汝自燔。”"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 150
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2013357951
           wds.大模型资源 = 2366906872------------
        elseif wd == "鸣鸿" then
           wds.说明 = "刀长三尺，与轩辕剑同炉而成。黄帝见其刀意过盛，恐反噬刀主，欲以轩辕剑毁之；此刀乃化玄红云鹊，穿霞而逝。"
           wds.类型 = "武器"
           wds.类别 = "刀"
           wds.等级 = 160
           wds.角色限制 = "剑侠客,巨魔王"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3497244314
           wds.大模型资源 = 3024786057---------------
 --------------------------------剑
        elseif wd == "青铜短剑" then
            wds.说明 = "青铜制成，上铸有简单的图案纹饰，初学者练习常用"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1261240304
            wds.大模型资源 = 4201657972--------
        elseif wd == "铁齿剑" then
            wds.说明 = "铁制的短剑，边缘带齿"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.价格 = 1000
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3620311050
            wds.大模型资源 = 2592140397----------
        elseif wd == "吴越剑" then
            wds.说明 = "吴越之地产名剑，极其锐利"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2309179882
            wds.大模型资源 = 118455348----------
        elseif wd == "青锋剑" then
            wds.说明 = "剑长三尺，用起来方便灵活，是常用的兵器"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.价格 = 9000
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3995192813
            wds.大模型资源 = 1523776943----------
        elseif wd == "龙泉剑" then
            wds.说明 = "精铁铸造，又以龙泉之水淬制，上刻青龙，其利无比。"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.价格 = 14000
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3230696706
            wds.大模型资源 = 3094355456----------
        elseif wd == "黄金剑" then
            wds.说明 = "真金铸脊，精钢淬锋，其光灿烂夺人眼，魔障远避不及；沉于渭水，可镇河魔。"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.等级 = 50
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2052074173
            wds.大模型资源 = 1174324866----------
        elseif wd == "游龙剑" then
            wds.说明 = "剑体九曲如龙形，覆鳞片，手弹其锋刃，鸣如龙吟。"
            wds.类型 = "武器"
            wds.类别 = "剑"
            wds.等级 = 60
            wds.角色限制 = "逍遥生,剑侠客"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3272689235
            wds.大模型资源 = 2801949288----------
        elseif wd == "北斗七星剑" then
           wds.说明 = "精炼白金之铁铸造，有北斗七星之纹饰"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 70
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1102206433
           wds.大模型资源 = 2931293335-------
        elseif wd == "碧玉剑" then
           wds.说明 = "以寒冰碧玉制成,剑身如竹节,刚硬无比,难得的奇珍之物"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 80
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3706066988
           wds.大模型资源 = 2167893769----------
        elseif wd == "鱼肠" then
           wds.说明 = "上古名剑，细短柔软，可入鱼腹"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 90
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 703624793
           wds.大模型资源 = 3087768505------------
        elseif wd == "倚天" then
           wds.说明 = "上古名剑，雾中山神铁所制，剑光如电，切金如泥"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 =  100
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1234219918
           wds.大模型资源 = 1100401070------------
        elseif wd == "湛卢" then
           wds.说明 = "上古名剑，剑身宽大，湛然如墨，骨锻其脊，血淬其锋，成绝世名剑"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 =  100
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1556155532
           wds.大模型资源 = 559555740------------
        elseif wd == "魏武青虹" then
           wds.说明 = "通体青亮如玉，长八尺余，可于千里外取人首级，吸血如虹。曾为魏王曹操配剑，后在战乱中失传，如今又重现江湖。"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 120
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1705858659
           wds.大模型资源 = 2821638391--------
        elseif wd == "灵犀神剑" then
           wds.说明 = "剑长七寸，有鼻有眼，与主人心有灵犀。常藏于宝葫芦内，主人有令，则出三丈白光扣敌泥丸宫，尽斩仙魔鬼神。"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 130
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2712960569
           wds.大模型资源 = 866429909--------
        elseif wd == "四法青云" then
           wds.说明 = "上有符印，分别为地，火，风，水四字。挥舞时天昏地暗，火蛇狂舞，飞沙走石，龙腾海啸。"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 140
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1749029011
           wds.大模型资源 = 1989171435--------
        elseif wd == "霜冷九州" then
           wds.说明 = "冰晶雪魄，浑然天成；出则霜华满地，朔风回舞。其气凛冽，砭人肌骨；其意萧条，山川寂寥。乃天下至寒至洁之物，惟赤子心可驭之。"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 150
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2892720118
           wds.大模型资源 = 1286710645--------
        elseif wd == "擒龙" then
           wds.说明 = "上古魔神所铸神兵，可抗八部天龙。型如鱼骨，伸缩随心；刃化千万剑丝，柔如春蚕吐丝，韧如天地经纬。"
           wds.类型 = "武器"
           wds.类别 = "剑"
           wds.等级 = 160
           wds.角色限制 = "逍遥生,剑侠客"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1088458361
           wds.大模型资源 = 3004473909-----------
 --------------------------------爪
        elseif wd == "铁爪" then
            wds.说明 = "镔铁制成的铁爪"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 252413888
            wds.大模型资源 = 3216807428
        elseif wd == "天狼爪" then
            wds.说明 = "天山雪狼之爪，锐利无比"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds.价格 = 1000
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3766972932
            wds.大模型资源 = 2543379065
        elseif wd == "幽冥鬼爪" then
            wds.说明 = "精钢打造，饰以冥文，透出森森鬼气"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 473959188
            wds.大模型资源 = 3711514054
        elseif wd == "青龙牙" then
            wds.说明 = "龙牙状的尖刺，可以透入坚固的衣甲"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3410109601
            wds.大模型资源 = 2844091192
        elseif wd == "勾魂爪" then
            wds.说明 = "状似银勾，夺命勾魂"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds.价格 = 14000
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2945873492
            wds.大模型资源 = 3529952713
        elseif wd == "玄冰刺" then
            wds.说明 = "吸玄冰阴寒之气，可以直透骨骸"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds[4] = 6
            wds.等级 = 50
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3220766446
            wds.大模型资源 = 1038526054
        elseif wd == "青刚刺" then
            wds.说明 = "寒刚玉所制，坚硬无比，可断金"
            wds.类型 = "武器"
            wds.类别 = "爪刺"
            wds[4] = 6
            wds.等级 = 60
            wds.角色限制 = "狐美人,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1950914816
            wds.大模型资源 = 414938796
        elseif wd == "华光刺" then
           wds.说明 = "百炼纯钢打造，华光刺目"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 70
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1028689229
           wds.大模型资源 = 127658579
        elseif wd == "龙鳞刺" then
           wds.说明 = "金色似龙鳞的针刺"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 80
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 111030939
           wds.大模型资源 = 1177500748
        elseif wd == "撕天" then
           wds.说明 = "天如幕，白光起，破九重天"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 90
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3339488013
           wds.大模型资源 = 1471615949
        elseif wd == "毒牙" then
           wds.说明 = "恐惧，只有经历无尽虚空中袭来的毒牙才知其真意"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 =  100
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3832046145
           wds.大模型资源 = 3854364995
        elseif wd == "胭脂" then
           wds.说明 = "血红的颜色，胭脂的香气，将缤纷的灵魂凝结为黑白"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 =  100
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3460551634
           wds.大模型资源 = 3441823945
        elseif wd == "九阴勾魂" then
           wds.说明 = "爪有九刺，阴寒冷酷，破颅勾魂，使正道名门闻之色变。为地藏王所得，将要销毁，不知何人盗出，又重现江湖。"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 120
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 393789023
           wds.大模型资源 = 3180402786
        elseif wd == "雪蚕之刺" then
           wds.说明 = "昆仑山雪蚕之毒毛所化，坚硬非常，穿犀牛皮如针穿薄纱，瞬间透入；刺中后使人身中热毒，如在灼热地狱之中。"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 130
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2033558106
           wds.大模型资源 = 712813114
        elseif wd == "贵霜之牙" then
           wds.说明 = "昆仑山雪蚕之毒毛所化，坚硬非常，穿犀牛皮如针穿薄纱，瞬间透入；刺中后使人身中热毒，如在灼热地狱之中。"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 140
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3540442244
           wds.大模型资源 = 989722994
        elseif wd == "忘川三途" then
           wds.说明 = "忘川为生死界河，水分三途，缓急不同。此爪凝三途之水，可证罪业，望之似缓，暗流甚疾。伤者不可骤脱，如坠地狱、饿鬼、畜生道。"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 150
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4114737939
           wds.大模型资源 = 1943604910
        elseif wd == "离钩" then
           wds.说明 = "鸿蒙初辟，混沌之气混焉；后清浊渐离，有神识之气胶而不舍，亦聚亦离，愁丝千丈，钩绊难舍，岁月风化遂成离钩。"
           wds.类型 = "武器"
           wds.类别 = "爪刺"
           wds[4] = 6
           wds.等级 = 160
           wds.角色限制 = "狐美人,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3368946268
           wds.大模型资源 = 3833664365
 -------------------------------斧钺
        elseif wd == "青铜斧" then
            wds.说明 = "以硬木和青铜制造的大斧，是常用的兵器"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1194618546
            wds.大模型资源 = 1823715223
        elseif wd == "开山斧" then
            wds.说明 = "铁铸的巨大战斧，沉重无比，力大者用以开石裂碑"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.价格 = 1000
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2876225806
            wds.大模型资源 = 511692637
        elseif wd == "双面斧" then
            wds.说明 = "两面均制成半月状锋刃，双面制敌，力猛势强"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 595341788
            wds.大模型资源 = 917777182
        elseif wd == "双弦钺" then
            wds.说明 = "铁杆黝黑，银月如弦，舞动时眩人眼目"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.价格 = 9000
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2083951632
            wds.大模型资源 = 709060887
        elseif wd == "精钢禅钺" then
            wds.说明 = "精钢打造，环随舞动，铿锵入耳，震人心魄"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4268709342
            wds.大模型资源 = 1201979768
        elseif wd == "黄金钺" then
            wds.说明 = "以华丽的黄金装饰,使其更加沉重威猛,金光起处血光飞溅"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.等级 = 50
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 804669659
            wds.大模型资源 = 3468478581
        elseif wd == "乌金鬼头镰" then
            wds.说明 = "乌金打造的巨镰，透出森森鬼气，令人不寒而栗"
            wds.类型 = "武器"
            wds.类别 = "斧钺"
            wds.等级 = 60
            wds.角色限制 = "巨魔王,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1269150805
            wds.大模型资源 = 2315090325
        elseif wd == "狂魔镰" then
           wds.说明 = "沉于万魔之地，染魔性，映魔心，挥之即魔瘴四起"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 70
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2105378421
           wds.大模型资源 = 1325914922
        elseif wd == "恶龙之齿" then
           wds.说明 = "寒钢为柄，龙齿为刃，风过龙鸣，摄破敌胆"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 80
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2656260066
           wds.大模型资源 = 2796123546
        elseif wd == "破魄" then
           wds.说明 = "集艳日与阴月之精华，挥舞如日月同晖，中者形神俱灭"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 90
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3159282811
           wds.大模型资源 = 4245622670
        elseif wd == "肃魂" then
           wds.说明 = "吸取无尽黑暗之气，肃清无规、无序、无章之魂"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 =  100
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 0x2D6D80DE
           wds.大模型资源 = 0x1CE23AB4
        elseif wd == "无敌" then
           wds.说明 = "斧杆以千年古木之根打造，盘桓虬曲；寒铁锻造的双面巨斧发出魔气逼人的暗光"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 110
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 872395348
           wds.大模型资源 = 3255150154
        elseif wd == "五丁开山" then
           wds.说明 = "古蜀有五力士奉命凿通群山，见一大蛇尾在洞外，乃曳之出，其身长数十里。惊讶间，山崩，五丁与蛇俱死，化为神斧，执之者有开山之力。"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 120
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1781160921
           wds.大模型资源 = 354227088
        elseif wd == "元神禁锢" then
           wds.说明 = "阴司轮转王司过之宝。若有鬼仙犯大恶，即以此物拟其头，吸尽元神，再无转生可能。细看刃缘，可见无数痛苦鬼脸。"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 130
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1793469736
           wds.大模型资源 = 3058265679
        elseif wd == "护法灭魔" then
           wds.说明 = "佛门护法之武器，柄为鲁班所雕吴刚伐桂之枝，斧为老君所炼炉中千年玄铁，上有佛门梵文，妖怪见之退避三舍。"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 140
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3099472859
           wds.大模型资源 = 2571183494
        elseif wd == "碧血干戚" then
           wds.说明 = "刑天与帝争神，身首异处，碧血零落，干缺戚残；后人冶于一炉乃得神兵。长戚舞处，风云雷动，碧波流转，巨盾透体而出，其鸣呜咽，如英雄泣。"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 150
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3681089967
           wds.大模型资源 = 1076008261
        elseif wd == "裂天" then
           wds.说明 = "蚩尤随身魔武之一，取大荒四极异兽骨所制，形如巨镰，镰刃淬海皇兽魂，隐然有嘶鸣之声，裂天笞地，割风断雨。"
           wds.类型 = "武器"
           wds.类别 = "斧钺"
           wds.等级 = 160
           wds.角色限制 = "巨魔王,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3711850827
           wds.大模型资源 = 1944978391
 -------------------------------飘带
        elseif wd == "五色缎带" then
            wds.说明 = "五色蚕丝织成的彩缎"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 964324271
            wds.大模型资源 = 359839285
        elseif wd == "幻彩银纱" then
            wds.说明 = "上等蚕丝与银丝混织成的纱带"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3727411355
            wds.大模型资源 = 1872145487
        elseif wd == "金丝彩带" then
            wds.说明 = "金丝与彩缎编成的丝带"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3086295697
            wds.大模型资源 = 81211738
        elseif wd == "无极丝" then
            wds.说明 = "天地无极尽收于其间"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 85830180
            wds.大模型资源 = 4066138115
        elseif wd == "天蚕丝带" then
            wds.说明 = "以名贵天蚕丝制成，可通生命之灵气"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 794010110
            wds.大模型资源 = 140768110
        elseif wd == "云龙绸带" then
            wds.说明 = "绸如云彩，龙隐其间"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 50
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 140457428
            wds.大模型资源 = 4041408902
        elseif wd == "七彩罗刹" then
            wds.说明 = "用各式丝绸编织而成的丝带"
            wds.类型 = "武器"
            wds.类别 = "飘带"
            wds.等级 = 60
            wds.角色限制 = "舞天姬,玄彩娥"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4216697314
            wds.大模型资源 = 1196299810
        elseif wd == "缚神绫" then
           wds.说明 = "丝带可紧可松，传言可缚神灵"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 70
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2774766030
           wds.大模型资源 = 316900368
        elseif wd == "九天仙绫" then
           wds.说明 = "存于九天，吸日月玄灵"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 80
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 812342002
           wds.大模型资源 = 262652275
        elseif wd == "彩虹" then
           wds.说明 = "以七彩天蚕丝制成，似天边彩虹"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 90
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1130709640
           wds.大模型资源 = 3920434794
        elseif wd == "流云" then
           wds.说明 = "流光溢彩，似天边云彩幻生幻灭"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 =  100
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1567038678
           wds.大模型资源 = 2712300537
        elseif wd == "碧波" then
           wds.说明 = "聚海之灵气，碧波起，天地清"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 =  100
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3222386953
           wds.大模型资源 = 2391803383
        elseif wd == "秋水落霞" then
           wds.说明 = "“落霞与孤鹜齐飞，秋水共长天一色。”此带乃天女掬长空之色，染落霞之艳织成。展开绚烂无比，使人俗念顿消，心无点尘。"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 120
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 604608093
           wds.大模型资源 = 1182432751
        elseif wd == "晃金仙绳" then
           wds.说明 = "“此物可随主人意捆住神仙魔怪，令其不能再施展变化之术。且此绳坚韧异常，火烧刀砍不断。"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 130
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 730252838
           wds.大模型资源 = 2065742633
        elseif wd == "此最相思" then
           wds.说明 = "“道是无情却有情，从来最苦是相思。”此物乃太虚幻境警幻仙子敕令还情债之物，一旦缠住，至死方休；虽有金石心，也顿为绕指柔，神仙难逃。"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 140
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1161776179
           wds.大模型资源 = 1438567800
        elseif wd == "揽月摘星" then
           wds.说明 = "翩若惊鸿，婉若游龙，仿佛兮若轻云之蔽月，飘飖兮若流风之回雪。三垣为画布，此带为软毫，可清夜起舞，拟为漫天星宿平添花鸟虫鱼。"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 150
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3130490807
           wds.大模型资源 = 1909699944
        elseif wd == "九霄" then
           wds.说明 = "九霄澄云所凝，诸天护佑，乾坤氤氲。形随意动，其高则冠盖乎九霄，其旷则笼罩乎八隅，祥瑞纷现，异香扑鼻。"
           wds.类型 = "武器"
           wds.类别 = "飘带"
           wds.等级 = 160
           wds.角色限制 = "舞天姬,玄彩娥"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3468947580
           wds.大模型资源 = 3104627686
 -------------------------------宝珠
        elseif wd == "琉璃珠" then
            wds.说明 = "三色琉璃熔成的宝珠，轻盈剔透，适合新手携带"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2607779407
            wds.大模型资源 = 3954061095
        elseif wd == "水晶珠" then
            wds.说明 = "晶莹水晶打磨而成的水晶球，发出微微的光晕。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 424958581
            wds.大模型资源 = 517101356
        elseif wd == "珍宝珠" then
            wds.说明 = "东海海底方能寻得的巨大珍珠，光华缓缓流动，似是凝聚了海底之中游鱼的灵气。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2649110916
            wds.大模型资源 = 1758505762
        elseif wd == "翡翠珠" then
            wds.说明 = "南蛮之地进献之翡翠，经能工巧匠九琢九磨，方成玲珑宝珠，通体翠绿凝碧，触手冰凉，仿似有仙气缭绕。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3824352456
            wds.大模型资源 = 3517783128
        elseif wd == "莲华珠" then
            wds.说明 = "来自天竺佛国的灵巧宝珠，晶莹圆润，光华四溢，置于水中，可化作轻盈莲花，净水生香。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 121754786
            wds.大模型资源 = 3809551301
        elseif wd == "夜灵珠" then
            wds.说明 = "白日之时看似平平无奇，置于灯火昏昏之处，方有幽光融融。其夜愈深，而光华愈明。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 50
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3855395369
            wds.大模型资源 = 814300484
        elseif wd == "如意宝珠" then
            wds.说明 = "琅嬛福地生香橼，百年方熟，熟而坠地，化为如意宝珠，鲜明莹洁，祥瑞兆也。"
            wds.类型 = "武器"
            wds.类别 = "宝珠"
            wds.等级 = 60
            wds.角色限制 = "巫蛮儿,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1237441752
            wds.大模型资源 = 296013680
        elseif wd == "沧海明珠" then
           wds.说明 = "东海之浪，西海之湍，南海之潮，北海之涌，凝为玲珑明珠，置于耳畔，可闻四海浪涛奔涌。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 70
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3775040856
           wds.大模型资源 = 1581614754
        elseif wd == "无量玉璧" then
           wds.说明 = "奇珍璞玉，历神工鬼斧琢磨百年，方成无瑕白璧，明澈如镜，可见本心。心无旁骛，不惹尘埃。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 80
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 219820456
           wds.大模型资源 = 1327897055
        elseif wd == "离火" then
           wds.说明 = "北地铁骨，质坚且寒，南疆离火，熔之成珠。铁骨灼灼，烈火熊熊，炽焰炎炎，刚猛之至。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 90
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3130687183
           wds.大模型资源 = 295884750
        elseif wd == "飞星" then
           wds.说明 = "河汉渺渺，星华灿灿。流星坠地，而光华不灭，繁光云影，剔透灵动。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 =  100
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2857125772
           wds.大模型资源 = 3385292234
        elseif wd == "月华" then
           wds.说明 = "芙蓉出水，风荷凌波。采月宫新荷之叶，凝流金月色之光，成琳琅花灯一盏，暖光融融，晴光皎皎。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 =  100
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3767404245
           wds.大模型资源 = 776402569
        elseif wd == "回风舞雪" then
           wds.说明 = "洛水汤汤，钟灵毓秀。河洛之女质比霜雪，态似惊鸿；一日童心乍起，采河滨之蝶、沉水之冰，吟清歌而成宝珠，蝶舞之时回风舞雪，琴音泠泠。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 120
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3065427050
           wds.大模型资源 = 269710225
        elseif wd == "紫金葫芦" then
           wds.说明 = "混沌初分，天开地辟，昆仑山脚下生仙藤一缕，上结葫芦七枚，名曰紫金葫芦，千年方得瓜熟蒂落，有吞吐万象之力。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 130
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1536710915
           wds.大模型资源 = 2788268887
        elseif wd == "裂云啸日" then
           wds.说明 = "烛龙之鳞，采璇玑之华，待千年。浴羲和之光，再千年。经九煅九灼，方熔为赤金之珠，其光煌煌，可照千里，其势熊熊，耀日贯虹。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 140
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3954481057
           wds.大模型资源 = 3032716270
        elseif wd == "云雷万里" then
           wds.说明 = "北溟之鲲，化而为鹏，凌风而起，翼若垂天之云，啸如雷霆之怒，水击三千，风驰如电。沉眠时化作神异之珠，聚风雷云电之力，有呼山啸海之能。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 150
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3251343750
           wds.大模型资源 = 3115679889
        elseif wd == "赤明" then
           wds.说明 = "盘古开天辟地后，齿骨为金石，精髓为珠玉，灵窍一点聚天地浩气，化为宝珠“赤明”，虹霓为衣，金螭为息，吞云吐明，异光万里。"
           wds.类型 = "武器"
           wds.类别 = "宝珠"
           wds.等级 = 160
           wds.角色限制 = "巫蛮儿,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1303853669
           wds.大模型资源 = 4145962850
 -------------------------------法杖
        elseif wd == "曲柳杖" then
            wds.说明 = "柳木雕凿的法杖，蕴含一点法力，适合新手使用"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 214558735
            wds.大模型资源 = 1713378827
        elseif wd == "红木杖" then
            wds.说明 = "用上好红木刻成的法杖，十分坚硬，内含一点奇妙的法力。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2519088005
            wds.大模型资源 = 2809977168
        elseif wd == "白椴杖" then
            wds.说明 = "用极佳的椴木精心雕凿而成的法杖，杖身光滑，蕴含着奇妙的法力。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1994971958
            wds.大模型资源 = 1336522328
        elseif wd == "墨铁拐" then
            wds.说明 = "墨铁打造而成的法杖，冰冷坚硬，似乎含有奇妙的力量。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 6277753
            wds.大模型资源 = 2738948963
            elseif wd == "玄铁牛角杖" then
            wds.说明 = "用寒铁锻冶的牛角状法杖，尖锐坚硬，闪着寒光，隐含奇妙的法力。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3096649690
            wds.大模型资源 = 1043225710
        elseif wd == "鹰眼法杖" then
            wds.说明 = "孤鹰展翅，划破长空，依照孤鹰之形打造的法杖，蕴含神奇法力。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 50
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3849435987
            wds.大模型资源 = 2058809879
        elseif wd == "腾云杖" then
            wds.说明 = "仙人踏云而至，有着祥云之形的云母石法杖，有着奇妙法力。"
            wds.类型 = "武器"
            wds.类别 = "法杖"
            wds.等级 = 60
            wds.角色限制 = "巫蛮儿,羽灵神"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4198430474
            wds.大模型资源 = 1425813651
        elseif wd == "引魂杖" then
           wds.说明 = "点点萤光，引路幽魂，指引迷途幽魂的法杖，有着强大的法力"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 = 70
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2962804578
           wds.大模型资源 = 1687273756
        elseif wd == "碧玺杖" then
           wds.说明 = "碧霞莹莹，玉暖生烟，浑然天成的碧玉法杖，神妙异常"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 = 80
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 264053858
           wds.大模型资源 = 203186473
        elseif wd == "业焰" then
           wds.说明 = "取地狱燧石锻冶，燃万年业火之杖。“以业火乾枯，酬其宿债”，业焰之舞，焚尽世间罪孽。"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 = 90
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1824574630
           wds.大模型资源 = 2003240450
        elseif wd == "玉辉" then
           wds.说明 = "琉璃玉辉，星降人间，熠熠生辉的琉璃法杖，挥舞时如繁星落尽，蕴含无限灵力。"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 =  100
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3341482479
           wds.大模型资源 = 3389535995
        elseif wd == "鹿鸣" then
           wds.说明 = "牡鹿凄鸣，风云雷动，天下至寒之墨玉炼成鹿角之形，夹风雷之势，让敌人闻之色变。"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 =  100
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4184601743
           wds.大模型资源 = 1009912375
        elseif wd == "庄周梦蝶" then
           wds.说明 = "“昔者庄周梦为蝴蝶，栩栩然蝴蝶也”梦中蝶舞花落，焉知人世几何？杖取冥府幽灯为首，引路之蝶翩飞，最善蛊惑人心。"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 = 120
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4210455798
           wds.大模型资源 = 3003334713
        elseif wd == "凤翼流珠" then
           wds.说明 = "有凤来仪，祥瑞兆也。炎帝融千金，取明珠，塑凤凰振翅衔珠之形，炼得一杖，光华灿烂，百鸟朝凤，深蕴妙法。"
           wds.类型 = "武器"
           wds.类别 = "法杖"
           wds.等级 = 130
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3777157831
           wds.大模型资源 = 1526765929
        elseif wd == "雪蟒霜寒" then
           wds.说明 = "刘邦斩白蛇，取其精魄封于至阴至寒之地，吸寒冰精华千年，蛇魄化杖，成“雪蟒霜寒”，阴鸷寒毒，唯奇人异士方可驾驭自如。"
           wds.类型 = "武器"
           wds.类别 = "法杖"

           wds.等级 = 140
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1585143397
           wds.大模型资源 = 3652569447
        elseif wd == "碧海潮生" then
           wds.说明 = "应龙伏旱魃，寻深海冰晶，取潮涌奔腾之势，融白浪连山之气，吐龙息为灵珠，成龙神逐珠之形，终获一杖，名曰“碧海潮生”。"
           wds.类型 = "武器"
           wds.类别 = "法杖"

           wds.等级 = 150
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 859134972
           wds.大模型资源 = 2904010505
        elseif wd == "弦月" then
           wds.说明 = "盘古开天辟地之时，一外化陨铁落于不周山之巅，沐月华，吸星魄，尽得天地清明之气，化一杖，清辉万丈，疏影琉璃，曰之“弦月”。"
           wds.类型 = "武器"
           wds.类别 = "法杖"

           wds.等级 = 160
           wds.角色限制 = "巫蛮儿,羽灵神"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1620809403
           wds.大模型资源 = 481332718
 -------------------------------弓弩
        elseif wd == "硬木弓" then
            wds.说明 = "柳木头制成的短弓，质轻便携，很适合新手练习使用"
            wds.类型 = "武器"
            wds.类别 = "弓弩"
             wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1102238567
            wds.大模型资源 = 322855055
        elseif wd == "铁胆弓" then
            wds.说明 = "硬木制成的弓箭，背嵌铁条，射程很远。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1814282557
            wds.大模型资源 = 2181027484
        elseif wd == "紫檀弓" then
            wds.说明 = "精致的紫楠木弓箭，云纹雕花流畅雅致。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1817338084
            wds.大模型资源 = 1474683356
        elseif wd == "宝雕长弓" then
            wds.说明 = "乌木长弓，弓身镌刻之宝雕，栩栩如生，振翅欲飞。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2253432731
            wds.大模型资源 = 1106872316
        elseif wd == "錾金宝弓" then
            wds.说明 = "黑檀宝弓，经巧匠之手精雕细錾，华贵不可方物。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 234218100
            wds.大模型资源 = 706673394
        elseif wd == "玉腰弯弓" then
            wds.说明 = "柘木宝弓嵌以碧绿美玉，质坚且韧，精准凌厉。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 50
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3397522151
            wds.大模型资源 = 106500071
        elseif wd == "连珠神弓" then
            wds.说明 = "玲珑连珠，发机如电，一发连五，绝无虚矢。"
            wds.类型 = "武器"
            wds.类别 = "弓弩"

            wds.等级 = 60
            wds.角色限制 = "羽灵神,杀破狼"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4094037333
            wds.大模型资源 = 2641579130
        elseif wd == "游鱼戏珠" then
           wds.说明 = "弓箭之形仿若双鱼，共戏剔透宝珠。出箭之势，亦如游鱼嬉水，婉转灵动，让对手无从躲避。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 70
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1424340640
           wds.大模型资源 = 4113025121
        elseif wd == "灵犀望月" then
           wds.说明 = "天降奇石，锻为兵器，自生光华。美玉做灵犀之形，嵌于弓腰，祥瑞兆也。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 80
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2652946197
           wds.大模型资源 = 3194521736
        elseif wd == "非攻" then
           wds.说明 = "墨家机关之术神秘莫测，相传有采玉人偶得其残片，上记“非攻”弓弩，机括精妙，威力非常。遂寻美玉，勤琢磨，终得天下难见之绝品。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 90
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3157963095
           wds.大模型资源 = 1966338355
        elseif wd == "幽篁" then
           wds.说明 = "潮音洞外梵音悠悠，云岚缭绕，幽篁万杆，满目翠色。林中紫竹既坚且韧，扣之琅然有金石声，金童子置之于老君炉中，锻成神弓，名曰“幽篁”。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 =  100
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3823221168
           wds.大模型资源 = 3873525610
        elseif wd == "百鬼" then
           wds.说明 = "春秋时古墓中，青铜之樽，凝百鬼之魂，锻九千日，乃成。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 =  100
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1920235031
           wds.大模型资源 = 878233843
        elseif wd == "冥火薄天" then
           wds.说明 = "幽冥之地，麒麟衔火而生，踏火而行，所经处金石皆熔，游九州聚天地精华，得遇有缘之士，乃化作良弓一柄，凌风驭火，箭矢如焰，有燎日之能。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 120
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3592078104
           wds.大模型资源 = 944851297
        elseif wd == "龙鸣寒水" then
           wds.说明 = "云龙居寒水，可呼云雨，开江海，鸣动天地，腾云起风。此弩熔云龙之鳞，凝万年寒水之底，聚玄冰苦寒之气，千年方成，出矢如冰，霜寒雪冷。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 130
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4125263988
           wds.大模型资源 = 566006425
        elseif wd == "太极流光" then
           wds.说明 = "天地之道，以阴阳二气造化万物，日月、风雨，刚柔、动静莫不合此道也。此番水火相逢，相生相克，有大神通。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 140
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1065909571
           wds.大模型资源 = 453912996
        elseif wd == "九霄风雷" then
           wds.说明 = "灵凰栖九天，高飞穿云，清音裂石。妙手仙匠采灵凰之魄，河汉之星，云雷之怒，以九霄长风锻造千载而成此弓，箭簇既出，风雷声动，江海怒号。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 150
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1632354244
           wds.大模型资源 = 4026131118
        elseif wd == "若木" then
           wds.说明 = "金乌栖若木，振翅日升，归巢日落，十鸟齐飞，则十日齐出，民不聊生。后羿采若木为弓，连射九日，凝金乌之魂于弓上，羲和光华，日夜不息。"
           wds.类型 = "武器"
           wds.类别 = "弓弩"

           wds.等级 = 160
           wds.角色限制 = "羽灵神,杀破狼"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2281686232
           wds.大模型资源 = 1581270869
 -------------------------------枪矛
        elseif wd == "红缨枪" then
            wds.说明 = "白腊木制成的枪杆，柔韧有弹性"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3032516444
            wds.大模型资源 = 661869979
        elseif wd == "曲尖枪" then
            wds.说明 = "采用上等合木，枪尖九曲，是常用的兵器"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3971589602
            wds.大模型资源 = 632407849
        elseif wd == "锯齿矛" then
            wds.说明 = "枪尖三陷三突，枪杆红绸缠绕，彩缨闪动令人眼花瞭乱"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2315151120
            wds.大模型资源 = 1069998337
        elseif wd == "乌金三叉戟" then
            wds.说明 = "玄铁长枪，头部以乌金打造，气势如虹"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2487968590
            wds.大模型资源 = 3770180780
        elseif wd == "火焰枪" then
            wds.说明 = "以火焰状的精铁代替枪缨，焰缘锋利无比"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 382126947
            wds.大模型资源 = 2230233436
        elseif wd == "墨杆金钩" then
            wds.说明 = "乌金铁杆，枪尖粗大且制有金色的粗大倒钩，威猛无比"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 50
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1104964498
            wds.大模型资源 = 3076442266
        elseif wd == "玄铁矛" then
            wds.说明 = "枪头玄铁制成，宽大沉重，可刺可砍，威势不可挡"
            wds.类型 = "武器"
            wds.类别 = "枪矛"
            wds.等级 = 60
            wds.角色限制 = "神天兵,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4108563339
            wds.大模型资源 = 1450072521
        elseif wd == "金蛇信" then
           wds.说明 = "神木制杆，弯曲似蛇形，金枪透甲，腥红双叉似金蛇出信"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 70
           wds.角色限制  = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3443351294
           wds.大模型资源 = 2134413363
        elseif wd == "丈八点钢矛" then
           wds.说明 = "通体百精纯钢，如天雷击落、蛟龙出海"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 80
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 853333771
           wds.大模型资源 = 2376999944
        elseif wd == "暗夜" then
           wds.说明 = "通体乌黑,极细的枪尖无形中直抵咽喉,如暗夜中引魂之手"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 90
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2297122969
           wds.大模型资源 = 4085619601
        elseif wd == "梨花" then
           wds.说明 = "锋刃五裂似梨花绽放,粉红的枪缨银白的枪,尽显潇洒风流"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 =  100
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1491619643
           wds.大模型资源 = 3901170806
        elseif wd == "霹雳" then
           wds.说明 = "以万古溶岩之火锤炼，枪杆似古树盘结，枪尖似雷霆霹雳"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 =  100
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1553552793
           wds.大模型资源 = 1828152464
        elseif wd == "刑天之逆" then
           wds.说明 = "刑天应劫而出，劫分天兵火水土，此即为其应兵劫之武器。形长两丈，刃如月牙，锋似寒星，身雕百龙蟠纹，挥舞直欲搅碎银汉，号为枪中霸君。"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 120
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3591831101
           wds.大模型资源 = 3656521229
        elseif wd == "五虎断魂" then
           wds.说明 = "出如虎奔，刺如虎爪，扫如虎尾，锁如虎噬，声如虎啸；使敌魂断胆破，不战自溃，谓之“五虎断魂”，有枪中王者之气。"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 130
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1491403685
           wds.大模型资源 = 1403609878
        elseif wd == "飞龙在天" then
           wds.说明 = "曾为文殊菩萨降妖密宝，如今幻化为一杆金龙宝枪。此枪掷出可化为飞龙，从万丈高空俯冲而下，撕碎敌人。"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 140
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4100462769
           wds.大模型资源 = 3889305017
        elseif wd == "天龙破城" then
           wds.说明 = "天外陨石炼九日九夜，雷生地底、天坠神龙，乃成此戟。霸王项羽持之横行当世，睥睨天下，故名“天龙破城”。霸王既殁，佚于乌江，龙衔乃出。"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 150
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1375567358
           wds.大模型资源 = 1041126896
        elseif wd == "弑皇" then
           wds.说明 = "炎黄之战，杀怨之气俱归此兵。黄帝得胜，以此兵祭祀天地，誓曰:若治下三界失靖、四海不宁，则天下之人皆可持此兵取吾之首级！"
           wds.类型 = "武器"
           wds.类别 = "枪矛"
           wds.等级 = 160
           wds.角色限制 = "龙太子,神天兵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 439395128
           wds.大模型资源 = 127581936
 -------------------------------环圈
        elseif wd == "黄铜圈" then
            wds.说明 = "黄铜制的环圈，握手处缚上丝线，是常用的兵器"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 8632408
            wds.大模型资源 = 526420633
        elseif wd == "精钢日月圈" then
            wds.说明 = "精钢打造的圆环，外缘锋利"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2611939848
            wds.大模型资源 = 523588802
        elseif wd == "离情环" then
            wds.说明 = "乌金铁环，通体乌黑，似悠悠离情"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2588804470
            wds.大模型资源 = 2925351135
        elseif wd == "金刺轮" then
            wds.说明 = "由多片精铁打造的极薄的锋刃组成，刃口白光闪闪"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1780092674
            wds.大模型资源 = 2925351135
        elseif wd == "风火圈" then
            wds.说明 = "钢制的边缘有似火焰般的刀锋"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3696047428
            wds.大模型资源 = 3443374908
        elseif wd == "赤炎环" then
            wds.说明 = "环边有锯齿，如太阳之火焰"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 50
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 692700444
            wds.大模型资源 = 609650976
        elseif wd == "蛇形月" then
            wds.说明 = "划动时锋刃似盘卷的黑蛇，令人防不胜防"
            wds.类型 = "武器"
            wds.类别 = "环圈"
            wds.等级 = 60
            wds.角色限制 = "飞燕女,舞天姬"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3576968772
            wds.大模型资源 = 2783575959
        elseif wd == "子母双月" then
           wds.说明 = "锋刃似弯月一般交错，可四面制敌"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 70
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3736409706
           wds.大模型资源 = 1612643041
        elseif wd == "斜月狼牙" then
           wds.说明 = "青玉制成，锋似狼牙"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 80
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4116561830
           wds.大模型资源 = 2276604919
        elseif wd == "如意" then
           wds.说明 = "白玉制成，光滑清丽，如意随心"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 90
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2711948052
           wds.大模型资源 = 952271091
        elseif wd == "乾坤" then
           wds.说明 = "集天地乾坤之灵，可制万物"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 =  100
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2571022629
           wds.大模型资源 = 338904132
        elseif wd == "月光" then
           wds.说明 = "清明无暇，似映月之光"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 =  100
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3562014914
           wds.大模型资源 = 2953019352
        elseif wd == "别情离恨" then
           wds.说明 = "乐莫乐兮新相知，悲莫悲兮生别离；此情黯然销魂，永恨伤心。有人制同心永结环象征永不分离，但别离之后把玩，却更加伤感。"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 120
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2437493189
           wds.大模型资源 = 3959090370
        elseif wd == "金玉双环" then
           wds.说明 = "乐莫乐兮新相知，悲莫悲兮生别离；此情黯然销魂，永恨伤心。有人制同心永结环象征永不分离，但别离之后把玩，却更加伤感。"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 130
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2908362125
           wds.大模型资源 = 1659862730
        elseif wd == "九天金线" then
           wds.说明 = "此物套在手上，金光闪闪。圈飘金线，声音清越，守中有攻，刚柔并济，使敌人不知所措。"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 140
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1468552095
           wds.大模型资源 = 2047583638
        elseif wd == "无关风月" then
           wds.说明 = "精玉双环一名“离风”，一名“遮月”，舞之月影憧憧，别愁离绪，俱上心头。诗云：人生自是有情痴，此恨不关风与月。"
           wds.类型 = "武器"
           wds.类别 = "环圈"
           wds.等级 = 150
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3036111619
           wds.大模型资源 = 3104854202
        elseif wd == "朝夕" then
           wds.说明 = "此双环伴日月而生，沐浴光华，吞吐烟云，三清将其置于天穹之上，日夜吸纳诸天亿兆流光，朝夕霞雾源出于此。"
           wds.类型 = "武器"
           wds.类别 = "环圈"

           wds.等级 = 160
           wds.角色限制 = "飞燕女,舞天姬"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1379061378
           wds.大模型资源 = 1662428245
 -------------------------------魔棒
        elseif wd == "细木棒" then
            wds.说明 = "看似细小却有不小的威力，可以当作兵器防身"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3647840943
            wds.大模型资源 = 445770073
        elseif wd == "金丝魔棒" then
            wds.说明 = "棒身缠以金丝，显得华丽"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2190248338
            wds.大模型资源 = 850861563
        elseif wd == "玉如意" then
            wds.说明 = "细长的白玉棒，白玉使魔棒能力得到加强"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1722461164
            wds.大模型资源 = 4043165218
        elseif wd == "点金棒" then
            wds.说明 = "华丽的蛇形棒，相传作法之人常用其点石成金"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3187022651
            wds.大模型资源 = 2074779560
        elseif wd == "云龙棒" then
            wds.说明 = "棒身九曲，似云龙腾跃"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1889288915
            wds.大模型资源 = 741134712
        elseif wd == "幽路引魂" then
            wds.说明 = "通体乌黑，引魂归冥"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 50
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3655607718
            wds.大模型资源 = 496189030
        elseif wd == "满天星" then
            wds.说明 = "棒头细密的镶嵌了无数宝石，闪闪发光，似满天星辰"
            wds.类型 = "武器"
            wds.类别 = "魔棒"
            wds.等级 = 60
            wds.角色限制 = "玄彩娥,骨精灵"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3427188370
            wds.大模型资源 = 1777161115
        elseif wd == "水晶棒" then
           wds.说明 = "通体以水晶制成，棒头镶嵌了纯净的水晶球，灵光闪闪"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 70
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3433794516
           wds.大模型资源 = 1032230318
        elseif wd == "日月光华" then
           wds.说明 = "棒端有日月神珠，半黑半白，收太极其内，五行其间"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 80
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1445409930
           wds.大模型资源 = 345311421
        elseif wd == "沧海" then
           wds.说明 = "静如海之渊深博从，动似海之激荡翻腾"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 90
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3417923595
           wds.大模型资源 = 32733626
        elseif wd == "红莲" then
           wds.说明 = "碧杆红莲，阅众生相，度众生孽"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 =  100
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2931792494
           wds.大模型资源 = 2090478337
        elseif wd == "盘龙" then
           wds.说明 = "身若九曲盘龙，口吞八卦金珠"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 =  100
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2599922092
           wds.大模型资源 = 1006965180
        elseif wd == "降魔玉杵" then
           wds.说明 = "乃一块绝世好玉雕刻而成，杵头刻有仙家符咒。拿在手中，轻如鸿毛，打在人身，重如泰山。"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 120
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 969185484
           wds.大模型资源 = 1480701803
        elseif wd == "青藤玉树" then
           wds.说明 = "翡翠藤蔓缠绕于碧玉树干之上，红宝石所雕的鲜花正怒放枝头；它是完美的艺术品，更是仙家妙宝、除魔利器。"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 130
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3608519305
           wds.大模型资源 = 1933825756
        elseif wd == "墨玉骷髅" then
           wds.说明 = "魔棒上镶嵌着一个刻画精美的墨水晶骷髅，流露出神秘的死亡气息，似乎与地府有很深的渊源。"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 140
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 758533892
           wds.大模型资源 = 1365535468
        elseif wd == "丝萝乔木" then
           wds.说明 = "上古仙侣所化，丝萝出尘，花分五色；中为桐木，曾栖彩凤。千秋万载，其情不泯。仙气缠绕，百邪不侵：“醉向丝萝惊自醒，与君清耳听松湍。”"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 150
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 832692260
           wds.大模型资源 = 18189260
        elseif wd == "醍醐" then
           wds.说明 = "天地未分时，混沌有异树，亿万年长成，亿万年花开，亿万年结果，果落化为醍醐。持此杖者洞悉天地，与万物同息。"
           wds.类型 = "武器"
           wds.类别 = "魔棒"
           wds.等级 = 160
           wds.角色限制 = "玄彩娥,骨精灵"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 493750456
           wds.大模型资源 = 3429406352
 ------------------------------双短剑
        elseif wd == "双短剑" then
            wds.说明 = "青铜制的双短剑，制造简单，是练习常用的兵器"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3212246691
            wds.大模型资源 = 1334943898
        elseif wd == "镔铁双剑" then
            wds.说明 = "铁制双短剑，轻巧实用"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1169767913
            wds.大模型资源 = 2278749441
        elseif wd == "龙凤双剑" then
            wds.说明 = "双剑剑身一刻龙纹一刻凤饰，可同入一鞘"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2574377494
            wds.大模型资源 = 2688315512
        elseif wd == "竹节双剑" then
            wds.说明 = "精钢制成，剑身似竹节般伸长"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1329171010
            wds.大模型资源 = 2218313214
        elseif wd == "狼牙双剑" then
            wds.说明 = "剑身向前方密布粗长的狼牙锯齿，令人胆寒长"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1830383421
            wds.大模型资源 = 885886005
        elseif wd == "鱼骨双剑" then
            wds.说明 = "剑身似鱼骨，长满倒钩刺"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 50
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 741866499
            wds.大模型资源 = 1761353936
        elseif wd == "赤焰双剑" then
            wds.说明 = "剑身赤红，齿如焰苗窜动，有一股刚烈之势"
            wds.类型 = "武器"
            wds.类别 = "双短剑"
            wds.等级 = 60
            wds.角色限制 = "英女侠,飞燕女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1459131170
            wds.大模型资源 = 1669329241
        elseif wd == "墨玉双剑" then
           wds.说明 = "体采寒冰墨玉所制，暗光流动"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 70
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3841664619
           wds.大模型资源 = 3428424501
        elseif wd == "梅花双剑" then
           wds.说明 = "千年古木所制，剑似梅枝"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 80
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 956549407
           wds.大模型资源 = 353661251
        elseif wd == "阴阳" then
           wds.说明 = "赤阳之铁成阳，玄冰之玉成阴，两仪相生，气韵非凡"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 90
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2351851050
           wds.大模型资源 = 2463244045
        elseif wd == "月光双剑" then
           wds.说明 = "皎洁如白玉，清明如月光  "
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 =  100
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 38511533
           wds.大模型资源 = 2184315991
        elseif wd == "灵蛇" then
           wds.说明 = "采千年寒冰碧玉制成，通体碧绿，剑出似灵蛇出洞"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 =  100
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3192742770
           wds.大模型资源 = 1950373522
        elseif wd == "金龙双剪" then
           wds.说明 = "传说乃是一对金龙所化，头并头如剑，尾交尾如股。从半空中呼啸而下，一绞之力无人能挡。"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 120
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2674540671
           wds.大模型资源 = 2242603958
        elseif wd == "连理双树" then
           wds.说明 = "一对痴情男女坟前精魂所化双树，攀枝连理，象征在世虽不能合枕，来世愿永无分离。以此木为剑，因至情贯其中，也可成绕指柔，也可以断金玉。"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 130
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 835871520
           wds.大模型资源 = 1841177480
        elseif wd == "祖龙对剑" then
           wds.说明 = "三国时蜀帝刘备所用双剑。据说一为白龙所化，一为赤龙所化。白龙乃是秦皇，赤龙乃是汉祖。执之者当有九五之尊。"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 140
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4254611708
           wds.大模型资源 = 3430248664
        elseif wd == "紫电青霜" then
           wds.说明 = "鸿蒙灵气所钟，诸天精魄所禳。异器天生，一派自然。紫电稚弱，性如婴孩；青霜老沉，俨如兄长。翛忽隐现，捷如流星，啾啾呢喃，紫燕青虬。"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 150
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1921395100
           wds.大模型资源 = 3349222718
        elseif wd == "浮犀" then
           wds.说明 = "九天玄女以夔牛之角所制双剑，锋锐无双，轻如无物，曾助黄帝擒杀蚩尤。其上书天书密咒，丹血为文，翠莹如碧。"
           wds.类型 = "武器"
           wds.类别 = "双短剑"
           wds.等级 = 160
           wds.角色限制 = "英女侠,飞燕女"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3650328746
           wds.大模型资源 = 2262501629
 --------------------------------锤
        elseif wd == "松木锤" then
            wds.说明 = "松木质轻，制成的锤主要作练习用"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 753815448
            wds.大模型资源 = 3891646017
        elseif wd == "镔铁锤" then
            wds.说明 = "镔铁打造的锤是价廉而常被采用的兵器"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2560759534
            wds.大模型资源 = 2680776140
        elseif wd == "八棱金瓜" then
            wds.说明 = "锤似金瓜，八棱突起，是实用的战锤"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2288044564
            wds.大模型资源 = 959970229
        elseif wd == "狼牙锤" then
            wds.说明 = "锤面密布狼牙细齿，杀伤力不可小视"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 575451669
            wds.大模型资源 = 3613953095
        elseif wd == "烈焰锤" then
            wds.说明 = "锤面上制造了不少火焰波浪一般的锋刃，可怕的武器"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1937110983
            wds.大模型资源 = 2558099166
        elseif wd == "破甲战锤" then
            wds.说明 = "精钢打造的巨型战锤，上部粗大的尖齿，可破甲裂碑"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 50
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2961406351
            wds.大模型资源 = 2582952463
        elseif wd == "震天锤" then
            wds.说明 = "寒钢制成的锤头巨大沉重，一击之下威势震天"
            wds.类型 = "武器"
            wds.类别 = "锤"
            wds.等级 = 60
            wds.角色限制 = "神天兵,虎头怪"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1233600514
            wds.大模型资源 = 3915438961
        elseif wd == "巨灵神锤" then
           wds.说明 = "巨灵神所用神锤，锤似流星，非凡力可举，可挡十万天兵"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 70
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4112468633
           wds.大模型资源 = 977830532
        elseif wd == "天崩地裂" then
           wds.说明 = "静似天柱巍巍，动则天崩地裂"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 80
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 708738253
           wds.大模型资源 = 1573610528
        elseif wd == "八卦" then
           wds.说明 = "八卦其间，太极天成，天地造化，混沌无常"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 90
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 4244312578
           wds.大模型资源 = 928644092
        elseif wd == "鬼牙" then
           wds.说明 = "以万炼钢，合猛鬼牙，噬三界"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 =  100
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 737823355
           wds.大模型资源 = 25218036
        elseif wd == "雷神" then
           wds.说明 = "一击天地轰鸣，再击电光四起，三击万物俱灰"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 =  100
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3048546465
           wds.大模型资源 = 4287755866
        elseif wd == "混元金锤" then
           wds.说明 = "锤按六道之纹，柄接阴阳之气，持之者自有五行造化，诸天看护。"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 120
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1164647107
           wds.大模型资源 = 2379160869
        elseif wd == "九瓣莲花" then
           wds.说明 = "此锤乃仙池中的一朵婷婷玉莲，花开九瓣，蕊露金蓬。不知被谁采来，炼为兵器。虽已过五百年，依然盛开如新发。"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 130
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2151061457
           wds.大模型资源 = 432377528
        elseif wd == "鬼王蚀日" then
           wds.说明 = "斩魔王之头为锤，隐隐发出邪气，使日月无光。执此物则将与魔神契约，可拥魔王之力。"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 140
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1075077021
           wds.大模型资源 = 3952974465
        elseif wd == "狂澜碎岳" then
           wds.说明 = "水神共工至宝，内收九曲黄河，出则天地齐喑，奔涛如雷。共工怒触不周山，天柱折地维绝，水潦尘埃俱归东南，双锤亦泯然其中，为黄帝所得。"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 150
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3300988828
           wds.大模型资源 = 3989323201
        elseif wd == "碎寂" then
           wds.说明 = "盘古龙齿所化，固若金汤。嘘为风雨，吹为雷电，舞之铿锵如鸣金铁，乃天下至阳至刚之物，邪魔宵小望之丧魂。"
           wds.类型 = "武器"
           wds.类别 = "锤"
           wds.等级 = 160
           wds.角色限制 = "神天兵,虎头怪"
           wds.资源 = "WP.FT"
           wds.小模型资源 = 0xEA5B0F34
           wds.大模型资源 = 0xE977DF0B
 --------------------------------扇
        elseif wd == "折扇" then
            wds.说明 = "普通的纸扇改装而成，可以此练习最基本的使扇方法"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3563784870
            wds.大模型资源 = 817774560
        elseif wd == "铁骨扇" then
            wds.说明 = "铁制的扇骨会对敌人造成较大的威胁"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 290823954
            wds.大模型资源 = 1443372759
        elseif wd == "精钢扇" then
            wds.说明 = "精钢制的扇骨，可以格挡锋利的兵器攻击"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 742558490
            wds.大模型资源 = 208678969
        elseif wd == "铁面扇" then
            wds.说明 = "连扇面也用铁片构成，更易格挡，晃动可出声，乱敌耳目"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3848744867
            wds.大模型资源 = 2633687797
        elseif wd == "百折扇" then
            wds.说明 = "采用非常多的扇骨，扇面百折，非常坚固"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 742792031
            wds.大模型资源 = 972230490
        elseif wd == "劈水扇" then
            wds.说明 = "挥舞之下可使狂风止，水断流"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 50
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1676697803
            wds.大模型资源 = 108510315
        elseif wd == "神火扇" then
            wds.说明 = "扇面用红绸制，此扇动则似神火四起，使敌人心不能宁"
            wds.类型 = "武器"
            wds.类别 = "扇"
            wds.等级 = 60
            wds.角色限制 = "逍遥生,龙太子"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1646280508
            wds.大模型资源 = 2148260737
        elseif wd == "阴风扇" then
           wds.说明 = "扇骨采玄冰制，阴风习习，让人不寒而粟"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 = 70
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1174967558
           wds.大模型资源 = 466225414
        elseif wd == "风云雷电" then
           wds.说明 = "气势勇猛的大型折扇，扇开似风云突变，扇出似雷鸣电闪"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 = 80
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3812625478
           wds.大模型资源 = 876675307
        elseif wd == "太极" then
           wds.说明 = "包容太极于其内，博采天地之灵气"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 = 90
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 795792061
           wds.大模型资源 = 2262423432
        elseif wd == "玉龙" then
           wds.说明 = "白玉制的扇骨，优雅灵巧，似白龙出海"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 =  100
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3052815805
           wds.大模型资源 = 896644877
        elseif wd == "秋风" then
           wds.说明 = "一纸折扇，荡平天下事，一缕秋风，吹尽凡尘心"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 =  100
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 516817499
           wds.大模型资源 = 1806626143
        elseif wd == "画龙点睛" then
           wds.说明 = "本是一把普通的折扇，只因张僧繇画神龙于上，并点其睛，挥动时则隐有风雷之声、云雨之气，展开更有神龙奔出。"
           wds.类型 = "武器"
           wds.类别 = "扇"
           wds.等级 = 120
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 848845144
           wds.大模型资源 = 1782681953
        elseif wd == "秋水人家" then
           wds.说明 = "扇面上所画《秋水人家图》出神入化，栩栩如生；展扇更可进入其中，游山玩水，身临其境。其中定有道家仙法无疑。"
           wds.类型 = "武器"
           wds.类别 = "扇"

           wds.等级 = 130
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2332586416
           wds.大模型资源 = 1923582221
        elseif wd == "逍遥江湖" then
           wds.说明 = "持此扇行走江湖，可预先知晓福祸吉凶。据说是钦天监台正先生袁天罡书奇门妙法于其上，方使此扇有逍遥美名。"
           wds.类型 = "武器"
           wds.类别 = "扇"

           wds.等级 = 140
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1361205238
           wds.大模型资源 = 1457973706
        elseif wd == "浩气长舒" then
           wds.说明 = "持之若登高望远，五湖四海俱在眼前。不以物喜，不以己悲，心旷神怡，宠辱偕忘。感浩然之气至大至刚，以直养而无害，充盈天地。不亦快哉！"
           wds.类型 = "武器"
           wds.类别 = "扇"

           wds.等级 = 150
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3595165574
           wds.大模型资源 = 2725529530
        elseif wd == "星瀚" then
           wds.说明 = "三垣中坠，不知其故。展则漫天星辰失色，收则幽渺万籁无声。万顷星瀚，无始无终，无他无我，怎不使人目眩神驰？"
           wds.类型 = "武器"
           wds.类别 = "扇"

           wds.等级 = 160
           wds.角色限制 = "逍遥生,龙太子"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2182063168
           wds.大模型资源 = 1649810470
 --------------------------------鞭
        elseif wd == "牛皮鞭" then
            wds.说明 = "用牛皮制的皮鞭，很容易得到的练习用品"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1637717692
            wds.大模型资源 = 799104265
        elseif wd == "牛筋鞭" then
            wds.说明 = "以上等牛筋制成，弹性极佳，抽之有破空之声"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 156247531
            wds.大模型资源 = 2779337532
        elseif wd == "乌龙鞭" then
            wds.说明 = "以鲨鱼皮制成，表皮毛糙，极其坚韧"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3122588387
            wds.大模型资源 = 3141830232
        elseif wd == "钢结鞭" then
            wds.说明 = "以细钢环联结起一个个钢条，柔中带刚"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3232417863
            wds.大模型资源 = 2274385367
        elseif wd == "蛇骨鞭" then
            wds.说明 = "结似蛇骨，环环相联，抖动时骨节相撞，噼啪作响"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2780394861
            wds.大模型资源 = 2403200026
        elseif wd == "玉竹金铃" then
            wds.说明 = "由玉制的竹节联结而成,节间缚着金铃,铃随鞭动,声声悦耳"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 50
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 462481146
            wds.大模型资源 = 478344459
        elseif wd == "青藤柳叶鞭" then
            wds.说明 = "采深山古藤制成，上缚柳叶刀片，刀光闪动，杀气逼人"
            wds.类型 = "武器"
            wds.类别 = "鞭"
            wds.等级 = 60
            wds.角色限制 = "英女侠,狐美人"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 962191784
            wds.大模型资源 = 996954971
        elseif wd == "雷鸣嗜血鞭" then
           wds.说明 = "管节空洞，银钩闪闪，挥动时隐隐有风雷之音"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 70
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1854489623
           wds.大模型资源 = 3234780066
        elseif wd == "混元金钩" then
           wds.说明 = "混元钢索之上缚有金钩，追命勾魂"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 =  100
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 0x6182C9F2
           wds.大模型资源 = 0x8AB6DF4B
        elseif wd == "龙筋" then
           wds.说明 = "以恶龙之筋制成，挥舞若龙腾"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 90
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 1054575251
           wds.大模型资源 = 2303382990
        elseif wd == "百花" then
           wds.说明 = "龙藤塑其筋，百花其上，集仙灵之气，香飘四海"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 =  100
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3472315232
           wds.大模型资源 = 1331779470
        elseif wd == "吹雪" then
           wds.说明 = "鞭上石晶经千年生长，成雪花密布，映日月之光"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 =  100
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3293208513
           wds.大模型资源 = 1657462890
        elseif wd == "游龙惊鸿" then
           wds.说明 = "此鞭舞之矫如游龙，翩若惊鸿，宛如活物。据传是因仙家点化，方有此功效。"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 120
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 722618703
           wds.大模型资源 = 3699553560
        elseif wd == "仙人指路" then
           wds.说明 = "此鞭之奇，在于打斗时会自寻敌之弱点，或攻或守，或点或缠，如仙人指路，变化万端；又如灵蛇狂舞，眼花缭乱。"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 130
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 2337416369
           wds.大模型资源 = 3100047545
        elseif wd == "血之刺藤" then
           wds.说明 = "此鞭遍体生有倒刺，散发黑色邪气。据传打斗之时会如巨蟒缠住敌人，倒刺嵌入敌人体内，吸食其血肉。"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 140
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3451408733
           wds.大模型资源 = 3574831487
        elseif wd == "牧云清歌" then
           wds.说明 = "此鞭具大神通，持之则神虑清明，可驭使万物。苍穹为牧野，云霞为羔犊，驱鞭行九天，驭风乘云，竞雷逐电。"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 150
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 422685665
           wds.大模型资源 = 4192744388
        elseif wd == "霜陨" then
           wds.说明 = "昆仑之巅有涧曰冰溪，灿然如银，凝霜聚雪。溪流九曲，矫然若蛟，后凝为软玉长鞭，姑射仙子倚为护身仙器。"
           wds.类型 = "武器"
           wds.类别 = "鞭"
           wds.等级 = 160
           wds.角色限制 = "英女侠,狐美人"
           wds.资源 = "ZY.FT"
           wds.小模型资源 = 3829142397
           wds.大模型资源 = 522032539
 -------------------------------巨剑
        elseif wd == "钝铁重剑" then
            wds.说明 = "废铁弃疗打造出来的重剑，价格低廉，适宜新手。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"
            wds.等级 = 0
            wds.价格 = 500
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4219998266
            wds.大模型资源 = 3460607829
       elseif wd == "桃印铁刃" then
            wds.说明 = "常见铁剑，以桃木为剑柄。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3651866845
            wds.大模型资源 = 151833112
        elseif wd == "赭石巨剑" then
            wds.说明 = "以赭石去杂质锻淬而成，色泽沉郁。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1525417198
            wds.大模型资源 = 2861515391
        elseif wd == "壁玉长铗" then
            wds.说明 = "精铁重剑，剑身嵌美玉。碧波流光，颇有君子之风。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x51105074
            wds.大模型资源 = 0x976BFD27
        elseif wd == "青铜古剑" then
            wds.说明 = "青铜宝剑，古朴大气，然剑锋凌厉，不可小觑。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x809D1FDD
            wds.大模型资源 = 0xCC013376
        elseif wd == "金错巨刃" then
            wds.说明 = "铁匠采丹阳铜矿冶炼锻造，剑身以镶金包裹，工艺精湛，灼灼夺目。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 50
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xBD5C010E
            wds.大模型资源 = 0xE8F34110
        elseif wd == "惊涛雪" then
            wds.说明 = "铸剑师偶得深海晶石一块，冶炼成剑，挥斩之时，流光溢彩。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 60
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x1F648228
            wds.大模型资源 = 0x0B0AB6FB
        elseif wd == "醉浮生" then
            wds.说明 = "浮生若梦，一醉解忧，以布裹之，掩锋芒，平杀气，大巧不工，颇具豪侠之意。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 70
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x379213E4
            wds.大模型资源 = 0xE98BD873
        elseif wd == "沉戟天戊" then
            wds.说明 = "原为沙场断戟，后经人收敛重铸，以缅忠魂。握之，勇武无惧。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 80
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x2E58E991
            wds.大模型资源 = 0xE7692943
        elseif wd == "鸦九" then
            wds.说明 = "剑痴鸦九得欧冶子所遗铸剑秘法，铸长剑无名。因不舍爱剑磨损，剑成当日。即以冰匣封之。十余年后得人点拔开悟，欲拔剑出匣，然而冰匣早已与剑芯融为一体，不可分离，寻得熔岩炼化三年，方重铸此剑，剑身自此红雾缭绕。后人得之，为此剑冠以剑痴鸦九之名。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 90
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xEE633A71
            wds.大模型资源 = 0x3ADE1603
        elseif wd == "昆吾" then
            wds.说明 = "前朝君主雄才大略，文治武功皆成鼎盛，有邦国朝献天降陨铁。以为祥瑞，遂寻当世铸剑高手，炼化成剑，彪炳功勋。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 =  100
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x458E5B98
            wds.大模型资源 = 0xF9D8AF7B
        elseif wd == "弦歌" then
            wds.说明 = "乐师师旷以剑为胆，棹伏羲木，铺天蚕丝，制成琴身重剑，以酬知己，友人闻弦歌而知雅意，奏高山流水，成一段佳话。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 110
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7B631424
            wds.大模型资源 = 0x2952AF91
        elseif wd == "墨骨枯麟" then
            wds.说明 = "东海龙冢有龙骨万千，尤以黑龙龙骨为尊，取其残骨，熔以业火，成墨骨重剑。弹触剑身，争鸣作响，似有龙吟。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 120
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x84131194
            wds.大模型资源 = 0xEDAE8F1A
        elseif wd == "腾蛇郁刃" then
            wds.说明 = "中山之州有赤目飞蛇，乘雾游云，于柴桑之山嬉戏，疲乏后盘松石小憩，留蛇蜕一副，化为巨剑，此乃王道之师。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 130
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x6F6CAC00
            wds.大模型资源 = 0x9B3ACC82
        elseif wd == "秋水澄流" then
            wds.说明 = "泑水之畔盛产婴垣之玉，得蓐收赤足踏过而成金秋之色，胜碧玉琼瑶。有人界巧匠偶得之，不知何物，投剑炉一试，不想即刻炉毁剑成，更一日入秋，遂得此名。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 140
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x5F69A15B
            wds.大模型资源 = 0x60665CA2
        elseif wd == "百辟镇魂" then
            wds.说明 = "千年以前，世间妖魔横行，有神将除妖邪，斩厉鬼，力竭而亡，然其精魂不灭，附于生前所用神兵重剑，继续护佑四方。此剑祭出，则正气浩然冲天，可诛邪平患，可定历熄灾。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 150
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xF88C7462
            wds.大模型资源 = 0x18605DF5
        elseif wd == "长息" then
            wds.说明 = "洪水滔天，鲧窃帝之息壤以堙洪水，息壤生生不息，却于疏洪无益。帝令祝融取息壤，复命禹布土疏洪，以定九州。此时山川逼仄，水塞不出。禹借祝融火龙之力，熔炼息壤成重剑之体，以之开山疏浚，方江河倾泻，洪水东去。"
            wds.类型 = "武器"
            wds.类别 = "巨剑"

            wds.等级 = 160
            wds.角色限制 = "偃无师"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x14412394
            wds.大模型资源 = 0x50C65EB2
 -------------------------------灯笼
        elseif wd == "素纸灯" then
            wds.说明 = "轻巧的纸灯笼，最适合新手练习使用。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 0
            wds.价格 = 500
            wds.等级 = 0
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC2554C5A
            wds.大模型资源 = 0x053A2EC4
        elseif wd == "竹骨灯" then
            wds.说明 = "以竹骨扎成的灯笼，看起来精致又风雅。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x3EC471A0
            wds.大模型资源 = 0xEA6DAE59
        elseif wd == "红灯笼" then
            wds.说明 = "节日时方可悬挂起来的红灯笼，看到它就能感到喜庆的氛围。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC7DFDFEE
            wds.大模型资源 = 0x38E5EA0B
        elseif wd == "鲤鱼灯" then
            wds.说明 = "仿照锦鲤的样子精致而成的花灯，说不定会和锦鲤一样带给人好运呢！"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x97F6C50B
            wds.大模型资源 = 0x97DB902B
        elseif wd == "芙蓉花灯" then
            wds.说明 = "晶莹剔透的白瓷灯笼，更有名家所绘之工笔芙蓉，美轮美奂。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xCD622A06
            wds.大模型资源 = 0x7E3F070B
        elseif wd == "如意宫灯" then
            wds.说明 = "宫廷巧匠耗费月余精制而成的宫灯，象征着平安吉祥，如意你心。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 50
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xB4FBA38A
            wds.大模型资源 = 0xCE867B66
        elseif wd == "玲珑盏" then
            wds.说明 = "西域工匠以 费盘斯 工艺烧制的灯笼，其色流云清彩，其质晶莹剔透，玲珑奇巧，引人注目。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 60
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x97987F5C
            wds.大模型资源 = 0xA3B8F8E5
        elseif wd == "玉兔盏" then
            wds.说明 = "白璧无瑕琢成团绒玉兔，碧玉青翠雕琢剔透环珮，光润莹然，美不胜收。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 70
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD1839465
            wds.大模型资源 = 0x9C3DDB33
        elseif wd == "冰心盏" then
            wds.说明 = "北俱寒冰，澄澈剔透，经年不化。炽焰为刃，琢成玲珑灯盏，寒光为焰，凛风逼人。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 80
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x06427E44
            wds.大模型资源 = 0xF2575943
        elseif wd == "蟠龙" then
            wds.说明 = "风从龙出，云有龙息，夜放光华，金碧辉煌。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 = 90
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xA2DC05D6
            wds.大模型资源 = 0x73DE6960
        elseif wd == "云鹤" then
            wds.说明 = "饥餐瑶草，渴饮琼泉。朝飞阆苑，暮宿云烟。白鹤雪羽为灯盏，光耀如雪，不惹尘埃。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"

            wds.等级 =  100
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x5470130D
            wds.大模型资源 = 0x7B377B2F
        elseif wd == "风荷" then
            wds.说明 = "清莲出水，玉立亭亭。其灵性卓然者，化为灵仙，凡胎肉身，则凝为灯盏。含苞欲放，清芬十里。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 110
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xE61D828A
            wds.大模型资源 = 0x03F289DB
        elseif wd == "金风玉露" then
            wds.说明 = "今日云軿渡鹊桥，应非脉脉与迢迢。灵鹊仙羽，织就金凤，七夕月华，酿作玉露。皎皎华光，可照良人。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 120
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD9D63E3A
            wds.大模型资源 = 0x56ECB65C
        elseif wd == "凰火燎原" then
            wds.说明 = "凰与凤，生相伴，死同归。一方离世，则生者必引颈而鸣，唤涅槃之火，双双浴火重生。此火五百年不灭，奔腾炽烈，可焚八方。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 130
            wds.角色限制 = "桃夭夭"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x9129C027
            wds.大模型资源 = 0x9DD18309
        elseif wd == "月露清愁" then
            wds.说明 = "三界之西，有湖曰灵，湖水乃天女眼泪所化，夜色深浓时，有溶溶光华。水滴如泪，泪干为夜明灵珠，风流藴籍，清光冷冷。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 140
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7DB16375
            wds.大模型资源 = 0x17020397
        elseif wd == "夭桃侬李" then
            wds.说明 = "桃之夭夭，灼灼其华。烂漫春光，转瞬即逝。九重天上仙娥取之以为灯盏，光华所至处，枯木吐芽，桃李争芳。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 150
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x71FEDD98
            wds.大模型资源 = 0xEAEFA73D
        elseif wd == "荒尘" then
            wds.说明 = "星河浩瀚，繁星若尘，亿万星尘聚沙为塔，是以沧海桑田，人世幻变，其光华永恒也。"
            wds.类型 = "武器"
            wds.类别 = "灯笼"
            wds.等级 = 160
            wds.角色限制 = "桃夭夭"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x889E6AB7
            wds.大模型资源 = 0xFEEAD4B6
 -------------------------------伞
        elseif wd == "油纸伞" then
            wds.说明 = "普通的油纸伞，美观轻巧，最适合新手使用。"
            wds.类型 = "武器"
            wds.类别 = "伞"
            wds.等级 = 0
            wds.价格 = 500
            wds.等级 = 0
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x75285B05
            wds.大模型资源 = 0x63C38A86
        elseif wd == "红罗伞" then
            wds.说明 = "当下最时兴的雨伞样式，正红色的罗缎伞面看起来甚是喜庆。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 10
            wds.价格 = 1000
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xABC1FF05
            wds.大模型资源 = 0x1485AABF
        elseif wd == "紫竹伞" then
            wds.说明 = "伞骨及伞柄皆是紫竹制成，非常坚韧。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 20
            wds.价格 = 5000
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7EB4CE4A
            wds.大模型资源 = 0x28EEC31C
        elseif wd == "锦绣椎" then
            wds.说明 = "巧手绣娘，择上品丝绸，飞针走线，历时年逾，方成此伞，设色精妙，光彩夺目。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 30
            wds.价格 = 9000
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x38E84E48
            wds.大模型资源 = 0xA0A62144
        elseif wd == "幽兰帐" then
            wds.说明 = "本为一尺无暇素锦，不慎沾染墨痕，幸得雅士泼墨挥毫，绘幽兰一枝，其清俊姿态，栩栩如生，其清雅香气，扑鼻欲来。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 40
            wds.价格 = 14000
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x898B70A3
            wds.大模型资源 = 0xF2391CAB
        elseif wd == "琳琅盖" then
            wds.说明 = "《般若经》有七宝：乃金、银、琉璃、珊瑚、琥珀、砗磲、玛瑙。能工巧匠以金银为骨，饰以琉璃、砗磲之属，方成此伞。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 50
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD105662F
            wds.大模型资源 = 0x38B4CA6B
        elseif wd == "孔雀羽" then
            wds.说明 = "西域巧匠，取珍兽孔雀之尾羽，以玄妙秘法织造而成，碧彩闪烁，金翠辉煌。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 60
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xFEBE2460
            wds.大模型资源 = 0x24BD153A
        elseif wd == "金刚伞" then
            wds.说明 = "金刚怒目，降妖四魔。金刚伞以精钢百炼而成，铁骨刚叶，坚不可摧。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 70
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xCC7106BF
            wds.大模型资源 = 0x4FD7BCC4
        elseif wd == "落梅伞" then
            wds.说明 = "北俱苦寒之地，生千年老梅。一甲子方开花一次，色若胭脂，香欺兰蕙。梅枝落雪上，苦心雕琢，方成此伞。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 80
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC0151FE6
            wds.大模型资源 = 0x25E301EE
        elseif wd == "鬼骨" then
            wds.说明 = "昆山有玉，性阴厉，蓄百年煞气，幻为枯骨，谓之鬼玉。此玉为伞骨，伞下阴魂环绕，寒气森然。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 90
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC1D8FB19
            wds.大模型资源 = 0x5CBC9754
        elseif wd == "云梦" then
            wds.说明 = "高唐有梦，水阔花飞，梦有神女取巫山之风云为伞，谓之云梦。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 =  100
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x9DF0BB9B
            wds.大模型资源 = 0x32DC87DD
        elseif wd == "枕霞" then
            wds.说明 = "瑶池有仙蝶，枕霞沐云而生，其态纤巧翩然，其色艳若丹霞，取其羽翼为伞，霞光云影，如在目前。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 110
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x28771B6D
            wds.大模型资源 = 0x4FBB679F
        elseif wd == "碧火琉璃" then
            wds.说明 = "金蛇之鳞，可萃命魂之焰。以百枚金蛇之鳞，取百鬼命魂之火，成此伞。碧火幽幽，杀气腾腾，此伞过处，百鬼夜哭，绕梁不绝。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 120
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xEB48DDFB
            wds.大模型资源 = 0x284DD72B
        elseif wd == "雪羽穿云" then
            wds.说明 = "昆仑山中，有仙禽雪鸢，振翅可穿云，歌有裂石音，灵气所钟，位列仙班，其羽裳化伞，持此者，身轻如燕，振翅可至九天。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 130
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD189B9E5
            wds.大模型资源 = 0x14945888
        elseif wd == "月影星痕" then
            wds.说明 = "汲天河之水，取星河之光。凝月华之露，锻千日，成此伞。其光灿然，其色清灵，伞起时，月华照人，清辉泻地。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 140
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x8E5D6A40
            wds.大模型资源 = 0x45CB2F4A
        elseif wd == "浮生归梦" then
            wds.说明 = "三生河畔，彼岸花开，殷红似火，灼灼其华。曼珠沙华一株，藏浮生忆念一世。幽梦为瓣，情丝若蕊，墨蝶翩翩，归梦何时？"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 150
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xE1EC0610
            wds.大模型资源 = 0x2DF481E2
        elseif wd == "晴雪" then
            wds.说明 = "燕山雪，大如席，有华胥之女幽娴娇丽，踏凌云风波，乘逐月之辇，辇有华盖，正是燕山晴雪，清光冷冷，不可逼视。"
            wds.类型 = "武器"
            wds.类别 = "伞"

            wds.等级 = 160
            wds.角色限制 = "鬼潇潇"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x8020E83B
            wds.大模型资源 = 0x4C2F3E26

 ------------------装备---- 男帽
        elseif wd == "方巾" then
            wds.说明 = "普通人常带的帽子"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
             wds.等级 = 0
            wds.价格 = 500
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4205295661
            wds.大模型资源 = 266830706----------
        elseif wd == "布帽" then
            wds.说明 = "布制，普通人家使用的头冠"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 10
            wds.价格 = 1000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2131362658
            wds.大模型资源 = 1813515840----------
        elseif wd == "面具" then
            wds.说明 = "用皮制的面具，可以起到防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 20
            wds.价格 = 5000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1622680174
            wds.大模型资源 = 544766765
        elseif wd == "纶巾" then
            wds.说明 = "真丝织成的白色头巾，可以起到防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 30
            wds.价格 = 9000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1730098396
            wds.大模型资源 = 2497291232
        elseif wd == "缨络丝带" then
            wds.说明 = "红色的缨络结成的丝带，可以起到防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 40
            wds.价格 = 14000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1901332626
            wds.大模型资源 = 1222266422
        elseif wd == "羊角盔" then
            wds.说明 = "以羊角为装饰的盔，有非常强的防御力"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 50
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 48596533
            wds.大模型资源 = 1115249525
        elseif wd == "水晶帽" then
            wds.说明 = "水晶制成的帽子，因为坚硬，有相当强的防御力"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 60
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 606790429
            wds.大模型资源 = 2525896057
        elseif wd == "乾坤帽" then
            wds.说明 = "天下名冠，暗藏乾坤玄机，有相当强的防御力"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 70
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1272695123
            wds.大模型资源 = 1771271296
        elseif wd == "黑魔冠" then
            wds.说明 = "魔王妖王常戴的冠帽，有相当强的防御力"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 80
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 685770468
            wds.大模型资源 = 1237041059
        elseif wd == "白玉龙冠" then
            wds.说明 = "高贵雅致的冠帽，镶有上等白玉精雕之飞龙图案"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 90
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4284490218
            wds.大模型资源 = 1412110631
        elseif wd == "水晶夔帽" then
            wds.说明 = "其状如牛,苍色无角,一足能走,出入水即风雨,目光如日月,其声如雷,名曰夔。黄帝杀之,取皮以冒鼓,声闻五百里。”相传远古奇兽夔牛皮最终为一能工巧匠所得，辅以水晶玉石经上经千锤百炼雕琢编制而成一件冠帽。据传此帽拥有奇兽的庇佑，千年不坏"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 =  100
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 77382880
            wds.大模型资源 = 2164041151
        elseif wd == "翡翠曜冠" then
            wds.说明 = "相传为三清之一灵宝天尊佩戴过的帽冠。七颗翡翠分别代表“日、月、火、水、金、木、土”环绕在玉石宝珠上。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 110
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 482138369
            wds.大模型资源 = 830308889
        elseif wd == "金丝黑玉冠" then
            wds.说明 = "远古盘古开天辟地，混沌初开时凝结而成的玉石雕琢而成的冠帽，充满着神奇的灵气。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 120
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1140741771
            wds.大模型资源 = 948281107
        elseif wd == "白玉琉璃冠" then
            wds.说明 = "太上老君曾佩戴过的冠帽，用仙界白玉与琉璃珠辅以天丝编织而成。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 130
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 778966943
            wds.大模型资源 = 243238969
        elseif wd == "兽鬼珐琅面" then
            wds.说明 = "上古神魔所戴，虽经几千年，面具独角四周仍散发着神奇的魔力。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 140
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2310107289
            wds.大模型资源 = 125393860
        elseif wd == "紫金磐龙冠" then
            wds.说明 = "火神遗留之物。冠上用金雕琢为吉龙盘云，四周散发着神奇的力量。需要有一定修为之人方能佩戴。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 150
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 447473455
            wds.大模型资源 = 3859378496
        elseif wd == "浑天玄火盔" then
            wds.说明 = "黄帝征伐四方之时所配战盔，刀枪不入，水火不侵，瘟邪难近，更添无尽战意。"
            wds.类型 = "头盔"
            wds.类别 = "头盔"
            wds.等级 = 160
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3566352403
            wds.大模型资源 = 1502470676
 ---------------------- 女帽
        elseif wd == "簪子" then
            wds.说明 = "女孩子们喜欢的簪子，可以起到防御作用"
            wds.类型 = "头盔"
            wds.等级 = 0
            wds.价格 = 500
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1004476789
            wds.大模型资源 = 2338230752
        elseif wd == "玉钗" then
            wds.说明 = "用玉制成的钗，可以起到防御作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 10
            wds.价格 = 1000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 509457918
            wds.大模型资源 = 3519518702
        elseif wd == "梅花簪子" then
            wds.说明 = "因簪子外端有梅花形状而得名，可以起到防御作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 20
            wds.价格 = 5000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2353187566
            wds.大模型资源 = 3355592593
        elseif wd == "珍珠头带" then
            wds.说明 = "珍珠结成的头带，非常好看女性饰品，可以起到防御作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 30
            wds.价格 = 9000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3660503604
            wds.大模型资源 = 1074719537
        elseif wd == "凤头钗" then
            wds.说明 = "雕刻着凤头的金钗，可以起到防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 40
            wds.价格 = 14000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1305963475
            wds.大模型资源 = 1323749060
        elseif wd == "媚狐头饰" then
            wds.说明 = "狐眼嵌着宝石的狐头帽，魔女们喜爱的头饰"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 50
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 413224440
            wds.大模型资源 = 798712466
        elseif wd == "玉女发冠" then
            wds.说明 = "仙女常带的头环，有相当强的防御力"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 60
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3770740399
            wds.大模型资源 = 438307144
        elseif wd == "魔女发冠" then
            wds.说明 = "魔女们常带的头环，可以起到非常强的防御作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 70
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2508308579
            wds.大模型资源 = 845102994
        elseif wd == "七彩花环" then
            wds.说明 = "用七彩神花编成的花环，可以起到极强防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 80
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3183056480
            wds.大模型资源 = 2570521405
        elseif wd == "凤翅金翎" then
            wds.说明 = "以凤翅和凤翎编织而成的金丝头环，有极强防御的作用"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 90
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2373769150
            wds.大模型资源 = 3600964026
        elseif wd == "寒雉霜蚕" then
            wds.说明 = "由寒玉以及蚕丝编制而成，相传为七仙女常戴之物"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 =  100
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 86765519
            wds.大模型资源 = 42155348
        elseif wd == "曜月嵌星" then
            wds.说明 = "瑶池圣母所戴之物。冠上镶有七星宝珠环绕在月光石周围形成七星曜月的图案。"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 110
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3373770544
            wds.大模型资源 = 2683232345
        elseif wd == "郁金流苏簪" then
            wds.说明 = "白晶晶佩戴的发髻。白色玉石与金丝编织而成。"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 120
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3156867369
            wds.大模型资源 = 2529030399
        elseif wd == "玉翼附蝉翎" then
            wds.说明 = "嫦娥仙女佩戴过的帽冠。将月宫中玉石经千年雕琢而成，羽翼薄如蝉翼"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 130
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 678644164
            wds.大模型资源 = 3321477618
        elseif wd == "鸾羽九凤冠" then
            wds.说明 = "九只五彩凤凰的羽翼与金丝编织而成的冠帽。"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 140
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1580187502
            wds.大模型资源 = 2242355262
        elseif wd == "金铛紫焰冠" then
            wds.说明 = "水神遗留之物。上古神兽凤凰羽翼制成的帽冠，四周散发着神奇的力量。需要有一定修为之人方能佩戴。"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 150
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3789475693
            wds.大模型资源 = 3128529272
        elseif wd == "乾元鸣凤冕" then
            wds.说明 = "有感于女娲造人之德，三界名匠合力铸造献上的帝王冠冕。五德齐聚，凤仪天下。"
            wds.类型 = "头盔"
            wds.类别 = "发钗"
            wds.等级 = 160
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2064393561
            wds.大模型资源 = 364230466
 ---------------------- 男衣
        elseif wd == "布衣" then
            wds.说明 = "普通的布衣，可以起到防御的作用"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.性别限制 = "男"
            wds.等级 = 0
            wds.价格 = 500
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2686301321
            wds.大模型资源 = 2162639125
        elseif wd == "皮衣" then
            wds.说明 = "用兽皮制的皮衣，防御力较布衣要好的多"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 10
            wds.价格 = 1000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1569129428
            wds.大模型资源 = 4004133926
        elseif wd == "鳞甲" then
            wds.说明 = "用铁鳞制成的铠甲，有相当的防御力"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 20
            wds.价格 = 5000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 391851478
            wds.大模型资源 = 2281395560
        elseif wd == "锁子甲" then
            wds.说明 = "胸部是铁环，如铁锁加身，防御力相当不错"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 30
            wds.价格 = 9000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2078226169
            wds.大模型资源 = 2963205363
        elseif wd == "紧身衣" then
            wds.说明 = "紧身的皮衣，非常轻便，有不错的防御力"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 40
            wds.价格 = 14000
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2338366605
            wds.大模型资源 = 4031115015
        elseif wd == "钢甲" then
            wds.说明 = "用精钢制成的铠甲，防御能力较强"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 50
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1672609844
            wds.大模型资源 = 1862358389
        elseif wd == "夜魔披风" then
            wds.说明 = "来自阴间的披风，隐隐透出神秘的幽冥气息"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 60
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2191193414
            wds.大模型资源 = 1388622524
        elseif wd == "龙骨甲" then
            wds.说明 = "以龙骨制成的宝甲，有惊人的防御力"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 70
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 541785355
            wds.大模型资源 = 1741806040
        elseif wd == "死亡斗篷" then
            wds.说明 = "魔王妖王梦寐以求的防具，非常有价值"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 80
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1766901621
            wds.大模型资源 = 1885524269
        elseif wd == "神谕披风" then
            wds.说明 = "受到神谕祝福的银色披风，带有灵动之气"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 90
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1771196770
            wds.大模型资源 = 1105958467
        elseif wd == "珊瑚玉衣" then
            wds.说明 = "相传为龙王所用铠甲，穿上后能获得仙界的庇护。"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 =  100
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2065701734
            wds.大模型资源 = 2474213076
        elseif wd == "金蚕披风" then
            wds.说明 = "由千年玉桑上的金蚕吐丝为材料缝制而成的衣甲，有极强的韧性。"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 110
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2152164595
            wds.大模型资源 = 1839718700
        elseif wd == "乾坤护心甲" then
            wds.说明 = "为道教流传的宝物，内有八卦乾坤玄机，能抵御极强的伤害。"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 120
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3449582254
            wds.大模型资源 = 3993401193
        elseif wd == "蝉翼金丝甲" then
            wds.说明 = "月宫桂树上的灵蝉身上的蝉翼辅以金丝编织而成的铠甲。绝非寻常仙家之物"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 130
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3962403816
            wds.大模型资源 = 60903505
        elseif wd == "金丝鱼鳞甲" then
            wds.说明 = "水神遗留之物。时隔千年，依然充满着灵气。"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 140
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1521396689
            wds.大模型资源 = 340629621
        elseif wd == "紫金磐龙甲" then
            wds.说明 = "相传为黄帝的遗物。紫金磐龙，四周散发着神奇的力量"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 150
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 400462349
            wds.大模型资源 = 54627476
        elseif wd == "混元一气甲" then
            wds.说明 = "混元一气所凝成的宝甲，至坚不摧之物，可与天地同寿"
            wds.类型 = "衣服"
            wds.类别 = "男衣"
            wds.等级 = 160
            wds.性别限制 = "男"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1802888376
            wds.大模型资源 = 3052290356
 ---------------------- 女衣
        elseif wd == "布裙" then
            wds.说明 = "普通的布裙，女子用，可以起到防御的作用"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.性别限制 = "女"
             wds.等级 = 0
            wds.价格 = 500
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2415237558
            wds.大模型资源 = 4137686182
        elseif wd == "丝绸长裙" then
            wds.说明 = "用名贵丝绸制成的长裙，女子用，可以起到防御作用"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 10
            wds.价格 = 1000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1759000550
            wds.大模型资源 = 451022751
        elseif wd == "五彩裙" then
            wds.说明 = "五彩绵制成的裙子，可以起到一定的防御作用"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 20
            wds.价格 = 5000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2890413629
            wds.大模型资源 = 3454325495
        elseif wd == "龙鳞羽衣" then
            wds.说明 = "用龙鳞制成的羽衣，有相当的防御力"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 30
            wds.价格 = 9000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4057563157
            wds.大模型资源 = 2734928170
        elseif wd == "天香披肩" then
            wds.说明 = "瑶台玉女穿的披肩，有相当的防御力"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 40
            wds.价格 = 14000
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3709034410
            wds.大模型资源 = 3803079314
        elseif wd == "金缕羽衣" then
            wds.说明 = "用金丝缝制的羽衣，可以起到一定的防御作用"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 50
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 995989694
            wds.大模型资源 = 4188048378
        elseif wd == "霓裳羽衣" then
            wds.说明 = "非常美丽的羽衣，可以起到一定的防御作用"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 60
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3005484634
            wds.大模型资源 = 2665296729
        elseif wd == "流云素裙" then
            wds.说明 = "相传是织女用流云织成的裙子，有很非常的防御力"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 70
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3322971644
            wds.大模型资源 = 1771157433
        elseif wd == "七宝天衣" then
            wds.说明 = "所谓天衣无缝，上有七宝华文，是可遇不可求的极品"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 80
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4180182573
            wds.大模型资源 = 4034456555
        elseif wd == "飞天羽衣" then
            wds.说明 = "相传以玉女飞天时所着衣装，受到天地精华的凝炼"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 90
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2637194038
            wds.大模型资源 = 1353325810
        elseif wd == "穰花翠裙" then
            wds.说明 = "翠玉宝珠装饰而成的镶花裙，为王母娘娘赏赐之物。"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 =  100
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 869673869
            wds.大模型资源 = 1836280211
        elseif wd == "金蚕丝裙 " then
            wds.说明 = "由千年玉桑上的金蚕吐丝为材料缝制而成的衣甲，有极强的韧性。"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 110
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2107840682
            wds.大模型资源 = 1711858998
        elseif wd == "紫香金乌裙" then
            wds.说明 = "汲取三界花魂而制成的裙子，穿上此裙香飘千里。"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 120
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2898176567
            wds.大模型资源 = 273903801
        elseif wd == "碧霞彩云衣" then
            wds.说明 = "收集了天界的碧霞五彩祥云编织而成的彩云衣，相传七仙女下凡时就是穿的这件。"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 130
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1972600201
            wds.大模型资源 = 2452196507
        elseif wd == "金丝蝉翼衫" then
            wds.说明 = "月宫桂树上的灵蝉身上的蝉翼，并取自瑶池的金丝，经仙界仙女七七四十九天编织而成的软甲"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 140
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 910126967
            wds.大模型资源 = 2753459745
        elseif wd == "五彩凤翅衣" then
            wds.说明 = "相传女娲娘娘所穿。穿上此衣能得到上古神灵的庇佑"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 150
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2784730653
            wds.大模型资源 = 4139222799
        elseif wd == "鎏金浣月衣" then
            wds.说明 = "以日月光芒所织造的霞衣，对敌之时往往令其目眩神驰"
            wds.类型 = "衣服"
            wds.类别 = "女衣"
            wds.等级 = 160
            wds.性别限制 = "女"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1359780053
            wds.大模型资源 = 2596354317
 ---------------------- 鞋子
        elseif wd == "布鞋" then
            wds.说明 = "布缝制的鞋子，可以起到防御的作用"
            wds.类型 = "鞋子"
            wds.资源 = "ZY.FT"
            wds.等级 = 0
            wds.价格 = 500
            wds.小模型资源 = 2418869371
            wds.大模型资源 = 2669010133
        elseif wd == "牛皮靴" then
            wds.说明 = "牛皮缝制的靴子，速度和防御作用都比较强"
            wds.类型 = "鞋子"
            wds.等级 = 10
            wds.价格 = 1000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4095390557
            wds.大模型资源 = 2232108129
        elseif wd == "马靴" then
            wds.说明 = "皮制的马靴，可以起到防御的作用"
            wds.类型 = "鞋子"
            wds.等级 = 20
            wds.价格 = 5000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3241248586
            wds.大模型资源 = 3542020689
        elseif wd == "侠客履" then
            wds.说明 = "非常强的鞋，速度和防御作用都属上品"
            wds.类型 = "鞋子"
            wds.等级 = 30
            wds.价格 = 9000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 32184820
            wds.大模型资源 = 558037323
        elseif wd == "神行靴" then
            wds.说明 = "非常轻便的靴子，可以起到防御的作用"
            wds.类型 = "鞋子"
            wds.等级 = 40
            wds.价格 = 14000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3963558258
            wds.大模型资源 = 1110999757
        elseif wd == "绿靴" then
            wds.说明 = "绿水晶制成的鞋子，防御作用非常的强"
            wds.类型 = "鞋子"
            wds.等级 = 50
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2631819456
            wds.大模型资源 = 3993409018
        elseif wd == "追星踏月" then
            wds.说明 = "非由于鞋上有云月图案而得名，有不错的防御力"
            wds.类型 = "鞋子"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1891477910
            wds.大模型资源 = 4017464926
        elseif wd == "九州履" then
            wds.说明 = "天下名履，有非常不错的防御力和速度"
            wds.类型 = "鞋子"
            wds.等级 = 70
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1192033620
            wds.大模型资源 = 1060649976
        elseif wd == "万里追云履" then
            wds.说明 = "可在任何地形疾走的神履，有不错的防御力可在任何地形疾走的神履，有不错的防御力"
            wds.类型 = "鞋子"
            wds.等级 = 80
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2928326681
            wds.大模型资源 = 1121748353
        elseif wd == "踏雪无痕" then
            wds.说明 = "包蓄地之灵气，穿之幻影如风，踏雪无痕"
            wds.类型 = "鞋子"
            wds.等级 = 90
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1140435077
            wds.大模型资源 = 888533789
        elseif wd == "平步青云" then
            wds.说明 = "寻常仙界之物，穿上此靴可平步青云，借风云飞腾。"
            wds.类型 = "鞋子"
            wds.等级 =  100
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1930941353
            wds.大模型资源 = 771313116
        elseif wd == "七星逐月靴" then
            wds.说明 = "天罡星所穿之靴。穿之及疾如风，三界之地任我遨游。"
            wds.类型 = "鞋子"
            wds.等级 = 130
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1623857733
            wds.大模型资源 = 2763463360
        elseif wd == "碧霞流云履" then
            wds.说明 = "相传天界神匠以五彩天霞为材料，缝制而成的靴子。"
            wds.类型 = "鞋子"
            wds.等级 = 140
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3450927651
            wds.大模型资源 = 1312286829
        elseif wd == "九州履" then
            wds.说明 = "相传是织女用流云织成的裙子，有很非常的防御力"
            wds.类型 = "鞋子"
            wds.等级 =  100
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1192033620
            wds.大模型资源 = 1060649976
        elseif wd == "追云逐电" then
            wds.说明 = "相传为雷公电母经过千万年炼化而成，穿上后可以追逐云电，日行万里。"
            wds.类型 = "鞋子"
            wds.等级 = 110
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1149904553
            wds.大模型资源 = 3062275612
        elseif wd == "乾坤天罡履" then
            wds.说明 = "相传为太上老君所穿。乃天地之气编织而成"
            wds.类型 = "鞋子"
            wds.等级 = 120
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1238537463
            wds.大模型资源 = 2413633901
        elseif wd == "九州履" then
            wds.说明 = "相传是织女用流云织成的裙子，有很非常的防御力"
            wds.类型 = "鞋子"
            wds.等级 = 140
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1192033620
            wds.大模型资源 = 1060649976
        elseif wd == "金丝逐日履" then
            wds.说明 = "相传神匠为纪念夸父特造此靴。穿上可追逐太阳，风云皆抛于脑后"
            wds.类型 = "鞋子"
            wds.等级 = 150
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 967718944
            wds.大模型资源 = 2734810061
        elseif wd == "辟尘分光履" then
            wds.说明 = "上古秘宝，着此履者可于星尘间穿梭，与霞光相竞逐"
            wds.类型 = "鞋子"
            wds.等级 = 160
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2907023969
            wds.大模型资源 = 3418242860
 ---------------------- 腰带
        elseif wd == "腰带" then
            wds.说明 = "一条普通的腰带，可以起到防御的作用"
            wds.类型 = "腰带"
            wds.等级 = 0
            wds.价格 = 500
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 581742663
            wds.大模型资源 = 1978200666
        elseif wd == "缎带" then
            wds.说明 = "高级丝绸制成的腰带，能起到不错的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 10
            wds.价格 = 1000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3002795508
            wds.大模型资源 = 4182781118
        elseif wd == "银腰带" then
            wds.说明 = "银制的腰带，可以起到防御的作用"
            wds.类型 = "腰带"
            wds.等级 = 20
            wds.价格 = 5000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3920753159
            wds.大模型资源 = 1636787949
        elseif wd == "水晶腰带" then
            wds.说明 = "佩带在腰间的水晶腰带，能起到不错的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 30
            wds.价格 = 9000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 588758831
            wds.大模型资源 = 684918234
        elseif wd == "玉树腰带" then
            wds.说明 = "玉树之丝绦制成的腰带，能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 30
            wds.价格 = 9000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1806472204
            wds.大模型资源 = 3616026443
        elseif wd == "琥珀腰链" then
            wds.说明 = "用琥珀制的腰链，能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 40
            wds.价格 = 14000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1085821316
            wds.大模型资源 = 4041311819
        elseif wd == "白面狼牙" then
            wds.说明 = "白色的神镜片，可束在腰间，能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 40
            wds.价格 = 14000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3575831350
            wds.大模型资源 = 3209666649
        elseif wd == "乱牙咬" then
            wds.说明 = "神铁制的腰箍，能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 50
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4081289026
            wds.大模型资源 = 1830358071
        elseif wd == "魔童大牙" then
            wds.说明 = "巨大的魔牙，系在腰间能起到极强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 50
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3917101017
            wds.大模型资源 = 246498213
        elseif wd == "攫魂铃" then
            wds.说明 = "来自阴间的魔铃，佩在腰间，能起到极强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3503405586
            wds.大模型资源 = 4282767598
        elseif wd == "双魂引" then
            wds.说明 = "来自阴间的宝物，可以佩在腰间，能起到极强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3493684718
            wds.大模型资源 = 2691281237
        elseif wd == "兽王腰带" then
            wds.说明 = "兽王曾使用过的腰饰，能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 70
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2946790593
            wds.大模型资源 = 281570977
        elseif wd == "百窜云" then
            wds.说明 = "神秘的缎带，系在腰间能起到较强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 70
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1956418448
            wds.大模型资源 = 457288512
        elseif wd == "八卦锻带" then
            wds.说明 = "暗藏八卦玄机的腰带，能起到极强的防御作用"
            wds.类型 = "腰带"
            wds.等级 = 80
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4238438306
            wds.大模型资源 = 3676875648
        elseif wd == "圣王坠" then
            wds.说明 = "相传为Saber的缠腰带,不如被谁偷走了,流入市场."
            wds.类型 = "腰带"
            wds.等级 = 80
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4161518362
            wds.大模型资源 = 2601988852
        elseif wd == "幻彩玉带" then
            wds.说明 = "以集天界灵气之幻彩宝玉制成，有强大的护佑作用"
            wds.类型 = "腰带"
            wds.等级 = 90
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2383008893
            wds.大模型资源 = 871942394
        elseif wd == "珠翠玉环" then
            wds.说明 = "集合千年地气生出的翡翠宝珠雕琢而成的腰带，有强大的庇佑作用"
            wds.类型 = "腰带"
            wds.等级 =  100
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4092702542
            wds.大模型资源 = 4023277032
        elseif wd == "金蟾含珠" then
            wds.说明 = "相传“蟾”能口吐金钱为旺财之物。由金身蟾口含夜光珠再辅以金丝编织而成的腰带，世间少有，绝非人界之物"
            wds.类型 = "腰带"
            wds.等级 = 110
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1436553780
            wds.大模型资源 = 415633459
        elseif wd == "乾坤紫玉带" then
            wds.说明 = "千年紫玉雕琢而成的腰带，相传为道家失传的宝物，有极强的庇佑作用"
            wds.类型 = "腰带"
            wds.等级 = 120
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1070699611
            wds.大模型资源 = 885196883
        elseif wd == "琉璃寒玉带" then
            wds.说明 = "天界银河之水经亿万年凝结而成的玉石制成的腰带，不为寻常仙物"
            wds.类型 = "腰带"
            wds.等级 = 130
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 761737729
            wds.大模型资源 = 290193038
        elseif wd == "蝉翼鱼佩带" then
            wds.说明 = "月宫桂树上的灵蝉身上的蝉翼以及金丝编织而成的腰带，所散发出来的灵气能抵御极强的伤害。"
            wds.类型 = "腰带"
            wds.等级 = 140
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1344450765
            wds.大模型资源 = 2406174850
        elseif wd == "磐龙凤翔带" then
            wds.说明 = "远古天神遗物，龙飞凤舞呈吉祥之态，佩戴起来能受到上古神兽的庇护。"
            wds.类型 = "腰带"
            wds.等级 = 150
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2563450676
            wds.大模型资源 = 4015638612
        elseif wd == "紫霄云芒带" then
            wds.说明 = "以云霞束起雷电所制成的腰带，会散发出闪电自行防御来袭的攻击。"
            wds.类型 = "腰带"
            wds.等级 = 160
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2824149864
            wds.大模型资源 = 2230161080
 ---------------------- 项链
        elseif wd == "护身符" then
            wds.说明 = "一块护身符，佩带在身上有一定法力作用"
            wds.类型 = "项链"
            wds.等级 = 0
            wds.价格 = 500
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2460748358
            wds.大模型资源 = 359787750
        elseif wd == "五色飞石" then
            wds.说明 = "仙人炼制的饰品，佩带在身上有极强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 10
            wds.价格 = 1000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3139664596
            wds.大模型资源 = 374271655
        elseif wd == "珍珠链" then
            wds.说明 = "一串珍珠，佩带在身上有一定法力作用"
            wds.类型 = "项链"
            wds.等级 = 20
            wds.价格 = 5000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2451318875
            wds.大模型资源 = 4232202616
        elseif wd == "苍魂珠" then
            wds.说明 = "据说珠里附有魂魄，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 30
            wds.价格 = 9000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4237594230
            wds.大模型资源 = 1893159766
        elseif wd == "骷髅吊坠" then
            wds.说明 = "皮绳上吊着一个骷髅，佩带在身上有一定法力作用"
            wds.类型 = "项链"
            wds.等级 = 30
            wds.价格 = 9000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3414243240
            wds.大模型资源 = 3509217536
        elseif wd == "九宫坠" then
            wds.说明 = "以九宫制成的坠子，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 40
            wds.价格 = 14000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1591157328
            wds.大模型资源 = 2581108802
        elseif wd == "江湖夜雨" then
            wds.说明 = "流传于中土的奇异饰物，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 40
            wds.价格 = 14000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3985913426
            wds.大模型资源 = 293361877
        elseif wd == "高速之星" then
            wds.说明 = "神秘的项圈，来历至今无人知晓"
            wds.类型 = "项链"
            wds.等级 = 50
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4175067704
            wds.大模型资源 = 1572566259
        elseif wd == "荧光坠子" then
            wds.说明 = "有荧光的宝石，佩带在身上有相当强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 50
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 240077184
            wds.大模型资源 = 4212393181
        elseif wd == "八卦坠" then
            wds.说明 = "以八卦制成的饰物，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1357232476
            wds.大模型资源 = 3364357037
        elseif wd == "风月宝链" then
            wds.说明 = "名家制成的精致饰品，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 91099687
            wds.大模型资源 = 1990243519
        elseif wd == "碧水青龙" then
            wds.说明 = "龙形环状可戴在项部，有相当的法力作用"
            wds.类型 = "项链"
            wds.等级 = 70
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2761659977
            wds.大模型资源 = 4002371974
        elseif wd == "鬼牙攫魂" then
            wds.说明 = "来自阴间的饰物，佩带在身上有较强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 70
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3973878954
            wds.大模型资源 = 2356109296
        elseif wd == "万里卷云" then
            wds.说明 = "据说是大禹留下的宝物，佩带在身上有极强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 80
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2814775714
            wds.大模型资源 = 3789092586
        elseif wd == "疾风之铃" then
            wds.说明 = "来历不详的神秘饰物，佩带在身上有极强的法力作用"
            wds.类型 = "项链"
            wds.等级 = 80
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1790391367
            wds.大模型资源 = 3931720117
        elseif wd == "七彩玲珑" then
            wds.说明 = "串有七彩玲珑的珍珠，集海之灵气，给予佩戴者祥瑞祝福"
            wds.类型 = "项链"
            wds.等级 = 90
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 61182840
            wds.大模型资源 = 1790107582
        elseif wd == "黄玉琉佩" then
            wds.说明 = "女娲补天遗留下的神石雕琢而成，拥有神奇的灵力。"
            wds.类型 = "项链"
            wds.等级 =  100
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4273718431
            wds.大模型资源 = 3596201129
        elseif wd == "鸾飞凤舞" then
            wds.说明 = "仙界仙女们常佩之物。呈祥瑞和平之气，定能给佩戴者带来好运。"
            wds.类型 = "项链"
            wds.等级 = 110
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 234019286
            wds.大模型资源 = 805892737
        elseif wd == "衔珠金凤佩" then
            wds.说明 = "相传曾为嫦娥仙子佩戴之物。此物寓示着美好的追求与向往。"
            wds.类型 = "项链"
            wds.等级 = 120
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1808237370
            wds.大模型资源 = 1175473770
        elseif wd == "七璜珠玉佩" then
            wds.说明 = "以七个天珠以及天丝编制而成的坠子。集天地之灵气，能给予佩戴者吉祥如意。"
            wds.类型 = "项链"
            wds.等级 = 130
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 150576745
            wds.大模型资源 = 3350488882
        elseif wd == "鎏金点翠佩" then
            wds.说明 = "玉皇大帝平常佩戴之物，集合了仙界之力，给予佩戴者神奇的魔力。"
            wds.类型 = "项链"
            wds.等级 = 140
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 91896384
            wds.大模型资源 = 2019827339
        elseif wd == "紫金碧玺佩" then
            wds.说明 = "上古神兽遗留之物。四颗碧玉珠似乎依附着神奇的魔力。"
            wds.类型 = "项链"
            wds.等级 = 150
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 782330888
            wds.大模型资源 = 2103726062
        elseif wd == "落霞陨星坠" then
            wds.说明 = "以陨落的星辰制成的宝玉，带有莫名的灵气与能量。"
            wds.类型 = "项链"
            wds.等级 = 160
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4104259920
            wds.大模型资源 = 1986550511
 -----------一级药
        elseif wd == "九香虫" then
            wds.说明="来自海外的仙草"
            wds.功效="恢复30点法力"
            wds.气血=30
            wds.价格=140
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC30AE6EF
            wds.大模型资源 = 0x1BF0DE4E-------------
        elseif wd == "鬼切草" then
            wds.说明="产自阴间的药草"
            wds.功效="恢复法力40点"
            wds.魔法=40
            wds.价格=200
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x4CE95CD4
            wds.大模型资源 = 0x405AE01C
        elseif wd == "山药" then
            wds.说明="常见的药草"
            wds.功效="恢复气血40，治疗伤势10点"
            wds.气血=40
            wds.伤势=10
            wds.价格=180
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x9D941C39
            wds.大模型资源 = 0xC0EC938B---------------
        elseif wd == "人参" then
            wds.说明="千年的老参，药性非常强"
            wds.功效="恢复气血80，治疗伤势20点"
            wds.气血=80
            wds.伤势=20
            wds.价格=400
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x15E4304F
            wds.大模型资源 = 0xF09CBA45-------------
        elseif wd == "八角莲叶" then
            wds.说明="八角形状的莲叶"
            wds.功效="恢复气血60，治疗伤势15点"
            wds.气血=60
            wds.伤势=15
            wds.价格=180
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x35C25C5F
            wds.大模型资源 = 0x2B46AADD----------
        elseif wd == "四叶花" then
            wds.说明="长了四片叶子的小花"
            wds.功效="恢复40点气血"
            wds.气血=40
            wds.价格=100
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2403345371
            wds.大模型资源 = 2730519933-------------
        elseif wd == "七叶莲" then
            wds.说明="七片叶子的莲花"
            wds.功效="恢复60点气血"
            wds.气血=60
            wds.价格=150
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1559451305
            wds.大模型资源 = 3792310897------------
        elseif wd == "天青地白" then
            wds.说明="产于北方的药草"
            wds.功效="恢复80点气血"
            wds.气血=80
            wds.价格=200
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2832513988
            wds.大模型资源 = 296513300-----------
        elseif wd == "紫丹罗" then
            wds.说明="产自东胜神洲的药草"
            wds.功效="恢复法力20点"
            wds.魔法=20
            wds.价格=80
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3370160386
            wds.大模型资源 = 1385083912----------
        elseif wd == "佛手" then
            wds.说明="常见的药草"
            wds.功效="恢复法力20点"
            wds.魔法=20
            wds.价格=80
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2548894140
            wds.大模型资源 = 383352299-------------
        elseif wd == "旋复花" then
            wds.说明="产于东海海滨的药草"
            wds.功效="恢复法力20点"
            wds.魔法=20
            wds.价格=80
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3005200763
            wds.大模型资源 = 1826111581-------------
        elseif wd == "百色花" then
            wds.说明="来自海外的仙草"
            wds.功效="恢复法力30点"
            wds.魔法=30
            wds.价格=140
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 542670697
            wds.大模型资源 = 2739363770--------------
        elseif wd == "香叶" then
            wds.说明="散发香气的药草叶子"
            wds.功效="恢复法力30点"
            wds.魔法=30
            wds.价格=140
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1613933236
            wds.大模型资源 = 4251066160-------------
        elseif wd == "龙须草" then
            wds.说明="相传是龙须所化的药草"
            wds.功效="恢复法力30点"
            wds.魔法=30
            wds.价格=140
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1050552246
            wds.大模型资源 = 2301092035---------
        elseif wd == "灵脂" then
            wds.说明="采天地灵气而生的药物"
            wds.功效="恢复法力40点"
            wds.魔法=40
            wds.价格=200
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2234254574
            wds.大模型资源 = 4179146971-----------
        elseif wd == "白玉骨头" then
            wds.说明="东方药兽的骨头"
            wds.功效="恢复法力40点"
            wds.魔法=40
            wds.价格=200
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3784788337
            wds.大模型资源 = 3455660261---------
        elseif wd == "曼佗罗花" then
            wds.说明="常用的药草"
            wds.功效="恢复法力50点"
            wds.魔法=50
            wds.价格=275
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1670376717
            wds.大模型资源 = 351908663--------
        elseif wd == "月见草" then
            wds.说明="生长在暗处的药草"
            wds.功效="恢复气血40，法力30点"
            wds.气血=40
            wds.魔法=30
            wds.伤势=20
            wds.价格=250
            wds.等级=1
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x23E1DE00
            wds.大模型资源 = 0x5F39ECF5---------
 ---------- 二级药
        elseif wd == "天不老" then
            wds.说明="蓝色花朵，有奇特的香气"
            wds.功效="恢复气血100点"
            wds.气血=100
            wds.价格=1500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA1292738
            wds.大模型资源 = 0x812F04D7
        elseif wd == "紫石英" then
            wds.说明="紫色的石英石，具有药性"
            wds.功效="恢复气血100点"
            wds.气血=100
            wds.价格=1500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x388CCEE9
            wds.大模型资源 = 0x8AC7636D--------
        elseif wd == "鹿茸" then
            wds.说明="南极仙鹿之茸"
            wds.功效="恢复气血150点"
            wds.气血=150
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x92DCE8F4
            wds.大模型资源 = 0x4B5BF1AF----------
        elseif wd == "血色茶花" then
            wds.说明="出自南方的野生茶花"
            wds.功效="恢复气血150点"
            wds.气血=150
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x50B560E1
            wds.大模型资源 = 0x4DE946B5--------
        elseif wd == "六道轮回" then
            wds.说明="天宫的异花"
            wds.功效="恢复气血200点"
            wds.气血=200
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7A231B4A
            wds.大模型资源 = 0x6F66E996---
        elseif wd == "熊胆" then
            wds.说明="千年熊罴的胆"
            wds.功效="恢复气血200点"
            wds.气血=200
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x51EC528C
            wds.大模型资源 = 0x10929E69--------
        elseif wd == "凤凰尾" then
            wds.说明="凤凰尾部的羽毛"
            wds.功效="恢复气血250点"
            wds.气血=250
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x5BC584A7
            wds.大模型资源 = 0x9E720FD5--------
        elseif wd == "硫磺草" then
            wds.说明="在硫磺地长大的圣草"
            wds.功效="恢复气血250点"
            wds.气血=250
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x338422D9
            wds.大模型资源 = 0xA49FDDC1-------
        elseif wd == "龙之心屑" then
            wds.说明="用龙心提炼出的灵药"
            wds.功效="恢复气血300点"
            wds.气血=300
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xBF847936
            wds.大模型资源 = 0xC26FA33C------
        elseif wd == "火凤之睛" then
            wds.说明="凤凰的眼睛"
            wds.功效="恢复气血300点"
            wds.气血=300
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD0B9CCDF
            wds.大模型资源 = 0x7756A846-------
        elseif wd == "丁香水" then
            wds.说明="从仙岛上的丁香花中精心提炼而成"
            wds.功效="恢复法力75点"
            wds.魔法=75
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x33DBD039
            wds.大模型资源 = 0x23B1D2DA----------
        elseif wd == "月星子" then
            wds.说明="残月形状的白色石头"
            wds.功效="恢复法力75点"
            wds.魔法=75
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x18450CD6
            wds.大模型资源 = 0xE3307E95--------
        elseif wd == "仙狐涎" then
            wds.说明="仙狐的涎水制成的药草"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xB5F99FA9
            wds.大模型资源 = 0x1E547A7E-------
        elseif wd == "地狱灵芝" then
            wds.说明="产自地狱深处的灵芝草"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x2F7C81CE
            wds.大模型资源 = 0x696C18EE------
        elseif wd == "麝香" then
            wds.说明="出自野生麝"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x070423BB
            wds.大模型资源 = 0x6292168E---------
        elseif wd == "血珊瑚" then
            wds.说明="东海海底的千年珊瑚"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x0789B660
            wds.大模型资源 = 0x92583E49------
        elseif wd == "餐风饮露" then
            wds.说明="南海普陀山的仙竹枝"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xEB07E77A
            wds.大模型资源 = 0x28AF1886-------
        elseif wd == "白露为霜" then
            wds.说明="五庄观前的仙松之枝"
            wds.功效="恢复法力100点"
            wds.魔法=100
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xC94308D2
            wds.大模型资源 = 0x95A825DD--------
        elseif wd == "天龙水" then
            wds.说明="天龙血炼成的药物"
            wds.功效="恢复法力150点"
            wds.魔法=150
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x3256793E
            wds.大模型资源 = 0xDA628B3B------
        elseif wd == "孔雀红" then
            wds.说明="用孔雀的精华制成的药品"
            wds.功效="恢复法力150点"
            wds.魔法=150
            wds.价格=2500
            wds.等级=2
            wds.类型="药品"
            wds.叠加=true
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xF4A285EE
            wds.大模型资源 = 0x82670C5A----------
 -----------三级药
        elseif wd == "十香返生丸" then
            wds.说明="有着浓郁香气的药丸"
            wds.功效="解除酒类异常状态，恢复魔法=品质*3+50"
            wds.价格=20000
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xC4D733DA
            wds.大模型资源 = 0x3D4A4DFD
        elseif wd == "金创药" then
            wds.说明="常见的疗伤药"
            wds.功效="恢复气血400点"
            wds.气血=400
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3360268674
            wds.大模型资源 = 3769489910------
        elseif wd == "金香玉" then
            wds.说明="仙子仙女常用的药物"
            wds.功效="功效 = 恢复气血=品质*12+150"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 875333087
            wds.大模型资源 = 2395394781---------
        elseif wd == "小还丹" then
            wds.说明="散发着香气的紫色药丸"
            wds.功效="品质*8+100，治疗伤势=品质+80"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1261161767
            wds.大模型资源 = 4110383651-------
        elseif wd == "红雪散" then
            wds.说明="仙家灵药"
            wds.功效="解除毒类异常状态，恢复气血=品质*4，恢复防御=品质*0.4"
            wds.价格=20000
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x8ABE812C
            wds.大模型资源 = 0x09BA5E8F--------
        elseif wd == "千年保心丹" then
            wds.说明="大量名贵药品制成的药丹"
            wds.功效="恢复气血=品质*4+200，治疗伤势=品质*4+100"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3968259402
            wds.大模型资源 = 212192249----------
        elseif wd == "风水混元丹" then
            wds.说明="各类名贵药品精炼的丹药"
            wds.功效="恢复魔法=品质*3+50,恢复灵力=品质*0.3+44"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1613137255
            wds.大模型资源 = 3441623302
        elseif wd == "定神香" then
            wds.说明="名贵药物精心炼成的丹药"
            wds.功效="解除奇经八脉技能产生的异常状态，恢复魔法=品质*5+50"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2570329553
            wds.大模型资源 = 4232010254-------
        elseif wd == "蛇蝎美人" then
            wds.说明="魔王妖王常用的药物"
            wds.功效="恢复魔法=品质*5+100"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2982020731
            wds.大模型资源 = 2737638149--------
        elseif wd == "九转回魂丹" then
            wds.说明="炼过九转的仙丹，可复活目标并恢复一定气血"
            wds.功效="复活、恢复气血=品质*5+100，临时气血上限变为品质*5+100"
            wds.价格=2500
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 68961914
            wds.大模型资源 = 1116089577-------
        elseif wd == "佛光舍利子" then
            wds.说明="佛的舍利子，可复活目标并恢复一定气血"
            wds.功效=[[复活、恢复气血=品质*3，临时气血上限变为品质*7+100]]
            wds.品质=10
            wds.价格=20000
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x1F920A65
            wds.大模型资源 = 0x15A0C084
        elseif wd == "五龙丹" then
            wds.说明="来自东海龙宫的珍贵药品"
            wds.功效="解除封类异常状态，恢复气血=品质*3"
            wds.价格=20000
            wds.等级=3
            wds.类型="药品"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 260113467
            wds.大模型资源 = 3795878131-------------

 -------------------------------召唤兽护腕--------------------------- 召唤兽装备
        elseif wd == "铁腕" then
            wds.说明 = "铁做的护腕，可以起到伤害力"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2535965770
            wds.大模型资源 = 1102893761
        elseif wd == "竹编护腕" then
            wds.说明 = "竹子做的护腕，可以起到伤害力"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 5
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1408822181
            wds.大模型资源 = 2157708488
        elseif wd == "皮腕" then
            wds.说明 = "皮布做的护腕，可以起到伤害力"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 15
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 862596806
            wds.大模型资源 = 2834822848
        elseif wd == "针腕" then
            wds.说明 = "针做的护腕，可以起到伤害力"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 25
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3248808566
            wds.大模型资源 = 472447855
        elseif wd == "骨镯" then
            wds.说明 = "骨头做的护腕，可以起到伤害力"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 35
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1197998449
            wds.大模型资源 = 3679624742
        elseif wd == "青铜护腕" then
            wds.说明 = "铜做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 45
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 355149374
            wds.大模型资源 = 2634874482
        elseif wd == "玛瑙护腕" then
            wds.说明 = "玛缁做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 55
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3007339583
            wds.大模型资源 = 3717112991
        elseif wd == "琉璃护腕" then
            wds.说明 = "琉璃的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 65
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1069097956
            wds.大模型资源 = 3349215227
        elseif wd == "镂空银镯" then
            wds.说明 = "银做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 75
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3082110275
            wds.大模型资源 = 3781752769
        elseif wd == "笼玉镯" then
            wds.说明 = "玉做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 85
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 813354899
            wds.大模型资源 = 4276176260
        elseif wd == "嵌宝金腕" then
            wds.说明 = "金做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 95
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3180910965
            wds.大模型资源 = 2826073060
        elseif wd == "玳瑁护腕" then
            wds.说明 = "玳瑁做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 =  105
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2446137013
            wds.大模型资源 = 188760799
        elseif wd == "七星宝腕" then
            wds.说明 = "寒冰沂蒙精心给大家的护腕，戴上这个可以秒天秒地秒空气"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 115
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 770473038
            wds.大模型资源 = 361633315
        elseif wd == "缚龙筋" then
            wds.说明 = "龙筋做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 125
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1671231141
            wds.大模型资源 = 3934076700
        elseif wd == "凤翎护腕" then
            wds.说明 = "凤凰的羽毛做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 135
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1520923728
            wds.大模型资源 = 3501233104
        elseif wd == "织锦彩带" then
            wds.说明 = "锦做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 145
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 426710040
            wds.大模型资源 = 4049294130
        elseif wd == "冰蚕丝带" then
            wds.说明 = "冰蚕丝做的护腕"
            wds.类型 = "召唤兽装备"
            wds.类别 = "护腕"
            wds.等级 = 155
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 669823245
            wds.大模型资源 = 3491081774
 -------------------------------召唤兽项圈
        elseif wd == "藤圈" then
            wds.说明 = "藤条围成的项圈，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4170754053
            wds.大模型资源 = 2301647242
        elseif wd == "竹编脖环" then
            wds.说明 = "竹子围成的项圈，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"
            wds.等级 = 5
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 308637008
            wds.大模型资源 = 3243526110
        elseif wd == "钢圈" then
            wds.说明 = "皮布围成的项圈，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"
            wds.等级 = 15
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1375604814
            wds.大模型资源 = 4041865794
        elseif wd == "荆棘环" then
            wds.说明 = "荆棘围成的项圈，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"
            wds.等级 = 25
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 352928497
            wds.大模型资源 = 2194758983
        elseif wd == "骨环" then
            wds.说明 = "骨头围成的项圈，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"
            wds.等级 = 35
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1727331457
            wds.大模型资源 = 440555011
        elseif wd == "青铜颈环" then
            wds.说明 = "铜做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 45
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1454090395
            wds.大模型资源 = 786120977
        elseif wd == "玛瑙石环" then
            wds.说明 = "玛缁做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 55
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 391314910
            wds.大模型资源 = 1466260477
        elseif wd == "琉璃环" then
            wds.说明 = "琉璃的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 65
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1897984925
            wds.大模型资源 = 2342712762
        elseif wd == "九曲环" then
            wds.说明 = "银做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 75
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3613005753
            wds.大模型资源 = 3507788902
        elseif wd == "笼玉环" then
            wds.说明 = "玉做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 85
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1850407544
            wds.大模型资源 = 3784211553
        elseif wd == "嵌宝金环" then
            wds.说明 = "金做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 95
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3283266712
            wds.大模型资源 = 3275437370
        elseif wd == "玳瑁环" then
            wds.说明 = "玳瑁做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 =  105
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3240013292
            wds.大模型资源 = 225036096
        elseif wd == "七星宝环" then
            wds.说明 = "寒冰沂蒙精心给大家制作的手环，戴上他可以秒天秒地秒空气"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 115
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 678406505
            wds.大模型资源 = 552173832
        elseif wd == "缚龙圈" then
            wds.说明 = "龙筋做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 125
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2612616501
            wds.大模型资源 = 3489814847
        elseif wd == "鸾尾环" then
            wds.说明 = "凤凰的羽毛做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 135
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2344853585
            wds.大模型资源 = 1622496513
        elseif wd == "织锦颈圈" then
            wds.说明 = "锦做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 =145
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4086876715
            wds.大模型资源 = 888220028
        elseif wd == "冰蚕丝圈" then
            wds.说明 = "冰蚕丝做的项圈"
            wds.类型 = "召唤兽装备"
            wds.类别 = "项圈"

            wds.等级 = 155
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1454090395
            wds.大模型资源 = 786120977
 -------------------------------召唤兽铠甲
        elseif wd == "麻衣" then
            wds.说明 = "麻布制成的衣服，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3547852010
            wds.大模型资源 = 276685974
        elseif wd == "皮甲" then
            wds.说明 = "皮布制成的衣服，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 5
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1141657141
            wds.大模型资源 = 3835149326
        elseif wd == "刺甲" then
            wds.说明 = "精铁制成的衣服，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 15
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1718680620
            wds.大模型资源 = 360517064
        elseif wd == "骨排甲" then
            wds.说明 = "骨头制成的衣服，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 25
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1237852263
            wds.大模型资源 = 1956127955
        elseif wd == "青铜披甲" then
            wds.说明 = "青铜制成的衣服，可以起到防御作用"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 35
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3118973276
            wds.大模型资源 = 1828827555
        elseif wd == "青铜披甲" then
            wds.说明 = "铜做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 45
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3118973276
            wds.大模型资源 = 1828827555
        elseif wd == "玛瑙软甲" then
            wds.说明 = "玛缁做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 55
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2919721376
            wds.大模型资源 = 3353446058
        elseif wd == "琉璃罩甲" then
            wds.说明 = "琉璃的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 65
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 696270961
            wds.大模型资源 = 2418258211
         elseif wd == "连环铠甲" then
            wds.说明 = "银做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 75
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 801892250
            wds.大模型资源 = 151739274
        elseif wd == "笼玉甲" then
            wds.说明 = "玉做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 85
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4176524624
            wds.大模型资源 = 307625549
        elseif wd == "嵌宝金甲" then
            wds.说明 = "金做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 95
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 592200716
            wds.大模型资源 = 4156302542
        elseif wd == "玳瑁衣" then
            wds.说明 = "玳瑁做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 =  105
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1446992849
            wds.大模型资源 = 371678350
        elseif wd == "七星宝甲" then
            wds.说明 = "寒冰沂蒙精心为大家做的铠甲，戴上后可以秒天秒地秒空气"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 115
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 1584383840
            wds.大模型资源 = 1345840279
        elseif wd == "缚龙甲" then
            wds.说明 = "龙筋做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 125
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 67336897
            wds.大模型资源 = 3490861079
        elseif wd == "凤凰彩衣" then
            wds.说明 = "凤凰的羽毛做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 135
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3984235518
            wds.大模型资源 = 3296343909
        elseif wd == "织锦软褡" then
            wds.说明 = "锦做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 145
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 3346981047
            wds.大模型资源 = 1314867248
        elseif wd == "冰蚕织甲" then
            wds.说明 = "冰蚕丝做的铠甲"
            wds.类型 = "召唤兽装备"
            wds.类别 = "铠甲"
            wds.等级 = 155
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 524242583
            wds.大模型资源 = 2617390472
    ----------------翅膀
        elseif wd == "太古" then
            wds.说明 = "上古凶兽所化的双翼"
            wds.类型 = "翅膀"
            wds.等级 = 60
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x295FD4C1
            wds.大模型资源 = 0x00B98599


     -- 新增物品

        elseif wd == "鬼谷子" then
            wds.说明 = "战国智者鬼谷子所著的兵法兼玄学书籍。"
            wds.价格 = 300000
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 4271545734
            wds.大模型资源 = 1264860140
        elseif wd == "打狗棒" then
            wds.说明 = "在四季活动-天狗食月中在战斗中对噬月天狗使用，可以对其造成2000点伤害"
            wds.类型 = "活动道具"
            wds.类别 = 1
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xA545ABC8
            wds.大模型资源 = 0xD05BA36C
         elseif wd == "月饼" then
            wds.说明 = "中秋节活动的奖品。食用后可以增加人物2点潜能（不可以对召唤兽使用），但每个人物最多食用50个月饼"
            wds.类型 = "消耗"
            wds.类别 = 10
            wds[8] = 1
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x846AD205
            wds.大模型资源 = 0x49B4F6DC
         elseif wd == "元宵" then
            wds.说明 = "元宵节活动的奖品。在道具行囊界面直接食用后可以提高召唤兽的属性（不可以对人物使用），但每个召唤兽最多食用10个元宵"
            wds.类型 = "消耗"
            wds.类别 = 10
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x522C3747
            wds.大模型资源 = 0xF04964D3

         elseif wd == "回天丹" then
            wds.说明 = "一种神奇的丹药，使用后在接下来的100场战斗结束后自动恢复全体队友状态，效果可叠加。"
            wds.类型 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x522C3747
            wds.大模型资源 = 0xF04964D3
        elseif wd == "超级回天丹" then
            wds.说明 = "一种神奇的丹药，使用后在接下来的1000场战斗结束后自动恢复全体队友状态，效果可叠加。"
            wds.类型 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x522C3747
            wds.大模型资源 = 0xF04964D3
        elseif wd == "点化石" then
            wds.说明 = "记载着神奇能力的石头，可以为宝宝的装备附加上套装效果"
            wds.类型 = "宝石"
            wds.类别 = 5
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xE23B789E
            wds.大模型资源 = 0x02563911
        elseif wd == "鬼王的密信" then
            wds.说明 = "从捣乱的鬼怪身上截获的一封密信"
            wds.类型 = "副本五宝"
            wds.类别 = 1
            wds[8] = "右键即可领取鬼王任务"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xD75A7F1C
            wds.大模型资源 = 0xEA0E76DF
        elseif wd == "梦魇幻境" then
            wds.说明 = "似乎藏着很多人的美梦呢"
            wds.类型 = "副本五宝"
            wds.类别 = 1
            wds[8] = "右键即可领取无尽梦魇任务"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x25492E36
            wds.大模型资源 = 0x663FC75A
        elseif wd == "突厥密令" then
            wds.说明 = "从突厥军中身上截获的令牌，似乎可以混入其中。"
            wds.类型 = "副本五宝"
            wds.类别 = 1
            wds[8] = "右键即可领取突厥入侵任务"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x79E9E6B0
            wds.大模型资源 = 0xE84B01F2
    ------------------------------------------------------------------ 任务物品

        elseif wd == "特赦令牌" then
            wds.说明 = "阎罗地狱里用来宽恕极恶不赦的鬼魂，非常罕见。"
            wds.类型 = "剧情道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x8FF330C0
            wds.大模型资源 = 0xFE72166A
        elseif wd == "枯萎的金莲" then
            wds.说明 = "尘世间人性的金莲，已因心魔之灭而焦枯。"
            wds.类型 = "剧情道具"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x88DCF8C0
            wds.大模型资源 = 0x3B5F875C
        elseif wd == "五级镖银" then
            wds.说明 = "五级镖银,较贵的货物，如果失镖了,赔偿一定少不了。"
            wds.类型 = "剧情道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x04BC6EAC
            wds.大模型资源 = 0xB8CDA2EE
        elseif wd == "四级镖银" then
            wds.说明 = "四级镖银,较贵的货物，如果失镖了,赔偿一定少不了。"
            wds.类型 = "剧情道具"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x9C59875F
            wds.大模型资源 = 0x3B5F875C
        elseif wd == "三级镖银" then
            wds.说明 = "三级镖银,较贵的货物，如果失镖了,赔偿一定少不了。"
            wds.类型 = "剧情道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x57A5DB2A
            wds.大模型资源 = 0x19869C59
        elseif wd == "二级镖银" then
            wds.说明 = "二级镖银,较贵的货物，如果失镖了,赔偿一定少不了。"
            wds.类型 = "剧情道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6BBB0E37
            wds.大模型资源 = 0x3B8C4FDF
        elseif wd == "一级镖银" then
            wds.说明 = "一级镖银,较贵的货物，如果失镖了,赔偿一定少不了。"
            wds.类型 = "剧情道具"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9512F4FF
            wds.大模型资源 = 0x774AFCA6
    ------------------------------------------------------------------ 副本物品
        elseif wd == "玲珑" then
            wds.说明 = "交给观音大士，是锻造照妖镜所需要的材料。"
            wds.类型 = "副本五宝"
            wds.类别 = 2
            wds[8] = nil
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xDDA1D3AA
            wds.大模型资源 = 0xD3DC3123
        elseif wd == "普通木材" then
            wds.说明 = "一种常见的建筑木料，在搭建房屋时必须要用到它。"
            wds.类型 = "副本五宝"
            wds.类别 = 2
            wds[8] = "在“车迟斗法副本”中可以增加道观建设度8点"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 1192555457
            wds.大模型资源 = 4056791876
        elseif wd == "高级木材" then
            wds.说明 = "比较稀有的建筑木料，在搭建房屋时必须要用到它。"
            wds.类型 = "副本五宝"
            wds.类别 = 2
            wds[8] = "在“车迟斗法副本”中可以增加道观建设度20点"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 757799681
            wds.大模型资源 = 2818741734
        elseif wd == "红木短剑" then
            wds.说明 = "古代儿童玩具，可以提高儿童武学悟性"
            wds.类型 = "儿童用品"
            wds.类别 = 1
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xB88A499A
            wds.大模型资源 = 0x5E4CC079
        elseif wd == "搜神记" then
            wds.说明 = "古代儿童玩具，可以提高儿童法术悟性"
            wds.类型 = "儿童用品"
            wds.类别 = 2
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0xCBE7FC23
            wds.大模型资源 = 0xD8BCD815
        elseif wd == "论语" then
            wds.说明 = "古代儿童玩具，可以提高儿童心态"
            wds.类型 = "儿童用品"
            wds.类别 = 3
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x6B17D536
            wds.大模型资源 = 0x8BC893E3
        elseif wd == "瑶池蟠桃" then
            wds.说明 = "古代儿童玩具，可以提高儿童体质"
            wds.类型 = "儿童用品"
            wds.类别 = 4
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x58849704
            wds.大模型资源 = 0x140D0428

 --------------------------------------------------------------------- 法宝
        elseif wd == "法宝碎片" then
             wds.说明 = "集齐99个可以获得随机法宝，右击使用。"
            wds.类型 = "法宝道具"
            wds.资源 = "JM.FT"
            wds.小模型资源 = 0x40994633
            wds.大模型资源 = 0x14E8E290
            wds.价格 = 50000
            wds.叠加 =true
        elseif wd == "碧玉葫芦" then
            wds.说明 = "华佗悬壶济世时所带的翡翠葫芦，此宝长期跟随华佗行医，也深谙医术。佩戴此宝会增加师门法术回复的气血。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "恢复"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
            wds.价格 = 1------
        elseif wd == "飞剑" then---
            wds.说明 = "此飞剑可从千里之外出击，取敌人首级如探囊取物，精准的袭击令人瞠目，佩带飞剑可以增加人物的命中。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1C7633D1
            wds.大模型资源 = 0xBC2FAF63
        elseif wd == "拭剑石" then--
            wds.说明 = "以石拭剑，可以厉其锋，此乃蜀山上千百剑仙拭剑之石，佩带此石可增加人物伤害。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xF95F660F
            wds.大模型资源 = 0xF37BA20D
        elseif wd == "金甲仙衣" then-----
            wds.说明 = "拥有神奇效果的道家仙衣，穿上它在战斗中承受物理攻击时候可能出现天兵保护。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "防御"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x081E1FF5
            wds.大模型资源 = 0x0BDFD09C
        elseif wd == "缚妖索" then--
            wds.说明 = "天师的法宝，战斗中祭起后将目标牢牢捆缚住，使其的逃跑成功率降低。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD1AE3698
            wds.大模型资源 = 0x555438C2
        elseif wd == "鬼脸面具" then--
            wds.说明 = "用天蚕丝所制的面具，辅以上古符咒，是非常好的变身辅助。戴上它可以在战斗中使用变身卡进行变身。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xF6001248
            wds.大模型资源 = 0x119F576D
        elseif wd == "金钱镖" then---
            wds.说明 = "财神爷的看家宝贝，散出千枚铜钱伤人，如果碰上不为金钱所动的人就没有办法了。每次使用消耗当前金钱2000。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "攻击"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x3D9C84A5
            wds.大模型资源 = 0xC876D78A
        elseif wd == "惊魂铃" then-----
            wds.说明 = "铃声长鸣，闻者胆战心惊。战斗中摇晃此铃可以恐吓敌方召唤兽，使其丧失斗志试图逃离战场。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xEAA6AD2E
            wds.大模型资源 = 0x450BE7E8
        elseif wd == "嗜血幡" then-----
            wds.说明 = "暗红色的布幡，散发着血腥嗜杀的气息，拥有此旗在物理攻击时有一定几率额外多攻击一个目标。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "攻击"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xD92DF835
            wds.大模型资源 = 0x50997E59
        elseif wd == "风袋" then--
            wds.说明 = "风婆系在腰间装风的布袋，放出风来可吹人至数里外。配上此袋可增加人物速度。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9968555F
            wds.大模型资源 = 0x59E58E52
        elseif wd == "清心咒" then---
            wds.说明 = "佛门经文，在心中默念三遍可不受外界干扰，清心自如。使用后可解除因法宝而带来的异常效果。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCC7D6870
            wds.大模型资源 = 0xA284A058
        elseif wd == "九黎战鼓" then--
            wds.说明 = "九黎氏族用蛮兽之皮所制的战鼓，佩带此宝可以在战斗中增加召唤兽的攻击力。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "攻击"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xAA69D61E
            wds.大模型资源 = 0xB4FA2E7D
        elseif wd == "盘龙壁" then---
            wds.说明 = "以恶龙之骨制成的饰品，佩带此宝可以在战斗中增加召唤兽的防御。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "防御"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x54C9A8DC
            wds.大模型资源 = 0xC7F67DF4
        elseif wd == "神行飞剑" then--
            wds.说明 = "玄铁打造的雌雄飞剑，佩带此宝可以在战斗中增加召唤兽的速度。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5F80A13D
            wds.大模型资源 = 0x9F57F723
        elseif wd == "汇灵盏" then----
            wds.说明 = "灵霄殿内的宫灯，佩带此宝可以在战斗中增加召唤兽的灵力。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x82982E6B
            wds.大模型资源 = 0x513D3054
        elseif wd == "五色旗盒" then---
            wds.说明 = "昔日唐王拜将，天降五色旗勤兵。臣民以为祥瑞。丞相乃进五色旗盒，以盛锦旗。江湖素有能人巧士，依样画葫芦得制此宝，为行囊便利之用。方寸山觉岸处可获此宝。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x2FC44068
            wds.大模型资源 = 0x5D2FF1BC
        elseif wd == "天师符" then---
            wds.说明 = "神奇的符纸，上面记载着张天师的秘传制符心法，是方寸山不传之秘，佩带此符的方寸山弟子将拥有更强大的符咒操纵能力。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "方寸山"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x44CA233C
            wds.大模型资源 = 0xA5310A75
        elseif wd == "织女扇" then--
            wds.说明 = "织女所持羽扇，拥有它就能像织女一样美丽动人。此扇可提示封印命中率。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "女儿村"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6EA31DE4
            wds.大模型资源 = 0xD3F4F8DB
        elseif wd == "雷兽" then---
            wds.说明 = "李天王为门下弟子专门炼制的宝贝，为五雷之精魄，祭起时夹风雷之势，天旋地转飞沙走石。佩带此物的天宫弟子使用乾坤塔时更为得心应手。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "天宫"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x33768EB0
            wds.大模型资源 = 0xA204F1F0
        elseif wd == "迷魂灯" then--
            wds.说明 = "白晶晶的独门法宝，据说这灯是用百只狐妖的眼神为芯，佩带在身上散发着摄人心魂的灯光。佩带此灯的盘丝洞弟子将提高迷惑敌人的效果。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "盘丝洞"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xAAC25649
            wds.大模型资源 = 0x6EB0B809
        elseif wd == "定风珠" then---
            wds.说明 = "镇元大仙修炼多年的宝贝，拥有摄人的威力，佩带此灯的五庄观弟子对乾坤袖的使用更加纯熟。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "辅助"
            wds.等级限制 = 60
            wds.门派限制 = "五庄观"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x49D1AE82
            wds.大模型资源 = 0x62DBF406
        elseif wd == "幽灵珠" then
            wds.说明 = "增加1级阴风绝章技能，随着境界提升增加技能等级与对封系法术的抗性。"
            wds.类型 = "法宝"
            wds.等级 = 1
            wds.法宝类型 = "恢复"
            wds.等级限制 = 60
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000028
            wds.大模型资源 = 0x00000028--------------
 ------------------------------------------------------2级法宝
        elseif wd == "玄冰面具" then---
            wds.说明 = "戴上这个面具，面容就像千年寒冰下的湖水，谁也不知道是什么样子，拥有者可以在战斗中使用变身卡变幻身份。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x2A0DD83C
            wds.大模型资源 = 0xC1D43099
        elseif wd == "降魔斗篷" then
            wds.说明 = "战斗中遭受法术攻击的时候有一定几率出现神兵现身保护，随着境界提升增加被保护的几率。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x8F0E6273
            wds.大模型资源 = 0xC1B54E00
        elseif wd == "附灵玉" then---
            wds.说明 = "雪山之巅峰中挖掘出的宝玉，蕴藏着千年灵气，佩戴后可以提升自己的灵力。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5C571B45
            wds.大模型资源 = 0x7F16538C
        elseif wd == "捆仙绳" then---
            wds.说明 = "天庭法宝，专为缉拿神仙所制，材料不知为何物，柔韧结实，一旦被捆则降低目标的逃跑成功率。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x2320B82F
            wds.大模型资源 = 0x483454AC
        elseif wd == "现形符" then---
            wds.说明 = "无论对方如何变幻形体，用此符都有可能将其识破。只允许在战斗中对对方使用，有一定几率解除目标变身效果。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x86AF33D1
            wds.大模型资源 = 0x6FE3C110
        elseif wd == "发瘟匣" then--
            wds.说明 = "五瘟正神的秘宝，匣内盛满着发瘟蛊，一旦开启能使目标晕头转向，无法使用法术并且攻击失误。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5F0D2E50
            wds.大模型资源 = 0x780299ED
        elseif wd == "聚妖铃" then--
            wds.说明 = "这铃铛声是号召妖怪们战斗的号角，据说是蚩尤当年为涿鹿之战而制。战斗中摇晃此铃，附近的妖物会聚集过来协助战斗。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCB2C7DF6
            wds.大模型资源 = 0xCF3377CC
        elseif wd == "万鬼幡" then----
            wds.说明 = "地府钟馗的法宝，此幡一祭，天地变色，鬼影憧憧，黄泉十万阴兵应幡而出。在战斗中使用可以召唤鬼魂助战。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xF1D0DFC7
            wds.大模型资源 = 0x0E4DC36F
        elseif wd == "断线木偶" then--
            wds.说明 = "桃木做的人形玩偶，上面雕刻着古怪的符文，会吸引召唤兽对它进行攻击。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "防御"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA9A08E99
            wds.大模型资源 = 0x812181A6
        elseif wd == "五彩娃娃" then---
            wds.说明 = "衣着五彩，色泽鲜美的布娃娃，战斗中会替主人承受一定的伤害。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "防御"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5C86CDBC
            wds.大模型资源 = 0xEFDC64D0
        elseif wd == "番天印" then---
            wds.说明 = "广成子修炼的独门宝贝，砸中后能使人神志不清，三魂出窍，失去对法宝的控制。战斗中祭后有一定几率使目标身上佩带的被动法宝3回合内失去作用。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB33784E0
            wds.大模型资源 = 0x8B590810
        elseif wd == "七杀" then----
            wds.说明 = "堪破人间仁与义，七杀下凡啸腥风。此宝为武斗星君的饰物，战斗中祭起后战意漫天，令对手丧失斗志，毫无招架之力。佩戴后可以增加伤害并忽略对方部分防御。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 90
            wds.门派限制 = "大唐官府"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x1715E7DE
            wds.大模型资源 = 0x3AE1FD6A
        elseif wd == "罗汉珠" then-------
            wds.说明 = "使用后玩家下3个回合使用任何消耗魔法的门派法术消耗减少。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "化生寺"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x14846B1B
            wds.大模型资源 = 0x695614DB
        elseif wd == "分水" then----
            wds.说明 = "以水为盾，可以护体。此宝为西海龙女佩带之物，祭起后遭受攻击时一定几率减少受到的50%伤害，是龙宫秘宝。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "龙宫"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD330F6C9
            wds.大模型资源 = 0xF1CF0FC1
        elseif wd == "赤焰" then-------
            wds.说明 = "此物乃牛魔王与铁扇公主定情之时赠送给娘子的宝贝，使用此宝能在三回合内回复魔法。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "魔王寨"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x743685D8
            wds.大模型资源 = 0x62AEC7E3
        elseif wd == "金刚杵" then---
            wds.说明 = "佛门利器，可断除烦恼、除恶魔，代表佛智、空性、真如与智慧。佩带后可使五行法术的伤害增加。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "攻击"
            wds.等级限制 = 90
            wds.门派限制 = "普陀山"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6F9B0510
            wds.大模型资源 = 0x3E968F47
        elseif wd == "兽王令" then---
            wds.说明 = "玉麒麟甲制成的令牌，经青毛狮以妖力修炼，拥有号令百兽的威力。佩戴此物战斗中召唤兽的所有能力都得到提升。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "辅助"
            wds.等级限制 = 90
            wds.门派限制 = "狮驼岭"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6D4DD526
            wds.大模型资源 = 0xF3F163D2
        elseif wd == "摄魂" then---
            wds.说明 = "阎王要你三更死，谁敢留人过五更。此物为黄泉中至阴之物，专门摄取生魂，通常勾魂使者才能携带。祭起后使目标失魂落魄，失去生气，三回合内承受的伤害增加。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 90
            wds.门派限制 = "阴曹地府"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xBDD4BF97
            wds.大模型资源 = 0xAFF8536F
        elseif wd == "宝烛" then
            wds.说明 = "地涌夫人于大雷音寺之时所盗之宝烛，佩戴此物如有佛祖护佑，每回合结束时可自动回复气血。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000002
            wds.大模型资源 = 0x00000001-----------
        elseif wd == "天煞" then
            wds.说明 = "二郎神行游三界，千年间战火煞气凝为此珠。佩戴此物者，祭起后，回合开始时恢复魔法，持续3回合。满层情况下，天煞状态结束时可回复1点战意。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000030
            wds.大模型资源 = 0x00000029
        elseif wd == "神木宝鼎" then
            wds.说明 = "神木林祭师祭天地所用之物。在战斗中祭起此鼎临时提升自己三回合内大量法术伤害。"
            wds.类型 = "法宝"
            wds.等级 = 2
            wds.法宝类型 = "恢复"
            wds.等级限制 = 90
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000016
            wds.大模型资源 = 0x00000015
 -------------------------------------------------3级法宝
        elseif wd == "神木面具" then---
            wds.说明 = "十多人才能合抱的神木，散发着古老神秘的气息，佩带者可以在战斗中使用高级变身卡进行变身。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDB18248B
            wds.大模型资源 = 0xAA4248EA
        elseif wd == "落雨金钱" then----
            wds.说明 = "财神爷的不传之秘，用银锭砸人，如果碰上不为金钱所动的人就没有办法了。每次使用消耗当前金钱4000。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "攻击"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xFC98FD6A
            wds.大模型资源 = 0xFA7125D2
        elseif wd == "缚龙索" then---
            wds.说明 = "此索就算八部天龙被缚也难以逃脱，一般人被捆缚住后，逃跑成功率大幅度降低，只有乖乖被擒。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x0309AD83
            wds.大模型资源 = 0x335299FF
        elseif wd == "照妖镜" then--
            wds.说明 = "此镜一出，妖魔鬼怪无处遁形。只允许在战斗中对对方使用，有很大几率识破对方的幻化效果。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA4E6ECB0
            wds.大模型资源 = 0x9F83D4D7
        elseif wd == "鬼泣" then---
            wds.说明 = "在十八层地狱深处锻造之铃，吸收地府冤怨之气，摇之如闻听百鬼群号，可震慑敌方召唤兽，使其丧失斗志试图逃离战场。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xF600EEBC
            wds.大模型资源 = 0xEAF4BEB0
        elseif wd == "月光宝盒" then-----
            wds.说明 = "神奇的宝盒，可以前去500年，后回500年。对着月光念出咒语，你就可以到达想去的地方。可以在任意地点定下标记，在非战斗中使用直接达到，一个小时限用一次。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x95DC9FE9
            wds.大模型资源 = 0x10ADFCD8
        elseif wd == "缩地尺" then----
            wds.说明 = "某个神仙制造的逃跑工具，战斗中使用后可以迅速离开战斗逃至其他地方，有可能逃跑失败哦。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x58C421DC
            wds.大模型资源 = 0x8C891F83
        elseif wd == "影蛊" then----
            wds.说明 = "苗疆的奇特宝贝，只要在敌人的影子里投放蛊虫，就可以在一段时间内获知他所在的位置，非战斗中使用。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x63E46E78
            wds.大模型资源 = 0xA5CCFED9
        elseif wd == "混元伞" then----
            wds.说明 = "多闻天王的巨伞，撑起后可遮天蔽日。它能随机将自己承受的部分伤害反弹给对方单个成员。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7729A9E5
            wds.大模型资源 = 0x4110011C
        elseif wd == "落宝金钱" then---
            wds.说明 = "金钱过处，法宝落地。它可以将法宝击落，取消目标主动法宝所增加的临时效果，是战场上强有力的威胁。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1E296612
            wds.大模型资源 = 0x32003A2B
        elseif wd == "无魂傀儡" then---
            wds.说明 = "眼神涣散、毫无生机的傀儡，他们的魂魄已经消失，只有机械的吸引目标对其发起攻击。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "防御"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x05896BCC
            wds.大模型资源 = 0x32F92F8A
        elseif wd == "苍白纸人" then---
            wds.说明 = "符纸制成的纸人，看起来单薄羸弱，在主人受到伤害时会自动现身护主。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "防御"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB91E0707
            wds.大模型资源 = 0x46EC6216
        elseif wd == "通灵宝玉" then----
            wds.说明 = "拥有通灵能力的宝玉，汲取天地间的灵气，佩戴此宝可在获得经验时额外增加一些经验。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x4FC565AA
            wds.大模型资源 = 0xA83BA91E
        elseif wd == "聚宝盆" then----
            wds.说明 = "宝盆能聚宝，财源滚滚来。佩带此盆能比平时获得更多的金钱。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xAE883DD2
            wds.大模型资源 = 0xCE09A059
        elseif wd == "乾坤玄火塔" then---
            wds.说明 = "托塔天王的另外一件宝贝，祭起后可增加斗志与杀气，使勇者更勇。使用后可在一定回合内增加愤怒值。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xF5601CC2
            wds.大模型资源 = 0x1824AF14
        elseif wd == "无尘扇" then--
            wds.说明 = "弥勒佛随身携带的蒲扇，一扇肝火熄，再扇心火消，三扇通体顺。战斗中使用此扇能使对手4回合内持续减少愤怒值。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA1224711
            wds.大模型资源 = 0x9F92A376
        elseif wd == "七宝玲珑灯" then---
            wds.说明 = "佛门至宝，佛祖当年以大雷音寺内玉池内白莲所制，携带此宝者气定神闲，不受七情六欲之苦。此灯一旦佩带，战斗中每回合自动对拥有法宝异常状态的成员进行解除，有一定几率解除状态失败。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6099C4D3
            wds.大模型资源 = 0x10B360A3
        elseif wd == "无字经" then--
            wds.说明 = "无字无画的经书，据说只有开启天眼之人才可以看到竹简上的文字。战斗中祭起经书有很高几率使目标丧失心神，无法使用主动法宝。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7E0AC2BC
            wds.大模型资源 = 0x83A634D9
        elseif wd == "干将莫邪" then----
            wds.说明 = "采五山之铁精，六合之金英，七七四十九日以身赴铜水，血凝剑气而成，削金如泥，吹毛得过，佩带在身上就已剑气横溢，令人心生怯意。战斗中祭起后杀气冲天，两回合内法宝携带者增加极高的伤害。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "攻击"
            wds.等级限制 = 110
            wds.门派限制 = "大唐官府"
            wds.佩戴 = true
            wds.主动被动 = "主动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x6B5B81EF
            wds.大模型资源 = 0x5723DCC0
        elseif wd == "慈悲" then--
            wds.说明 = "大慈与一切众生乐，大悲拔一切众生苦。大慈以喜乐因缘与众生，大悲以离苦因缘与众生。佩带此物者将受到佛祖的庇护，使用门派法术与特技恢复气血的效果将大幅度提升。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 110
            wds.门派限制 = "化生寺"
            wds.佩戴 = true
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000025
            wds.大模型资源 = 0x00000026
        elseif wd == "曼陀罗" then
            wds.说明 = "精巧的银制匣子，内藏百花之毒，是女儿村的镇山法宝，佩戴此宝的女儿村弟子通过师门技能或暗器使目标中毒后受到伤害增加。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "女儿村"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1--
            wds.大模型资源 = 0x546D7271
        elseif wd == "救命毫毛" then------
            wds.说明 = "当年观音大士赠予孙悟空取经路上救急之用的宝贝，孙悟空修成正果后把此宝转赠方寸山弟子，佩带毫毛在战斗中死亡后每回合有一定几率复活，每场战斗允许复活3次。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "方寸山"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xCD4EA3D4
            wds.大模型资源 = 0xE95997AD
        elseif wd == "伏魔天书" then---
            wds.说明 = "雷鞭电闪商羊舞，流金泻火混沌开。五雷正法鬼神惧，九天降魔显神威。此物为荡魔天尊赠予托塔天王，只有得到李天王赏识的天宫门下弟子才有机会得到这件宝物。佩带此宝的天宫弟子在施展雷部法术时威力将更加惊人。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "攻击"
            wds.等级限制 = 110
            wds.门派限制 = "天宫"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x36E1F4B7
            wds.大模型资源 = 0x00A48670
        elseif wd == "普渡" then---
            wds.说明 = "佩带法宝者使用门派法术普渡众生时有一定几率额外为队伍中另外一个目标施放普渡众生"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 110
            wds.门派限制 = "普陀山"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xF4E2C1E9
            wds.大模型资源 = 0x890FA6D3
        elseif wd == "镇海珠" then----
            wds.说明 = "以大禹治水时的石尺炼化而成，祭起此珠再大的风浪都要平息。佩带此宝的龙宫弟子可以领会本门神通的精髓，使自己的法术在战斗中发挥更大的威力。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "攻击"
            wds.等级限制 = 110
            wds.门派限制 = "龙宫"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x9FB5513B
            wds.大模型资源 = 0xE73D5164
        elseif wd == "奇门五行令" then---
            wds.说明 = "镇元大仙的信物，可号令九洲土地，其本身还有特殊的功效，战斗中祭起一定几率使目标两回合内神智不清，法术攻击失去准头。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "五庄观"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x3463ADAD
            wds.大模型资源 = 0x85774C73
        elseif wd == "失心钹" then---
            wds.说明 = "以玄铁为材料，在畜生道中历练千年的霸道妖器。祭起后鬼哭狼嚎，可能令目标召唤兽失去心智攻击其本方任意成员。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "诅咒"
            wds.等级限制 = 110
            wds.门派限制 = "狮驼岭"
            wds.佩戴 = false
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA2FE6F68
            wds.大模型资源 = 0xFAB633D8
        elseif wd == "五火神焰印" then----
            wds.说明 = "一祭风沙起，再祭烈火燃，三祭浓烟蔽日。红孩儿以内丹在离火内修炼数百年的印章。佩带此宝能增加法术暴击的几率"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "辅助"
            wds.等级限制 = 110
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x2890FFA9
            wds.大模型资源 = 0xC36E1F99
        elseif wd == "九幽" then----
            wds.说明 = "孟婆的茶盏，沉淀着每个鬼魂投胎转世前的怨恨与无奈，其怨之深，其恨之毒，天下无双。此物佩带者在战斗中可通过尸腐毒造成的伤害为本方玩家成员治疗当前气血，包括持续伤害。每人每回合气血回复血量最多不超过等级*3。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 110
            wds.门派限制 = "阴曹地府"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x7E21AF3F
            wds.大模型资源 = 0xB47273A0
        elseif wd == "忘情" then---
            wds.说明 = "永远有多远，拿一世来衡量。我许你三生三世，你给我一点惆怅。此扇为白晶晶之物，佩带者能感受到她已经忘却的情伤，佩带后可以在使用盘丝洞封系法术时一定几率使目标身中情毒，三回合内治疗效果下降。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 110
            wds.门派限制 = "盘丝洞"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xB7113AA0
            wds.大模型资源 = 0xB664DE8F
        elseif wd == "斩魔" then
            wds.说明 = "四海纵横，剑斩妖魔，此剑为二郎神杨戬心爱之物。佩戴此法宝可增加物理攻击时的物理伤害结果。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 100
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000022
            wds.大模型资源 = 0x00000021
        elseif wd == "金蟾" then
            wds.说明 = "无底洞洞府极幽深处所居之蟾精，历经千年而尸身不腐，玲珑剔透状如珠玉。使用此法宝，护体金蟾被击破后，可回复本方所有玩家气血。"
            wds.类型 = "法宝"
            wds.等级 = 3
            wds.法宝类型 = "恢复"
            wds.等级限制 = 100
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "主动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000004
            wds.大模型资源 = 0x00000003
 -----------------------------------------4级法宝
        elseif wd == "重明战鼓" then
            wds.说明 = "重明鸟灵所化之战鼓，击之可激扬士气，倍增勇武。佩戴此法宝，可大大提升召唤兽的物理伤寒和法术伤害。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000024
            wds.大模型资源 = 0x00000023
        elseif wd == "梦云幻甲" then
            wds.说明 = "以九天云雾炼制而成的宝甲，轻若无物，如梦似幻，令人目眩神迷，可大大提升召唤兽的物理防御和法术防御。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000017
            wds.大模型资源 = 0x00000018
        elseif wd == "河图洛书" then
            wds.说明 = "蕴含易理的天地灵宝，透出一股玄奥之感。佩戴此法宝，可大大提升封印命中率和抵抗封印命中率。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000020
            wds.大模型资源 = 0x00000019
        elseif wd == "炽焰丹珠" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "翡翠芝兰" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "太虚玉液" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "蟠龙玉璧" then
            wds.说明 = "金色蟠龙所化的玉璧，能自行以龙气护主。佩戴此法宝，受到物理攻击或者法术攻击时，一定几率减免部分伤害。不能与金甲仙衣、降魔斗篷同时佩戴。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000012
            wds.大模型资源 = 0x00000011
        elseif wd == "九梵清莲" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "苍灵雪羽" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "烽火狼烟" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "舞雪冰蝶" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "紫火如意" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "凌波仙符" then
            wds.说明 = "聚清风为气，锁流云为形，能使人进入战斗状态，神行千里，凌波飞度。佩戴此法宝，在强烈的战斗欲望驱使下，使得人物速度大大提升。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000022
            wds.大模型资源 = 0x00000021
        elseif wd == "归元圣印" then
            wds.说明 = "圣气非凡的一方小印，上书《楞严经》全篇，可助人明心见性，证道菩提。佩戴此法宝，可大幅提升治疗量。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
             wds.资源 = "WP1.dll"
            wds.小模型资源 = 0x00000024
            wds.大模型资源 = 0x00000023
        elseif wd == "落星飞鸿" then
            wds.说明 = "云霄之上有飞鸿，掠影落星下九重。传说中的神物，佩戴后可让人在战斗时借用九天星辰之力。佩戴此法宝，进入战斗时，大幅提升物理伤害。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000008
            wds.大模型资源 = 0x00000007
        elseif wd == "流影云笛" then
            wds.说明 = "天光流不定，云影共徘徊。以飘渺云竹制成的仙笛，能令人在施展法术时事半功倍。进入战斗时，大幅提升法术伤害。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000006
            wds.大模型资源 = 0x00000005
        elseif wd == "月影" then
            wds.说明 = "此物为冬夜极寒之时，神木林祭师亲自前往神木之顶采集残月之月华凝结而成。佩戴此法宝可增加法术攻击的连击几率。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000020
            wds.大模型资源 = 0x00000019
        elseif wd == "软烟罗锦" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "谷玄星盘" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "千斗金樽" then
            wds.说明 = "随战斗回合数增加物理暴击率。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "common/item.wdf"
            wds.小模型资源 = 0x8B4E9AC5
            wds.大模型资源 = 0xAA6F6118
        elseif wd == "宿幕星河" then
            wds.说明 = "银河倾泻，夜幕悬，星空之万千变化皆在其中，以星河之力激荡本心，源源不绝，佩戴此法宝，可以随战斗回合倍增加法术暴击。"
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x00000018
            wds.大模型资源 = 0x00000017
        elseif wd == "北冥巨鳞" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "血魄元幡" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "翠碧玉箫" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "冷龙石磐" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "琉璃云罩" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
        elseif wd == "璞玉灵钵" then
            wds.说明 = ""
            wds.类型 = "法宝"
            wds.等级 = 4
            wds.法宝类型 = "恢复"
            wds.等级限制 = 120
            wds.门派限制 = "无"
            wds.佩戴 = true
            wds.主动被动 = "被动"
            wds.资源 = "item.wdf"
            wds.小模型资源 = 0x60523FF1
            wds.大模型资源 = 0x7669E147
 --------------------------------------------------------------------染色道具
        elseif wd == "彩果" then
        wds.说明 = "漂亮的七彩果，是非常难得的颜料，能够将人物头发和服饰染成特殊的颜色"
        wds.类型 = "染色道具"
        wds.价格=50000
        wds.叠加 =true
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 567106113
        wds.大模型资源 = 4251752243
        elseif wd == "花豆" then
        wds.说明 = "漂亮的七彩豆，是常用的颜料，可以改变人物头发和服饰的颜色，每粒代表1个颜料。"
        wds.类型 = "染色道具"
        wds.价格=2000
        wds.叠加 =true
        wds.资源 = "WP.FT"
        wds.小模型资源 = 0x3E929A4D
        wds.大模型资源 = 0xF6711EC8
 ---------------------------------------------------------------------五宝
        elseif wd == "金刚石" then
        wds.说明 = "最坚硬美丽的宝石，是传说中的五宝之一。右击后可自动与包裹内的其它四宝合成一张高级藏宝图"
        wds.类型 = "五宝"
        wds.价格=20000
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0x517A3AFF
        wds.大模型资源 = 0x797267D4
        elseif wd == "定魂珠" then
        wds.说明 = "可以用来寻找失散在人间的鬼魂的奇妙宝珠，是传说中的五宝之一。右击后可自动与包裹内的其它四宝合成一张高级藏宝图"
        wds.类型 = "五宝"
        wds.价格=20000
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0x3B83CE08
        wds.大模型资源 = 0x605D073F
        elseif wd == "避水珠" then
        wds.说明 = "能够避水的宝贝，用这件宝物即使不熟水性之人也可以去到深海之底。右击后可自动与包裹内的其它四宝合成一张高级藏宝图"
        wds.类型 = "五宝"
        wds.价格=20000
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0xAF394F2F
        wds.大模型资源 = 0xAF8B6CC5
        elseif wd == "夜光珠" then
        wds.说明 = "能够在夜里发光的明珠，是传说中的五宝之一。右击后可自动与包裹内的其它四宝合成一张高级藏宝图"
        wds.类型 = "五宝"
        wds.价格=20000
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0x4497E508
        wds.大模型资源 = 0x91BB3A47
        elseif wd == "龙鳞" then
        wds.说明 = "龙族身上的鳞片，是传说中的五宝之一。右击后可自动与包裹内的其它四宝合成一张高级藏宝图"
        wds.类型 = "五宝"
        wds.价格=20000
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0x1C64382E
        wds.大模型资源 = 0x853BB4DA
 ---------------------------------------------------------------------宝石

        elseif wd == "星辉石" then
            wds.说明 = "如星光一样璀璨的宝石，散发出耀眼的光芒。用于强化灵饰效果。"
            wds.类型 = "宝石"
            wds.部位="武器、头盔"
            wds.效果="增加8点伤害力"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD2DFDC4D
            wds.大模型资源 = 0x1F2B86DF
            wds.价格 = 300000
        elseif wd == "红宝石" then
            wds.说明 = "闪耀着红色光彩的宝石，代表友情、友爱与希望；传说有着护主的奇效。\n增加4*宝石等级%的火系法术吸收率"
            wds.类型 = "宝石"
            wds.部位="饰品、鞋子"
            wds.效果="增加4%的火系法术吸收率"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x651D319C
            wds.大模型资源 = 0xD7E9FDB3
            wds.价格 = 300000
        elseif wd == "绿宝石" then
            wds.说明 = "闪耀着绿色光彩的宝石，代表友情、友爱与希望；传说有着护主的奇效。\n增加4*宝石等级%的土系法术吸收率"
            wds.类型 = "宝石"
            wds.部位="饰品、鞋子"
            wds.效果="增加4%的土系法术吸收率"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xE59EFA68
            wds.大模型资源 = 0x0DCD4F81
            wds.价格 = 300000
        elseif wd == "蓝宝石" then
            wds.说明 = "闪耀着蓝色光彩的宝石，代表友情、友爱与希望；传说有着护主的奇效。\n增加4*宝石等级%的水系法术吸收率"
            wds.类型 = "宝石"
            wds.部位="饰品、鞋子"
            wds.效果="增加4%的水系法术吸收率"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x791427E4
            wds.大模型资源 = 0x67107BB6
            wds.价格 = 300000
        elseif wd == "黄宝石" then
            wds.说明 = "闪耀着金色光彩的宝石，代表友情、友爱与希望；传说有着护主的奇效。\n增加4*宝石等级%的雷系法术吸收率"
            wds.类型 = "宝石"
            wds.部位="饰品、鞋子"
            wds.效果="增加4%的雷系法术吸收率"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x88253E8D
            wds.大模型资源 = 0x4A96C39E
            wds.价格 = 300000
        elseif wd == "光芒石" then
                wds.说明 = "聚天地之精华而成的宝石，通体散发着夺目的光彩，令人不敢直视。是用来合成装备的神奇矿石。"
                wds.类型 = "宝石"
                wds.部位="铠甲、腰带"
                wds.效果="提高40点气血上限"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 182374181
                wds.大模型资源 = 1097166752
                wds.价格 = 300000
        elseif wd == "月亮石" then
                wds.说明 = "由月宫中流落到人间的宝石，吸取了月光之精华，能起到鼓舞和提神的作用。是用来合成装备的神奇矿石。"
                wds.类型 = "宝石"
                wds.部位="铠甲、头盔"
                wds.效果="增加12点防御力"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 3488952287
                wds.大模型资源 = 3067278649
                wds.价格 = 300000
        elseif wd == "太阳石" then
                wds.说明 = "从上古时期流传至今的“太阳之石”，附有神力。是用来合成装备的神奇矿石。"
                wds.类型 = "宝石"
                wds.部位="武器、头盔"
                wds.效果="增加8点伤害力"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 1387446262
                wds.大模型资源 = 2414382713
                wds.价格 = 300000
        elseif wd == "舍利子" then
                wds.说明 = "佛的舍利子是合成装备的神秘材料。"
                wds.类型 = "宝石"
                wds.部位="铠甲、项链"
                wds.效果="增加6点灵力"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 2228232000
                wds.大模型资源 = 3414114421
                wds.价格 = 300000
        elseif wd == "红玛瑙" then
                wds.说明 = "红色的玛瑙石，是合成装备的神秘材料。"
                wds.类型 = "宝石"
                wds.部位="武器、头盔"
                wds.效果="增加25点命中"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 2783082158
                wds.大模型资源 = 3655137177
                wds.价格 = 300000
        elseif wd == "黑宝石" then
                wds.类型 = "宝石"
                wds.说明="坚硬无比的宝石；表面看起来毫不起眼，实际上却蕴涵着巨大的力量。是用来合成装备的神奇矿石"
                wds.部位="腰带、鞋子"
                wds.效果="提高8点速度"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 4208912380
                wds.大模型资源 = 1963658266
                wds.价格 = 300000
        elseif wd == "神秘石" then
                wds.说明 = "在民间流传已久的的宝石，来历不详；似乎对外界施与的打击有着一股神秘的抗力。"
                wds.类型 = "宝石"
                wds.部位="武器、鞋子"
                wds.效果 = "提高20点躲避力"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 749980813
                wds.大模型资源 = 3278224394
                wds.价格 = 300000
     ---------------------------------------------------------------------暗器
           elseif wd == "飞刀" then
                wds.说明="锋利的飞刀，普通的暗器"
                wds.等级=10
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xA4C811C5
                wds.大模型资源 = 0x935EC081
                wds.价格 = 5000
           elseif wd == "飞蝗石" then
                wds.说明="普通暗器，但如果使用熟了，非常方便"
                wds.等级=20
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x4C07009E
                wds.大模型资源 = 0xBF944057
                wds.价格 = 20000
           elseif wd == "铁蒺藜" then
                wds.说明="形状似蒺藜的铁制暗器"
                wds.等级=30
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x2851A8EC
                wds.大模型资源 = 0x5D501D52
                wds.价格 = 35000
           elseif wd == "无影神针" then
                wds.说明="著名暗器，特点为伤害力较大，极不宜防范"
                wds.等级=40
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x961527F8
                wds.大模型资源 = 0x9E5D7895
                wds.价格 = 85000
           elseif wd == "孔雀翎" then
                wds.说明="传说中的暗器，据说伤人例不虚发"
                wds.等级=50
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xB2657A38
                wds.大模型资源 = 0x454FC9EB
                wds.价格 = 135000
           elseif wd == "含沙射影" then
                wds.说明="传说中的暗器，扣动机关，万针齐射，非常不易防"
                wds.等级=60
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xBE43C96E
                wds.大模型资源 = 0xE4366820
                wds.价格 = 170000
           elseif wd == "回龙摄魂标" then
                wds.说明="名暗器，发出时阴风阵阵，令人防不胜防"
                wds.等级=70
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xCD324F8E
                wds.大模型资源 = 0x3EF314E5
                wds.价格 = 250000
           elseif wd == "寸阴若梦" then
                wds.说明="来自阴府的暗器，甩出时带动惨惨阴风"
                wds.等级=80
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xFB2A8F4C
                wds.大模型资源 = 0x57255C54
                wds.价格 = 350000
           elseif wd == "魔睛子" then
                wds.说明="魔王妖王所用的邪恶暗器，会自己追被攻击人"
                wds.等级=90
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x82A15466
                wds.大模型资源 = 0x69404DEA
                wds.价格 = 450000
           elseif wd == "顺逆神针" then
                wds.说明="此暗器，一旦被伤害，神针不易取出，极为厉害"
                wds.等级=100
                wds.类型="暗器"
                wds.耐久= 100
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xA1716F61
                wds.大模型资源 = 0x7AFB7396
                wds.价格 = 850000
     ---------------------------------------------------------------------乐器
            elseif wd == "木鱼" then
                wds.说明="古代打击乐器，常用于法事。"
                wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x171AE24F
                wds.大模型资源 = 0x6D12F11F
            elseif wd == "编钟" then
                wds.说明="古代乐器，为宫廷和豪门所喜爱。"
                wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x26114A21
                wds.大模型资源 = 0x7836B5A5
            elseif wd == "唢呐" then
                wds.说明="古代乐器，普通百姓所爱。"
                 wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x80310385
                wds.大模型资源 = 0xB5758271
            elseif wd == "笛子" then
                wds.说明="古代乐器，文人雅士所爱。"
                wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0x11BF355E
                wds.大模型资源 = 0x21135929
            elseif wd == "萧" then
                wds.说明="古代乐器，妇人们的最爱。"
               wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x3BAD5575
                wds.大模型资源 = 0x6B2746E7
            elseif wd == "钹" then
                wds.说明="古代乐器，为和尚所爱。"
                wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x2A798918
                wds.大模型资源 = 0x3CCB26E8
            elseif wd == "竖琴" then
                wds.说明="古代弹奏乐器，声音悦耳。"
                wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "WP.FT"
                wds.小模型资源 = 0xAC00E345
                wds.大模型资源 = 0x699C657A
            elseif wd == "琵琶" then
                wds.说明="犹抱琵琶半遮面。"
                 wds.价格=500
                wds.类型 = "乐器"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xE4D3F15A
                wds.大模型资源 = 0x1E2183D9
     ---------------------------------------------------------------------花
            elseif wd == "百合" then
                wds.说明="白色的百合，纯洁的象征。"
                wds.价格=50000
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x206394EF
                wds.大模型资源 = 0xCE65E9FA
            elseif wd == "桃花" then
                wds.说明="人面桃花相映红。"
                wds.价格=500
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x07870048
                wds.大模型资源 = 0xAAB0BA1D
            elseif wd == "兰花" then
                wds.说明="一种高贵的象征。"
                wds.价格=500
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xF26A689A
                wds.大模型资源 = 0xBEAE27D0
            elseif wd == "红玫瑰" then
                wds.说明="代表着送花之人的倾慕与心意。"
                wds.价格=500000
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x3944ABFA
                wds.大模型资源 = 0x614A5011
            elseif wd == "康乃馨" then
                wds.说明="青色的康乃馨，代表着祝福。"
                wds.价格=50000
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0x28DA1984
                wds.大模型资源 = 0x960C3163
            elseif wd == "牡丹" then
                wds.说明="国色天香，代表着人世间的纯洁。"
                wds.价格=500
                wds.类型 = "花"
                wds.资源 = "ZY.FT"
                wds.小模型资源 = 0xFFF1B4CE
                wds.大模型资源 = 0xCFD3A149
 ---------------------------------------------------------------------烹饪
        elseif wd == "长寿面" then
            wds.说明 = "一碗热气腾腾的面条。可以增加召唤兽寿命。"
            wds.类型 = "烹饪"
            wds.功效="恢复召唤兽寿命=品质*2+50"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x9914F83B
            wds.大模型资源 = 0xC5652305
        elseif wd == "佛跳墙" then
            wds.说明="东吴名厨创制的一道菜，味道鲜美。"
            wds.功效="恢复魔法=品质*10+100"
            wds.类别 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x76A70298
            wds.大模型资源 = 0x01F1ED48
        elseif wd == "烤鸭" then
            wds.说明="北方特产，油滑可口。"
            wds.功效="恢复气血=品质*10+100"
            wds.类别 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x900198D8
            wds.大模型资源 = 0xAA17FF00
        elseif wd == "醉生梦死" then
            wds.说明="东吴名厨创制的一道菜，味道鲜美。"
            wds.功效="恢复愤怒值=品质*1,附件疯狂状态3~4回合"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xED576385
            wds.大模型资源 = 0x2B2980FB
        elseif wd == "桂花丸" then
            wds.说明="由糯米和桂花制成的食品。"
            wds.功效="恢复召唤兽寿命=品质*0.5"
            wds.类别 = "消耗"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCCC8D804
            wds.大模型资源 = 0xDF9A8FAB
        elseif wd == "珍露酒" then
            wds.说明="用露水酿制的酒。"
            wds.功效="恢复愤怒值=品质*0.4+10"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x71F4446B
            wds.大模型资源 = 0xEFFC63BB
        elseif wd == "女儿红" then
            wds.说明="名酒，酒香浓烈。"
            wds.功效="恢复愤怒值=20"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x2ECB8558
            wds.大模型资源 = 0x5F376254
        elseif wd == "臭豆腐" then
            wds.说明="常见的一道小菜"
            wds.功效="恢复气血=品质*20+200"
            wds.类别 = "消耗"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7E096A40
            wds.大模型资源 = 0xCC401415
        elseif wd == "烤肉" then
            wds.说明="烤出的肉，非常好吃"
            wds.功效="补充气血、治疗伤势"
            wds.类别 = "消耗"
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x775117AB
            wds.大模型资源 = 0x09D51835
        elseif wd == "翡翠豆腐" then
            wds.说明="有名的菜肴。"
            wds.功效="恢复气血=品质*15+150,恢复魔法=品质*10+100"
            wds.类别 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x11D6858F
            wds.大模型资源 = 0x3178273A
        elseif wd == "豆斋果" then
            wds.说明="素食品,豆面制成的各种水果"
            wds.功效="恢复召唤兽寿命=品质*3，但有3%几率出现食物中毒而减少某些资质"
            wds.类别 = "消耗"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xEA4ED968
            wds.大模型资源 = 0x58130DB3
       elseif wd == "梅花酒" then
            wds.说明="泡梅花的酒，有梅花的清香"
            wds.功效="恢复愤怒值=品质*0.6,昏睡2~3回合"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x717E19FF
            wds.大模型资源 = 0xF8D6B828
        elseif wd == "虎骨酒" then
            wds.说明="虎骨泡制的酒，有药性"
            wds.功效="补充较多愤怒值，导致错乱状态"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0xCD6FDF37
            wds.大模型资源 = 0x46A54492
        elseif wd == "百味酒" then
            wds.说明="泡了很多味药品的药酒"
            wds.功效="补充大量愤怒值，导致中毒和昏睡状态"
            wds.类别 = "酒"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 0x1FA7B36A
            wds.大模型资源 = 0xFFB20F40
        elseif wd == "包子" then
            wds.说明 = "精面粉制作的肉包子"
            wds.类型 = "烹饪"
            wds.类别 = "消耗"
            wds.功效="恢复100点气血"
            wds.价格 = 100
            wds.资源 = "ZY.FT"
            wds.叠加 = true
            wds.小模型资源 = 1457520669
            wds.大模型资源 = 1302655530
        elseif wd == "蛇胆酒" then
            wds.说明 = "蛇胆泡制的酒，有药性"
            wds.类型 = "烹饪"
            wds.类别 = "酒"
            wds.功效="恢复怒气值=品质*1,降低3~4回合的物法防御力"
            wds.资源 = "ZY.FT"
            wds.叠加 = true
            wds.小模型资源 = 0x6607309D
            wds.大模型资源 = 0x67A3D814
 ---------------------------------------------------------------------雪人活动
      --  elseif wd == "小雪块" then
          --  wds.说明 = "在堆雪人活动上交给雪人可以增加堆雪人进度2点"
           -- wds.类型 = "雪人活动"
            --wds.资源 = "item.wdf"
           -- wds.小模型资源 = 0x9BEE2ED1
           -- wds.大模型资源 = 0xBCE0AE05
       -- elseif wd == "大雪块" then
           -- wds.说明 = "在堆雪人活动上交给雪人可以增加堆雪人进度4点"
            --wds.类型 = "雪人活动"
          --  wds.资源 = "item.wdf"
           -- wds.小模型资源 = 0x43F99886
           -- wds.大模型资源 = 0x1E5C9739
     --   elseif wd == "雪人的鼻子" then
           -- wds.说明 = "在堆雪人中雪人成长最后所需要的物品"
           -- wds.类型 = "雪人活动"
           -- wds.资源 = "item.wdf"
          --  wds.小模型资源 = 0x925F02E3
          --  wds.大模型资源 = 0x587AE336
     --   elseif wd == "雪人的帽子" then
           -- wds.说明 = "在堆雪人中雪人成长最后所需要的物品"
           -- wds.类型 = "雪人活动"
           -- wds.资源 = "item.wdf"
            --wds.小模型资源 = 0xA0502DA4
           -- wds.大模型资源 = 0xD487C373
      --  elseif wd == "雪人的眼睛" then
           -- wds.说明 = "在堆雪人中雪人成长最后所需要的物品"
           -- wds.类型 = "雪人活动"
            --wds.资源 = "item.wdf"
            --wds.小模型资源 = 0x5BD31714
            --wds.大模型资源 = 0xCAFDB990
 ----------------------------------耳环-------------------------------灵饰
        elseif wd == "翠叶环" then
            wds.说明 = "以翠色欲滴的嫩叶为耳饰，似乎可见盎然春意。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 60
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x447CB949
            wds.大模型资源 = 0x841B1A0B
        elseif wd == "明月铛" then
            wds.说明 = "珍珠如明月，盈盈有辉光；故名为明月珰。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 80
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD99DA265
            wds.大模型资源 = 0xED4BFC24
        elseif wd == "明月铛" then
            wds.说明 = "珍珠如明月，盈盈有辉光；故名为明月珰。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 80
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD99DA265
            wds.大模型资源 = 0xED4BFC24
        elseif wd == "玉蝶翩" then
            wds.说明 = "彩蝶成双，翩然而舞；有双蝶偶然停驻于灵泉之畔，感其气息，化而为此玉饰。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 =  100
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDC6E80FB
            wds.大模型资源 = 0x0890D479
        elseif wd == "点星芒" then
            wds.说明 = "福禄寿三星以自身星芒点化而成的异石，紫芒莹然，贵不可言。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 120
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x97353053
            wds.大模型资源 = 0x718C161F
        elseif wd == "凤羽流苏" then
            wds.说明 = "灵凤舞九霄，振翼遗金羽；仙人偶得，以火元培之、以仙气养之，又缀以流苏，成一耳饰。静心而听，有凤鸣隐隐，奏大道之声。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 140
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x32534D07
            wds.大模型资源 = 0xC855E37F
        elseif wd == "焰云霞珠" then
            wds.说明 = "后羿射日，十去其九；三足金乌之血化为丹霞，灼烈而烧，火光冲天。许久之后，其血方凝为此珠，赤光熠然。"
            wds.类型 = "灵饰"
            wds.类别 = "耳环"
            wds.等级 = 160
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xED104336
            wds.大模型资源 = 0xF36D99FC
    ----------------------------------佩饰
        elseif wd == "芝兰佩" then
            wds.说明 = "玉树芝兰生于其中，良才美质现于其外。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 = 60
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xE07ABFDD
            wds.大模型资源 = 0x0B70B0FC
        elseif wd == "逸云佩" then
            wds.说明 = "飘逸如云，洒脱如风；君子之行也。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 = 80
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x44B729A2
            wds.大模型资源 = 0x8B0683BC
        elseif wd == "莲音玦" then
            wds.说明 = "仙池清莲所化的玉玦，隐隐可闻其中仙音缭绕，长佩于身，有助修行。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 =  100
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x09A1563F
            wds.大模型资源 = 0x9AD70885
        elseif wd == "相思染" then
            wds.说明 = "一寸相思一寸灰，牛郎织女分离之时滴落相思血泪，遇相思灰而化为鸳鸯之形，故名为“相思染”。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 = 120
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x59D5C544
            wds.大模型资源 = 0x3AB4C90B
        elseif wd == "玄龙苍珀" then
            wds.说明 = "潜龙出世，纵情一啸，声震凌霄宝殿；玉帝罪之，命天将封其龙气入苍珀之中，以为惩罚；又将玄龙苍珀送入红尘，以待有缘人。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 = 140
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x20FA6A66
            wds.大模型资源 = 0x5E226429
        elseif wd == "碧海青天" then
            wds.说明 = "鲲鹏破海而出，水浪滔天，排空怒卷；其化形之蜕则凝为一明玉，聚海中灵气而成奇宝，名曰“碧海青天”。"
            wds.类型 = "灵饰"
            wds.类别 = "佩饰"
            wds.等级 = 160
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x91E363E8
            wds.大模型资源 = 0xBB4BFFCB
    ----------------------------------戒指
        elseif wd == "枫华戒" then
            wds.说明 = "以红枫饰于梧桐木上，炼以为戒。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 = 60
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x8245870F
            wds.大模型资源 = 0xBB24BAC3
        elseif wd == "芙蓉戒" then
            wds.说明 = "涉江采芙蓉，饰于白玉之上，是为芙蓉戒。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 = 80
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9D1BB8EC
            wds.大模型资源 = 0x4694CB26
        elseif wd == "金麟绕" then
            wds.说明 = "金麒麟之影投于其上，使得此戒有护主之能，故名之“金麟绕”。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 =  100
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x629A6E47
            wds.大模型资源 = 0x33446FE4
        elseif wd == "悦碧水" then
            wds.说明 = "智者乐水，观音于水边有所悟，故以佛力凝碧水以为戒。迎光观之，清波荡漾。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 = 120
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x81A29179
            wds.大模型资源 = 0x92E3D4EB
        elseif wd == "九曜光华" then
            wds.说明 = "九曜星君为谢王母娘娘蟠桃会之请，特凝聚九曜光华，成此宝戒；戒中聚星辰之力，华彩耀目，仙气袅然。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 = 140
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x3EC26F1F
            wds.大模型资源 = 0x215BBE3E
        elseif wd == "太虚渺云" then
            wds.说明 = "九霄云气飘渺无踪，自在逍遥；太上老君采撷七七四十九缕云气，以道法凝练九九八十一回，终成此戒，名为“太虚渺云”。"
            wds.类型 = "灵饰"
            wds.类别 = "戒指"
            wds.等级 = 160
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x86693F55
            wds.大模型资源 = 0xC416CB3D
    ----------------------------------手镯
        elseif wd == "香木镯" then
            wds.说明 = "楠木所制，可辟邪护身。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 = 60
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5B5A5A7B
            wds.大模型资源 = 0xBA5B693B
        elseif wd == "翡玉镯" then
            wds.说明 = "山灵水秀，方出璞石；翡玉镯以翡翠打造而成，自有光华。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 = 80
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x59E948D9
            wds.大模型资源 = 0x243475F0
        elseif wd == "墨影扣" then
            wds.说明 = "仙人绘山水，留影玉石中；巧匠采墨影仙石制为手镯，形制奇巧，内蕴仙力。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 =  100
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xFF46F65C
            wds.大模型资源 = 0xCF27B111
        elseif wd == "花映月" then
            wds.说明 = "嫦娥于广寒宫内观月照花、花映月，思念人间冷暖，故凝月华而成此手镯。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 = 120
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x39BFA707
            wds.大模型资源 = 0x9630B1CE
        elseif wd == "金水菩提" then
            wds.说明 = "佛于西天观芸芸众生，有感于凡间诸般喜怒哀乐，故以纯净佛力凝为金水菩提，愿渡红尘之苦。佩戴此镯者，可得佛力为护，趋吉避凶。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 = 140
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD7A907DD
            wds.大模型资源 = 0xCF3E45BF
        elseif wd == "浮雪幻音" then
            wds.说明 = "混沌初开，天地生成；其时也，有初生霜雪自天而降。此雪遇混沌清气后凝而不化，成一奇物，静听则有天地玄音。"
            wds.类型 = "灵饰"
            wds.类别 = "手镯"

            wds.等级 = 160
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB2326701
            wds.大模型资源 = 0xFB5EEB41

  ----------------------炼妖
    elseif wd == "超级金柳露" then
        wds.说明 = "非常神奇的炼妖物品，能够令万物回归原始状态，将一些特殊的怪物变成0级"
        wds.功效 = "将携带等级≥95级的召唤兽变为0级；儿童使用体质+5温饱+50"
        wds.类型 = "炼妖"
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 1572601177
        wds.大模型资源 = 2703428501
        wds.价格 = 500000
    elseif wd == "金柳露" then
        wds.说明 = "一种非常难得仙露，据说有着令万物回归到原始状态的奇效，可以把怪物变成0级，是用来炼妖的神奇物品"
        wds.功效 = "将出站等级≤85级的召唤兽变为0级；儿童使用体质+2温饱+35"
        wds.类型 = "炼妖"
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 2911364506
        wds.大模型资源 = 1443777343
        wds.价格= 100000
    elseif wd == "魔兽要诀" then
        wds.说明 = "记载着上古魔兽特技，可供你的召唤兽学习。"
        wds.类型 = "炼妖"
        wds.资源 = "ZY.FT"
        wds.小模型资源 = 0x4A0644D7
        wds.大模型资源 = 0xAFE21707
    elseif wd == "高级魔兽要诀" then
        wds.说明 = "记载着上古魔兽特技，可供你的召唤兽学习。"
        wds.类型 = "炼妖"
        wds.资源 = "ZY.FT"
           wds.小模型资源 = 0x4A0644D7
           wds.大模型资源 = 0xAFE21707
    elseif wd == "召唤兽内丹" then
        wds.说明 = "上古魔兽遗留下来的精气经万年而成的高级内丹，可供你的召唤兽学习。"
        wds.类型 = "炼妖"
        wds.资源 = "WP.FT"
        wds.小模型资源 = 0x722F3F52
        wds.大模型资源 = 0x46D073B2
        wds.价格 = 500000
    elseif wd == "高级召唤兽内丹" then
        wds.说明 = "上古魔兽遗留下来的精气经万年而成的高级内丹，可供你的召唤兽学习。"
        wds.类型 = "炼妖"
        wds.技能 = "迅敏"
        wds.介绍= "提升召唤兽伤害力与速度，提升效果受召唤兽自身等级影响。"
        wds.资源 = "WP.FT"
        wds.小模型资源 = 0x02A776DE
        wds.大模型资源 = 0x4E43EBFF
        wds.价格 = 1500000
 ------------------------1级符石
        elseif wd =="冰符石"  then
            wds.等级 = 1
            wds.颜色 = "红色"
            wds.体质 = 1
            wds.小模型资源 = 0x934E9733
            wds.大模型资源 = 0x6A962ED6
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="土符石"  then
            wds.等级 = 1
            wds.颜色 = "红色"
            wds.耐力 = 1
            wds.小模型资源 = 0x3E964C29
            wds.大模型资源 = 0x7E78A604
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="雷符石"  then
            wds.等级 = 1
            wds.颜色 = "红色"
            wds.敏捷 = 1
            wds.小模型资源 = 0x67B1F794
            wds.大模型资源 = 0x953A5574
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="电符石"  then
            wds.等级 = 1
            wds.颜色 = "红色"
            wds.法术防御 = 1
            wds.小模型资源 = 0xFCCD6BDA
            wds.大模型资源 = 0x45E07435
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="风符石"  then
            wds.等级 = 1
            wds.颜色 = "黄色"
            wds.魔力 = 1
            wds.小模型资源 = 0x9D282EE9
            wds.大模型资源 = 0x4180FB8D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="炎符石"  then
            wds.等级 = 1
            wds.颜色 = "黄色"
            wds.力量 = 1
            wds.小模型资源 = 0x21B13715
            wds.大模型资源 = 0xEEAF3D8E
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="火符石"  then
            wds.等级 = 1
            wds.颜色 = "黄色"
            wds.法术伤害 = 1
            wds.小模型资源 = 0xDC81D311
            wds.大模型资源 = 0x71E54CDB
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
 -----------------------------------------------二级符石
        elseif wd =="红云符石"  then----
            wds.等级 = 2
            wds.颜色 = "红色"
            wds.气血 = 10
            wds.小模型资源 = 0x76BEE0E4 --
            wds.大模型资源 = 0x74261456
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="碧玉符石"  then
            wds.等级 = 2
            wds.颜色 = "红色"
            wds.防御 = 3
            wds.小模型资源 = 0x43676CDF
            wds.大模型资源 = 0x710AAA1D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="金光符石"  then
            wds.等级 = 2
            wds.颜色 = "蓝色"
            wds.命中 = 4
            wds.小模型资源 = 0x5737526C --
            wds.大模型资源 = 0x727F812B
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="天神符石"  then
            wds.等级 = 2
            wds.颜色 = "蓝色"
            wds.伤害 = 1.5
            wds.小模型资源 = 0x81D8C883 --
            wds.大模型资源 = 0x24549F76
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="飘渺符石"  then
            wds.等级 = 2
            wds.颜色 = "绿色"
            wds.灵力 = 1.5
            wds.小模型资源 = 0x6B9B8016--
            wds.大模型资源 = 0x463956C5
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="天仙符石"  then
            wds.等级 = 2
            wds.颜色 = "绿色"
            wds.魔法 = 6
            wds.小模型资源 = 0xE059F69F
            wds.大模型资源 = 0xCC940789
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="霞光符石"  then
            wds.等级 = 2
            wds.颜色 = "绿色"
            wds.速度 = 1.5
            wds.小模型资源 = 0xC32FCFBF
            wds.大模型资源 = 0xC94732AA
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="逍遥符石"  then
            wds.等级 = 2
            wds.颜色 = "绿色"
            wds.躲避 = 3
            wds.小模型资源 = 0x096B0982 --
            wds.大模型资源 = 0x8CCCA725
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
 ---------------------------------------三级符石
 -------------------三级蓝色浮石
        elseif wd =="三级未激活符石"  then
            wds.等级 = 3
            wds.小模型资源 = 0xB39A2CAB
            wds.大模型资源 = 0x2675DF2C
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="虹珀符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.魔法 = 6
            wds.小模型资源 = 0xDFD61967
            wds.大模型资源 = 0xAEB5940D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="陌影符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.命中 =4
            wds.小模型资源 = 0x6CCF8F49
            wds.大模型资源 = 0x2207D0E7
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="北冥符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.伤害 = 1.5
            wds.小模型资源 = 0x90B92A2C
            wds.大模型资源 = 0x8197B629
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="灵月符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.防御 = 3
            wds.小模型资源 = 0xFEA25D6F
            wds.大模型资源 = 0xD83B95B5
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="锦瑟符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.速度 = 1.5
            wds.小模型资源 = 0x22CBF322
            wds.大模型资源 = 0xA9D94F23
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="银光符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.躲避 = 3
            wds.小模型资源 = 0xFC3CDE97
            wds.大模型资源 = 0x23F7A22E
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="清心符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.灵力 = 1.5
            wds.小模型资源 = 0x5610AE50
            wds.大模型资源 = 0x2E6B730A
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="星辰符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.法术伤害 = 2
            wds.小模型资源 = 0x2E893EE4
            wds.大模型资源 = 0x41C0E80A
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="雪月符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.法术防御 = 2
            wds.小模型资源 = 0xEF64D308
            wds.大模型资源 = 0x1088694D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="玄魂符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.体质 = 1
            wds.固定伤害 = 2
            wds.小模型资源 = 0x35232317
            wds.大模型资源 = 0xBCA29B7A
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="乾坤符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.气血 = 10
            wds.小模型资源 = 0x07F4F2A9
            wds.大模型资源 = 0x44861CDA
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="珍珀符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.魔法 = 6
            wds.小模型资源 = 0x5CF34122
            wds.大模型资源 = 0x0DF8358B
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="银竹符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.命中 = 4
            wds.小模型资源 = 0x03C1B72B
            wds.大模型资源 = 0xCCE17B90
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="神川符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.伤害 = 1.5
            wds.小模型资源 = 0xF95B369D
            wds.大模型资源 = 0x240C05AE
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="玲珑符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.速度 = 1.5
            wds.小模型资源 = 0x06AACFD5
            wds.大模型资源 = 0x22F35AB7
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="暮影符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.躲避 = 3
            wds.小模型资源 = 0x066A5CAF
            wds.大模型资源 = 0xA1EFCBCD
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="天珍符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.灵力 = 1.5
            wds.小模型资源 = 0xDD550E98
            wds.大模型资源 = 0x5A1058B4
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="九影符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.法术伤害 = 2
            wds.小模型资源 = 0xC14E158C
            wds.大模型资源 = 0x74570AD9
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="百冥符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.法术防御 = 2
            wds.小模型资源 = 0xE7C31FAB
            wds.大模型资源 = 0x204A7770
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="莫念符石"  then
            wds.等级 = 3
            wds.颜色 = "蓝色"
            wds.耐力 = 1
            wds.固定伤害 = 2
            wds.小模型资源 = 0x0C337A06
            wds.大模型资源 = 0x7E8A4D54
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
 ----------------------三级白色符石
        elseif wd =="醉魂符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.气血 = 10
            wds.小模型资源 = 0xDF29F445
            wds.大模型资源 = 0x684A5B33
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="玄羽符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.命中 = 4
            wds.小模型资源 = 0x2BFB0E6E
            wds.大模型资源 = 0xFD8EB3CF
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="霸风符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.伤害 = 1.5
            wds.小模型资源 = 0x579C9F2A
            wds.大模型资源 = 0x5AA0CA98
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="无相符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.防御 = 3
            wds.小模型资源 = 0x4ADA8502
            wds.大模型资源 = 0x5FBDBE85
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="无极符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.速度 = 1.5
            wds.小模型资源 = 0x1ED49DA6
            wds.大模型资源 = 0x8A6D58ED
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="紫晶符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.躲避 = 3
            wds.小模型资源 = 0x53616D8A
            wds.大模型资源 = 0xA7782996
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="圣火符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.固定伤害 = 2
            wds.小模型资源 = 0xCA106E2F
            wds.大模型资源 = 0x74D36FFF
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="昔光符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.魔法 = 6
            wds.小模型资源 = 0x6FC365B8
            wds.大模型资源 = 0xCBD03425
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="子蚀符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.法术防御 = 2
            wds.小模型资源 = 0xF688BFDE
            wds.大模型资源 = 0x93C04775
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="流风符石"  then
            wds.等级 = 3
            wds.颜色 = "白色"
            wds.魔力 = 1
            wds.法术伤害 = 2
            wds.小模型资源 = 0x9D0D30EF
            wds.大模型资源 = 0x9B44A508
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        ---------------------三级黑色符石
        elseif wd =="地炎符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.气血 = 10
            wds.小模型资源 = 0x9577140F
            wds.大模型资源 = 0x2B2257FF
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="素影符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 =1
            wds.魔法 = 6
            wds.小模型资源 = 0xED15475A
            wds.大模型资源 = 0x17DE2A61
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="燕灵符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.防御 = 3
            wds.小模型资源 = 0xEE41CD63
            wds.大模型资源 = 0xD5F14ADC
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="降龙符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.速度 = 1.5
            wds.小模型资源 = 0xEA2DFDAF
            wds.大模型资源 = 0x7F18A6FB
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="苍玉符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.躲避 = 3
            wds.小模型资源 = 0x867985EB
            wds.大模型资源 = 0x5336983C
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="流魂符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.灵力 = 1.5
            wds.小模型资源 = 0xF0888E17
            wds.大模型资源 = 0xF21E1F30
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="九凤符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.法术防御 = 2
            wds.小模型资源 = 0x7CCD1DFA
            wds.大模型资源 = 0xEC558958
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="墨陀符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.法术伤害 = 2
            wds.小模型资源 = 0x03F791B1
            wds.大模型资源 = 0x44ED9122
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="南夕符石"  then
            wds.等级 = 3
            wds.颜色 = "黑色"
            wds.力量 = 1
            wds.固定伤害 = 2
            wds.小模型资源 = 0x5DBC1A4E
            wds.大模型资源 = 0xBAB64C34
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
 --------------------三级紫色符石
        elseif wd =="引幽符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.气血 = 10
            wds.小模型资源 = 0xA3130920
            wds.大模型资源 = 0xAC5683E0
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="波涛符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.魔法 = 6
            wds.小模型资源 = 0x5DA7088A
            wds.大模型资源 = 0xDFD1841C
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="铃星符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.命中 = 4
            wds.小模型资源 = 0xAC6C9B58
            wds.大模型资源 = 0x7D44AFFC
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="狂念符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.伤害 = 1.5
            wds.小模型资源 = 0x64DD0A08
            wds.大模型资源 = 0x5D8AFACD
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="乱花符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.灵力 = 1.5
            wds.小模型资源 = 0x9722DD59
            wds.大模型资源 = 0xFAE16C2E
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="幽月符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.法术伤害 = 2
            wds.小模型资源 = 0x08EDB338
            wds.大模型资源 = 0x5C16DDD5
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="两仪符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.法术防御 = 2
            wds.小模型资源 = 0x6149F028
            wds.大模型资源 = 0xB0A5618D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="七情符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.固定伤害 = 2
            wds.小模型资源 = 0x4FBA3175
            wds.大模型资源 = 0x35DC8D80
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"
        elseif wd =="召影符石"  then
            wds.等级 = 3
            wds.颜色 = "紫色"
            wds.敏捷 = 1
            wds.防御 = 3
            wds.小模型资源 = 0x7AF9E2F7
            wds.大模型资源 = 0x0F28A91D
            wds.资源 ="WP.FT"
            wds.类型 = "符石"
            wds.说明 ="铭刻着神秘符文的上古神石，散落三界，具备神秘的力量。"

 ----------------强化石
        elseif wd == "青龙石" then
            wds.说明 = "带有灵气的石头，是强化装备所需的原料之一"
            wds.类型 = "强化石"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2289281503
            wds.大模型资源 = 603616920
            wds.叠加=true
        elseif wd == "白虎石" then
            wds.说明 = "带有灵气的石头，是强化装备所需的原料之一"
            wds.类型 = "强化石"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 2867857574
            wds.大模型资源 = 1492919136
             wds.叠加=true
        elseif wd == "朱雀石" then
            wds.说明 = "带有灵气的石头，是强化装备所需的原料之一"
            wds.类型 = "强化石"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 650195920
            wds.大模型资源 = 3642232128
             wds.叠加=true
        elseif wd == "玄武石" then
            wds.说明 = "带有灵气的石头，是强化装备所需的原料之一"
            wds.类型 = "强化石"
            wds.资源 = "ZY.FT"
            wds.小模型资源 = 727733710
            wds.大模型资源 = 3146274026
             wds.叠加=true
        elseif wd == "怪物卡片" then
            wds.说明 = "一张记载着某种怪物造型的神奇卡片，使用后可以变化身形"

 ---------------------------------------------------------------------------------召唤兽饰品--------------------------------------------------------------------------

        elseif wd == "海星饰品" then
            wds.说明 = "适合海星使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7D676C74
            wds.大模型资源 = 0xEA62CA32
        elseif wd == "章鱼饰品" then
            wds.说明 = "适合章鱼使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1B15731D
            wds.大模型资源 = 0x6CD2B633
        elseif wd == "泡泡饰品" then
            wds.说明 = "适合泡泡使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xE28D168D
            wds.大模型资源 = 0xC0639106
        elseif wd == "山贼饰品" then
            wds.说明 = "适合山贼使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x4B1A9A48
            wds.大模型资源 = 0x0E709527
        elseif wd == "蚌精饰品" then
            wds.说明 = "适合蚌精使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9C8799D6
            wds.大模型资源 = 0xCE212384
        elseif wd == "进阶蝴蝶仙子饰品" then
            wds.说明 = "适合进阶蝴蝶仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCF439FF6
            wds.大模型资源 = 0xF6C2E868
        elseif wd == "进阶黑山老妖饰品" then
            wds.说明 = "适合进阶黑山老妖使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7F0984BC
            wds.大模型资源 = 0xF62D1967
        elseif wd == "古代瑞兽饰品" then
            wds.说明 = "适合古代瑞兽使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x671967E1
            wds.大模型资源 = 0x36648B93
        elseif wd == "进阶雷鸟人饰品" then
            wds.说明 = "适合进阶雷鸟人使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x91781353
            wds.大模型资源 = 0xC8DB8181
        elseif wd == "白熊饰品" then
            wds.说明 = "适合白熊使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x0BFC6BDD
            wds.大模型资源 = 0x61077013
        elseif wd == "风伯饰品" then
            wds.说明 = "适合风伯使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x518471FD
            wds.大模型资源 = 0x011DF58C
        elseif wd == "进阶地狱战神饰品" then
            wds.说明 = "适合进阶地狱战神使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDDEDA244
            wds.大模型资源 = 0x0550F690
                   elseif wd == "夜罗刹饰品" then
            wds.说明 = "适合夜罗刹使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDDEDA244
            wds.大模型资源 = 0x0550F690
              elseif wd == "进阶鬼将饰品" then
            wds.说明 = "适合进阶鬼将使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDDEDA244
            wds.大模型资源 = 0x0550F690
        elseif wd == "进阶天兵饰品" then
            wds.说明 = "适合进阶天兵使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB90799EA
            wds.大模型资源 = 0x794C33D0
        elseif wd == "机关兽饰品" then
            wds.说明 = "适合机关兽使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x082DF2E8
            wds.大模型资源 = 0xAA6521F9
        elseif wd == "百足将军饰品" then
            wds.说明 = "适合百足将军使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5C295A3C
            wds.大模型资源 = 0xC3B83C79
        elseif wd == "金身罗汉饰品" then
            wds.说明 = "适合金身罗汉使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA545ABC8
            wds.大模型资源 = 0xD05BA36C
        elseif wd == "进阶巨力神猿饰品" then
            wds.说明 = "适合进阶巨力神猿使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x07C4984D
            wds.大模型资源 = 0x4A26363A
        elseif wd == "幽灵饰品" then
            wds.说明 = "适合幽灵使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x0A7B4FE9
            wds.大模型资源 = 0x34FB2CD2
        elseif wd == "雾中仙饰品" then
            wds.说明 = "适合雾中仙使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x0DB6DE65
            wds.大模型资源 = 0x5705837D
        elseif wd == "进阶灵符女娲饰品" then
            wds.说明 = "适合进阶灵符女娲使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x0EBAA59F
            wds.大模型资源 = 0xEE608A2F
        elseif wd == "进阶芙蓉仙子饰品" then
            wds.说明 = "适合进阶芙蓉仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x105046E1
            wds.大模型资源 = 0xD977475C
        elseif wd == "长眉灵猴饰品" then
            wds.说明 = "适合长眉灵猴使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x11CF5CD9
            wds.大模型资源 = 0xAF1E223E
        elseif wd == "猪八戒饰品" then
            wds.说明 = "适合猪八戒使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1218BD6F
            wds.大模型资源 = 0x138924C9
        elseif wd == "机关鸟饰品" then
            wds.说明 = "适合机关鸟使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x12425B20
            wds.大模型资源 = 0x1DE65975
        elseif wd == "画魂饰品" then
            wds.说明 = "适合画魂使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xADE9E51F
            wds.大模型资源 = 0x13FF213A
        elseif wd == "进阶百足将军饰品" then
            wds.说明 = "适合进阶百足将军使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xDFCDF195
            wds.大模型资源 = 0x15F647DD
        elseif wd == "进阶炎魔神饰品" then
            wds.说明 = "适合进阶炎魔神使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x17A6BC23
            wds.大模型资源 = 0x9BD59357
        elseif wd == "碧海夜叉饰品" then
            wds.说明 = "适合碧海夜叉使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1A80ECB4
            wds.大模型资源 = 0x13725FAD
        elseif wd == "蝎子精饰品" then
            wds.说明 = "适合蝎子精使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xC184C139
            wds.大模型资源 = 0xBE6B2487
        elseif wd == "进阶毗舍童子饰品" then
            wds.说明 = "适合进阶毗舍童子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x719F0348
            wds.大模型资源 = 0x1E3090FA
        elseif wd == "进阶噬天虎饰品" then
            wds.说明 = "适合进阶噬天虎使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB0E4CE03
            wds.大模型资源 = 0x205765E7
        elseif wd == "进阶雨师饰品" then
            wds.说明 = "适合进阶雨师使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x2F135077
            wds.大模型资源 = 0xE4D1B1A7
        elseif wd == "巡游天神饰品" then
            wds.说明 = "适合巡游天神使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5F195813
            wds.大模型资源 = 0x3773E8DE
        elseif wd == "进阶巡游天神饰品" then
            wds.说明 = "适合进阶巡游天神使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x3EF393FF
            wds.大模型资源 = 0x82A3FA26
        elseif wd == "如意仙子饰品" then
            wds.说明 = "适合如意仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x4062E575
            wds.大模型资源 = 0x7C061EBB
        elseif wd == "进阶如意仙子饰品" then
            wds.说明 = "适合进阶如意仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x43F8CB57
            wds.大模型资源 = 0xD52C3F3E
        elseif wd == "机关人战车饰品" then
            wds.说明 = "适合机关人战车使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xE9AE67C2
            wds.大模型资源 = 0x45169512
        elseif wd == "进阶蝎子精饰品" then
            wds.说明 = "适合进阶蝎子精使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB680C138
            wds.大模型资源 = 0x46B334CE
        elseif wd == "进阶犀牛将军兽饰品" then
            wds.说明 = "适合进阶犀牛将军兽使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x46BC3F59
            wds.大模型资源 = 0x9F407319
        elseif wd == "进阶凤凰饰品" then
            wds.说明 = "适合进阶凤凰使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xED3634F0
            wds.大模型资源 = 0x484141E4
        elseif wd == "进阶机关人饰品" then
            wds.说明 = "适合进阶机关人使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x490C0345
            wds.大模型资源 = 0x76714710
        elseif wd == "鼠先锋饰品" then
            wds.说明 = "适合鼠先锋使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xC0AB355B
            wds.大模型资源 = 0x4E0CED3B
        elseif wd == "进阶修罗傀儡妖饰品" then
            wds.说明 = "适合进阶修罗傀儡妖使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7C45DC26
            wds.大模型资源 = 0x4E3E60DF
        elseif wd == "持国巡守饰品" then
            wds.说明 = "适合持国巡守饰品使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x53C8C818
            wds.大模型资源 = 0x2C22EBCF
        elseif wd == "进阶鼠先锋饰品" then
            wds.说明 = "适合进阶鼠先锋使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA8E24F81
            wds.大模型资源 = 0x5925AB70
        elseif wd == "百足将军饰品" then
            wds.说明 = "适合百足将军使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5C295A3C
            wds.大模型资源 = 0x5EA142D4
        elseif wd == "真陀护法饰品" then
            wds.说明 = "适合真陀护法使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x18918659
            wds.大模型资源 = 0x5CD1BDD7
        elseif wd == "星灵仙子饰品" then
            wds.说明 = "适合星灵仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x6E7B1B52
            wds.大模型资源 = 0x96710CDD
        elseif wd == "吸血鬼饰品" then
            wds.说明 = "适合星灵仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1D124E01
            wds.大模型资源 = 0x0C01D076
        elseif wd == "灵符女娲饰品" then
            wds.说明 = "适合灵符女娲使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x43564039
            wds.大模型资源 = 0xDF93EA17
        elseif wd == "进阶阴阳伞饰品" then
            wds.说明 = "适合进阶阴阳伞使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xD2B5D50A
            wds.大模型资源 = 0x62454D13
        elseif wd == "进阶律法女娲饰品" then
            wds.说明 = "适合进阶律法女娲使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x7E42E492
            wds.大模型资源 = 0xE3675B07
        elseif wd == "进阶鲛人饰品" then
            wds.说明 = "适合进阶鲛人使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCEC72909
            wds.大模型资源 = 0xC331017E
        elseif wd == "进阶蛟龙饰品" then
            wds.说明 = "适合进阶蛟龙使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA74A167C
            wds.大模型资源 = 0xBB1EA168
        elseif wd == "进阶雨师饰品" then
            wds.说明 = "适合进阶雨师使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x2F135077
            wds.大模型资源 = 0xE4D1B1A7
        elseif wd == "进阶碧水夜叉饰品" then
            wds.说明 = "适合进阶碧水夜叉使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9B4BF70D
            wds.大模型资源 = 0xFFCDFCE7
        elseif wd == "碧水夜叉饰品" then
            wds.说明 = "适合碧水夜叉使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x1A80ECB4
            wds.大模型资源 = 0x13725FAD
        elseif wd == "阴阳伞饰品" then
            wds.说明 = "适合阴阳伞使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xA166C299
            wds.大模型资源 = 0xDB57031E
        elseif wd == "天兵饰品" then
            wds.说明 = "适合天兵使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x58E19A2A
            wds.大模型资源 = 0x794C33D0
        elseif wd == "进阶天兵饰品" then
            wds.说明 = "适合进阶天兵使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB90799EA
            wds.大模型资源 = 0x97C67437
        elseif wd == "蚌精饰品" then
            wds.说明 = "适合蚌精使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9C8799D6
            wds.大模型资源 = 0xCE212384
        elseif wd == "狂豹人形饰品" then
            wds.说明 = "适合狂豹人使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xE7F09028
            wds.大模型资源 = 0x4FF4D46D
        elseif wd == "进阶连弩车饰品" then
            wds.说明 = "适合进阶连弩车使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x9EF97739
            wds.大模型资源 = 0xB2CBE8EE
        elseif wd == "巴蛇饰品" then
            wds.说明 = "适合巴蛇使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x97B5C8B3
            wds.大模型资源 = 0xBBDDD02C
        elseif wd == "狂豹兽形饰品" then
            wds.说明 = "适合狂豹兽使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x05C9E305
            wds.大模型资源 = 0x953F4F60
        elseif wd == "进阶龙龟饰品" then
            wds.说明 = "适合进阶龙龟使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x70253BD1
            wds.大模型资源 = 0x8B86B3A4
        elseif wd == "红萼仙子饰品" then
            wds.说明 = "适合红萼仙子使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x4F4DABF2
            wds.大模型资源 = 0x01235F2E
        elseif wd == "炎魔神饰品" then
            wds.说明 = "适合炎魔神使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xCD208493
            wds.大模型资源 = 0x6AC5801C
        elseif wd == "幽萤娃娃饰品" then
            wds.说明 = "适合幽萤娃娃使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x5DFF4E29
            wds.大模型资源 = 0xAAEEB373
        elseif wd == "进阶画魂饰品" then
            wds.说明 = "适合进阶画魂使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xAC637ECB
            wds.大模型资源 = 0x66E9D6B6
        elseif wd == "画魂饰品" then
            wds.说明 = "适合画魂使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xADE9E51F
            wds.大模型资源 = 0x13FF213A
        elseif wd == "鬼将饰品" then
            wds.说明 = "适合鬼将使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x107841EB
            wds.大模型资源 = 0x836F2B2D
        elseif wd == "进阶古代瑞兽饰品" then
            wds.说明 = "适合进阶古代瑞兽使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0x99E44027
            wds.大模型资源 = 0x8FE46833
        elseif wd == "进阶大力金刚饰品" then
            wds.说明 = "适合进阶大力金刚使用的饰品，给予其使用后可以增加新的形态装饰效果。"
            wds.类型 = "召唤兽饰品"
            wds.类别 = 2
            wds[4] = 0
            wds.资源 = "WP.FT"
            wds.小模型资源 = 0xB499FF30
            wds.大模型资源 = 0x693E2418

        end
    return wds
end

function 引擎.取变身卡(类型)
    local 属性 = {}
 -------------一----------------------------------------------级变身卡
        if 类型== "海星" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xE709CDAD
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
        elseif 类型== "狸" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xA60EB0D9
        elseif 类型== "黄色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x4A975EF6
        elseif 类型== "粉色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x55797346
        elseif 类型== "红色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x7C73C57A
        elseif 类型== "紫色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x8DEA97F3
        elseif 类型== "黑色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x94C4C4ED
        elseif 类型== "灰色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x99FEA29E
        elseif 类型== "青色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xC8351872
        elseif 类型== "蓝色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xCDC5ECDC
        elseif 类型== "绿色泡泡" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xFFA4B4B8
        elseif 类型== "章鱼" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xABB68989
            ----------------------------------------------------------------二级变身卡
        elseif 类型== "大海龟" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x3C7B89E8
        elseif 类型== "大蝙蝠" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x2481DFCC
        elseif 类型== "赌徒" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x6BE81A68
        elseif 类型== "海毛虫" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x3BD0B554
        elseif 类型== "护卫" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x7003F174
        elseif 类型== "巨蛙" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x98E3377F
        elseif 类型== "强盗" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0xD5C2566E
        elseif 类型== "山贼" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x5F7346A8
        elseif 类型== "树怪" then
            属性.卡片等级 = 1
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 ="无"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x4A028BEE
            属性.大模型 = 0x4ED5C9C4
 ----------------------------------------------------------------二级变身卡
        elseif 类型== "蛤蟆精" then
            属性.卡片等级 = 2
            属性.三维 = "防御"
            属性.数值= 0.15
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="毒"
            属性.属性 ="-15%防御"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0x8A53158C
        elseif 类型== "黑熊" then
            属性.卡片等级 = 2
            属性.三维 = "灵力"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="防御"
            属性.属性 ="-5%灵力"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0xD4D2660A
        elseif 类型== "狐狸精" then
            属性.卡片等级 = 2
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="感知"
            属性.属性 ="-5%气血"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0xDC14E699
        elseif 类型== "花妖" then
            属性.卡片等级 = 2
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="慧根"
            属性.属性 ="无"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0xD294444C
        elseif 类型== "老虎" then
            属性.卡片等级 = 2
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="强力"
            属性.属性 ="-10%速度"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0x463F3E9B
        elseif 类型== "羊头怪" then
            属性.卡片等级 = 2
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="幸运"
            属性.属性 ="-10%气血"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0x8F19EF2A
        elseif 类型== "骷髅怪" then
            属性.卡片等级 = 2
            属性.三维 = "最大气血"
            属性.数值= 0.2
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "一级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="-20%气血"
            属性.小模型 = 0xBB35E1EE
            属性.大模型 = 0xD0BE29D3
 ----------------------------------------------------------------三级变身卡
        elseif 类型== "狼" then
            属性.卡片等级 = 3
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-10%气血"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0x92B59426
        elseif 类型== "牛妖" then
            属性.卡片等级 = 3
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="反击"
            属性.属性 ="-5%防御"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0x3AF799AA
        elseif 类型== "虾兵" then
            属性.卡片等级 = 3
            属性.三维 = "伤害"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="-10%伤害力"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0xE89179D1
        elseif 类型== "小龙女" then
            属性.卡片等级 = 3
            属性.三维 = "伤害"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="驱鬼"
            属性.属性 ="-5%伤害力"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0xE05E7656
        elseif 类型== "蟹将" then
            属性.卡片等级 = 3
            属性.三维 = "速度"
            属性.数值= 0.12
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="连击"
            属性.属性 ="-12%速度"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0x65DBE48A
        elseif 类型== "野鬼" then
            属性.卡片等级 = 3
            属性.三维 = "灵力"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "二级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="-5%灵力"
            属性.小模型 = 0xE7FC64D2
            属性.大模型 = 0xB90EC617
 ----------------------------------------------------------------四级变身卡
        elseif 类型== "龟丞相" then
            属性.卡片等级 = 4
            属性.三维 = "最大气血"
            属性.数值= 0.07
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="无"
            属性.属性 ="+7%气血"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0x67E0006E
        elseif 类型== "黑熊精" then
            属性.卡片等级 = 4
            属性.三维 = "防御"
            属性.数值= 0.1
            属性.类型 =3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="反震"
            属性.属性 ="-10%防御"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0x35BDCFC8
        elseif 类型== "僵尸" then
            属性.卡片等级 = 4
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="弱点雷"
            属性.属性 ="+10%气血"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0xC7B126C6
        elseif 类型== "马面" then
            属性.卡片等级 = 4
            属性.三维 = "伤害"
            属性.数值= 0.08
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="-8%伤害力"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0xCA322977
        elseif 类型== "牛头" then
            属性.卡片等级 = 4
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="招架"
            属性.属性 ="无"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0x06971D21
        elseif 类型== "兔子怪" then
            属性.卡片等级 = 4
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="-10%气血"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0xA1B8ACD0
        elseif 类型== "蜘蛛精" then
            属性.卡片等级 = 4
            属性.三维 = "防御"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "三级变化之术"
            属性.技能 ="毒"
            属性.属性 ="-10%防御"
            属性.小模型 = 0xA00740F6
            属性.大模型 = 0xD2C2093D
 ----------------------------------------------------------------五级变身卡
        elseif 类型== "白熊" then
            属性.卡片等级 = 5
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="迟钝"
            属性.属性 ="无"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x7092E7F5
        elseif 类型== "进阶白熊" then
            属性.卡片等级 = 5
            属性.三维 = "最大气血"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="迟钝"
            属性.属性 ="+20气血"
            属性.小模型 = 0x21838782
            属性.大模型 = 0xD822B68E
        elseif 类型== "古代瑞兽" then
            属性.卡片等级 = 5
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="反震"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x7728C3B2
        elseif 类型== "进阶古代瑞兽" then
            属性.卡片等级 = 5
            属性.三维 = "最大气血"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="反震"
            属性.属性 ="-9%气血"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x00EEC2D6
        elseif 类型== "黑山老妖" then
            属性.卡片等级 = 5
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-10%速度"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x95FDC90D
        elseif 类型== "进阶黑山老妖" then
            属性.卡片等级 = 5
            属性.三维 = "速度"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-9%速度"
            属性.小模型 = 0x21838782
            属性.大模型 = 0xA2985168
        elseif 类型== "蝴蝶仙子" then
            属性.卡片等级 = 5
            属性.三维 = "防御"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="魔之心"
            属性.属性 ="-10%防御"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x976975FB
        elseif 类型== "进阶蝴蝶仙子" then
            属性.卡片等级 = 5
            属性.三维 = "防御"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="魔之心"
            属性.属性 ="-9%防御"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x9E4DD433
        elseif 类型== "雷鸟人" then
            属性.卡片等级 = 5
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="飞行"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x21838782
            属性.大模型 = 0x4E646343
        elseif 类型== "进阶雷鸟人" then
            属性.卡片等级 = 5
            属性.三维 = "最大气血"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="飞行"
            属性.属性 ="-9%气血"
            属性.小模型 = 0x21838782
            属性.大模型 = 0xADA260F6
 ----------------------------------------------------------------六级变身卡
        elseif 类型== "地狱战神" then
            属性.卡片等级 = 6
            属性.三维 = "气血"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="连击"
            属性.属性 ="-5%气血"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xB5FE5920
        elseif 类型== "进阶地狱战神" then
            属性.卡片等级 = 6
            属性.三维 = "最大气血"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="连击"
            属性.属性 ="-4%气血"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x80166047
        elseif 类型== "风伯" then
            属性.卡片等级 = 6
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="-5%防御"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xDF2F3035
        elseif 类型== "进阶风伯" then
            属性.卡片等级 = 6
            属性.三维 = "防御"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="-4%防御"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xF9E87CB3
        elseif 类型== "天兵" then
            属性.卡片等级 = 6
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="防御"
            属性.属性 ="无"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x9AB7515F
        elseif 类型== "进阶天兵" then
            属性.卡片等级 = 6
            属性.三维 = "防御"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="防御"
            属性.属性 ="+10防御"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x12CCC85D
        elseif 类型== "天将" then
            属性.卡片等级 = 6
            属性.三维 = "灵力"
            属性.数值= 0.08
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="强力"
            属性.属性 ="-8%灵力"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x7E86C2A9
        elseif 类型== "进阶天将" then
            属性.卡片等级 = 6
            属性.三维 = "灵力"
            属性.数值= 0.07
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="强力"
            属性.属性 ="-7%灵力"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x78BDE1FE
        elseif 类型== "凤凰" then
            属性.卡片等级 = 6
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="飞行"
            属性.属性 ="无"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x2A4159F7
        elseif 类型== "进阶凤凰" then
            属性.卡片等级 = 6
            属性.三维 = "速度"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="飞行"
            属性.属性 ="+10速度"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x129382ED
        elseif 类型== "雨师" then
            属性.卡片等级 = 6
            属性.三维 = "灵力"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="无"
            属性.属性 ="+20灵力"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xF53D7AE7
        elseif 类型== "进阶雨师" then
            属性.卡片等级 = 6
            属性.三维 = "灵力"
            属性.数值= 30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="无"
            属性.属性 ="+30灵力"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x1CE04232
        elseif 类型== "蛟龙" then
            属性.卡片等级 = 6
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="感知"
            属性.属性 ="无"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xD4442C3A
        elseif 类型== "进阶蛟龙" then
            属性.卡片等级 = 6
            属性.三维 = "速度"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="感知"
            属性.属性 ="+10速度"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xDC247E87
        elseif 类型== "蚌精" then
            属性.卡片等级 = 6
            属性.三维 = "防御"
            属性.数值= 30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="无"
            属性.属性 ="+30防御"
            属性.小模型 = 0xF1C84EB5

            属性.大模型 = 0x4BAA9CBE
        elseif 类型== "进阶蚌精" then
            属性.卡片等级 = 6
            属性.三维 = "防御"
            属性.数值= 40
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="无"
            属性.属性 ="+40防御"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xD52DECA1
        elseif 类型== "进阶碧水夜叉" then
            属性.卡片等级 = 6
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="反震"
            属性.属性 ="无"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x8B74E4E2
        elseif 类型== "碧水夜叉" then
            属性.卡片等级 = 6
            属性.三维 = "最大气血"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="反震"
            属性.属性 ="+20气血"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0x22B8518F
        elseif 类型== "鲛人" then------------------
            属性.卡片等级 = 6
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="移花接木"
            属性.属性 ="无"
            属性.小模型 = 0xF1C84EB5
          ---  属性.大模型 = 0x
        elseif 类型== "进阶鲛人" then
            属性.卡片等级 = 6
            属性.三维 = "最大气血"
            属性.数值= 0.03
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "四级变化之术"
            属性.技能 ="移花接木"
            属性.属性 ="+3%气血"
            属性.小模型 = 0xF1C84EB5
            属性.大模型 = 0xBF03B385
 ----------------------------------------------------------------七级变身卡
        elseif 类型== "百足将军" then
            属性.卡片等级 = 7
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="无"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xAA8E51A5
        elseif 类型== "进阶百足将军" then
            属性.卡片等级 = 7
            属性.三维 = "伤害"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="+20伤害"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x53FA422E
        elseif 类型== "锦毛貂精" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="-5%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xEABE811E
        elseif 类型== "进阶锦毛貂精" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="-4%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xC37485FE
        elseif 类型== "镜妖" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="强力"
            属性.属性 ="-5%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xA51B99F1
        elseif 类型== "进阶镜妖" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="强力"
            属性.属性 ="-4%速度"
            属性.小模型 = 0x2767C40B
            属性.大模型 = 0x0A545E77
        elseif 类型== "泪妖" then
            属性.卡片等级 = 7
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="毒"
            属性.属性 ="-5%防御"
            属性.小模型 = 0x58549952
            属性.大模型 = 0x2D59D0C2
        elseif 类型== "进阶泪妖" then
            属性.卡片等级 = 7
            属性.三维 = "防御"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="毒"
            属性.属性 ="-4%防御"
            属性.小模型 = 0x62D9A3B8
            属性.大模型 = 0xA22EBC2E
        elseif 类型== "千年蛇魅" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.07
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-7%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x65D26225
        elseif 类型== "进阶千年蛇魅" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.06
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-6%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x54C1CE90
        elseif 类型== "如意仙子" then
            属性.卡片等级 = 7
            属性.三维 = "灵力"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+20灵力"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x411A18C7
        elseif 类型== "进阶如意仙子" then
            属性.卡片等级 = 7
            属性.三维 = "灵力"
            属性.数值= 30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+30灵力"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x95539E8E
        elseif 类型== "鼠先锋" then
            属性.卡片等级 = 7
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="无"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xDC338F6E
        elseif 类型== "进阶鼠先锋" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+10速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xEEE1CBD0
        elseif 类型== "星灵仙子" then
            属性.卡片等级 = 7
            属性.三维 = "灵力"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="慧根"
            属性.属性 ="+10灵力"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xD226E204
        elseif 类型== "进阶星灵仙子" then
            属性.卡片等级 = 7
            属性.三维 = "灵力"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="慧根"
            属性.属性 ="+20灵力"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x24011D88
        elseif 类型== "巡游天神" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.2
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="-20%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xFD35D4E3
        elseif 类型== "进阶巡游天神" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.19
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="-19%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x45D1D778
        elseif 类型== "野猪精" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="反击"
            属性.属性 ="+5%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xF1127EEA
        elseif 类型== "进阶野猪精" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.06
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="反击"
            属性.属性 ="+6%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x58565061
        elseif 类型== "芙蓉仙子" then
            属性.卡片等级 = 7
            属性.三维 = "防御"
            属性.数值= -30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="再生"
            属性.属性 ="-30防御"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xBAFBFAA8
        elseif 类型== "进阶芙蓉仙子" then
            属性.卡片等级 = 7
            属性.三维 = "防御"
            属性.数值= -20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="再生"
            属性.属性 ="-20防御"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x92B02C7C
        elseif 类型== "进阶犀牛将军人形" then
            属性.卡片等级 = 7
            属性.三维 = "伤害"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="剑荡四方"
            属性.属性 ="+10伤害"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x298383CE
        elseif 类型== "犀牛将军人形" then
            属性.卡片等级 = 7
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="剑荡四方"
            属性.属性 ="无"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x696AF38B
        elseif 类型== "犀牛将军兽形" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+5%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0xCABF6445
        elseif 类型== "进阶犀牛将军兽形" then
            属性.卡片等级 = 7
            属性.三维 = "最大气血"
            属性.数值= 0.06
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+6%气血"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x1FF974F1
        elseif 类型== "阴阳伞" then
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="剑荡四方"
            属性.属性 ="-10%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x60AC19B3
        elseif 类型== "进阶阴阳伞" then------
            属性.卡片等级 = 7
            属性.三维 = "速度"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="剑荡四方"
            属性.属性 ="-9%速度"
            属性.小模型 = 0xEFA4BA2C
            属性.大模型 = 0x280E7DDE
 --------------------------------------------------------------------------------八级变身卡
        elseif 类型== "进阶巴蛇" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 15
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级毒"
            属性.属性 ="+15速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x67AD162F
        elseif 类型== "超级腾蛇" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级必杀"
            属性.属性 ="+10%伤害"
            属性.小模型 = 0x66E05E50
            属性.大模型 = 0xFC2930D6
        elseif 类型== "巴蛇" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级毒"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xBE748368
        elseif 类型== "进阶龙龟" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="法术防御"
            属性.属性 ="+10%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xD014FDA0
        elseif 类型== "龙龟" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.15
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="法术防御"
            属性.属性 ="-15%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x964E7FB8
        elseif 类型== "进阶大力金刚" then
            属性.卡片等级 = 8
            属性.三维 = "灵力"
            属性.数值= 0.03
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级防御"
            属性.属性 ="-3%灵力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xF1EC56BD
        elseif 类型== "大力金刚" then
            属性.卡片等级 = 8
            属性.三维 = "灵力"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级防御"
            属性.属性 ="-5%灵力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x64287AE7
        elseif 类型== "进阶鬼将" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值= 0.07
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级必杀"
            属性.属性 ="-7%伤害力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x6FEE9C4A
        elseif 类型== "鬼将" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级必杀"
            属性.属性 ="-10%伤害力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x023AA63E
        elseif 类型== "进阶红萼仙子" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.07
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+7%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xCC5937E0
        elseif 类型== "红萼仙子" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+5%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x2D0D5755
        elseif 类型== "葫芦宝贝" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 = "慧根"
            属性.属性 = "+5%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x48E257D4
        elseif 类型== "进阶葫芦宝贝" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.08
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="慧根"
            属性.属性 ="+8%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x82F7ACDB
        elseif 类型== "画魂" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级幸运"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xB73FB904
        elseif 类型== "进阶画魂" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.04
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级幸运"
            属性.属性 ="+4%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x73B9BBFE
        elseif 类型== "机关鸟" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级再生"
            属性.属性 ="-5%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x8423434C
        elseif 类型== "进阶机关鸟" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.03
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级再生"
            属性.属性 ="-3%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xC0F3874E
        elseif 类型== "机关人" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="壁垒击破"
            属性.属性 ="-5%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xEA888609
        elseif 类型== "进阶机关人" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.03
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="壁垒击破"
            属性.属性 ="-3%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x4387190C
        elseif 类型== "机关兽" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+10%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x9655B4E8
        elseif 类型== "进阶机关兽" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.11
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+11%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x48790118
        elseif 类型== "金绕僧" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级雷属性吸收"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xA03CD29E
        elseif 类型== "进阶金绕僧" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.15
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级雷属性吸收"
            属性.属性 ="+15%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xA8259D75
        elseif 类型== "进阶净瓶女娲" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.04
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级永恒"
            属性.属性 ="-4%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xC24C115C
        elseif 类型== "净瓶女娲" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级永恒"
            属性.属性 ="-5%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x7E99A5F4
        elseif 类型== "连驽车" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xD123A916
        elseif 类型== "进阶连驽车" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值= 50
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="+50伤害"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xF8715AED
        elseif 类型== "进阶灵鹤" then
            属性.卡片等级 = 8
            属性.三维 = "灵力"
            属性.数值= 15
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级法术暴击"
            属性.属性 ="+15灵力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xDBF272F5
        elseif 类型== "灵鹤" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级魔之心"
            属性.属性 ="-10%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xACEF8DB7
        elseif 类型== "进阶灵符女娲" then
            属性.卡片等级 = 8
            属性.三维 = "灵力"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级魔之心"
            属性.属性 ="+20灵力"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x4D0FE249
        elseif 类型== "灵符女娲" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="魔之心"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x5726FB84
        elseif 类型== "进阶律法女娲" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.07
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级反击"
            属性.属性 ="-7%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xCFB943A2
        elseif 类型== "律法女娲" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级反击"
            属性.属性 ="-10%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x4ED64302
        elseif 类型== "琴仙" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级法术暴击"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x3C3F4DB7
        elseif 类型== "进阶琴仙" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级法术暴击"
            属性.属性 ="+30防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x202A29B0
        elseif 类型== "进阶踏云兽" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.12
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+12%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x602A979A
        elseif 类型== "踏云兽" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="+10%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xC6F3C665
        elseif 类型== "进阶雾中仙" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.09
            属性.类型 =3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级敏捷"
            属性.属性 ="-9%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x311E19BE
        elseif 类型== "雾中仙" then
            属性.卡片等级 = 8
            属性.三维 = "防御"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级敏捷"
            属性.属性 ="-10%防御"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xA88F486E
        elseif 类型== "进阶吸血鬼" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.03
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-3%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x6EE49461
        elseif 类型== "吸血鬼" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-5%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x9D5FA3EC
        elseif 类型== "进阶噬天虎" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值= 50
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="+50伤害"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xA7A248EC
        elseif 类型== "噬天虎" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="-10%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x8DC23CAE
        elseif 类型== "进阶炎魔神" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级法术暴击"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x02DF99C1
        elseif 类型== "炎魔神" then
            属性.卡片等级 = 8
            属性.三维 = "最大气血"
            属性.数值= 0.15
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级魔之心"
            属性.属性 ="-15%气血"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xB9917494
        elseif 类型== "进阶幽灵" then
            属性.卡片等级 = 8
            属性.三维 = "伤害"
            属性.数值=50
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级夜战"
            属性.属性 ="+50伤害"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x38E27555
        elseif 类型== "幽灵" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级夜战"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x62875401
        elseif 类型== "进阶幽莹娃娃" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.06
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="防御"
            属性.属性 ="+6%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xCA91031D
        elseif 类型== "幽萤娃娃" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="防御"
            属性.属性 ="+5%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xD576EDF9
        elseif 类型== "夜罗刹" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级夜战"
            属性.属性 ="+5%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xAB1EFFB3
        elseif 类型== "进阶夜罗刹" then
            属性.卡片等级 = 8
            属性.三维 = "速度"
            属性.数值= 0.07
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="高级夜战"
            属性.属性 ="+7%速度"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x21995706
        elseif 类型== "超级泡泡" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0x0BD6B8AC-----------------------------
        elseif 类型== "超级大熊猫" then
            属性.卡片等级 = 8
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "五级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x2E030271
            属性.大模型 = 0xC1D91B08
 ----------------------------------------------------------------------------九级变身卡
        elseif 类型== "进阶毗舍童子" then----------------------
            属性.卡片等级 = 9
            属性.三维 = "伤害"
            属性.数值= 0.06
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级必杀"
            属性.属性 ="-6%伤害"
            属性.小模型 = 0x00000050
            属性.大模型 = 0x29A568DE
        elseif 类型== "毗舍童子" then-----------------------------
            属性.卡片等级 = 9
            属性.三维 = "伤害"
            属性.数值= 0.08
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级必杀"
            属性.属性 ="-8%伤害"
            属性.小模型 = 0x00000050
           属性.大模型 = 0x7A85051D
        elseif 类型== "长眉灵猴" then-----------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="法术暴击"
            属性.属性 ="-10%气血"
            属性.小模型 = 0x00000050
            属性.大模型 = 0xBE6EDEDE
        elseif 类型== "进阶长眉灵猴" then-----------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.07
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="法术暴击"
            属性.属性 ="-7%气血"
            属性.小模型 =0x00000050
            属性.大模型 = 0x7DFDC3DE
        elseif 类型== "进阶持国巡守" then-------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.06
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="法术暴击"
            属性.属性 ="-6%气血"
            属性.小模型 =0x00000050
            属性.大模型 = 0xE13AF8E0
        elseif 类型== "持国巡守" then------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.08
            属性.类型 = 3
             属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="法术暴击"
            属性.属性 ="-8%气血"
            属性.小模型 =0x00000050
            属性.大模型 = 0x6DD35CEB
        elseif 类型== "进阶混沌兽" then---------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级魔之心"
            属性.属性 ="-10%气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0x79900319
        elseif 类型== "混沌兽" then-----------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.12
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级魔之心"
            属性.属性 ="-12%气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0xA416C496
        elseif 类型== "金身罗汉" then----------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="+10%气血"
           属性.小模型 =0x00000050
            属性.大模型 = 0xB1B68D86
        elseif 类型== "进阶金身罗汉" then------------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.15
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="+15%气血"
           属性.小模型 =0x00000050
            属性.大模型 = 0xD013696E
        elseif 类型== "巨力神猿" then--------------
            属性.卡片等级 = 9
            属性.三维 = "灵力"
            属性.数值= 0.05
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="-5%灵力"
           属性.小模型 =0x00000050
            属性.大模型 = 0x99FACF23
        elseif 类型== "进阶巨力神猿" then------------
            属性.卡片等级 = 9
            属性.三维 = "伤害"
            属性.数值= 40
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级强力"
            属性.属性 ="+40伤害"
             属性.小模型 =0x00000050
            属性.大模型 = 0x8A41B896
        elseif 类型== "狂豹人" then------------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级飞行"
            属性.属性 ="+5%速度"
            属性.小模型 =0x00000050
            属性.大模型 = 0x5579CC03
        elseif 类型== "进阶狂豹人形" then-------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 0.08
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级飞行"
            属性.属性 ="+8%速度"
            属性.小模型 =0x00000050
            属性.大模型 = 0x58F599FE
        elseif 类型== "狂豹兽形" then------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 0.05
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级飞行"
            属性.属性 ="+5%速度"
             属性.小模型 =0x00000050
            属性.大模型 = 0xD20879BD
        elseif 类型== "进阶狂豹兽形" then------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 0.07
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级飞行"
            属性.属性 ="+7%速度"
            属性.小模型 =0x00000050
            属性.大模型 = 0x88BA4A5A
        elseif 类型== "曼珠沙华" then---------
            属性.卡片等级 = 9
            属性.三维 = "防御"
            属性.数值= 0.09
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级敏捷"
            属性.属性 ="-9%防御"
            属性.小模型 =0x00000050
            属性.大模型 = 0xEA0AD25A
        elseif 类型== "进阶曼珠沙华" then----------
            属性.卡片等级 = 9
            属性.三维 = "防御"
            属性.数值= 0.08
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级敏捷"
            属性.属性 ="-8%防御"
            属性.小模型 =0x00000050
            属性.大模型 = 0x5823D2D0
        elseif 类型== "猫灵人" then---------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+10速度"
           属性.小模型 =0x00000050
            属性.大模型 = 0x4C82A29E
        elseif 类型== "进阶猫灵人" then-------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 25
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+25速度"
             属性.小模型 =0x00000050
            属性.大模型 = 0xEC67F9DF
        elseif 类型== "猫灵兽" then------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 10
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+10速度"
            属性.小模型 =0x00000050
            属性.大模型 = 0x9ED7CAA8
        elseif 类型== "进阶猫灵兽" then------------
            属性.卡片等级 = 9
            属性.三维 = "速度"
            属性.数值= 20
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="敏捷"
            属性.属性 ="+20速度"
             属性.小模型 =0x00000050
            属性.大模型 = 0xBDAB6009
        elseif 类型== "藤蔓妖花" then--------
            属性.卡片等级 = 9
            属性.三维 = "命中"
            属性.数值= 100
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="毒"
            属性.属性 ="命中+100"
             属性.小模型 =0x00000050
            属性.大模型 = 0x5BC38FA5
        elseif 类型== "进阶藤蔓妖花" then--------------
            属性.卡片等级 = 9
            属性.三维 = "命中"
            属性.数值= 300
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="毒"
            属性.属性 ="命中+300"
             属性.小模型 =0x00000050
            属性.大模型 = 0x9487DA08
        elseif 类型== "进阶蝎子精" then---------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.08
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级反震"
            属性.属性 ="-8%气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0x46BCC57C
        elseif 类型== "蝎子精" then--------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 0.1
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="高级反震"
            属性.属性 ="-10%气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0x7B74E81
        elseif 类型== "修罗傀儡鬼" then--------------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 150
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="+150气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0x4D34C63C
        elseif 类型== "进阶修罗傀儡鬼" then-----------
            属性.卡片等级 = 9
            属性.三维 = "最大气血"
            属性.数值= 350
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="夜战"
            属性.属性 ="+350气血"
             属性.小模型 =0x00000050
            属性.大模型 = 0xEAB36ED9
        elseif 类型== "修罗傀儡妖" then-------------
            属性.卡片等级 = 9
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="无"
          属性.小模型 =0x00000050
            属性.大模型 = 0x5E499D7E
        elseif 类型== "进阶修罗傀儡妖" then-----------
            属性.卡片等级 = 9
            属性.三维 = "伤害"
            属性.数值= 0.01
            属性.类型 = 2
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="必杀"
            属性.属性 ="+1%伤害"
             属性.小模型 =0x00000050
            属性.大模型 = 0xC50DA3B0
        elseif 类型== "增长巡守" then----------------
            属性.卡片等级 = 9
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
   属性.小模型 =0x00000050
             属性.大模型 = 0xF26149F6
        elseif 类型== "进阶增长巡守" then--------------
            属性.卡片等级 = 9
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 =""
            属性.属性 =""
             属性.小模型 =0x00000050
            属性.大模型 = 0xCCD35C11----------------
        elseif 类型== "进阶真陀护法" then-------------
            属性.卡片等级 = 9
            属性.三维 = "防御"
            属性.数值= 0.01
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-1%防御"
             属性.小模型 =0x00000050
            属性.大模型 = 0x021526EC
        elseif 类型== "真陀护法" then-----------
            属性.卡片等级 = 9
            属性.三维 = "防御"
            属性.数值= 0.03
            属性.类型 = 3
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="偷袭"
            属性.属性 ="-3%防御"
            属性.小模型 =0x00000050
            属性.大模型 = 0x5A88298E
        elseif 类型== "蜃气妖" then
            属性.卡片等级 = 9
            属性.三维 = "防御"
            属性.数值= 30
            属性.类型 = 1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="魔之心"
            属性.属性 ="+30防御"
             属性.小模型 =0x00000050
            属性.大模型 = 0x904F2844
        elseif 类型== "进阶蜃气妖" then--------------
            属性.卡片等级 = 9
            属性.三维 = "灵力"
            属性.数值= 45
            属性.类型 =1
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="魔之心"
            属性.属性 ="+45灵力"
             属性.小模型 =0x00000050
            属性.大模型 = 0x19FF24CD
        elseif 类型== "灵灯侍者" then
            属性.卡片等级 = 9
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 =""
            属性.属性 =""
            属性.小模型 = 0x00000050
            属性.大模型 = 0x19A4720A---------------
        elseif 类型== "进阶灵灯侍者" then
            属性.卡片等级 = 9
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x00000050
            属性.大模型 = 0x275209D3
 ----------------------------------------------------------------------十级变身卡
        elseif 类型== "般若天女" then--------------
            属性.卡片等级 = 10
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 =0xF966E116
            属性.大模型 = 0xC020441E-------------
        elseif 类型== "进阶般若天女" then
            属性.卡片等级 = 10
            属性.三维 = ""
            属性.数值= 0
            属性.类型 = 0
            属性.资源 = "ZY.FT"
            属性.变化之术等级 = "六级变化之术"
            属性.技能 ="无"
            属性.属性 ="无"
            属性.小模型 = 0x59E4179D
            属性.大模型 = 0xF82A359D----------



              -- elseif wd == "魔兽要诀" then
      --  wds.说明 = "记载着上古魔兽特技，可供你的召唤兽学习。"
       -- wds.类型 = "炼妖"
      --  wds.资源 = "ZY.FT"
         --  wds.小模型资源 = 0x04694A50
         --  wds.大模型资源 = 0xC3A8BEA9
 --   elseif wd == "高级魔兽要诀" then
      --  wds.说明 = "记载着上古魔兽特技，可供你的召唤兽学习。"
      --  wds.类型 = "炼妖"
      --  wds.资源 = "JM.FT"
         --  wds.小模型资源 = 0x04694A50
         --  wds.大模型资源 = 0xC3A8BEA9



        end
return  属性
end














