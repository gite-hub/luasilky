--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-03-17 05:27:03
--======================================================================--
function 引擎.特效库(tx)
	local txs = {}
	if tx == "雨落寒沙" then --新
		txs[1] = 0xA1399E74
		txs[2] = "ZY.FT"
	elseif tx == "失忆符" then
		txs[1] = 0x3B86B284
		txs[2] = "ZY.FT"
	elseif tx == "状态_失忆符" then
		txs[1] = 0xEB3D0AC1
		txs[2] = "ZY.FT"
	elseif tx == "龙腾" then --新
		txs[1] = 0x3887B377
		txs[2] = "ZY.FT"
	elseif tx == "妙手回春" then
		txs[1] = 51617616
		txs[2] = "ZY.FT"
	elseif tx == "凝神诀" then
		txs[1] = 2128818470
		txs[2] = "ZY.FT"
	elseif tx == "千里神行" then
		txs[1] = 3356723242
		txs[2] = "ZY.FT"
	elseif tx == "水攻" then
		txs[1] = 4180877467
		txs[2] = "ZY.FT"
	elseif tx == "混元伞" then
		txs[1] = 0x253C9538
		txs[2] = "ZY.FT"
	elseif tx == "碎星诀" then
		txs[1] = 2704907892
		txs[2] = "ZY.FT"
	elseif tx == "尸腐毒" then  --新
		txs[1] = 0x3C172EFA
		txs[2] = "ZY.FT"
	elseif tx == "锢魂术" then  --新
		txs[1] = 0x0FB076DE
		txs[2] = "ZY.FT"
	elseif tx == "捕捉" then
		txs[1] = 3195920150
		txs[2] = "ZY.FT"
	elseif tx == "满天花雨" then --新
		txs[1] = 0x253C9538
		txs[2] = "ZY.FT"
	elseif tx == "似玉生香"  then --新
		txs[1] = 0xBA10E451
		txs[2] = "ZY.FT"
	elseif tx == "娉婷嬝娜"  then --新
		txs[1] = 0xAF8E7DDB
		txs[2] = "ZY.FT"
	elseif tx == "状态_娉婷嬝娜"  then --新
		txs[1] = 0x400392AD
		txs[2] = "ZY.FT"
	elseif tx == "状态_似玉生香" then
		txs[1] = 0x59D77EF9
		txs[2] = "ZY.FT"
	elseif tx == "状态_飞花摘叶" then
		txs[1] = 0x59D77EF9
		txs[2] = "ZY.FT"
	elseif tx == "状态_象形" then
		txs[1] = 0x3646A4D9
		txs[2] = "ZY.FT"
	elseif tx == "状态_雾杀" then
		txs[1] = 1506044316
		txs[2] = "ZY.FT"
	elseif tx == "落岩" then
		txs[1] = 180555238
		txs[2] = "ZY.FT"
	elseif tx == "回魂咒" then
		txs[1] = 2391977602
		txs[2] = "ZY.FT"
	elseif tx == "韦陀护法" then
		txs[1] = 2493890284
		txs[2] = "ZY.FT"
	elseif tx == "烟雨剑法" then
		txs[1] = 0xA3FD9577
		txs[2] = "ZY.FT"
	elseif tx == "百万神兵" then  --新
		txs[1] = 0x65955666
		txs[2] = "ZY.FT"
	elseif tx == "巨岩破" then --新
		txs[1] = 0xC4FABE4C
		txs[2] = "ZY.FT"
	elseif tx == "惊心一剑" then
		txs[1] = 1000651155
		txs[2] = "ZY.FT"
	elseif tx == "落魄符" then --新
		txs[1] = 0xB008AC11
		txs[2] = "ZY.FT"
	elseif tx == "水遁" then
		txs[1] = 1280017893
		txs[2] = "ZY.FT"
	elseif tx == "泰山压顶" then
		txs[1] = 2017434912
		txs[2] = "ZY.FT"
	elseif tx == "泰山压顶1" then
		txs[1] = 0xDF4C3BE3
		txs[2] = "ZY.FT"
	elseif tx == "泰山压顶2" then
		txs[1] = 0xE9401B00
		txs[2] = "ZY.FT"
	elseif tx == "泰山压顶3" then
		txs[1] = 0x133F8E31
		txs[2] = "ZY.FT"
	elseif tx == "达摩护体" then
		txs[1] = 2565901429
		txs[2] = "ZY.FT"
	elseif tx == "地涌金莲" then
		txs[1] = 0x0E1DCDB9
		txs[2] = "ZY.FT"
	elseif tx == "杀气诀" then
		txs[1] = 3264874295
		txs[2] = "ZY.FT"
	elseif tx == "龙卷雨击" then --新
		txs[1] = 0x2ED86FD0
		txs[2] = "ZY.FT"
	elseif tx == "地狱烈火" then
		txs[1] = 0xDED2253F
		txs[2] = "ZY.FT"
	elseif tx == "炎护" then
		txs[1] = 0xADF90980
		txs[2] = "ZY.FT"
	elseif tx == "加血" then
		txs[1] = 2010253357
		txs[2] = "ZY.FT"
	elseif tx == "杳无音讯" or tx == "凋零之歌" then
		txs[1] = 2709550029
		txs[2] = "ZY.FT"
	elseif tx == "苍茫树" then --新
		txs[1] = 0x2DEE722A
		txs[2] = "ZY.FT"
	elseif tx == "自在心法" then --新
		txs[1] = 0xD6BF55AD
		txs[2] = "ZY.FT"
	elseif tx == "魂飞魄散" then --新
		txs[1] = 0xAFF3AFBD
		txs[2] = "ZY.FT"
	elseif tx == "黄泉之息" then --新
		txs[1] = 0x1ADABFA6
		txs[2] = "ZY.FT"
	elseif tx == "裂石" then --新
		txs[1] = 0x3C5C1616 --新
		txs[2] = "ZY.FT"
	elseif tx == "断岳势" then --新
		txs[1] = 0x6AD55205 --新
		txs[2] = "ZY.FT"
	elseif tx == "推气过宫" then--新
		txs[1] = 0x28E639AA
		txs[2] = "ZY.FT"
	elseif tx == "金蟾" then--新
		txs[1] = 0x28E639AA
		txs[2] = "ZY.FT"
	elseif tx == "状态_金刚护体" then
		txs[1] = 916220457
		txs[2] = "ZY.FT"
	elseif tx == "离魂符" then
		txs[1] = 0x9A94D461
		txs[2] = "ZY.FT"
	elseif tx == "加愤怒" then
		txs[1] = 2156718962
		txs[2] = "ZY.FT"
	elseif tx == "五雷轰顶" then --新
		txs[1] = 0x6044E21A
		txs[2] = "ZY.FT"
	elseif tx == "冰清诀" or tx == "晶清诀" or tx == "玉清诀" or tx == "水清诀" or tx == "同生共死" then
		txs[1] = 388205471
		txs[2] = "ZY.FT"
	elseif tx == "楚楚可怜" then --新
		txs[1] = 0x28FAEFF7
		txs[2] = "ZY.FT"
	elseif tx == "佛门普渡" then
		txs[1] = 428453890
		txs[2] = "ZY.FT"
	elseif tx == "防御" then
		txs[1] = 908223343
		txs[2] = "ZY.FT"
	elseif tx == "天神护体" then
		txs[1] = 3231493430
		txs[2] = "ZY.FT"
	-- elseif tx == "龙卷雨击4" then
	-- 	txs[1] = 1459200517
	-- 	txs[2] = "ZY.FT"
	elseif tx == "活血" then --新
		txs[1] = 0xEF9691F3
		txs[2] = "ZY.FT"
	elseif tx == "救死扶伤" then --新
		txs[1] = 0xF2C7BC8E
		txs[2] = "ZY.FT"
	elseif tx == "雷击" then
		txs[1] = 238079300
		txs[2] = "ZY.FT"
	elseif tx == "天蚕丝" then
		txs[1] = 341882911
		txs[2] = "ZY.FT"
	elseif tx == "天罗地网" then --新
		txs[1] = 0x7DB0552B
		txs[2] = "ZY.FT"
	elseif tx == "天神护法" then
		txs[1] = 1935690327
		txs[2] = "ZY.FT"
	elseif tx == "状态_夺魄令" then
		txs[1] = 566328485
		txs[2] = "ZY.FT"
	elseif tx == "牛刀小试" then
		txs[1] = 1000651155
		txs[2] = "ZY.FT"
	elseif tx == "二龙戏珠" then --新
		txs[1] = 0x36262006
		txs[2] = "ZY.FT"
	elseif tx == "盘丝阵" then
		txs[1] = 1774142217
		txs[2] = "ZY.FT"
	elseif tx == "惊魂掌" then
		txs[1] = 0xC3D52CB3
		txs[2] = "ZY.FT"
	elseif tx == "煞气诀" then
		txs[1] = 0x8CD6756E
		txs[2] = "ZY.FT"
	elseif tx == "状态_尸腐毒" then
		txs[1] = 2208088730
		txs[2] = "ZY.FT"
	elseif tx == "四海升平" then
		txs[1] = 3497509792
		txs[2] = "ZY.FT"
	elseif tx == "雷霆万钧" then ---新
		txs[1] = 0x911E3921
		txs[2] = "ZY.FT"
	elseif tx == "状态_韦陀护法" then
		txs[1] = 2243155697
		txs[2] = "ZY.FT"
	elseif tx == "移形换影" then
		txs[1] = 1963789198
		txs[2] = "ZY.FT"
	elseif tx == "坐莲" then
		txs[1] = 1915810093
		txs[2] = "ZY.FT"
	elseif tx == "后发制人" then --新
		txs[1] = 0x22DFD2EA
		txs[2] = "ZY.FT"
	elseif tx == "阎罗令" then  --新
		txs[1] = 0x0280AC7B
		txs[2] = "ZY.FT"
	elseif tx == "飞砂走石" then
		txs[1] = 0x335A5A78
		txs[2] = "ZY.FT"
	elseif tx == "红袖添香" then
		txs[1] = 3858048292
		txs[2] = "ZY.FT"
	elseif tx == "状态_追魂符" then
		txs[1] = 3655438388
		txs[2] = "ZY.FT"
	elseif tx == "牛屎遁" then
		txs[1] = 2938873934
		txs[2] = "ZY.FT"
	elseif tx == "横扫千军" then --新
		txs[1] = 0x19C102D1
		txs[2] = "ZY.FT"
	elseif tx == "破釜沉舟" then --新
		txs[1] = 0x439692DE
		txs[2] = "ZY.FT"
	elseif tx == "命归术" then
		txs[1] = 821212684
		txs[2] = "ZY.FT"
	elseif tx == "炼气化神" then --新
		txs[1] = 0x1CFE70FA
		txs[2] = "ZY.FT"
	elseif tx == "移魂化骨" then
		txs[1] = 330324521
		txs[2] = "ZY.FT"
	elseif tx == "推拿" then
		txs[1] = 51617616
		txs[2] = "ZY.FT"
	elseif tx == "状态_红袖添香" then
		txs[1] = 3037907947
		txs[2] = "ZY.FT"
	elseif tx == "日月乾坤" then --新
		txs[1] = 0x2C2DA419
		txs[2] = "ZY.FT"
	elseif tx == "飘渺式" then --新
		txs[1] = 0x7C7D67A6
		txs[2] = "ZY.FT"
	elseif tx == "状态_楚楚可怜" then
		txs[1] = 1923968442
		txs[2] = "ZY.FT"
	elseif tx == "追魂符" then
		txs[1] = 0x25F14A5E
		txs[2] = "ZY.FT"
	elseif tx == "状态_离魂符" then
		txs[1] = 3655438388
		txs[2] = "ZY.FT"
	elseif tx == "状态_百万神兵" then
		txs[1] = 3297426043
		txs[2] = "ZY.FT"
	elseif tx == "烈火" then
		txs[1] = 3507654973
		txs[2] = "ZY.FT"
	elseif tx == "变身" then
		txs[1] = 0x0B361FA6
		txs[2] = "ZY.FT"
	elseif tx == "状态_变身"  then
		txs[1] = 702028255
		txs[2] = "ZY.FT"
	elseif tx == "破血狂攻" then
		txs[1] = 387269810
		txs[2] = "ZY.FT"
	elseif tx == "鹰击" then
		txs[1] = 0x82C9074A
		txs[2] = "ZY.FT"
	elseif tx == "弓弩攻击" then
		txs[1] = 3804004647
		--txs[1] =0x775176BB
		--txs[1] =0xDFC0E136
		txs[2] = "ZY.FT"
	elseif tx == "状态_炼气化神" then
		txs[1] = 1906144895
		txs[2] = "ZY.FT"
	elseif tx == "状态_达摩护体" then
		txs[1] = 0x0EEC978E
		txs[2] = "ZY.FT"
	elseif tx == "状态_生命之泉" then
		txs[1] = 4176381242
		txs[2] = "ZY.FT"
	elseif tx == "状态_定身符" then
		txs[1] = 2461182746
		txs[2] = "ZY.FT"
	elseif tx == "状态_定心术" then
		txs[1] = 0x2D07CCEC
		txs[2] = "ZY.FT"
	elseif tx == "状态_极度疯狂" or tx == "极度疯狂" then
		txs[1] = 0x718754A2
		txs[2] = "ZY.FT"
	elseif tx == "状态_不动如山" then
		txs[1] = 281904888
		txs[2] = "ZY.FT"
	elseif tx == "金刚护体" then --新
		txs[1] = 0x9E4E9755
		txs[2] = "ZY.FT"
	-- elseif tx == "龙卷雨击3" then
	-- 	txs[1] = 3514247917
	-- 	txs[2] = "ZY.FT"
	elseif tx == "浪涌" then --新
		txs[1] = 0xD53A25EB
		txs[2] = "ZY.FT"
	elseif tx == "状态_如花解语" then
		txs[1] = 505084121
		txs[2] = "ZY.FT"
	elseif tx == "气归术" then
		txs[1] = 3497509792
		txs[2] = "ZY.FT"
	elseif tx == "天崩地裂" then
		txs[1] = 0xEEF5BAF6
		txs[2] = "ZY.FT"
	elseif tx == "龙吟" then --新
		txs[1] = 0x48639CF5
		txs[2] = "ZY.FT"
	elseif tx == "状态_莲步轻舞" then
		txs[1] = 3387695093
		txs[2] = "ZY.FT"
	elseif tx == "加蓝" then
		txs[1] = 1049700101
		txs[2] = "ZY.FT"

	elseif tx == "狮搏" then --新
		txs[1] = 0x096766A8
		txs[2] = "ZY.FT"
	elseif tx == "尘土刃" then
		txs[1] = 0x63BADFEC
		txs[2] = "ZY.FT"
	elseif tx == "状态_明光宝烛" then
		txs[1] = 540412418
		txs[2] = "JM.FT"
	elseif tx == "判官令" then --新
		txs[1] = 0x2711A4A3
		txs[2] = "ZY.FT"
	elseif tx == "状态_煞气决" then
		txs[1] = 2126428619
		txs[2] = "ZY.FT"
	elseif tx == "状态_惊魂掌" then
		txs[1] = 2156799976
		txs[2] = "ZY.FT"
	elseif tx == "错乱" then  --新
		txs[1] = 0x8D302CE0
		txs[2] = "ZY.FT"
	elseif tx == "魔王回首" then
		txs[1] = 0x16B27FFC
		txs[2] = "ZY.FT"
	elseif tx == "状态_魔王回首" then
		txs[1] = 0xBBEA83F4
		txs[2] = "ZY.FT"
	elseif tx == "魔音摄魂" then --新
		txs[1] = 0x85E6AE55
		txs[2] = "ZY.FT"
	elseif tx == "姐妹同心" then --新
		txs[1] = 0x42ED98B8
		txs[2] = "ZY.FT"
	elseif tx == "状态_魔音摄魂" then
		txs[1] = 0xC4AA8E18
		txs[2] = "ZY.FT"
	elseif tx == "状态_炎护" then
		txs[1] = 2336654137
		txs[2] = "ZY.FT"
	elseif tx == "状态_后发制人" then
		txs[1] = 0x2A2A0663
		txs[2] = "ZY.FT"
	elseif tx == "阵型_不动如山" then
		txs[1] = 4106238113
		txs[2] = "wzife.wd2"
	elseif tx == "状态_碎星诀" then
		txs[1] = 814399755
		txs[2] = "ZY.FT"
	elseif tx == "心疗术" then
		txs[1] = 4227373440
		txs[2] = "ZY.FT"
	elseif tx == "状态_日月乾坤" then
		txs[1] = 826002553
		txs[2] = "ZY.FT"
	elseif tx == "状态_盘丝阵" then
		txs[1] = 69928620
		txs[2] = "ZY.FT"
	elseif tx == "落叶萧萧"  then --新
		txs[1] = 0x36BFE018
		txs[2] = "ZY.FT"
	elseif tx == "状态_象形" then
		txs[1] = 2213550572
		txs[2] = "ZY.FT"
	elseif tx == "水漫金山" then
		txs[1] = 2431146143
		txs[2] = "ZY.FT"
	elseif tx == "天雷斩" then  --新
		txs[1] = 0xF17168C5
		txs[2] = "ZY.FT"
	elseif tx == "状态_紧箍咒" then
		txs[1] = 4024909497
		txs[2] = "ZY.FT"
	elseif tx == "命疗术" then
		txs[1] = 4088602190
		txs[2] = "ZY.FT"
	elseif tx == "状态_失心符" then
		txs[1] = 3635978625
		txs[2] = "ZY.FT"
	elseif tx == "定身符" then ---新
		txs[1] = 0x8485B3E2
		txs[2] = "ZY.FT"
	elseif tx == "状态_金身舍利" then
		txs[1] = 0xECEC37F7
		txs[2] = "ZY.FT"
	elseif tx == "摧心术" then
		txs[1] = 2702496872
		txs[2] = "ZY.FT"
	elseif tx == "碎甲术" or tx == "破甲术" then
		txs[1] = 3505309973
		txs[2] = "ZY.FT"
	elseif tx == "夺命咒" then
		txs[1] = 0x011F0C5B
		txs[2] = "ZY.FT"
	elseif tx == "状态_一苇渡江" then
		txs[1] = 382287583
		txs[2] = "ZY.FT"
	elseif tx == "一苇渡江" then
		txs[1] = 4082420920
		txs[2] = "ZY.FT"
	elseif tx == "凝气诀" then
		txs[1] = 843116756
		txs[2] = "ZY.FT"
	elseif tx == "状态_乘风破浪" then
		txs[1] = 3366209335
		txs[2] = "ZY.FT"
	elseif tx == "状态_逆鳞" then
		txs[1] = 4066099146
		txs[2] = "ZY.FT"
	elseif tx == "状态_冰封" then
		txs[1] = 324036383
		txs[2] = "ZY.FT"
	elseif tx == "如花解语" then --新
		txs[1] = 0xB493EF60
		txs[2] = "ZY.FT"
	elseif tx == "状态_杀气诀" then
		txs[1] = 0x62EDD1CB
		txs[2] = "ZY.FT"
	elseif tx == "保护" then
		txs[1] = 4027829983
		txs[2] = "ZY.FT"
	elseif tx == "复苏" then
		txs[1] = 3698682735
		txs[2] = "ZY.FT"
	elseif tx == "瘴气" then ---新
		txs[1] = 0x9B69747B
		txs[2] = "ZY.FT"
	elseif tx == "状态_催眠符" then
		txs[1] = 3398426285
		txs[2] = "ZY.FT"
	elseif tx == "状态_横扫千军" then
		txs[1] = 0xD9463A0C
		txs[2] = "ZY.FT"
	elseif tx == "还阳术" then
		txs[1] = 1538345049
		txs[2] = "ZY.FT"
	elseif tx == "状态_毒" then
		txs[1] = 3687768876
		txs[2] = "ZY.FT"
	elseif tx == "月光" then
		txs[1] = 685510219
		txs[2] = "common/ski.wdf"
	elseif tx == "含情脉脉" then --新
		txs[1] = 0xCEAF0E44
		txs[2] = "ZY.FT"
	elseif tx == "状态_错乱" then
		txs[1] = 2557820820
		txs[2] = "ZY.FT"
	elseif tx == "纵地金光" then
		txs[1] = 3546433571
		txs[2] = "ZY.FT"
	elseif tx == "血雨" then --新
		txs[1] = 0xBACD6F79
		txs[2] = "ZY.FT"
	elseif tx == "普渡众生" then --新
		txs[1] = 0x3D0C6FF1
		txs[2] = "ZY.FT"
	elseif tx == "状态_颠倒五行" then
		txs[1] = 923673984
		txs[2] = "ZY.FT"
	elseif tx == "催眠符" then --新
		txs[1] = 0xB8E57120
		txs[2] = "ZY.FT"
	elseif tx == "冰川怒" then
		txs[1] = 0x4C7470BF
		txs[2] = "ZY.FT"
	elseif tx == "状态_落魄符" then
		txs[1] = 3635978625
		txs[2] = "ZY.FT"
	elseif tx == "气疗术" then
		txs[1] = 4088602190
		txs[2] = "ZY.FT"
	elseif tx == "起死回生" then
		txs[1] = 2391977602
		txs[2] = "ZY.FT"
	elseif tx == "力劈华山" then
		txs[1] = 4281577710
		txs[2] = "ZY.FT"
    elseif tx == "剑荡四方" then
		txs[1] = 0x729ADE62
		txs[2] = "WE.dll"
	elseif tx == "壁垒击破" then
		txs[1] = 0x99DDC32A
		txs[2] = "WE.dll"
	elseif tx == "腾雷" then --新
		txs[1] = 0x8A506A89
		txs[2] = "ZY.FT"
	elseif tx == "叶隐" then
		txs[1] = 1859374553
		txs[2] = "ZY.FT"
	elseif tx == "失心符" then --新
		txs[1] = 0x89B2D33A
		txs[2] = "ZY.FT"
	elseif tx == "腾云驾雾" then
		txs[1] = 871130409
		txs[2] = "ZY.FT"
	elseif tx == "不动如山" then
		txs[1] = 0xC514B5E7
		txs[2] = "ZY.FT"
	elseif tx  == "分身术" then --新
		txs[1] = 0x4B9ACA92
		txs[2] = "ZY.FT"
	elseif tx  == "镇妖" then --新
		txs[1] = 0x14965928
		txs[2] = "ZY.FT"
	elseif tx == "解毒" then
		txs[1] = 3727996990
		txs[2] = "ZY.FT"
	elseif tx == "奔雷咒" then
		txs[1] = 4115415077
		txs[2] = "ZY.FT"
	elseif tx == "明光宝烛" then
		txs[1] = 1479134995
		txs[2] = "JM.FT"
	elseif tx == "状态_移魂化骨" then
		txs[1] = 759125516
		txs[2] = "ZY.FT"
	elseif tx == "唧唧歪歪" then --新
		txs[1] = 0xF71DEAF7
		txs[2] = "ZY.FT"
	elseif tx == "连环击" then --新
		txs[1] =0x83020DAE
		txs[2] = "ZY.FT"
	elseif tx == "被击中" then
		txs[1] = 490729788
		txs[2] = "ZY.FT"
	elseif tx == "翻江搅海" then
		txs[1] = 0x584B98C0
		txs[2] = "ZY.FT"
	elseif tx == "落雷符" then --新
		txs[1] = 0xC1731A60
		txs[2] = "ZY.FT"
	elseif tx == "日光华" then
		txs[1] = 0x9371DB10
		txs[2] = "ZY.FT"
	elseif tx == "地裂火" then  --新
		txs[1] = 0xDC603FBE
		txs[2] = "ZY.FT"
	elseif tx == "解封" then
		txs[1] = 1833750106
		txs[2] = "ZY.FT"
	elseif tx == "反震" then
		txs[1] = 3690777786
		txs[2] = "ZY.FT"
	elseif tx == "蜜润" then
		txs[1] = 3298164407
		txs[2] = "ZY.FT"
	elseif tx == "状态_蜜润" then
		txs[1] = 0x20DFE815
		txs[2] = "ZY.FT"
	elseif tx == "三花聚顶" then
		txs[1] = 2002768611
		txs[2] = "ZY.FT"
	elseif tx == "必杀" then
		txs[1] = 3973111811
		txs[2] = "ZY.FT"
	-- elseif tx == "法暴" then
	-- 	txs[1] = 0xB94D0B94
	-- 	txs[2] = "ZY.FT"
	elseif tx == "法暴" then
		txs[1] = 0xDAD8AC20
		txs[2] = "shape.wdf"
	elseif tx == "宁心" then
		txs[1] = 0xA72FDB18
		txs[2] = "ZY.FT"
	elseif tx == "状态_天神护法" then
		txs[1] = 0xAC3D253B
		txs[2] = "ZY.FT"
	elseif tx == "荆棘舞" then
		txs[1] = 0x34826440
		txs[2] = "ZY.FT"
	elseif tx == "三昧真火" then --新
		txs[1] = 0x774B7CEF
		txs[2] = "ZY.FT"
	elseif tx == "我佛慈悲" then--新
		txs[1] = 0x6FC329C6
		txs[2] = "ZY.FT"
	elseif tx == "紧箍咒" then --新
		txs[1] = 0x1EA5FACC
		txs[2] = "ZY.FT"
	elseif tx == "杨柳甘露" then
		txs[1] = 939734977
		txs[2] = "ZY.FT"
	elseif tx == "莲步轻舞" then --新
		txs[1] = 0x73D87E89
		txs[2] = "ZY.FT"
	elseif tx == "靛沧海" then --新
		txs[1] = 0x057B7209
		txs[2] = "ZY.FT"
	elseif tx == "五雷咒" then --新
		txs[1] = 0x5BCBB64F
		txs[2] = "ZY.FT"
	-- elseif tx == "龙卷雨击2" then
	-- 	txs[1] = 3592299801
	-- 	txs[2] = "ZY.FT"
	elseif tx == "摄魄" then  --新
		txs[1] = 0xB1A504B7
		txs[2] = "ZY.FT"
	elseif tx == "生命之泉" then --新
		txs[1] = 0xA1A5E0CD
		txs[2] = "ZY.FT"
	elseif tx == "颠倒五行" then
		txs[1] = 923673984
		txs[2] = "ZY.FT"
	elseif tx == "乘风破浪" then
		txs[1] = 1172493695
		txs[2] = "ZY.FT"
	elseif tx == "捕捉2" then
		txs[1] = 2601915514
		txs[2] = "ZY.FT"
	elseif tx == "清心" then
		txs[1] = 822452251
		txs[2] = "ZY.FT"
	elseif tx == "状态_天神护体" then
		txs[1] = 1724676649
		txs[2] = "ZY.FT"
	elseif tx == "勾魂" then ---新
		txs[1] = 0x1C57DA8A
		txs[2] = "ZY.FT"
	elseif tx == "振翅千里" then
		txs[1] = 2661053669
		txs[2] = "ZY.FT"
	elseif tx == "逆鳞" then
		txs[1] = 81488906
		txs[2] = "ZY.FT"
	elseif tx == "象形"  then --新
		txs[1] = 0x73DA1CCF
		txs[2] = "ZY.FT"
	elseif tx == "惊涛怒" then
		txs[1] = 0x697E01E6
		txs[2] = "ZY.FT"
	elseif tx == "反间之计" then
		txs[1] = 1804814488
		txs[2] = "ZY.FT"
	elseif tx == "星月之惠" then
		txs[1] = 3298164407
		txs[2] = "ZY.FT"
	elseif tx == "斗转星移" then
		txs[1] = 1056770863
		txs[2] = "ZY.FT"
	elseif tx == "状态_含情脉脉" then
		txs[1] = 2164502482
		txs[2] = "ZY.FT"
	elseif tx == "雾杀" then
		txs[1] = 0x033F18D1
		txs[2] = "ZY.FT"
	elseif tx == "夺魄令" then
		txs[1] = 0x9F07F1C3
		txs[2] = "ZY.FT"
	elseif tx == "上古灵符" then
		txs[1] = 48901659
		txs[2] = "ZY.FT"
	elseif tx == "升级" then
		txs[1] = 2604332261
		txs[2] = "ZY.FT"
	elseif tx == "放下屠刀" or tx == "河东狮吼" then
		txs[1] = 0x81B4599F
		txs[2] = "ZY.FT"
	elseif tx == "夜舞倾城" then
		txs[1] = 0xF18C76DD
		txs[2] = "ZY.FT"
	elseif tx == "打狗棒" then
		txs[1] = 0x1EE9406C
		txs[2] = "ZY.FT"
	elseif tx == "金刚护法"  then --新
		txs[1] = 0xAB0D8604
		txs[2] = "ZY.FT"
	elseif tx == "归元咒" or tx == "乾天罡气" then
		txs[1] = 1366709986
		txs[2] = "ZY.FT"
	elseif tx == "牛劲"  then --新
		txs[1] = 0x9F9F85EF
		txs[2] = "ZY.FT"
	elseif tx ==  "状态_牛劲" then --新
		txs[1] = 0xA168E26F
		txs[2] = "ZY.FT"
	elseif tx == "失魂符" then --新
		txs[1] = 0x2406DA40
		txs[2] = "ZY.FT"
	elseif tx == "碎甲符"  then --新
		txs[1] = 0x0116C932
		txs[2] = "ZY.FT"
	elseif tx == "状态_失魂符" then
		txs[1] = 0x8B43833D
		txs[2] = "ZY.FT"
	elseif tx == "状态_失心符" then
		txs[1] = 0x8B43833D
		txs[2] = "ZY.FT"
	elseif tx == "状态_碎甲符" then
		txs[1] = 0xEB3D0AC1
		txs[2] = "ZY.FT"
	elseif tx == "飞花摘叶" then
		txs[1] = 0x78BF64A4
		txs[2] = "ZY.FT"
	elseif tx == "百毒不侵" then
		txs[1] = 0xD69CAE82
		txs[2] = "ZY.FT"
	elseif tx == "一笑倾城" then --新
		txs[1] = 0x935FA878
		txs[2] = "ZY.FT"
	elseif tx == "状态_百毒不侵" then
		txs[1] = 0x860E67C9
		txs[2] = "ZY.FT"
	elseif tx == "状态_一笑倾城" then
		txs[1] = 0x760B7BD7
		txs[2] = "ZY.FT"
	elseif tx == "状态_血雨" then
		txs[1] = 0x497E890D
		txs[2] = "ZY.FT"
	elseif tx == "状态_护法紫丝" then
		txs[1] = 0x042B06AC
		txs[2] = "ZY.FT"
	elseif tx == "状态_佛法无边" then
		txs[1] = 0xEA4D704A
		txs[2] = "ZY.FT"
	elseif tx == "佛法无边" then
		txs[1] = 0xC8011EF1
		txs[2] = "ZY.FT"
	elseif tx == "威慑" then
		txs[1] = 0x8476181B
		txs[2] = "ZY.FT"
	elseif tx == "状态_威慑" then
		txs[1] = 0x8476181B
		txs[2] = "ZY.FT"
	elseif tx == "舍生取义"  then--新
		txs[1] = 0x8CBA06E2
		txs[2] = "ZY.FT"
	elseif tx == "状态_普渡众生"  then
		txs[1] = 0xD8857128
		txs[2] = "ZY.FT"
	elseif tx == "状态_灵动九天"  then
		txs[1] = 0x95FF4460
		txs[2] = "ZY.FT"
	elseif tx == "火甲术"  then
		txs[1] = 0x4B621DF7
		txs[2] = "ZY.FT"
	elseif tx == "九天玄火"  then
		txs[1] = 0x6BA62F2C
		txs[2] = "ZY.FT"
	elseif tx == "状态_火甲术"  then
		txs[1] = 0x82D17DF8
		txs[2] = "ZY.FT"
	elseif tx == "状态_鹰击"  then
		txs[1] = 0x58628406
		txs[2] = "ZY.FT"
	elseif tx == "金甲仙衣"  then
		txs[1] = 0x00000016
		txs[2] = "ZY.FT"
	elseif tx == "状态_混元伞"  then
		txs[1] = 0xDCCB25E2
		txs[2] = "ZY.FT"
    elseif tx == "状态_神木宝鼎"  then
		txs[1] = 0x196F4A1D
		txs[2] = "ZY.FT"
	elseif tx == "神木宝鼎"  then
		txs[1] = 0xFB076DE
		txs[2] = "ZY.FT"
	elseif tx == "赤焰"  then
		txs[1] = 0x139AF9A1
		txs[2] = "ZY.FT"
	elseif tx == "状态_赤焰"  then
		txs[1] = 0xCEAF91A
		txs[2] = "ZY.FT"
	elseif tx == "状态_分水"  then
		txs[1] = 0xB35AE126
		txs[2] = "ZY.FT"
	elseif tx == "分水"  then
		txs[1] = 0xB94D0B94
		txs[2] = "ZY.FT"


	end
	return txs
end
function 引擎.取光环(tx)
	local n={}
	if tx == "烈焰澜翻" then
	   n[1]=0x52A385FE
	   n[2]=0x60FB8930
	   n[3]=0x1CDB4C3F
	   n[4]="ZY.FT"
	elseif tx == "水墨游龙" then
	   n[1]=0x0DF5F610
	   n[2]=0x8109B081
	   n[3]=0xFFCF398C
	    n[4]="ZY.FT"
	elseif tx == "星光熠熠" then
	   n[1]=0xCA82DD54
	   n[2]=0xFCC5CE3A
	   n[3]=0xCA82DD54
	      n[4]="ZY.FT"
	elseif tx == "双鲤寄情" then
	   n[1]=0xC056D8DD
	   n[2]=0x865FF084
	   n[3]=0x654EAD9E
	      n[4]="ZY.FT"
	elseif tx == "凌波微步" then
	   n[1]=0x6F1A7860
	   n[2]=0x242264D2
	   n[3]=0xE1D6A08F
	      n[4]="ZY.FT"
	elseif tx == "浩瀚星河" then
	   n[1]=0x200B20C2
	   n[2]=0x1E064D72
	   n[3]=0x04A28CA1
	      n[4]="ZY.FT"
	elseif tx == "荷塘涟漪" then
	   n[1]=0x085A1CE8
	   n[2]=0x4F049527
	   n[3]=0x268ED6D4
	      n[4]="ZY.FT"
	elseif tx == "雪花飘落" then
	   n[1]=0x12907C0F
	   n[2]=0xDE953084
	   n[3]=0xDE953084
	      n[4]="ZY.FT"
	elseif tx == "花的海洋" then
	   n[1]=0xCA3B3A8F
	   n[2]=0xBACF8614
	   n[3]=0xCA3B3A8F
	      n[4]="ZY.FT"
	elseif tx == "爱的光影" then
	   n[1]=0xF0CBB55B
	   n[2]=0xF0CBB55B
	   n[3]=0xF0CBB55B
	      n[4]="ZY.FT"
	      n[4]="ZY.FT"
	elseif tx == "祥云瑞气" then
	   n[1]=0xD5E2F0A8
	   n[2]=0x84BE68C9
	   n[3]=0x50907DFA
	      n[4]="ZY.FT"
	elseif tx == "珠落玉盘" then
	   n[1]=0xAAC2A157
	   n[2]=0xAAC2A157
	   n[3]=0xAAC2A157
	      n[4]="ZY.FT"
	elseif tx == "红叶随风" then
	   n[1]=0x00000017
	   n[2]=0x00000017
	   n[3]=0x00000017
	      n[4]="ZY.FT"
	elseif tx == "桃花飞舞" then
	   n[1]=0x683C4BB
	   n[2]=0x683C4BB
	   n[3]=0x683C4BB
	      n[4]="ZY.FT"
	elseif tx == "月影婆娑" then
	   n[1]=0x00000018
	   n[2]=0x00000018
	   n[3]=0x00000018
	      n[4]="ZY.FT"
	end

	return n
end

function 引擎.取足迹(tx)
	local n={}
	if tx == "龙卷风足迹" then
	   n[1]=0x00000019
	   n[4]="ZY.FT"
    elseif tx == "皮球足迹" then
	   n[1]=0x9C146BBA
	   n[4]="ZY.FT"
	elseif tx == "雀屏足迹" then
	   n[1]=0xFFED8401
	   n[4]="ZY.FT"
	elseif tx == "旋律足迹" then
	   n[1]=0xD8D8AFFF
	   n[4]="ZY.FT"
	elseif tx == "枫叶足迹" then
	   n[1]=0xF879A813
	   n[4]="ZY.FT"
	elseif tx == "飞天足迹" then
	   n[1]=0xE7B37A0B
	   n[4]="ZY.FT"
	elseif tx == "两心相悦足迹" then
	   n[1]=0x7D75476A
	   n[4]="ZY.FT"
	elseif tx == "心花怒放" then
	   n[1]=0xD5933DC2
	   n[4]="ZY.FT"
	elseif tx == "雷电足迹" then
	   n[1]=0xC3060301
	   n[4]="ZY.FT"
	elseif tx == "寒冰足迹" then
	   n[1]=0xD588789A
	   n[4]="ZY.FT"
	elseif tx == "星星祈愿" then
	   n[1]=0xFCA197EB
	   n[4]="ZY.FT"
	elseif tx == "地狱焰火" then
	   n[1]=0x7D931979
	   n[4]="ZY.FT"
	elseif tx == "元宝足迹" then
	   n[1]=0x8E5FA0FF
	   n[4]="ZY.FT"
	elseif tx == "光剑足迹" then
	   n[1]=0x988D7A27
	   n[4]="ZY.FT"
	elseif tx == "雪花足迹" then
	   n[1]=0xF7071DAB
	   n[4]="ZY.FT"






	end
	return n
end