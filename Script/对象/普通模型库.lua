
function 引擎.取模型(ch,wq)
	local chs = {}
	if wq == nil then
		wq = "普通"
	end
	if ch == "巫蛮儿" or ch == "杀破狼" or ch == "羽灵神" then
	   	s = "_"..wq
	else
	 	s = "_普通"
	end
	ch = ch..s

    ------------------------------------宠物
		if ch ==  "生肖狗_普通" then
			   chs[1] = 0xF41F6038
			   chs[2] = 0xDE58CA6F
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖猴_普通" then
			   chs[1] = 0xAA910FFF
			   chs[2] = 0x51FC5B28
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖虎_普通" then
			   chs[1] = 0x59D6FB3B
			   chs[2] = 0xD7DC93DE
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖鸡_普通" then
			   chs[1] = 0xCC9CE563
			   chs[2] = 0x9D353BF4
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖龙_普通" then
			   chs[1] = 0x7FD9D3BC
			   chs[2] = 0x289C773D
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖马_普通" then
			   chs[1] = 0x7FD84FE9
			   chs[2] = 0x4D8E1F0C
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖牛_普通" then
			   chs[1] = 0x6644A046
			   chs[2] = 0x350A49B8
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖蛇_普通" then
			   chs[1] = 0xB0F79819
			   chs[2] = 0x800A4859
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖鼠_普通" then
			   chs[1] = 0xDAC84F94
			   chs[2] = 0x39195BC3
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖兔_普通" then
			   chs[1] = 0x9D99A10D
			   chs[2] = 0x524EA098
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖羊_普通" then
			   chs[1] = 0xDEBBC85B
			   chs[2] = 0x087C63E1
			   chs[3] = "ZY.FT"
		elseif ch ==  "生肖猪_普通" then
			   chs[1] = 0x60976277
			   chs[2] = 0x1F9309D8
			   chs[3] = "ZY.FT"
	    elseif ch ==  "摇钱树_普通" then
			   chs[1] = 0x56FFF012
			   chs[2] = 0x56FFF012
			   chs[3] = "JM.FT"
    ------------------------------------0级宝宝
			elseif ch == "狸_普通"  then
				chs[1] = 0x3CDBAFB2
				chs[2] = 0x0E6210D6
				chs[3] = "ZHS.FT"
		    elseif ch == "饰品_狸_普通"  then
				chs[1] = 0x3CDBAFB2
				chs[2] = 0x1F1B1BC1
				chs[3] = "ZHS.FT"
			elseif ch == "海星_普通" then--有这个模型 可以当NPC用
				chs[1] = 0x7260A22B
				chs[2] = 0x71253EE7
				chs[3] = "ZHS.FT"
			elseif ch == "饰品_海星_普通"then
				   静立= 0x354E8366
                   行走= 0xD559C81E
			elseif ch == "章鱼_普通" then
				chs[1] = 0x3213A07E
				chs[2] = 0xA09FA982
				chs[3] = "ZHS.FT"
            elseif ch == "饰品_章鱼_普通" then
            	chs[1] = 0xA33259ED
				chs[2] = 0x755DEDBC
				chs[3] = "ZHS.FT"
			elseif ch ==  "护卫_普通" then
				   chs[1] = 0x03A423A7
				   chs[2] = 0xA260CA8C
				   chs[3] = "ZHS.FT"
			elseif ch ==  "巨蛙_普通" then
				   chs[1] = 0xD69D74DA
				   chs[2] = 0xB0E02259
				   chs[3] = "ZHS.FT"
			elseif ch ==  "泡泡_普通" then
				   chs[1] = 0x0F19E0B4
				   chs[2] = 0xD85C35AC
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_泡泡_普通" then
   				   chs[1] = 0xD98F0E8D
				   chs[2] = 0xD98F0E8D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "大海龟_普通" then
				   chs[1] = 0x4E4577E9
				   chs[2] = 0x545A24CA
				   chs[3] = "ZHS.FT"
	------------------------------------5级
		elseif ch ==  "海毛虫_普通" then
			   chs[1] = 0xA9AD0013
			   chs[2] = 0xE7CF4AE4
			   chs[3] = "ZHS.FT"
		elseif ch ==  "野猪_普通" then
			   chs[1] = 0x5034A927
			   chs[2] = 0x99EE8435
			   chs[3] = "ZHS.FT"
		elseif ch ==  "饰品_野猪_普通" then
			   chs[1] = 0x935A0CD1
			   chs[2] = 0xFB2601B1
			   chs[3] = "ZHS.FT"
		elseif ch ==  "强盗_普通" then
			   chs[1] = 0x4E1EC463
			   chs[2] = 0xFD851096
			   chs[3] = "ZHS.FT"
		elseif ch ==  "山贼_普通" then
			   chs[1] = 0xE8A7D963
			   chs[2] = 0xAEF0806F
			   chs[3] = "ZHS.FT"
		elseif ch ==  "树怪_普通" then
			   chs[1] = 0x21E267D5
			   chs[2] = 0x3ED84216
			   chs[3] = "ZHS.FT"
		elseif ch ==  "赌徒_普通" then
			   chs[1] = 0xB59A19B8
			   chs[2] = 0x32907688
			   chs[3] = "ZHS.FT"
		elseif ch ==  "大蝙蝠_普通" then
			   chs[1] = 0x10614BC6
			   chs[2] = 0xDBA9BE37
			   chs[3] = "ZHS.FT"
	------------------------------------15级
			elseif ch ==  "羊头怪_普通" then
				   chs[1] = 0x8EE3FB9A
				   chs[2] = 0x1F263139
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_羊头怪_普通" then
				   chs[1] = 0x6E2E511A
				   chs[2] = 0xDC42DDF1
				   chs[3] = "ZHS.FT"
			elseif ch ==  "骷髅怪_普通" then
				   chs[1] = 0x780D4A9C
				   chs[2] = 0x15CD81CA
				   chs[3] = "ZHS.FT"
			elseif ch ==  "蛤蟆精_普通" then
				   chs[1] = 0xDF976C1E
				   chs[2] = 0x81D749BE
				   chs[3] = "ZHS.FT"
			elseif ch ==  "黑熊_普通" then
				   chs[1] = 0x3CEC1591
				   chs[2] = 0x58AA587E
				   chs[3] = "ZHS.FT"
			elseif ch ==  "狐狸精_普通" then
				   chs[1] = 0xF20F8357
				   chs[2] = 0x106839A5
				   chs[3] = "ZHS.FT"
			elseif ch ==  "花妖_普通" then
				   chs[1] = 0xDE984023
				   chs[2] = 0x09C07E77
				   chs[3] = "ZHS.FT"
			elseif ch ==  "老虎_普通" then
				   chs[1] = 0x21CE1B76
				   chs[2] = 0x8D53D7DD
				   chs[3] = "ZHS.FT"
	------------------------------------25级
			elseif ch ==  "野鬼_普通" then
				   chs[1] = 0xD117595B
				   chs[2] = 0x57A214CF
				   chs[3] = "ZHS.FT"
			elseif ch ==  "狼_普通" then
				   chs[1] = 0xA94BB647
				   chs[2] = 0xCB14A5E2
				   chs[3] = "ZHS.FT"
			elseif ch ==  "牛妖_普通" then
				   chs[1] = 0xF58E06E8
				   chs[2] = 0x5274F324
				   chs[3] = "ZHS.FT"
			elseif ch ==  "虾兵_普通" then
				   chs[1] = 0xDB68BB03
				   chs[2] = 0x2A203B1F
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小龙女_普通" then
				   chs[1] = 0x6821177C
				   chs[2] = 0x993C5C2C
				   chs[3] = "ZHS.FT"
			elseif ch ==  "蟹将_普通" then
				   chs[1] = 0x60888D2F
				   chs[2] = 0x3E952AAA
				   chs[3] = "ZHS.FT"
	------------------------------------35级
			elseif ch ==  "蜘蛛精_普通" then
				   chs[1] = 0x74606705
				   chs[2] = 0x1D855A0D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "兔子怪_普通" then
				   chs[1] = 0x6EEFA438
				   chs[2] = 0x46242998
				   chs[3] = "ZHS.FT"
			elseif ch ==  "僵尸_普通" then
				   chs[1] = 0x76A5ABFD
				   chs[2] = 0x87D5CA4D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "龟丞相_普通" then
				   chs[1] = 0x51C20B00
				   chs[2] = 0x53BB423C
				   chs[3] = "ZHS.FT"
			elseif ch ==  "黑熊精_普通" then
				   chs[1] = 0x578A6F8A
				   chs[2] = 0x91E7D789
				   chs[3] = "ZHS.FT"
			elseif ch ==  "马面_普通" then
				   chs[1] = 0xB350A5B4
				   chs[2] = 0x99AC3A10
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_马面_普通" then
				   chs[1] = 0x1FBDD393
				   chs[2] = 0x9F57D026
				   chs[3] = "ZHS.FT"
			elseif ch ==  "牛头_普通" then
				   chs[1] = 0xE6E7DE44
				   chs[2] = 0x7F2ABF21
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_牛头_普通" then
				   chs[1] = 0xEF5E5215
				   chs[2] = 0xAFF57351
				   chs[3] = "ZHS.FT"
	------------------------------------45级
			elseif ch ==  "黑山老妖_普通" then
				   chs[1] = 0x759DB60F
				   chs[2] = 0x5EBE3941
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_黑山老妖_普通" then
				   chs[1] = 0x266C5FE5
				   chs[2] = 0x5EBE3941
				   chs[3] = "ZHS.FT"
			elseif ch ==  "蝴蝶仙子_普通" then
				   chs[1] = 0xD72C8A4B
				   chs[2] = 0x316273BC
				   chs[3] = "ZHS.FT"
			elseif ch ==  "雷鸟人_普通" then
				   chs[1] = 0x398BFB4F
				   chs[2] = 0x1BD4E973
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_雷鸟人_普通" then
				   chs[1] = 0xD2286C28
				   chs[2] = 0x0EEDC147
				   chs[3] = "ZHS.FT"
			elseif ch ==  "古代瑞兽_普通" then
				   chs[1] = 0xD9C108BF
				   chs[2] = 0x8BF68D5D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_古代瑞兽_普通" then
				   chs[1] = 0x1F671C28
				   chs[2] = 0xAE550681
				   chs[3] = "ZHS.FT"
			elseif ch ==  "白熊_普通" then
				   chs[1] = 0x24F343D2
				   chs[2] = 0x41D12500
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_白熊_普通" then
				   chs[1] = 0x308185DC
				   chs[2] = 0xC5220058
				   chs[3] = "ZHS.FT"
	------------------------------------55级
			elseif ch ==  "天兵_普通" then
				   chs[1] = 0x2CC4FE10
				   chs[2] = 0x98071CDB
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_天兵_普通" then
				   chs[1] = 0xEEBB6DE7
				   chs[2] = 0xBFB98901
				   chs[3] = "ZHS.FT"
			elseif ch ==  "天将_普通" then
				   chs[1] = 0x04C8C142
				   chs[2] = 0x2D8893D1
				   chs[3] = "ZHS.FT"
			elseif ch ==  "风伯_普通" then
				   chs[1] = 0xF879501E
				   chs[2] = 0x788F12EB
				   chs[3] = "ZHS.FT"
			elseif ch ==  "地狱战神_普通" then
				   chs[1] = 0x12807229
				   chs[2] = 0x7F8C00C3
				   chs[3] = "ZHS.FT"
	------------------------------------65级
			elseif ch ==  "蚌精_普通" then
				   chs[1] = 0x04C8C142
				   chs[2] = 0x56B29D6B
				   chs[3] = "ZHS.FT"
			elseif ch ==  "碧水夜叉_普通" then
				   chs[1] = 0x2CC27C0E
				   chs[2] = 0x480AFDA4
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_碧水夜叉_普通" then
				   chs[1] = 0x63EF7648
				   chs[2] = 0xA43982AB
				   chs[3] = "ZHS.FT"
			elseif ch ==  "鲛人_普通" then
				   chs[1] = 0xE2807954
				   chs[2] = 0xDE138F5E
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_鲛人_普通" then
				   chs[1] = 0x4050ADB1
				   chs[2] = 0x1E2540EF
				   chs[3] = "ZHS.FT"
			elseif ch ==  "雨师_普通" then
				   chs[1] = 0x59B3FCAD
				   chs[2] = 0x0B510184
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_雨师_普通" then
				   chs[1] = 0x554E3F20
				   chs[2] = 0x43065E3B
				   chs[3] = "ZHS.FT"
			elseif ch ==  "蛟龙_普通" then
				   chs[1] = 0x034598C3
				   chs[2] = 0xB157E173
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_蛟龙_普通" then
				   chs[1] = 0xA695F1EA
				   chs[2] = 0x60E2FE6A
				   chs[3] = "ZHS.FT"
			elseif ch ==  "凤凰_普通" then
				   chs[1] = 0x2DD87623
				   chs[2] = 0xA9027E94
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_凤凰_普通" then
				   chs[1] = 0xBF90783B
				   chs[2] = 0xCB7B1B20
				   chs[3] = "ZHS.FT"
	------------------------------------75级
			elseif ch == "锦毛貂精_普通" then
					chs[1] = 0xC768A820
					chs[2] = 0xDFF4FD6A
					chs[3] = "ZHS.FT"
			elseif ch == "饰品_锦毛貂精_普通" then
					chs[1] = 0xB18C9EE6
					chs[2] = 0xAA708449
					chs[3] = "ZHS.FT"
		    elseif ch ==  "千年蛇魅_普通" then
				   chs[1] = 0x511FF3AD
				   chs[2] = 0xDB3ABB96
				   chs[3] = "ZHS.FT"
			elseif ch ==  "犀牛将军人形_普通" then
				   chs[1] = 0xF4714CEA
				   chs[2] = 0xBE8D9F87
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_犀牛将军人形_普通" then
				   chs[1] = 0xA86A8F7F
				   chs[2] = 0x54E43640
				   chs[3] = "ZHS.FT"
			elseif ch ==  "犀牛将军兽形_普通" then
				   chs[1] = 0xC7DEA5CC
				   chs[2] = 0x4C22E015
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_犀牛将军兽形_普通" then
				   chs[1] = 0x7A718D3D
				   chs[2] = 0x5DA38E0D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "芙蓉仙子_普通" then
				   chs[1] = 0x2FEF7615
				   chs[2] = 0xB4028C57
				   chs[3] = "ZHS.FT"
			elseif ch ==  "如意仙子_普通" then
				   chs[1] = 0xA2C8444D
				   chs[2] = 0x85BD39BA
				   chs[3] = "ZHS.FT"
		   elseif ch ==  "饰品_如意仙子_普通" then
				   chs[1] = 0x03E06944
				   chs[2] = 0xD9195BCE
				   chs[3] = "ZHS.FT"
			elseif ch ==  "星灵仙子_普通" then
				   chs[1] = 0x265B5E90
				   chs[2] = 0x58B7A942
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_星灵仙子_普通" then
				   chs[1] = 0x39ADB536
				   chs[2] =  0x2C5DBF70
				   chs[3] = "ZHS.FT"
			elseif ch ==  "巡游天神_普通" then
				   chs[1] = 0xE3E15EAC
				   chs[2] = 0xA3545B8E
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_巡游天神_普通" then
				   chs[1] = 0x0F12BC80
				   chs[2] = 0xEEAC554F
				   chs[3] = "ZHS.FT"
	------------------------------------85级
			elseif ch ==  "百足将军_普通" then
				   chs[1] = 0xC5355942
				   chs[2] = 0xE08B8AE8
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_百足将军_普通" then
				   chs[1] = 0x659E004B
				   chs[2] = 0xC59D3C2D
				   chs[3] = "ZHS.FT"
	        elseif ch ==  "鼠先锋_普通" then
				   chs[1] = 0xA9634C86
				   chs[2] = 0x4BBA23CE
				   chs[3] = "ZHS.FT"
	        elseif ch ==  "饰品_鼠先锋_普通" then
				   chs[1] = 0x8C74B098
				   chs[2] = 0x96D1C377
				   chs[3] = "ZHS.FT"
		    elseif ch ==  "野猪精_普通" then
				   chs[1] = 0xB6CE5945
				   chs[2] = 0x05BFFE67
				   chs[3] = "ZHS.FT"
		    elseif ch ==  "饰品_野猪精_普通" then
				   chs[1] = 0x15274EAE
				   chs[2] = 0xA765DE53
				   chs[3] = "ZHS.FT"
			elseif ch =="镜妖_普通"then
				chs[1]= 0x41CADB40
				chs[2]= 0xEB637A52
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_镜妖_普通"then
				chs[1]= 0x148BFB95
				chs[2]= 0x343705AA
			    chs[3] = "ZHS.FT"
			elseif ch =="泪妖_普通"then
				chs[1]= 0x27BECE4E
				chs[2]= 0x529BDB4A
			    chs[3] = "ZHS.FT"
	------------------------------------95级
			elseif ch ==  "阴阳伞_普通" then
				   chs[1] = 0x6037DEBD
				   chs[2] = 0xBF06CDDA
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_阴阳伞_普通" then
				   chs[1] = 0x02C87FEC
				   chs[2] = 0x19840C7F
				   chs[3] = "ZHS.FT"
	        elseif ch ==  "灵符女娲_普通" then
				   chs[1] = 0x1108DE67
				   chs[2] = 0xE1E4E409
				   chs[3] = "ZHS.FT"
	        elseif ch ==  "饰品_灵符女娲_普通" then
				   chs[1] = 0x6428D20E
				   chs[2] = 0x47667B41
				   chs[3] = "ZHS.FT"
			elseif ch ==  "律法女娲_普通" then
				   chs[1] = 0x82A55C83
				   chs[2] = 0xBFECF819
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_律法女娲_普通" then
				   chs[1] = 0x93AF956A
				   chs[2] = 0x596B5505
				   chs[3] = "ZHS.FT"
			elseif ch ==  "吸血鬼_普通" then
				   chs[1] = 0xA9B78CF6
				   chs[2] = 0x1708205C
				   chs[3] = "ZHS.FT"
			elseif ch ==  "幽灵_普通" then
				   chs[1] = 0xB3084DB8
				   chs[2] = 0x3935CD44
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_幽灵_普通" then
				   chs[1] = 0x0D3666CE
				   chs[2] = 0x3B9976E1
				   chs[3] = "ZHS.FT"
	------------------------------------105级
			elseif ch =="幽萤娃娃_普通"then
			    chs[1]= 0xFECECDED
			    chs[2]= 0x7FFE52A5
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_幽萤娃娃_普通"then
			    chs[1]= 0x34DFB0CD
			    chs[2]= 0xC8E92DFF
			    chs[3] = "ZHS.FT"
			elseif ch == "画魂_普通" then
				chs[1] = 0x947BA25A
				chs[2] = 0x9358CF65
				chs[3] = "ZHS.FT"
			elseif ch == "饰品_画魂_普通" then
				chs[1] = 0xEBCB6424
				chs[2] = 0x25A1C181
				chs[3] = "ZHS.FT"
	        elseif ch ==  "鬼将_普通" then
				   chs[1] = 0xBEAED63A
				   chs[2] = 0x12D27416
				   chs[3] = "ZHS.FT"
	        elseif ch ==  "饰品_鬼将_普通" then
				   chs[1] = 0xBEAED63A
				   chs[2] = 0x12D27416
				   chs[3] = "ZHS.FT"
			elseif ch ==  "净瓶女娲_普通" then
				   chs[1] = 0x3F6E1130
				   chs[2] = 0xC7F51159
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_净瓶女娲_普通" then
				   chs[1] = 0x1A931B54
				   chs[2] = 0x6DF7AE9E
				   chs[3] = "ZHS.FT"
	------------------------------------125级
			elseif ch =="金铙僧_普通"then
			    chs[1]= 0x8631C2B1
			    chs[2]= 0x3113DFBB
			    chs[3] = "ZHS.FT"
			 elseif ch =="武器_金铙僧_普通"then
			    chs[1]= 0x9B77BB9E
			    chs[2]= 0x9A50A304
			    chs[3] = "ZHS.FT"
			 elseif ch =="饰品_金铙僧_普通"then
			    chs[1]= 0xAEA6C98A
			    chs[2]= 0xDB873214
			    chs[3] = "ZHS.FT"
			elseif ch =="琴仙_普通"then
			    chs[1]= 0x23944B4F
			    chs[2]= 0x0C1E363C
			    chs[3] = "ZHS.FT"
			elseif ch ==  "大力金刚_普通" then
				   chs[1] = 0xE3587E7B
				   chs[2] = 0xE2DD30D1
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_大力金刚_普通" then
				   chs[1] = 0x297291AB
				   chs[2] = 0x1BF62956
				   chs[3] = "ZHS.FT"
			elseif ch ==  "夜罗刹_普通" then
				   chs[1] = 0x9CE06363
				   chs[2] = 0xFE85E201
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_夜罗刹_普通" then
				   chs[1] = 0xD990BF34
				   chs[2] = 0x60C67EB9
				   chs[3] = "ZHS.FT"
			elseif ch ==  "灵鹤_普通" then
				   chs[1] = 0x31288244
				   chs[2] = 0x4F069C43
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_灵鹤_普通" then
				   chs[1] = 0x9779BD2D
				   chs[2] = 0xFA80332B
				   chs[3] = "ZHS.FT"
			elseif ch ==  "噬天虎_普通" then
				   chs[1] = 0xAC1EE358
				   chs[2] = 0x116C8AA0
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_噬天虎_普通" then
				   chs[1] = 0x1B07AA53
				   chs[2] = 0xB076C110
				   chs[3] = "ZHS.FT"
			elseif ch ==  "雾中仙_普通" then
				   chs[1] = 0xA0F8A0DD
				   chs[2] = 0x77338BEE
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_雾中仙_普通" then
				   chs[1] = 0x8F188EBA
				   chs[2] = 0xC8ED6E7E
				   chs[3] = "ZHS.FT"
			elseif ch ==  "炎魔神_普通" then
				   chs[1] = 0xE41E0CE2
				   chs[2] = 0x81A85EA4
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_炎魔神_普通" then
				   chs[1] = 0xAD793A2C
				   chs[2] = 0x0862F4FA
				   chs[3] = "ZHS.FT"
	------------------------------------135级
			elseif ch =="葫芦宝贝_普通"then
				 chs[1]= 0xC77749A6
				 chs[2]= 0x599D9243
                 chs[3] = "ZHS.FT"
			elseif ch =="武器_葫芦宝贝_普通"then
				 chs[1]= 0x2C61BE3D
				 chs[2]= 0x9E7D076D
                 chs[3] = "ZHS.FT"
			elseif ch =="饰品_葫芦宝贝_普通"then
				 chs[1]= 0x95F1BC5E
				 chs[2]= 0x548E9E05
                 chs[3] = "ZHS.FT"
			elseif ch =="机关人_普通"then
				 chs[1]= 0xC27A35D2
				 chs[2]= 0x92173C08
                 chs[3] = "ZHS.FT"
			elseif ch =="饰品_机关人_普通"then
				 chs[1]= 0xB1108478
				 chs[2]= 0xF788602C
                 chs[3] = "ZHS.FT"
			elseif ch =="蝎子精_普通"then
				 chs[1]= 0xBA7D48D7
				 chs[2]= 0xD7F44511
                 chs[3] = "ZHS.FT"
			elseif ch =="武器_蝎子精_普通"then
				 chs[1]= 0x717C51A3
				 chs[2]= 0x36A1EE2A
                 chs[3] = "ZHS.FT"
			elseif ch =="饰品_蝎子精_普通"then
				 chs[1]= 0x7F0F1BB9
				 chs[2]= 0xCD0846B7
                 chs[3] = "ZHS.FT"
		    elseif ch =="狂豹兽形_普通"then
			    chs[1]= 0x9D574574
			    chs[2]= 0xD1966358
			     chs[3] = "ZHS.FT"
		    elseif ch =="饰品_狂豹兽形_普通"then
			    chs[1]= 0xF09E0071
			    chs[2]= 0x5F6E2E0D
			     chs[3] = "ZHS.FT"
		    elseif ch =="猫灵兽形_普通"then
			    chs[1]= 0xC5A58467
			    chs[2]= 0x6E4AF049
			      chs[3] = "ZHS.FT"
		    elseif ch =="武器_猫灵兽形_普通"then
			    chs[1]= 0x9DF766DD
			    chs[2]= 0x89B3D5D3
			      chs[3] = "ZHS.FT"
		    elseif ch =="饰品_猫灵兽形_普通"then
			    chs[1]= 0x2FA8BF28
			    chs[2]= 0xDE181E26
			      chs[3] = "ZHS.FT"
			elseif ch ==  "红萼仙子_普通" then
				   chs[1] = 0xE49FB4CA
				   chs[2] = 0xA7E4E9E5
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_红萼仙子_普通" then
				   chs[1] = 0xEAF10B72
				   chs[2] = 0xD550D618
				   chs[3] = "ZHS.FT"
			elseif ch ==  "龙龟_普通" then
				   chs[1] = 0x6CEAFF1E
				   chs[2] = 0x04D2CF27
				   chs[3] = "ZHS.FT"
			elseif ch ==  "饰品_龙龟_普通" then
				   chs[1] = 0xCD3B36BB
				   chs[2] = 0x71D71E7D
				   chs[3] = "ZHS.FT"
			elseif ch ==  "踏云兽_普通" then
				   chs[1] = 0x33724BBA
				   chs[2] = 0xBF5C0DE2
				   chs[3] = "ZHS.FT"
	------------------------------------145级
			elseif ch =="巴蛇_普通"then
				 chs[1]= 0xE636B06F
				 chs[2]= 0xF1665619
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_巴蛇_普通"then
				 chs[1]= 0xE5806D4F
				 chs[2]= 0xED6A93A8
				 chs[3] = "ZHS.FT"
			elseif ch =="机关鸟_普通"then
				 chs[1]= 0x316AFE01
				 chs[2]= 0x52F9A65C
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_机关鸟_普通"then
				 chs[1]= 0xD16F0139
				 chs[2]= 0x757BACE3
				 chs[3] = "ZHS.FT"
			elseif ch =="机关兽_普通"then
				 chs[1]= 0x179128CC
				 chs[2]= 0x03AC1684
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_机关兽_普通"then
				 chs[1]= 0xF03CB9B8
				 chs[2]= 0xD39A52B6
				 chs[3] = "ZHS.FT"
			elseif ch =="连驽车_普通"then
				 chs[1]= 0x7482435E
				 chs[2]= 0x9F0EA2F8
				 chs[3] = "ZHS.FT"
	------------------------------------155级
			elseif ch =="长眉灵猴_普通"then
				 chs[1]= 0xDD227A4C
				chs[2]= 0x6FCDED81
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_长眉灵猴_普通"then
				 chs[1]= 0x80F714F5
				chs[2]= 0xEC658253
				 chs[3] = "ZHS.FT"
			elseif ch =="混沌兽_普通"then
				 chs[1]= 0x79208A5C
				chs[2]= 0xB2528581
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_混沌兽_普通"then
				 chs[1]= 0x2999EB12
				chs[2]= 0xCAC18E35
				 chs[3] = "ZHS.FT"
			elseif ch =="巨力神猿_普通"then
				 chs[1]= 0xD920BF09
				chs[2]= 0x20AE4BF5
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_巨力神猿_普通"then
				 chs[1]= 0xE4D66B65
				chs[2]= 0xCF546A8F
				 chs[3] = "ZHS.FT"
			elseif ch =="狂豹人形_普通"then
				 chs[1]= 0x263F3B50
				chs[2]= 0xDDED951B
				 chs[3] = "ZHS.FT"
			elseif ch =="武器_狂豹人形_普通"then
				 chs[1]= 0x0D96505C
				chs[2]= 0xE6200B25
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_狂豹人形_普通"then
				 chs[1]= 0x42CBF7FD
				chs[2]= 0xED900591
				 chs[3] = "ZHS.FT"
			elseif ch =="猫灵人形_普通"then
				 chs[1]= 0x66CEC074
				chs[2]= 0x8C47DA98
				 chs[3] = "ZHS.FT"
			elseif ch =="武器_猫灵人形_普通"then
				 chs[1]= 0x197F43E1
				chs[2]= 0x57320086
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_猫灵人形_普通"then
				 chs[1]= 0xEF4BB370
				chs[2]= 0xE69856AC
				 chs[3] = "ZHS.FT"
			elseif ch =="藤蔓妖花_普通"then
				 chs[1]= 0x6C4BB701
				chs[2]= 0xE5778E97
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_藤蔓妖花_普通"then
				 chs[1]= 0x9713AC00
				chs[2]= 0x71C31F62
				 chs[3] = "ZHS.FT"
			elseif ch =="修罗傀儡鬼_普通"then
				 chs[1]= 0xD70E1A23
				chs[2]= 0x9B9A0E1B
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_修罗傀儡鬼_普通"then
				 chs[1]= 0x7A997853
				chs[2]= 0xF7A1DCBD
				 chs[3] = "ZHS.FT"
			elseif ch =="蜃气妖_普通"then
				 chs[1]= 0xB689F8D4
				chs[2]= 0x5E9E8FEE
				 chs[3] = "ZHS.FT"
			elseif ch =="饰品_蜃气妖_普通"then
				 chs[1]= 0x9C209C27
				chs[2]= 0x0863C7A9
				 chs[3] = "ZHS.FT"
	------------------------------------165级
			elseif ch =="金身罗汉_普通"then
			chs[1]= 0x7B1F88D8
			chs[2]= 0x289299C4
			 chs[3] = "ZHS.FT"
			elseif ch =="武器_金身罗汉_普通"then
			chs[1]= 0x5BC15465
			chs[2]= 0xFA58A97F
			 chs[3] = "ZHS.FT"
			elseif ch =="饰品_金身罗汉_普通"then
			chs[1]= 0xD02C9BD6
			chs[2]= 0x2C59F696
			 chs[3] = "ZHS.FT"
			elseif ch =="曼珠沙华_普通"then
			chs[1]= 0x45ACDEDF
			chs[2]= 0x26C83C18
			 chs[3] = "ZHS.FT"
			elseif ch =="饰品_曼珠沙华_普通"then
			chs[1]= 0x86A626ED
			chs[2]= 0x0699441B
			 chs[3] = "ZHS.FT"
			elseif ch =="修罗傀儡妖_普通"then
			chs[1]= 0x32FC3B00
			chs[2]= 0x81C678B3
			 chs[3] = "ZHS.FT"
			elseif ch =="饰品_修罗傀儡妖_普通"then
			chs[1]=  0x9D78DA2B
			chs[2]= 0x5942C648
			 chs[3] = "ZHS.FT"
	------------------------------------175级
			elseif ch =="般若天女_普通"then
				chs[1]= 0x3E01B3D0
				chs[2]= 0x7DFE4872
				chs[3] = "ZHS.FT"
			elseif ch =="武器_般若天女_普通"then
				chs[1]= 0x169334C3
				chs[2]= 0xA17AE5BC
				chs[3] = "ZHS.FT"
			elseif ch =="饰品_般若天女_普通"then
				chs[1]= 0x219ABFE2
				chs[2]= 0x3DAECC80
				chs[3] = "ZHS.FT"
			elseif ch =="持国巡守_普通"then
				chs[1]= 0x98BDB61A
				chs[2]= 0x3A55967E
				chs[3] = "ZHS.FT"
			elseif ch =="武器_持国巡守_普通"then
				chs[1]= 0xDBDE3C54
				chs[2]= 0x10593FDB
				chs[3] = "ZHS.FT"
		    elseif ch =="饰品_持国巡守_普通"then
				chs[1]= 0xF92CDC30
				chs[2]= 0xDA8E452B
				chs[3] = "ZHS.FT"
			elseif ch =="灵灯侍者_普通"then
				chs[1]= 0x5723E136
				chs[2]= 0xD06B232C
				chs[3] = "ZHS.FT"
			elseif ch =="武器_灵灯侍者_普通"then
				chs[1]= 0x1857BDFA
				chs[2]=  0xC592F223
				chs[3] = "ZHS.FT"
			elseif ch =="饰品_灵灯侍者_普通"then
				chs[1]= 0xEB2DA1C8
				chs[2]=  0xD7B8C495
				chs[3] = "ZHS.FT"
			elseif ch =="毗舍童子_普通"then
				chs[1]= 0x9B022221
				chs[2]= 0x3AEB838B
				chs[3] = "ZHS.FT"
			elseif ch =="增长巡守_普通"then
				chs[1]= 0x5886EC62
				chs[2]= 0x82845BA2
				chs[3] = "ZHS.FT"
			elseif ch =="武器_增长巡守_普通"then
				chs[1]= 0x0B75D87B
				chs[2]= 0xE45C72A2
				chs[3] = "ZHS.FT"
			elseif ch =="饰品_增长巡守_普通"then
				chs[1]= 0x6647D1E6
				chs[2]= 0x9528DFFA
				chs[3] = "ZHS.FT"
			elseif ch =="真陀护法_普通"then
				chs[1]= 0x6C19FF7F
				chs[2]= 0xA0A4FCCE
				chs[3] = "ZHS.FT"
			elseif ch =="武器_真陀护法_普通"then
				chs[1]= 0x7ECE7677
				chs[2]= 0x0155F7E6
				chs[3] = "ZHS.FT"
			elseif ch =="饰品_真陀护法_普通"then
				chs[1]= 0x119A18A5
				chs[2]= 0xB55EAE46
				chs[3] = "ZHS.FT"
	------------------------------------45级进阶
			elseif ch =="进阶白熊_普通"then
			    chs[1]= 0xD7BF0EE0
			    chs[2]= 0x2F767DF8
			     chs[3] = "ZHS.FT"

			elseif ch =="进阶古代瑞兽_普通"then
			    chs[1]= 0x1798EA00
			    chs[2]= 0xDA3BC474
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶古代瑞兽_普通"then
			    chs[1]= 0xD4A3D645
			    chs[2]= 0xC9B014AF
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶黑山老妖_普通"then
			    chs[1]= 0xBF3AC969
			    chs[2]= 0x94258F07
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶黑山老妖_普通"then
			    chs[1]= 0x568B725F
			    chs[2]= 0xCB580FB4
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶蝴蝶仙子_普通"then
			    chs[1]= 0x40267F4C
			    chs[2]= 0xA07F8533
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶蝴蝶仙子_普通"then
			    chs[1]= 0xB1248728
			    chs[2]= 0x42EAEA18
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶雷鸟人_普通"then
			    chs[1]= 0x20393E62
			    chs[2]= 0xE3FB3536
			     chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶雷鸟人_普通"then
			    chs[1]= 0x72EFFC7B
			    chs[2]= 0xBAD79452
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶雷鸟人_普通"then
			    chs[1]= 0x593879B6
			    chs[2]= 0x0B696995
			     chs[3] = "ZHS.FT"
	------------------------------------55级
			elseif ch =="进阶地狱战神_普通"then
			    chs[1]= 0x6B19D682
			    chs[2]= 0x9F9298AB
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶地狱战神_普通"then
			    chs[1]= 0x736760B3
			    chs[2]= 0x5CDEF6F4
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶风伯_普通"then
			    chs[1]= 0x21E83685
			    chs[2]= 0x5D4A0DA2
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶风伯_普通"then
			    chs[1]=  0xF9E48C6F
			    chs[2]=  0x19F7D70C
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶风伯_普通"then
			    chs[1]=  0x7461360D
			    chs[2]=  0xBAEAA6D1
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶天兵_普通"then
			    chs[1]= 0x93150846
			    chs[2]= 0x5F4DDF7F
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶天兵_普通"then
			    chs[1]= 0x76D984D5
			    chs[2]= 0xCA5278E8
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶天兵_普通"then
			    chs[1]= 0xF7A04CD7
			    chs[2]= 0xCAA3CD18
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶天将_普通"then
			    chs[1]= 0x236C7E78
			    chs[2]= 0x7D069CD3
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶天将_普通"then
			    chs[1]= 0xD23301B9
			    chs[2]= 0x552EAF9A
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶天将_普通"then
			    chs[1]= 0xBBCC16DF
			    chs[2]= 0x13302548
			    chs[3] = "ZHS.FT"
	------------------------------------65级
			elseif ch =="进阶蚌精_普通"then
			    chs[1]= 0x31454554
			    chs[2]= 0x50DB2F65
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶蚌精_普通"then
			    chs[1]= 0xEE7EE57F
			    chs[2]= 0x1FAF4B1B
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶碧水夜叉_普通"then
			    chs[1]= 0x1EDA1CD8
			    chs[2]= 0xA7E11652
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶碧水夜叉_普通"then
			    chs[1]= 0xB1FCF9BC
			    chs[2]= 0x347DEDB7
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶碧水夜叉_普通"then
			    chs[1]= 0x4062C921
			    chs[2]= 0x0DBF706C
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶凤凰_普通"then
			    chs[1]= 0x8AE90761
			    chs[2]= 0xDA528ED1
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶凤凰_普通"then
			    chs[1]= 0xF0F800BF
			    chs[2]= 0x6B3050DA
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶凤凰_普通"then
			    chs[1]= 0x519D5917
			    chs[2]= 0xFE804156
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶雨师_普通"then
			    chs[1]= 0x80607671
			    chs[2]= 0x7D912078
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶雨师_普通"then
			    chs[1]= 0x9670C666
			    chs[2]= 0x2295A2DC
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶蛟龙_普通"then
			    chs[1]= 0x48915E95
			    chs[2]= 0xC3D151FF
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶蛟龙_普通"then
			    chs[1]= 0x20E5B4BF
			    chs[2]= 0x4A8D4D2C
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶蛟龙_普通"then
			    chs[1]= 0xA552B800
			    chs[2]= 0x85E8E3E8
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶鲛人_普通"then
			    chs[1]= 0x78ED4C3F
			    chs[2]= 0x930A30A2
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶鲛人_普通"then
			    chs[1]=0x53E54FB8
			    chs[2]=  0x4103BBAE
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶鲛人_普通"then
			    chs[1]=0x0E964E2F
			    chs[2]=  0x1EAE31D6
			    chs[3] = "ZHS.FT"
	------------------------------------75级
			elseif ch =="进阶锦毛貂精_普通"then
			     chs[1]= 0x99544991
			    chs[2]= 0xFC3D1D87
			     chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶锦毛貂精_普通"then
			     chs[1]= 0x9DF80203
			    chs[2]=  0xB98310C8
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶千年蛇魅_普通"then
			     chs[1]= 0xA14F4A62
			    chs[2]= 0xEF00BB2C
			     chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶千年蛇魅_普通"then
			     chs[1]=  0x2FE148ED
			    chs[2]=  0xCB167002
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶千年蛇魅_普通"then
			     chs[1]=  0x6269EE72
			    chs[2]=  0x1B41E74C
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶如意仙子_普通"then
			     chs[1]= 0x2F00B37A
			    chs[2]= 0xEB6AEEF4
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶如意仙子_普通"then
			     chs[1]= 0xC12C81CA
			    chs[2]= 0x580794D1
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶犀牛将军人形_普通"then
			     chs[1]= 0xB1156F73
			    chs[2]= 0xEFC00198
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶犀牛将军人形_普通"then
			     chs[1]= 0xB92BC047
			    chs[2]= 0xD77B1FCB
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶犀牛将军兽形_普通"then
			     chs[1]=  0x6C15BE14
			    chs[2]= 0x605E3B33
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶犀牛将军兽形_普通"then
			     chs[1]=  0xE538108A
			    chs[2]=  0x3086BBC0
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶星灵仙子_普通"then
			     chs[1]= 0x9DB4E74E
			    chs[2]= 0xEBB561D8
			     chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶星灵仙子_普通"then
			     chs[1]=0x980253AF
			    chs[2]= 0x4582CE4D
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶星灵仙子_普通"then
			     chs[1]=0xC21FB81B
			    chs[2]= 0x9723D7E4
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶巡游天神_普通"then
			     chs[1]= 0x533DE9F7
			    chs[2]= 0xFBA6389C
			     chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶巡游天神_普通"then
			     chs[1]= 0x5AC4DDCF
			    chs[2]= 0xF128405E
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶巡游天神_普通"then
			     chs[1]= 0xD462D477
			    chs[2]=  0xD2FC5207
			     chs[3] = "ZHS.FT"
			elseif ch =="进阶芙蓉仙子_普通"then
			     chs[1]= 0x2206E612
			    chs[2]= 0x96BC94AC
			     chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶芙蓉仙子_普通"then
			     chs[1]=0x88039B5A
			    chs[2]= 0xD8E75FB4
			     chs[3] = "ZHS.FT"
	------------------------------------85级
			elseif ch =="进阶百足将军_普通"then
			     chs[1]= 0x358DCB43
			    chs[2]= 0x54FB5E1C
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶百足将军_普通"then
			     chs[1]= 0x695C5BF5
			    chs[2]= 0xEF83C192
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶百足将军_普通"then
			     chs[1]= 0xAFA9C2EC
			    chs[2]= 0x4E2E6307
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶镜妖_普通"then
			     chs[1]= 0xE3ECCDCD
			    chs[2]= 0xB90F8E0A
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶镜妖_普通"then
			     chs[1]= 0xB57EB974
			    chs[2]= 0xC957CC8D
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶泪妖_普通"then
			     chs[1]= 0x13F0C5C2
			    chs[2]= 0x97779A94
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶鼠先锋_普通"then
			     chs[1]= 0xE48308FC
			    chs[2]= 0xF13F6EF1
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶鼠先锋_普通"then
			     chs[1]= 0xDEE37B84
			    chs[2]=  0x87A5A35D
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶野猪精_普通"then
			     chs[1]= 0x91BB8E60
			    chs[2]= 0x292706E6
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶野猪精_普通"then
			     chs[1]= 0x9B807FAB
			    chs[2]= 0xE049DC54
			    chs[3] = "ZHS.FT"
		    elseif ch =="饰品_进阶野猪精_普通"then
			     chs[1]= 0x686562D4
			    chs[2]= 0xFE6AA039
			    chs[3] = "ZHS.FT"
			     elseif ch =="饰品_吸血鬼_普通"then
			 	     chs[1]= 0xDE22B5AC
			    chs[2]= 0xD58C431F
			    chs[3] = "ZHS.FT"
	------------------------------------95级
			elseif ch =="进阶灵符女娲_普通"then
			     chs[1]= 0x1F56240E
			    chs[2]= 0x32811A70
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶灵符女娲_普通"then
			     chs[1]= 0xF1DB39D7
			    chs[2]= 0x81508729
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶律法女娲_普通"then
			     chs[1]= 0x6275654E
			    chs[2]= 0xD2A7D7F7
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶律法女娲_普通"then
			     chs[1]= 0x8951A17F
			    chs[2]= 0xA8BBA5AF
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶吸血鬼_普通"then
			     chs[1]= 0xEF26063C
			    chs[2]= 0x573B321A
			    chs[3] = "ZHS.FT"
			    elseif ch =="吸血鬼_普通"then
			     chs[1]= 0xDE22B5AC
			    chs[2]= 0xD58C431F
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶吸血鬼_普通"then
			     chs[1]= 0xD7516360
			    chs[2]= 0x204E5379
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶吸血鬼_普通"then
			     chs[1]= 0xE7751553
			    chs[2]= 0xED36DA6F
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶阴阳伞_普通"then
			     chs[1]= 0x1AFDC3F6
			    chs[2]= 0x50362472
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶阴阳伞_普通"then
			     chs[1]= 0xC0594A8A
			    chs[2]= 0x1C6F39EB
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶幽灵_普通"then
			     chs[1]= 0x5990A7C9
			    chs[2]= 0xFC648705
			    chs[3] = "ZHS.FT"
	------------------------------------105级
			elseif ch =="进阶鬼将_普通"then
			     chs[1]= 0x275E43D0
			    chs[2]= 0xCB75BA5A
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶鬼将_普通"then
			     chs[1]= 0x4AA66287
			    chs[2]= 0xF685DFAD
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶鬼将_普通"then
			     chs[1]= 0x01BC27CA
			    chs[2]= 0x7C4018B6
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶画魂_普通"then
			     chs[1]= 0xBAEE2ABE
			    chs[2]= 0x87508114
			    chs[3] = "ZHS.FT"
		   elseif ch =="饰品_进阶画魂_普通"then
			     chs[1]= 0xA5387256
			    chs[2]= 0x5A8CE8EB
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶净瓶女娲_普通"then
			     chs[1]= 0x1B5425E6
			    chs[2]= 0xAF851642
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶净瓶女娲_普通"then
			     chs[1]= 0x79F863A9
			    chs[2]= 0x61723C50
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶幽萤娃娃_普通"then
			     chs[1]= 0x11FF6780
			    chs[2]= 0x3A093FAF
			    chs[3] = "ZHS.FT"
	------------------------------------125级
			elseif ch =="进阶大力金刚_普通"then
			     chs[1]= 0xE096B9B6
			    chs[2]= 0x9B1D1514
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶大力金刚_普通"then
			     chs[1]= 0xB922E0AA
			    chs[2]= 0x8702BF5B
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶大力金刚_普通"then
			     chs[1]= 0x4E4CD739
			    chs[2]= 0x9652FE7A
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶金铙僧_普通"then
			     chs[1]= 0x1BBF2CFB
			    chs[2]= 0x01927405
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶金铙僧_普通"then
			     chs[1]=  0xC3870AAF
			    chs[2]= 0x8C166D84
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶金铙僧_普通"then
			     chs[1]=  0x5AA4379F
			    chs[2]= 0xF0D58F35
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶灵鹤_普通"then
			     chs[1]= 0xC88AB042
			    chs[2]= 0xCB26BF39
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶琴仙_普通"then
			     chs[1]= 0x4CA62CED
			    chs[2]= 0xDCDBF3A3
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶噬天虎_普通"then
			     chs[1]= 0x4B31B9DB
			    chs[2]= 0xCBBB2541
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶噬天虎_普通"then
			     chs[1]= 0x09742776
			    chs[2]= 0x1F6A3AA7
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶雾中仙_普通"then
			     chs[1]= 0x2D88FD84
			    chs[2]= 0x6C3F15EC
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶雾中仙_普通"then
			     chs[1]=  0x8D47A766
			    chs[2]=  0x5BC344C9
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶炎魔神_普通"then
			     chs[1]= 0xEA21D521
			    chs[2]= 0x08F54545
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶炎魔神_普通"then
			     chs[1]= 0x204E1063
			    chs[2]= 0x29646894
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶炎魔神_普通"then
			     chs[1]= 0xD0EF4613
			    chs[2]= 0x6E36E60E
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶夜罗刹_普通"then
			     chs[1]= 0x9FB38604
			    chs[2]= 0xF42A579E
			    chs[3] = "ZHS.FT"
	------------------------------------135级
			elseif ch =="进阶红萼仙子_普通"then
			     chs[1]= 0x21EC36BA
			    chs[2]= 0x0780CEE2
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶葫芦宝贝_普通"then
			     chs[1]= 0x586B462C
			    chs[2]= 0x0B5EB183
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶机关人_普通"then
			     chs[1]= 0xC7EDA495
			    chs[2]= 0x36F172ED
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶机关人_普通"then
			     chs[1]= 0xEF2AF51F
			    chs[2]= 0x995D926E
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶机关人_普通"then
			     chs[1]= 0x567B4519
			    chs[2]= 0x79061DDB
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶狂豹兽形_普通"then
			     chs[1]= 0x4C2863D5
			    chs[2]= 0xE27E0F07
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶龙龟_普通"then
			     chs[1]= 0xA599AEE0
			    chs[2]= 0xE38771B9
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶龙龟_普通"then
			     chs[1]= 0x7B925FC8
			    chs[2]= 0x918FF89D
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶猫灵兽形_普通"then
			     chs[1]= 0x5D711007
			    chs[2]= 0x2AFCEB20
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶踏云兽_普通"then
			     chs[1]= 0xB13667B0
			    chs[2]= 0x4194B6ED
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶踏云兽_普通"then
			     chs[1]= 0x5F1AC730
			    chs[2]=  0xB62295AB
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶踏云兽_普通"then
			     chs[1]= 0x48CD3EC1
			    chs[2]=  0x4BE21568
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶蝎子精_普通"then
			     chs[1]= 0xC433446F
			    chs[2]= 0xD48ACEB0
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶蝎子精_普通"then
			     chs[1]= 0x4E827754
			    chs[2]=  0xF9044ED0
			    chs[3] = "ZHS.FT"
	------------------------------------145级
			elseif ch =="进阶巴蛇_普通"then
			     chs[1]= 0xFAA2148A
			    chs[2]= 0xBABC11FD
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶巴蛇_普通"then
			     chs[1]= 0x62F29527
			    chs[2]= 0x29E98226
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶机关鸟_普通"then
			     chs[1]= 0x7FBF3FBC
			    chs[2]= 0x557E8420
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶机关鸟_普通"then
			     chs[1]= 0x86BEF83A
			    chs[2]= 0x6CF13BFF
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶机关鸟_普通"then
			     chs[1]=  0x1C98B510
			    chs[2]= 0x34F8BB34
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶机关兽_普通"then
			     chs[1]= 0x3C4007F5
			    chs[2]= 0x9CE1B447
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶机关兽_普通"then
			     chs[1]= 0xF2EA1BC3
			    chs[2]= 0xE46A6184
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶连驽车_普通"then
			     chs[1]= 0xEDE79D1B
			    chs[2]= 0x16B3E177
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶连驽车_普通"then
			     chs[1]= 0x89C32F37
			    chs[2]= 0x639A3CD0
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶连驽车_普通"then
			     chs[1]= 0x05B30C39
			    chs[2]= 0x3F9690B7
			    chs[3] = "ZHS.FT"
	------------------------------------155级
			elseif ch =="进阶长眉灵猴_普通"then
			     chs[1]= 0xD0889100
			    chs[2]= 0xDEE20947
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶长眉灵猴_普通"then
			     chs[1]= 0xA04E0801
			    chs[2]= 0xA7102F86
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶长眉灵猴_普通"then
			     chs[1]= 0xE03D0029
			    chs[2]= 0x5EBE9C44
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶混沌兽_普通"then
			     chs[1]= 0xB6C7E183
			    chs[2]= 0x2E8D1286
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶混沌兽_普通"then
			     chs[1]= 0xC7C133B9
			    chs[2]= 0xAB7B4719
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶巨力神猿_普通"then
			     chs[1]= 0x06B8518E
			    chs[2]= 0x99E3C537
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶巨力神猿_普通"then
			     chs[1]= 0x29519167
			    chs[2]= 0x0CD5AF90
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶巨力神猿_普通"then
			     chs[1]= 0x74C8444E
			    chs[2]= 0x61B2BA5C
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶狂豹人形_普通"then
			     chs[1]= 0xE9E47BF8
			    chs[2]= 0x2A1B8C69
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶猫灵人形_普通"then
			     chs[1]= 0x8256C28
			    chs[2]= 0x82EC8664
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶猫灵人形_普通"then
			     chs[1]= 0xEC9B0AEA
			    chs[2]= 0xB0E8A165
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶藤蔓妖花_普通"then
			     chs[1]= 0xFB7B4EA4
			    chs[2]= 0xDC43CAD3
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶修罗傀儡鬼_普通"then
			     chs[1]= 0x19AF45FA
			    chs[2]= 0x2F6A0446
			    chs[3] = "ZHS.FT"
		   elseif ch =="饰品_进阶修罗傀儡鬼_普通"then
			     chs[1]= 0x307B4F17
			    chs[2]= 0xB5BFE9B5
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶蜃气妖_普通"then
			     chs[1]= 0x304CE44C
			    chs[2]= 0x49DD3B77
			    chs[3] = "ZHS.FT"
	------------------------------------165级
			elseif ch =="进阶金身罗汉_普通"then
			     chs[1]= 0x6BF3CC00
			    chs[2]= 0xF4973A76
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶金身罗汉_普通"then
			     chs[1]= 0x97999AC2
			    chs[2]= 0xACC99864
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶金身罗汉_普通"then
			     chs[1]= 0x5B9E5132
			    chs[2]= 0x48DFA598
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶曼珠沙华_普通"then
			     chs[1]= 0xD4400E10
			    chs[2]= 0x0A22889E
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶修罗傀儡妖_普通"then
			     chs[1]= 0x9ADB6CAC
			    chs[2]= 0xE3026E8A
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶修罗傀儡妖_普通"then
			     chs[1]= 0xADB211E7
			    chs[2]= 0xC0F39C55
			    chs[3] = "ZHS.FT"
	------------------------------------175级
			elseif ch =="进阶般若天女_普通"then
			     chs[1]= 0x802F930D
			    chs[2]= 0x6A0FF793
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶般若天女_普通"then
			     chs[1]= 0xBB03CFCA
			    chs[2]= 0x658E8F8B
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶般若天女_普通"then
			     chs[1]=  0xD7B7AC58
			    chs[2]= 0x8159719C
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶持国巡守_普通"then
			     chs[1]= 0x990FC5A1
			    chs[2]= 0xA5E9EF08
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶灵灯侍者_普通"then
			     chs[1]= 0x8B46AE73
			    chs[2]= 0xF0590BB0
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶灵灯侍者_普通"then
			     chs[1]= 0x2E6C2503
			    chs[2]= 0xC43C8B6B
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶毗舍童子_普通"then
			     chs[1]= 0x16AF10BA
			    chs[2]= 0xBA4E18FC
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶毗舍童子_普通"then
			     chs[1]= 0xB953D04F
			    chs[2]= 0xE0DFAA3B
			    chs[3] = "ZHS.FT"
			elseif ch =="饰品_进阶毗舍童子_普通"then
			     chs[1]= 0x2E667B16
			    chs[2]= 0xB4B98E34
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶增长巡守_普通"then
			     chs[1]= 0x4CDF834E
			    chs[2]= 0xA9ED497B
			    chs[3] = "ZHS.FT"
			elseif ch =="进阶真陀护法_普通"then
			     chs[1]= 0x56F4A8CE
			    chs[2]= 0x0E6CDB9E
			    chs[3] = "ZHS.FT"
			elseif ch =="武器_进阶真陀护法_普通"then
			     chs[1]= 0x6EA65783
			    chs[2]= 0x668A49E3
			    chs[3] = "ZHS.FT"
	------------------------------------神兽
			elseif ch =="超级白泽_普通"then
				chs[1]= 0x9E17A3F2
				chs[2]= 0x0DB7BD78
				chs[3] = "ZHS.FT"
			elseif ch =="超级小白泽_普通"then
				chs[1]= 0x2E25F2D9
				chs[2]= 0x35262187
				chs[3] = "ZHS.FT"
			elseif ch =="超级赤焰兽_普通"then
				chs[1]= 0xF3FAEC33
				chs[2]= 0x98F7D5EA
				chs[3] = "ZHS.FT"
			elseif ch =="超级大鹏_普通"then
				chs[1]= 0xEB61471A
				chs[2]= 0x05DC68B5
				chs[3] = "ZHS.FT"
			elseif ch =="超级大象_普通"then
				chs[1]= 0x3D46505C
				chs[2]= 0x62AA4BE1
				chs[3] = "ZHS.FT"
			elseif ch =="超级大熊猫_普通"then
				chs[1]= 0x17C37276
				chs[2]= 0x216B2745
				chs[3] = "ZHS.FT"
			elseif ch =="超级海豚_普通"then
				chs[1]= 0x8F783300
				chs[2]= 0x22D302F4
				chs[3] = "ZHS.FT"
			elseif ch =="超级筋斗云_普通"then
				chs[1]= 0x0DC1C46C
				chs[2]= 0x02379071
				chs[3] = "ZHS.FT"
			elseif ch =="超级孔雀_普通"then
				chs[1]= 0xF225F2D5
				chs[2]= 0x1D87262C
				chs[3] = "ZHS.FT"
			elseif ch =="超级金猴_普通"then
				chs[1]= 0xED850381
				chs[2]= 0xF98BEFBD
				chs[3] = "ZHS.FT"
			elseif ch =="超级灵狐_普通"then
				chs[1]= 0x4FDE0C86
				chs[2]= 0x90E6F577
				chs[3] = "ZHS.FT"
			elseif ch =="超级灵鹿_普通"then
				chs[1]= 0x20F2C401
				chs[2]= 0x8CE0777D
				chs[3] = "ZHS.FT"
			elseif ch =="超级六耳猕猴_普通"then
				chs[1]= 0xF8CE22F4
					chs[2]= 0x383054A8
					chs[3] = "ZHS.FT"
			elseif ch =="超级泡泡_普通"then
				chs[1]= 0x574EEAB7
				chs[2]= 0xC3C3CE08
				chs[3] = "ZHS.FT"
			elseif ch =="超级青鸾_普通"then
				chs[1]= 0x780A7A13
				chs[2]= 0xAEEFFEB2
				chs[3] = "ZHS.FT"
			elseif ch =="超级人参娃娃_普通"then
				chs[1]= 0xD01C7435
				chs[2]= 0x11BE9AEF
				chs[3] = "ZHS.FT"
			elseif ch =="超级神猴_普通"then
				chs[1]= 0xC562D617
				chs[2]= 0xEC31C18E
				chs[3] = "ZHS.FT"
			elseif ch =="超级神虎_普通"then
				chs[1]= 0x56688360
				chs[2]= 0x5A223551
				chs[3] = "ZHS.FT"
			elseif ch =="超级神鸡_普通"then
				chs[1]= 0x2BDAF419
				chs[2]= 0x2C277A7E
				chs[3] = "ZHS.FT"
			elseif ch =="超级神龙_普通"then
				chs[1]= 0x7B124653
				chs[2]= 0x00A896B4
				chs[3] = "ZHS.FT"
			elseif ch =="超级神马_普通"then
				chs[1]= 0x29221139
				chs[2]= 0xABEBBD99
				chs[3] = "ZHS.FT"
			elseif ch =="超级神牛_普通"then
				chs[1]= 0x9F7CB714
				chs[2]= 0xC76E74A1
				chs[3] = "ZHS.FT"
			elseif ch =="超级神蛇_普通"then
				chs[1]= 0x5B9D0DCC
				chs[2]= 0xDD52C93A
				chs[3] = "ZHS.FT"
			elseif ch =="超级神兔_普通"then
				chs[1]= 0x42EEC759
				chs[2]= 0xF807415E
				chs[3] = "ZHS.FT"
			elseif ch =="超级神羊_普通"then
				chs[1]= 0x994182A0
				chs[2]= 0x7C69EEEC
				chs[3] = "ZHS.FT"
			elseif ch =="超级腾蛇_普通"then
				chs[1]= 0xBEB42B08
				chs[2]= 0x06F52F0B
				chs[3] = "ZHS.FT"
			elseif ch =="超级天马_普通"then
				chs[1]= 0x8801FE92
				chs[2]= 0x598AA693
				chs[3] = "ZHS.FT"
			elseif ch =="超级土地公公_普通"then
				chs[1]= 0x41BADAF6
				chs[2]= 0x1871EC74
				chs[3] = "ZHS.FT"
			elseif ch =="超级仙鹿_普通"then
				chs[1]= 0x746AC442
				chs[2]= 0x3973278F
				chs[3] = "ZHS.FT"
			elseif ch =="超级玉兔_普通"then
				chs[1]= 0x1DA4FE4F
				chs[2]= 0x7E1310CA
				chs[3] = "ZHS.FT"
			elseif ch =="超级麒麟_普通"then
				chs[1]= 0x5DC917BD
				chs[2]= 0xD719314F
				chs[3] = "ZHS.FT"
    -------------------------进阶神兽
   		    elseif ch =="进阶超级筋斗云_普通"then
				chs[1]= 0x85D20F2E
				chs[2]= 0xFC743D1A
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级白泽_普通"then
				chs[1]= 0xAA27D3BB
				chs[2]= 0x216094B0
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级赤焰兽_普通"then
				chs[1]= 0x183AB443
				chs[2]= 0x2B9DCF67
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级大鹏_普通"then
				chs[1]= 0x3AF99E76
				chs[2]= 0xE16D5890
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级大象_普通"then
				chs[1]= 0x8E5B6E48
				chs[2]= 0x75E91E7D
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级大熊猫_普通"then
				chs[1]= 0x4FF2EBAB
				chs[2]= 0x060E0098
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级海豚_普通"then
				chs[1]= 0x4B592393
				chs[2]= 0x8FE92E81
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级孔雀_普通"then
				chs[1]= 0x6A788030
				chs[2]= 0x6F734AC2
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级金猴_普通"then
				chs[1]= 0x5CFB35A4
				chs[2]= 0x1115350E
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级灵狐_普通"then
				chs[1]= 0xB45739B6
				chs[2]= 0x667BFD7B
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级灵鹿_普通"then
				chs[1]= 0x49578F89
				chs[2]= 0x4E23E18C
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级六耳猕猴_普通"then
				chs[1]= 0x5AE939F4
				chs[2]= 0x6EC709E1
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级泡泡_普通"then
				chs[1]= 0xE2D684A7
				chs[2]= 0x90C49784
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级青鸾_普通"then
				chs[1]= 0x5A74E42F
				chs[2]= 0x22ADF5E1
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级人参娃娃_普通"then
				chs[1]= 0x2BC67A5F
				chs[2]= 0x06258C20
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神猴_普通"then
				chs[1]= 0x5BA0B69C
				chs[2]= 0x04A109ED
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神虎_普通"then
				chs[1]= 0x1174DEC2
				chs[2]= 0xCDBFE2DC
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神鸡_普通"then
				chs[1]= 0x3291C30D
				chs[2]= 0x2AD92B5F
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神龙_普通"then
				chs[1]= 0x5FF3EEFD
				chs[2]= 0xDC2C33F6
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神马_普通"then
				chs[1]= 0x7F354CC7
				chs[2]= 0xEB15762A
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神牛_普通"then
				chs[1]= 0xA9C5802F
				chs[2]= 0xB068D594
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神蛇_普通"then
				chs[1]= 0xB94403FE
				chs[2]= 0x94E21235
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神兔_普通"then
				chs[1]= 0xF9777AA1
				chs[2]= 0xDA0CB5D7
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级神羊_普通"then
				chs[1]= 0x2296B535
				chs[2]= 0x4441ECB7
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级土地公公_普通"then
				chs[1]= 0x0F422E08
				chs[2]= 0x1AF0A040
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级玉兔_普通"then
				chs[1]= 0x5A396A2F
				chs[2]= 0x5A396A30
				chs[3] = "ZHS.FT"
			elseif ch =="进阶超级麒麟_普通"then
				chs[1]= 0xFF1A400D
				chs[2]= 0x22AB1D22
				chs[3] = "ZHS.FT"
	------------------------------------孩子
			elseif ch ==  "小精灵_普通" then
				   chs[1] = 0xB26BBE81
				   chs[2] = 0xF3A9B571
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小毛头_普通" then
				   chs[1] = 0xC55222A4
				   chs[2] = 0x5E36834F
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小魔头_普通" then
				   chs[1] = 0xF02CE935
				   chs[2] = 0x611E25CC
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小神灵_普通" then
				   chs[1] = 0x92F244E3
				   chs[2] = 0x9330E1CF
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小仙女_普通" then
				   chs[1] = 0x4D70F380
				   chs[2] = 0xA6401032
				   chs[3] = "ZHS.FT"
			elseif ch ==  "小丫丫_普通" then
				   chs[1] = 0xE61E56B9
				   chs[2] = 0x6B6EDE08
				   chs[3] = "ZHS.FT"
	------------------------------------NPC
		elseif ch =="桃树_普通"then
				chs[1]= 0x544A2675
				chs[2]= 0x544A2675
				chs[3] = "ZHS.FT"
		elseif ch =="帮派机关人_普通"then
				chs[1]= 0x48EFADFA
				chs[2]= 0x48EFADFA
				chs[3] = "JM.FT"

		elseif ch =="宝箱_普通"then
				chs[1]= 0xEFFD57F2
				chs[2]= 0xEFFD57F2
				chs[3] = "ZHS.FT"
		elseif ch =="小白象_普通"then
				chs[1]= 0x098C776C
				chs[2]= 0x7263E8FA
				chs[3] = "ZHS.FT"
		elseif ch =="李靖_普通"then
				chs[1]= 0xB760B5CC
				chs[2]= 0x431A24FA
				chs[3] = "ZHS.FT"
		elseif ch =="模型_普通"then
				chs[1]= 0xBFCA22B3
				chs[2]= 0xC69C5C9B
				chs[3] = "ZHS.FT"
		elseif ch =="东海龙王_普通"then
				chs[1]= 0x263C28F5
				chs[2]= 0x77748072
				chs[3] = "ZHS.FT"
		elseif ch =="杨戬_普通"then--没有
				chs[1]= 0x894CBF63
				chs[2]= 0x894CBF63
				chs[3] = "ZHS.FT"
		elseif ch =="地藏王_普通"then
				chs[1]= 0x16B69387
				chs[2]= 0xD13E1FFF
				chs[3] = "ZHS.FT"
		elseif ch =="帮派守护兽_普通"then
				chs[1]= 0xD252E998
				chs[2]= 0xF70F4844
				chs[3] = "ZHS.FT"
		-- elseif ch =="机关人_普通"then
		-- 		chs[1]= 0xD252E998
		-- 		chs[2]= 0xF70F4844
		-- 		chs[3] = "ZHS.FT"
		elseif ch =="九头精怪_普通"then
				chs[1]= 0x7CAF3ABB
				chs[2]= 0x0B8285F9
				chs[3] = "ZHS.FT"
		elseif ch =="孙婆婆_普通"then
				chs[1]= 0x81F4F89D
				chs[2]= 0x600BA182
				chs[3] = "ZHS.FT"
		elseif ch =="大大王_普通"then
				chs[1]= 0x3F24ED26
				chs[2]= 0xAD23D962
				chs[3] = "ZHS.FT"
		elseif ch =="知了王_普通"then
				chs[1]= 0x790BD1B7
				chs[2]= 0x05B6E64F
				chs[3] = "ZHS.FT"
		elseif ch =="周杰伦_普通"then
				chs[1]= 0x5DACE991
				chs[2]= 0x1E12FFC4
				chs[3] = "ZHS.FT"
		elseif ch =="菩提祖师_普通"then
				chs[1]= 0x0D8C4E53
				chs[2]= 0x31B5E0BE
				chs[3] = "ZHS.FT"
		elseif ch =="观音姐姐_普通"then
				chs[1]= 0x0405C585
				chs[2]= 0xF45349EA
				chs[3] = "ZHS.FT"
		elseif ch =="进阶蚩尤_普通"then
				chs[1]= 0x5D1C654C
				chs[2]= 0x84E89BDB
				chs[3] = "ZHS.FT"
		elseif ch =="哮天犬_普通"then
				chs[1]= 0x39796CC6
				chs[2]= 0xEE55164B
				chs[3] = "ZHS.FT"
		elseif ch =="进阶哮天犬_普通"then
				chs[1]= 0x09351393
				chs[2]= 0x8A284760
				chs[3] = "ZHS.FT"
		elseif ch =="镇元子_普通"then
				chs[1]= 0x22406E66
				chs[2]= 0x5E8CC3F3
				chs[3] = "ZHS.FT"
		elseif ch =="程咬金_普通"then
				chs[1]= 0xE1A120FF
				chs[2]= 0x58F4B663
				chs[3] = "ZHS.FT"
		elseif ch =="孙悟空_普通"then
				chs[1]= 0x14692807
				chs[2]= 0x2C4D8EC8
				chs[3] = "ZHS.FT"
		elseif ch =="蚩尤_普通"then
				chs[1]= 0x00000220
				chs[2]= 0x00000224
				chs[3] = "ZHS.FT"
		elseif ch ==  "二郎神_普通" then---
			   chs[1] = 0x1F5F4404
			   chs[2] = 0xF87DE3EF
			   chs[3] = "ZY.FT"
			   chs[4] =0xCF6966D6
		elseif ch ==  "捕鱼人_普通" then
			   chs[1] = 0x767095C2
			   chs[2] = 0x3792B5F3
			   chs[3] = "ZY.FT"--
		elseif ch ==  "超级巫医_普通" then
			   chs[1] = 0x3A16E96D
			   chs[2] = 0xDCEBC9E8
			   chs[3] = "ZY.FT"--
		elseif ch ==  "翠花_普通" then--
			   chs[1] = 0x70357245
			   chs[2] = 0x3790FE0A
			   chs[3] = "ZY.FT"
		elseif ch ==  "大生_普通" then--
			   chs[1] = 0xBC6BD1E8
			   chs[2] = 0x8736E40D
			   chs[3] = "ZY.FT"
		elseif ch ==  "程夫人_普通" then--
			   chs[1] = 0xAF20E948
			   chs[2] = 0xA3F4919E
			   chs[3] = "ZY.FT"
		elseif ch ==  "道士_普通" then
			   chs[1] = 0xB7809CE0
			   chs[2] = 0x159CCC9F
			   chs[3] = "ZY.FT"--
		elseif ch ==  "道童_普通" then
			   chs[1] = 0xF4848781
			   chs[2] = 0x7376DB37
			   chs[3] = "ZY.FT"--
		elseif ch ==  "赌天霸_普通" then
			   chs[1] = 0xC888F0C3
			   chs[2] = 0x6A957B1B
			   chs[3] = "ZY.FT"--
		elseif ch ==  "二宝_普通" then
			   chs[1] = 0x9493C216
			   chs[2] = 0xF269B9B7
			   chs[3] = "ZY.FT"--
		elseif ch ==  "二大王_普通" then
			   chs[1] = 0x50355C48
			   chs[2] = 0x403A153E
			   chs[3] = "ZY.FT"--
		elseif ch ==  "服装店老板_普通" then
			   chs[1] = 0x2EB42770
			   chs[2] = 0x4C62D25F
			   chs[3] = "ZY.FT"--
		elseif ch ==  "福星_普通" then
			   chs[1] = 0x9E8FC397
			   chs[2] = 0xA25FFDB3
			   chs[3] = "ZY.FT"
			   chs[4] =0xCB71F060--
		elseif ch ==  "管家_普通" then
			   chs[1] = 0x3112FBE1
			   chs[2] = 0x0E34A5A3
			   chs[3] = "ZY.FT"--
		elseif ch ==  "老书生_普通" then--
			   chs[1] = 0xAD7A1BD3
			   chs[2] = 0xBB2ECF8E
			   chs[3] = "ZY.FT"
		elseif ch ==  "空度禅师_普通" then--
			   chs[1] = 0xB73531B1
			   chs[2] = 0x06EA6E77
			   chs[3] = "ZY.FT"
		elseif ch ==  "空善_普通" then--
			   chs[1] = 0x21EAE195
			   chs[2] = 0xA6169D14
			   chs[3] = "ZY.FT"
		elseif ch ==  "胖和尚_普通" then--
			   chs[1] = 0xE4926FC7
			   chs[2] = 0xBC6686FD
			   chs[3] = "ZY.FT"
		elseif ch ==  "瘦和尚_普通" then--
			   chs[1] = 0x6341E0D6
			   chs[2] = 0x177CDA70
			   chs[3] = "ZY.FT"
		elseif ch ==  "本事和尚_普通" then--
			   chs[1] = 0xD5A91213
			   chs[2] = 0x2AF8E37F
			   chs[3] = "ZY.FT"
		elseif ch ==  "赤脚大仙_普通" then--
			   chs[1] = 0xA831A782
			   chs[2] = 0xA7362DEF
			   chs[3] = "ZY.FT"
		elseif ch ==  "红娘_普通" then--
			   chs[1] = 0xFF25D8BA
			   chs[2] = 0x879921C3
			   chs[3] = "ZY.FT"
		elseif ch ==  "蒋大全_普通" then--
			   chs[1] = 0x65D0C53D
			   chs[2] = 0xD8300A1A
			   chs[3] = "ZY.FT"
		elseif ch ==  "酒店老板_普通" then--
			   chs[1] = 0x8783CF82
			   chs[2] = 0xAF2386A2
			   chs[3] = "ZY.FT"
		elseif ch ==  "巨子_普通" then--
			   chs[1] = 0x636012E7
			   chs[2] = 0x66FE03D3
			   chs[3] = "ZY.FT"
		elseif ch ==  "考官_普通" then--
			   chs[1] = 0x6ABF75AD
			   chs[2] = 0x8DDC27F7
			   chs[3] = "ZY.FT"
		elseif ch ==  "兰虎_普通" then--
			   chs[1] = 0x06F1DE7D
			   chs[2] = 0x37ADE1CA
			   chs[3] = "ZY.FT"
		elseif ch ==  "老花农_普通" then--
			   chs[1] = 0x5D14A411
			   chs[2] = 0x4E0B8DAC
			   chs[3] = "ZY.FT"
		elseif ch ==  "老马猴_普通" then--
			   chs[1] = 0xBDD0DC76
			   chs[2] = 0xC230BE81
			   chs[3] = "ZY.FT"
		elseif ch ==  "老太婆_普通" then--
			   chs[1] = 0xAFA62A72
			   chs[2] = 0xCB62E343
			   chs[3] = "ZY.FT"
		elseif ch ==  "老太爷_普通" then--
			   chs[1] = 0xFD419E2C
			   chs[2] = 0xF3B31502
			   chs[3] = "ZY.FT"
		elseif ch ==  "雷黑子_普通" then--
			   chs[1] = 0xDCF6A150
			   chs[2] = 0x7C3B4FB7
			   chs[3] = "ZY.FT"
		elseif ch ==  "李世民_普通" then--
			   chs[1] = 0xB28A3DB1
			   chs[2] = 0xBE8A3EC9
			   chs[3] = "ZY.FT"
		elseif ch ==  "王大嫂_普通" then--
			   chs[1] = 0x2C2259AB
			   chs[2] = 0xB0250DEE
			   chs[3] = "ZY.FT"
		elseif ch ==  "鲁成_普通" then--
			   chs[1] = 0x67C56BF0
			   chs[2] = 0x5B5B600B
			   chs[3] = "ZY.FT"
		elseif ch ==  "禄星_普通" then--
			   chs[1] = 0x0C490F81
			   chs[2] = 0xA67ACBC8
			   chs[3] = "ZY.FT"
			   chs[4] =0x589BBFFE
		elseif ch ==  "罗百万_普通" then--
			   chs[1] = 0x165AEB4C
			   chs[2] = 0x464E819A
			   chs[3] = "ZY.FT"
		elseif ch ==  "罗师爷_普通" then--
			   chs[1] = 0x165AEB4C
			   chs[2] = 0x521BFFA4
			   chs[3] = "ZY.FT"
		elseif ch ==  "马货商_普通" then--
			   chs[1] = 0xD6C941FC
			   chs[2] = 0x2094134A
			   chs[3] = "ZY.FT"
		elseif ch ==  "莫夫人_普通" then--
			   chs[1] = 0x234BC575
			   chs[2] = 0x36F5656A
			   chs[3] = "ZY.FT"
		elseif ch ==  "莫爷爷_普通" then--
			   chs[1] = 0xDE7DA687
			   chs[2] = 0x1210A82F
			   chs[3] = "ZY.FT"
		elseif ch ==  "铁匠_普通" then--
			   chs[1] = 0x75BB33D2
			   chs[2] = 0xE0942BC2
			   chs[3] = "ZY.FT"
		elseif ch ==  "宁小倩_普通" then--
			   chs[1] = 0x53D7FFCA
			   chs[2] = 0x241D8F39
			   chs[3] = "ZY.FT"
		elseif ch ==  "扇子女_普通" then--
			   chs[1] = 0x6282B5AB
			   chs[2] = 0x35F4E996
			   chs[3] = "ZY.FT"
		elseif ch ==  "白晶晶_普通" then--
			   chs[1] = 0xF5D47E2E
			   chs[2] = 0x9957A256
			   chs[3] = "ZY.FT"
		elseif ch ==  "春十三娘_普通" then--
			   chs[1] = 0x2E305A78
			   chs[2] = 0x61A8F3FF
			   chs[3] = "ZY.FT"
		elseif ch ==  "判官_普通" then--
			   chs[1] = 0x56738B45
			   chs[2] = 0x6617CE07
			   chs[3] = "ZY.FT"
		elseif ch ==  "钱庄老板_普通" then--
			   chs[1] = 0xDA498AD2
			   chs[2] = 0xBB95D714
			   chs[3] = "ZY.FT"
		elseif ch ==  "秦琼_普通" then--
			   chs[1] = 0x4D41D5D2
			   chs[2] = 0x8A05EDF0
			   chs[3] = "ZY.FT"
		elseif ch ==  "穷汉_普通" then--
			   chs[1] = 0xBF882D3F
			   chs[2] = 0x97CDC297
			   chs[3] = "ZY.FT"
		elseif ch ==  "三大王_普通" then--
			   chs[1] = 0x228D340F
			   chs[2] = 0x2059CBB0
			   chs[3] = "ZY.FT"
		elseif ch ==  "商会总管_普通" then
			   chs[1] = 0x0BB9C549
			   chs[2] = 0x703420B3
			   chs[3] = "ZY.FT"--
		elseif ch ==  "上官丞相_普通" then--
			   chs[1] = 0x40D087F9
			   chs[2] = 0xD104A2CD
			   chs[3] = "ZY.FT"
		elseif ch ==  "少女_普通" then--
			   chs[1] = 0x1BB1AF36
			   chs[2] = 0xA69EAB9D
			   chs[3] = "ZY.FT"
		elseif ch ==  "书生_普通" then--
			   chs[1] = 0xAE82050D
			   chs[2] = 0x8DB0C74C
			   chs[3] = "ZY.FT"
		elseif ch ==  "书童_普通" then--
			   chs[1] = 0x015B95C2
			   chs[2] = 0xF771C268
			   chs[3] = "ZY.FT"
		elseif ch ==  "飞儿_普通" then--
			   chs[1] = 0x7555A8D0
			   chs[2] = 0x4468668F
			   chs[3] = "ZY.FT"
		elseif ch ==  "唐僧_普通" then--
			   chs[1] = 0xF707EA37
			   chs[2] = 0xFF76393B
			   chs[3] = "ZY.FT"
		elseif ch ==  "特产商人_普通" then--
			   chs[1] = 0x64E5C547
			   chs[2] = 0xBE35EE06
			   chs[3] = "ZY.FT"
		elseif ch ==  "哪吒_普通" then--
			   chs[1] = 0x5903D418
			   chs[2] = 0x259EC5A7
			   chs[3] = "ZY.FT"
		elseif ch ==  "南极仙翁_普通" then--
			   chs[1] = 0xB4DA85C7
			   chs[2] = 0xBE41583A
			   chs[3] = "ZY.FT"
		elseif ch ==  "诗中仙_普通" then--
			   chs[1] = 0x41B1D8EC
			   chs[2] = 0x5FD74EE1
			   chs[3] = "ZY.FT"
		elseif ch ==  "太白金星_普通" then--
			   chs[1] = 0xCA3334FF
			   chs[2] = 0x5745E837
			   chs[3] = "ZY.FT"
		elseif ch ==  "太上老君_普通" then--
			   chs[1] = 0x3599D5C3
			   chs[2] = 0x16505471
			   chs[3] = "ZY.FT"
		elseif ch ==  "守门天将_普通" then--
			   chs[1] = 0x35F5C343
			   chs[2] = 0x028B91D6
			   chs[3] = "ZY.FT"
		elseif ch ==  "王母_普通" then--
			   chs[1] = 0x248A3E4C
			   chs[2] = 0x2B2CC199
			   chs[3] = "ZY.FT"
		elseif ch ==  "小花_普通" then--
			   chs[1] = 0x7792B6DA
			   chs[2] = 0xDB11C815
			   chs[3] = "ZY.FT"
		elseif ch ==  "巡游天官_普通" then--
			   chs[1] = 0x2032D0C0
			   chs[2] = 0x56128DA7
			   chs[3] = "ZY.FT"
		elseif ch ==  "渡劫神天兵_普通" then--
			   chs[1] = 0x1D8F9ABB
			   chs[2] = 0x04103E1D
			   chs[3] = "ZY.FT"
		elseif ch ==  "游戏管理员_普通" then--
			   chs[1] = 0x474B1237
			   chs[2] = 0xC33B0F39
			   chs[3] = "ZY.FT"
		elseif ch ==  "玉帝_普通" then--
			   chs[1] = 0xEB00DE5F
			   chs[2] = 0x3C7DB1FD
			   chs[3] = "ZY.FT"
		elseif ch ==  "嫦娥_普通" then--
			   chs[1] = 0x93A1B0A9
			   chs[2] = 0xEC91EDA2
			   chs[3] = "ZY.FT"
		elseif ch ==  "长安铁匠_普通" then--
			   chs[1] = 0x0734C9CA
			   chs[2] = 0xB4DB94F0
			   chs[3] = "ZY.FT"
		elseif ch ==  "土地_普通" then--
			   chs[1] = 0x81895D83
			   chs[2] = 0x6E805BF9
			   chs[3] = "ZY.FT"
		elseif ch ==  "万圣公主_普通" then--
			   chs[1] = 0xC82770E2
			   chs[2] = 0x3101E857
			   chs[3] = "ZY.FT"
		elseif ch ==  "文老伯_普通" then--
			   chs[1] = 0x4ED41E93
			   chs[2] = 0x3DD602B8
			   chs[3] = "ZY.FT"
		elseif ch ==  "乌鸡公主_普通" then--
			   chs[1] = 0x2BB71D40
			   chs[2] = 0xF21DAB4A
			   chs[3] = "ZY.FT"
			   chs[4] =0x708A094F
		elseif ch ==  "乌鸡国王_普通" then
			   chs[1] = 0x31684231
			   chs[2] = 0x23021644
			   chs[3] = "ZY.FT"
			   chs[4] =0x7DD7F706--
		elseif ch ==  "白无常_普通" then
			   chs[1] = 0x67B85348
			   chs[2] = 0x837D7776
			   chs[3] = "ZY.FT"--
		elseif ch ==  "黑无常_普通" then
			   chs[1] = 0xB181318C
			   chs[2] = 0xF250443E
			   chs[3] = "ZY.FT"--
		elseif ch ==  "武器店老板_普通" then--
			   chs[1] = 0x732407B7
			   chs[2] = 0x44801CB6
			   chs[3] = "ZY.FT"
		elseif ch ==  "西梁国王_普通" then
			   chs[1] = 0x3EA2FA4D
			   chs[2] = 0x12DAD117
			   chs[3] = "ZY.FT"--
		elseif ch ==  "西门将军_普通" then
			   chs[1] = 0xE7D72D11
			   chs[2] = 0x6E8CD009
			   chs[3] = "ZY.FT"--
		elseif ch ==  "小白龙_普通" then
			   chs[1] = 0xCC61903E
			   chs[2] = 0x29DCD4EF
			   chs[3] = "ZY.FT"--
		elseif ch ==  "小笛_普通" then
			   chs[1] = 0x54D687BC
			   chs[2] = 0x3EAA6EC8
			   chs[3] = "ZY.FT"--
		elseif ch ==  "小二_普通" then
			   chs[1] = 0x8F5E39E1
			   chs[2] = 0xDD211906
			   chs[3] = "ZY.FT"--
		elseif ch ==  "衙役_普通" then
			   chs[1] = 0x40B1BCC3
			   chs[2] = 0x8AC693A9
			   chs[3] = "ZY.FT"--
		elseif ch ==  "药店老板_普通" then
			   chs[1] = 0xE0DC6D93
			   chs[2] = 0xC3627242
			   chs[3] = "ZY.FT"--
		elseif ch ==  "尹孝乾坤_普通" then
			   chs[1] = 0x5641D9BE
			   chs[2] = 0x9D836B06
			   chs[3] = "ZY.FT"--
		elseif ch ==  "佣人_普通" then
			   chs[1] = 0xD0A9859B
			   chs[2] = 0xC5E98CA8
			   chs[3] = "ZY.FT"--
		elseif ch ==  "御林军_普通" then
			   chs[1] = 0x6843A912
			   chs[2] = 0x4EE87EE9
			   chs[3] = "ZY.FT"--
		elseif ch ==  "月老_普通" then
			   chs[1] = 0xA33DB160
			   chs[2] = 0x20A19816
			   chs[3] = "ZY.FT"--
		elseif ch ==  "张老财_普通" then
			   chs[1] = 0xA9F796EA
			   chs[2] = 0xF637E75A
			   chs[3] = "ZY.FT"--
		elseif ch ==  "赵姨娘_普通" then
			   chs[1] = 0xD97E30F0
			   chs[2] = 0x6FBF6A9F
			   chs[3] = "ZY.FT"--
		elseif ch ==  "珍品商人_普通" then
			   chs[1] = 0xD7A74DE2
			   chs[2] = 0xF846C711
			   chs[3] = "ZY.FT"--
		elseif ch ==  "镇塔之神_普通" then
			   chs[1] = 0x589A2894
			   chs[2] = 0x7ECE7DB2
			   chs[3] = "ZY.FT"--
		elseif ch ==  "郑镖头_普通" then
			   chs[1] = 0xD02A9BB5
			   chs[2] = 0x4D0AAF3C
			   chs[3] = "ZY.FT"--
		elseif ch ==  "至尊宝_普通" then
			   chs[1] = 0xEA27AD31
			   chs[2] = 0xFB629346
			   chs[3] = "ZY.FT"--
		elseif ch ==  "钟馗_普通" then
			   chs[1] = 0xF1E86993
			   chs[2] = 0x39A1E40F
			   chs[3] = "ZY.FT"--
		elseif ch ==  "装备收购商_普通" then--
			   chs[1] = 0x1AA01951
			   chs[2] = 0x5DFA3BB9
			   chs[3] = "ZY.FT"
		elseif ch ==  "驿站老板_普通" then--
			   chs[1] = 0x72EEB38D
			   chs[2] = 0x6F6C1A7C
			   chs[3] = "ZY.FT"
		elseif ch ==  "灵鼠娃娃_普通" then--
			   chs[1] = 0x738F33AF
			   chs[2] = 0x9A5FA693
			   chs[3] = "ZY.FT"
		elseif ch ==  "将军_普通" then--
			   chs[1] = 0x1D232BFA
			   chs[2] = 0x5DBA1EE4
			   chs[3] = "ZY.FT"
		elseif ch ==  "云小奴_普通" then--
			   chs[1] = 0x18791291
			   chs[2] = 0x90065904
			   chs[3] = "ZY.FT"
		elseif ch ==  "巫奎虎_普通" then--
			   chs[1] = 0x062150F6
			   chs[2] = 0x922CB5F2
			   chs[3] = "ZY.FT"
		elseif ch ==  "神龙_普通" then--
			   chs[1] = 0x6F71376B
			   chs[2] = 0xEBF15AEB
			   chs[3] = "ZY.FT"
		elseif ch ==  "酒鬼_普通" then--
			   chs[1] = 0xF98D2A1B
			   chs[2] = 0xBE860ACE
			   chs[3] = "ZY.FT"
		elseif ch ==  "美女_普通" then--
			   chs[1] = 0x6D96A509
			   chs[2] = 0x1A8279CA
			   chs[3] = "ZY.FT"
		elseif ch ==  "猪八戒_普通" then--
			   chs[1] = 0x046ADE41
			   chs[2] = 0x85C12349
			   chs[3] = "ZHS.FT"
		elseif ch ==  "沙僧_普通" then--
			   chs[1] = 0x9E8754B2
			   chs[2] = 0xF58104E2
			   chs[3] = "ZHS.FT"
		elseif ch =="阎罗王_普通"then
			    chs[1]= 0xA5B436EE
			    chs[2]= 0x558B14D5
			    chs[3] = "ZHS.FT"
		elseif ch =="牛魔王_普通"then
				chs[1]= 0x0B69B44D
				chs[2]= 0xFBB7A2A2
				chs[3] = "ZHS.FT"
		elseif ch =="巫师_普通"then
				chs[1]= 0xB692F3E2
				chs[2]= 0xD569F35B
				chs[3] = "ZHS.FT"
		elseif ch =="地涌夫人_普通"then
				chs[1]= 0x2EDEAF19
				chs[2]= 0xA69241A0
				chs[3] = "ZHS.FT"
	---------------------------------------
		elseif ch == "竹节双剑_飞燕女_普通" then
		chs[1] = 2478979274
		chs[2] = 683536595
		chs[3] = "ZY.FT"
		elseif ch == "无极丝_玄彩娥_普通" then
			chs[1] = 1086469645
			chs[2] = 2643257844
			chs[3] = "ZY.FT"
		elseif ch == "羽灵神_法杖" then
			chs[1] = 2581796502
			chs[2] = 3940257408
			chs[3] = "ZY.FT"
		elseif ch == "狼牙锤_虎头怪_普通" then
			chs[1] = 900073506
			chs[2] = 556692511
			chs[3] = "ZY.FT"
		elseif ch == "腾云杖_巫蛮儿_普通" then
			chs[1] = 3237314719
			chs[2] = 1747492117
			chs[3] = "ZY.FT"
		elseif ch == "逍遥生_普通" then
			chs[1] = 1492865095
			chs[2] = 304945535
			chs[3] = "ZY.FT"
		elseif ch == "柳叶刀_剑侠客_普通" then
			chs[1] = 1229743190
			chs[2] = 194354478
			chs[3] = "ZY.FT"
		elseif ch == "游龙剑_剑侠客_普通" then
			chs[1] = 4239696014
			chs[2] = 1495300408
			chs[3] = "ZY.FT"
		elseif ch == "缝纫台_普通" then
			chs[1] = 837447295
			chs[2] = 837447295
			chs[3] = "JM.FT"
		elseif ch == "神树_普通" then
			chs[1] = 0xF747E46
			chs[2] = 0xF747E46
			chs[3] = "JM.FT"
		elseif ch == "青铜短剑_逍遥生_普通" then
			chs[1] = 614590386
			chs[2] = 2214774214
			chs[3] = "ZY.FT"
		elseif ch == "飞燕女_普通" then
			chs[1] = 2708080383
			chs[2] = 1337608376
			chs[3] = "ZY.FT"
		elseif ch == "七彩罗刹_玄彩娥_普通" then
			chs[1] = 2992307123
			chs[2] = 3171287220
			chs[3] = "ZY.FT"
		elseif ch == "神火扇_龙太子_普通" then
			chs[1] = 3998986408
			chs[2] = 207256533
			chs[3] = "ZY.FT"
		elseif ch == "青铜斧_虎头怪_普通" then
			chs[1] = 1495520707
			chs[2] = 1952969514
			chs[3] = "ZY.FT"
		elseif ch == "青龙牙_骨精灵_普通" then
			chs[1] = 819272546
			chs[2] = 4143052600
			chs[3] = "ZY.FT"
		elseif ch == "如意宝珠_杀破狼_普通" then
			chs[1] = 3027466503
			chs[2] = 3046998488
			chs[3] = "ZY.FT"
		elseif ch == "二郎神_普通" then
			chs[1] = 526337028
			chs[2] = 4168999919
			chs[3] = "ZY.FT"
		elseif ch == "二郎神_坐_普通" then
			chs[1] = 0xCF6966D6
			chs[2] = 4168999919
			chs[3] = "ZY.FT"
		elseif ch == "翡翠珠_巫蛮儿_普通" then
			chs[1] = 1191800816
			chs[2] = 1331598936
			chs[3] = "ZY.FT"
		elseif ch == "点金棒_骨精灵_普通" then
			chs[1] = 2615197180
			chs[2] = 3429205695
			chs[3] = "ZY.FT"
		elseif ch == "鸣鸿_巨魔王_普通" then
			chs[1] = 2272535883
			chs[2] = 4183963940
			chs[3] = "ZY.FT"
		elseif ch == "牛皮鞭_英女侠_普通" then
			chs[1] = 2721039077
			chs[2] = 4092691055
			chs[3] = "ZY.FT"
		elseif ch == "钢结鞭_狐美人_普通" then
			chs[1] = 1358766658
			chs[2] = 219014606
			chs[3] = "ZY.FT"
		elseif ch == "折扇_逍遥生_普通" then
			chs[1] = 2120322561
			chs[2] = 4052148846
			chs[3] = "ZY.FT"
		elseif ch == "裂天_虎头怪_普通" then
			chs[1] = 161677133
			chs[2] = 2342305231
			chs[3] = "ZY.FT"
		elseif ch == "青刚刺_狐美人_普通" then
			chs[1] = 943875548
			chs[2] = 2075432907
			chs[3] = "ZY.FT"
		elseif ch == "金背大砍刀_剑侠客_普通" then
			chs[1] = 3102182178
			chs[2] = 1238352835
			chs[3] = "ZY.FT"
		elseif ch == "腾云杖_羽灵神_普通" then
			chs[1] = 1584570480
			chs[2] = 1046023851
			chs[3] = "ZY.FT"
		elseif ch == "龙太子_普通" then
			chs[1] = 2944637360
			chs[2] = 4098637790
			chs[3] = "ZY.FT"
		elseif ch == "弑皇_龙太子_普通" then
			chs[1] = 2305995091
			chs[2] = 1956552460
			chs[3] = "ZY.FT"
		elseif ch == "双弦钺_巨魔王_普通" then
			chs[1] = 4111880178
			chs[2] = 2679811341
			chs[3] = "ZY.FT"
		elseif ch == "擒龙_剑侠客_普通" then
			chs[1] = 3544836379
			chs[2] = 1980184881
			chs[3] = "ZY.FT"
		elseif ch == "狼牙锤_神天兵_普通" then
			chs[1] = 952314130
			chs[2] = 1577638509
			chs[3] = "ZY.FT"
		elseif ch == "红缨枪_神天兵_普通" then
			chs[1] = 3564813918
			chs[2] = 2236546380
			chs[3] = "ZY.FT"
		elseif ch == "青锋剑_逍遥生_普通" then
			chs[1] = 540655754
			chs[2] = 526326278
			chs[3] = "ZY.FT"
		elseif ch == "如意宝珠_巫蛮儿_普通" then
			chs[1] = 2417421936
			chs[2] = 2197534441
			chs[3] = "ZY.FT"
		elseif ch == "翡翠珠_杀破狼_普通" then
			chs[1] = 1314894172
			chs[2] = 433513441
			chs[3] = "ZY.FT"
		elseif ch == "狐美人_普通" then
			chs[1] = 3753105193
			chs[2] = 3988533543
			chs[3] = "ZY.FT"
		elseif ch == "满天星_骨精灵_普通" then
			chs[1] = 412146092
			chs[2] = 59782698
			chs[3] = "ZY.FT"
		elseif ch == "骨精灵_普通" then
			chs[1] = 812821174
			chs[2] = 493656179
			chs[3] = "ZY.FT"
		elseif ch == "琉璃珠_巫蛮儿_普通" then
			chs[1] = 4171731900
			chs[2] = 143410001
			chs[3] = "ZY.FT"
		elseif ch == "硬木弓_杀破狼_普通" then
			chs[1] = 2500153518
			chs[2] = 34690017
			chs[3] = "ZY.FT"
		elseif ch == "宝雕长弓_杀破狼_普通" then
			chs[1] = 3168212514
			chs[2] = 1524411866
			chs[3] = "ZY.FT"
		elseif ch == "玄铁矛_龙太子_普通" then
			chs[1] = 2397764102
			chs[2] = 2300479837
			chs[3] = "ZY.FT"
		elseif ch == "巨魔王_普通" then
			chs[1] = 2213567130
			chs[2] = 1051618199
			chs[3] = "ZY.FT"
		elseif ch == "青藤柳叶鞭_狐美人_普通" then
			chs[1] = 2017171936
			chs[2] = 1015863531
			chs[3] = "ZY.FT"
		elseif ch == "宝雕长弓_羽灵神_普通" then
			chs[1] = 487656505
			chs[2] = 162322797
			chs[3] = "ZY.FT"
		elseif ch == "连珠神弓_羽灵神_普通" then
			chs[1] = 3702678012
			chs[2] = 2163189324
			chs[3] = "ZY.FT"
		elseif ch == "硬木弓_羽灵神_普通" then
			chs[1] = 3985633058
			chs[2] = 2192045984
			chs[3] = "ZY.FT"
		elseif ch == "打铁炉_普通" then
			chs[1] = 2074974822
			chs[2] = 2074974822
			chs[3] = "JM.FT"
		elseif ch == "震天锤_神天兵_普通" then
			chs[1] = 3106536422
			chs[2] = 4262815425
			chs[3] = "ZY.FT"
		elseif ch == "墨铁拐_巫蛮儿_普通" then
			chs[1] = 2710601924
			chs[2] = 495554266
			chs[3] = "ZY.FT"
		elseif ch == "龟丞相_普通" then
			chs[1] = 1371671296
			chs[2] = 1404781116
			chs[3] = "ZY.FT"
		elseif ch == "曲柳杖_巫蛮儿_普通" then
			chs[1] = 438684246
			chs[2] = 2418364461
			chs[3] = "ZY.FT"
		elseif ch == "狼牙刀_巨魔王_普通" then
			chs[1] = 1185865684
			chs[2] = 2820957694
			chs[3] = "ZY.FT"
		elseif ch == "赤明_杀破狼_宝珠_普通" then
			chs[1] = 3482219011
			chs[2] = 3906907508
			chs[3] = "ZY.FT"
		elseif ch == "细木棒_骨精灵_普通" then
			chs[1] = 1202797055
			chs[2] = 4178020883
			chs[3] = "ZY.FT"
		elseif ch == "星瀚_逍遥生_普通" then
			chs[1] = 791015559
			chs[2] = 3682713078
			chs[3] = "ZY.FT"
		elseif ch == "柳叶刀_巨魔王_普通" then
			chs[1] = 4115620300
			chs[2] = 3330022961
			chs[3] = "ZY.FT"
		elseif ch == "蛇形月_舞天姬_普通" then
			chs[1] = 1644759402
			chs[2] = 1759014159
			chs[3] = "ZY.FT"
		elseif ch == "蛇形月_飞燕女_普通" then
			chs[1] = 3586173418
			chs[2] = 732948875
			chs[3] = "ZY.FT"
		elseif ch == "金刺轮_舞天姬_普通" then
			chs[1] = 2807451206
			chs[2] = 138314297
			chs[3] = "ZY.FT"
		elseif ch == "墨铁拐_羽灵神_普通" then
			chs[1] = 2848235637
			chs[2] = 2266600687
			chs[3] = "ZY.FT"
		elseif ch == "黄铜圈_舞天姬_普通" then
			chs[1] = 194575710
			chs[2] = 2421095781
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼_宝珠" then
			chs[1] = 2930414626
			chs[2] = 4291811931
			chs[3] = "ZY.FT"
		elseif ch == "玄彩娥_普通" then
			chs[1] = 1362263605
			chs[2] = 3715462681
			chs[3] = "ZY.FT"
		elseif ch == "黄铜圈_飞燕女_普通" then
			chs[1] = 737601414
			chs[2] = 1669143734
			chs[3] = "ZY.FT"
		elseif ch == "双短剑_英女侠_普通" then
			chs[1] = 2246741733
			chs[2] = 2201236800
			chs[3] = "ZY.FT"
		elseif ch == "青藤柳叶鞭_英女侠_普通" then
			chs[1] = 3009625218
			chs[2] = 2830037944
			chs[3] = "ZY.FT"
		elseif ch == "钢结鞭_英女侠_普通" then
			chs[1] = 1003144235
			chs[2] = 1524918418
			chs[3] = "ZY.FT"
		elseif ch == "羽灵神_弓弩" then
			chs[1] = 2581796502
			chs[2] = 1858805344
			chs[3] = "ZY.FT"
		elseif ch == "羽灵神_弓弩1" then
			chs[1] = 0262638661
			chs[2] = 3940257408
			chs[3] = "ZY.FT"
		elseif ch == "震天锤_虎头怪_普通" then
			chs[1] = 676548466
			chs[2] = 1816923096
			chs[3] = "ZY.FT"
		elseif ch == "剑侠客_普通" then
			chs[1] = 1228435406
			chs[2] = 1425276052
			chs[3] = "ZY.FT"
		elseif ch == "金刺轮_飞燕女_普通" then
			chs[1] = 860305472
			chs[2] = 4154736570
			chs[3] = "ZY.FT"
		elseif ch == "狼牙刀_剑侠客_普通" then
			chs[1] = 213847515
			chs[2] = 3147078622
			chs[3] = "ZY.FT"
		elseif ch == "青龙牙_狐美人_普通" then
			chs[1] = 3750219087
			chs[2] = 346973613
			chs[3] = "ZY.FT"
		elseif ch == "赤焰双剑_英女侠_普通" then
			chs[1] = 1427570181
			chs[2] = 3478724046
			chs[3] = "ZY.FT"
		elseif ch == "满天星_玄彩娥_普通" then
			chs[1] = 1121013667
			chs[2] = 291147333
			chs[3] = "ZY.FT"
		elseif ch == "点金棒_玄彩娥_普通" then
			chs[1] = 1581306371
			chs[2] = 1230293004
			chs[3] = "ZY.FT"
		elseif ch == "金背大砍刀_巨魔王_普通" then
			chs[1] = 2395375353
			chs[2] = 3599969440
			chs[3] = "ZY.FT"
		elseif ch == "巫蛮儿_宝珠" then
			chs[1] = 202554088
			chs[2] = 255156094
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼_普通" then
			chs[1] = 652210495
			chs[2] = 1032044905
			chs[3] = "ZY.FT"
		elseif ch == "神火扇_逍遥生_普通" then
			chs[1] = 1076608023
			chs[2] = 2463492889
			chs[3] = "ZY.FT"
		elseif ch == "铁面扇_龙太子_普通" then
			chs[1] = 506375536
			chs[2] = 4142026495
			chs[3] = "ZY.FT"
		elseif ch == "英女侠_普通" then
			chs[1] = 2898894124
			chs[2] = 3708489082
			chs[3] = "ZY.FT"
		elseif ch == "折扇_龙太子_普通" then
			chs[1] = 506375536
			chs[2] = 4142026495
			chs[3] = "ZY.FT"
		elseif ch == "青刚刺_骨精灵_普通" then
			chs[1] = 4186507615
			chs[2] = 2094848573
			chs[3] = "ZY.FT"
		elseif ch == "松木锤_神天兵_普通" then
			chs[1] = 385167718
			chs[2] = 887944587
			chs[3] = "ZY.FT"
		elseif ch == "巫蛮儿_法杖" then
			chs[1] = 202554088
			chs[2] = 4185651500
			chs[3] = "ZY.FT"
		elseif ch == "铁爪_骨精灵_普通" then
			chs[1] = 980144775
			chs[2] = 630615359
			chs[3] = "ZY.FT"
		elseif ch == "铁爪_狐美人_普通" then
			chs[1] = 3977087148
			chs[2] = 1562072464
			chs[3] = "ZY.FT"
		elseif ch == "七彩罗刹_舞天姬_普通" then
			chs[1] = 903786802
			chs[2] = 2633357816
			chs[3] = "ZY.FT"
		elseif ch == "无极丝_舞天姬_普通" then
			chs[1] = 4219745722
			chs[2] = 3471825039
			chs[3] = "ZY.FT"
		elseif ch == "五色缎带_玄彩娥_普通" then
			chs[1] = 4201646940
			chs[2] = 2449560215
			chs[3] = "ZY.FT"
		elseif ch == "五色缎带_舞天姬_普通" then
			chs[1] = 3852826693
			chs[2] = 1284868321
			chs[3] = "ZY.FT"
		elseif ch == "羽灵神_普通" then
			chs[1] = 2581796502
			chs[2] = 1858805344
			chs[3] = "ZY.FT"
		elseif ch == "琉璃珠_杀破狼_普通" then
			chs[1] = 2754911857
			chs[2] = 2888668416
			chs[3] = "ZY.FT"
		elseif ch == "竹节双剑_英女侠_普通" then
			chs[1] = 1476049499
			chs[2] = 2441717360
			chs[3] = "ZY.FT"
		elseif ch == "双短剑_飞燕女_普通" then
			chs[1] = 932337963
			chs[2] = 1862513649
			chs[3] = "ZY.FT"
		elseif ch == "连珠神弓_杀破狼_普通" then
			chs[1] = 824051161
			chs[2] = 3255758268
			chs[3] = "ZY.FT"
		elseif ch == "青铜短剑_剑侠客_普通" then
			chs[1] = 3428752505
			chs[2] = 696309892
			chs[3] = "ZY.FT"
		elseif ch == "乌金三叉戟_龙太子_普通" then
			chs[1] = 1709314112
			chs[2] = 50056701
			chs[3] = "ZY.FT"
		elseif ch == "青锋剑_剑侠客_普通" then
			chs[1] = 2459468380
			chs[2] = 702488915
			chs[3] = "ZY.FT"
		elseif ch == "双弦钺_虎头怪_普通" then
			chs[1] = 1464696308
			chs[2] = 1468494251
			chs[3] = "ZY.FT"
		elseif ch == "玄铁矛_神天兵_普通" then
			chs[1] = 491583081
			chs[2] = 2008609870
			chs[3] = "ZY.FT"
		elseif ch == "乌金三叉戟_神天兵_普通" then
			chs[1] = 733304931
			chs[2] = 235606285
			chs[3] = "ZY.FT"
		elseif ch == "乌金鬼头镰_巨魔王_普通" then
			chs[1] = 1147867834
			chs[2] = 3049186137
			chs[3] = "ZY.FT"
		elseif ch == "红缨枪_龙太子_普通" then
			chs[1] = 2274614746
			chs[2] = 1825813896
			chs[3] = "ZY.FT"
		elseif ch == "浮犀_飞燕女_普通" then
			chs[1] = 3366765078
			chs[2] = 3831481111
			chs[3] = "ZY.FT"
		elseif ch == "牛皮鞭_狐美人_普通" then
			chs[1] = 476936950
			chs[2] = 387504165
			chs[3] = "ZY.FT"
		elseif ch == "青铜斧_巨魔王_普通" then
			chs[1] = 550609107
			chs[2] = 1015351627
			chs[3] = "ZY.FT"
		elseif ch == "GM_普通" then
			chs[1] = 1196102199
			chs[2] = 3275427641
			chs[3] = "ZY.FT"
		elseif ch == "细木棒_玄彩娥_普通" then
			chs[1] = 534278293
			chs[2] = 4175187443
			chs[3] = "ZY.FT"
		elseif ch == "神天兵_普通" then
			chs[1] = 2115558165
			chs[2] = 1433167185
			chs[3] = "ZY.FT"
		elseif ch == "松木锤_虎头怪_普通" then
			chs[1] = 1728983487
			chs[2] = 2655200756
			chs[3] = "ZY.FT"
		elseif ch == "普陀_接引仙女_普通" then
			chs[1] = 2006103770
			chs[2] = 3675375637
			chs[3] = "ZY.FT"
		elseif ch == "曲柳杖_羽灵神_普通" then
			chs[1] = 1868152845
			chs[2] = 460325915
			chs[3] = "ZY.FT"
		elseif ch == "舞天姬_普通" then
			chs[1] = 1050893021
			chs[2] = 3778545729
			chs[3] = "ZY.FT"
		elseif ch == "铁面扇_逍遥生_普通" then
			chs[1] = 696084557
			chs[2] = 2358358928
			chs[3] = "ZY.FT"
		elseif ch == "赤焰双剑_飞燕女_普通" then
			chs[1] = 4061318075
			chs[2] = 2660522564
			chs[3] = "ZY.FT"
		elseif ch == "游龙剑_逍遥生_普通" then
			chs[1] = 1313817198
			chs[2] = 3892924667
			chs[3] = "ZY.FT"
		elseif ch == "巫蛮儿_普通" then
			chs[1] = 1602126433
			chs[2] = 4185651500
			chs[3] = "ZY.FT"
		elseif ch == "乌金鬼头镰_虎头怪_普通" then
			chs[1] = 3940375773
			chs[2] = 3876300955
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼_宝珠_普通" then
			chs[1] = 2930414626
			chs[2] = 4291811931
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼_弓弩" then
			chs[1] = 0652210495
			chs[2] = 1032044905
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼_弓弩1" then
			chs[1] = 0652210495
			chs[2] = 4291811931
			chs[3] = "ZY.FT"
		elseif ch == "虎头怪_普通" then
			chs[1] = 571142035
			chs[2] = 803245291
			chs[3] = "ZY.FT"
		-- v1.3新增
		elseif ch == "暗夜_龙太子_普通" then
			chs[1] = 0906768777
			chs[2] = 0402972999
			chs[3] = "ZY.FT"
		elseif ch == "暗夜_神天兵_普通" then
			chs[1] = 3587304792
			chs[2] = 3537373418
			chs[3] = "ZY.FT"
		elseif ch == "梨花_龙太子_普通" then
			chs[1] = 3562006868
			chs[2] = 0563824081
			chs[3] = "ZY.FT"
		elseif ch == "梨花_神天兵_普通" then
			chs[1] = 3155530506
			chs[2] = 4149381636
			chs[3] = "ZY.FT"
		elseif ch == "霹雳_龙太子_普通" then
			chs[1] = 0663241555
			chs[2] = 0882577208
			chs[3] = "ZY.FT"
		elseif ch == "霹雳_神天兵_普通" then
			chs[1] = 3690872798
			chs[2] = 3942970730
			chs[3] = "ZY.FT"
		elseif ch == "刑天之逆_龙太子_普通" then
			chs[1] = 0x31CA69F0
			chs[2] = 0x898AF13F
			chs[3] = "ZY.FT"
		elseif ch == "五虎断魂_龙太子_普通" then
			chs[1] = 0x929158FC
			chs[2] = 0x7A0C3835
			chs[3] = "ZY.FT"
		elseif ch == "飞龙在天_龙太子_普通" then
			chs[1] = 0x15E284F9
			chs[2] = 0x0A73B2E0
			chs[3] = "ZY.FT"
		elseif ch == "天龙破城_龙太子_普通" then
			chs[1] = 1524391868
			chs[2] = 3805099760
			chs[3] = "ZY.FT"
		elseif ch == "刑天之逆_神天兵_普通" then
			chs[1] = 0xDBFE3FDE
			chs[2] = 0xEB04F56A
			chs[3] = "ZY.FT"
		elseif ch == "五虎断魂_神天兵_普通" then
			chs[1] = 0xEF75190E
			chs[2] = 0x7A0C3835
			chs[3] = "ZY.FT"
		elseif ch == "飞龙在天_神天兵_普通" then
			chs[1] = 0x37F26C44
			chs[2] = 0xD751042A
			chs[3] = "ZY.FT"
		elseif ch == "天龙破城_神天兵_普通" then
			chs[1] = 3154146622
			chs[2] = 2015118453
			chs[3] = "ZY.FT"
		elseif ch == "弑皇_神天兵_普通" then
			chs[1] = 1837680668
			chs[2] = 0689112756
			chs[3] = "ZY.FT"
		elseif ch == "破魄_虎头怪_普通" then
			chs[1] = 0x53F914DE
			chs[2] = 0x8102D783
			chs[3] = "ZY.FT"
		elseif ch == "破魄_巨魔王_普通" then
			chs[1] = 0x02CECBCB
			chs[2] = 0xB8554947
			chs[3] = "ZY.FT"
		elseif ch == "肃魂_虎头怪_普通" then
			chs[1] = 0xE8866C8F
			chs[2] = 0x16B86A5E
			chs[3] = "ZY.FT"
		elseif ch == "肃魂_巨魔王_普通" then
			chs[1] = 0x726F7762
			chs[2] = 0x199CF19E
			chs[3] = "ZY.FT"
		elseif ch == "无敌_虎头怪_普通" then
			chs[1] = 0xD88565BC
			chs[2] = 0x375756CE
			chs[3] = "ZY.FT"
		elseif ch == "无敌_巨魔王_普通" then
			chs[1] = 0x6AE68E94
			chs[2] = 0x29FE3B3E
			chs[3] = "ZY.FT"
		elseif ch == "五丁开山_巨魔王_普通" then
			chs[1] = 0xDDE32A30
			chs[2] = 0x7D02D5DB
			chs[3] = "ZY.FT"
		elseif ch == "元神禁锢_巨魔王_普通" then
			chs[1] = 0x6693BB73
			chs[2] = 0x4E32D0BC
			chs[3] = "ZY.FT"
		elseif ch == "护法灭魔_巨魔王_普通" then
			chs[1] = 0x75E45C94
			chs[2] = 0xDA6126C5
			chs[3] = "ZY.FT"
		elseif ch == "碧血干戚_巨魔王_普通" then
			chs[1] = 1651186704
			chs[2] = 1048057784
			chs[3] = "ZY.FT"
		elseif ch == "裂天_巨魔王_普通" then
			chs[1] = 3160261512
			chs[2] = 0098835428
			chs[3] = "ZY.FT"
		elseif ch == "五丁开山_虎头怪_普通" then
			chs[1] = 0x2261BF73
			chs[2] = 0xB02E0452
			chs[3] = "ZY.FT"
		elseif ch == "元神禁锢_虎头怪_普通" then
			chs[1] = 0xFC8BAA37
			chs[2] = 0xC133DEC4
			chs[3] = "ZY.FT"
		elseif ch == "护法灭魔_虎头怪_普通" then
			chs[1] = 0x0F200DE2
			chs[2] = 0xEE971A2F
			chs[3] = "ZY.FT"
		elseif ch == "碧血干戚_虎头怪_普通" then
			chs[1] = 2740822575
			chs[2] = 1846382457
			chs[3] = "ZY.FT"
		elseif ch == "裂天_虎头怪_普通" then
			chs[1] = 0161677133
			chs[2] = 2342305231
			chs[3] = "ZY.FT"
		elseif ch == "鱼肠_剑侠客_普通" then
			chs[1] = 3889393968
			chs[2] = 0792072082
			chs[3] = "ZY.FT"
		elseif ch == "鱼肠_逍遥生_普通" then
			chs[1] = 0x39BAEA10
			chs[2] = 0xE9D644EA
			chs[3] = "ZY.FT"
		elseif ch == "倚天_剑侠客_普通" then
			chs[1] = 2329390480
			chs[2] = 0026468517
			chs[3] = "ZY.FT"
		elseif ch == "倚天_逍遥生_普通" then
			chs[1] = 0745286869
			chs[2] = 1361011629
			chs[3] = "ZY.FT"
		elseif ch == "湛卢_剑侠客_普通" then
			chs[1] = 1840181339
			chs[2] = 0603337468
			chs[3] = "ZY.FT"
		elseif ch == "湛卢_逍遥生_普通" then
			chs[1] = 0x439811D3
			chs[2] = 0x6DE2CCCB
			chs[3] = "ZY.FT"
		elseif ch == "画龙点睛_逍遥生_普通" then
			chs[1] = 0xF38680F5
			chs[2] = 0xBDC4259D
			chs[3] = "ZY.FT"
		elseif ch == "秋水人家_逍遥生_普通" then
			chs[1] = 0x9B3FF437
			chs[2] = 0xEEFDA661
			chs[3] = "ZY.FT"
		elseif ch == "逍遥江湖_逍遥生_普通" then
			chs[1] = 0x08D4818E
			chs[2] = 0x0FF23BFF
			chs[3] = "ZY.FT"
		elseif ch == "浩气长舒_逍遥生_普通" then
			chs[1] = 3196397447
			chs[2] = 0378375394
			chs[3] = "ZY.FT"
		elseif ch == "星瀚_逍遥生_普通" then
			chs[1] = 0791015559
			chs[2] = 3682713078
			chs[3] = "ZY.FT"
		elseif ch == "魏武青虹_剑侠客_普通" then
			chs[1] = 0x241DF4E3
			chs[2] = 0x3DDB7FF0
			chs[3] = "ZY.FT"
		elseif ch == "灵犀神剑_剑侠客_普通" then
			chs[1] = 0x31034D66
			chs[2] = 0xDFC62A73
			chs[3] = "ZY.FT"
		elseif ch == "四法青云_剑侠客_普通" then
			chs[1] = 0xDF749306
			chs[2] = 0x1BEC0D8A
			chs[3] = "ZY.FT"
		elseif ch == "霜冷九州_剑侠客_普通" then
			chs[1] = 0007350584
			chs[2] = 2444149863
			chs[3] = "ZY.FT"
		elseif ch == "擒龙_剑侠客_普通" then
			chs[1] = 1376642035
			chs[2] = 1247967770
			chs[3] = "ZY.FT"
		elseif ch == "魏武青虹_逍遥生_普通" then
			chs[1] = 0x2562AA24
			chs[2] = 0x2C0190DC
			chs[3] = "ZY.FT"
		elseif ch == "灵犀神剑_逍遥生_普通" then
			chs[1] = 0xB532E559
			chs[2] = 0x55AA2A03
			chs[3] = "ZY.FT"
		elseif ch == "四法青云_逍遥生_普通" then
			chs[1] = 0x345FD6AC
			chs[2] = 0x5763E3AE
			chs[3] = "ZY.FT"
		elseif ch == "霜冷九州_逍遥生_普通" then
			chs[1] = 3023520771
			chs[2] = 3722552317
			chs[3] = "ZY.FT"
		elseif ch == "擒龙_逍遥生_普通" then
			chs[1] = 1376642035
			chs[2] = 1247967770
			chs[3] = "ZY.FT"
		elseif ch == "太极_逍遥生_普通" then
			chs[1] = 0568453373
			chs[2] = 3793189291
			chs[3] = "ZY.FT"
		elseif ch == "太极_龙太子_普通" then
			chs[1] = 0x58D20E79
			chs[2] = 0xA963CEE9
			chs[3] = "ZY.FT"
		elseif ch == "玉龙_逍遥生_普通" then
			chs[1] = 1635836812
			chs[2] = 4197667404
			chs[3] = "ZY.FT"
		elseif ch == "玉龙_龙太子_普通" then
			chs[1] = 0x44BEAA26
			chs[2] = 0xE819C83D
			chs[3] = "ZY.FT"
		elseif ch == "秋风_逍遥生_普通" then
			chs[1] = 3028020838
			chs[2] = 4268258437
			chs[3] = "ZY.FT"
		elseif ch == "秋风_龙太子_普通" then
			chs[1] = 0x387B481F
			chs[2] = 0xFB69C03A
			chs[3] = "ZY.FT"
		elseif ch == "画龙点睛_龙太子_普通" then
			chs[1] = 0x741AAD01
			chs[2] = 0x7691CE27
			chs[3] = "ZY.FT"
		elseif ch == "秋水人家_龙太子_普通" then
			chs[1] = 0xB68FBD09
			chs[2] = 0x9A1E802B
			chs[3] = "ZY.FT"
		elseif ch == "逍遥江湖_龙太子_普通" then
			chs[1] = 0xD1FABFBB
			chs[2] = 0x46AB4BEC
			chs[3] = "ZY.FT"
		elseif ch == "浩气长舒_龙太子_普通" then
			chs[1] = 0xD5E7FC6E
			chs[2] = 0x804E4F85
			chs[3] = "ZY.FT"
		elseif ch == "星瀚_龙太子_普通" then
			chs[1] = 0xBD8FF16C
			chs[2] = 0xC7E0CEC1
			chs[3] = "ZY.FT"
		elseif ch == "冷月_巨魔王_普通" then
			chs[1] = 1674010879
			chs[2] = 1438363487
			chs[3] = "ZY.FT"
		elseif ch == "冷月_剑侠客_普通" then
			chs[1] = 0xDA08D26D
			chs[2] = 0x6C6F4579
			chs[3] = "ZY.FT"
		elseif ch == "屠龙_巨魔王_普通" then
			chs[1] = 2271660461
			chs[2] = 0895095420
			chs[3] = "ZY.FT"
		elseif ch == "屠龙_剑侠客_普通" then
			chs[1] = 3479012312
			chs[2] = 2635216567
			chs[3] = "ZY.FT"
		elseif ch == "血刃_巨魔王_普通" then
			chs[1] = 3645677695
			chs[2] = 1888150636
			chs[3] = "ZY.FT"
		elseif ch == "血刃_剑侠客_普通" then
			chs[1] = 0x85D0D994
			chs[2] = 0x50DA4A82
			chs[3] = "ZY.FT"
		elseif ch == "偃月青龙_剑侠客_普通" then
			chs[1] = 0xC68B564D
			chs[2] = 0x922E909A
			chs[3] = "ZY.FT"
		elseif ch == "斩妖泣血_剑侠客_普通" then
			chs[1] = 0x0EFA39F2
			chs[2] = 0x16FE7AF5
			chs[3] = "ZY.FT"
		elseif ch == "晓风残月_剑侠客_普通" then
			chs[1] = 0xE6AD88AE
			chs[2] = 0x6ED6B121
			chs[3] = "ZY.FT"
		elseif ch == "业火三灾_剑侠客_普通" then
			chs[1] = 4059506148
			chs[2] = 1966900337
			chs[3] = "ZY.FT"
		elseif ch == "鸣鸿_剑侠客_普通" then
			chs[1] = 1616701029
			chs[2] = 1774867550
			chs[3] = "ZY.FT"
		elseif ch == "偃月青龙_巨魔王_普通" then
			chs[1] = 0xF6A68E4C
			chs[2] = 0xB20BA1E9
			chs[3] = "ZY.FT"
		elseif ch == "斩妖泣血_巨魔王_普通" then
			chs[1] = 0x15B2D6D4
			chs[2] = 0x14997A50
			chs[3] = "ZY.FT"
		elseif ch == "晓风残月_巨魔王_普通" then
			chs[1] = 0x4B2232FE
			chs[2] = 0x2D70D93E
			chs[3] = "ZY.FT"
		elseif ch == "业火三灾_巨魔王_普通" then
			chs[1] = 4160821910
			chs[2] = 2586259872
			chs[3] = "ZY.FT"
		elseif ch == "鸣鸿_巨魔王_普通" then
			chs[1] = 2272535883
			chs[2] = 4183963940
			chs[3] = "ZY.FT"
		elseif ch == "沧海_骨精灵_普通" then
			chs[1] = 3768392022
			chs[2] = 3372471073
			chs[3] = "ZY.FT"
		elseif ch == "沧海_玄彩娥_普通" then
			chs[1] = 0xC1ACB479
			chs[2] = 0x6A9147F7
			chs[3] = "ZY.FT"
		elseif ch == "红莲_骨精灵_普通" then
			chs[1] = 2232215638
			chs[2] = 1432534548
			chs[3] = "ZY.FT"
		elseif ch == "红莲_玄彩娥_普通" then
			chs[1] = 0x0ADFB109
			chs[2] = 0xCA7E32B9
			chs[3] = "ZY.FT"
		elseif ch == "盘龙_骨精灵_普通" then
			chs[1] = 3403515550
			chs[2] = 00912832719
			chs[3] = "ZY.FT"
		elseif ch == "盘龙_玄彩娥_普通" then
			chs[1] = 0x69036521
			chs[2] = 0x02E73DE2
			chs[3] = "ZY.FT"
		elseif ch == "降魔玉杵_骨精灵_普通" then
			chs[1] = 0x51F29AF8
			chs[2] = 0x77E6448E
			chs[3] = "ZY.FT"
		elseif ch == "墨玉骷髅_骨精灵_普通" then
			chs[1] = 0x41184BFA
			chs[2] = 0x1C9639D1
			chs[3] = "ZY.FT"
		elseif ch == "青藤玉树_骨精灵_普通" then
			chs[1] = 0x162DD3BA
			chs[2] = 0xC603E808
			chs[3] = "ZY.FT"
		elseif ch == "丝萝乔木_骨精灵_普通" then
			chs[1] = 1422256901
			chs[2] = 3988348379
			chs[3] = "ZY.FT"
		elseif ch == "醍醐_骨精灵_普通" then
			chs[1] = 0609424635
			chs[2] = 4017665874
			chs[3] = "ZY.FT"
		elseif ch == "降魔玉杵_玄彩娥_普通" then
			chs[1] = 0xB3AB79E6
			chs[2] = 0x87E2D3A6
			chs[3] = "ZY.FT"
		elseif ch == "墨玉骷髅_玄彩娥_普通" then
			chs[1] = 0x89AFF2A0
			chs[2] = 0x791841F4
			chs[3] = "ZY.FT"
		elseif ch == "青藤玉树_玄彩娥_普通" then
			chs[1] = 0x98440F9D
			chs[2] = 0xA6807253
			chs[3] = "ZY.FT"
		elseif ch == "丝萝乔木_玄彩娥_普通" then
			chs[1] = 2492839980
			chs[2] = 0908131256
			chs[3] = "ZY.FT"
		elseif ch == "醍醐_玄彩娥_普通" then
			chs[1] = 2560304889
			chs[2] = 3032370082
			chs[3] = "ZY.FT"
		elseif ch == "八卦_神天兵_普通" then
			chs[1] = 0885266650
			chs[2] = 1350386258
			chs[3] = "ZY.FT"
		elseif ch == "八卦_虎头怪_普通" then
			chs[1] = 0xA438A403
			chs[2] = 0xAC1BAB97
			chs[3] = "ZY.FT"
		elseif ch == "鬼牙_神天兵_普通" then
			chs[1] = 0050371172
			chs[2] = 1900721739
			chs[3] = "ZY.FT"
		elseif ch == "鬼牙_虎头怪_普通" then
			chs[1] = 0x56DA382F
			chs[2] = 0x959DF477
			chs[3] = "ZY.FT"
		elseif ch == "雷神_神天兵_普通" then
			chs[1] = 1937688127
			chs[2] = 3648341818
			chs[3] = "ZY.FT"
		elseif ch == "雷神_虎头怪_普通" then
			chs[1] = 0x8CF27534
			chs[2] = 0xDA17EC9A
			chs[3] = "ZY.FT"
		elseif ch == "混元金锤_虎头怪_普通" then
			chs[1] = 0x8E7866EB
			chs[2] = 0x9101F598
			chs[3] = "ZY.FT"
		elseif ch == "九瓣莲花_虎头怪_普通" then
			chs[1] = 0xA7B9EF31
			chs[2] = 0xF5C7E01A
			chs[3] = "ZY.FT"
		elseif ch == "鬼王蚀日_虎头怪_普通" then
			chs[1] = 0xA14E222D
			chs[2] = 0x757CC5CE
			chs[3] = "ZY.FT"
		elseif ch == "狂澜碎岳_虎头怪_普通" then
			chs[1] = 0981496208
			chs[2] = 1685513235
			chs[3] = "ZY.FT"
		elseif ch == "碎寂_虎头怪_普通" then
			chs[1] = 0512165103
			chs[2] = 2912368983
			chs[3] = "ZY.FT"
		elseif ch == "混元金锤_神天兵_普通" then
			chs[1] = 4173823692
			chs[2] = 0903872944
			chs[3] = "ZY.FT"
		elseif ch == "九瓣莲花_神天兵_普通" then
			chs[1] = 1960543447
			chs[2] = 1550557054
			chs[3] = "ZY.FT"
		elseif ch == "鬼王蚀日_神天兵_普通" then
			chs[1] = 1056981959
			chs[2] = 0461194116
			chs[3] = "ZY.FT"
		elseif ch == "狂澜碎岳_神天兵_普通" then
			chs[1] = 4056574244
			chs[2] = 2900798348
			chs[3] = "ZY.FT"
		elseif ch == "碎寂_神天兵_普通" then
			chs[1] = 0577319844
			chs[2] = 2305127073
			chs[3] = "ZY.FT"
		elseif ch == "撕天_狐美人_普通" then
			chs[1] = 2959598354
			chs[2] = 3959909274
			chs[3] = "ZY.FT"
		elseif ch == "撕天_骨精灵_普通" then
			chs[1] = 0x3DF34F1E
			chs[2] = 0xE5D43556
			chs[3] = "ZY.FT"
		elseif ch == "毒牙_狐美人_普通" then
			chs[1] = 3492190917
			chs[2] = 0057548694
			chs[3] = "ZY.FT"
		elseif ch == "毒牙_骨精灵_普通" then
			chs[1] = 0xD6EF0627
			chs[2] = 0x831F74AE
			chs[3] = "ZY.FT"
		elseif ch == "胭脂_狐美人_普通" then
			chs[1] = 3087805369
			chs[2] = 0210949852
			chs[3] = "ZY.FT"
		elseif ch == "胭脂_骨精灵_普通" then
			chs[1] = 0x7D916F78
			chs[2] = 0xF65B1CC4
			chs[3] = "ZY.FT"
		elseif ch == "九阴勾魂_骨精灵_普通" then
			chs[1] = 0x63413CA4
			chs[2] = 0x9C82F08B
			chs[3] = "ZY.FT"
		elseif ch == "雪蚕之刺_骨精灵_普通" then
			chs[1] = 0xF1E4D97A
			chs[2] = 0x5D773943
			chs[3] = "ZY.FT"
		elseif ch == "贵霜之牙_骨精灵_普通" then
			chs[1] = 0xAE948CBB
			chs[2] = 0x7E47B81B
			chs[3] = "ZY.FT"
		elseif ch == "忘川三途_骨精灵_普通" then
			chs[1] = 2856473183
			chs[2] = 2645115155
			chs[3] = "ZY.FT"
		elseif ch == "离钩_骨精灵_普通" then
			chs[1] = 1495713199
			chs[2] = 2314275840
			chs[3] = "ZY.FT"
		elseif ch == "九阴勾魂_狐美人_普通" then
			chs[1] = 0xC2DC1244
			chs[2] = 0xB0ED001E
			chs[3] = "ZY.FT"
		elseif ch == "雪蚕之刺_狐美人_普通" then
			chs[1] = 0xC4B56A00
			chs[2] = 0x0381802B
			chs[3] = "ZY.FT"
		elseif ch == "贵霜之牙_狐美人_普通" then
			chs[1] = 0x762F7556
			chs[2] = 0xCDD20A7E
			chs[3] = "ZY.FT"
		elseif ch == "忘川三途_狐美人_普通" then
			chs[1] = 1211891114
			chs[2] = 0425062404
			chs[3] = "ZY.FT"
		elseif ch == "离钩_狐美人_普通" then
			chs[1] = 1495713199
			chs[2] = 1510775202
			chs[3] = "ZY.FT"
		elseif ch == "彩虹_舞天姬_普通" then
			chs[1] = 0573669624
			chs[2] = 1480465175
			chs[3] = "ZY.FT"
		elseif ch == "彩虹_玄彩娥_普通" then
			chs[1] = 0x223180F8
			chs[2] = 0x583E1B17
			chs[3] = "ZY.FT"
		elseif ch == "流云_舞天姬_普通" then
			chs[1] = 1071235145
			chs[2] = 2395250490
			chs[3] = "ZY.FT"
		elseif ch == "流云_玄彩娥_普通" then
			chs[1] = 0x3FD9C049
			chs[2] = 0x8EC49F3A
			chs[3] = "ZY.FT"
		elseif ch == "碧波_舞天姬_普通" then
			chs[1] = 2358752644
			chs[2] = 3055396661
			chs[3] = "ZY.FT"
		elseif ch == "碧波_玄彩娥_普通" then
			chs[1] = 0x8C97B584
			chs[2] = 0xB61DA735
			chs[3] = "ZY.FT"
		elseif ch == "秋水落霞_舞天姬_普通" then
			chs[1] = 0xBC559AEB
			chs[2] = 0xBC89D4EB
			chs[3] = "ZY.FT"
		elseif ch == "晃金仙绳_舞天姬_普通" then
			chs[1] = 0x14918F2D
			chs[2] = 0x7AEB22FC
			chs[3] = "ZY.FT"
		elseif ch == "此最相思_舞天姬_普通" then
			chs[1] = 0xB04EF3E7
			chs[2] = 0xE47097A9
			chs[3] = "ZY.FT"
		elseif ch == "揽月摘星_舞天姬_普通" then
			chs[1] = 2425294287
			chs[2] = 0718029991
			chs[3] = "ZY.FT"
		elseif ch == "九霄_舞天姬_普通" then
			chs[1] = 1944035871
			chs[2] = 0335082726
			chs[3] = "ZY.FT"
		elseif ch == "别情离恨_舞天姬_普通" then
			chs[1] = 0xF420B6F9
			chs[2] = 0x0958E12B
			chs[3] = "ZY.FT"
		elseif ch == "金玉双环_舞天姬_普通" then
			chs[1] = 0x93CDA250
			chs[2] = 0xA50B0DB0
			chs[3] = "ZY.FT"
		elseif ch == "九天金线_舞天姬_普通" then
			chs[1] = 0xF5E7C6EF
			chs[2] = 0x0218E725
			chs[3] = "ZY.FT"
		elseif ch == "无关风月_舞天姬_普通" then
			chs[1] = 1007231129
			chs[2] = 1829725423
			chs[3] = "ZY.FT"
		elseif ch == "朝夕_舞天姬_普通" then
			chs[1] = 2753598378
			chs[2] = 0871520866
			chs[3] = "ZY.FT"
		elseif ch == "秋水落霞_玄彩娥_普通" then
			chs[1] = 0x8BF9992F
			chs[2] = 0x17D413B7
			chs[3] = "ZY.FT"
		elseif ch == "晃金仙绳_玄彩娥_普通" then
			chs[1] = 0xEBA114CC
			chs[2] = 0x8FDCA745
			chs[3] = "ZY.FT"
		elseif ch == "此最相思_玄彩娥_普通" then
			chs[1] = 0xBEF9E038
			chs[2] = 0xFCF69018
			chs[3] = "ZY.FT"
		elseif ch == "揽月摘星_玄彩娥_普通" then
			chs[1] = 4239911535
			chs[2] = 1327298920
			chs[3] = "ZY.FT"
		elseif ch == "九霄_玄彩娥_普通" then
			chs[1] = 1385035033
			chs[2] = 2513919345
			chs[3] = "ZY.FT"
		elseif ch == "龙筋_英女侠_普通" then
			chs[1] = 0120114237
			chs[2] = 4185441630
			chs[3] = "ZY.FT"
		elseif ch == "龙筋_狐美人_普通" then
			chs[1] = 0xD76152B7
			chs[2] = 0xB241D7A6
			chs[3] = "ZY.FT"
		elseif ch == "百花_英女侠_普通" then
			chs[1] = 3000747610
			chs[2] = 0571279871
			chs[3] = "ZY.FT"
		elseif ch == "百花_狐美人_普通" then
			chs[1] = 0x888C7C46
			chs[2] = 0xE7CBB64B
			chs[3] = "ZY.FT"
		elseif ch == "吹雪_英女侠_普通" then
			chs[1] = 1309495973
			chs[2] = 0159974487
			chs[3] = "ZY.FT"
		elseif ch == "吹雪_狐美人_普通" then
			chs[1] = 0xBC2F95EF
			chs[2] = 0x16AC35C8
			chs[3] = "ZY.FT"
		elseif ch == "游龙惊鸿_狐美人_普通" then
			chs[1] = 0xB11264BE
			chs[2] = 0x7C2A8EF7
			chs[3] = "ZY.FT"
		elseif ch == "仙人指路_狐美人_普通" then
			chs[1] = 0x87BE19F7
			chs[2] = 0x10C7E94F
			chs[3] = "ZY.FT"
		elseif ch == "血之刺藤_狐美人_普通" then
			chs[1] = 0x430429EE
			chs[2] = 0xAB7969FB
			chs[3] = "ZY.FT"
		elseif ch == "牧云清歌_狐美人_普通" then
			chs[1] = 0846824610
			chs[2] = 3304484241
			chs[3] = "ZY.FT"
		elseif ch == "霜陨_狐美人_普通" then
			chs[1] = 1215671834
			chs[2] = 1056698647
			chs[3] = "ZY.FT"
		elseif ch == "如意_舞天姬_普通" then
			chs[1] = 2798387657
			chs[2] = 0709983425
			chs[3] = "ZY.FT"
		elseif ch == "如意_飞燕女_普通" then
			chs[1] = 0x108FFF69
			chs[2] = 0xEEB4814C
			chs[3] = "ZY.FT"
		elseif ch == "乾坤_舞天姬_普通" then
			chs[1] = 4117773016
			chs[2] = 0888040957
			chs[3] = "ZY.FT"
		elseif ch == "乾坤_飞燕女_普通" then
			chs[1] = 0x516AC342
			chs[2] = 0x6011B8EA
			chs[3] = "ZY.FT"
		elseif ch == "月光_舞天姬_普通" then
			chs[1] = 2969095231
			chs[2] = 3586695559
			chs[3] = "ZY.FT"
		elseif ch == "月光_飞燕女_普通" then
			chs[1] = 0xD2011BDE
			chs[2] = 0x118B2316
			chs[3] = "ZY.FT"
		elseif ch == "阴阳_飞燕女_普通" then
			chs[1] = 0006565675
			chs[2] = 0527831083
			chs[3] = "ZY.FT"
		elseif ch == "阴阳_英女侠_普通" then
			chs[1] = 0xB8D3407A
			chs[2] = 0x1F76102B
			chs[3] = "ZY.FT"
		elseif ch == "月光_飞燕女_普通" then
			chs[1] = 1968668223
			chs[2] = 1806180317
			chs[3] = "ZY.FT"
		elseif ch == "月光_英女侠_普通" then
			chs[1] = 0x0E469C32
			chs[2] = 0x6BA81FDD
			chs[3] = "ZY.FT"
		elseif ch == "灵蛇_飞燕女_普通" then
			chs[1] = 0585049597
			chs[2] = 0883345409
			chs[3] = "ZY.FT"
		elseif ch == "灵蛇_英女侠_普通" then
			chs[1] = 0x36D0EAA0
			chs[2] = 0x34A6C801
			chs[3] = "ZY.FT"
		elseif ch == "游龙惊鸿_英女侠_普通" then
			chs[1] = 0x3E7DDDB1
			chs[2] = 0x339E1DD5
			chs[3] = "ZY.FT"
		elseif ch == "仙人指路_英女侠_普通" then
			chs[1] = 0x36FA7E8F
			chs[2] = 0x33825F90
			chs[3] = "ZY.FT"
		elseif ch == "血之刺藤_英女侠_普通" then
			chs[1] = 0xCEF9D441
			chs[2] = 0xCB134023
			chs[3] = "ZY.FT"
		elseif ch == "牧云清歌_英女侠_普通" then
			chs[1] = 2729481727
			chs[2] = 1717626599
			chs[3] = "ZY.FT"
		elseif ch == "霜陨_英女侠_普通" then
			chs[1] = 1215671834
			chs[2] = 1056698647
			chs[3] = "ZY.FT"
		elseif ch == "金龙双剪_飞燕女_普通" then
			chs[1] = 0x88E2548C
			chs[2] = 0xC70FF23A
			chs[3] = "ZY.FT"
		elseif ch == "连理双树_飞燕女_普通" then
			chs[1] = 0xBFBE4DC9
			chs[2] = 0x4894EB42
			chs[3] = "ZY.FT"
		elseif ch == "祖龙对剑_飞燕女_普通" then
			chs[1] = 0x088A1188
			chs[2] = 0x0D3ED468
			chs[3] = "ZY.FT"
		elseif ch == "紫电青霜_飞燕女_普通" then
			chs[1] = 4174514804
			chs[2] = 3343319550
			chs[3] = "ZY.FT"
		elseif ch == "浮犀_飞燕女_普通" then
			chs[1] = 3366765078
			chs[2] = 3831481111
			chs[3] = "ZY.FT"
		elseif ch == "别情离恨_飞燕女_普通" then
			chs[1] = 0x72013AF5
			chs[2] = 0xF2FB1AFA
			chs[3] = "ZY.FT"
		elseif ch == "金玉双环_飞燕女_普通" then
			chs[1] = 0xA1661BC0
			chs[2] = 0xB6C5947A
			chs[3] = "ZY.FT"
		elseif ch == "九天金线_飞燕女_普通" then
			chs[1] = 0x454E8758
			chs[2] = 0x2D8326FB
			chs[3] = "ZY.FT"
		elseif ch == "无关风月_飞燕女_普通" then
			chs[1] = 0888561149
			chs[2] = 3212121801
			chs[3] = "ZY.FT"
		elseif ch == "朝夕_飞燕女_普通" then
			chs[1] = 0961373825
			chs[2] = 1399381898
			chs[3] = "ZY.FT"
		elseif ch == "金龙双剪_英女侠_普通" then
			chs[1] = 0xB2D36625
			chs[2] = 0x373FA8FC
			chs[3] = "ZY.FT"
		elseif ch == "连理双树_英女侠_普通" then
			chs[1] = 0x2F82163D
			chs[2] = 0xA051D73F
			chs[3] = "ZY.FT"
		elseif ch == "祖龙对剑_英女侠_普通" then
			chs[1] = 0x466B6959
			chs[2] = 0xEE3BCF6B
			chs[3] = "ZY.FT"
		elseif ch == "紫电青霜_英女侠_普通" then
			chs[1] = 4174514804
			chs[2] = 0254271220
			chs[3] = "ZY.FT"
		elseif ch == "浮犀_英女侠_普通" then
			chs[1] = 1454482476
			chs[2] = 3425270325
			chs[3] = "ZY.FT"
		elseif ch == "离火_巫蛮儿_普通" then
			chs[1] = 3491744635
			chs[2] = 3429826897
			chs[3] = "ZY.FT"
		elseif ch == "离火_杀破狼_普通" then
			chs[1] = 2641155879
			chs[2] = 1590716186
			chs[3] = "ZY.FT"
		elseif ch == "飞星_巫蛮儿_普通" then
			chs[1] = 3062034163
			chs[2] = 3170325397
			chs[3] = "ZY.FT"
		elseif ch == "飞星_杀破狼_普通" then
			chs[1] = 2717746866
			chs[2] = 1504946577
			chs[3] = "ZY.FT"
		elseif ch == "月华_巫蛮儿_普通" then
			chs[1] = 4115068184
			chs[2] = 2364973462
			chs[3] = "ZY.FT"
		elseif ch == "月华_杀破狼_普通" then
			chs[1] = 2389596849
			chs[2] = 2983597511
			chs[3] = "ZY.FT"
		elseif ch == "回风舞雪_杀破狼_普通" then
			chs[1] = 3286763223
			chs[2] = 1088084197
			chs[3] = "ZY.FT"
		elseif ch == "紫金葫芦_杀破狼_普通" then
			chs[1] = 3085783481
			chs[2] = 2243295981
			chs[3] = "ZY.FT"
		elseif ch == "裂云啸日_杀破狼_普通" then
			chs[1] = 3958823224
			chs[2] = 1544529746
			chs[3] = "ZY.FT"
		elseif ch == "云雷万里_杀破狼_普通" then
			chs[1] = 2031537277
			chs[2] = 1403541362
			chs[3] = "ZY.FT"
		elseif ch == "赤明_杀破狼_普通" then
			chs[1] = 3883902194
			chs[2] = 0xD74EF908
			chs[3] = "ZY.FT"
		elseif ch == "回风舞雪_巫蛮儿_普通" then
			chs[1] = 3587358129
			chs[2] = 3378197526
			chs[3] = "ZY.FT"
		elseif ch == "紫金葫芦_巫蛮儿_普通" then
			chs[1] = 0668840870
			chs[2] = 3082640737
			chs[3] = "ZY.FT"
		elseif ch == "裂云啸日_巫蛮儿_普通" then
			chs[1] = 0033586962
			chs[2] = 3500204883
			chs[3] = "ZY.FT"
		elseif ch == "云雷万里_巫蛮儿_普通" then
			chs[1] = 0086034382
			chs[2] = 2492569356
			chs[3] = "ZY.FT"
		elseif ch == "赤明_巫蛮儿_普通" then
			chs[1] = 3482219011
			chs[2] = 0125440945
			chs[3] = "ZY.FT"
		elseif ch == "非攻_杀破狼_普通" then
			chs[1] = 2360647601
			chs[2] = 3580642624
			chs[3] = "ZY.FT"
		elseif ch == "非攻_羽灵神_普通" then
			chs[1] = 4118502147
			chs[2] = 1301985929
			chs[3] = "ZY.FT"
		elseif ch == "幽篁_杀破狼_普通" then
			chs[1] = 1961778763
			chs[2] = 2093379552
			chs[3] = "ZY.FT"
		elseif ch == "幽篁_羽灵神_普通" then
			chs[1] = 0044473302
			chs[2] = 0943743279
			chs[3] = "ZY.FT"
		elseif ch == "百鬼_杀破狼_普通" then
			chs[1] = 3918682738
			chs[2] = 3616556566
			chs[3] = "ZY.FT"
		elseif ch == "百鬼_羽灵神_普通" then
			chs[1] = 4227459057
			chs[2] = 0226502213
			chs[3] = "ZY.FT"
		elseif ch == "冥火薄天_羽灵神_普通" then
			chs[1] = 1329583138
			chs[2] = 2921186180
			chs[3] = "ZY.FT"
		elseif ch == "龙鸣寒水_羽灵神_普通" then
			chs[1] = 2146481723
			chs[2] = 1237674101
			chs[3] = "ZY.FT"
		elseif ch == "太极流光_羽灵神_普通" then
			chs[1] = 0635066655
			chs[2] = 0454346244
			chs[3] = "ZY.FT"
		elseif ch == "九霄风雷_羽灵神_普通" then
			chs[1] = 1118851042
			chs[2] = 3857072095
			chs[3] = "ZY.FT"
		elseif ch == "若木_羽灵神_普通" then
			chs[1] = 2140866752
			chs[2] = 0300870526
			chs[3] = "ZY.FT"
		elseif ch == "冥火薄天_杀破狼_普通" then
			chs[1] = 4237566348
			chs[2] = 3313167169
			chs[3] = "ZY.FT"
		elseif ch == "龙鸣寒水_杀破狼_普通" then
			chs[1] = 1522717413
			chs[2] = 0xECB3ED94
			chs[3] = "ZY.FT"
		elseif ch == "太极流光_杀破狼_普通" then
			chs[1] = 3966436183
			chs[2] = 3741875286
			chs[3] = "ZY.FT"
		elseif ch == "九霄风雷_杀破狼_普通" then
			chs[1] = 0929770205
			chs[2] = 3222657771
			chs[3] = "ZY.FT"
		elseif ch == "若木_杀破狼_普通" then
			chs[1] = 1083623074
			chs[2] = 3047863049
			chs[3] = "ZY.FT"
		elseif ch == "业焰_羽灵神_普通" then
			chs[1] = 0834237487
			chs[2] = 3629386269
			chs[3] = "ZY.FT"
		elseif ch == "业焰_巫蛮儿_普通" then
			chs[1] = 0830839494
			chs[2] = 1738625165
			chs[3] = "ZY.FT"
		elseif ch == "玉辉_羽灵神_普通" then
			chs[1] = 0713952553
			chs[2] = 3757988895
			chs[3] = "ZY.FT"
		elseif ch == "玉辉_巫蛮儿_普通" then
			chs[1] = 0378020939
			chs[2] = 3913240810
			chs[3] = "ZY.FT"
		elseif ch == "鹿鸣_羽灵神_普通" then
			chs[1] = 1396998121
			chs[2] = 2061375933
			chs[3] = "ZY.FT"
		elseif ch == "鹿鸣_巫蛮儿_普通" then
			chs[1] = 1036718398
			chs[2] = 1545197282
			chs[3] = "ZY.FT"
		elseif ch == "庄周梦蝶_羽灵神_普通" then
			chs[1] = 4147727634
			chs[2] = 3250095538
			chs[3] = "ZY.FT"
		elseif ch == "凤翼流珠_羽灵神_普通" then
			chs[1] = 3818690168
			chs[2] = 3332193101
			chs[3] = "ZY.FT"
		elseif ch == "雪蟒霜寒_羽灵神_普通" then
			chs[1] = 1208982538
			chs[2] = 3271417203
			chs[3] = "ZY.FT"
		elseif ch == "碧海潮生_羽灵神_普通" then
			chs[1] = 3786902490
			chs[2] = 0069282931
			chs[3] = "ZY.FT"
		elseif ch == "弦月_羽灵神_普通" then
			chs[1] = 4218574639
			chs[2] = 3403641761
			chs[3] = "ZY.FT"
		elseif ch == "庄周梦蝶_巫蛮儿_普通" then
			chs[1] = 0383346255
			chs[2] = 0788977205
			chs[3] = "ZY.FT"
		elseif ch == "凤翼流珠_巫蛮儿_普通" then
			chs[1] = 2738200746
			chs[2] = 3013213807
			chs[3] = "ZY.FT"
		elseif ch == "雪蟒霜寒_巫蛮儿_普通" then
			chs[1] = 2446794254
			chs[2] = 2344781059
			chs[3] = "ZY.FT"
		elseif ch == "碧海潮生_巫蛮儿_普通" then
			chs[1] = 3147201779
			chs[2] = 3842138961
			chs[3] = "ZY.FT"
		elseif ch == "弦月_巫蛮儿_普通" then
			chs[1] = 2558772509
			chs[2] = 0994443450
			chs[3] = "ZY.FT"

		-- 新增人物
		elseif ch == "偃无师_普通" then
			chs[1] = 0x00000098
			chs[2] = 0x00000102
			chs[3] = "common/shape.wda"
		elseif ch == "钝铁重剑_偃无师_普通" then
			chs[1] = 0x00000071
			chs[2] = 0x00000060
			chs[3] = "common/shape.wda"
		elseif ch == "壁玉长铗_偃无师_普通" then
			chs[1] = 0x00000078
			chs[2] = 0x00000058
			chs[3] = "common/shape.wda"
		elseif ch == "惊涛雪_偃无师_普通" then
			chs[1] = 0x00000073
			chs[2] = 0x00000061
			chs[3] = "common/shape.wda"
		elseif ch == "鸦九_偃无师_普通" then
			chs[1] = 0x00000079
			chs[2] = 0x00000050
			chs[3] = "common/shape.wda"
		elseif ch == "昆吾_偃无师_普通" then
			chs[1] = 0x00000074
			chs[2] = 0x00000062
			chs[3] = "common/shape.wda"
		elseif ch == "弦歌_偃无师_普通" then
			chs[1] = 0x00000081
			chs[2] = 0x00000069
			chs[3] = "common/shape.wda"
		elseif ch == "墨骨枯麟_偃无师_普通" then
			chs[1] = 0x00000075
			chs[2] = 0x00000056
			chs[3] = "common/shape.wda"
		elseif ch == "腾蛇郁刃_偃无师_普通" then
			chs[1] = 0x00000077
			chs[2] = 0x00000065
			chs[3] = "common/shape.wda"
		elseif ch == "秋水澄流_偃无师_普通" then
			chs[1] = 0x00000076
			chs[2] = 0x00000104
			chs[3] = "common/shape.wda"
		elseif ch == "百辟镇魂_偃无师_普通" then
			chs[1] = 0x00000070
			chs[2] = 0x00000059
			chs[3] = "common/shape.wda"
		elseif ch == "长息_偃无师_普通" then
			chs[1] = 0x00000080
			chs[2] = 0x00000068
			chs[3] = "common/shape.wda"
		elseif ch == "桃夭夭_普通" then
			chs[1] = 0x00000091
			chs[2] = 0x00000090
			chs[3] = "common/shape.wdb"
		elseif ch == "素纸灯_桃夭夭_普通" then
			chs[1] = 0x00000059
			chs[2] = 0x00000096
			chs[3] = "common/shape.wdb"
		elseif ch == "如意宫灯_桃夭夭_普通" then
			chs[1] = 0x00000058
			chs[2] = 0x00000097
			chs[3] = "common/shape.wdb"
		elseif ch == "玉兔盏_桃夭夭_普通" then
			chs[1] = 0x00000061
			chs[2] = 0x00000098
			chs[3] = "common/shape.wdb"
		elseif ch == "蟠龙_桃夭夭_普通" then
			chs[1] = 0x00000057
			chs[2] = 0x00000099
			chs[3] = "common/shape.wdb"
		elseif ch == "云鹤_桃夭夭_普通" then
			chs[1] = 0x00000063
			chs[2] = 0x00000100
			chs[3] = "common/shape.wdb"
		elseif ch == "风荷_桃夭夭_普通" then
			chs[1] = 0x00000053
			chs[2] = 0x00000042
			chs[3] = "common/shape.wdb"
		elseif ch == "金风玉露_桃夭夭_普通" then
			chs[1] = 0x00000056
			chs[2] = 0x00000104
			chs[3] = "common/shape.wdb"
		elseif ch == "凰火燎原_桃夭夭_普通" then
			chs[1] = 0x00000103
			chs[2] = 0x00000044
			chs[3] = "common/shape.wdb"
		elseif ch == "月露清愁_桃夭夭_普通" then
			chs[1] = 0x00000062
			chs[2] = 0x00000101
			chs[3] = "common/shape.wdb"
		elseif ch == "夭桃侬李_桃夭夭_普通" then
			chs[1] = 0x00000060
			chs[2] = 0x00000105
			chs[3] = "common/shape.wdb"
		elseif ch == "荒尘_桃夭夭_普通" then
			chs[1] = 0x00000102
			chs[2] = 0x00000045
			chs[3] = "common/shape.wdb"
		elseif ch == "鬼潇潇_普通" then
			chs[1] = 0x00000065
			chs[2] = 0x00000053
			chs[3] = "common/shape.wdc"
		elseif ch == "红罗伞_鬼潇潇_普通" then
			chs[1] = 0x00000058
			chs[2] = 0x00000106
			chs[3] = "common/shape.wdc"
		elseif ch == "琳琅盖_鬼潇潇_普通" then
			chs[1] = 0x00000060
			chs[2] = 0x00000050
			chs[3] = "common/shape.wdc"
		elseif ch == "金刚伞_鬼潇潇_普通" then
			chs[1] = 0x00000110
			chs[2] = 0x00000072
			chs[3] = "common/shape.wdc"
		elseif ch == "鬼骨_鬼潇潇_普通" then
			chs[1] = 0x00000057
			chs[2] = 0x00000112
			chs[3] = "common/shape.wdc"
		elseif ch == "云梦_鬼潇潇_普通" then
			chs[1] = 0x00000064
			chs[2] = 0x00000113
			chs[3] = "common/shape.wdc"
		elseif ch == "枕霞_鬼潇潇_普通" then
			chs[1] = 0x00000059
			chs[2] = 0x00000066
			chs[3] = "common/shape.wdc"
		elseif ch == "碧火琉璃_鬼潇潇_普通" then
			chs[1] = 0x00000055
			chs[2] = 0x00000043
			chs[3] = "common/shape.wdc"
		elseif ch == "雪羽穿云_鬼潇潇_普通" then
			chs[1] = 0x00000062
			chs[2] = 0x00000117
			chs[3] = "common/shape.wdc"
		elseif ch == "月影星痕_鬼潇潇_普通" then
			chs[1] = 0x00000063
			chs[2] = 0x00000051
			chs[3] = "common/shape.wdc"
		elseif ch == "浮生归梦_鬼潇潇_普通" then
			chs[1] = 0x00000056
			chs[2] = 0x00000069
			chs[3] = "common/shape.wdc"
		elseif ch == "晴雪_鬼潇潇_普通" then
			chs[1] = 0x00000061
			chs[2] = 0x00000049
			chs[3] = "common/shape.wdc"
		-- 新神兽
		-- 新Npc
        elseif ch == "雪人_普通" then
			chs[1] = 0x00000020
			chs[2] = 0x00000009
			chs[3] = "ZHS.FT"
		elseif ch == "雪人1_普通" then
			chs[1] = 0xDA74178C
			chs[2] = nil
			chs[3] = "ZY.FT"
		elseif ch == "雪人2_普通" then
			chs[1] = 0xBE52981A
			chs[2] = nil
			chs[3] = "ZY.FT"
		elseif ch == "雪人3_普通" then
			chs[1] = 0xF6655BCB
			chs[2] = nil
			chs[3] = "ZY.FT"
		elseif ch == "雪人4_普通" then
			chs[1] = 0x7ED2C0BE
			chs[2] = nil
			chs[3] = "ZY.FT"
		elseif ch == "雪人5_普通" then
			chs[1] = 0x6843DF1A
			chs[2] = 0x6843DF1A
			chs[3] = "ZY.FT"
		elseif ch == "宝箱_普通" then
			chs[1] = 0xEFFD57F2
			chs[2] = nil
			chs[3] = "ZY.FT"
		-- 任务特效
		elseif ch == "僵尸倒地_普通" then
			chs[1] = 0x5723F6F9
			chs[2] = 0x5723F6F9
			chs[3] = "ZY.FT"
		elseif ch == "赌徒倒地_普通" then
			chs[1] = 0x93371DC2
			chs[2] = 0x93371DC2
			chs[3] = "ZY.FT"
		elseif ch == "逍遥生坐下_普通" then
			chs[1] = 0x81105904
			chs[2] = 0x81105904
			chs[3] = "ZY.FT"
		elseif ch == "英女侠坐下_普通" then
			chs[1] = 0x8AE7E3A4
			chs[2] = 0x8AE7E3A4
			chs[3] = "ZY.FT"
		elseif ch == "玄彩娥坐下_普通" then
			chs[1] = 0xF6820612
			chs[2] = 0xF6820612
			chs[3] = "ZY.FT"
		elseif ch == "舞天姬坐下_普通" then
			chs[1] = 3933281088
			chs[2] = 3933281088
			chs[3] = "ZY.FT"
		elseif ch == "神天兵坐下_普通" then
			chs[1] = 0xA4D3F138
			chs[2] = 0xA4D3F138
			chs[3] = "ZY.FT"
		elseif ch == "龙太子坐下_普通" then
			chs[1] = 0x1D76393F
			chs[2] = 0x1D76393F
			chs[3] = "ZY.FT"
		elseif ch == "巨魔王坐下_普通" then
			chs[1] = 0x8349ECD1
			chs[2] = 0x8349ECD1
			chs[3] = "ZY.FT"
		elseif ch == "剑侠客坐下_普通" then
			chs[1] = 0xCA7EED95
			chs[2] = 0xCA7EED95
			chs[3] = "ZY.FT"
		elseif ch == "虎头怪坐下_普通" then
			chs[1] = 0x0431506B
			chs[2] = 0x0431506B
			chs[3] = "ZY.FT"
		elseif ch == "狐美人坐下_普通" then
			chs[1] = 0x9C1633C8
			chs[2] = 0x9C1633C8
			chs[3] = "ZY.FT"
		elseif ch == "骨精灵坐下_普通" then
			chs[1] = 0x72AFA4E9
			chs[2] = 0x72AFA4E9
			chs[3] = "ZY.FT"
		elseif ch == "飞燕女坐下_普通" then
			chs[1] = 0x45988F73
			chs[2] = 0x45988F73
			chs[3] = "ZY.FT"
		elseif ch == "巫蛮儿坐下_普通" then
			chs[1] = 0803847012
			chs[2] = 0803847012
			chs[3] = "ZY.FT"
		elseif ch == "杀破狼坐下_普通" then
			chs[1] = 448666846
			chs[2] = 448666846
			chs[3] = "ZY.FT"
		elseif ch == "羽灵神坐下_普通" then
			chs[1] = 0775868075
			chs[2] = 0775868075
			chs[3] = "ZY.FT"
		elseif ch == "偃无师坐下_普通" then
			chs[1] = 0x00000098
			chs[2] = 0x00000102
			chs[3] = "common/shape.wda"
		elseif ch == "桃夭夭坐下_普通" then
			chs[1] = 0x00000091
			chs[2] = 0x00000090
			chs[3] = "common/shape.wdb"
		elseif ch == "鬼潇潇坐下_普通" then
			chs[1] = 0x00000065
			chs[2] = 0x00000053
			chs[3] = "common/shape.wdc"
-----------------------------锦衣
      	elseif ch == "冰灵蝶翼·月笼_逍遥生_普通" then
			chs[1] = 0x00000011
			chs[2] = 0x00000012
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_逍遥生_普通" then
			chs[1] = 0x02000103
			chs[2] = 0x02000104
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_逍遥生_普通" then
			chs[1] = 0x02000107
			chs[2] = 0x02000108
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_剑侠客_普通" then
			chs[1] = 0x00000003
			chs[2] = 0x00000004
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_剑侠客_普通" then
			chs[1] = 0x02000097
			chs[2] = 0x02000098
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_剑侠客_普通" then
			chs[1] = 0x02000101
			chs[2] = 0x02000102
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_飞燕女_普通" then
			chs[1] = 0x00000007
			chs[2] = 0x00000008
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_飞燕女_普通" then
			chs[1] = 0x02000091
			chs[2] = 0x02000092
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_飞燕女_普通" then
			chs[1] = 0x02000095
			chs[2] = 0x02000096
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_英女侠_普通" then
			chs[1] = 0x00000015
			chs[2] = 0x00000016
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_英女侠_普通" then
			chs[1] = 0x02000085
			chs[2] = 0x02000086
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_英女侠_普通" then
			chs[1] = 0x02000089
			chs[2] = 0x02000090
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_巨魔王_普通" then
			chs[1] = 0x00000025
			chs[2] = 0x00000026
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_巨魔王_普通" then
			chs[1] = 0x02000079
			chs[2] = 0x02000080
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_巨魔王_普通" then
			chs[1] = 0x02000083
			chs[2] = 0x02000084
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_虎头怪_普通" then
			chs[1] = 0x00000023
			chs[2] = 0x00000024
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_虎头怪_普通" then
			chs[1] = 0x02000073
			chs[2] = 0x02000074
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_虎头怪_普通" then
			chs[1] = 0x02000077
			chs[2] = 0x02000078
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_狐美人_普通" then
			chs[1] = 0x00000021
			chs[2] = 0x00000022
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_狐美人_普通" then
			chs[1] = 0x02000067
			chs[2] = 0x02000068
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_狐美人_普通" then
			chs[1] = 0x02000071
			chs[2] = 0x02000072
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_骨精灵_普通" then
			chs[1] = 0x00000017
			chs[2] = 0x00000018
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_骨精灵_普通" then
			chs[1] = 0x02000061
			chs[2] = 0x02000062
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_骨精灵_普通" then
			chs[1] = 0x02000065
			chs[2] = 0x02000066
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_龙太子_普通" then
			chs[1] = 0x00000029
			chs[2] = 0x00000030
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_龙太子_普通" then
			chs[1] = 0x02000055
			chs[2] = 0x02000056
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_龙太子_普通" then
			chs[1] = 0x02000059
			chs[2] = 0x02000060
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_舞天姬_普通" then
			chs[1] = 0x00000035
			chs[2] = 0x00000036
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_舞天姬_普通" then
			chs[1] = 0x02000049
			chs[2] = 0x02000050
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_舞天姬_普通" then
			chs[1] = 0x02000053
			chs[2] = 0x02000054
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_玄彩娥_普通" then
			chs[1] = 0x00000037
			chs[2] = 0x00000038
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_玄彩娥_普通" then
			chs[1] = 0x02000043
			chs[2] = 0x02000044
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_玄彩娥_普通" then
			chs[1] = 0x02000047
			chs[2] = 0x02000048
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_偃无师_普通" then
			chs[1] = 0x00000013
			chs[2] = 0x00000014
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_偃无师_普通" then
			chs[1] = 0x02000037
			chs[2] = 0x02000038
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_偃无师_普通" then
			chs[1] = 0x02000041
			chs[2] = 0x02000042
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_巫蛮儿_普通" then
			chs[1] = 0x00000009
			chs[2] = 0x00000010
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_巫蛮儿_普通" then
			chs[1] = 0x02000031
			chs[2] = 0x02000032
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_巫蛮儿_普通" then
			chs[1] = 0x02000035
			chs[2] = 0x02000036
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_杀破狼_普通" then
			chs[1] = 0x00000027
			chs[2] = 0x00000028
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_杀破狼_普通" then
			chs[1] = 0x02000025
			chs[2] = 0x02000026
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_杀破狼_普通" then
			chs[1] = 0x02000029
			chs[2] = 0x02000030
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_鬼潇潇_普通" then
			chs[1] = 0x00000019
			chs[2] = 0x00000020
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_鬼潇潇_普通" then
			chs[1] = 0x02000019
			chs[2] = 0x02000020
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_鬼潇潇_普通" then
			chs[1] = 0x02000023
			chs[2] = 0x02000024
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_羽灵神_普通" then
			chs[1] = 0x00000039
			chs[2] = 0x00000040
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_羽灵神_普通" then
			chs[1] = 0x02000013
			chs[2] = 0x02000014
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_羽灵神_普通" then
			chs[1] = 0x02000017
			chs[2] = 0x02000018
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_桃夭夭_普通" then
			chs[1] = 0x00000033
			chs[2] = 0x00000034
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_桃夭夭_普通" then
			chs[1] = 0x02000007
			chs[2] = 0x02000008
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_桃夭夭_普通" then
			chs[1] = 0x02000011
			chs[2] = 0x02000012
			chs[3] = "JM.FT"
       	elseif ch == "冰灵蝶翼·月笼_神天兵_普通" then
			chs[1] = 0x00000031
			chs[2] = 0x00000032
			chs[3] = "JM.FT"
      	elseif ch == "齐天大圣_神天兵_普通" then
			chs[1] = 0x02000001
			chs[2] = 0x02000002
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱·月白_神天兵_普通" then
			chs[1] = 0x02000005
			chs[2] = 0x02000006
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_羽灵神_普通" then
			chs[1] = 0x00000075
			chs[2] = 0x00000076
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_英女侠_普通" then
			chs[1] = 0x00000073
			chs[2] = 0x00000074
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_剑侠客_普通" then
			chs[1] = 0x00000071
			chs[2] = 0x00000072
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_桃夭夭_普通" then
			chs[1] = 0x00000069
			chs[2] = 0x00000070
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_逍遥生_普通" then
			chs[1] = 0x00000067
			chs[2] = 0x00000068
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_舞天姬_普通" then
			chs[1] = 0x00000065
			chs[2] = 0x00000066
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_巫蛮儿_普通" then
			chs[1] = 0x00000063
			chs[2] = 0x00000064
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_玄彩娥_普通" then
			chs[1] = 0x00000061
			chs[2] = 0x00000062
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_神天兵_普通" then
			chs[1] = 0x00000059
			chs[2] = 0x00000060
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_杀破狼_普通" then
			chs[1] = 0x00000057
			chs[2] = 0x00000058
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_龙太子_普通" then
			chs[1] = 0x00000055
			chs[2] = 0x00000056
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_巨魔王_普通" then
			chs[1] = 0x00000053
			chs[2] = 0x00000054
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_虎头怪_普通" then
			chs[1] = 0x00000049
			chs[2] = 0x00000050
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_狐美人_普通" then
			chs[1] = 0x00000047
			chs[2] = 0x00000048
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_鬼潇潇_普通" then
			chs[1] = 0x00000045
			chs[2] = 0x00000046
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_骨精灵_普通" then
			chs[1] = 0x00000043
			chs[2] = 0x00000044
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_偃无师_普通" then
			chs[1] = 0x00000051
			chs[2] = 0x00000052
			chs[3] = "JM.FT"
      	elseif ch == "浪淘纱_飞燕女_普通" then
			chs[1] = 0x00000041
			chs[2] = 0x00000042
			chs[3] = "JM.FT"
      	elseif ch == "七彩神驴_普通" then
			chs[1] = 0x3D33442E
			chs[2] = 0xE8252C7B
			chs[3] = "ZY.FT"

        elseif ch == "尖叫服_飞燕女_普通" then
			chs[1] = 0x00000002
			chs[2] = 0x00000001
			chs[3] = "WE.dll"
		elseif ch == "尖叫服_骨精灵_普通" then
			chs[1] = 0x00000004
			chs[2] = 0x00000003
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_鬼潇潇_普通" then
			chs[1] = 0x00000006
			chs[2] = 0x00000005
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_狐美人_普通" then
			chs[1] = 0x00000008
			chs[2] = 0x00000007
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_虎头怪_普通" then
			chs[1] = 0x00000010
			chs[2] = 0x00000009
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_剑侠客_普通" then
			chs[1] = 0x00000012
			chs[2] = 0x00000011
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_巨魔王_普通" then
			chs[1] = 0x00000014
			chs[2] = 0x00000013
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_龙太子_普通" then
			chs[1] = 0x00000016
			chs[2] = 0x00000015
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_杀破狼_普通" then
			chs[1] = 0x00000018
			chs[2] = 0x00000017
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_神天兵_普通" then
			chs[1] = 0x00000020
			chs[2] = 0x00000019
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_桃夭夭_普通" then
			chs[1] = 0x00000022
			chs[2] = 0x00000021
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_巫蛮儿_普通" then
			chs[1] = 0x00000024
			chs[2] = 0x00000023
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_偃无师_普通" then
			chs[1] = 0x00000026
			chs[2] = 0x00000025
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_舞天姬_普通" then
			chs[1] = 0x00000028
			chs[2] = 0x00000027
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_逍遥生_普通" then
			chs[1] = 0x00000030
			chs[2] = 0x00000029
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_玄彩娥_普通" then
			chs[1] = 0x00000032
			chs[2] = 0x00000031
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_英女侠_普通" then
			chs[1] = 0x00000034
			chs[2] = 0x00000033
			chs[3] = "WE.dll"
			elseif ch == "尖叫服_羽灵神_普通" then
			chs[1] = 0x00000036
			chs[2] = 0x00000035
			chs[3] = "WE.dll"

elseif ch == "锦绣幻梦_飞燕女_普通" then
			chs[1] = 0x00000041
			chs[2] = 0x00000042
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_骨精灵_普通" then
			chs[1] = 0x00000043
			chs[2] = 0x00000044
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_鬼潇潇_普通" then
			chs[1] = 0x00000045
			chs[2] = 0x00000046
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_狐美人_普通" then
			chs[1] = 0x00000047
			chs[2] = 0x00000048
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_虎头怪_普通" then
			chs[1] = 0x00000049
			chs[2] = 0x00000050
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_剑侠客_普通" then
			chs[1] = 0x00000051
			chs[2] = 0x00000052
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_巨魔王_普通" then
			chs[1] = 0x00000053
			chs[2] = 0x00000054
			chs[3] = "WE.dll"

elseif ch == "锦绣幻梦_龙太子_普通" then
			chs[1] = 0x00000055
			chs[2] = 0x00000056
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_杀破狼_普通" then
			chs[1] = 0x00000057
			chs[2] = 0x00000058
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_神天兵_普通" then
			chs[1] = 0x00000059
			chs[2] = 0x00000060
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_桃夭夭_普通" then
			chs[1] = 0x00000061
			chs[2] = 0x00000062
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_巫蛮儿_普通" then
			chs[1] = 0x00000063
			chs[2] = 0x00000064
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_舞天姬_普通" then
			chs[1] = 0x00000065
			chs[2] = 0x00000066
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_逍遥生_普通" then
			chs[1] = 0x00000067
			chs[2] = 0x00000068
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_玄彩娥_普通" then
			chs[1] = 0x00000069
			chs[2] = 0x00000070
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_偃无师_普通" then
			chs[1] = 0x00000071
			chs[2] = 0x00000072
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_英女侠_普通" then
			chs[1] = 0x00000073
			chs[2] = 0x00000074
			chs[3] = "WE.dll"
elseif ch == "锦绣幻梦_羽灵神_普通" then
			chs[1] = 0x00000075
			chs[2] = 0x00000076
			chs[3] = "WE.dll"




elseif ch == "萤雪轻裘_飞燕女_普通" then
			chs[1] = 0x00000093
			chs[2] = 0x00000094
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_骨精灵_普通" then
			chs[1] = 0x00000095
			chs[2] = 0x00000096
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_鬼潇潇_普通" then
			chs[1] = 0x00000097
			chs[2] = 0x00000098
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_狐美人_普通" then
			chs[1] = 0x00000099
			chs[2] = 0x00000100
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_虎头怪_普通" then
			chs[1] = 0x00000101
			chs[2] = 0x00000102
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_剑侠客_普通" then
			chs[1] = 0x00000103
			chs[2] = 0x00000104
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_巨魔王_普通" then
			chs[1] = 0x00000105
			chs[2] = 0x00000106
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_龙太子_普通" then
			chs[1] = 0x00000107
			chs[2] = 0x00000108
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_杀破狼_普通" then
			chs[1] = 0x00000109
			chs[2] = 0x00000110
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_神天兵_普通" then
			chs[1] = 0x00000111
			chs[2] = 0x00000112
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_桃夭夭_普通" then
			chs[1] = 0x00000113
			chs[2] = 0x00000114
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_巫蛮儿_普通" then
			chs[1] = 0x00000115
			chs[2] = 0x00000116
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_偃无师_普通" then
			chs[1] = 0x00000117
			chs[2] = 0x00000118
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_舞天姬_普通" then
			chs[1] = 0x00000119
			chs[2] = 0x00000120
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_逍遥生_普通" then
			chs[1] = 0x00000121
			chs[2] = 0x00000122
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_玄彩娥_普通" then
			chs[1] = 0x00000123
			chs[2] = 0x00000124
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_英女侠_普通" then
			chs[1] = 0x00000125
			chs[2] = 0x00000126
			chs[3] = "WE.dll"
elseif ch == "萤雪轻裘_羽灵神_普通" then
			chs[1] = 0x00000127
			chs[2] = 0x00000128
			chs[3] = "WE.dll"



elseif ch == "铃儿叮当_飞燕女_普通" then
			chs[1] = 0x00000129
			chs[2] = 0x00000130
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_骨精灵_普通" then
			chs[1] = 0x00000131
			chs[2] = 0x00000132
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_鬼潇潇_普通" then
			chs[1] = 0x00000133
			chs[2] = 0x00000134
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_狐美人_普通" then
			chs[1] = 0x00000135
			chs[2] = 0x00000136
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_虎头怪_普通" then
			chs[1] = 0x00000137
			chs[2] = 0x00000138
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_剑侠客_普通" then
			chs[1] = 0x00000139
			chs[2] = 0x00000140
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_巨魔王_普通" then
			chs[1] = 0x00000141
			chs[2] = 0x00000142
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_龙太子_普通" then
			chs[1] = 0x00000143
			chs[2] = 0x00000144
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_杀破狼_普通" then
			chs[1] = 0x00000145
			chs[2] = 0x00000146
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_神天兵_普通" then
			chs[1] = 0x00000147
			chs[2] = 0x00000148
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_桃夭夭_普通" then
			chs[1] = 0x00000149
			chs[2] = 0x00000150
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_巫蛮儿_普通" then
			chs[1] = 0x00000151
			chs[2] = 0x00000152
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_舞天姬_普通" then
			chs[1] = 0x00000153
			chs[2] = 0x00000154
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_逍遥生_普通" then
			chs[1] = 0x00000155
			chs[2] = 0x00000156
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_玄彩娥_普通" then
			chs[1] = 0x00000157
			chs[2] = 0x00000158
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_偃无师_普通" then
			chs[1] = 0x00000159
			chs[2] = 0x00000160
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_英女侠_普通" then
			chs[1] = 0x00000161
			chs[2] = 0x00000162
			chs[3] = "WE.dll"
elseif ch == "铃儿叮当_羽灵神_普通" then
			chs[1] = 0x00000163
			chs[2] = 0x00000164
			chs[3] = "WE.dll"

elseif ch == "顽皮小恶魔_飞燕女_普通" then
			chs[1] = 0x00000165
			chs[2] = 0x00000166
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_骨精灵_普通" then
			chs[1] = 0x00000167
			chs[2] = 0x00000168
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_鬼潇潇_普通" then
			chs[1] = 0x00000169
			chs[2] = 0x00000170
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_狐美人_普通" then
			chs[1] = 0x00000171
			chs[2] = 0x00000172
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_虎头怪_普通" then
			chs[1] = 0x00000173
			chs[2] = 0x00000174
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_剑侠客_普通" then
			chs[1] = 0x00000175
			chs[2] = 0x00000176
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_巨魔王_普通" then
			chs[1] = 0x00000177
			chs[2] = 0x00000178
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_龙太子_普通" then
			chs[1] = 0x00000179
			chs[2] = 0x00000180
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_杀破狼_普通" then
			chs[1] = 0x00000181
			chs[2] = 0x00000182
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_神天兵_普通" then
			chs[1] = 0x00000183
			chs[2] = 0x00000184
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_桃夭夭_普通" then
			chs[1] = 0x00000185
			chs[2] = 0x00000186
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_巫蛮儿_普通" then
			chs[1] = 0x00000187
			chs[2] = 0x00000188
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_偃无师_普通" then
			chs[1] = 0x00000195
			chs[2] = 0x00000196
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_舞天姬_普通" then
			chs[1] = 0x00000189
			chs[2] = 0x00000190
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_逍遥生_普通" then
			chs[1] = 0x00000191
			chs[2] = 0x00000192
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_玄彩娥_普通" then
			chs[1] = 0x00000193
			chs[2] = 0x00000194
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_英女侠_普通" then
			chs[1] = 0x00000197
			chs[2] = 0x00000198
			chs[3] = "WE.dll"
elseif ch == "顽皮小恶魔_羽灵神_普通" then
			chs[1] = 0x00000199
			chs[2] = 0x00000200
			chs[3] = "WE.dll"


        elseif ch == "碧华锦·凌雪_英女侠_普通" then
			chs[1] = 0x02000143
			chs[2] = 0x02000144
			chs[3] = "WP.FT"
		elseif ch == "碧华锦·凌雪_剑侠客_普通" then
			chs[1] = 0x02000121
			chs[2] = 0x02000122
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_桃夭夭_普通" then
			chs[1] = 0x02000131
			chs[2] = 0x02000132
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_逍遥生_普通" then
			chs[1] = 0x02000137
			chs[2] = 0x02000138
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_舞天姬_普通" then
			chs[1] = 0x02000135
			chs[2] = 0x02000136
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_巫蛮儿_普通" then
			chs[1] = 0x02000133
			chs[2] = 0x02000134
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_玄彩娥_普通" then
			chs[1] = 0x02000139
			chs[2] = 0x02000140
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_神天兵_普通" then
			chs[1] = 0x02000129
			chs[2] = 0x02000130
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_杀破狼_普通" then
			chs[1] = 0x02000125
			chs[2] = 0x02000126
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_龙太子_普通" then
			chs[1] = 0x02000123
			chs[2] = 0x02000124
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_巨魔王_普通" then
			chs[1] = 0x02000127
			chs[2] = 0x02000128
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_虎头怪_普通" then
			chs[1] = 0x02000119
			chs[2] = 0x02000120
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_狐美人_普通" then
			chs[1] = 0x02000117
			chs[2] = 0x02000118
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_鬼潇潇_普通" then
			chs[1] = 0x02000115
			chs[2] = 0x02000116
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_骨精灵_普通" then
			chs[1] = 0x02000114
			chs[2] = 0x02000113
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_偃无师_普通" then
			chs[1] = 0x02000142
			chs[2] = 0x02000141
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_飞燕女_普通" then
			chs[1] = 0x02000112
			chs[2] = 0x02000111
			chs[3] = "WP.FT"
      	elseif ch == "碧华锦·凌雪_羽灵神_普通" then
			chs[1] = 0x02000145
			chs[2] = 0x02000146
			chs[3] = "WP.FT"


		end
		return chs
end
