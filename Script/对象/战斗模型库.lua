--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-04-09 08:18:58
--======================================================================--
--1是攻击 2是第二种形态攻击 3是死亡 5 返回4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包
function 引擎.取战斗模型(pg,wq)

	local pgs = {}
	local s = "_普通"
	if wq ~= nil then
	   	s = "_"..wq
	end
	pg = pg..s

    -----------------------神兽


			if pg == "超级白泽_普通"then
				pgs[6] = 0xBBDDA695
				pgs[9] = 0x14DB61B1
				pgs[8] = 0xDBA0C128
				pgs[5] = 0xF9BD7D20
				pgs[4] = 0x1BAC097C
			    pgs[1]= 0x8B3498E6
                pgs[2]=0xCD707E8A
				pgs[7]= 0x6E0A2649
				pgs[3]= 0xC64AEC4D
				pgs[10] = "ZHS.FT"
			elseif pg == "雪人_普通"then
		        pgs[6] = 0x00000010
		        pgs[9] = 0x00000003
		        pgs[8] = 0x00000011
		        pgs[5] = 0x00000007
		        pgs[4] = 0x00000006
		        pgs[1] = 0x00000005
		        pgs[2] = 0x00000008
		        pgs[7] = 0x00000004
		        pgs[3] = 0x00000012
		        静立= 0x00000020
		        行走= 0x00000009
		   pgs[10] = "ZHS.FT"
			elseif pg == "超级大机_普通"then
		         pgs[6] = 0x00000013
		         pgs[9] = 0x00000017
		         pgs[8] = 0x00000015
		         pgs[5] = 0x00000015
		         pgs[4] = 0x00000015
		         pgs[1] = 0x00000016
		         pgs[2] = 0x00000019
		         pgs[7] = 0x00000018
		         pgs[3] = 0x00000014
		        静立= 0x00000015
		        行走= 0x00000015
		   pgs[10] = "ZHS.FT"
			elseif pg == "超级小白泽_普通"then
				pgs[6] = 0x0CC97E71
				pgs[9] = 0x82384438
				pgs[8] = 0x5617E188
				pgs[5] = 0x5E0B405A
				pgs[4] = 0x82384438
		  		pgs[1]= 0x441977D2
                pgs[2]=0x68F2C5C4
				pgs[7]= 0x3F3CC7CE
				pgs[3]= 0x3F3CC7CE
				pgs[10] = "ZHS.FT"
			elseif pg == "进阶超级白泽_普通"then
				pgs[6] = 0xDE06522C
				pgs[9] = 0xB56BA2DF
				pgs[8] = 0xA45E50B2
				pgs[5] = 0xA6C0756E
				pgs[4] = 0x920B8D0E
			    pgs[1]= 0x46229E6F
                pgs[2]=0x660C3935
				pgs[7]= 0xCC60A0B4
				pgs[3]= 0x8786565C
				pgs[10] = "ZHS.FT"
			elseif pg =="超级赤焰兽_普通"then
				pgs[6] = 0xF3FAEC33
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xA6948363
				pgs[8] = 0xE58C5E3F
				pgs[5] = 0xB263ABDC
				pgs[4] = 0x01F4B42D
				pgs[2]= 0x80B546BB
				pgs[1]=0x9AA9FFA5
				pgs[7] =  0xD4C96E33
				pgs[3] = 0x40DBF841
			elseif pg =="进阶超级赤焰兽_普通"then
				pgs[6] = 0x183AB443
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xADDB702B
				pgs[8] = 0xBF41F3FE
				pgs[5] = 0x3B3A3B37
				pgs[4] = 0xB4A07130
				pgs[2]= 0xD24D54AC
				pgs[1]= 0x4B1D77C0
				pgs[7] =  0xD4C96E33
				pgs[3] = 0x8F5C9F7A
			elseif pg =="超级大鹏_普通"then
				pgs[6] = 0xEB61471A
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x4864FC9E
				pgs[8] = 0xBA9681F9
				pgs[5] = 0x6ED31640
				pgs[4] = 0x4BB4FA5C
				pgs[1]= 0x558AA8C4
				pgs[2]= 0xAD653344
				pgs[7] = 0xE7D40AFA
				pgs[3] = 0x250ACC97
			elseif pg =="进阶超级大鹏_普通"then
				pgs[6] = 0xBCB5AD39
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xF320C91E
				pgs[8] = 0x8A6EB7CC
				pgs[5] = 0x6ED31640
				pgs[4] = 0x57388E49
				pgs[1]= 0x0459D0C9
				pgs[2]= 0x1CE13606
				pgs[7] = 0xBE426A11
				pgs[3] = 0x43FE337B
			elseif pg =="超级大象_普通"then
				pgs[6] = 0x3D46505C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x83A877F7
				pgs[8] = 0x389845D5
				pgs[5] = 0xA2135444
				pgs[4] = 0xE0F602EC
				pgs[1]= 0x2A44E34D
				pgs[2]= 0xFF56282B
				pgs[7] = 0x27DCD80C
				pgs[3] = 0xD08E3166
			elseif pg =="进阶超级大象_普通"then
				pgs[6] = 0x8E5B6E48
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x0EB91A8D
				pgs[8] = 0xD26F85A4
				pgs[5] = 0xCC62F298
				pgs[4] = 0x73C54B13
				pgs[1]= 0x1752A15D
				pgs[2]= 0x6EE899E7
				pgs[7] = 0xBE5CDC49
				pgs[3] = 0x6B012F02
			elseif pg =="超级大熊猫_普通"then
				pgs[6] = 0x17C37276
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x921C71C7
				pgs[8] = 0xACF4E61C
				pgs[5] = 0x4E6AC3E5
				pgs[4] = 0x1C8026A4
				pgs[1]= 0x90E04F21
				pgs[2]= 0xE3D29421
				pgs[7] = 0xF379FD8E
				pgs[3] = 0x897770DA
			elseif pg =="进阶超级大熊猫_普通"then
				pgs[6] = 0x24EAC80D
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x3380E107
				pgs[8] = 0xD1BDA75F
				pgs[5] = 0x0B84CA4D
				pgs[4] = 0x4517E097
				pgs[1]= 0x6D7D1072
				pgs[2]= 0xEEA8BA73
				pgs[7] = 0x479EB55A
				pgs[3] = 0x28F2D85A
			elseif pg =="超级海豚_普通"then
				pgs[6] = 0xBCC5F712
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x1A7BECFF
				pgs[8] = 0x22D302F4
				pgs[5] = 0x67B1DF01
				pgs[4] = 0x5FA6F172
				pgs[1]= 0x28B12AE9
				pgs[2]= 0x2907A178
				pgs[7] = 0xA28683D4
				pgs[3] = 0xB319132D
			elseif pg =="进阶超级海豚_普通"then
				pgs[6] = 0x2E9A246C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xF2142000
				pgs[8] = 0x84D2F6B7
				pgs[5] = 0xC0592E23
				pgs[4] = 0x65BCDD9F
				pgs[1]= 0x52AB750A
				pgs[2]= 0x7CAC9222
				pgs[7] = 0x66B6EDAD
				pgs[3] = 0xD03C19EF
			elseif pg =="超级筋斗云_普通"then
				pgs[6] = 0x0DC1C46C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x6BB301C5
				pgs[8] = 0x02379071
				pgs[5] = 0x3B6A0F84
				pgs[4] = 0x23076AEF
				pgs[2]= 0x92C5B61C
				pgs[1]= 0xB80D599D
				pgs[7]= 0xDD86B4EC
				pgs[3] = 0x723CEB57
			elseif pg =="进阶超级筋斗云_普通"then
				pgs[6] = 0x85D20F2E
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x9862F314
				pgs[8] = 0x256F1AF4
				pgs[5] = 0x9C6D58E5
				pgs[4] = 0x818FF438
				pgs[1]= 0x24251EFC
				pgs[2]= 0x8B2F2F9B
				pgs[7]= 0xF509F1E1
				pgs[3] = 0x7A8436FD
			elseif pg =="超级孔雀_普通"then
				pgs[6] = 0xBD7EF6B7
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x4E033690
				pgs[8] = 0xAAA22A21
				pgs[5] = 0x5F77749E
				pgs[4] = 0xD0829F8F
				pgs[1]= 0x51ABC63B
				pgs[2]= 0x6B382C76
				pgs[7] = 0x4444BECA
				pgs[3] = 0x3A6A756E
			elseif pg =="进阶超级孔雀_普通"then
				pgs[6] = 0xC5BD436C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x6B62E486
				pgs[8] = 0xDD1B6B4A
				pgs[5] = 0x40C0030B
				pgs[4] = 0xB553702F
				pgs[1]= 0x0504A51B
				pgs[2]= 0xF3C41015
				pgs[7] = 0xD05DC9EB
				pgs[3] = 0x8378F293
			elseif pg =="超级金猴_普通"then
				pgs[6] = 0xED850381
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x417163CD
				pgs[8] = 0x3420E201
				pgs[5] = 0xDC846E45
				pgs[4] = 0x439C7952
				pgs[1]= 0x7D46BAFC
				pgs[2]= 0xB88BC6D0
				pgs[7] = 0x56F17261
				pgs[3] = 0x5704BD50
			elseif pg =="进阶超级金猴_普通"then
				pgs[6] = 0x5CFB35A4
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x818B17F7
				pgs[8] = 0x2A76AF8E
				pgs[5] = 0x2A4A610D
				pgs[4] = 0xBAF219BA
				pgs[1]= 0xB26E14F5
				pgs[2]= 0x12BF6FC0
				pgs[7] = 0x3F2F6B47
				pgs[3] = 0x2E6F465E
			elseif pg =="超级灵狐_普通"then
				pgs[6] = 0xAE6FFCA4
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x6945703C
				pgs[8] = 0xBE8856EE
				pgs[5] = 0xC4AA0EF2
				pgs[4] = 0x046E69D3
				pgs[1]= 0x30CA6EF0
				pgs[2]= 0x5AC8F9C2
				pgs[7] = 0xAEB15C0B
				pgs[3] = 0x3CE12B04
			elseif pg =="进阶超级灵狐_普通"then
				pgs[6] = 0x83B6301C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x094B19E8
				pgs[8] = 0x7E850C9B
				pgs[5] = 0xEBDBC670
				pgs[4] = 0x12B7F915
				pgs[1]= 0xAC07EE00
				pgs[2]= 0x9D3B0167
				pgs[7] = 0xD11C3E29
				pgs[3] = 0xE66363B4
			elseif pg =="超级灵鹿_普通"then
				pgs[6] = 0x20F2C401
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xC4B6BEE7
				pgs[8] = 0x7B97BEC9
				pgs[5] = 0x54196AF9
				pgs[4] = 0xF0F84EB5
				pgs[1]= 0xFD20E25C
				pgs[2]= 0x9BDBBF00
				pgs[7] = 0x7D7FE75B
				pgs[3] = 0xFF69656F
			elseif pg =="进阶超级灵鹿_普通"then
				pgs[6] = 0x49578F89
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x5D0BA7D9
				pgs[8] = 0xCAC235C1
				pgs[7] = 0x7D7FE75B
				pgs[5] = 0x726CCEB2
				pgs[4] = 0xF776BF6E
				pgs[1]= 0x8C15E636
				pgs[2]= 0x40E44F30
				pgs[3] = 0x5419AFEC
			elseif pg =="超级六耳猕猴_普通"then
				pgs[6] = 0x5423F6C7
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x604E28EE
				pgs[8] = 0x0B331E60
				pgs[7] = 0xD628E368
				pgs[5] = 0xDE52C900
				pgs[4] = 0xDB37489F
				pgs[1]= 0x31D95459
				pgs[2]= 0xDD3494C2
				pgs[3] = 0x8868C8EF
			elseif pg =="进阶超级六耳猕猴_普通"then
				pgs[6] = 0x68108E38
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xE93C3E81
				pgs[8] = 0xDE8F6E5A
				pgs[5] = 0xACD2514F
				pgs[4] = 0xDE8FCD95
				pgs[1]= 0x658531AB
				pgs[2]= 0xBFB3306A
				pgs[3] = 0xF45D1F93
			elseif pg =="超级泡泡_普通"then
				pgs[6] = 0x574EEAB7
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xC27E40BA
				pgs[8] = 0xC3C3CE08
				pgs[5] = 0x2EC505F0
				pgs[4] = 0x7B14D8E7
				pgs[1]= 0x362FA40A
				pgs[2]= 0x812BC457
				pgs[7] = 0x06C69364
				pgs[3] = 0x0F97BAFB
			elseif pg =="进阶超级泡泡_普通"then
				pgs[6] = 0xE2D684A7
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x3413F529
				pgs[8] = 0x9502C70B
				pgs[5] = 0x5A488708
				pgs[4] = 0x1025669C
				pgs[1]= 0x578E4E20
				pgs[2]= 0xEA07D23E
				pgs[7] = 0x31FE8589
				pgs[3] = 0xC1F40330
			elseif pg =="超级青鸾_普通"then
				pgs[6] = 0xE10AB87A
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x16F3413F
				pgs[8] = 0x43ACA988
				pgs[5] = 0x1E39D673
				pgs[4] = 0xC5CF4D09
				pgs[1]= 0x1ACC1EEA
				pgs[2]= 0x31119883
				pgs[7] = 0x381F5E32
				pgs[3] = 0x46A1DC93
			elseif pg =="进阶超级青鸾_普通"then
				pgs[6] = 0xC1E9A273
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x7013F056
				pgs[8] = 0xC369D5F9
				pgs[5] = 0x35EFB4DC
				pgs[4] = 0x99A6AED4
				pgs[1]= 0xD2494FBC
				pgs[2]= 0x4E372650
				pgs[7] = 0x3CA1C679
				pgs[3] = 0x136F530A
			elseif pg =="超级人参娃娃_普通"then
				pgs[6] = 0xF69A1184
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x749CA3E4
				pgs[8] = 0xA1658C7C
				pgs[5] = 0x00E2342D
				pgs[4] = 0x28AE6BC6
				pgs[1]= 0xD4B8C139
				pgs[2]= 0xF326BA4B
				pgs[7] = 0x1698842F
				pgs[3] = 0x5FDE8EDF---------------
			elseif pg =="进阶超级人参娃娃_普通"then
				pgs[6] = 0xB3BE1374
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x8AF9964F
				pgs[8] = 0xD146D0B8
				pgs[5] = 0x00E2342D
				pgs[4] = 0x4CDC6BB6
				pgs[1]= 0x0C42D1D8
				pgs[2]= 0x000D9783
				pgs[7] = 0x48684A76
				pgs[3] = 0xE7A003F1-------------
			elseif pg =="超级神猴_普通"then
				pgs[6] = 0xCA11BD7C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xD1BDB55B
				pgs[8] = 0x81A2A016
				pgs[5] = 0x752BD56F
				pgs[4] = 0xCDE434CC
				pgs[1]= 0x2F3340FD
				pgs[2]= 0xFE64B9D9
				pgs[7] = 0x3B7793AB
				pgs[3] = 0x2D011B58
			elseif pg =="进阶超级神猴_普通"then
				pgs[6] = 0x5BA0B69C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x70925992
				pgs[8] = 0x29017C5B
				pgs[5] = 0xC6CD1119
				pgs[4] = 0x12BDF324
				pgs[1]= 0xF9233E94
				pgs[2]= 0x562127B8
				pgs[7] = 0xA69436FF
				pgs[3] = 0x28339EC5
			elseif pg =="超级神虎_普通"then
				pgs[6] = 0x56688360
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x5BCADFD4
				pgs[8] = 0x645686AD
				pgs[5] = 0xDA48C108
				pgs[4] = 0x30CA247F
				pgs[1]= 0x121B327B
				pgs[2]= 0xE97AC333
				pgs[7] = 0xD8E9B5FE
				pgs[3] = 0xADC813B9
			elseif pg =="进阶超级神虎_普通"then
				pgs[6] = 0x1174DEC2
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xEAFEAE18
				pgs[8] = 0xDE5B6EB0
				pgs[5] = 0x81E1FDAF
				pgs[4] = 0x9B50048D
				pgs[1]= 0x8452D27F
				pgs[2]= 0xC812DE4B
				pgs[7] = 0x24F80534
				pgs[3] = 0x16F9A3E7
			elseif pg =="超级神鸡_普通"then
				pgs[6] = 0x2BDAF419
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x36BD858F
				pgs[8] = 0x03C55755
				pgs[5] = 0x5B8BE6B6
				pgs[4] = 0x9718CF01
				pgs[1]= 0x15965F51
				pgs[2]= 0x5B6A11D7
				pgs[7] = 0x5601C64B
				pgs[3] = 0x4311585D
			elseif pg =="进阶超级神鸡_普通"then
				pgs[6] = 0x3291C30D
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x070CB2A1
				pgs[8] = 0x82EEF8AD
				pgs[5] = 0xE7847279
				pgs[4] = 0x0F38B4D9
				pgs[1]= 0xFF7095C8
				pgs[2]= 0xB048C29D
				pgs[7] = 0x6F5CB76C
				pgs[3] = 0xF918866B
			elseif pg =="超级神龙_普通"then
				pgs[6] = 0x5949EC31
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x4A238044
				pgs[8] = 0x3B42F8B4
				pgs[5] = 0x165542AE
				pgs[4] = 0x97AD4D51
				pgs[1]= 0x6F1EE22E
				pgs[2]= 0x83D92AE3
				pgs[7] = 0xF49E974E
				pgs[3] = 0x0E167636
			elseif pg =="进阶超级神龙_普通"then
				pgs[6] = 0x5949EC31
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x4A238044
				pgs[8] = 0x3B42F8B4
				pgs[5] = 0x165542AE
				pgs[4] = 0x97AD4D51
				pgs[1]= 0x6F1EE22E
				pgs[2]= 0x83D92AE3
				pgs[7] = 0xF49E974E
				pgs[3] = 0x0E167636
			elseif pg =="超级神马_普通"then
				pgs[6] = 0x29221139
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x56733013
				pgs[8] = 0xA5FA9B14
				pgs[5] = 0x9047C01C
				pgs[4] = 0x45A40E19
				pgs[1]= 0xAD05ACB0
				pgs[2]= 0xDAFF18F4
				pgs[7] = 0x0C62BBCC
				pgs[3] = 0x2E58A599
			elseif pg =="进阶超级神马_普通"then
				pgs[6] = 0x0B9AC09C
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x2803B38C
				pgs[8] = 0xC198AE91
				pgs[5] = 0x327D72A7
				pgs[4] = 0xCDEE504D
				pgs[1]= 0xFAE30A85
				pgs[2]= 0x54E2C4FC
				pgs[7] = 0xBB81A407
				pgs[3] = 0x815821B5
			elseif pg =="超级神牛_普通"then
				pgs[6] = 0x9F7CB714
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x41749269
				pgs[8] = 0xA00C4404
				pgs[5] = 0x340D23D3
				pgs[4] = 0xF2555DE4
				pgs[1]= 0x8D6E23F8
				pgs[2]= 0x97FC7FEC
				pgs[7] = 0xEB2108A7
				pgs[3] = 0xEC48D084
			elseif pg =="进阶超级神牛_普通"then
				pgs[6] = 0xA9C5802F
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x70327CC1
				pgs[8] = 0xBD747215
				pgs[5] = 0xC3F5DFB4
				pgs[4] = 0x7F00FDBB
				pgs[1]= 0x875D93FE
				pgs[2]= 0xB97EBFC0
				pgs[7] = 0xC9B03C6C
				pgs[3] = 0x64043116
			elseif pg =="超级神蛇_普通"then
				pgs[6] = 0x6E041547
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xC87B8D00
				pgs[8] = 0xDD52C93A
				pgs[5] = 0x2D523061
				pgs[4] = 0x683DDDAE
				pgs[1]= 0x3095AC63
				pgs[2]= 0x620DD7B0
				pgs[7] = 0x0D8E10A7
				pgs[3] = 0x7F0704A7
			elseif pg =="进阶超级神蛇_普通"then
				pgs[6] = 0xB94403FE
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x55E342FC
				pgs[8] = 0x9911DED2
				pgs[5] = 0x2B8CCB15
				pgs[4] = 0x6CDDC04D
				pgs[1]= 0x7AD386DF
				pgs[2]= 0xA6BA684D
				pgs[7] = 0xB255F50B
				pgs[3] = 0x7BBEC060
			elseif pg =="超级神兔_普通"then
				pgs[6] = 0x42EEC759
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x126AA146
				pgs[8] = 0x8818F11A
				pgs[5] = 0xDD7B3DDE
				pgs[4] = 0x192B83E6
				pgs[1]= 0x02C946FF
				pgs[2]= 0x242587CA
				pgs[7] = 0xF0BBA8AF
				pgs[3] = 0x302D6564
			elseif pg =="进阶超级神兔_普通"then
				pgs[6] = 0x77845235
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xAEF12C9A
				pgs[8] = 0x54370885
				pgs[5] = 0x3EC0F614
				pgs[4] = 0x3A52E796
				pgs[1]= 0x97F8F299
				pgs[2]= 0x53ECC589
				pgs[7] = 0xF5A92C68
				pgs[3] = 0x487C29A2
			elseif pg =="超级神羊_普通"then
				pgs[6] = 0xCB933F83
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x488007D0
				pgs[8] = 0x4B3532CD
				pgs[5] = 0x6ED89C42
				pgs[4] = 0x89C82925
				pgs[1]= 0x67D9B9A1
				pgs[2]= 0xCD16A816
				pgs[7] = 0x68DC7ED6
				pgs[3] = 0x3C9CAF3D
			elseif pg =="进阶超级神羊_普通"then
				pgs[6] = 0x68BC67E8
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x139C8FF3
				pgs[8] = 0xBF961C95
				pgs[5] = 0x874CFA47
				pgs[4] = 0x4312B448
				pgs[1]= 0x116605E1
				pgs[2]= 0x7E60BB6C
				pgs[7] = 0x26A9EF1F
				pgs[3] = 0xC162B3F3
			elseif pg =="超级腾蛇_普通"then
				pgs[6] = 0x277A9966
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x0ADF554C
				pgs[8] = 0x8CFB271D
				pgs[5] = 0x14D8E03F
				pgs[4] = 0xF4DDF4C7
				pgs[1]= 0x2D55C711
				pgs[2]= 0x2D55C711
				pgs[7] = 0x212B61B8
				pgs[3] = 0x194B0A66
			elseif pg =="超级天马_普通"then
				pgs[6] = 0x0D8CE56B
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x7515DFAA
				pgs[8] = 0x6A27A943
				pgs[5] = 0xB66089D0
				pgs[4] = 0xD9326464
				pgs[1]= 0x4EC9F85A
				pgs[2]= 0xBB661D15
				pgs[7] = 0xF7BAB56F
				pgs[3] = 0xF549E418
			elseif pg =="超级土地公公_普通"then
				pgs[6] = 0xDB917557
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x6B53A718
				pgs[8] = 0x1871EC74
				pgs[5] = 0x0AEC9396
				pgs[4] = 0xEB64BD4C
				pgs[1]= 0x3E6A97CB
				pgs[2]= 0x3E6A97CB
				pgs[7] = 0xE7ABE39C
				pgs[3] = 0xFD2CD3F2
			elseif pg =="进阶超级土地公公_普通"then
				pgs[6] = 0x97026CE8
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x99223ED8
				pgs[8] = 0x1AF0A040
				pgs[5] = 0x05E5DFCB
				pgs[4] = 0xCA94057A
				pgs[1]= 0x25AF3DAA
				pgs[2]= 0xCC98E999
				pgs[7] = 0x47A8D01F
				pgs[3] = 0x2AF1B450
			elseif pg =="超级仙鹿_普通"then
				pgs[6] = 0x746AC442
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xEB9B2D80
				pgs[8] = 0x22518607
				pgs[5] = 0x5C2D78CF
				pgs[4] = 0xFF13CB78
				pgs[1]= 0x25286A3A
				pgs[2]= 0x5959A126
				pgs[7] = 0x676EB44D
				pgs[3] = 0x0EC48D75
			elseif pg =="超级玉兔_普通"then
				pgs[6] = 0x42C85368
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xA0EBB5DA
				pgs[8] = 0x254E8F14
				pgs[5] = 0x5DB85CBF
				pgs[4] = 0xFBF07F14
				pgs[1]= 0xB9EB5837
				pgs[2]= 0x4FE172A9
				pgs[7] = 0xDA0F4915
				pgs[3] = 0x6C8E114F
			elseif pg =="进阶超级玉兔_普通"then
				pgs[6] = 0x5A396A2B
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x5A396A2A
				pgs[8] = 0x5A396A32
				pgs[5] = 0x5A396A31
				pgs[4] = 0x5A396A2D
				pgs[1]= 0x5A396A34
				pgs[2]= 0x5A396A2E
				pgs[7] = 0x5A396A33
				pgs[3] = 0x5A396A2C
			elseif pg =="超级麒麟_普通"then
				pgs[6] = 0xC7EAC3F1
				pgs[10] = "ZHS.FT"
				pgs[9] = 0xBF82889B
				pgs[8] = 0xC120C56F
				pgs[5] = 0x46851BDF
				pgs[4] = 0xB7E3B65C
				pgs[1]= 0xEBE564E7
				pgs[2]= 0xD6F16CB5
				pgs[7] = 0x1F33C964
				pgs[3] = 0x6728CEFF
			elseif pg =="进阶超级麒麟_普通"then
				pgs[6] = 0x481CF06B
				pgs[10] = "ZHS.FT"
				pgs[9] = 0x04B94943
				pgs[8] = 0x3F2F2665
				pgs[5] = 0xE660B37D
				pgs[4] = 0xFE90C7E8
				pgs[1]= 0x8B975642
				pgs[2]= 0x9CEBC627
				pgs[7] = 0x51E4D7C2
				pgs[3] = 0x6CF41D88
   --------------人物
			elseif pg == "竹节双剑_飞燕女_普通" then
				pgs[1] = 1107292387
				pgs[2] = 1107292387
				pgs[3] = 2521369664
				pgs[4] = 3431990852
				pgs[6] = 2150756975
				pgs[7] = 2948306353
				pgs[8] = 3415383948
				pgs[9] = 2723304031
				pgs[10] = "ZY.FT"
			elseif pg == "飞燕女_普通" then
				pgs[1] = 1018070922
				pgs[2] = 1018070922
				pgs[3] = 322750433
				pgs[4] = 29450010
				pgs[6] = 3234089534
				pgs[7] = 2076610755
				pgs[8] = 2826457680
				pgs[9] = 2418368925
				pgs[10] = "ZY.FT"
		    elseif pg == "飞燕女_双" then
				pgs[1] = 2103710679
				pgs[2] = 2103710679
				pgs[3] = 322750433
				pgs[4] = 29450010
				pgs[6] = 3234089534
				pgs[7] = 2076610755
				pgs[8] = 2826457680
				pgs[9] = 2418368925
				pgs[10] = "ZY.FT"
		    elseif pg == "黄铜圈_飞燕女_普通" then
				pgs[1] = 4146113787
				pgs[2] = 4146113787
				pgs[3] = 3328869375
				pgs[4] = 2935267523
				pgs[6] = 2104438938
				pgs[7] = 3418401452
				pgs[8] = 867616434
				pgs[9] = 2580407403
				pgs[10] = "ZY.FT"
		    elseif pg == "蛇形月_飞燕女_普通" then
				pgs[1] = 1584140220
				pgs[2] = 1584140220
				pgs[3] = 1692063004
				pgs[4] = 1463150317
				pgs[6] = 4215251914
				pgs[7] = 3715550978
				pgs[8] = 174531720
				pgs[9] = 913070441
				pgs[10] = "ZY.FT"
		    elseif pg == "双短剑_飞燕女_普通" then
				pgs[1] = 18896713
				pgs[2] = 18896713
				pgs[3] = 2502791430
				pgs[4] = 3950458782
				pgs[6] = 2408807811
				pgs[7] = 3421854459
				pgs[8] = 967461082
				pgs[9] = 372406582
				pgs[10] = "ZY.FT"
		    elseif pg == "金刺轮_飞燕女_普通" then
				pgs[1] = 3720262398
				pgs[2] = 3720262398
				pgs[3] = 1115289817
				pgs[4] = 2525078858
				pgs[6] = 827935432
				pgs[7] = 1403033620
				pgs[8] = 2466319363
				pgs[9] = 113367602
				pgs[10] = "ZY.FT"
		    elseif pg == "祖龙对剑_飞燕女_普通" then
				pgs[1] = 985733706
				pgs[2] = 985733706
				pgs[3] = 3613383230
				pgs[4] = 1544369507
				pgs[6] = 3553066663
				pgs[7] = 4238979964
				pgs[8] = 2190872843
				pgs[9] = 2053572839
				pgs[10] = "ZY.FT"
			elseif pg == "紫电青霜_飞燕女_普通" then
				pgs[1] = 1709377296
				pgs[2] = 1709377296
				pgs[3] = 0883367126
				pgs[4] = 2079837773
				pgs[6] = 2835812814
				pgs[7] = 1289475462
				pgs[8] = 1892940716
				pgs[9] = 1899538113
				pgs[10] = "ZY.FT"
		    elseif pg == "飞燕女_环" then
				pgs[1] = 1018070922
				pgs[2] = 1018070922
				pgs[3] = 2068177574
				pgs[4] = 4111908170
				pgs[6] = 1489146739
				pgs[7] = 607870271
				pgs[8] = 1607593791
				pgs[9] = 3911067979
				pgs[10] = "ZY.FT"
		    elseif pg == "浮犀_飞燕女_普通" then
				pgs[1] = 1474639442
				pgs[2] = 1474639442
				pgs[3] = 3471018206
				pgs[4] = 2084990754
				pgs[6] = 2071526304
				pgs[7] = 1695237386
				pgs[8] = 2750824335
				pgs[9] = 0969736831
				pgs[10] = "ZY.FT"
		    elseif pg == "赤焰双剑_飞燕女_普通" then
				pgs[1] = 1548996612
				pgs[2] = 1548996612
				pgs[3] = 2446905048
				pgs[4] = 3742981382
				pgs[6] = 2519910999
				pgs[7] = 313011222
				pgs[8] = 3245654395
				pgs[9] = 2878513884
				pgs[10] = "ZY.FT"
		    elseif pg == "金玉双环_飞燕女_普通" then
				pgs[1] = 358856899
				pgs[2] = 358856899
				pgs[3] = 1504523359
				pgs[4] = 1837818019
				pgs[6] = 2360002112
				pgs[7] = 1630259287
				pgs[8] = 1285329813
				pgs[9] = 1980919951
				pgs[10] = "ZY.FT"
		    elseif pg == "别情离恨_飞燕女_普通" then
				pgs[1] = 2294614510
				pgs[2] = 2294614510
				pgs[3] = 1412079494
				pgs[4] = 1646305352
				pgs[6] = 2256502232
				pgs[7] = 104447335
				pgs[8] = 471741487
				pgs[9] = 1039274121
				pgs[10] = "ZY.FT"
			elseif pg == "连理双树_飞燕女_普通" then
				pgs[1] = 2798452597
				pgs[2] = 2798452597
				pgs[3] = 2779592090
				pgs[4] = 111849301
				pgs[6] = 3818492150
				pgs[7] = 359904366
				pgs[8] = 981796200
				pgs[9] = 1623427017
				pgs[10] = "ZY.FT"
		    elseif pg == "月光双剑_飞燕女_普通" then
				pgs[1] = 2513982445
				pgs[2] = 2513982445
				pgs[3] = 1641721613
				pgs[4] = 2962390676
				pgs[6] = 586287519
				pgs[7] = 851655841
				pgs[8] = 1872246816
				pgs[9] = 3097886899
				pgs[10] = "ZY.FT"
		    elseif pg == "乾坤_飞燕女_普通" then
				pgs[1] = 3034288425
				pgs[2] = 3034288425
				pgs[3] = 2723272677
				pgs[4] = 1960402927
				pgs[6] = 3867771865
				pgs[7] = 1780449233
				pgs[8] = 967310259
				pgs[9] = 2120714102
				pgs[10] = "ZY.FT"
		    elseif pg == "金龙双剪_飞燕女_普通" then
				pgs[1] = 1411197712
				pgs[2] = 1411197712
				pgs[3] = 4046830583
				pgs[4] = 2149768266
				pgs[6] = 3896276189
				pgs[7] = 1274536324
				pgs[8] = 3865893351
				pgs[9] = 4293096481
				pgs[10] = "ZY.FT"
		    elseif pg == "九天金线_飞燕女_普通" then
				pgs[1] = 256145777
				pgs[2] = 256145777
				pgs[3] = 805152074
				pgs[4] = 612236907
				pgs[6] = 1205883736
				pgs[7] = 4056107702
				pgs[8] = 1146798449
				pgs[9] = 876728636
				pgs[10] = "ZY.FT"
			elseif pg == "无关风月_飞燕女_普通" then
				pgs[1] = 2051866479
				pgs[2] = 2051866479
				pgs[3] = 1785158753
				pgs[4] = 1982666386
				pgs[6] = 3204942337
				pgs[7] = 1654668611
				pgs[8] = 0930614766
				pgs[9] = 3051530981
				pgs[10] = "ZY.FT"
		    elseif pg == "朝夕_飞燕女_普通" then
				pgs[1] = 4138802289
				pgs[2] = 4138802289
				pgs[3] = 0258017396
				pgs[4] = 0480690756
				pgs[6] = 2701132789
				pgs[7] = 3192094510
				pgs[8] = 3576848265
				pgs[9] = 1213207317
				pgs[10] = "ZY.FT"
		    elseif pg == "月光_飞燕女_普通" then
				pgs[1] = 4194305873
				pgs[2] = 4194305873
				pgs[3] = 2807816879
				pgs[4] = 73773078
				pgs[6] = 1041791674
				pgs[7] = 1651978044
				pgs[8] = 2848081881
				pgs[9] = 3069409432
				pgs[10] = "ZY.FT"
			elseif pg == "灵蛇_飞燕女_普通" then
				pgs[1] = 2306957117
				pgs[2] = 2306957117
				pgs[3] = 1793932680
				pgs[4] = 2656017222
				pgs[6] = 3801799119
				pgs[7] = 2677792087
				pgs[8] = 1072683373
				pgs[9] = 2798173689
				pgs[10] = "ZY.FT"
			elseif pg == "如意_飞燕女_普通" then
				pgs[1] = 361357102
				pgs[2] = 361357102
				pgs[3] = 1779832361
				pgs[4] = 1983738335
				pgs[6] = 1018400713
				pgs[7] = 3356485876
				pgs[8] = 1369851554
				pgs[9] = 4083603000
				pgs[10] = "ZY.FT"
		    elseif pg == "阴阳_飞燕女_普通" then
				pgs[1] = 308284096
				pgs[2] = 308284096
				pgs[3] = 1909875299
				pgs[4] = 624581176
				pgs[6] = 2662103332
				pgs[7] = 338180652
				pgs[8] = 2211026380
				pgs[9] = 162524465
				pgs[10] = "ZY.FT"
		     ---------------------------------------------------------------------------------------
			elseif pg == "骨精灵_爪" then
				pgs[1] = 3043670818
				pgs[2] = 3043670818
				pgs[3] = 3644222844
				pgs[4] = 2656336023
				pgs[6] = 4229617854
				pgs[7] = 3601844930
				pgs[8] = 2956176231
				pgs[9] = 4259873334
				pgs[10] = "ZY.FT"
			elseif pg == "青龙牙_骨精灵_普通" then
				pgs[1] = 3799914888
				pgs[2] = 3799914888
				pgs[3] = 1849406648
				pgs[4] = 623293418
				pgs[6] = 4122645564
				pgs[7] = 674755217
				pgs[8] = 1075420620
				pgs[9] = 3643822727
				pgs[10] = "ZY.FT"
		    elseif pg == "点金棒_骨精灵_普通" then
				pgs[1] = 3310814476
				pgs[2] = 3310814476
				pgs[3] = 3103212281
				pgs[4] = 3301564224
				pgs[6] = 1122132964
				pgs[7] = 3107411095
				pgs[8] = 155295757
				pgs[9] = 1580683403
				pgs[10] = "ZY.FT"
		    elseif pg == "铁爪_骨精灵_普通" then
				pgs[1] = 3158218287
				pgs[2] = 3158218287
				pgs[3] = 4058918673
				pgs[4] = 619798639
				pgs[6] = 4166133046
				pgs[7] = 2469680581
				pgs[8] = 1948730791
				pgs[9] = 2320459601
				pgs[10] = "ZY.FT"
		    elseif pg == "细木棒_骨精灵_普通" then
				pgs[1] = 572946160
				pgs[2] = 572946160
				pgs[3] = 869013113
				pgs[4] = 430229213
				pgs[6] = 1202797055
				pgs[7] = 87009475
				pgs[8] = 3532585728
				pgs[9] = 1194431645
				pgs[10] = "ZY.FT"
			elseif pg == "青刚刺_骨精灵_普通" then
				pgs[1] = 3075032207
				pgs[2] = 3075032207
				pgs[3] = 4256318239
				pgs[4] = 3075032207
				pgs[6] = 1414239405
				pgs[7] = 1414239405
				pgs[8] = 2053717070
				pgs[9] = 2785457115
				pgs[10] = "ZY.FT"
		    elseif pg == "骨精灵_普通" then
				pgs[1] = 3043670818
				pgs[2] = 3043670818
				pgs[3] = 3644222844
				pgs[4] = 2656336023
				pgs[6] = 4229617854
				pgs[7] = 3601844930
				pgs[8] = 2956176231
				pgs[9] = 4259873334
				pgs[10] = "ZY.FT"
			elseif pg == "骨精灵_棒" then
				pgs[1] = 2452516043
				pgs[2] = 2452516043
				pgs[3] = 1899323518
				pgs[4] = 3477875588
				pgs[6] = 1063711236
				pgs[7] = 3657445131
				pgs[8] = 2024668974
				pgs[9] = 3508788176
				pgs[10] = "ZY.FT"
			elseif pg == "满天星_骨精灵_普通" then
				pgs[1] = 3437083756
				pgs[2] = 3437083756
				pgs[3] = 2306843235
				pgs[4] = 1714484126
				pgs[6] = 373133398
				pgs[7] = 3805394703
				pgs[8] = 1606121584
				pgs[9] = 1531375263
				pgs[10] = "ZY.FT"
			elseif pg == "雪蚕之刺_骨精灵_普通" then
				pgs[1] = 463147476
				pgs[2] = 463147476
				pgs[3] = 156477563
				pgs[4] = 2554614685
				pgs[6] = 1016195647
				pgs[7] = 1685978750
				pgs[8] = 882510520
				pgs[9] = 3100053953
				pgs[10] = "ZY.FT"
			elseif pg == "撕天_骨精灵_普通" then
				pgs[1] = 1763722445
				pgs[2] = 1763722445
				pgs[3] = 933726342
				pgs[4] = 974601076
				pgs[6] = 311571350
				pgs[7] = 482224323
				pgs[8] = 1316921786
				pgs[9] = 3581381656
				pgs[10] = "ZY.FT"
			elseif pg == "红莲_骨精灵_普通" then
				pgs[1] = 3424827150
				pgs[2] = 3424827150
				pgs[3] = 934671466
				pgs[4] = 1076423299
				pgs[6] = 3574472708
				pgs[7] = 2280284843
				pgs[8] = 506056336
				pgs[9] = 2313738142
				pgs[10] = "ZY.FT"
			elseif pg == "墨玉骷髅_骨精灵_普通" then
				pgs[1] = 577830528
				pgs[2] = 577830528
				pgs[3] = 3266821735
				pgs[4] = 1068099312
				pgs[6] = 4070992100
				pgs[7] = 977905309
				pgs[8] = 2508349179
				pgs[9] = 100402035
				pgs[10] = "ZY.FT"
			elseif pg == "丝萝乔木_骨精灵_普通" then
				pgs[1] = 0901353063
				pgs[2] = 0901353063
				pgs[3] = 2063487357
				pgs[4] = 0147143279
				pgs[6] = 2653363272
				pgs[7] = 1155369624
				pgs[8] = 1510947809
				pgs[9] = 3973054279
				pgs[10] = "ZY.FT"
			elseif pg == "醍醐_骨精灵_普通" then
				pgs[1] = 1250633387
				pgs[2] = 1250633387
				pgs[3] = 2381723311
				pgs[4] = 1282610978
				pgs[6] = 0138367003
				pgs[7] = 1160534304
				pgs[8] = 3907592293
				pgs[9] = 0808638054
				pgs[10] = "ZY.FT"
			elseif pg == "九阴勾魂_骨精灵_普通" then
				pgs[1] = 1703807983
				pgs[2] = 1703807983
				pgs[3] = 1108591457
				pgs[4] = 2771732364
				pgs[6] = 489022779
				pgs[7] = 3267946192
				pgs[8] = 3766522110
				pgs[9] = 2175760076
				pgs[10] = "ZY.FT"
			elseif pg == "青藤玉树_骨精灵_普通" then
				pgs[1] = 3707129187
				pgs[2] = 3707129187
				pgs[3] = 260368700
				pgs[4] = 1260458106
				pgs[6] = 2796632770
				pgs[7] = 1119703569
				pgs[8] = 229416981
				pgs[9] = 2366517522
				pgs[10] = "ZY.FT"
			elseif pg == "贵霜之牙_骨精灵_普通" then
				pgs[1] = 13151833
				pgs[2] = 13151833
				pgs[3] = 3119070731
				pgs[4] = 2925176370
				pgs[6] = 799282094
				pgs[7] = 602955150
				pgs[8] = 2619296391
				pgs[9] = 1165096955
				pgs[10] = "ZY.FT"
			elseif pg == "忘川三途_骨精灵_普通" then
				pgs[1] = 1057196119
				pgs[2] = 1057196119
				pgs[3] = 3284493434
				pgs[4] = 3066925446
				pgs[6] = 0859112470
				pgs[7] = 2663913050
				pgs[8] = 1551227703
				pgs[9] = 2945401960
				pgs[10] = "ZY.FT"
			elseif pg == "离钩_骨精灵_普通" then
				pgs[1] = 4162960663
				pgs[2] = 4162960663
				pgs[3] = 2169364374
				pgs[4] = 0433475784
				pgs[6] = 0158171847
				pgs[7] = 2484616195
				pgs[8] = 1699445520
				pgs[9] = 2751461351
				pgs[10] = "ZY.FT"
			elseif pg == "盘龙_骨精灵_普通" then
				pgs[1] = 399188318
				pgs[2] = 399188318
				pgs[3] = 3336570526
				pgs[4] = 2823302607
				pgs[6] = 4143522053
				pgs[7] = 4066814475
				pgs[8] = 2053873433
				pgs[9] = 4254444118
				pgs[10] = "ZY.FT"
		    elseif pg == "胭脂_骨精灵_普通" then
				pgs[1] = 4115672778
				pgs[2] = 4115672778
				pgs[3] = 2352019632
				pgs[4] = 1274576130
				pgs[6] = 970419487
				pgs[7] = 2008951769
				pgs[8] = 275870055
				pgs[9] = 2737514270
				pgs[10] = "ZY.FT"
		    elseif pg == "沧海_骨精灵_普通" then
				pgs[1] = 3063376651
				pgs[2] = 3063376651
				pgs[3] = 2245942073
				pgs[4] = 3879413923
				pgs[6] = 3414421168
				pgs[7] = 4221563191
				pgs[8] = 3538250889
				pgs[9] = 3779942817
				pgs[10] = "ZY.FT"
			elseif pg == "降魔玉杵_骨精灵_普通" then
				pgs[1] = 3334934404
				pgs[2] = 3334934404
				pgs[3] = 3854049791
				pgs[4] = 1672795955
				pgs[6] = 2227811489
				pgs[7] = 2499485952
				pgs[8] = 265141570
				pgs[9] = 1457344498
				pgs[10] = "ZY.FT"
		    elseif pg == "毒牙_骨精灵_普通" then
				pgs[1] = 991315240
				pgs[2] = 991315240
				pgs[3] = 1625283763
				pgs[4] = 2524067340
				pgs[6] = 1149167732
				pgs[7] = 1113746352
				pgs[8] = 590367663
				pgs[9] = 288632015
				pgs[10] = "ZY.FT"
   ------------------------------------------------------------------------------------
			elseif pg == "无极丝_玄彩娥_普通" then
				pgs[1] = 720570501
				pgs[2] = 720570501
				pgs[3] = 177832508
				pgs[4] = 2487726993
				pgs[6] = 3513269889
				pgs[7] = 629550622
				pgs[8] = 327433169
				pgs[9] = 2643257844
				pgs[10] = "ZY.FT"
			elseif pg == "七彩罗刹_玄彩娥_普通" then
				pgs[1] = 2097271664
				pgs[2] = 2097271664
				pgs[3] = 446065688
				pgs[4] = 34704861
				pgs[6] = 2226391655
				pgs[7] = 4023029297
				pgs[8] = 2658422875
				pgs[9] = 1050015636
				pgs[10] = "ZY.FT"
		    elseif pg == "玄彩娥_棒" then
				pgs[1] = 3947923690
				pgs[2] = 3947923690
				pgs[3] = 1501032219
				pgs[4] = 3870010961
				pgs[6] = 184048410
				pgs[7] = 2162415082
				pgs[8] = 2234123373
				pgs[9] = 2657283590
				pgs[10] = "ZY.FT"
		    elseif pg == "玄彩娥_普通" then
				pgs[1] = 3947923690
				pgs[2] = 3947923690
				pgs[3] = 1501032219
				pgs[4] = 728904944
				pgs[6] = 2291528828
				pgs[7] = 2162415082
				pgs[8] = 2234123373
				pgs[9] = 2657283590
				pgs[10] = "ZY.FT"
		    elseif pg == "满天星_玄彩娥_普通" then
				pgs[1] = 3100078257
				pgs[2] = 3100078257
				pgs[3] = 677178981
				pgs[4] = 4188799764
				pgs[6] = 2373806356
				pgs[7] = 123928187
				pgs[8] = 2280722867
				pgs[9] = 591449018
				pgs[10] = "ZY.FT"
		    elseif pg == "五色缎带_玄彩娥_普通" then
				pgs[1] = 986673291
				pgs[2] = 986673291
				pgs[3] = 2847084631
				pgs[4] = 1814531001
				pgs[6] = 2478192000
				pgs[7] = 3703252301
				pgs[8] = 3541518516
				pgs[9] = 3812334075
				pgs[10] = "ZY.FT"
		    elseif pg == "点金棒_玄彩娥_普通" then
				pgs[1] = 321961419
				pgs[2] = 321961419
				pgs[3] = 1825339978
				pgs[4] = 2854164977
				pgs[6] = 1309940385
				pgs[7] = 3400892076
				pgs[8] = 3042819558
				pgs[9] = 499124570
				pgs[10] = "ZY.FT"
		    elseif pg == "细木棒_玄彩娥_普通" then
				pgs[1] = 2705175188
				pgs[2] = 2705175188
				pgs[3] = 4202680960
				pgs[4] = 3228448399
				pgs[6] = 436522649
				pgs[7] = 2039230457
				pgs[8] = 1506278188
				pgs[9] = 375356336
				pgs[10] = "ZY.FT"
		    elseif pg == "玄彩娥_飘" then
				pgs[1] = 4201959108
				pgs[2] = 4201959108
				pgs[3] = 3290427143
				pgs[4] = 728904944
				pgs[6] = 2291528828
				pgs[7] = 1617545683
				pgs[8] = 339045070
				pgs[9] = 2545689750
				pgs[10] = "ZY.FT"
		    elseif pg == "红莲_玄彩娥_普通" then
				pgs[1] = 3643377339
				pgs[2] = 3643377339
				pgs[3] = 476128501
				pgs[4] = 3322875805
				pgs[6] = 1450950143
				pgs[7] = 1405056236
				pgs[8] = 477554400
				pgs[9] = 1296371001
				pgs[10] = "ZY.FT"
		    elseif pg == "沧海_玄彩娥_普通" then
				pgs[1] = 3065680078
				pgs[2] = 3065680078
				pgs[3] = 504261236
				pgs[4] = 60075072
				pgs[6] = 3170504444
				pgs[7] = 3448269542
				pgs[8] = 1357669355
				pgs[9] = 3608953515
				pgs[10] = "ZY.FT"
			elseif pg == "碧波_玄彩娥_普通" then
				pgs[1] = 970441283
				pgs[2] = 970441283
				pgs[3] = 1029717118
				pgs[4] = 1436245505
				pgs[6] = 3228799230
				pgs[7] = 3957482085
				pgs[8] = 1916346891
				pgs[9] = 1521611307
				pgs[10] = "ZY.FT"
		    elseif pg == "盘龙_玄彩娥_普通" then
				pgs[1] = 3068635068
				pgs[2] = 3068635068
				pgs[3] = 959077202
				pgs[4] = 3616804633
				pgs[6] = 2106636810
				pgs[7] = 555283478
				pgs[8] = 2056912982
				pgs[9] = 2481130669
				pgs[10] = "ZY.FT"
		    elseif pg == "秋水落霞_玄彩娥_普通" then
				pgs[1] = 2834670277
				pgs[2] = 2834670277
				pgs[3] = 3677397747
				pgs[4] = 1773897847
				pgs[6] = 2096851051
				pgs[7] = 2528379760
				pgs[8] = 4035277094
				pgs[9] = 2619283310
				pgs[10] = "ZY.FT"
			elseif pg == "此最相思_玄彩娥_普通" then
				pgs[1] = 193367860
				pgs[2] = 193367860
				pgs[3] = 1702432156
				pgs[4] = 2340791616
				pgs[6] = 493173921
				pgs[7] = 1169690561
				pgs[8] = 2674679688
				pgs[9] = 668529798
				pgs[10] = "ZY.FT"
			elseif pg == "揽月摘星_玄彩娥_普通" then
				pgs[1] = 3957461210
				pgs[2] = 3957461210
				pgs[3] = 1507018653
				pgs[4] = 1507018653
				pgs[6] = 2821704604
				pgs[7] = 3505463631
				pgs[8] = 2825593192
				pgs[9] = 1456089502
				pgs[10] = "ZY.FT"
			elseif pg == "九霄_玄彩娥_普通" then
				pgs[1] = 1925606125
				pgs[2] = 1925606125
				pgs[3] = 1672964794
				pgs[4] = 3074777996
				pgs[6] = 3263307981
				pgs[7] = 1083356098
				pgs[8] = 2475418304
				pgs[9] = 2130671600
				pgs[10] = "ZY.FT"
		    elseif pg == "降魔玉杵_玄彩娥_普通" then
				pgs[1] = 4099217390
				pgs[2] = 4099217390
				pgs[3] = 3206381160
				pgs[4] = 2229271781
				pgs[6] = 2242027872
				pgs[7] = 775886307
				pgs[8] = 3480235058
				pgs[9] = 3182933271
				pgs[10] = "ZY.FT"
		    elseif pg == "流云_玄彩娥_普通" then
				pgs[1] = 2363368052
				pgs[2] = 2363368052
				pgs[3] = 469609710
				pgs[4] = 1588271293
				pgs[6] = 688944433
				pgs[7] = 4012911086
				pgs[8] = 4199981403
				pgs[9] = 459658607
				pgs[10] = "ZY.FT"
		    elseif pg == "彩虹_玄彩娥_普通" then
				pgs[1] = 4010760941
				pgs[2] = 4010760941
				pgs[3] = 3725639444
				pgs[4] = 444072186
				pgs[6] = 1602142769
				pgs[7] = 3326013825
				pgs[8] = 1763662402
				pgs[9] = 2222466647
				pgs[10] = "ZY.FT"
		    elseif pg == "墨玉骷髅_玄彩娥_普通" then
				pgs[1] = 1829371224
				pgs[2] = 1829371224
				pgs[3] = 2002019356
				pgs[4] = 2320316366
				pgs[6] = 2555196614
				pgs[7] = 335647506
				pgs[8] = 2671690470
				pgs[9] = 85118950
				pgs[10] = "ZY.FT"
			elseif pg == "丝萝乔木_玄彩娥_普通" then
				pgs[1] = 1121230155
				pgs[2] = 1121230155
				pgs[3] = 0322946589
				pgs[4] = 4206785453
				pgs[6] = 2777298091
				pgs[7] = 2186653861
				pgs[8] = 1884580054
				pgs[9] = 0740212343
				pgs[10] = "ZY.FT"
			elseif pg == "醍醐_玄彩娥_普通" then
				pgs[1] = 3632446855
				pgs[2] = 3632446855
				pgs[3] = 2786004275
				pgs[4] = 4043816298
				pgs[6] = 0583826926
				pgs[7] = 2781044081
				pgs[8] = 0752278480
				pgs[9] = 0053905909
				pgs[10] = "ZY.FT"
		    elseif pg == "青藤玉树_玄彩娥_普通" then
				pgs[1] = 2503627450
				pgs[2] = 2503627450
				pgs[3] = 3110285636
				pgs[4] = 1992978765
				pgs[6] = 3809375891
				pgs[7] = 2547741955
				pgs[8] = 4282993605
				pgs[9] = 462404473
				pgs[10] = "ZY.FT"
		    elseif pg == "晃金仙绳_玄彩娥_普通" then
				pgs[1] = 2648596978
				pgs[2] = 2648596978
				pgs[3] = 3580927340
				pgs[4] = 2708781818
				pgs[6] = 1007388957
				pgs[7] = 629130124
				pgs[8] = 99182575
				pgs[9] = 359094555
				pgs[10] = "ZY.FT"
    --------------------------------------------------------------------------
			elseif pg == "羽灵神_法杖" then
				pgs[1] = 529648379
				pgs[2] = 529648379
				pgs[3] = 570130129
				pgs[4] = 1552868103
				pgs[6] = 2280958058
				pgs[7] = 4138194582
				pgs[8] = 1198313117
				pgs[9] = 1516641634
				pgs[10] = "ZY.FT"
		    elseif pg == "羽灵神_弓弩" then
				pgs[1] = 1894911422
				pgs[2] = 1894911422
				pgs[3] = 570130129
				pgs[4] = 1552868103
				pgs[6] = 1384231887
				pgs[7] = 3035774294
				pgs[8] = 1198313117
				pgs[9] = 1516641634
				pgs[10] = "ZY.FT"
			elseif pg == "羽灵神_弓弩1" then
				pgs[1] = 0042392635
				pgs[2] = 0042392635
				pgs[3] = 0570130129
				pgs[4] = 1552868103
				pgs[6] = 1139367548
				pgs[7] = 0xB4F23D56
				pgs[8] = 1750053013
				pgs[9] = 1516641634
				pgs[10] = "ZY.FT"
			elseif pg == "腾云杖_羽灵神_普通" then
				pgs[1] = 446004749
				pgs[2] = 446004749
				pgs[3] = 866998113
				pgs[4] = 30654504
				pgs[6] = 3664608621
				pgs[7] = 631837046
				pgs[8] = 3821365630
				pgs[9] = 172627858
				pgs[10] = "ZY.FT"
		    elseif pg == "连珠神弓_羽灵神_普通" then
				pgs[1] = 2326794251
				pgs[2] = 2326794251
				pgs[3] = 2712600528
				pgs[4] = 3987564547
				pgs[6] = 427605991
				pgs[7] = 3545789610
				pgs[8] = 49846298
				pgs[9] = 4190609956
				pgs[10] = "ZY.FT"
		    elseif pg == "墨铁拐_羽灵神_普通" then
				pgs[1] = 581491157
				pgs[2] = 581491157
				pgs[3] = 95568889
				pgs[4] = 4136518094
				pgs[6] = 1603576455
				pgs[7] = 2492027915
				pgs[8] = 616237873
				pgs[9] = 2456783957
				pgs[10] = "ZY.FT"
		    elseif pg == "羽灵神_普通" then
				pgs[1] = 1894911422
				pgs[2] = 1894911422
				pgs[3] = 570130129
				pgs[4] = 1552868103
				pgs[6] = 1384231887
				pgs[7] = 3035774294
				pgs[8] = 1198313117
				pgs[9] = 1516641634
				pgs[10] = "ZY.FT"
		    elseif pg == "宝雕长弓_羽灵神_普通" then
				pgs[1] = 4253393401
				pgs[2] = 4253393401
				pgs[3] = 1615127028
				pgs[4] = 3401409995
				pgs[6] = 2163483216
				pgs[7] = 2874238993
				pgs[8] = 290712464
				pgs[9] = 412551822
				pgs[10] = "ZY.FT"
		    elseif pg == "硬木弓_羽灵神_普通" then
				pgs[1] = 150635600
				pgs[2] = 150635600
				pgs[3] = 1149486021
				pgs[4] = 1352870002
				pgs[6] = 3031812840
				pgs[7] = 2982615098
				pgs[8] = 3723589923
				pgs[9] = 1534728987
				pgs[10] = "ZY.FT"
		    elseif pg == "曲柳杖_羽灵神_普通" then
				pgs[1] = 177392781
				pgs[2] = 177392781
				pgs[3] = 126506799
				pgs[4] = 2086224634
				pgs[6] = 2070674478
				pgs[7] = 2841942625
				pgs[8] = 1310664391
				pgs[9] = 945248214
				pgs[10] = "ZY.FT"
			elseif pg == "冥火薄天_羽灵神_普通" then
				pgs[1] = 2410653859
				pgs[2] = 2410653859
				pgs[3] = 412668278
				pgs[4] = 411982136
				pgs[6] = 2414080025
				pgs[7] = 3270399877
				pgs[8] = 1113080394
				pgs[9] = 2526125934
				pgs[10] = "ZY.FT"
			elseif pg == "雪蟒霜寒_羽灵神_普通" then
				pgs[1] = 2930005782
				pgs[2] = 2930005782
				pgs[3] = 1411447909
				pgs[4] = 1157464245
				pgs[6] = 1798260255
				pgs[7] = 3245955639
				pgs[8] = 1135006044
				pgs[9] = 272976564
				pgs[10] = "ZY.FT"
			elseif pg == "非攻_羽灵神_普通" then
				pgs[1] = 2832976024
				pgs[2] = 2832976024
				pgs[3] = 1185088706
				pgs[4] = 4004334076
				pgs[6] = 3652084138
				pgs[7] = 384985378
				pgs[8] = 2233740441
				pgs[9] = 1204493716
				pgs[10] = "ZY.FT"
			elseif pg == "庄周梦蝶_羽灵神_普通" then
				pgs[1] = 101429091
				pgs[2] = 101429091
				pgs[3] = 1184168411
				pgs[4] = 3432364546
				pgs[6] = 3657330082
				pgs[7] = 548056296
				pgs[8] = 3683860076
				pgs[9] = 1443396752
				pgs[10] = "ZY.FT"
		    elseif pg == "鹿鸣_羽灵神_普通" then
				pgs[1] = 430621291
				pgs[2] = 430621291
				pgs[3] = 3690027526
				pgs[4] = 2690907506
				pgs[6] = 2067206624
				pgs[7] = 3983937939
				pgs[8] = 2646614429
				pgs[9] = 664180130
				pgs[10] = "ZY.FT"
		    elseif pg == "碧海潮生_羽灵神_普通" then
				pgs[1] = 2874500726
				pgs[2] = 2874500726
				pgs[3] = 2974311966
				pgs[4] = 3236853267
				pgs[6] = 2141705747
				pgs[7] = 3946028962
				pgs[8] = 4233351592
				pgs[9] = 2739863852
				pgs[10] = "ZY.FT"
		    elseif pg == "玉辉_羽灵神_普通" then
				pgs[1] = 1799011000
				pgs[2] = 1799011000
				pgs[3] = 3831187618
				pgs[4] = 2997063111
				pgs[6] = 3312955607
				pgs[7] = 3013082841
				pgs[8] = 1939053710
				pgs[9] = 1116805220
				pgs[10] = "ZY.FT"
		    elseif pg == "业焰_羽灵神_普通" then
				pgs[1] = 1471856529
				pgs[2] = 1471856529
				pgs[3] = 1895438642
				pgs[4] = 1769877803
				pgs[6] = 4245874516
				pgs[7] = 572738149
				pgs[8] = 1774544621
				pgs[9] = 565335915
				pgs[10] = "ZY.FT"
		    elseif pg == "龙鸣寒水_羽灵神_普通" then
				pgs[1] = 2288999152
				pgs[2] = 2288999152
				pgs[3] = 3191815629
				pgs[4] = 975203915
				pgs[6] = 3559987378
				pgs[7] = 0xDE5063CD
				pgs[8] = 55816849
				pgs[9] = 1433469896
				pgs[10] = "ZY.FT"
			elseif pg == "弦月_羽灵神_普通" then
				pgs[1] = 4100861245
				pgs[2] = 4100861245
				pgs[3] = 2434737195
				pgs[4] = 2220097459
				pgs[6] = 0862692538
				pgs[7] = 2870088340
				pgs[8] = 1671028156
				pgs[9] = 2129606162
				pgs[10] = "ZY.FT"
		    elseif pg == "太极流光_羽灵神_普通" then
				pgs[1] = 2848033504
				pgs[2] = 2848033504
				pgs[3] = 2656382753
				pgs[4] = 2977776427
				pgs[6] = 1141425103
				pgs[7] = 3968350241
				pgs[8] = 2316321007
				pgs[9] = 104034525
				pgs[10] = "ZY.FT"
			elseif pg == "九霄风雷_羽灵神_普通" then
				pgs[1] = 0158730771
				pgs[2] = 0158730771
				pgs[3] = 0132145415
				pgs[4] = 0764898549
				pgs[6] = 3046428836
				pgs[7] = 0756094913
				pgs[8] = 1838448889
				pgs[9] = 0827920690
				pgs[10] = "ZY.FT"
			elseif pg == "若木_羽灵神_普通" then
				pgs[1] = 4068136814
				pgs[2] = 4068136814
				pgs[3] = 0418458259
				pgs[4] = 3490606442
				pgs[6] = 3417142708
				pgs[7] = 3650939868
				pgs[8] = 3613858643
				pgs[9] = 3612898812
				pgs[10] = "ZY.FT"
		    elseif pg == "幽篁_羽灵神_普通" then
				pgs[1] = 1385345996
				pgs[2] = 1385345996
				pgs[3] = 767991055
				pgs[4] = 2413280637
				pgs[6] = 3907295353
				pgs[7] = 4034909222
				pgs[8] = 590632369
				pgs[9] = 1102812055
				pgs[10] = "ZY.FT"
		    elseif pg == "百鬼_羽灵神_普通" then
				pgs[1] = 433769011
				pgs[2] = 433769011
				pgs[3] = 1388983839
				pgs[4] = 2412124226
				pgs[6] = 3519132595
				pgs[7] = 2744030084
				pgs[8] = 3242785816
				pgs[9] = 4010155361
				pgs[10] = "ZY.FT"
			elseif pg == "凤翼流珠_羽灵神_普通" then
				pgs[1] = 3353149334
				pgs[2] = 3353149334
				pgs[3] = 1074224727
				pgs[4] = 160508292
				pgs[6] = 3400914167
				pgs[7] = 3537311768
				pgs[8] = 1351147384
				pgs[9] = 279371088
				pgs[10] = "ZY.FT"
    ---------------------------------------------------------------------------
			elseif pg == "狼牙锤_虎头怪_普通" then
				pgs[1] = 2073386691
				pgs[2] = 2073386691
				pgs[3] = 336486465
				pgs[4] = 381771912
				pgs[6] = 4106128916
				pgs[7] = 1791439790
				pgs[8] = 568824381
				pgs[9] = 1676981226
				pgs[10] = "ZY.FT"
		    elseif pg == "青铜斧_虎头怪_普通" then
				pgs[1] = 2625820889
				pgs[2] = 2625820889
				pgs[3] = 1963828881
				pgs[4] = 1319228411
				pgs[6] = 860172680
				pgs[7] = 36420174
				pgs[8] = 3821452431
				pgs[9] = 303003892
				pgs[10] = "ZY.FT"
		    elseif pg == "双弦钺_虎头怪_普通" then
				pgs[1] = 3106550770
				pgs[2] = 3106550770
				pgs[3] = 867109572
				pgs[4] = 813505863
				pgs[6] = 4256430594
				pgs[7] = 1073168906
				pgs[8] = 3722911626
				pgs[9] = 1532549372
				pgs[10] = "ZY.FT"
		    elseif pg == "虎头怪_斧" then
				pgs[1] = 2471916192
				pgs[2] = 2471916192
				pgs[3] = 1740983547
				pgs[4] = 1851258377
				pgs[6] = 1927395527
				pgs[7] = 1883982028
				pgs[8] = 2089007572
				pgs[9] = 92383142
				pgs[10] = "ZY.FT"
		    elseif pg == "虎头怪_锤" then
				pgs[1] = 3188334211
				pgs[2] = 3188334211
				pgs[3] = 3845828864
				pgs[4] = 3951179215
				pgs[6] = 1705376638
				pgs[7] = 659922551
				pgs[8] = 3836622392
				pgs[9] = 4274012337
				pgs[10] = "ZY.FT"
		    elseif pg == "松木锤_虎头怪_普通" then
				pgs[1] = 3954765830
				pgs[2] = 3954765830
				pgs[3] = 1463806219
				pgs[4] = 4224177154
				pgs[6] = 78625091
				pgs[7] = 2652829739
				pgs[8] = 2214494438
				pgs[9] = 1060924399
				pgs[10] = "ZY.FT"
			elseif pg == "乌金鬼头镰_虎头怪_普通" then
				pgs[1] = 3466710001
				pgs[2] = 3466710001
				pgs[3] = 2029741881
				pgs[4] = 3433763427
				pgs[6] = 752010966
				pgs[7] = 2871143747
				pgs[8] = 1261420343
				pgs[9] = 2046811187
				pgs[10] = "ZY.FT"
		    elseif pg == "震天锤_虎头怪_普通" then
				pgs[1] = 910543157
				pgs[2] = 910543157
				pgs[3] = 396098490
				pgs[4] = 51606250
				pgs[6] = 647794191
				pgs[7] = 1034863730
				pgs[8] = 34149923
				pgs[9] = 2195214010
				pgs[10] = "ZY.FT"
		    elseif pg == "肃魂_虎头怪_普通" then
				pgs[1] = 3347502793
				pgs[2] = 3347502793
				pgs[3] = 3860676924
				pgs[4] = 118700546
				pgs[6] = 2438511946
				pgs[7] = 654574910
				pgs[8] = 2443863215
				pgs[9] = 3044188142
				pgs[10] = "ZY.FT"
	        elseif pg == "八卦_虎头怪_普通" then
				pgs[1] = 0xF4E93C19
				pgs[2] = 0xF4E93C19
				pgs[3] = 0xDAA88DDF
				pgs[4] = 0xDCA25DA6
				pgs[6] = 0x9EB4376A
				pgs[7] = 0x257EEB28
				pgs[8] = 0x6AD54C39
				pgs[9] = 0xBF887095
				pgs[10] = "ZY.FT"
			 elseif pg == "八卦_神天兵_普通" then
				pgs[1] = 0x94265799
				pgs[2] = 0x14046CB9
				pgs[3] = 0x35A8F0B2
				pgs[4] = 0xF721C3A5
				pgs[5] = 0xCAEC2F8A
				pgs[6] = 0x82407177
				pgs[7] = 0x14046CB9
				pgs[8] = 0x793550FC
				pgs[9] = 0x66D1B6C4
				pgs[10] = "ZY.FT"
			elseif pg == "无敌_虎头怪_普通" then
				pgs[1] = 3570280462
				pgs[2] = 3570280462
				pgs[3] = 4153716432
				pgs[4] = 4066794943
				pgs[6] = 2962716394
				pgs[7] = 2728193647
				pgs[8] = 1018712733
				pgs[9] = 3542032877
				pgs[10] = "ZY.FT"
		    elseif pg == "雷神_虎头怪_普通" then
				pgs[1] = 3666407377
				pgs[2] = 3666407377
				pgs[3] = 465270733
				pgs[4] = 2173835439
				pgs[6] = 1254874348
				pgs[7] = 2506000487
				pgs[8] = 3727411622
				pgs[9] = 2103867801
				pgs[10] = "ZY.FT"
			elseif pg == "虎头怪_普通" then
				pgs[1] = 3188334211
				pgs[2] = 3188334211
				pgs[3] = 3845828864
				pgs[4] = 3951179215
				pgs[6] = 1705376638
				pgs[7] = 659922551
				pgs[8] = 2089007572
				pgs[9] = 4274012337
				pgs[10] = "ZY.FT"
		    elseif pg == "九瓣莲花_虎头怪_普通" then
				pgs[1] = 4211862022
				pgs[2] = 4211862022
				pgs[3] = 3700497436
				pgs[4] = 2579634951
				pgs[6] = 3976648204
				pgs[7] = 2765994764
				pgs[8] = 3401808842
				pgs[9] = 4094340569
				pgs[10] = "ZY.FT"
		    elseif pg == "元神禁锢_虎头怪_普通" then
				pgs[1] = 345884366
				pgs[2] = 345884366
				pgs[3] = 1989535927
				pgs[4] = 3684415104
				pgs[6] = 3779343772
				pgs[7] = 1164144313
				pgs[8] = 2870850177
				pgs[9] = 1485270944
				pgs[10] = "ZY.FT"
		    elseif pg == "护法灭魔_虎头怪_普通" then
				pgs[1] = 2333757160
				pgs[2] = 2333757160
				pgs[3] = 608625932
				pgs[4] = 1240918279
				pgs[6] = 4006191476
				pgs[7] = 291325187
				pgs[8] = 1586774719
				pgs[9] = 2726510090
				pgs[10] = "ZY.FT"
			elseif pg == "碧血干戚_虎头怪_普通" then
				pgs[1] = 2522616044
				pgs[2] = 2522616044
				pgs[3] = 3551561984
				pgs[4] = 4132910073
				pgs[6] = 2916654539
				pgs[7] = 1375729749
				pgs[8] = 3235367936
				pgs[9] = 2444186613
				pgs[10] = "ZY.FT"
			elseif pg == "裂天_虎头怪_普通" then
				pgs[1] = 3605525759
				pgs[2] = 3605525759
				pgs[3] = 1658258788
				pgs[4] = 1428790516
				pgs[6] = 1927582914
				pgs[7] = 0157199347
				pgs[8] = 2018235479
				pgs[9] = 0184731195
				pgs[10] = "ZY.FT"
		    elseif pg == "鬼牙_虎头怪_普通" then
				pgs[1] = 3191385649
				pgs[2] = 3191385649
				pgs[3] = 964246481
				pgs[4] = 439390498
				pgs[6] = 2557976454
				pgs[7] = 899308703
				pgs[8] = 1809962467
				pgs[9] = 1310176885
				pgs[10] = "ZY.FT"
		    elseif pg == "鬼王蚀日_虎头怪_普通" then
				pgs[1] = 2268846765
				pgs[2] = 2268846765
				pgs[3] = 257733900
				pgs[4] = 733650393
				pgs[6] = 2155760337
				pgs[7] = 3690009014
				pgs[8] = 3223595996
				pgs[9] = 553446556
				pgs[10] = "ZY.FT"
			elseif pg == "狂澜碎岳_虎头怪_普通" then
				pgs[1] = 0843924172
				pgs[2] = 0843924172
				pgs[3] = 3497818294
				pgs[4] = 0119723529
				pgs[6] = 2748969191
				pgs[7] = 1323769114
				pgs[8] = 1279428078
				pgs[9] = 1811551067
				pgs[10] = "ZY.FT"
			elseif pg == "碎寂_虎头怪_普通" then
				pgs[1] = 3620927202
				pgs[2] = 3620927202
				pgs[3] = 1460939045
				pgs[4] = 3684775387
				pgs[6] = 3894472815
				pgs[7] = 0534677085
				pgs[8] = 1857702941
				pgs[9] = 3336710114
				pgs[10] = "ZY.FT"
		    elseif pg == "混元金锤_虎头怪_普通" then
				pgs[1] = 3310158404
				pgs[2] = 3310158404
				pgs[3] = 3916719461
				pgs[4] = 2256126103
				pgs[6] = 1047119087
				pgs[7] = 649007137
				pgs[8] = 3287631002
				pgs[9] = 3272074169
				pgs[10] = "ZY.FT"
		    elseif pg == "五丁开山_虎头怪_普通" then
				pgs[1] = 2062014821
				pgs[2] = 2062014821
				pgs[3] = 2363797327
				pgs[4] = 3661893622
				pgs[6] = 1889040253
				pgs[7] = 1541320741
				pgs[8] = 1738256578
				pgs[9] = 722835269
				pgs[10] = "ZY.FT"
		    elseif pg == "破魄_虎头怪_普通" then
				pgs[1] = 66503730
				pgs[2] = 66503730
				pgs[3] = 2454517146
				pgs[4] = 147936745
				pgs[6] = 915959107
				pgs[7] = 2946347461
				pgs[8] = 1892595080
				pgs[9] = 1320517389
				pgs[10] = "ZY.FT"
    ---------------------------------------------------------------------------------------
			elseif pg == "腾云杖_巫蛮儿_普通" then
				pgs[1] = 3456418962
				pgs[2] = 3456418962
				pgs[3] = 1443524884
				pgs[4] = 2407737199
				pgs[6] = 4073450724
				pgs[7] = 4073450724
				pgs[8] = 2828185705
				pgs[9] = 2088694658
				pgs[10] = "ZY.FT"
		    elseif pg == "翡翠珠_巫蛮儿_普通" then
				pgs[1] = 3539296504
				pgs[2] = 3539296504
				pgs[3] = 2255315467
				pgs[4] = 1802143462
				pgs[6] = 1510770330
				pgs[7] = 1930701898
				pgs[8] = 3505690179
				pgs[9] = 1435774063
				pgs[10] = "ZY.FT"
		    elseif pg == "琉璃珠_巫蛮儿_普通" then
				pgs[1] = 1403236209
				pgs[2] = 1403236209
				pgs[3] = 1329700091
				pgs[4] = 1691312599
				pgs[6] = 786593580
				pgs[7] = 164320860
				pgs[8] = 2463504053
				pgs[9] = 2573416526
				pgs[10] = "ZY.FT"
			elseif pg == "如意宝珠_巫蛮儿_普通" then
				pgs[1] = 3047110383
				pgs[2] = 3047110383
				pgs[3] = 2280873549
				pgs[4] = 2717151389
				pgs[6] = 3043609869
				pgs[7] = 3638266331
				pgs[8] = 2356120530
				pgs[9] = 62625934
				pgs[10] = "ZY.FT"
		    elseif pg == "巫蛮儿_宝珠" then
				pgs[1] = 724036542
				pgs[2] = 724036542
				pgs[3] = 857427883
				pgs[4] = 1487210709
				pgs[6] = 2600092653
				pgs[7] = 4235120544
				pgs[8] = 3377965731
				pgs[9] = 170880794
				pgs[10] = "ZY.FT"
		    elseif pg == "墨铁拐_巫蛮儿_普通" then
				pgs[1] = 333733022
				pgs[2] = 333733022
				pgs[3] = 923276916
				pgs[4] = 1683281266
				pgs[6] = 3200804307
				pgs[7] = 2976979233
				pgs[8] = 160002331
				pgs[9] = 1565301941
				pgs[10] = "ZY.FT"
		    elseif pg == "巫蛮儿_法杖" then
				pgs[1] = 2404345852
				pgs[2] = 2404345852
				pgs[3] = 857427883
				pgs[4] = 3299007490
				pgs[6] = 3847342356
				pgs[7] = 2219256477
				pgs[8] = 3377965731
				pgs[9] = 1458812347
				pgs[10] = "ZY.FT"
		    elseif pg == "巫蛮儿_普通" then
				pgs[1] = 2404345852
				pgs[2] = 2404345852
				pgs[3] = 857427883
				pgs[4] = 3299007490
				pgs[6] = 3847342356
				pgs[7] = 2219256477
				pgs[8] = 3377965731
				pgs[9] = 1458812347
				pgs[10] = "ZY.FT"
		    elseif pg == "曲柳杖_巫蛮儿_普通" then
				pgs[1] = 3570910020
				pgs[2] = 3570910020
				pgs[3] = 923276916
				pgs[4] = 1683281266
				pgs[6] = 3312479705
				pgs[7] = 429861263
				pgs[8] = 3316485994
				pgs[9] = 1090261872
				pgs[10] = "ZY.FT"
		    elseif pg == "裂云啸日_巫蛮儿_普通" then
				pgs[1] = 1953175087
				pgs[2] = 1953175087
				pgs[3] = 3696783624
				pgs[4] = 2606125215
				pgs[6] = 1758916755
				pgs[7] = 3089221190
				pgs[8] = 147933165
				pgs[9] = 3171772535
				pgs[10] = "ZY.FT"
			elseif pg == "云雷万里_巫蛮儿_普通" then
				pgs[1] = 3496291125
				pgs[2] = 3496291125
				pgs[3] = 1465968075
				pgs[4] = 1397554648
				pgs[6] = 2459508456
				pgs[7] = 2516514287
				pgs[8] = 3589170230
				pgs[9] = 3317700918
				pgs[10] = "ZY.FT"
			elseif pg == "赤明_巫蛮儿_普通" then
				pgs[1] = 0200658060
				pgs[2] = 0200658060
				pgs[3] = 3759594662
				pgs[4] = 3318654178
				pgs[6] = 4035253624
				pgs[7] = 2693415826
				pgs[8] = 2972224698
				pgs[9] = 3906907508
				pgs[10] = "ZY.FT"
		    elseif pg == "离火_巫蛮儿_普通" then
				pgs[1] = 593251101
				pgs[2] = 593251101
				pgs[3] = 1674828781
				pgs[4] = 83754672
				pgs[6] = 3337153292
				pgs[7] = 341330281
				pgs[8] = 662302587
				pgs[9] = 1380161976
				pgs[10] = "ZY.FT"
		    elseif pg == "紫金葫芦_巫蛮儿_普通" then
				pgs[1] = 2831632905
				pgs[2] = 2831632905
				pgs[3] = 3521304774
				pgs[4] = 3768453514
				pgs[6] = 2981305233
				pgs[7] = 2400756176
				pgs[8] = 1889441810
				pgs[9] = 2164075551
				pgs[10] = "ZY.FT"
		    elseif pg == "雪蟒霜寒_巫蛮儿_普通" then
				pgs[1] = 739148944
				pgs[2] = 739148944
				pgs[3] = 1573479582
				pgs[4] = 1491739579
				pgs[6] = 851932811
				pgs[7] = 3605348497
				pgs[8] = 1658854000
				pgs[9] = 759781703
				pgs[10] = "ZY.FT"
			elseif pg == "碧海潮生_巫蛮儿_普通" then
				pgs[1] = 3910342400
				pgs[2] = 3910342400
				pgs[3] = 0733894543
				pgs[4] = 1050839754
				pgs[6] = 3505371915
				pgs[7] = 1647388270
				pgs[8] = 1832777753
				pgs[9] = 0844025414
				pgs[10] = "ZY.FT"
			elseif pg == "弦月_巫蛮儿_普通" then
				pgs[1] = 3736877850
				pgs[2] = 3736877850
				pgs[3] = 3588393432
				pgs[4] = 2220097459
				pgs[6] = 0280199730
				pgs[7] = 3405371659
				pgs[8] = 2586240981
				pgs[9] = 0420838884
				pgs[10] = "ZY.FT"
		    elseif pg == "飞星_巫蛮儿_普通" then
				pgs[1] = 902532881
				pgs[2] = 902532881
				pgs[3] = 1311875894
				pgs[4] = 1263683269
				pgs[6] = 4006180572
				pgs[7] = 941967075
				pgs[8] = 1752683572
				pgs[9] = 173425117
				pgs[10] = "ZY.FT"
			elseif pg == "回风舞雪_巫蛮儿_普通" then
				pgs[1] = 1226070803
				pgs[2] = 1226070803
				pgs[3] = 3590109632
				pgs[4] = 3593543610
				pgs[6] = 3882426852
				pgs[7] = 136839080
				pgs[8] = 1265716525
				pgs[9] = 126040992
				pgs[10] = "ZY.FT"
		    elseif pg == "庄周梦蝶_巫蛮儿_普通" then
				pgs[1] = 2699629829
				pgs[2] = 2699629829
				pgs[3] = 19941067
				pgs[4] = 2214888719
				pgs[6] = 2405414221
				pgs[7] = 3124365588
				pgs[8] = 181704645
				pgs[9] = 1598617101
				pgs[10] = "ZY.FT"
		    elseif pg == "玉辉_巫蛮儿_普通" then
				pgs[1] = 2197847487
				pgs[2] = 2197847487
				pgs[3] = 647814036
				pgs[4] = 643336377
				pgs[6] = 3288644239
				pgs[7] = 869836664
				pgs[8] = 2936127898
				pgs[9] = 2134751551
				pgs[10] = "ZY.FT"
		    elseif pg == "业焰_巫蛮儿_普通" then
				pgs[1] = 358788281
				pgs[2] = 358788281
				pgs[3] = 3187541069
				pgs[4] = 1276652632
				pgs[6] = 746435059
				pgs[7] = 1492176534
				pgs[8] = 4078550197
				pgs[9] = 2864410571
				pgs[10] = "ZY.FT"
			elseif pg == "凤翼流珠_巫蛮儿_普通" then
				pgs[1] = 2005711219
				pgs[2] = 2005711219
				pgs[3] = 1777115444
				pgs[4] = 738268467
				pgs[6] = 3554514707
				pgs[7] = 2166949971
				pgs[8] = 2242679788
				pgs[9] = 3110874208
				pgs[10] = "ZY.FT"
		    elseif pg == "鹿鸣_巫蛮儿_普通" then
				pgs[1] = 2923599993
				pgs[2] = 2923599993
				pgs[3] = 862900418
				pgs[4] = 2806977290
				pgs[6] = 2678464587
				pgs[7] = 893469617
				pgs[8] = 3689245011
				pgs[9] = 780797617
				pgs[10] = "ZY.FT"
			elseif pg == "月华_巫蛮儿_普通" then
				pgs[1] = 1792811676
				pgs[2] = 1792811676
				pgs[3] = 4148391824
				pgs[4] = 3636515320
				pgs[6] = 3355609993
				pgs[7] = 4240539050
				pgs[8] = 3498546662
				pgs[9] = 2176161640
				pgs[10] = "ZY.FT"
    ----------------------------------------------------------------------------------------------------
			elseif pg == "逍遥生_普通" then
				pgs[1] = 776342191
				pgs[2] = 776342191
				pgs[3] = 2305122459
				pgs[4] = 4097908221
				pgs[6] = 947396698
				pgs[7] = 3952279767
				pgs[8] = 1198567883
				pgs[9] = 1011046408
				pgs[10] = "ZY.FT"
		    elseif pg == "青铜短剑_逍遥生_普通" then
				pgs[1] = 1198108472
				pgs[2] = 1198108472
				pgs[3] = 1361263285
				pgs[4] = 2271925935
				pgs[6] = 2946567894
				pgs[7] = 3149178979
				pgs[8] = 3990213912
				pgs[9] = 3929526014
				pgs[10] = "ZY.FT"
		    elseif pg == "逍遥生_剑" then
				pgs[1] = 2801151609
				pgs[2] = 2801151609
				pgs[3] = 1782439566
				pgs[4] = 3738039458
				pgs[6] = 1629704855
				pgs[7] = 1539613628
				pgs[8] = 1512265752
				pgs[9] = 116130982
				pgs[10] = "ZY.FT"
		    elseif pg == "折扇_逍遥生_普通" then
				pgs[1] = 2964614373
				pgs[2] = 2964614373
				pgs[3] = 3257923907
				pgs[4] = 293936349
				pgs[6] = 1375158316
				pgs[7] = 3094890393
				pgs[8] = 1511168433
				pgs[9] = 391725534
				pgs[10] = "ZY.FT"
		    elseif pg == "逍遥生_扇" then
				pgs[1] = 776342191
				pgs[2] = 776342191
				pgs[3] = 2305122459
				pgs[4] = 4097908221
				pgs[6] = 947396698
				pgs[7] = 3952279767
				pgs[8] = 1198567883
				pgs[9] = 1011046408
				pgs[10] = "ZY.FT"
		    elseif pg == "青锋剑_逍遥生_普通" then
				pgs[1] = 2028105319
				pgs[2] = 2028105319
				pgs[3] = 3791928171
				pgs[4] = 2088494889
				pgs[6] = 91578554
				pgs[7] = 518706841
				pgs[8] = 3909235980
				pgs[9] = 526326278
				pgs[10] = "ZY.FT"
		    elseif pg == "铁面扇_逍遥生_普通" then
				pgs[1] = 265930064
				pgs[2] = 265930064
				pgs[3] = 1315781189
				pgs[4] = 851934430
				pgs[6] = 3398365059
				pgs[7] = 1327574185
				pgs[8] = 3720853183
				pgs[9] = 2140890725
				pgs[10] = "ZY.FT"
		    elseif pg == "神火扇_逍遥生_普通" then
				pgs[1] = 809466984
				pgs[2] = 809466984
				pgs[3] = 3900102624
				pgs[4] = 1484450534
				pgs[6] = 1946292417
				pgs[7] = 2773394627
				pgs[8] = 3033319609
				pgs[9] = 815459690
				pgs[10] = "ZY.FT"
		    elseif pg == "游龙剑_逍遥生_普通" then
				pgs[1] = 2298557178
				pgs[2] = 2298557178
				pgs[3] = 923259950
				pgs[4] = 145374235
				pgs[6] = 4267993677
				pgs[7] = 3641474775
				pgs[8] = 345371692
				pgs[9] = 551366747
				pgs[10] = "ZY.FT"
		    elseif pg == "玉龙_逍遥生_普通" then
				pgs[1] = 291599680
				pgs[2] = 291599680
				pgs[3] = 715603823
				pgs[4] = 1026330223
				pgs[6] = 3159923026
				pgs[7] = 2920506479
				pgs[8] = 2112187507
				pgs[9] = 3163375922
				pgs[10] = "ZY.FT"
		    elseif pg == "魏武青虹_逍遥生_普通" then
				pgs[1] = 2954647770
				pgs[2] = 2954647770
				pgs[3] = 1287899115
				pgs[4] = 3269954632
				pgs[6] = 2567725998
				pgs[7] = 2862658372
				pgs[8] = 4023660358
				pgs[9] = 1267402087
				pgs[10] = "ZY.FT"
		    elseif pg == "鱼肠_逍遥生_普通" then
				pgs[1] = 111752242
				pgs[2] = 111752242
				pgs[3] = 2779964916
				pgs[4] = 3099677412
				pgs[6] = 1315883113
				pgs[7] = 4096670906
				pgs[8] = 3380575361
				pgs[9] = 3347297766
				pgs[10] = "ZY.FT"
		    elseif pg == "霜冷九州_逍遥生_普通" then
				pgs[1] = 3500923187
				pgs[2] = 3500923187
				pgs[3] = 3419416515
				pgs[4] = 4121135422
				pgs[6] = 1766329695
				pgs[7] = 1208240575
				pgs[8] = 2948332373
				pgs[9] = 0569489195
				pgs[10] = "ZY.FT"
		    elseif pg == "擒龙_逍遥生_普通" then
				pgs[1] = 2313480743
				pgs[2] = 2313480743
				pgs[3] = 1394178168
				pgs[4] = 3931344485
				pgs[6] = 4049254285
				pgs[7] = 1046891202
				pgs[8] = 2147278343
				pgs[9] = 4088283715
				pgs[10] = "ZY.FT"
		    elseif pg == "倚天_逍遥生_普通" then
				pgs[1] = 944530413
				pgs[2] = 944530413
				pgs[3] = 4288693369
				pgs[4] = 2648862354
				pgs[6] = 893536575
				pgs[7] = 3580840293
				pgs[8] = 1513199932
				pgs[9] = 1959183902
				pgs[10] = "ZY.FT"
		    elseif pg == "湛卢_逍遥生_普通" then
				pgs[1] = 4122820611
				pgs[2] = 4122820611
				pgs[3] = 39942374
				pgs[4] = 3784693035
				pgs[6] = 3879135399
				pgs[7] = 1799718291
				pgs[8] = 2656683712
				pgs[9] = 356672046
				pgs[10] = "ZY.FT"
		    elseif pg == "画龙点睛_逍遥生_普通" then
				pgs[1] = 1820963257
				pgs[2] = 1820963257
				pgs[3] = 177826354
				pgs[4] = 847585063
				pgs[6] = 2168341927
				pgs[7] = 2999351206
				pgs[8] = 2759214368
				pgs[9] = 3485764253
				pgs[10] = "ZY.FT"
		    elseif pg == "太极_逍遥生_普通" then
				pgs[1] = 649896254
				pgs[2] = 649896254
				pgs[3] = 1386995615
				pgs[4] = 2206939775
				pgs[6] = 4084370231
				pgs[7] = 730224004
				pgs[8] = 2110134716
				pgs[9] = 297375576
				pgs[10] = "ZY.FT"
		    elseif pg == "秋风_逍遥生_普通" then
				pgs[1] = 2946234719
				pgs[2] = 2946234719
				pgs[3] = 918869516
				pgs[4] = 913597263
				pgs[6] = 1906683947
				pgs[7] = 3000647151
				pgs[8] = 3901133072
				pgs[9] = 1776219114
				pgs[10] = "ZY.FT"
		    elseif pg == "四法青云_逍遥生_普通" then
				pgs[1] = 3016226515
				pgs[2] = 3016226515
				pgs[3] = 52451636
				pgs[4] = 1691019474
				pgs[6] = 2058662475
				pgs[7] = 1670645347
				pgs[8] = 372928672
				pgs[9] = 3475794618
				pgs[10] = "ZY.FT"
		    elseif pg == "秋水人家_逍遥生_普通" then
				pgs[1] = 3722556358
				pgs[2] = 3722556358
				pgs[3] = 1425493699
				pgs[4] = 817486875
				pgs[6] = 2119665882
				pgs[7] = 79540806
				pgs[8] = 1912450629
				pgs[9] = 3131282288
				pgs[10] = "ZY.FT"
		    elseif pg == "逍遥江湖_逍遥生_普通" then
				pgs[1] = 3459742955
				pgs[2] = 3459742955
				pgs[3] = 1723293957
				pgs[4] = 4224277364
				pgs[6] = 1818516162
				pgs[7] = 3783711264
				pgs[8] = 3566337709
				pgs[9] = 2650676483
				pgs[10] = "ZY.FT"
			elseif pg == "浩气长舒_逍遥生_普通" then
				pgs[1] = 1787687382
				pgs[2] = 1787687382
				pgs[3] = 2437076334
				pgs[4] = 2007103844
				pgs[6] = 1976544695
				pgs[7] = 1456930860
				pgs[8] = 2385855153
				pgs[9] = 3599367465
				pgs[10] = "ZY.FT"
		    elseif pg == "灵犀神剑_逍遥生_普通" then
				pgs[1] = 4048939390
				pgs[2] = 4048939390
				pgs[3] = 2956972471
				pgs[4] = 91663105
				pgs[6] = 1499210091
				pgs[7] = 3108724735
				pgs[8] = 3464352844
				pgs[9] = 1984805790
				pgs[10] = "ZY.FT"
			elseif pg == "星瀚_逍遥生_普通" then
				pgs[1] = 2809028406
				pgs[2] = 2809028406
				pgs[3] = 3517798323
				pgs[4] = 0635822686
				pgs[6] = 4199298440
				pgs[7] = 4100767959
				pgs[8] = 3470836750
				pgs[9] = 0649411872
				pgs[10] = "ZY.FT"
		    elseif pg == "擒龙_逍遥生_普通" then
				pgs[1] = 2313480743
				pgs[2] = 2313480743
				pgs[3] = 1394178168
				pgs[4] = 3931344485
				pgs[6] = 4049254285
				pgs[7] = 1046891202
				pgs[8] = 2147278343
				pgs[9] = 4088283715
				pgs[10] = "ZY.FT"
    -----------------------------------------------------------------------------------------
			elseif pg == "龙太子_扇" then
				pgs[1] = 241140809
				pgs[2] = 241140809
				pgs[3] = 1280216665
				pgs[4] = 3101694266
				pgs[6] = 2034116134
				pgs[7] = 2603954881
				pgs[8] = 4232546957
				pgs[9] = 3966136859
				pgs[10] = "ZY.FT"
		    elseif pg == "神火扇_龙太子_普通" then
				pgs[1] = 1818358093
				pgs[2] = 1818358093
				pgs[3] = 3568094185
				pgs[4] = 1584219148
				pgs[6] = 3409145103
				pgs[7] = 666236837
				pgs[8] = 3980290410
				pgs[9] = 1171511807
				pgs[10] = "ZY.FT"
		    elseif pg == "折扇_龙太子_普通" then
				pgs[1] = 1892693347
				pgs[2] = 1892693347
				pgs[3] = 3257923907
				pgs[4] = 1204995981
				pgs[6] = 1362359639
				pgs[7] = 2228831626
				pgs[8] = 3633790550
				pgs[9] = 80506382
				pgs[10] = "ZY.FT"
		    elseif pg == "龙太子_普通" then
				pgs[1] = 995698620
				pgs[2] = 995698620
				pgs[3] = 2482790664
				pgs[4] = 708419920
				pgs[6] = 397538195
				pgs[7] = 1042710685
				pgs[8] = 1284179344
				pgs[9] = 1173564955
				pgs[10] = "ZY.FT"
		    elseif pg == "玄铁矛_龙太子_普通" then
				pgs[1] = 650631390
				pgs[2] = 650631390
				pgs[3] = 1332270280
				pgs[4] = 1220141886
				pgs[6] = 407764697
				pgs[7] = 477458607
				pgs[8] = 4281686609
				pgs[9] = 663869830
				pgs[10] = "ZY.FT"
		    elseif pg == "龙太子_枪" then
				pgs[1] = 995698620
				pgs[2] = 995698620
				pgs[3] = 2482790664
				pgs[4] = 708419920
				pgs[6] = 397538195
				pgs[7] = 1042710685
				pgs[8] = 1284179344
				pgs[9] = 1173564955
				pgs[10] = "ZY.FT"
		    elseif pg == "红缨枪_龙太子_普通" then
				pgs[1] = 4208641994
				pgs[2] = 4208641994
				pgs[3] = 3367459571
				pgs[4] = 4155725796
				pgs[6] = 170841557
				pgs[7] = 2694226760
				pgs[8] = 1612726780
				pgs[9] = 1217969354
				pgs[10] = "ZY.FT"
		    elseif pg == "乌金三叉戟_龙太子_普通" then
				pgs[1] = 2664793060
				pgs[2] = 2664793060
				pgs[3] = 1779222969
				pgs[4] = 1212846725
				pgs[6] = 3544733296
				pgs[7] = 2097787361
				pgs[8] = 2856910184
				pgs[9] = 2573475492
				pgs[10] = "ZY.FT"
		    elseif pg == "铁面扇_龙太子_普通" then
				pgs[1] = 1892693347
				pgs[2] = 1892693347
				pgs[3] = 3257923907
				pgs[4] = 1204995981
				pgs[6] = 1362359639
				pgs[7] = 2228831626
				pgs[8] = 3633790550
				pgs[9] = 80506382
				pgs[10] = "ZY.FT"
		    elseif pg == "秋水人家_龙太子_普通" then
				pgs[1] = 3391613661
				pgs[2] = 3391613661
				pgs[3] = 1065383079
				pgs[4] = 2120876772
				pgs[6] = 2647082872
				pgs[7] = 1201013296
				pgs[8] = 1941233906
				pgs[9] = 817870297
				pgs[10] = "ZY.FT"
		    elseif pg == "刑天之逆_龙太子_普通" then
				pgs[1] = 1200609263
				pgs[2] = 1200609263
				pgs[3] = 2005252098
				pgs[4] = 3509150743
				pgs[6] = 3445110785
				pgs[7] = 951231487
				pgs[8] = 220558504
				pgs[9] = 4062233204
				pgs[10] = "ZY.FT"
			elseif pg == "霹雳_龙太子_普通" then
				pgs[1] = 278992607
				pgs[2] = 278992607
				pgs[3] = 1512127783
				pgs[4] = 3642679990
				pgs[6] = 1313071015
				pgs[7] = 3971560979
				pgs[8] = 2173299537
				pgs[9] = 4060439247
				pgs[10] = "ZY.FT"
		    elseif pg == "画龙点睛_龙太子_普通" then
				pgs[1] = 479654062
				pgs[2] = 479654062
				pgs[3] = 2783759891
				pgs[4] = 2828288868
				pgs[6] = 2229841293
				pgs[7] = 235465927
				pgs[8] = 3332787791
				pgs[9] = 1018236421
				pgs[10] = "ZY.FT"
		    elseif pg == "五虎断魂_龙太子_普通" then
				pgs[1] = 1272648373
				pgs[2] = 1272648373
				pgs[3] = 1067074679
				pgs[4] = 1592032597
				pgs[6] = 2132267301
				pgs[7] = 4089962192
				pgs[8] = 1673526656
				pgs[9] = 2635610460
				pgs[10] = "ZY.FT"
		    elseif pg == "暗夜_龙太子_普通" then
				pgs[1] = 741288949
				pgs[2] = 741288949
				pgs[3] = 1972123841
				pgs[4] = 2182082171
				pgs[6] = 4066915831
				pgs[7] = 1492298468
				pgs[8] = 3678212997
				pgs[9] = 1751076384
				pgs[10] = "ZY.FT"
		    elseif pg == "逍遥江湖_龙太子_普通" then
				pgs[1] = 346935725
				pgs[2] = 346935725
				pgs[3] = 91893473
				pgs[4] = 4293635787
				pgs[6] = 4152040342
				pgs[7] = 3271976691
				pgs[8] = 3980106552
				pgs[9] = 2687029336
				pgs[10] = "ZY.FT"
			elseif pg == "玉龙_龙太子_普通" then
				pgs[1] = 2333774279
				pgs[2] = 2333774279
				pgs[3] = 1147744590
				pgs[4] = 444659320
				pgs[6] = 629954791
				pgs[7] = 263378940
				pgs[8] = 1570393999
				pgs[9] = 862461187
				pgs[10] = "ZY.FT"
			elseif pg == "浩气长舒_龙太子_普通" then
				pgs[1] = 0x2CA2202A
				pgs[2] = 0x2CA2202A
				pgs[3] = 0x46585355
				pgs[4] = 0x322040E
				pgs[6] = 0xEBBBD6A
				pgs[7] = 0xAF198759
				pgs[8] = 0x7B25ED36
				pgs[9] = 0x8D6EBCD
				pgs[10] = "ZY.FT"
			elseif pg == "星瀚_龙太子_普通" then
				pgs[1] = 0xF67E30AD
				pgs[2] = 0xF67E30AD
				pgs[3] = 0x36CC7C67
				pgs[4] = 0xA7FAAB06
				pgs[6] = 0x67BC1005
				pgs[7] = 0x9FE029BF
				pgs[8] = 0x8B72FF9
				pgs[9] = 0xBF00ABF2
				pgs[10] = "ZY.FT"
		    elseif pg == "梨花_龙太子_普通" then
				pgs[1] = 3747222349
				pgs[2] = 3747222349
				pgs[3] = 3189155092
				pgs[4] = 3062365732
				pgs[6] = 1598243358
				pgs[7] = 2389430522
				pgs[8] = 3918605409
				pgs[9] = 1648372626
				pgs[10] = "ZY.FT"
		    elseif pg == "飞龙在天_龙太子_普通" then
				pgs[1] = 0x49187E40
				pgs[2] = 0x49187E40
				pgs[3] = 0xA87E49E8
				pgs[4] = 0x41E835B5
				pgs[6] = 0x28FCF221
				pgs[7] = 0x367AC070
				pgs[8] = 0x1255644D
				pgs[9] = 0x4E9D13AE
				pgs[10] = "ZY.FT"
			elseif pg == "天龙破城_龙太子_普通" then
				pgs[1] = 0666227171
				pgs[2] = 0666227171
				pgs[3] = 3545294647
				pgs[4] = 1075279479
				pgs[6] = 4013753607
				pgs[7] = 1440842173
				pgs[8] = 1146668387
				pgs[9] = 1609569152
				pgs[10] = "ZY.FT"
		    elseif pg == "弑皇_龙太子_普通" then
				pgs[1] = 0x2F84D4CA
				pgs[2] = 0x2F84D4CA
				pgs[3] = 0x9792B519
				pgs[4] = 0xB80DD092
				pgs[6] = 0xEF721760
				pgs[7] = 0x6B780672
				pgs[8] = 0xA85D5556
				pgs[9] = 0x28B45289
				pgs[10] = "ZY.FT"
		    elseif pg == "秋风_龙太子_普通" then
				pgs[1] = 775882284
				pgs[2] = 775882284
				pgs[3] = 749947548
				pgs[4] = 4103046621
				pgs[6] = 4079294468
				pgs[7] = 1774952424
				pgs[8] = 409973300
				pgs[9] = 342475400
				pgs[10] = "ZY.FT"
		    elseif pg == "太极_龙太子_普通" then
				pgs[1] = 3905888692
				pgs[2] = 3905888692
				pgs[3] = 2235760273
				pgs[4] = 518907138
				pgs[6] = 121079973
				pgs[7] = 662010105
				pgs[8] = 1852360605
				pgs[9] = 269312850
				pgs[10] = "ZY.FT"
    ---------------------------------------------------------------------------
			elseif pg == "游龙剑_剑侠客_普通" then
				pgs[1] = 3922251448
				pgs[2] = 3922251448
				pgs[3] = 2729643500
				pgs[4] = 3737672137
				pgs[6] = 525352739
				pgs[7] = 948395215
				pgs[8] = 563220674
				pgs[9] = 3149578823
				pgs[10] = "ZY.FT"
		    elseif pg == "青铜短剑_剑侠客_普通" then
				pgs[1] = 4256351077
				pgs[2] = 4256351077
				pgs[3] = 1799000911
				pgs[4] = 3029718579
				pgs[6] = 2340710053
				pgs[7] = 874161900
				pgs[8] = 1001128045
				pgs[9] = 3927011624
				pgs[10] = "ZY.FT"
		    elseif pg == "金背大砍刀_剑侠客_普通" then
				pgs[1] = 2695990707
				pgs[2] = 2695990707
				pgs[3] = 1837946764
				pgs[4] = 1995983471
				pgs[6] = 3956010201
				pgs[7] = 3740065177
				pgs[8] = 3607972716
				pgs[9] = 1238352835
				pgs[10] = "ZY.FT"
		    elseif pg == "剑侠客_普通" then
				pgs[1] = 2840575336
				pgs[2] = 2840575336
				pgs[3] = 991657694
				pgs[4] = 3738000150
				pgs[6] = 2626634241
				pgs[7] = 1486978342
				pgs[8] = 512918654
				pgs[9] = 361988011
				pgs[10] = "ZY.FT"
		    elseif pg == "狼牙刀_剑侠客_普通" then
				pgs[1] = 914160701
				pgs[2] = 914160701
				pgs[3] = 790662160
				pgs[4] = 4033879125
				pgs[6] = 2718755916
				pgs[7] = 310154266
				pgs[8] = 2495162598
				pgs[9] = 2607187114
				pgs[10] = "ZY.FT"
		    elseif pg == "青锋剑_剑侠客_普通" then
				pgs[1] = 128739684
				pgs[2] = 128739684
				pgs[3] = 1360269025
				pgs[4] = 1259586579
				pgs[6] = 3155607107
				pgs[7] = 3585789919
				pgs[8] = 548399706
				pgs[9] = 3149578823
				pgs[10] = "ZY.FT"
			elseif pg == "柳叶刀_剑侠客_普通" then
				pgs[1] = 2309692066
				pgs[2] = 2309692066
				pgs[3] = 4083786274
				pgs[4] = 1649684398
				pgs[6] = 3909215663
				pgs[7] = 1820668649
				pgs[8] = 1323639728
				pgs[9] = 3231555815
				pgs[10] = "ZY.FT"
		    elseif pg == "剑侠客_刀" then
				pgs[1] = 3074477781
				pgs[2] = 3074477781
				pgs[3] = 991657694
				pgs[4] = 2583942498
				pgs[6] = 1121741199
				pgs[7] = 701848281
				pgs[8] = 621175708
				pgs[9] = 2285822938
				pgs[10] = "ZY.FT"
			elseif pg == "剑侠客_剑" then
				pgs[1] = 2840575336
				pgs[2] = 2840575336
				pgs[3] = 991657694
				pgs[4] = 3738000150
				pgs[6] = 2626634241
				pgs[7] = 1486978342
				pgs[8] = 512918654
				pgs[9] = 361988011
				pgs[10] = "ZY.FT"
		    elseif pg == "斩妖泣血_剑侠客_普通" then
				pgs[1] = 3366877163
				pgs[2] = 3366877163
				pgs[3] = 2527757700
				pgs[4] = 423606103
				pgs[6] = 2965089172
				pgs[7] = 3694647351
				pgs[8] = 223797192
				pgs[9] = 902186530
				pgs[10] = "ZY.FT"
			elseif pg == "业火三灾_剑侠客_普通" then
				pgs[1] = 0210215203
				pgs[2] = 0210215203
				pgs[3] = 0742329354
				pgs[4] = 4144128188
				pgs[6] = 3922982597
				pgs[7] = 3358965260
				pgs[8] = 2322529890
				pgs[9] = 2370381803
				pgs[10] = "ZY.FT"
			elseif pg == "鸣鸿_剑侠客_普通" then
				pgs[1] = 1301462828
				pgs[2] = 1301462828
				pgs[3] = 0874331809
				pgs[4] = 1002416013
				pgs[6] = 0119572952
				pgs[7] = 2800523884
				pgs[8] = 4144463970
				pgs[9] = 3391554113
				pgs[10] = "ZY.FT"
		    elseif pg == "魏武青虹_剑侠客_普通" then
				pgs[1] = 3545119585
				pgs[2] = 3545119585
				pgs[3] = 157523664
				pgs[4] = 1783491444
				pgs[6] = 1488281366
				pgs[7] = 2951999449
				pgs[8] = 1776089940
				pgs[9] = 599864712
				pgs[10] = "ZY.FT"
		    elseif pg == "血刃_剑侠客_普通" then
				pgs[1] = 3942369185
				pgs[2] = 3942369185
				pgs[3] = 2744623122
				pgs[4] = 3558966261
				pgs[6] = 2535946630
				pgs[7] = 1477301268
				pgs[8] = 658806952
				pgs[9] = 3254342488
				pgs[10] = "ZY.FT"
			elseif pg == "倚天_剑侠客_普通" then
				pgs[1] = 1368524342
				pgs[2] = 1368524342
				pgs[3] = 1068916714
				pgs[4] = 4221789782
				pgs[6] = 360062771
				pgs[7] = 517811079
				pgs[8] = 3380216158
				pgs[9] = 195389599
				pgs[10] = "ZY.FT"
		    elseif pg == "湛卢_剑侠客_普通" then
				pgs[1] = 174574078
				pgs[2] = 174574078
				pgs[3] = 2462930631
				pgs[4] = 623897396
				pgs[6] = 357132169
				pgs[7] = 1091639193
				pgs[8] = 132771616
				pgs[9] = 1502161679
				pgs[10] = "ZY.FT"
		    elseif pg == "四法青云_剑侠客_普通" then
				pgs[1] = 2301962273
				pgs[2] = 2301962273
				pgs[3] = 1642344190
				pgs[4] = 4019743340
				pgs[6] = 690548315
				pgs[7] = 654838706
				pgs[8] = 3259089844
				pgs[9] = 3268379919
				pgs[10] = "ZY.FT"
		    elseif pg == "霜冷九州_剑侠客_普通" then
				pgs[1] = 3750052903
				pgs[2] = 3750052903
				pgs[3] = 3978185529
				pgs[4] = 1126547325
				pgs[6] = 3133397477
				pgs[7] = 1400612648
				pgs[8] = 1048600867
				pgs[9] = 0587348277
				pgs[10] = "ZY.FT"
		    elseif pg == "擒龙_剑侠客_普通" then
				pgs[1] = 2066485351
				pgs[2] = 2066485351
				pgs[3] = 3200400983
				pgs[4] = 4074782058
				pgs[6] = 2739156816
				pgs[7] = 2620255839
				pgs[8] = 3164581880
				pgs[9] = 0839277591
				pgs[10] = "ZY.FT"
		    elseif pg == "晓风残月_剑侠客_普通" then
				pgs[1] = 3614142659
				pgs[2] = 3614142659
				pgs[3] = 222137428
				pgs[4] = 162101136
				pgs[6] = 3111105254
				pgs[7] = 1410594659
				pgs[8] = 1285154537
				pgs[9] = 1663734878
				pgs[10] = "ZY.FT"
			elseif pg == "屠龙_剑侠客_普通" then
				pgs[1] = 892827193
				pgs[2] = 892827193
				pgs[3] = 2530500829
				pgs[4] = 3739812188
				pgs[6] = 408410478
				pgs[7] = 2903940161
				pgs[8] = 1040159026
				pgs[9] = 319067749
				pgs[10] = "ZY.FT"
		    elseif pg == "鱼肠_剑侠客_普通" then
				pgs[1] = 4227823198
				pgs[2] = 4227823198
				pgs[3] = 2698488437
				pgs[4] = 3798717939
				pgs[6] = 3533475904
				pgs[7] = 3303465815
				pgs[8] = 1861666518
				pgs[9] = 1012813203
				pgs[10] = "ZY.FT"
		    elseif pg == "灵犀神剑_剑侠客_普通" then
				pgs[1] = 1664613177
				pgs[2] = 1664613177
				pgs[3] = 739286596
				pgs[4] = 3081380929
				pgs[6] = 2240524266
				pgs[7] = 2235430645
				pgs[8] = 2276071821
				pgs[9] = 83886513
				pgs[10] = "ZY.FT"
		    elseif pg == "冷月_剑侠客_普通" then
				pgs[1] = 1050757801
				pgs[2] = 1050757801
				pgs[3] = 2523626742
				pgs[4] = 332965488
				pgs[6] = 797204911
				pgs[7] = 1739503652
				pgs[8] = 3541810222
				pgs[9] = 1219913712
				pgs[10] = "ZY.FT"
		    elseif pg == "偃月青龙_剑侠客_普通" then
				pgs[1] = 190424072
				pgs[2] = 190424072
				pgs[3] = 1961249071
				pgs[4] = 3838801989
				pgs[6] = 2829649300
				pgs[7] = 3543370033
				pgs[8] = 1454309726
				pgs[9] = 1450613061
				pgs[10] = "ZY.FT"
		    elseif pg == "擒龙_剑侠客_普通" then
				pgs[1] = 2066485351
				pgs[2] = 2066485351
				pgs[3] = 3200400983
				pgs[4] = 4074782058
				pgs[6] = 2739156816
				pgs[7] = 2620255839
				pgs[8] = 3164581880
				pgs[9] = 0839277591
				pgs[10] = "ZY.FT"
    --------------------------------------------------------------------------------------------
			elseif pg == "狐美人_爪" then
				pgs[1] = 143164260
				pgs[2] = 143164260
				pgs[3] = 3180126153
				pgs[4] = 3467601219
				pgs[6] = 1873404188
				pgs[7] = 1585787695
				pgs[8] = 947996020
				pgs[9] = 3212527142
				pgs[10] = "ZY.FT"
		    elseif pg == "钢结鞭_狐美人_普通" then
				pgs[1] = 2383050056
				pgs[2] = 2383050056
				pgs[3] = 3281460149
				pgs[4] = 3516998304
				pgs[6] = 246739877
				pgs[7] = 3039794984
				pgs[8] = 875610474
				pgs[9] = 3210281280
				pgs[10] = "ZY.FT"
			elseif pg == "青刚刺_狐美人_普通" then
				pgs[1] = 181873383
				pgs[2] = 181873383
				pgs[3] = 1794635107
				pgs[4] = 429992710
				pgs[6] = 1822050644
				pgs[7] = 366643004
				pgs[8] = 2389652378
				pgs[9] = 2180063945
				pgs[10] = "ZY.FT"
		    elseif pg == "青藤柳叶鞭_狐美人_普通" then
				pgs[1] = 1451526591
				pgs[2] = 1451526591
				pgs[3] = 672510267
				pgs[4] = 2873780575
				pgs[6] = 2492119629
				pgs[7] = 599092463
				pgs[8] = 3419036208
				pgs[9] = 2641306317
				pgs[10] = "ZY.FT"
		    elseif pg == "狐美人_普通" then
				pgs[1] = 143164260
				pgs[2] = 143164260
				pgs[3] = 3180126153
				pgs[4] = 3467601219
				pgs[6] = 1734212905
				pgs[7] = 1585787695
				pgs[8] = 947996020
				pgs[9] = 3212527142
				pgs[10] = "ZY.FT"
		    elseif pg == "牛皮鞭_狐美人_普通" then
				pgs[1] = 803587567
				pgs[2] = 803587567
				pgs[3] = 2928593272
				pgs[4] = 3180100501
				pgs[6] = 3201126946
				pgs[7] = 3347553641
				pgs[8] = 435101418
				pgs[9] = 2721039077
				pgs[10] = "ZY.FT"
		    elseif pg == "青龙牙_狐美人_普通" then
				pgs[1] = 3328063850
				pgs[2] = 3328063850
				pgs[3] = 3138195936
				pgs[4] = 1207144244
				pgs[6] = 620017539
				pgs[7] = 1100137434
				pgs[8] = 957732521
				pgs[9] = 702824336
				pgs[10] = "ZY.FT"
		    elseif pg == "铁爪_狐美人_普通" then
				pgs[1] = 1341071015
				pgs[2] = 1341071015
				pgs[3] = 2333330309
				pgs[4] = 2453642175
				pgs[6] = 1760107365
				pgs[7] = 4127315635
				pgs[8] = 2476805398
				pgs[9] = 2928915082
				pgs[10] = "ZY.FT"
		    elseif pg == "狐美人_鞭" then
				pgs[1] = 1138804365
				pgs[2] = 1138804365
				pgs[3] = 247333098
				pgs[4] = 303785981
				pgs[6] = 1734212905
				pgs[7] = 3749438
				pgs[8] = 4126393258
				pgs[9] = 3502426669
				pgs[10] = "ZY.FT"
		    elseif pg == "胭脂_狐美人_普通" then
				pgs[1] = 435255964
				pgs[2] = 435255964
				pgs[3] = 2314371856
				pgs[4] = 3864765510
				pgs[6] = 505158205
				pgs[7] = 1747354624
				pgs[8] = 1199574166
				pgs[9] = 954931940
				pgs[10] = "ZY.FT"
			elseif pg == "贵霜之牙_狐美人_普通" then
				pgs[1] = 2026448705
				pgs[2] = 2026448705
				pgs[3] = 741309234
				pgs[4] = 3203975285
				pgs[6] = 1564840489
				pgs[7] = 474679658
				pgs[8] = 4058539230
				pgs[9] = 926627376
				pgs[10] = "ZY.FT"
			elseif pg == "忘川三途_狐美人_普通" then
				pgs[1] = 0236232349
				pgs[2] = 0236232349
				pgs[3] = 1075219754
				pgs[4] = 1037429893
				pgs[6] = 2989801805
				pgs[7] = 1339124563
				pgs[8] = 3926929253
				pgs[9] = 3874185526
				pgs[10] = "ZY.FT"
			elseif pg == "离钩_狐美人_普通" then
				pgs[1] = 0564401744
				pgs[2] = 0564401744
				pgs[3] = 1267356954
				pgs[4] = 2021299893
				pgs[6] = 3148444658
				pgs[7] = 3954561085
				pgs[8] = 4230441971
				pgs[9] = 1335188315
				pgs[10] = "ZY.FT"
			elseif pg == "百花_狐美人_普通" then
				pgs[1] = 1502748093
				pgs[2] = 1502748093
				pgs[3] = 2250675787
				pgs[4] = 2755073465
				pgs[6] = 1990532858
				pgs[7] = 1787167953
				pgs[8] = 3161043606
				pgs[9] = 546759080
				pgs[10] = "ZY.FT"
		    elseif pg == "仙人指路_狐美人_普通" then
				pgs[1] = 1169841436
				pgs[2] = 1169841436
				pgs[3] = 851455711
				pgs[4] = 1792787137
				pgs[6] = 1034828303
				pgs[7] = 4038631411
				pgs[8] = 1358665134
				pgs[9] = 657246130
				pgs[10] = "ZY.FT"
		    elseif pg == "九阴勾魂_狐美人_普通" then
				pgs[1] = 34055457
				pgs[2] = 34055457
				pgs[3] = 2107661171
				pgs[4] = 2594225835
				pgs[6] = 1606837702
				pgs[7] = 4229873282
				pgs[8] = 14066732
				pgs[9] = 3187095867
				pgs[10] = "ZY.FT"
		    elseif pg == "雪蚕之刺_狐美人_普通" then
				pgs[1] = 3375230005
				pgs[2] = 3375230005
				pgs[3] = 49297381
				pgs[4] = 4186182464
				pgs[6] = 4168563223
				pgs[7] = 3779159654
				pgs[8] = 1556439270
				pgs[9] = 3143637419
				pgs[10] = "ZY.FT"
		    elseif pg == "毒牙_狐美人_普通" then
				pgs[1] = 2678046311
				pgs[2] = 2678046311
				pgs[3] = 147162749
				pgs[4] = 1046153681
				pgs[6] = 3025877259
				pgs[7] = 4196456021
				pgs[8] = 2797725717
				pgs[9] = 648763427
				pgs[10] = "ZY.FT"
		    elseif pg == "游龙惊鸿_狐美人_普通" then
				pgs[1] = 1677496371
				pgs[2] = 1677496371
				pgs[3] = 1419416100
				pgs[4] = 1427572959
				pgs[6] = 3911390728
				pgs[7] = 4269124018
				pgs[8] = 1118732647
				pgs[9] = 1632285264
				pgs[10] = "ZY.FT"
		    elseif pg == "血之刺藤_狐美人_普通" then
				pgs[1] = 4050126740
				pgs[2] = 4050126740
				pgs[3] = 3429497989
				pgs[4] = 2489520435
				pgs[6] = 2605547995
				pgs[7] = 888707380
				pgs[8] = 1942318093
				pgs[9] = 3544559629
				pgs[10] = "ZY.FT"
			elseif pg == "牧云清歌_狐美人_普通" then
				pgs[1] = 1719727329
				pgs[2] = 1719727329
				pgs[3] = 0210056156
				pgs[4] = 4235354976
				pgs[6] = 3324406676
				pgs[7] = 4002747821
				pgs[8] = 2497747470
				pgs[9] = 0747942707
				pgs[10] = "ZY.FT"
			elseif pg == "霜陨_狐美人_普通" then
				pgs[1] = 1258580513
				pgs[2] = 1258580513--攻击
				pgs[3] = 1285805382
				pgs[4] = 3557049784
				pgs[6] = 0373506230
				pgs[7] = 1376923560
				pgs[8] = 4168352366
				pgs[9] = 1324744500
				pgs[10] = "ZY.FT"
		    elseif pg == "龙筋_狐美人_普通" then--1是攻击 2是第二种形态攻击 3是死亡  4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包
				pgs[1] = 3760276395
				pgs[2] = 3760276395
				pgs[3] = 4209944243
				pgs[4] = 3304310422
				pgs[6] = 1123548549
				pgs[7] = 478714521
				pgs[8] = 3148732170
				pgs[9] = 1847888299
				pgs[10] = "ZY.FT"
		    elseif pg == "吹雪_狐美人_普通" then
				pgs[1] = 2782365575
				pgs[2] = 2782365575
				pgs[3] = 2863024953
				pgs[4] = 2075646427
				pgs[6] = 1793299889
				pgs[7] = 3492647849
				pgs[8] = 4255111126
				pgs[9] = 3079814036
				pgs[10] = "ZY.FT"
		    elseif pg == "撕天_狐美人_普通" then
				pgs[1] = 3142217049
				pgs[2] = 3142217049
				pgs[3] = 448133610
				pgs[4] = 3204327512
				pgs[6] = 2232734491
				pgs[7] = 2803578940
				pgs[8] = 1204989569
				pgs[9] = 3697845403
				pgs[10] = "ZY.FT"
    ----------------------------------------------------------------------------------------------
			elseif pg == "乌金三叉戟_神天兵_普通" then
				pgs[1] = 408164304
				pgs[2] = 408164304
				pgs[3] = 4205323621
				pgs[4] = 2362411253
				pgs[6] = 2944638184
				pgs[7] = 611952719
				pgs[8] = 907746138
				pgs[9] = 1478073855
				pgs[10] = "ZY.FT"
		    elseif pg == "玄铁矛_神天兵_普通" then
				pgs[1] = 457024889
				pgs[2] = 457024889
				pgs[3] = 575337904
				pgs[4] = 970913679
				pgs[6] = 3111818852
				pgs[7] = 1880364342
				pgs[8] = 883798385
				pgs[9] = 2466450533
				pgs[10] = "ZY.FT"
		    elseif pg == "狼牙锤_神天兵_普通" then
				pgs[1] = 103920893
				pgs[2] = 103920893
				pgs[3] = 3337098745
				pgs[4] = 1754275390
				pgs[6] = 1598161855
				pgs[7] = 4165662626
				pgs[8] = 3269185703
				pgs[9] = 685627288
				pgs[10] = "ZY.FT"
		    elseif pg == "震天锤_神天兵_普通" then
				pgs[1] = 3035485426
				pgs[2] = 3035485426
				pgs[3] = 1437571424
				pgs[4] = 3129123356
				pgs[6] = 3250445538
				pgs[7] = 4112972849
				pgs[8] = 4145837202
				pgs[9] = 4145837202
				pgs[10] = "ZY.FT"
		    elseif pg == "神天兵_普通" then
				pgs[1] = 3139413485
				pgs[2] = 3139413485
				pgs[3] = 3251213113
				pgs[4] = 1857394109
				pgs[6] = 1539170847
				pgs[7] = 1525915374
				pgs[8] = 2489545420
				pgs[9] = 2207457325
				pgs[10] = "ZY.FT"
		    elseif pg == "神天兵_锤" then
				pgs[1] = 1465303491
				pgs[2] = 1465303491
				pgs[3] = 1649555822
				pgs[4] = 2910864082
				pgs[6] = 3107944047
				pgs[7] = 2714401860
				pgs[8] = 2489545420
				pgs[9] = 2796680623
				pgs[10] = "ZY.FT"
		    elseif pg == "红缨枪_神天兵_普通" then
				pgs[1] = 3072500196
				pgs[2] = 3072500196
				pgs[3] = 1936650700
				pgs[4] = 522874091
				pgs[6] = 72270422
				pgs[7] = 2966336322
				pgs[8] = 117062963
				pgs[9] = 3637390492
				pgs[10] = "ZY.FT"
		    elseif pg == "松木锤_神天兵_普通" then
				pgs[1] = 4014115127
				pgs[2] = 4014115127
				pgs[3] = 433361276
				pgs[4] = 2686623681
				pgs[6] = 316202605
				pgs[7] = 776522968
				pgs[8] = 400987167
				pgs[9] = 3257370843
				pgs[10] = "ZY.FT"
		    elseif pg == "飞龙在天_神天兵_普通" then
				pgs[1] = 2032927311
				pgs[2] = 2032927311
				pgs[3] = 1657151508
				pgs[4] = 3978455792
				pgs[6] = 1874013939
				pgs[7] = 3740325538
				pgs[8] = 2181635146
				pgs[9] = 599728780
				pgs[10] = "ZY.FT"
		    elseif pg == "天龙破城_神天兵_普通" then
				pgs[1] = 0171966452
				pgs[2] = 0171966452
				pgs[3] = 2084794125
				pgs[4] = 2107979991
				pgs[6] = 1581914867
				pgs[7] = 2942548432
				pgs[8] = 1411813089
				pgs[9] = 1849607449
				pgs[10] = "ZY.FT"
		    elseif pg == "弑皇_神天兵_普通" then
				pgs[1] = 1997962163
				pgs[2] = 1997962163
				pgs[3] = 0736116348
				pgs[4] = 1627526221
				pgs[6] = 0244362789
				pgs[7] = 0451837867
				pgs[8] = 3255465889
				pgs[9] = 1104681624
				pgs[10] = "ZY.FT"
		    elseif pg == "鬼王蚀日_神天兵_普通" then
				pgs[1] = 963362133
				pgs[2] = 963362133
				pgs[3] = 152105194
				pgs[4] = 3867741716
				pgs[6] = 1752393650
				pgs[7] = 278748353
				pgs[8] = 1749943111
				pgs[9] = 0xB1EC4BAE
				pgs[10] = "ZY.FT"
		    elseif pg == "狂澜碎岳_神天兵_普通" then
				pgs[1] = 2235903565
				pgs[2] = 2235903565
				pgs[3] = 2603636370
				pgs[4] = 2603636370
				pgs[6] = 2176071785
				pgs[7] = 4182805395
				pgs[8] = 0485622618
				pgs[9] = 0725700904
				pgs[10] = "ZY.FT"
		    elseif pg == "碎寂_神天兵_普通" then
				pgs[1] = 3625830153
				pgs[2] = 3625830153
				pgs[3] = 3568054291
				pgs[4] = 2897242392
				pgs[6] = 2339108953
				pgs[7] = 0520986973
				pgs[8] = 3030332360
				pgs[9] = 1269074276
				pgs[10] = "ZY.FT"
		    elseif pg == "神天兵_枪" then
			    pgs[1] = 3139413485
				pgs[2] = 3139413485
				pgs[3] = 3251213113
				pgs[4] = 1857394109
				pgs[6] = 1539170847
				pgs[7] = 1525915374
				pgs[8] = 2489545420
				pgs[9] = 2207457325
				pgs[10] = "ZY.FT"
		    elseif pg == "暗夜_神天兵_普通" then
				pgs[1] = 1082922820
				pgs[2] = 1082922820
				pgs[3] = 975258259
				pgs[4] = 3895199857
				pgs[6] = 1619820242
				pgs[7] = 3383885620
				pgs[8] = 336694600
				pgs[9] = 932923894
				pgs[10] = "ZY.FT"
		    elseif pg == "梨花_神天兵_普通" then
				pgs[1] = 1046350880
				pgs[2] = 1046350880
				pgs[3] = 3432918521
				pgs[4] = 2634626428
				pgs[6] = 3303390888
				pgs[7] = 1240292677
				pgs[8] = 3708021512
				pgs[9] = 1872876301
				pgs[10] = "ZY.FT"
		    elseif pg == "五虎断魂_神天兵_普通" then
				pgs[1] = 2816878895
				pgs[2] = 2816878895
				pgs[3] = 3237243180
				pgs[4] = 298941115
				pgs[6] = 555909501
				pgs[7] = 683330927
				pgs[8] = 2195670772
				pgs[9] = 223295368
				pgs[10] = "ZY.FT"
		    elseif pg == "鬼牙_神天兵_普通" then
				pgs[1] = 2430761776
				pgs[2] = 2430761776
				pgs[3] = 3359352706
				pgs[4] = 2621027170
				pgs[6] = 1110189315
				pgs[7] = 2486917341
				pgs[8] = 2741674073
				pgs[9] = 2681319409
				pgs[10] = "ZY.FT"
		    elseif pg == "霹雳_神天兵_普通" then
				pgs[1] = 1051053489
				pgs[2] = 1051053489
				pgs[3] = 4239140569
				pgs[4] = 2085918398
				pgs[6] = 3349289903
				pgs[7] = 3508045990
				pgs[8] = 143015740
				pgs[9] = 2889808459
				pgs[10] = "ZY.FT"
		    elseif pg == "雷神_神天兵_普通" then
				pgs[1] = 148474283
				pgs[2] = 148474283
				pgs[3] = 2398394513
				pgs[4] = 2945190989
				pgs[6] = 3920362664
				pgs[7] = 3086874992
				pgs[8] = 2993116090
				pgs[9] = 2515289535
				pgs[10] = "ZY.FT"
		    elseif pg == "混元金锤_神天兵_普通" then
				pgs[1] = 1923208867
				pgs[2] = 1923208867
				pgs[3] = 3952062894
				pgs[4] = 1295504995
				pgs[6] = 577114027
				pgs[7] = 1445398766
				pgs[8] = 3171781597
				pgs[9] = 1835217455
				pgs[10] = "ZY.FT"
		    elseif pg == "刑天之逆_神天兵_普通" then
				pgs[1] = 4050435392
				pgs[2] = 4050435392
				pgs[3] = 1597843931
				pgs[4] = 1688528240
				pgs[6] = 2425493529
				pgs[7] = 1710005881
				pgs[8] = 507405361
				pgs[9] = 167694840
				pgs[10] = "ZY.FT"
		    elseif pg == "九瓣莲花_神天兵_普通" then
				pgs[1] = 697482824
				pgs[2] = 697482824
				pgs[3] = 1082828846
				pgs[4] = 1141897393
				pgs[6] = 3748355701
				pgs[7] = 616355012
				pgs[8] = 2187902545
				pgs[9] = 927580732
				pgs[10] = "ZY.FT"
 --------------------------------------------------------------------------------------------
			elseif pg == "如意宝珠_杀破狼_普通" then
				pgs[1] = 2994264484
				pgs[2] = 2994264484
				pgs[3] = 1404894523
				pgs[4] = 530980268
				pgs[6] = 3593075951
				pgs[7] = 4288686185
				pgs[8] = 834957611
				pgs[9] = 935164006
				pgs[10] = "ZY.FT"
		    elseif pg == "翡翠珠_杀破狼_普通" then
				pgs[1] = 1888438119
				pgs[2] = 1888438119
				pgs[3] = 832082873
				pgs[4] = 2462082254
				pgs[6] = 3335305851
				pgs[7] = 3225276990
				pgs[8] = 3777022322
				pgs[9] = 2618824675
				pgs[10] = "ZY.FT"
			elseif pg == "杀破狼_普通" then
				pgs[1] = 2232599242
				pgs[2] = 2232599242
				pgs[3] = 3215162495
				pgs[4] = 3629275056
				pgs[6] = 4241012400
				pgs[7] = 3547444917
				pgs[8] = 1721194312
				pgs[9] = 3836921096
				pgs[10] = "ZY.FT"
			elseif pg == "杀破狼_宝珠" then
				pgs[1] = 3067304876
				pgs[2] = 3067304876
				pgs[3] = 3215162495
				pgs[4] = 1186759867
				pgs[6] = 1266038770
				pgs[7] = 2576943278
				pgs[8] = 1721194312
				pgs[9] = 3836921096
				pgs[10] = "ZY.FT"
		    elseif pg == "连珠神弓_杀破狼_普通" then
				pgs[1] = 3896611853
				pgs[2] = 3896611853
				pgs[3] = 2036432029
				pgs[4] = 2102929671
				pgs[6] = 3589969117
				pgs[7] = 3876338026
				pgs[8] = 1811002066
				pgs[9] = 1930889295
				pgs[10] = "ZY.FT"
		    elseif pg == "琉璃珠_杀破狼_普通" then
				pgs[1] = 1347542308
				pgs[2] = 1347542308
				pgs[3] = 1084648267
				pgs[4] = 2598475041
				pgs[6] = 220293482
				pgs[7] = 309382841
				pgs[8] = 3237534358
				pgs[9] = 4248308764
				pgs[10] = "ZY.FT"
		    elseif pg == "宝雕长弓_杀破狼_普通" then
				pgs[1] = 3187707461
				pgs[2] = 3187707461
				pgs[3] = 3770288341
				pgs[4] = 2752043077
				pgs[6] = 2434290964
				pgs[7] = 3329145004
				pgs[8] = 1745722591
				pgs[9] = 9444076
				pgs[10] = "ZY.FT"
		    elseif pg == "硬木弓_杀破狼_普通" then
				pgs[1] = 3001778802
				pgs[2] = 3001778802
				pgs[3] = 2629114281
				pgs[4] = 370350902
				pgs[6] = 3945843828
				pgs[7] = 297727953
				pgs[8] = 1273865503
				pgs[9] = 1767508772
				pgs[10] = "ZY.FT"
		    elseif pg == "杀破狼_弓弩" then
				pgs[1] = 2232599242
				pgs[2] = 2232599242
				pgs[3] = 3215162495
				pgs[4] = 3629275056
				pgs[6] = 4241012400
				pgs[7] = 3547444917
				pgs[8] = 1721194312
				pgs[9] = 3836921096
				pgs[10] = "ZY.FT"
			elseif pg == "杀破狼_弓弩1" then
				pgs[1] = 3555676410
				pgs[2] = 3555676410
				pgs[3] = 3215162495
				pgs[4] = 3629275056
				pgs[6] = 0167917977
				pgs[7] = 2697172445
				pgs[8] = 1721194312
				pgs[9] = 3836921096
				pgs[10] = "ZY.FT"
		    elseif pg == "裂云啸日_杀破狼_普通" then
				pgs[1] = 1323042371
				pgs[2] = 1323042371
				pgs[3] = 2471306192
				pgs[4] = 2206427343
				pgs[6] = 3359417010
				pgs[7] = 3912741797
				pgs[8] = 753797811
				pgs[9] = 656525376
				pgs[10] = "ZY.FT"
			elseif pg == "云雷万里_杀破狼_普通" then
				pgs[1] = 0929693147
				pgs[2] = 0929693147
				pgs[3] = 2339783982
				pgs[4] = 2673184399
				pgs[6] = 1053032888
				pgs[7] = 1343023772
				pgs[8] = 1593490105
				pgs[9] = 0305349853
				pgs[10] = "ZY.FT"
			elseif pg == "赤明_杀破狼_普通" then
				pgs[1] = 0454754499
				pgs[2] = 0454754499
				pgs[3] = 4014535055
				pgs[4] = 1847890126
				pgs[6] = 0039518222
				pgs[7] = 2065247607
				pgs[8] = 1441775582
				pgs[9] = 0138471714
				pgs[10] = "ZY.FT"
		    elseif pg == "龙鸣寒水_杀破狼_普通" then
				pgs[1] = 2135796424
				pgs[2] = 2135796424
				pgs[3] = 4117872420
				pgs[4] = 4105996895
				pgs[6] = 2127507146
				pgs[7] = 4261464327
				pgs[8] = 1514939238
				pgs[9] = 748425136
				pgs[10] = "ZY.FT"
		    elseif pg == "太极流光_杀破狼_普通" then
				pgs[1] = 2074525537
				pgs[2] = 2074525537
				pgs[3] = 449187270
				pgs[4] = 2759360079
				pgs[6] = 3413608092
				pgs[7] = 1682905191
				pgs[8] = 4051367450
				pgs[9] = 3252077695
				pgs[10] = "ZY.FT"
			elseif pg == "九霄风雷_杀破狼_普通" then
				pgs[1] = 3704392067
				pgs[2] = 3704392067
				pgs[3] = 3632605747
				pgs[4] = 2197826216
				pgs[6] = 2845081681
				pgs[7] = 2020968771
				pgs[8] = 3450511222
				pgs[9] = 1885695436
				pgs[10] = "ZY.FT"
			elseif pg == "若木_杀破狼_普通" then
				pgs[1] = 2783866786
				pgs[2] = 2783866786
				pgs[3] = 1862994822
				pgs[4] = 1445406110
				pgs[6] = 1501063042
				pgs[7] = 1500235485
				pgs[8] = 3264257605
				pgs[9] = 3963075748
				pgs[10] = "ZY.FT"
			elseif pg == "飞星_杀破狼_普通" then
				pgs[1] = 1920229574
				pgs[2] = 1920229574
				pgs[3] = 3670774470
				pgs[4] = 1565695987
				pgs[6] = 612719827
				pgs[7] = 304215519
				pgs[8] = 15688186
				pgs[9] = 3390941259
				pgs[10] = "ZY.FT"
			elseif pg == "冥火薄天_杀破狼_普通" then
				pgs[1] = 871369705
				pgs[2] = 871369705
				pgs[3] = 1399674031
				pgs[4] = 1805010867
				pgs[6] = 1620002437
				pgs[7] = 1839576620
				pgs[8] = 621841422
				pgs[9] = 3126248794
				pgs[10] = "ZY.FT"
			elseif pg == "幽篁_杀破狼_普通" then
				pgs[1] = 1663842501
				pgs[2] = 1663842501
				pgs[3] = 2110884946
				pgs[4] = 4241477506
				pgs[6] = 4245424904
				pgs[7] = 1184291529
				pgs[8] = 3595306000
				pgs[9] = 3541370331
				pgs[10] = "ZY.FT"
			elseif pg == "百鬼_杀破狼_普通" then
				pgs[1] = 2607438844
				pgs[2] = 2607438844
				pgs[3] = 894755654
				pgs[4] = 1601466888
				pgs[6] = 520922337
				pgs[7] = 3106122659
				pgs[8] = 610584642
				pgs[9] = 1809776963
				pgs[10] = "ZY.FT"
		    elseif pg == "非攻_杀破狼_普通" then
				pgs[1] = 1885839562
				pgs[2] = 1885839562
				pgs[3] = 3957124377
				pgs[4] = 3204102825
				pgs[6] = 2379415806
				pgs[7] = 1950788981
				pgs[8] = 1127738414
				pgs[9] = 2661471858
				pgs[10] = "ZY.FT"
		    elseif pg == "紫金葫芦_杀破狼_普通" then
				pgs[1] = 994535269
				pgs[2] = 994535269
				pgs[3] = 4014927528
				pgs[4] = 3310480847
				pgs[6] = 2831639351
				pgs[7] = 122206673
				pgs[8] = 1940957103
				pgs[9] = 2257546251
				pgs[10] = "ZY.FT"
			elseif pg == "回风舞雪_杀破狼_普通" then
				pgs[1] = 723925966
				pgs[2] = 723925966
				pgs[3] = 1798299344
				pgs[4] = 1212380108
				pgs[6] = 1637540313
				pgs[7] = 2653292719
				pgs[8] = 257191592
				pgs[9] = 2659671813
				pgs[10] = "ZY.FT"
		    elseif pg == "离火_杀破狼_普通" then
				pgs[1] = 1736891488
				pgs[2] = 1736891488
				pgs[3] = 1516097158
				pgs[4] = 1419099406
				pgs[6] = 619566973
				pgs[7] = 332324123
				pgs[8] = 942082261
				pgs[9] = 4082318050
				pgs[10] = "ZY.FT"
		    elseif pg == "月华_杀破狼_普通" then
				pgs[1] = 3883698348
				pgs[2] = 3883698348
				pgs[3] = 1535796704
				pgs[4] = 81237364
				pgs[6] = 786918783
				pgs[7] = 1149367266
				pgs[8] = 2874084219
				pgs[9] = 2578429791
				pgs[10] = "ZY.FT"
    ------------------------------------------------------------------------------------------
			elseif pg == "竹节双剑_英女侠_普通" then
				pgs[1] = 785035487
				pgs[2] = 785035487
				pgs[3] = 388404819
				pgs[4] = 3063317704
				pgs[6] = 781929242
				pgs[7] = 992739922
				pgs[8] = 2099829177
				pgs[9] = 2839104966
				pgs[10] = "ZY.FT"
		    elseif pg == "牛皮鞭_英女侠_普通" then
				pgs[1] = 3931398716
				pgs[2] = 3931398716
				pgs[3] = 1978009081
				pgs[4] = 3180100501
				pgs[6] = 2391704735
				pgs[7] = 2419933303
				pgs[8] = 2521677817
				pgs[9] = 2956424440
				pgs[10] = "ZY.FT"
		    elseif pg == "青藤柳叶鞭_英女侠_普通" then
				pgs[1] = 2804585084
				pgs[2] = 2804585084
				pgs[3] = 60106873
				pgs[4] = 1142701864
				pgs[6] = 3663657779
				pgs[7] = 1499832483
				pgs[8] = 3691312807
				pgs[9] = 3691312807
				pgs[10] = "ZY.FT"
		    elseif pg == "双短剑_英女侠_普通" then
				pgs[1] = 2749813573
				pgs[2] = 2749813573
				pgs[3] = 3182553286
				pgs[4] = 2212413736
				pgs[6] = 2974322210
				pgs[7] = 4220417948
				pgs[8] = 3709846822
				pgs[9] = 4251803233
				pgs[10] = "ZY.FT"
		    elseif pg == "英女侠_鞭" then
				pgs[1] = 1083652265
				pgs[2] = 1083652265
				pgs[3] = 550776032
				pgs[4] = 1830207536
				pgs[6] = 487493048
				pgs[7] = 4015503046
				pgs[8] = 3420727052
				pgs[9] = 2264625814
				pgs[10] = "ZY.FT"
		    elseif pg == "英女侠_普通" then
				pgs[1] = 1131552735
				pgs[2] = 1131552735
				pgs[3] = 550776032
				pgs[4] = 1762226248
				pgs[6] = 283881972
				pgs[7] = 1333230872
				pgs[8] = 2022264436
				pgs[9] = 1784128647
				pgs[10] = "ZY.FT"
		    elseif pg == "英女侠_双" then
				pgs[1] = 1131552735
				pgs[2] = 1131552735
				pgs[3] = 550776032
				pgs[4] = 1762226248
				pgs[6] = 283881972
				pgs[7] = 1333230872
				pgs[8] = 2022264436
				pgs[9] = 1784128647
				pgs[10] = "ZY.FT"
		    elseif pg == "钢结鞭_英女侠_普通" then
				pgs[1] = 271263789
				pgs[2] = 271263789
				pgs[3] = 1861510591
				pgs[4] = 1546457330
				pgs[6] = 2479743000
				pgs[7] = 95594363
				pgs[8] = 2418442398
				pgs[9] = 1501213311
				pgs[10] = "ZY.FT"
		    elseif pg == "灵蛇_英女侠_普通" then
				pgs[1] = 3036397933
				pgs[2] = 3036397933
				pgs[3] = 109615900
				pgs[4] = 2747926100
				pgs[6] = 2286194550
				pgs[7] = 2559098069
				pgs[8] = 227111990
				pgs[9] = 2520581903
				pgs[10] = "ZY.FT"
		    elseif pg == "游龙惊鸿_英女侠_普通" then
				pgs[1] = 1724242915
				pgs[2] = 1724242915
				pgs[3] = 84894305
				pgs[4] = 1910956791
				pgs[6] = 4279024946
				pgs[7] = 4245988549
				pgs[8] = 204351801
				pgs[9] = 3689683150
				pgs[10] = "ZY.FT"

		    elseif pg == "月光双剑_英女侠_普通" then
				pgs[1] = 2739970529
				pgs[2] = 2739970529
				pgs[3] = 1305035055
				pgs[4] = 908717950
				pgs[6] = 1760682539
				pgs[7] = 433987877
				pgs[8] = 2692136956
				pgs[9] = 3445935850
				pgs[10] = "ZY.FT"
		    elseif pg == "赤焰双剑_英女侠_普通" then
				pgs[1] = 4168647024
				pgs[2] = 4168647024
				pgs[3] = 3572817351
				pgs[4] = 712581498
				pgs[6] = 2658138658
				pgs[7] = 2764921579
				pgs[8] = 2741994479
				pgs[9] = 4235932064
				pgs[10] = "ZY.FT"
		    elseif pg == "祖龙对剑_英女侠_普通" then
				pgs[1] = 371522347
				pgs[2] = 371522347
				pgs[3] = 1072281713
				pgs[4] = 3068921098
				pgs[6] = 3389582086
				pgs[7] = 388288592
				pgs[8] = 640740376
				pgs[9] = 3752665492
				pgs[10] = "ZY.FT"
		    elseif pg == "紫电青霜_英女侠_普通" then
				pgs[1] = 2940261156
				pgs[2] = 2940261156
				pgs[3] = 0
				pgs[4] = 1476632389
				pgs[6] = 2376803038
				pgs[7] = 3092849276
				pgs[8] = 1335963374
				pgs[9] = 3005914168
				pgs[10] = "ZY.FT"
		    elseif pg == "浮犀_英女侠_普通" then
				pgs[1] = 1239588375
				pgs[2] = 1239588375
				pgs[3] = 0756053968
				pgs[4] = 0821109265
				pgs[6] = 1472865246
				pgs[7] = 2192274301
				pgs[8] = 1990043008
				pgs[9] = 3996927795
				pgs[10] = "ZY.FT"
		    elseif pg == "阴阳_英女侠_普通" then
				pgs[1] = 3489122494
				pgs[2] = 3489122494
				pgs[3] = 880749981
				pgs[4] = 4142038041
				pgs[6] = 2678300879
				pgs[7] = 3591452298
				pgs[8] = 351801901
				pgs[9] = 2182164503
				pgs[10] = "ZY.FT"
		    elseif pg == "连理双树_英女侠_普通" then
				pgs[1] = 1056087193
				pgs[2] = 1056087193
				pgs[3] = 158990972
				pgs[4] = 2230185189
				pgs[6] = 3682647142
				pgs[7] = 3580865845
				pgs[8] = 686282707
				pgs[9] = 3554518006
				pgs[10] = "ZY.FT"

		    elseif pg == "吹雪_英女侠_普通" then
				pgs[1] = 1984457860
				pgs[2] = 1984457860
				pgs[3] = 2659822722
				pgs[4] = 4091160803
				pgs[6] = 2259108135
				pgs[7] = 1616653577
				pgs[8] = 3980490964
				pgs[9] = 1671498156
				pgs[10] = "ZY.FT"
		    elseif pg == "百花_英女侠_普通" then
				pgs[1] = 1180240329
				pgs[2] = 1180240329
				pgs[3] = 2921076198
				pgs[4] = 2432608663
				pgs[6] = 1464871645
				pgs[7] = 2411376211
				pgs[8] = 421272947
				pgs[9] = 1686679961
				pgs[10] = "ZY.FT"
		    elseif pg == "血之刺藤_英女侠_普通" then
				pgs[1] = 543888368
				pgs[2] = 543888368
				pgs[3] = 3535124792
				pgs[4] = 1279996625
				pgs[6] = 676243872
				pgs[7] = 355830171
				pgs[8] = 3183836178
				pgs[9] = 373595036
				pgs[10] = "ZY.FT"
		    elseif pg == "牧云清歌_英女侠_普通" then
				pgs[1] = 4166360919
				pgs[2] = 4166360919
				pgs[3] = 2255696063
				pgs[4] = 3268785498
				pgs[6] = 0534627512
				pgs[7] = 1644779301
				pgs[8] = 0411350546
				pgs[9] = 3269704350
				pgs[10] = "ZY.FT"
		    elseif pg == "霜陨_英女侠_普通" then--1是攻击 2是第二种形态攻击 3是死亡  4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包
				pgs[1] = 3617687903
				pgs[2] = 3617687903
				pgs[3] = 3055706917
				pgs[4] = 0667306037
				pgs[6] = 4248825161
				pgs[7] = 0234849785
				pgs[8] = 0336164631
				pgs[9] = 2174583476
				pgs[10] = "ZY.FT"

			elseif pg == "金龙双剪_英女侠_普通" then
				pgs[1] = 2024899093
				pgs[2] = 2024899093
				pgs[3] = 3727381444
				pgs[4] = 2340031682
				pgs[6] = 1687704448
				pgs[7] = 573860286
				pgs[8] = 1396936106
				pgs[9] = 2113820492
				pgs[10] = "ZY.FT"
			elseif pg == "仙人指路_英女侠_普通" then
				pgs[1] = 1536229276
				pgs[2] = 1536229276
				pgs[3] = 3074198838
				pgs[4] = 781619913
				pgs[6] = 2533848487
				pgs[7] = 4069179535
				pgs[8] = 1975224690
				pgs[9] = 1681443649
				pgs[10] = "ZY.FT"
		    elseif pg == "龙筋_英女侠_普通" then
				pgs[1] = 983659718
				pgs[2] = 983659718
				pgs[3] = 4209944243
				pgs[4] = 1910275062
				pgs[6] = 2426563525
				pgs[7] = 3749832411
				pgs[8] = 277618128
				pgs[9] = 1847888299
				pgs[10] = "ZY.FT"

    -------------------------------------------------------------------------------------------------------
			elseif pg == "无极丝_舞天姬_普通" then
				pgs[1] = 614245463
				pgs[2] = 614245463
				pgs[3] = 1946824091
				pgs[4] = 694856255
				pgs[6] = 2175791405
				pgs[7] = 448343009
				pgs[8] = 2148337924
				pgs[9] = 2923284079
				pgs[10] = "ZY.FT"
		    elseif pg == "舞天姬_普通" then
				pgs[1] = 2774401093
				pgs[2] = 2774401093
				pgs[3] = 365985475
				pgs[4] = 86275401
				pgs[6] = 3443216643
				pgs[7] = 1786109105
				pgs[8] = 1241775590
				pgs[9] = 4118221086
				pgs[10] = "ZY.FT"
		    elseif pg == "黄铜圈_舞天姬_普通" then
				pgs[1] = 1246340040
				pgs[2] = 1246340040
				pgs[3] = 1695349470
				pgs[4] = 2991787754
				pgs[6] = 1524014588
				pgs[7] = 4075994022
				pgs[8] = 504842185
				pgs[9] = 1374023375
				pgs[10] = "ZY.FT"
		    elseif pg == "金刺轮_舞天姬_普通" then
				pgs[1] = 3926831021
				pgs[2] = 3926831021
				pgs[3] = 1774309751
				pgs[4] = 2501160049
				pgs[6] = 2255887722
				pgs[7] = 173996820
				pgs[8] = 4154265461
				pgs[9] = 3161036338
				pgs[10] = "ZY.FT"
		    elseif pg == "五色缎带_舞天姬_普通" then
				pgs[1] = 13536912
				pgs[2] = 13536912
				pgs[3] = 3018065077
				pgs[4] = 4096686404
				pgs[6] = 2509295261
				pgs[7] = 3306367653
				pgs[8] = 4257026415
				pgs[9] = 213886082
				pgs[10] = "ZY.FT"
		    elseif pg == "舞天姬_飘" then
				pgs[1] = 2774401093
				pgs[2] = 2774401093
				pgs[3] = 365985475
				pgs[4] = 86275401
				pgs[6] = 3443216643
				pgs[7] = 1786109105
				pgs[8] = 1241775590
				pgs[9] = 4118221086
				pgs[10] = "ZY.FT"
		    elseif pg == "蛇形月_舞天姬_普通" then
				pgs[1] = 3668441645
				pgs[2] = 3668441645
				pgs[3] = 1423059990
				pgs[4] = 9780356
				pgs[6] = 2142449311
				pgs[7] = 2142449311
				pgs[8] = 4149929342
				pgs[9] = 712091462
				pgs[10] = "ZY.FT"
		    elseif pg == "舞天姬_环" then
				pgs[1] = 4193428466
				pgs[2] = 4193428466
				pgs[3] = 3043244041
				pgs[4] = 2814262767
				pgs[6] = 4189229797
				pgs[7] = 3277294463
				pgs[8] = 3942685609
				pgs[9] = 610824997
				pgs[10] = "ZY.FT"
		    elseif pg == "七彩罗刹_舞天姬_普通" then
				pgs[1] = 1449553165
				pgs[2] = 1449553165
				pgs[3] = 2145124847
				pgs[4] = 4157162296
				pgs[6] = 1859257571
				pgs[7] = 2698141768
				pgs[8] = 2971905394
				pgs[9] = 2020209393
				pgs[10] = "ZY.FT"
		    elseif pg == "此最相思_舞天姬_普通" then
				pgs[1] = 1976958564
				pgs[2] = 1976958564
				pgs[3] = 3060982721
				pgs[4] = 3902626006
				pgs[6] = 1239150663
				pgs[7] = 1817978990
				pgs[8] = 3578667390
				pgs[9] = 2374794875
				pgs[10] = "ZY.FT"
			elseif pg == "揽月摘星_舞天姬_普通" then
				pgs[1] = 3095847005
				pgs[2] = 3095847005
				pgs[3] = 0328405538
				pgs[4] = 2694329428
				pgs[6] = 4179340991
				pgs[7] = 2099180209
				pgs[8] = 0795965688
				pgs[9] = 1691850913
				pgs[10] = "ZY.FT"
			elseif pg == "九霄_舞天姬_普通" then
				pgs[1] = 1709199725
				pgs[2] = 1709199725
				pgs[3] = 4183907044
				pgs[4] = 0940501812
				pgs[6] = 1332030559
				pgs[7] = 3784736116
				pgs[8] = 1618775589
				pgs[9] = 0895130345
				pgs[10] = "ZY.FT"
		    elseif pg == "彩虹_舞天姬_普通" then
				pgs[1] = 3921702263
				pgs[2] = 3921702263
				pgs[3] = 3711439731
				pgs[4] = 261592524
				pgs[6] = 3826935762
				pgs[7] = 1461988918
				pgs[8] = 658865406
				pgs[9] = 2510075859
				pgs[10] = "ZY.FT"
		    elseif pg == "金玉双环_舞天姬_普通" then
				pgs[1] = 3279495962
				pgs[2] = 3279495962
				pgs[3] = 1189482605
				pgs[4] = 2148299765
				pgs[6] = 3630009301
				pgs[7] = 3263066954
				pgs[8] = 3371000519
				pgs[9] = 2240511279
				pgs[10] = "ZY.FT"
		    elseif pg == "月光双环_舞天姬_普通" then
				pgs[1] = 3165243568
				pgs[2] = 3165243568
				pgs[3] = 16282348
				pgs[4] = 2288648467
				pgs[6] = 3693039080
				pgs[7] = 2693457978
				pgs[8] = 3984861090
				pgs[9] = 1429132352
				pgs[10] = "ZY.FT"
		    elseif pg == "碧波_舞天姬_普通" then
				pgs[1] = 1034789143
				pgs[2] = 1034789143
				pgs[3] = 4113412723
				pgs[4] = 2355929470
				pgs[6] = 2655943207
				pgs[7] = 2855246102
				pgs[8] = 241555200
				pgs[9] = 1141838919
				pgs[10] = "ZY.FT"
		    elseif pg == "流云_舞天姬_普通" then
				pgs[1] = 4195857049
				pgs[2] = 4195857049
				pgs[3] = 2514907580
				pgs[4] = 972420940
				pgs[6] = 198547491
				pgs[7] = 2802292186
				pgs[8] = 3885561582
				pgs[9] = 1837939421
				pgs[10] = "ZY.FT"
		    elseif pg == "如意_舞天姬_普通" then
				pgs[1] = 3267454880
				pgs[2] = 3267454880
				pgs[3] = 1911031086
				pgs[4] = 2351280146
				pgs[6] = 850292701
				pgs[7] = 500162329
				pgs[8] = 1166644566
				pgs[9] = 197968673
				pgs[10] = "ZY.FT"
		    elseif pg == "晃金仙绳_舞天姬_普通" then
				pgs[1] = 480998208
				pgs[2] = 480998208
				pgs[3] = 536662357
				pgs[4] = 1983509384
				pgs[6] = 2029228117
				pgs[7] = 1363856210
				pgs[8] = 66546999
				pgs[9] = 4226984468
				pgs[10] = "ZY.FT"
		    elseif pg == "别情离恨_舞天姬_普通" then
				pgs[1] = 799504916
				pgs[2] = 799504916
				pgs[3] = 842553389
				pgs[4] = 1982103646
				pgs[6] = 2210222918
				pgs[7] = 1858157676
				pgs[8] = 1117799210
				pgs[9] = 3296383020
				pgs[10] = "ZY.FT"
		    elseif pg == "九天金线_舞天姬_普通" then
				pgs[1] = 1669961605
				pgs[2] = 1669961605
				pgs[3] = 280626373
				pgs[4] = 2737030257
				pgs[6] = 1711064047
				pgs[7] = 2155201472
				pgs[8] = 2934052938
				pgs[9] = 2722500657
				pgs[10] = "ZY.FT"
		    elseif pg == "无关风月_舞天姬_普通" then
				pgs[1] = 3653729862
				pgs[2] = 3653729862
				pgs[3] = 2118476708
				pgs[4] = 1290614140
				pgs[6] = 2011121195
				pgs[7] = 3035499198
				pgs[8] = 3842984317
				pgs[9] = 1519396140
				pgs[10] = "ZY.FT"
		    elseif pg == "朝夕_舞天姬_普通" then
				pgs[1] = 2395353653
				pgs[2] = 2395353653
				pgs[3] = 0708502548
				pgs[4] = 3224442340
				pgs[6] = 3649114993
				pgs[7] = 2271525100
				pgs[8] = 3918368321
				pgs[9] = 3452351752
				pgs[10] = "ZY.FT"
		    elseif pg == "秋水落霞_舞天姬_普通" then
				pgs[1] = 1202111998
				pgs[2] = 1202111998
				pgs[3] = 2037655830
				pgs[4] = 3049902097
				pgs[6] = 690602552
				pgs[7] = 1342960347
				pgs[8] = 3838882501
				pgs[9] = 1483726919
				pgs[10] = "ZY.FT"
		    elseif pg == "乾坤_舞天姬_普通" then
				pgs[1] = 266685423
				pgs[2] = 266685423
				pgs[3] = 2851409574
				pgs[4] = 3890863565
				pgs[6] = 1515127869
				pgs[7] = 256273093
				pgs[8] = 2425863420
				pgs[9] = 615875314
				pgs[10] = "ZY.FT"
    ---------------------------------------------------------------------------------------
			elseif pg == "狼牙刀_巨魔王_普通" then
				pgs[1] = 1348040770
				pgs[2] = 1348040770
				pgs[3] = 352421824
				pgs[4] = 1800113692
				pgs[6] = 1284246655
				pgs[7] = 4283867446
				pgs[8] = 1391628501
				pgs[9] = 2232000579
				pgs[10] = "ZY.FT"
			elseif pg == "双弦钺_巨魔王_普通" then
				pgs[1] = 1793132112
				pgs[2] = 1793132112
				pgs[3] = 1963828881
				pgs[4] = 3898773470
				pgs[6] = 2550126650
				pgs[7] = 153091850
				pgs[8] = 544328121
				pgs[9] = 2797209389
				pgs[10] = "ZY.FT"
			elseif pg == "巨魔王_普通" then
				pgs[1] = 508905397
				pgs[2] = 508905397
				pgs[3] = 4036925699
				pgs[4] = 1635724392
				pgs[6] = 2639845870
				pgs[7] = 2750701818
				pgs[8] = 2273495960
				pgs[9] = 4081980249
				pgs[10] = "ZY.FT"
			elseif pg == "巨魔王_斧" then
				pgs[1] = 508905397
				pgs[2] = 508905397
				pgs[3] = 4036925699
				pgs[4] = 1635724392
				pgs[6] = 2639845870
				pgs[7] = 2750701818
				pgs[8] = 2273495960
				pgs[9] = 4081980249
				pgs[10] = "ZY.FT"
			elseif pg == "金背大砍刀_巨魔王_普通" then
				pgs[1] = 2076710374
				pgs[2] = 2076710374
				pgs[3] = 1462666995
				pgs[4] = 1595258466
				pgs[6] = 3808390705
				pgs[7] = 3112271650
				pgs[8] = 1162958128
				pgs[9] = 3317931600
				pgs[10] = "ZY.FT"
			elseif pg == "柳叶刀_巨魔王_普通" then
				pgs[1] = 3522958092
				pgs[2] = 3522958092
				pgs[3] = 2519475388
				pgs[4] = 3788202981
				pgs[6] = 333884425
				pgs[7] = 3427345453
				pgs[8] = 1323639728
				pgs[9] = 4266255286
				pgs[10] = "ZY.FT"
			elseif pg == "乌金鬼头镰_巨魔王_普通" then
				pgs[1] = 208276143
				pgs[2] = 208276143
				pgs[3] = 3914981211
				pgs[4] = 1471811059
				pgs[6] = 1976211634
				pgs[7] = 153091850
				pgs[8] = 3647700056
				pgs[9] = 4019257677
				pgs[10] = "ZY.FT"
			elseif pg == "巨魔王_刀" then
				pgs[1] = 1518169819
				pgs[2] = 1518169819
				pgs[3] = 2951893280
				pgs[4] = 533876880
				pgs[6] = 747524185
				pgs[7] = 594523269
				pgs[8] = 3417945932
				pgs[9] = 2896805672
				pgs[10] = "ZY.FT"
			elseif pg == "青铜斧_巨魔王_普通" then
				pgs[1] = 182516681
				pgs[2] = 182516681
				pgs[3] = 433361276
				pgs[4] = 3396040890
				pgs[6] = 3108223122
				pgs[7] = 1776525498
				pgs[8] = 3618533458
				pgs[9] = 2670573290
				pgs[10] = "ZY.FT"
			elseif pg == "晓风残月_巨魔王_普通" then
				pgs[1] = 684541206
				pgs[2] = 684541206
				pgs[3] = 575760044
				pgs[4] = 2922449802
				pgs[6] = 578321277
				pgs[7] = 1145919811
				pgs[8] = 1959824975
				pgs[9] = 2531067724
				pgs[10] = "ZY.FT"
			elseif pg == "元神禁锢_巨魔王_普通" then
				pgs[1] = 1611966521
				pgs[2] = 1611966521
				pgs[3] = 2319213890
				pgs[4] = 3825599050
				pgs[6] = 2980430167
				pgs[7] = 2239466708
				pgs[8] = 1421663600
				pgs[9] = 2749153529
				pgs[10] = "ZY.FT"
			elseif pg == "护法灭魔_巨魔王_普通" then
				pgs[1] = 1083493637
				pgs[2] = 1083493637
				pgs[3] = 812737125
				pgs[4] = 2551291495
				pgs[6] = 4082536262
				pgs[7] = 2847560160
				pgs[8] = 1955001763
				pgs[9] = 4262995164
				pgs[10] = "ZY.FT"
			elseif pg == "碧血干戚_巨魔王_普通" then
				pgs[1] = 0325617872
				pgs[2] = 0325617872
				pgs[3] = 2652356421
				pgs[4] = 1634024596
				pgs[6] = 2566297187
				pgs[7] = 1516430527
				pgs[8] = 1859893291
				pgs[9] = 1309789755
				pgs[10] = "ZY.FT"
			elseif pg == "裂天_巨魔王_普通" then
				pgs[1] = 3770058550
				pgs[2] = 3770058550
				pgs[3] = 0606207925
				pgs[4] = 4223217027
				pgs[6] = 3564628022
				pgs[7] = 2046633309
				pgs[8] = 2119151502
				pgs[9] = 1758406529
				pgs[10] = "ZY.FT"
			elseif pg == "血刃_巨魔王_普通" then
				pgs[1] = 1939335498
				pgs[2] = 1939335498
				pgs[3] = 877636726
				pgs[4] = 916167152
				pgs[6] = 867523001
				pgs[7] = 3564043256
				pgs[8] = 776791724
				pgs[9] = 1515674466
				pgs[10] = "ZY.FT"
			elseif pg == "屠龙_巨魔王_普通" then
				pgs[1] = 1484416784
				pgs[2] = 1484416784
				pgs[3] = 1775621951
				pgs[4] = 1549828750
				pgs[6] = 3154364234
				pgs[7] = 993921470
				pgs[8] = 2220195117
				pgs[9] = 2747950696
				pgs[10] = "ZY.FT"
			elseif pg == "斩妖泣血_巨魔王_普通" then
				pgs[1] = 1896056556
				pgs[2] = 1896056556
				pgs[3] = 399651097
				pgs[4] = 335656095
				pgs[6] = 4247090300
				pgs[7] = 3557597697
				pgs[8] = 131374191
				pgs[9] = 3408927878
				pgs[10] = "ZY.FT"
			elseif pg == "业火三灾_巨魔王_普通" then
				pgs[1] = 4258129878
				pgs[2] = 4258129878
				pgs[3] = 3927074237
				pgs[4] = 0727738647
				pgs[6] = 0460666887
				pgs[7] = 3086041675
				pgs[8] = 0213811481
				pgs[9] = 2227980701
				pgs[10] = "ZY.FT"
			elseif pg == "鸣鸿_巨魔王_普通" then
				pgs[1] = 2725911621
				pgs[2] = 2725911621
				pgs[3] = 1064946168
				pgs[4] = 0641529870
				pgs[6] = 1154664012
				pgs[7] = 2387777949
				pgs[8] = 0310225545
				pgs[9] = 2338581088
				pgs[10] = "ZY.FT"
			elseif pg == "肃魂_巨魔王_普通" then
				pgs[1] = 3727576188
				pgs[2] = 3727576188
				pgs[3] = 3246311288
				pgs[4] = 4127273897
				pgs[6] = 1400005662
				pgs[7] = 3888898365
				pgs[8] = 2920150876
				pgs[9] = 696981765
				pgs[10] = "ZY.FT"
			elseif pg == "破魄_巨魔王_普通" then
				pgs[1] = 319488423
				pgs[2] = 319488423
				pgs[3] = 276199838
				pgs[4] = 3984718092
				pgs[6] = 3043299876
				pgs[7] = 2152441077
				pgs[8] = 4105273607
				pgs[9] = 1812185996
				pgs[10] = "ZY.FT"
			elseif pg == "五丁开山_巨魔王_普通" then
				pgs[1] = 2764988110
				pgs[2] = 2764988110
				pgs[3] = 658496727
				pgs[4] = 1647167110
				pgs[6] = 3579833021
				pgs[7] = 1948978116
				pgs[8] = 1824676110
				pgs[9] = 1166051005
				pgs[10] = "ZY.FT"
			elseif pg == "无敌_巨魔王_普通" then
				pgs[1] = 1778946713
				pgs[2] = 1778946713
				pgs[3] = 524617246
				pgs[4] = 1841084145
				pgs[6] = 2152432898
				pgs[7] = 2969721676
				pgs[8] = 1229188957
				pgs[9] = 788779469
				pgs[10] = "ZY.FT"
			elseif pg == "偃月青龙_巨魔王_普通" then
				pgs[1] = 4010127360
				pgs[2] = 4010127360
				pgs[3] = 1888302904
				pgs[4] = 1208038344
				pgs[6] = 1840793383
				pgs[7] = 1348825008
				pgs[8] = 868863512
				pgs[9] = 514498611
				pgs[10] = "ZY.FT"
			elseif pg == "冷月_巨魔王_普通" then
				pgs[1] = 432866760
				pgs[2] = 432866760
				pgs[3] = 1400294209
				pgs[4] = 2104157931
				pgs[6] = 1649287571
				pgs[7] = 3373155289
				pgs[8] = 3573839422
				pgs[9] = 914206326
				pgs[10] = "ZY.FT"
	--------------------------------------------------------------------------------------------------
				elseif pg == "偃无师_普通" then
					pgs[1] = 0xC8FDA320
					pgs[2] = 0xC8FDA320
					pgs[3] = 0xC8FDA263
					pgs[4] = 0xC8FDA404
					pgs[6] = 0xC8FDA204
					pgs[7] = 0xC8FDA138
					pgs[8] = 0x0C8FDA63
					pgs[9] = 0xC8FDA352
					pgs[10] = "ZY.FT"
				elseif pg == "偃无师_巨剑" then
					pgs[1] = 0xC8FDA320
					pgs[2] = 0xC8FDA320
					pgs[3] = 0xC8FDA179
					pgs[4] = 0x00C8FDA5
					pgs[6] = 0xC8FDA204
					pgs[7] = 0xC8FDA138
					pgs[8] = 0x0C8FDA63
					pgs[9] = 0xC8FDA352
					pgs[10] = "ZY.FT"
				elseif pg == "钝铁重剑_偃无师_普通" then
					pgs[1] = 0xC8FDA232
					pgs[2] = 0xC8FDA232
					pgs[3] = 0xC8FDA237
					pgs[4] = 0xC8FDA369
					pgs[6] = 0xC8FDA398
					pgs[7] = 0xC8FDA394
					pgs[8] = 0xC8FDA398
					pgs[9] = 0xC8FDA324
					pgs[10] = "ZY.FT"
				elseif pg == "壁玉长铗_偃无师_普通" then
					pgs[1] = 0xC8FDA123
					pgs[2] = 0xC8FDA123
					pgs[3] = 0xC8FDA321
					pgs[4] = 0xC8FDA144
					pgs[6] = 0x0C8FDA68
					pgs[7] = 0x0C8FDA94
					pgs[8] = 0xC8FDA403
					pgs[9] = 0xC8FDA220
					pgs[10] = "ZY.FT"
				elseif pg == "惊涛雪_偃无师_普通" then
					pgs[1] = 0xC8FDA192
					pgs[2] = 0xC8FDA192
					pgs[3] = 0xC8FDA433
					pgs[4] = 0x0C8FDA19
					pgs[6] = 0xC8FDA143
					pgs[7] = 0xC8FDA285
					pgs[8] = 0xC8FDA141
					pgs[9] = 0x0C8FDA80
					pgs[10] = "ZY.FT"
				elseif pg == "鸦九_偃无师_普通" then
					pgs[1] = 0xC8FDA210
					pgs[2] = 0xC8FDA210
					pgs[3] = 0x0C8FDA29
					pgs[4] = 0xC8FDA317
					pgs[6] = 0xC8FDA156
					pgs[7] = 0xC8FDA104
					pgs[8] = 0xC8FDA286
					pgs[9] = 0x0C8FDA37
					pgs[10] = "ZY.FT"
				elseif pg == "昆吾_偃无师_普通" then
					pgs[1] = 0xC8FDA303
					pgs[2] = 0xC8FDA303
					pgs[3] = 0x0C8FDA64
					pgs[4] = 0xC8FDA288
					pgs[6] = 0x0C8FDA85
					pgs[7] = 0xC8FDA427
					pgs[8] = 0x0C8FDA91
					pgs[9] = 0xC8FDA444
					pgs[10] = "ZY.FT"
				elseif pg == "弦歌_偃无师_普通" then
					pgs[1] = 0xC8FDA312
					pgs[2] = 0xC8FDA312
					pgs[3] = 0xC8FDA116
					pgs[4] = 0x00C8FDA1
					pgs[6] = 0xC8FDA314
					pgs[7] = 0xC8FDA351
					pgs[8] = 0x0C8FDA22
					pgs[9] = 0xC8FDA360
					pgs[10] = "ZY.FT"
				elseif pg == "墨骨枯麟_偃无师_普通" then
					pgs[1] = 0xC8FDA226
					pgs[2] = 0xC8FDA226
					pgs[3] = 0xC8FDA392
					pgs[4] = 0x00C8FDA2
					pgs[6] = 0xC8FDA388
					pgs[7] = 0xC8FDA257
					pgs[8] = 0x00000063
					pgs[9] = 0xC8FDA378
					pgs[10] = "ZY.FT"
				elseif pg == "腾蛇郁刃_偃无师_普通" then
					pgs[1] = 0xC8FDA152
					pgs[2] = 0xC8FDA152
					pgs[3] = 0xC8FDA140
					pgs[4] = 0x0C8FDA33
					pgs[6] = 0xC8FDA105
					pgs[7] = 0x0C8FDA82
					pgs[8] = 0xC8FDA165
					pgs[9] = 0xC8FDA164
					pgs[10] = "ZY.FT"
				elseif pg == "秋水澄流_偃无师_普通" then -- 1是攻击 2是第二种形态攻击 3是死亡 4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包 11返回
					pgs[1] = 0x0C8FDA11
					pgs[2] = 0x0C8FDA11
					pgs[3] = 0xC8FDA423
					pgs[4] = 0xC8FDA155
					pgs[6] = 0xC8FDA137
					pgs[7] = 0xC8FDA375
					pgs[8] = 0xC8FDA393
					pgs[9] = 0xC8FDA358
					pgs[10] = "ZY.FT"
				elseif pg == "百辟镇魂_偃无师_普通" then
					pgs[1] = 0xC8FDA457
					pgs[2] = 0xC8FDA457
					pgs[3] = 0xC8FDA167
					pgs[4] = 0xC8FDA149
					pgs[6] = 0xC8FDA221
					pgs[7] = 0xC8FDA377
					pgs[8] = 0xC8FDA162
					pgs[9] = 0xC8FDA159
					pgs[10] = "ZY.FT"
				elseif pg == "长息_偃无师_普通" then
					pgs[1] = 0xC8FDA260
					pgs[2] = 0xC8FDA260
					pgs[3] = 0xC8FDA353
					pgs[4] = 0xC8FDA349
					pgs[6] = 0x0C8FDA53
					pgs[7] = 0x0C8FDA17
					pgs[8] = 0xC8FDA114
					pgs[9] = 0xC8FDA201
					pgs[10] = "ZY.FT"
	-----------------------------------------------------------------------------------------
			elseif pg == "桃夭夭_普通" then
				pgs[1] = 0x0C8FDA14
				pgs[2] = 0x0C8FDA14
				pgs[3] = 0x0C8FD720
				pgs[4] = 0x0C8FDA76
				pgs[6] = 0xC8FDA234
				pgs[7] = 0x0C8FDA47
				pgs[8] = 0x0C8FD732
				pgs[9] = 0x0C8FD727
				pgs[10] = "ZY.FT"
			elseif pg == "桃夭夭_灯笼" then
				pgs[1] = 0x0C8FDA47
				pgs[2] = 0x0C8FDA47
				pgs[3] = 0x0C8FD720
				pgs[4] = 0x0C8FDA76
				pgs[6] = 0xC8FDA234
				pgs[7] = 0x0C8FDA47
				pgs[8] = 0x0C8FD732
				pgs[9] = 0xC8FDA287
				pgs[10] = "ZY.FT"
			elseif pg == "素纸灯_桃夭夭_普通" then
				pgs[1] = 0x00C8FD74
				pgs[2] = 0x00C8FD74
				pgs[3] = 0x00C8FD76
				pgs[4] = 0xC8FDA338
				pgs[6] = 0xC8FDA370
				pgs[7] = 0x00C8FD75
				pgs[8] = 0x00C8FD77
				pgs[9] = 0x00C8FD71
				pgs[10] = "ZY.FT"
			elseif pg == "如意宫灯_桃夭夭_普通" then
				pgs[1] = 0x0C8FD745
				pgs[2] = 0x0C8FD745
				pgs[3] = 0xC8FDA227
				pgs[4] = 0x0C8FD744
				pgs[6] = 0x0C8FD749
				pgs[7] = 0x0C8FD746
				pgs[8] = 0x0C8FD748
				pgs[9] = 0xC8FDA381
				pgs[10] = "ZY.FT"
			elseif pg == "玉兔盏_桃夭夭_普通" then
				pgs[1] = 0x0C8FD755
				pgs[2] = 0x0C8FD755
				pgs[3] = 0xC8FDA346
				pgs[4] = 0x0C8FD754
				pgs[6] = 0x0C8FD759
				pgs[7] = 0x0C8FD756
				pgs[8] = 0xC8FDA283
				pgs[9] = 0xC8FDA342
				pgs[10] = "ZY.FT"
			elseif pg == "蟠龙_桃夭夭_普通" then
				pgs[1] = 0xC8FD7152
				pgs[2] = 0xC8FD7152
				pgs[3] = 0xC8FD7154
				pgs[4] = 0xC8FD7151
				pgs[6] = 0xC8FD7155
				pgs[7] = 0xC8FD7153
				pgs[8] = 0xC8FD7155
				pgs[9] = 0xC8FD7149
				pgs[10] = "ZY.FT"
			elseif pg == "云鹤_桃夭夭_普通" then
				pgs[1] = 0xC8FDA330
				pgs[2] = 0xC8FDA330
				pgs[3] = 0x0C8FD747
				pgs[4] = 0xC8FD7142
				pgs[6] = 0x0C8FDA86
				pgs[7] = 0xC8FDA248
				pgs[8] = 0xC8FD7145
				pgs[9] = 0xC8FDA418
				pgs[10] = "ZY.FT"
			elseif pg == "风荷_桃夭夭_普通" then
				pgs[1] = 0x0C8FD785
				pgs[2] = 0x0C8FD785
				pgs[3] = 0x0C8FD787
				pgs[4] = 0x0C8FD784
				pgs[6] = 0x0C8FD750
				pgs[7] = 0x0C8FD786
				pgs[8] = 0x0C8FD790
				pgs[9] = 0x0C8FD782
				pgs[10] = "ZY.FT"
			elseif pg == "金风玉露_桃夭夭_普通" then
				pgs[1] = 0x0C8FD716
				pgs[2] = 0x0C8FD716
				pgs[3] = 0xC8FD7164
				pgs[4] = 0x0C8FD715
				pgs[6] = 0xC8FD7166
				pgs[7] = 0x00000067
				pgs[8] = 0x0C8FD712
				pgs[9] = 0x0C8FD713
				pgs[10] = "ZY.FT"
			elseif pg == "凰火燎原_桃夭夭_普通" then
				pgs[1] = 0xC8FDA305
				pgs[2] = 0xC8FDA305
				pgs[3] = 0x0C8FD796
				pgs[4] = 0x0C8FD793
				pgs[6] = 0x0C8FD798
				pgs[7] = 0x0C8FD795
				pgs[8] = 0x0C8FD797
				pgs[9] = 0x0C8FD791
				pgs[10] = "ZY.FT"
			elseif pg == "月露清愁_桃夭夭_普通" then
				pgs[1] = 0xC8FDA271
				pgs[2] = 0xC8FDA271
				pgs[3] = 0x0C8FD767
				pgs[4] = 0x00C8FD73
				pgs[6] = 0x0C8FD769
				pgs[7] = 0x0C8FD766
				pgs[8] = 0x0C8FD768
				pgs[9] = 0x0C8FD743
				pgs[10] = "ZY.FT"
			elseif pg == "夭桃侬李_桃夭夭_普通" then
				pgs[1] = 0x0C8FD775
				pgs[2] = 0x0C8FD775
				pgs[3] = 0x0C8FD777
				pgs[4] = 0xC8FDA207
				pgs[6] = 0x0C8FD779
				pgs[7] = 0xC8FDA311
				pgs[8] = 0x0C8FD778
				pgs[9] = 0x0C8FD772
				pgs[10] = "ZY.FT"
			elseif pg == "荒尘_桃夭夭_普通" then
				pgs[1] = 0x0C8FD741
				pgs[2] = 0x0C8FD741
				pgs[3] = 0x0C8FDA44
				pgs[4] = 0x0C8FD740
				pgs[6] = 0x0C8FD742
				pgs[7] = 0x0C8FD737
				pgs[8] = 0xC8FDA463
				pgs[9] = 0x0C8FD738
				pgs[10] = "ZY.FT"
	--------------------------------------------------------------------------------------
			    elseif pg == "鬼潇潇_普通" then
					pgs[1] = 0xC8FD9114
					pgs[2] = 0xC8FD9114
					pgs[3] = 0xC8FD9247
					pgs[4] = 0xC8FDA279
					pgs[6] = 0xC8FDA244
					pgs[7] = 0x00C8FDA4
					pgs[8] = 0xC8FDA202
					pgs[9] = 0xC8FDA176
					pgs[10] = "ZY.FT"
				elseif pg == "鬼潇潇_伞" then
					pgs[1] = 0xC8FD9114
					pgs[2] = 0xC8FD9114
					pgs[3] = 0xC8FD9247
					pgs[4] = 0xC8FDA279
					pgs[6] = 0xC8FDA244
					pgs[7] = 0x00C8FDA4
					pgs[8] = 0xC8FDA202
					pgs[9] = 0xC8FDA176
					pgs[10] = "ZY.FT"
				elseif pg == "红罗伞_鬼潇潇_普通" then
					pgs[1] = 0xC8FD9294
					pgs[2] = 0xC8FD9294
					pgs[3] = 0x0C8FDA62
					pgs[4] = 0xC8FD9209
					pgs[6] = 0x0C8FD921
					pgs[7] = 0xC8FDA146
					pgs[8] = 0xC8FDA270
					pgs[9] = 0x0C8FDA92
					pgs[10] = "ZY.FT"
				elseif pg == "琳琅盖_鬼潇潇_普通" then
					pgs[1] = 0x00C8FD93
					pgs[2] = 0x00C8FD93
					pgs[3] = 0xC8FD9255
					pgs[4] = 0xC8FD9144
					pgs[6] = 0xC8FDA209
					pgs[7] = 0x0C8FD968
					pgs[8] = 0xC8FD9121
					pgs[9] = 0x0C8FD958
					pgs[10] = "ZY.FT"
				elseif pg == "碧火琉璃_鬼潇潇_普通" then
					pgs[1] = 0x0C8FD925
					pgs[2] = 0x0C8FD925
					pgs[3] = 0xC8FD9205
					pgs[4] = 0xC8FDA382
					pgs[6] = 0x0C8FDA13
					pgs[7] = 0x00C8FDA9
					pgs[8] = 0xC8FD9226
					pgs[9] = 0x0C8FD977
					pgs[10] = "ZY.FT"
				elseif pg == "浮生归梦_鬼潇潇_普通" then
					pgs[1] = 0xC8FD9195
					pgs[2] = 0xC8FD9195
					pgs[3] = 0xC8FD9257
					pgs[4] = 0x0C8FDA40
					pgs[6] = 0x0C8FD998
					pgs[7] = 0xC8FD9198
					pgs[8] = 0xC8FDA399
					pgs[9] = 0xC8FD9208
					pgs[10] = "ZY.FT"
				elseif pg == "鬼骨_鬼潇潇_普通" then
					pgs[1] = 0xC13DFBF9
					pgs[2] = 0xC13DFBF9
					pgs[3] = 0xC8FDA356
					pgs[4] = 0x0C8FDA79
					pgs[6] = 0xC8FD9126
					pgs[7] = 0xC8FD9258
					pgs[8] = 0x0C8FD923
					pgs[9] = 0x0C8FD979
					pgs[10] = "ZY.FT"
				elseif pg == "金刚伞_鬼潇潇_普通" then
					pgs[1] = 0xC8FDA183
					pgs[2] = 0xC8FDA183
					pgs[3] = 0xC8FD9307
					pgs[4] = 0xC8FD9191
					pgs[6] = 0x0C8FDA72
					pgs[7] = 0x0C8FD948
					pgs[8] = 0x0C8FD975
					pgs[9] = 0xC8FD9115
					pgs[10] = "ZY.FT"
				elseif pg == "晴雪_鬼潇潇_普通" then
					pgs[1] = 0x0C8FD990
					pgs[2] = 0x0C8FD990
					pgs[3] = 0x00C8FD98
					pgs[4] = 0xC8FD9292
					pgs[6] = 0xC8FD9189
					pgs[7] = 0xC8FD9260
					pgs[8] = 0x00C8FD95
					pgs[9] = 0x0C8FD954
					pgs[10] = "ZY.FT"
				elseif pg == "雪羽穿云_鬼潇潇_普通" then
					pgs[1] = 0x0C8FD929
					pgs[2] = 0x0C8FD929
					pgs[3] = 0x0C8FD941
					pgs[4] = 0x0C8FD986
					pgs[6] = 0xC8FD9237
					pgs[7] = 0xC8FD9155
					pgs[8] = 0x0C8FD970
					pgs[9] = 0xC8FDA364
					pgs[10] = "ZY.FT"
				elseif pg == "月影星痕_鬼潇潇_普通" then
					pgs[1] = 0xC8FD9215
					pgs[2] = 0xC8FD9215
					pgs[3] = 0x0C8FD928
					pgs[4] = 0x0C8FD919
					pgs[6] = 0xC8FD9179
					pgs[7] = 0x0C8FD922
					pgs[8] = 0x0C8FD933
					pgs[9] = 0x00C8FD99
					pgs[10] = "ZY.FT"
				elseif pg == "云梦_鬼潇潇_普通" then
					pgs[1] = 0xC8FDA177
					pgs[2] = 0xC8FDA177
					pgs[3] = 0xC8FD9159
					pgs[4] = 0x0C8FD937
					pgs[6] = 0x00C8FD92
					pgs[7] = 0xC8FD9223
					pgs[8] = 0xC8FDA462
					pgs[9] = 0xC8FD9272
					pgs[10] = "ZY.FT"
				elseif pg == "枕霞_鬼潇潇_普通" then
					pgs[1] = 0xC8FD9230
					pgs[2] = 0xC8FD9230
					pgs[3] = 0xC8FDA203
					pgs[4] = 0xC8FD9238
					pgs[6] = 0x0C8FDA78
					pgs[7] = 0xC8FD9299
					pgs[8] = 0xC8FDA308
					pgs[9] = 0x0C8FD953
					pgs[10] = "ZY.FT"

	---------------------------------------------------0级
		elseif pg == "大海龟_普通"then
		   pgs[6] = 0x4E4577E9
		   pgs[9] = 0xC1BE763B
		   pgs[8] = 0x1EBD486A
		   pgs[5] = 0x62117CB2
		   pgs[4] = 0x73E169FD
		   pgs[1] = 0xB0E742C3
		   pgs[2] = 0xB0E742C3
		   pgs[7] = 0x7247C2AF
		   pgs[3] = 0x302D0DC2
		   pgs[10] = "ZHS.FT"
		elseif pg == "海星_普通"then
		   pgs[6] = 0x19F4F169
		   pgs[9] = 0x4F0800B0
		   pgs[8] = 0x9700A3F8
		   pgs[5] = 0x0ADE0507
		   pgs[4] = 0xB1E2C93E
		   pgs[1] = 0x55BA236D
		   pgs[2] = 0xAE5C76F4
		   pgs[7] = 0x30FC49F4
		   pgs[3] = 0x4AA0CD72
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_海星_普通"then
		   pgs[6] = 0xB493BB73
		   pgs[9] = 0xDE6C1E4F
		   pgs[8] = 0xE7B23E25
		   pgs[5] = 0x78A6332D
		   pgs[4] = 0xED0FEFF2
		   pgs[1] = 0xED1A0CFB
		   pgs[2] = 0x0F9000C0
		   pgs[7] = 0xE614E1D3
		   pgs[3] = 0x76A91B64
		   pgs[10] = "ZHS.FT"
		elseif pg == "护卫_普通"then
		   pgs[6] = 0x03A423A7
		   pgs[9] = 0x3A42F93B
		   pgs[8] = 0xF3F328AA
		   pgs[5] = 0x11497564
		   pgs[4] = 0xADB19915
		   pgs[1]= 0x98D7F621
		   pgs[2]= 0x98D7F621
		   pgs[7] = 0x3190BBAD
		   pgs[3] = 0xAC48C200
		   pgs[10] = "ZHS.FT"
		elseif pg == "巨蛙_普通"then
		   pgs[6] = 0xD69D74DA
		   pgs[9] = 0xFBAECC68
		   pgs[8] = 0xB0E02259
		   pgs[5] = 0x200B54D8
		   pgs[4] = 0x77254EFE
		   pgs[1]= 0xE325DA76
		   pgs[2]= 0xE325DA76
		   pgs[7] = 0xFA163BDB
		   pgs[3] = 0xF15D9B13
		   pgs[10] = "ZHS.FT"
		elseif pg == "泡泡_普通"then
		   pgs[6] = 0x0F19E0B4
		   pgs[9] = 0xC18E5192
		   pgs[8] = 0xDBF24DFE
		   pgs[5] = 0xB3DF46CE
		   pgs[4] = 0x68C7CDF5
		   pgs[1] = 0xC54EA26A
		   pgs[2] = 0xC54EA26A
		   pgs[7] = 0xA8C50581
		   pgs[3] = 0xDE6B91C1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_泡泡_普通"then
		   pgs[6] = 0xD98F0E8D
		   pgs[9] = 0xD1A1B1D9
		   pgs[8] = 0x2EE1F9EB
		   pgs[5] = 0x98F0F97F
		   pgs[4] = 0x85B9593B
		   pgs[1] = 0x74880C0F
		   pgs[2] = 0x74880C0F
		   pgs[7] = 0xB7A81712
		   pgs[3] = 0xD0F1C46A
		   pgs[10] = "ZHS.FT"
		elseif pg == "树怪_普通"then
		   pgs[6] = 0x21E267D5
		   pgs[9] = 0x4B2CACB9
		   pgs[8] = 0x8D2A74DC
		   pgs[5] = 0xC545C3AD
		   pgs[4] = 0xB53BD551
		   pgs[1] = 0x7C76C905
		   pgs[2] = 0x7C76C905
		   pgs[7] = 0x64704A76
		   pgs[3] = 0xEC260188
		   pgs[10] = "ZHS.FT"
		elseif pg == "章鱼_普通"then
		   pgs[6] = 0x3213A07E
		   pgs[9] = 0x60B693AB
		   pgs[8] = 0xC5B7EF91
		   pgs[5] = 0x43F80D95
		   pgs[4] = 0x0DFB1D14
		   pgs[1] = 0x10D23AFA
		   pgs[2] = 0x10D23AFA
		   pgs[7] = 0xDD61EA80
		   pgs[3] = 0x17F264C3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_章鱼_普通"then
		   pgs[6] = 0xA22162BA
		   pgs[9] = 0xCC5BF54D
		   pgs[8] = 0xA33259ED
		   pgs[5] = 0x891B6514
		   pgs[4] = 0xBA7F6112
		   pgs[1] = 0x599CD8B9
		   pgs[2] = 0x599CD8B9
		   pgs[7] = 0x101CA1CB
		   pgs[3] = 0x88BF6138
		   pgs[10] = "ZHS.FT"
	-----------------------------------------5级
		elseif pg == "大蝙蝠_普通"then
		   pgs[6] = 0x10614BC6
		   pgs[9] = 0x154302AE
		   pgs[8] = 0x60B2E461
		   pgs[5] = 0x71B0F3F3
		   pgs[4] = 0x3105EDD1
		   pgs[1] = 0x1F19F135
		   pgs[2] = 0x1F19F135
		   pgs[7] = 0xC7766014
		   pgs[3] = 0x49258F6E
		   pgs[10] = "ZHS.FT"
		elseif pg == "赌徒_普通"then
		   pgs[6] = 0xB59A19B8
		   pgs[9] = 0x701232A0
		   pgs[8] = 0xE0811AE9
		   pgs[5] = 0xD0317D6C
		   pgs[4] = 0x061A19DD
		   pgs[1] = 0x9E8D1971
		   pgs[2] = 0x9E8D1971
		   pgs[7] = 0x7316CFAB
		   pgs[3] = 0x93371DC2
		   pgs[10] = "ZHS.FT"
		elseif pg == "狸_普通" or pg == "浣熊_普通" then
		   pgs[6] = 0xAA452502
		   pgs[9] = 0x2112F5F7
		   pgs[8] = 0xD1153369
		   pgs[5] = 0xF26E81DC
		   pgs[4] = 0xF0ACEB35
		   pgs[1] = 0xB6DB9CCB
		   pgs[2] = 0xB6DB9CCB
		   pgs[7] = 0x897A5492
		   pgs[3] = 0xFF27CA9E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_狸_普通" or pg == "饰品_浣熊_普通" then
		   pgs[6] = 0x528BFDC0
		   pgs[9] = 0xCB40BEC3
		   pgs[8] = 0x42D6AE71
		   pgs[5] = 0x3EF32EA9
		   pgs[4] = 0xCA669286
		   pgs[1] = 0xF4EBB0E4
		   pgs[2] = 0xF4EBB0E4
		   pgs[7] = 0xC76FBB00
		   pgs[3] = 0x8A931E88
		   pgs[10] = "ZHS.FT"
		elseif pg == "海毛虫_普通"then
		   pgs[6] = 0xA9AD0013
		   pgs[9] = 0x96819157
		   pgs[8] = 0x4E7C228F
		   pgs[5] = 0xC68F26D1
		   pgs[4] = 0x88EA2E9A
		   pgs[1] = 0x6DED4692
		   pgs[2] = 0x6DED4692
		   pgs[7] = 0x2876985B
		   pgs[3] = 0xA7B082F6
		   pgs[10] = "ZHS.FT"
		elseif pg == "强盗_普通"then
		   pgs[6] = 0x4E1EC463
		   pgs[9] = 0x593AE526
		   pgs[8] = 0xB5D068D4
		   pgs[5] = 0x4C4D7913
		   pgs[4] = 0x8A23ACE1
		   pgs[1] = 0xF26D871D
		   pgs[2] = 0xF26D871D
		   pgs[7] = 0x53BF0175
		   pgs[3] = 0xAC6D8056
		   pgs[10] = "ZHS.FT"
		elseif pg == "山贼_普通"then
		    pgs[6] = 0xE8A7D963
		   pgs[9] = 0x0FC45589
		   pgs[8] = 0xF873A07A
		   pgs[5] = 0xBBD02E25
		   pgs[4] = 0x5A0B27DE
		   pgs[1] = 0x85C0B736
		   pgs[2] = 0x85C0B736
		   pgs[7] = 0x33F4E27D
		   pgs[3] = 0x73D332F0
		   pgs[10] = "ZHS.FT"
		elseif pg == "野猪_普通"then
		   pgs[6] = 0x5034A927
		   pgs[9] = 0xC8406125
		   pgs[8] = 0xCC7BCA94
		   pgs[5] = 0x646D4779
		   pgs[4] = 0x7012CFD1
		   pgs[1] = 0xA5A50DD2
		   pgs[2] = 0xA5A50DD2
		   pgs[7] = 0x91505EF2
		   pgs[3] = 0x4E6D5EF5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_野猪_普通"then
		   pgs[6] = 0x935A0CD1
		   pgs[9] = 0x39D2948C
		   pgs[8] = 0x0884EBEE
		   pgs[5] = 0xA19CA69F
		   pgs[4] = 0xBE265EC9
		   pgs[1] = 0xD53C5B82
		   pgs[2] = 0xD53C5B82
		   pgs[7] = 0x0FD9040B
		   pgs[3] = 0x77FA9DC1
		   pgs[10] = "ZHS.FT"
	----------------------------------------------------15级
		elseif pg == "蛤蟆精_普通"then
		   pgs[6] = 0xDF976C1E
		   pgs[9] = 0xBFC9CB38
		   pgs[8] = 0x46014800
		   pgs[5] = 0xC738CE07
		   pgs[4] = 0x2A6693A5
		   pgs[1] = 0x42D2F569
		   pgs[2] = 0x42D2F569
		   pgs[7] = 0x23C8A844
		   pgs[3] = 0x0A308336
		   pgs[10] = "ZHS.FT"
		elseif pg == "黑熊_普通"then
		   pgs[6] = 0x3CEC1591
		   pgs[9] = 0xD70C8074
		   pgs[8] = 0x3A6A8611
		   pgs[5] = 0xB4A71E6F
		   pgs[4] = 0x65B88023
		   pgs[1] = 0x155EB9F4
		   pgs[2] = 0x155EB9F4
		   pgs[7] = 0x065141A6
		   pgs[3] = 0xD5760E7A
		   pgs[10] = "ZHS.FT"
		elseif pg == "狐狸精_普通"then
		   pgs[6] = 0xF20F8357
		   pgs[9] = 0x70760D7C
		   pgs[8] = 0xC7535935
		   pgs[5] = 0xF250E0A3
		   pgs[4] = 0xC6D7CC10
		   pgs[1] = 0xB4E430EF
		   pgs[2] = 0xB4E430EF
		   pgs[7] = 0xAF5DD531
		   pgs[3] = 0x42D06952
		   pgs[10] = "ZHS.FT"
		elseif pg == "花妖_普通"then
		   pgs[6] = 0xDE984023
		   pgs[9] = 0x0C34D10D
		   pgs[8] = 0xF3B3EB13
		   pgs[5] = 0x83EFA1C7
		   pgs[4] = 0x539ED20F
		   pgs[1] = 0x82F8B86A
		   pgs[2] = 0x82F8B86A
		   pgs[7] = 0x9733022E
		   pgs[3] = 0x363E98F7
		   pgs[10] = "ZHS.FT"
		elseif pg == "老虎_普通"then
		   pgs[6] = 0x21CE1B76
		   pgs[9] = 0x76E60111
		   pgs[8] = 0x00D5D25E
		   pgs[5] = 0x631A8D70
		   pgs[4] = 0x9A17F3E3
		   pgs[1] = 0x129FA3B0
		   pgs[2] = 0x129FA3B0
		   pgs[7] = 0x129FA3B0
		   pgs[3] = 0x8FDAD586
		   pgs[10] = "ZHS.FT"
		elseif pg == "羊头怪_普通"then
		   pgs[6] = 0x8EE3FB9A
		   pgs[9] = 0xE4042BBF
		   pgs[8] = 0xA5797713
		   pgs[5] = 0xA356F086
		   pgs[4] = 0xD463B910
		   pgs[1] = 0xE8104475
		   pgs[2] = 0xE8104475
		   pgs[7] = 0x833BB968
		   pgs[3] = 0x7098C324
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_羊头怪_普通"then
		   pgs[6] = 0x6E2E511A
		   pgs[9] = 0x37521F80
		   pgs[8] = 0x664C13EA
		   pgs[5] = 0xC2DD7E0D
		   pgs[4] = 0x9CBDEC33
		   pgs[1] = 0x3A707D5C
		   pgs[2] = 0x3A707D5C
		   pgs[7] = 0xF6708D29
		   pgs[3] = 0xFE13F049
		   pgs[10] = "ZHS.FT"
		elseif pg == "骷髅怪_普通"then
		   pgs[6] = 0x780D4A9C
		   pgs[9] = 0x8B36F832
		   pgs[8] = 0xB3680C5C
		   pgs[5] = 0x1E9F18B9
		   pgs[4] = 0x9CC5A01C
		   pgs[1] = 0x9364D8B3
		   pgs[2] = 0x9364D8B3
		   pgs[7] = 0xC2E23DBB
		   pgs[3] = 0x7D2932FA
		   pgs[10] = "ZHS.FT"
	----------------------------------------------------25级
		elseif pg == "狼_普通"then
		   pgs[6] = 0xA94BB647
		   pgs[9] = 0x2C78498A
		   pgs[8] = 0x918799AC
		   pgs[5] = 0x757CF83D
		   pgs[4] = 0x64537853
		   pgs[1] = 0xAE415B39
		   pgs[2] = 0xAE415B39
		   pgs[7] = 0xD1C83AD0
		   pgs[3] = 0xF6739CAE
		   pgs[10] = "ZHS.FT"
		elseif pg == "牛妖_普通"then
		    pgs[6] = 0xF58E06E8
		   pgs[9] = 0x51A98BB9
		   pgs[8] = 0x0CBE58B2
		   pgs[5] = 0x1BA51C92
		   pgs[4] = 0x1CD638BF
		   pgs[1] = 0x7FBB3BAC
		   pgs[2] = 0x7FBB3BAC
		   pgs[7] = 0xB3ED5B24
		   pgs[3] = 0x5C0510BA
		   pgs[10] = "ZHS.FT"
		elseif pg == "虾兵_普通"then
		    pgs[6] = 0xDB68BB03
		   pgs[9] = 0x6926B8F8
		   pgs[8] = 0x9659E556
		   pgs[5] = 0x4A87F03B
		   pgs[4] = 0x7EA8FA68
		   pgs[1] = 0x1F28A466
		   pgs[2] = 0x1F28A466
		   pgs[7] = 0x319A8AE7
		   pgs[3] = 0x496FBB1E
		   pgs[10] = "ZHS.FT"
		elseif pg == "小龙女_普通"then
		    pgs[6] = 0x6821177C
		   pgs[9] = 0x89D04FDB
		   pgs[8] = 0x509DC995
		   pgs[5] = 0x1ACF94DF
		   pgs[4] = 0x36D24BDA
		   pgs[1] = 0xA82A5097
		   pgs[2] = 0xA82A5097
		   pgs[7] = 0xF704761A
		   pgs[3] = 0x06F06887
		   pgs[10] = "ZHS.FT"
		elseif pg == "蟹将_普通"then
		   pgs[6] = 0x60888D2F
		   pgs[9] = 0xFFD5F67A
		   pgs[8] = 0x81405885
		   pgs[5] = 0x5637A867
		   pgs[4] = 0x94A5917C
		   pgs[1] = 0x78846A4D
		   pgs[2] = 0x78846A4D
		   pgs[7] = 0xF42D8D95
		   pgs[3] = 0xF296010A
		   pgs[10] = "ZHS.FT"
		elseif pg == "野鬼_普通"then
		   pgs[6] = 0xD117595B
		   pgs[9] = 0x2A479CCC
		   pgs[8] = 0x6579BFB5
		   pgs[5] = 0x2DE7F9C4
		   pgs[4] = 0x8754CB1D
		   pgs[1] = 0xC1B24BAC
		   pgs[2] = 0xC1B24BAC
		   pgs[7] = 0x5DF30CB8
		   pgs[3] = 0xED88294F
		   pgs[10] = "ZHS.FT"
	-------------------------------------------------35
		elseif pg == "龟丞相_普通"then
		    pgs[6] = 0x51C20B00
		   pgs[9] = 0x19953EFC
		   pgs[8] = 0x9424825C
		   pgs[5] = 0xF9050B96
		   pgs[4] = 0x6DCF1F37
		   pgs[1] = 0xACBDB9E5
		   pgs[2] = 0xACBDB9E5
		   pgs[7] = 0xD3B754A3
		   pgs[3] = 0x433AD572
		   pgs[10] = "ZHS.FT"
		elseif pg == "黑熊精_普通"then
		   pgs[6] = 0x578A6F8A
		   pgs[9] = 0xFB40C501
		   pgs[8] = 0xD0E587C7
		   pgs[5] = 0xA6907BF4
		   pgs[4] = 0x18A8B240
		   pgs[1] = 0x32B2BAE6
		   pgs[2] = 0x32B2BAE6
		   pgs[7] = 0x996F50B4
		   pgs[3] = 0xFADE22F3
		   pgs[10] = "ZHS.FT"
		elseif pg == "僵尸_普通"then
		   pgs[6] = 0x76A5ABFD
		   pgs[9] = 0x178626C1
		   pgs[8] = 0x28E149C0
		   pgs[5] = 0x2E5A460F
		   pgs[4] = 0x2FB5322A
		   pgs[1] = 0xF9321464
		   pgs[2] = 0xF9321464
		   pgs[7] = 0x2A8EE07C
		   pgs[3] = 0x5723F6F9
		   pgs[10] = "ZHS.FT"
		elseif pg == "马面_普通"then
		   pgs[6] = 0xB350A5B4
		   pgs[9] = 0xD55B8806
		   pgs[8] = 0xA9A520DF
		   pgs[5] = 0x2FAB5EF8
		   pgs[4] = 0xFB352928
		   pgs[1] = 0x17A0338A
		   pgs[2] = 0x17A0338A
		   pgs[7] = 0xC1717E79
		   pgs[3] = 0xA7C43DF4
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_马面_普通"then
		   pgs[6] = 0x1FBDD393
		   pgs[9] = 0x6621AC39
		   pgs[8] = 0x81604323
		   pgs[5] = 0x02DA2B1D
		   pgs[4] = 0x6B9F50F6
		   pgs[1] = 0x6DEB11CE
		   pgs[2] = 0x6DEB11CE
		   pgs[7] = 0x2B672E76
		   pgs[3] = 0x50B2FE99
		   pgs[10] = "ZHS.FT"
		elseif pg == "牛头_普通"then
		   pgs[6] = 0xE6E7DE44
		   pgs[9] = 0xA85E8726
		   pgs[8] = 0xCB5FE03A
		   pgs[5] = 0x04FE166F
		   pgs[4] = 0x052D9E0F
		   pgs[1] = 0x75D7E3ED
		   pgs[2] = 0x75D7E3ED
		   pgs[7] = 0x3C4FE166
		   pgs[3] = 0x4CBBEA5A
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_牛头_普通"then
		   pgs[6] = 0xEF5E5215
		   pgs[9] = 0xF5F422A2
		   pgs[8] = 0x602DCB23
		   pgs[5] = 0xFB57A089
		   pgs[4] = 0x8B8273AD
		   pgs[1] = 0x7D011E42
		   pgs[2] = 0x7D011E42
		   pgs[7] = 0x239B8456
		   pgs[3] = 0x2233295C
		   pgs[10] = "ZHS.FT"
		elseif pg == "兔子怪_普通"then
		   pgs[6] = 0x6EEFA438
		   pgs[9] = 0xFC527CFF
		   pgs[8] = 0xEE01BAB5
		   pgs[5] = 0x129D50D3
		   pgs[4] = 0xED788B3D
		   pgs[1] = 0x441588C8
		   pgs[2] = 0x441588C8
		   pgs[7] = 0xCF321F12
		   pgs[3] = 0x8053E23C
		   pgs[10] = "ZHS.FT"
		elseif pg == "蜘蛛精_普通"then
		    pgs[6] = 0x74606705
		   pgs[9] = 0x295D6DC9
		   pgs[8] = 0x9C8327C0
		   pgs[5] = 0x3ABB3170
		   pgs[4] = 0x86E1FD99
		   pgs[1] = 0x1B7B7194
		   pgs[2] = 0x1B7B7194
		   pgs[7] = 0x32201898
		   pgs[3] = 0xB6E6CA75
		   pgs[10] = "ZHS.FT"
	-------------------------------------------------45级
		elseif pg == "白熊_普通"then
		   pgs[6] = 0x24F343D2
		   pgs[9] = 0xE08BBB1A
		   pgs[8] = 0x1A43CBEE
		   pgs[5] = 0x71786293
		   pgs[4] = 0x1AEAF718
		   pgs[1] = 0x881F5EB3
		   pgs[2] = 0x881F5EB3
		   pgs[7] = 0x3E1A5B78
		   pgs[3] = 0x6429E0B5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_白熊_普通"then
		   pgs[6] = 0x308185DC
		   pgs[9] = 0x7BED95E0
		   pgs[8] = 0x34814E10
		   pgs[5] = 0x6DD97782
		   pgs[4] = 0xE69E56FC
		   pgs[1] = 0xE950CCDE
		   pgs[2] = 0xE950CCDE
		   pgs[7] = 0x24BEFE0C
		   pgs[3] = 0x86793301
		   pgs[10] = "ZHS.FT"
		elseif pg == "古代瑞兽_普通"then
		   pgs[6] = 0xD9C108BF
		   pgs[9] = 0x5CF6B96E
		   pgs[8] = 0x4790ACDD
		   pgs[5] = 0xEF6DC8B6
		   pgs[4] = 0xE383DE59
		   pgs[1] = 0x9B35CE8D
		   pgs[2] = 0x9B35CE8D
		   pgs[7] = 0xC944B9F3
		   pgs[3] = 0x8C952FFD
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_古代瑞兽_普通"then
		   pgs[6] = 0x1F671C28
		   pgs[9] = 0x6F641ACC
		   pgs[8] = 0x6BD99577
		   pgs[5] = 0x329DB69D
		   pgs[4] = 0x5281057F
		   pgs[1] = 0x3F236395
		   pgs[2] = 0x3F236395
		   pgs[7] = 0x79B54409
		   pgs[3] = 0xDCE5D149
		   pgs[10] = "ZHS.FT"
		elseif pg == "黑山老妖_普通"then
		   pgs[6] = 0x759DB60F
		   pgs[9] = 0xD0C5FFC8
		   pgs[8] = 0xA75F0C8E
		   pgs[5] = 0x487E44A6
		   pgs[4] = 0x28365622
		   pgs[1] = 0x388A4093
		   pgs[2] = 0x388A4093
		   pgs[7] = 0x1E4B0077
		   pgs[3] = 0x5A9BF84E
		   pgs[10] = "ZHS.FT"
		-- elseif pg == "饰品_黑山老妖_普通"then---------------
		--    pgs[6] = 0x266C5FE5
		--    pgs[9] = 0xE88E7F50
		--    pgs[8] = 0x
		--    pgs[5] = 0x
		--    pgs[4] = 0x3A904439
		--    pgs[1] = 0xBDC9DB09
		--    pgs[2] = 0xBDC9DB09
		--    pgs[7] = 0x7DAC4B6A
		--    pgs[3] = 0x3887EEDE
		--    pgs[10] = "ZHS.FT"
		elseif pg == "蝴蝶仙子_普通"then
		   pgs[6] = 0xD72C8A4B
		   pgs[9] = 0x3A071AEE
		   pgs[8] = 0x5F5F1594
		   pgs[5] = 0x9ED8185E
		   pgs[4] = 0xD5D3DBA9
		   pgs[1] = 0xE432C378
		   pgs[2] = 0xE432C378
		   pgs[7] = 0xB09F33ED
		   pgs[3] = 0xE70BF9D2
		   pgs[10] = "ZHS.FT"
		elseif pg == "雷鸟人_普通"then
		   pgs[6] = 0x398BFB4F
		   pgs[9] = 0x88F6B7E8
		   pgs[8] = 0x36C3648C
		   pgs[5] = 0x7B496468
		   pgs[4] = 0x9C7493B4
		   pgs[1] = 0x7F34308B
		   pgs[2] = 0x7F34308B
		   pgs[7] = 0xF8BFB2A0
		   pgs[3] = 0xEDA2F6A8
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_雷鸟人_普通"then
		   pgs[6] = 0xD2286C28
		   pgs[9] = 0xAFFA205D
		   pgs[8] = 0xB0F41230
		   pgs[5] = 0xE717D6AC
		   pgs[4] = 0x229D72AE
		   pgs[1] = 0xF9CFFFED
		   pgs[2] = 0xF9CFFFED
		   pgs[7] = 0x149556A1
		   pgs[3] = 0x0738AAE2
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------55级
		elseif pg == "地狱战神_普通"then
		   pgs[6] = 0x12807229
		   pgs[9] = 0x5B13FA1C
		   pgs[8] = 0xA4FAC284
		   pgs[5] = 0xDC53E015
		   pgs[4] = 0x8F487B17
		   pgs[1] = 0x3D5634A3
		   pgs[2] = 0x3D5634A3
		   pgs[7] = 0x1A6B6FFB
		   pgs[3] = 0xDF5D2760
		   pgs[10] = "ZHS.FT"
		elseif pg == "风伯_普通"then
		   pgs[6] = 0xF879501E
		   pgs[9] = 0x129ACE33
		   pgs[8] = 0xBBEDAA2D
		   pgs[5] = 0xDE07832C
		   pgs[4] = 0xB478D85B
		   pgs[1] = 0x71146663
		   pgs[2] = 0x71146663
		   pgs[7] = 0x9383411F
		   pgs[3] = 0x972CDC33
		   pgs[10] = "ZHS.FT"
		elseif pg == "天兵_普通"then
		   pgs[6] = 0x2CC4FE10
		   pgs[9] = 0x768E814C
		   pgs[8] = 0xCB876EC4
		   pgs[5] = 0xAF19411C
		   pgs[4] = 0xB9448E01
		   pgs[1] = 0xC5676415
		   pgs[2] = 0xC5676415
		   pgs[7] = 0x46AB1AA0
		   pgs[3] = 0xFF7C5F7C
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_天兵_普通"then
		   pgs[6] = 0xEEBB6DE7
		   pgs[9] = 0xD5DE240A
		   pgs[8] = 0x0F9596C7
		   pgs[5] = 0x986CB277
		   pgs[4] = 0x5551FC0E
		   pgs[1] = 0xD5ED0DF6
		   pgs[2] = 0xD5ED0DF6
		   pgs[7] = 0x89B38274
		   pgs[3] = 0xA6DC5D44
		   pgs[10] = "ZHS.FT"
		elseif pg == "天将_普通"then
		   pgs[6] = 0x04C8C142
		   pgs[9] = 0x884B84CA
		   pgs[8] = 0x9395B542
		   pgs[5] = 0x83DD6605
		   pgs[4] = 0xC5C3FCC4
		   pgs[1] = 0xE513139F
		   pgs[2] = 0xE513139F
		   pgs[7] = 0xB70488BD
		   pgs[3] = 0x9648321B
		   pgs[10] = "ZHS.FT"
	---------------------------------------------------65级   --1是攻击 2是第二种形态攻击 3是死亡 5 返回4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包
		elseif pg == "蚌精_普通"then
		   pgs[6] = 0x24FE9D58
		   pgs[9] = 0xF5FE9E40
		   pgs[8] = 0xAD42BA6B
		   pgs[5] = 0x6A602D51
		   pgs[4] = 0xCE125302
		   pgs[1] = 0x4EE2B9DC
		   pgs[2] = 0x4EE2B9DC
		   pgs[7] = 0xD2A54E5B
		   pgs[3] = 0x07570236
		   pgs[10] = "ZHS.FT"
		elseif pg == "碧水夜叉_普通"then
		   pgs[6] = 0x2CC27C0E
		   pgs[9] = 0x6B988A83
		   pgs[8] = 0xF81B9F3D
		   pgs[5] = 0x68CD0C4C
		   pgs[4] = 0x94037442
		   pgs[1] = 0x735A4452
		   pgs[2] = 0xF2102877
		   pgs[7] = 0x27DD9BD6
		   pgs[3] = 0xC487753D
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_碧水夜叉_普通"then
		   pgs[6] = 0x63EF7648
		   pgs[9] = 0xA455BFA1
		   pgs[8] = 0xE41CD597
		   pgs[5] = 0x61142020
		   pgs[4] = 0xB6B2A0D3
		   pgs[1] = 0x4C4AF499
		   pgs[2] = 0xB9CEAC45
		   pgs[7] = 0x50E526FB
		   pgs[3] = 0x02F57258
		   pgs[10] = "ZHS.FT"
		elseif pg == "凤凰_普通"then
		   pgs[6] = 0x2DD87623
		   pgs[9] = 0xA3DA2B3C
		   pgs[8] = 0x05E61447
		   pgs[5] = 0xBA8F0528
		   pgs[4] = 0x22883068
		   pgs[1] = 0x6B914BF3
		   pgs[2] = 0x6B914BF3
		   pgs[7] = 0x0B5E6653
		   pgs[3] = 0x0DDF2430
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_凤凰_普通"then
		   pgs[6] = 0xBF90783B
		   pgs[9] = 0x88D490D8
		   pgs[8] = 0xCF8F5168
		   pgs[5] = 0xF015A44A
		   pgs[4] = 0xB744D6BA
		   pgs[1] = 0x8A1F82A6
		   pgs[2] = 0x8A1F82A6
		   pgs[7] = 0x68260D8D
		   pgs[3] = 0xD87111EC
		   pgs[10] = "ZHS.FT"
		elseif pg == "雨师_普通"then
		   pgs[6] = 0xBF90783B
		   pgs[9] = 0x88D490D8
		   pgs[8] = 0xCF8F5168
		   pgs[5] = 0xF015A44A
		   pgs[4] = 0xB744D6BA
		   pgs[1] = 0x8A1F82A6
		   pgs[2] = 0x8A1F82A6
		   pgs[7] = 0x68260D8D
		   pgs[3] = 0xD87111EC
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_雨师_普通"then
		   pgs[6] = 0x554E3F20
		   pgs[9] = 0x0DCFBD51
		   pgs[8] = 0x9D24B4B9
		   pgs[5] = 0x80164EFE
		   pgs[4] = 0xA7CE4E53
		   pgs[1] = 0xDAF6992A
		   pgs[2] = 0xDAF6992A
		   pgs[7] = 0x1CE13061
		   pgs[3] = 0x4986E7D9
		   pgs[10] = "ZHS.FT"
		elseif pg == "蛟龙_普通"then
		   pgs[6] = 0x034598C3
		   pgs[9] = 0x5FE3F3C2
		   pgs[8] = 0x1C37C867
		   pgs[5] = 0x9401351B
		   pgs[4] = 0x213AE16A
		   pgs[1] = 0xC01BC830
		   pgs[2] = 0xC01BC830
		   pgs[7] = 0x110072C0
		   pgs[3] = 0x01846532
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_蛟龙_普通"then
		   pgs[6] = 0xA695F1EA
		   pgs[9] = 0x1948F4DF
		   pgs[8] = 0x87DAD062
		   pgs[5] = 0x45467A84
		   pgs[4] = 0xBE9F2A37
		   pgs[1] = 0xACD1EEF4
		   pgs[2] = 0xACD1EEF4
		   pgs[7] = 0x48BDE373
		   pgs[3] = 0x5D452A49
		   pgs[10] = "ZHS.FT"
		elseif pg == "鲛人_普通"then
		   pgs[6] = 0xE2807954
		   pgs[9] = 0x6B3EF2B9
		   pgs[8] = 0xAAD26B18
		   pgs[5] = 0xFF8E8A7F
		   pgs[4] = 0xC6153D40
		   pgs[1] = 0xE4697EB6
		   pgs[2] = 0xB2C6B54B
		   pgs[7] = 0x3625263A
		   pgs[3] = 0xF0F01E69
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_鲛人_普通"then
		   pgs[6] = 0x4050ADB1
		   pgs[9] = 0x4050ADB1
		   pgs[8] = 0x8E9AB5C9
		   pgs[5] = 0x5E5C6F08
		   pgs[4] = 0x4050ADB1
		   pgs[1] = 0xF0EB1315
		   pgs[2] = 0x427A9519
		   pgs[7] = 0x2BAE353F
		   pgs[3] = 0xB17A0D7A
		   pgs[10] = "ZHS.FT"
	----------------------------------------------------75级
		elseif pg == "锦毛貂精_普通"then
		   pgs[6] = 0xC768A820
		   pgs[9] = 0x12A9E525
		   pgs[8] = 0xC02E6F3B
		   pgs[5] = 0xAAECCF22
		   pgs[4] = 0x359676E1
		   pgs[1] = 0xE0755693
		   pgs[2] = 0x5320BA18
		   pgs[7] = 0x260AE8F1
		   pgs[3] = 0x8D7FF839
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_锦毛貂精_普通"then
		   pgs[6] = 0xB18C9EE6
		   pgs[9] = 0x12A9E525
		   pgs[8] = 0xAA708449
		   pgs[5] = 0xA5441CAF
		   pgs[4] = 0xCB9493F3
		   pgs[1] = 0xC36121D8
		   pgs[2] = 0x9DD6164F
		   pgs[7] = 0x47B24D87
		   pgs[3] = 0xE671B4B0
		   pgs[10] = "ZHS.FT"
		elseif pg == "千年蛇魅_普通"then
		   pgs[6] = 0x511FF3AD
		   pgs[9] = 0xBCEE8584
		   pgs[8] = 0x376DF52C
		   pgs[5] = 0x5181966E
		   pgs[4] = 0x312D4B23
		   pgs[1] = 0x8D034242
		   pgs[2] = 0x8D034242
		   pgs[7] = 0x38206D0D
		   pgs[3] = 0x9829C1EE
		   pgs[10] = "ZHS.FT"
		elseif pg == "如意仙子_普通"then
		   pgs[6] = 0xA2C8444D
		   pgs[9] = 0x27F8E12A
		   pgs[8] = 0x9300895F
		   pgs[5] = 0x1988B88C
		   pgs[4] = 0xE436F2B8
		   pgs[1] = 0x07FF0EF2
		   pgs[2] = 0x07FF0EF2
		   pgs[7] = 0xDD102EC4
		   pgs[3] = 0x04EB45F5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_如意仙子_普通"then
		   pgs[6] = 0xC4FFF856
		   pgs[9] = 0xA0D951B1
		   pgs[8] = 0xF84BA110
		   pgs[5] = 0x9F428467
		   pgs[4] = 0xDC72D73F
		   pgs[1] = 0xC4FFF856
		   pgs[2] = 0x03E06944
		   pgs[7] = 0x7E9C3C50
		   pgs[3] = 0x2FBC9E75
		   pgs[10] = "ZHS.FT"
		elseif pg == "犀牛将军人形_普通"then
		   pgs[6] = 0xF4714CEA
		   pgs[9] = 0x35539450
		   pgs[8] = 0xEEBB57B4
		   pgs[5] = 0xA4BA5176
		   pgs[4] = 0x36D92EF0
		   pgs[1] = 0x5D6B20D2
		   pgs[2] = 0x4A38AD27
		   pgs[7] = 0x7F17A414
		   pgs[3] = 0xA5D5C808
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_犀牛将军人形_普通"then
		   pgs[6] = 0xA86A8F7F
		   pgs[9] = 0xAC0C8A3D
		   pgs[8] = 0x67406E9B
		   pgs[5] = 0xCFB46686
		   pgs[4] = 0x6EC38C62
		   pgs[1] = 0x5458DCB5
		   pgs[2] = 0xCE13C203
		   pgs[7] = 0x0F7B3DF2
		   pgs[3] = 0xF900EE6C
		   pgs[10] = "ZHS.FT"
		elseif pg == "犀牛将军兽形_普通"then
		   pgs[6] = 0xC7DEA5CC
		   pgs[9] = 0xE27709A8
		   pgs[8] = 0x6173D3FA
		   pgs[5] = 0x7D445145
		   pgs[4] = 0xD7A458DD
		   pgs[1] = 0x5D7AC9A2
		   pgs[2] = 0x124F43E7
		   pgs[7] = 0x3A9902F8
		   pgs[3] = 0xCB6D3B5F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_犀牛将军兽形_普通"then
		   pgs[6] = 0x7A718D3D
		   pgs[9] = 0x7DE55BEC
		   pgs[8] = 0x8441340B
		   pgs[5] = 0x44E50377
		   pgs[4] = 0xC8D6682A
		   pgs[1] = 0x78A35328
		   pgs[2] = 0x2802C64B
		   pgs[7] = 0x80827189
		   pgs[3] = 0x1F6DFA52
		   pgs[10] = "ZHS.FT"
		elseif pg == "星灵仙子_普通"then
		   pgs[6] = 0x265B5E90
		   pgs[9] = 0x420A8983
		   pgs[8] = 0x38B89F32
		   pgs[5] = 0xC3B4C09E
		   pgs[4] = 0x3E4A6073
		   pgs[1] = 0x9CCED9F3
		   pgs[2] = 0x9CCED9F3
		   pgs[7] = 0xF7545337
		   pgs[3] = 0xD949630E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_星灵仙子_普通"then
		   pgs[6] = 0x39ADB536
		   pgs[9] = 0x0A0DD41A
		   pgs[8] = 0x067611C4
		   pgs[5] = 0x29D8C951
		   pgs[4] = 0x4EEF5E37
		   pgs[1] = 0x352C795F
		   pgs[2] = 0x352C795F
		   pgs[7] = 0x9F443300
		   pgs[3] = 0x8F7A3B54
		   pgs[10] = "ZHS.FT"
		elseif pg == "巡游天神_普通"then
		   pgs[6] = 0xE3E15EAC
		   pgs[9] = 0x678383BC
		   pgs[8] = 0xAC61B31B
		   pgs[5] = 0xF8B8CD2B
		   pgs[4] = 0xA8FF07E2
		   pgs[1] = 0x1E2DDE33
		   pgs[2] = 0x1E2DDE33
		   pgs[7] = 0xB2D419B5
		   pgs[3] = 0xF781FFDE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_巡游天神_普通"then
		   pgs[6] = 0x0F12BC80
		   pgs[9] = 0x25F95B6C
		   pgs[8] = 0x41890659
		   pgs[5] = 0xB525E60E
		   pgs[4] = 0xC6A9D45D
		   pgs[1] = 0xB62B5D4D
		   pgs[2] = 0xB62B5D4D
		   pgs[7] = 0xE516D9F4
		   pgs[3] = 0x4571340F
		   pgs[10] = "ZHS.FT"
		elseif pg == "芙蓉仙子_普通"then
		   pgs[6] = 0x2FEF7615
		   pgs[9] = 0x14EFDFC9
		   pgs[8] = 0xB4028C57
		   pgs[5] = 0x92425E67
		   pgs[4] = 0xDA0F8408
		   pgs[1] = 0x3A696908
		   pgs[2] = 0x3A696908
		   pgs[7] = 0x8260E356
		   pgs[3] = 0x0C5137A5
		   pgs[10] = "ZHS.FT"
	------------------------------------------------85级
		elseif pg == "百足将军_普通"then
		   pgs[6] = 0xC5355942
		   pgs[9] = 0x19ABD8D8
		   pgs[8] = 0xFB85C851
		   pgs[5] = 0x7DEECEBD
		   pgs[4] = 0xB8218F5C
		   pgs[1] = 0xF46A3BD3
		   pgs[2] = 0xF46A3BD3
		   pgs[7] = 0xE158DF15
		   pgs[3] = 0x6A3BECB3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_百足将军_普通"then
		   pgs[6] = 0x659E004B
		   pgs[9] = 0x3FA41F9D
		   pgs[8] = 0xD798EE17
		   pgs[5] = 0x091F0290
		   pgs[4] = 0x7D64C952
		   pgs[1] = 0x705C2C61
		   pgs[2] = 0x705C2C61
		   pgs[7] = 0x4A4E11B8
		   pgs[3] = 0x9F2B2398
		   pgs[10] = "ZHS.FT"
		elseif pg == "镜妖_普通"then
		   pgs[6] = 0x5D14FA93
		   pgs[9] = 0x90E88043
		   pgs[8] = 0x218810C3
		   pgs[5] = 0x218810C3
		   pgs[4] = 0x17FED580
		   pgs[1] = 0x65510E4C
		   pgs[2] = 0x14E37A84
		   pgs[7] = 0x864E1613
		   pgs[3] = 0xDC463BAE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_镜妖_普通"then
		   pgs[6] = 0xF2D1F2C1
		   pgs[9] = 0x337F7E24
		   pgs[8] = 0x40D79821
		   pgs[5] = 0x40D79821
		   pgs[4] = 0xAEAB9F92
		   pgs[1] = 0xEAD30AFB
		   pgs[2] = 0xD3D6D3A2
		   pgs[7] = 0xA25CA982
		   pgs[3] = 0xD1B3F222
		   pgs[10] = "ZHS.FT"
		elseif pg == "泪妖_普通"then
		   pgs[6] = 0xB3660D3A
		   pgs[9] = 0x6D93F6D8
		   pgs[8] = 0x147F84A6
		   pgs[5] = 0x147F84A6
		   pgs[4] = 0x17ACFC2C
		   pgs[1] = 0x6F0B153D
		   pgs[2] = 0xCC9AAEE5
		   pgs[7] = 0x9E599B6C
		   pgs[3] = 0xBB6C68EB
		   pgs[10] = "ZHS.FT"
		elseif pg == "鼠先锋_普通"then
		   pgs[6] = 0xA9634C86
		   pgs[9] = 0xF18EA112
		   pgs[8] = 0xDCD0D49B
		   pgs[5] = 0x20BF03E1
		   pgs[4] = 0x8B6B0959
		   pgs[1] = 0x92A345BC
		   pgs[2] = 0x92A345BC
		   pgs[7] = 0x89F2C157
		   pgs[3] = 0x6A620EE1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_鼠先锋_普通"then
		   pgs[6] = 0x8C74B098
		   pgs[9] = 0x8FCCF76B
		   pgs[8] = 0xE2A1511A
		   pgs[5] = 0xD53C3861
		   pgs[4] = 0xE3CD5F8B
		   pgs[1] = 0x159DAC6E
		   pgs[2] = 0x159DAC6E
		   pgs[7] = 0x6E9B6833
		   pgs[3] = 0xDDE6D3C3
		   pgs[10] = "ZHS.FT"
		elseif pg == "野猪精_普通"then
		   pgs[6] = 0xB6CE5945
		   pgs[9] = 0x44EED87A
		   pgs[8] = 0x98AEB8E6
		   pgs[5] = 0xEE1937AB
		   pgs[4] = 0xBAD48A9F
		   pgs[1] = 0x88FA74F5
		   pgs[2] = 0x88FA74F5
		   pgs[7] = 0x7D2E2675
		   pgs[3] = 0xC58184A3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_野猪精_普通"then
		   pgs[6] = 0x15274EAE
		   pgs[9] = 0x885D699C
		   pgs[8] = 0xDF5C8BC7
		   pgs[5] = 0xB82EFED7
		   pgs[4] = 0x604CEAF5
		   pgs[1] = 0xDF0B08E7
		   pgs[2] = 0xDF0B08E7
		   pgs[7] = 0xC6E17937
		   pgs[3] = 0xAAA3BDB2
		   pgs[10] = "ZHS.FT"
	---------------------------------------------------95级
		elseif pg == "灵符女娲_普通"then
		   pgs[6] = 0x1108DE67
		   pgs[9] = 0x2C707784
		   pgs[8] = 0xFF96E442
		   pgs[5] = 0xC0B35574
		   pgs[4] = 0x1BE201F8
		   pgs[1] = 0x6D780273
		   pgs[2] = 0x98D77093
		   pgs[7] = 0x04FC365D
		   pgs[3] = 0xA37FA080
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_灵符女娲_普通"then
		   pgs[6] = 0x6428D20E
		   pgs[9] = 0x75C58990
		   pgs[8] = 0x958666EF
		   pgs[5] = 0xB1A77CFA
		   pgs[4] = 0xB5922D2B
		   pgs[1] = 0xFF83343D
		   pgs[2] = 0xE35272F5
		   pgs[7] = 0x002F299E
		   pgs[3] = 0x091FC4BB
		elseif pg == "律法女娲_普通"then
		   pgs[6] = 0x82A55C83
		   pgs[9] = 0x96C1B363
		   pgs[8] = 0x48D6EF3F
		   pgs[5] = 0x741BA2DB
		   pgs[4] = 0x4D6C7DA4
		   pgs[1] = 0x09B2BF29
		   pgs[2] = 0xD163F10A
		   pgs[7] = 0x749ADF96
		   pgs[3] = 0x42D246FE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_律法女娲_普通"then
		   pgs[6] = 0x93AF956A
		   pgs[9] = 0x3EF42F82
		   pgs[8] = 0x5B85C52C
		   pgs[5] = 0xFCADE736
		   pgs[4] = 0x937F1557
		   pgs[1] = 0x37C3B528
		   pgs[2] = 0x8A8204C5
		   pgs[7] = 0xCF851323
		   pgs[3] = 0xA0C89C6D
		   pgs[10] = "ZHS.FT"
		elseif pg == "吸血鬼_普通"then
		   pgs[6] = 0xA9B78CF6
		   pgs[9] = 0x9E9C0CEA
		   pgs[8] = 0xA32D6CF3
		   pgs[5] = 0xA3C61281
		   pgs[4] = 0x249719A0
		   pgs[1] = 0xD58C431F
		   pgs[2] = 0x57EDEA8E
		   pgs[7] = 0xA9B78CF6
		   pgs[3] = 0xBFD770E7
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_吸血鬼_普通"then
		   pgs[6] = 0xDE22B5AC
		   pgs[9] = 0xD7BC9A02
		   pgs[8] = 0x176BFBC0
		   pgs[5] = 0xC467B6CB
		   pgs[4] = 0x249719A0
		   pgs[1] = 0xBD096363
		   pgs[2] = 0xD6031BB0
		   pgs[7] = 0xA9B78CF6
		   pgs[3] = 0x1E92C127
		   pgs[10] = "ZHS.FT"
		elseif pg == "阴阳伞_普通"then
		   pgs[6] = 0xA173E35D
		   pgs[9] = 0x176BA0DE
		   pgs[8] = 0xBF06CDDA
		   pgs[5] = 0xBF06CDDA
		   pgs[4] = 0x39ECEBFD
		   pgs[1] = 0xA6C14C68
		   pgs[2] = 0x430F60E2
		   pgs[7] = 0xBE1F3734
		   pgs[3] = 0xA52AB815
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_阴阳伞_普通"then
		   pgs[6] = 0xCE00B5B0
		   pgs[9] = 0xF4DA81F4
		   pgs[8] = 0x19840C7F
		   pgs[5] = 0x19840C7F
		   pgs[4] = 0xA0D65E72
		   pgs[1] = 0x962E6989
		   pgs[2] = 0x330A8E5E
		   pgs[7] = 0x9B051B28
		   pgs[3] = 0xF86ED999
		   pgs[10] = "ZHS.FT"
		elseif pg == "幽灵_普通"then
		   pgs[6] = 0xB3084DB8
		   pgs[9] = 0x61FC976E
		   pgs[8] = 0x1D63E822
		   pgs[5] = 0x677F12B6
		   pgs[4] = 0x59925669
		   pgs[1] = 0xB0202D02
		   pgs[2] = 0x035A0A13
		   pgs[7] = 0xBDA45060
		   pgs[3] = 0xF959E09C
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_幽灵_普通"then
		   pgs[6] = 0x0D3666CE
		   pgs[9] = 0x60668D07
		   pgs[8] = 0xAFF30D5A
		   pgs[5] = 0xFA65730B
		   pgs[4] = 0x383B59A9
		   pgs[1] = 0xE216ABA8
		   pgs[2] = 0x0E840F22
		   pgs[7] = 0xD77B2B80
		   pgs[3] = 0xF233781D
		   pgs[10] = "ZHS.FT"
	---------------------------------------------------105级
		elseif pg == "鬼将_普通"then
		   pgs[6] = 0xBEAED63A
		   pgs[9] = 0x3E11F5D1
		   pgs[8] = 0x41669A49
		   pgs[5] = 0xCA724CAE
		   pgs[4] = 0x3C35328F
		   pgs[1] = 0xA62F51EA
		   pgs[2] = 0x097183AC
		   pgs[7] = 0x2E013607
		   pgs[3] = 0x96CB1D39
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_鬼将_普通"then
		   pgs[6] = 0xDA85C9A1
		   pgs[9] = 0xC5F4E924
		   pgs[8] = 0xDE9481C1
		   pgs[5] = 0x1B01CEC8
		   pgs[4] = 0x18F49463
		   pgs[1] = 0x08D5A386
		   pgs[2] = 0x475C0DFD
		   pgs[7] = 0x24B8DE46
		   pgs[3] = 0x4D08C336
		   pgs[10] = "ZHS.FT"
		elseif pg == "画魂_普通"then
		   pgs[6] = 0x947BA25A
		   pgs[9] = 0x0AA5F60F
		   pgs[8] = 0x9358CF65
		   pgs[5] = 0x9358CF65
		   pgs[4] = 0xB48CAEA6
		   pgs[1] = 0x14257647
		   pgs[2] = 0xCA851961
		   pgs[7] = 0x984B7989
		   pgs[3] = 0x8E0167B2
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_画魂_普通"then
		   pgs[6] = 0xEBCB6424
		   pgs[9] = 0xA0199A08
		   pgs[8] = 0x25A1C181
		   pgs[5] = 0x25A1C181
		   pgs[4] = 0xAB82D9CD
		   pgs[1] = 0xF9812ADB
		   pgs[2] = 0xA4D613ED
		   pgs[7] = 0x698F0258
		   pgs[3] = 0x44E16B54
		   pgs[10] = "ZHS.FT"
		elseif pg == "净瓶女娲_普通"then
		   pgs[6] = 0x3F6E1130
		   pgs[9] = 0x54C4481A
		   pgs[8] = 0x1EF025D1
		   pgs[5] = 0x3DAC290B
		   pgs[4] = 0x7B9277F9
		   pgs[1] = 0xEC6F79F4
		   pgs[2] = 0xFE35FA3C
		   pgs[7] = 0x799965C4
		   pgs[3] = 0x05AE6D51
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_净瓶女娲_普通"then
		   pgs[6] = 0x1A931B54
		   pgs[9] = 0x91097C06
		   pgs[8] = 0x83667587
		   pgs[5] = 0x426B3C28
		   pgs[4] = 0xF2B8FE7E
		   pgs[1] = 0xA4E1AB81
		   pgs[2] = 0x340AE6C3
		   pgs[7] = 0x9D771342
		   pgs[3] = 0x7D5E7924
		   pgs[10] = "ZHS.FT"
		elseif pg == "幽萤娃娃_普通"then
		   pgs[6] = 0xE4992631
		   pgs[9] = 0x199C1663
		   pgs[8] = 0x5A9478EF
		   pgs[5] = 0x140C0320
		   pgs[4] = 0x3288E292
		   pgs[1] = 0x92A04679
		   pgs[2] = 0xF3AB0463
		   pgs[7] = 0x440F8F46
		   pgs[3] = 0xF37D30C4
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_幽萤娃娃_普通"then
		   pgs[6] = 0x115CA397
		   pgs[9] = 0x865C4912
		   pgs[8] = 0xED326C67
		   pgs[5] = 0x71476824
		   pgs[4] = 0x1FD55467
		   pgs[1] = 0xB8CDD6FC
		   pgs[2] = 0x886E00E2
		   pgs[7] = 0x440F8F46
		   pgs[3] = 0x04B2E753
		   pgs[10] = "ZHS.FT"
	----------------------------------------------------125级
		elseif pg == "大力金刚_普通"then
		   pgs[6] = 0xE3587E7B
		   pgs[9] = 0x15BC238B
		   pgs[8] = 0xC73FB514
		   pgs[5] = 0xF3161927
		   pgs[4] = 0x8C14660A
		   pgs[1] = 0x3E0F197F
		   pgs[2] = 0x3E0F197F
		   pgs[7] = 0x07E7D554
		   pgs[3] = 0x67C20DC7
		     pgs[10] = "ZHS.FT"
		elseif pg == "饰品_大力金刚_普通"then
		   pgs[6] = 0x297291AB
		   pgs[9] = 0x32E6398F
		   pgs[8] = 0xD0B45A03
		   pgs[5] = 0x9271D2EB
		   pgs[4] = 0x7AC6AF48
		   pgs[1] = 0x0EC8D421
		   pgs[2] = 0x0EC8D421
		   pgs[7] = 0x3190913C
		   pgs[3] = 0x720E9752
		   pgs[10] = "ZHS.FT"
		elseif pg == "金铙僧_普通"then
		   pgs[6] = 0x57857873
		   pgs[9] = 0x62CD189A
		   pgs[8] = 0x9B3CF0EA
		   pgs[5] = 0x07BE6CB5
		   pgs[4] = 0x15A79F28
		   pgs[1] = 0x7B2B6FC3
		   pgs[2] = 0x38E014D7
		   pgs[7] = 0x2248AA45
		   pgs[3] = 0x48AD22ED
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_金铙僧_普通"then
		   pgs[6] = 0x89B6502A
		   pgs[9] = 0xF8AF3916
		   pgs[8] = 0xABC1C98F
		   pgs[5] = 0xD3942357
		   pgs[4] = 0xC817F608
		   pgs[1] = 0xD565BCF5
		   pgs[2] = 0x3B562BCA
		   pgs[7] = 0xF0389F94
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_金铙僧_普通"then
		   pgs[6] = 0x49A8614E
		   pgs[9] = 0x16136B55
		   pgs[8] = 0xC1E4DE67
		   pgs[5] = 0x5A478D04
		   pgs[4] = 0x90861F82
		   pgs[1] = 0xF93DF26F
		   pgs[2] = 0x35FEE816
		   pgs[7] = 0x10EC72AD
		   pgs[3] = 0x09C6BE0C
		   pgs[10] = "ZHS.FT"
		elseif pg == "灵鹤_普通"then
		   pgs[6] = 0x31288244
		   pgs[9] = 0x691C0877
		   pgs[8] = 0x7CC8F05D
		   pgs[5] = 0xFF1C3C72
		   pgs[4] = 0x69C24857
		   pgs[1] = 0x96C4B1C9
		   pgs[2] = 0x96C4B1C9
		   pgs[7] = 0x111982BE
		   pgs[3] = 0xC46C607E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_灵鹤_普通"then
		   pgs[6] = 0x9779BD2D
		   pgs[9] = 0xCA329530
		   pgs[8] = 0x4FE203F4
		   pgs[5] = 0x0E24D48E
		   pgs[4] = 0x86E86CE1
		   pgs[1] = 0x86E25522
		   pgs[2] = 0x86E25522
		   pgs[7] = 0x441A3452
		   pgs[3] = 0x0F78511B
		   pgs[10] = "ZHS.FT"
		elseif pg == "琴仙_普通"then
		   pgs[6] = 0x6E6567D4
		   pgs[9] = 0xFB047799
		   pgs[8] = 0xD2D16C29
		   pgs[5] = 0xBFD93C2C
		   pgs[4] = 0x595067A7
		   pgs[1] = 0x3262C63D
		   pgs[2] = 0xBAA0035A
		   pgs[7] = 0x42270A8F
		   pgs[3] = 0xC4E0425C
		   pgs[10] = "ZHS.FT"
		elseif pg == "噬天虎_普通"then
		   pgs[6] = 0xAC1EE358
		   pgs[9] = 0x59C6F27E
		   pgs[8] = 0xA63AA400
		   pgs[5] = 0xE3980AE5
		   pgs[4] = 0x39F636C7
		   pgs[1] = 0x85305C40
		   pgs[2] = 0x85305C40
		   pgs[7] = 0xB2C9C2C1
		   pgs[3] = 0x9F90B311
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_噬天虎_普通"then
		   pgs[6] = 0x1B07AA53
		   pgs[9] = 0xAD6F908F
		   pgs[8] = 0x3449865D
		   pgs[5] = 0x40F7E457
		   pgs[4] = 0x6F3CBF51
		   pgs[1] = 0xF57C2F84
		   pgs[2] = 0xF57C2F84
		   pgs[7] = 0xCD60CB7C
		   pgs[3] = 0x384E1C4F
		   pgs[10] = "ZHS.FT"
		elseif pg == "雾中仙_普通"then
		   pgs[6] = 0xA0F8A0DD
		   pgs[9] = 0xFBC04019
		   pgs[8] = 0x88920353
		   pgs[5] = 0xEB9F3B5E
		   pgs[4] = 0xFD266883
		   pgs[1] = 0xF8A2C37A
		   pgs[2] = 0xF8A2C37A
		   pgs[7] = 0xA9B9E763
		   pgs[3] = 0x0402FE3E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_雾中仙_普通"then
		   pgs[6] = 0x8F188EBA
		   pgs[9] = 0xDA15135C
		   pgs[8] = 0xFB118871
		   pgs[5] = 0x5D6F09D8
		   pgs[4] = 0xDC47CD72
		   pgs[1] = 0xF7FBF315
		   pgs[2] = 0xF7FBF315
		   pgs[7] = 0xC45BDCE4
		   pgs[3] = 0x2E7BCAFF
		   pgs[10] = "ZHS.FT"
		elseif pg == "炎魔神_普通"then
		   pgs[6] = 0xE41E0CE2
		   pgs[9] = 0x6C28FDAE
		   pgs[8] = 0x32425E55
		   pgs[5] = 0x0A86F471
		   pgs[4] = 0xE65C789C
		   pgs[1] = 0xF26C7F04
		   pgs[2] = 0xF26C7F04
		   pgs[7] = 0x53542E8E
		   pgs[3] = 0x3C4585B9
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_炎魔神_普通"then
		   pgs[6] = 0xAD793A2C
		   pgs[9] = 0x55E9D89B
		   pgs[8] = 0x6CBCB59E
		   pgs[5] = 0xF3BC1861
		   pgs[4] = 0xD12893EF
		   pgs[1] = 0xA17BA606
		   pgs[2] = 0xA17BA606
		   pgs[7] = 0xF62A21D5
		   pgs[3] = 0xEE8F4DD9
		   pgs[10] = "ZHS.FT"
		elseif pg == "夜罗刹_普通"then
		   pgs[6] = 0x9CE06363
		   pgs[9] = 0xBE6B93F3
		   pgs[8] = 0x343886A2
		   pgs[5] = 0x7A864C26
		   pgs[4] = 0xE7CD4487
		   pgs[1] = 0x0F828F02
		   pgs[2] = 0x0F828F02
		   pgs[7] = 0xBA44D9F1
		   pgs[3] = 0xABDB5874
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_夜罗刹_普通"then
		   pgs[6] = 0xD990BF34
		   pgs[9] = 0x14EF9AAF
		   pgs[8] = 0x107A023E
		   pgs[5] = 0x8621A5BA
		   pgs[4] = 0xF5B12CF7
		   pgs[1] = 0xAA489C53
		   pgs[2] = 0xAA489C53
		   pgs[7] = 0x8F98250F
		   pgs[3] = 0x052E4576
		   pgs[10] = "ZHS.FT"
	-------------------------------------------------------------135
		elseif pg == "红萼仙子_普通"then
		   pgs[6] = 0xE1B0D0A5
		   pgs[9] = 0xD55C7155
		   pgs[8] = 0x2686C3FB
		   pgs[5] = 0x11C1703C
		   pgs[4] = 0x091F0978
		   pgs[1] = 0x329EAA42
		   pgs[2] = 0x91B22697
		   pgs[7] = 0x805E0A09
		   pgs[3] = 0x43B54C2F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_红萼仙子_普通"then
		   pgs[6] = 0x7B0A1F4E
		   pgs[9] = 0x90BA74AC
		   pgs[8] = 0xF138AA84
		   pgs[5] = 0xB5725C69
		   pgs[4] = 0x682964C5
		   pgs[1] = 0xBBDF5342
		   pgs[2] = 0x47574289
		   pgs[7] = 0x2204F904
		   pgs[3] = 0xC3A84CA6
		   pgs[10] = "ZHS.FT"
		elseif pg == "葫芦宝贝_普通"then
		   pgs[6] = 0x64D2F296
		   pgs[9] = 0x74F2939F
		   pgs[8] = 0xD514597D
		   pgs[5] = 0x17B0247D
		   pgs[4] = 0x95DB6FB6
		   pgs[1] = 0x12146B13
		   pgs[2] = 0x76FC8299
		   pgs[7] = 0xAA0CE148
		   pgs[3] = 0x2BCEF41B
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_葫芦宝贝_普通"then
		   pgs[6] = 0x6C359915
		   pgs[9] = 0x451AE40D
		   pgs[8] = 0xBEF64509
		   pgs[5] = 0xA0960AB1
		   pgs[4] = 0x3B66453C
		   pgs[1] = 0x1ACA499E
		   pgs[2] = 0xFDC292F8
		   pgs[7] = 0x96F813AE
		   pgs[3] = 0xAF54DB30
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_葫芦宝贝_普通"then
		   pgs[6] = 0x7823D2D1
		   pgs[9] = 0x55A12BA4
		   pgs[8] = 0xB48B2693
		   pgs[5] = 0x8BAE2FCA
		   pgs[4] = 0x7F6FDBD2
		   pgs[1] = 0x0807A41B
		   pgs[2] = 0x79C272B3
		   pgs[7] = 0x0246720C
		   pgs[3] = 0x03957043
		   pgs[10] = "ZHS.FT"
		elseif pg == "机关人_普通"then
		   pgs[6] = 0xC27A35D2
		   pgs[9] = 0x5049497C
		   pgs[8] = 0x92173C08
		   pgs[5] = 0x48A4505B
		   pgs[4] = 0xF1F94BD5
		   pgs[1] = 0xA77320BF
		   pgs[2] = 0xA77320BF
		   pgs[7] = 0x523AD9CB
		   pgs[3] = 0x08280268
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_机关人_普通"then
		   pgs[6] = 0xB1108478
		   pgs[9] = 0xB224E4D4
		   pgs[8] = 0xF788602C
		   pgs[5] = 0x21897844
		   pgs[4] = 0xF347E911
		   pgs[1] = 0x738AD6CE
		   pgs[2] = 0x738AD6CE
		   pgs[7] = 0x516A9751
		   pgs[3] = 0x3DF10CB9
		   pgs[10] = "ZHS.FT"
		elseif pg == "机关人战车_普通"then
		   pgs[6] = 0xD40A955B
		   pgs[9] = 0xDA1E6B20
		   pgs[8] = 0x6B02069E
		   pgs[5] = 0x6B02069E
		   pgs[4] = 0x5436236D
		   pgs[1] = 0x1542FECB
		   pgs[2] = 0x1542FECB
		   pgs[7] = 0x74EACCE2
		   pgs[3] = 0x46DD9B84
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_机关人战车_普通"then
		   pgs[6] = 0x30A031D6
		   pgs[9] = 0x2775FCCC
		   pgs[8] = 0x36CEDB8E
		   pgs[5] = 0x36CEDB8E
		   pgs[4] = 0x6FB5288D
		   pgs[1] = 0x4CBF8E96
		   pgs[2] = 0x4CBF8E96
		   pgs[7] = 0x22D00FF6
		   pgs[3] = 0xA81A6456
		   pgs[10] = "ZHS.FT"
		elseif pg == "狂豹兽形_普通"then
		   pgs[6] = 0x7650B053
		   pgs[9] = 0x647A0B83
		   pgs[8] = 0x462F51D8
		   pgs[5] = 0x7B8F9EA7
		   pgs[4] = 0x80B72700
		   pgs[1] = 0xCB3623DA
		   pgs[2] = 0xE5D9241B
		   pgs[7] = 0x04E784FA
		   pgs[3] = 0x609605BA
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_狂豹兽形_普通"then
		   pgs[6] = 0xC91653B0
		   pgs[9] = 0x7D787597
		   pgs[8] = 0x2436819D
		   pgs[5] = 0xB323264F
		   pgs[4] = 0xB047483B
		   pgs[1] = 0x126599F1
		   pgs[2] = 0xF380FD75
		   pgs[7] = 0xC75A4DD8
		   pgs[3] = 0x0A65CD8F
		   pgs[10] = "ZHS.FT"
		elseif pg == "龙龟_普通"then
		   pgs[6] = 0x6CEAFF1E
		   pgs[9] = 0xE4FB6A85
		   pgs[8] = 0xC567B951
		   pgs[5] = 0x4F8D3BA7
		   pgs[4] = 0x0369B033
		   pgs[1] = 0xB53C6620
		   pgs[2] = 0xBC7A1CFE
		   pgs[7] = 0x7D57961D
		   pgs[3] = 0xE1E529BC
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_龙龟_普通"then
		   pgs[6] = 0xCD3B36BB
		   pgs[9] = 0xE9458DBB
		   pgs[8] = 0xF519CF79
		   pgs[5] = 0x6D220A91
		   pgs[4] = 0x0369B033
		   pgs[1] = 0xFC7CDE80
		   pgs[2] = 0x5D943CF7
		   pgs[7] = 0xA5B6937F
		   pgs[3] = 0xFA5D6246
		   pgs[10] = "ZHS.FT"
		elseif pg == "猫灵兽形_普通"then
		   pgs[6] = 0xED255792
		   pgs[9] = 0x53BA1CA0
		   pgs[8] = 0xA7F7AFEA
		   pgs[5] = 0x2C2108E4
		   pgs[4] = 0x7FD521EB
		   pgs[1] = 0x7D1B15DC
		   pgs[2] = 0x754D0168
		   pgs[7] = 0xC141E9D0
		   pgs[3] = 0xC1BB091E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_猫灵兽形_普通"then
		   pgs[6] = 0x252F04BA
		   pgs[9] = 0x20DE9268
		   pgs[8] = 0x4C2AF322
		   pgs[5] = 0xF2A8B303
		   pgs[4] = 0x56A8D4D5
		   pgs[1] = 0xB9E809A5
		   pgs[2] = 0x5CC125FA
		   pgs[7] = 0x0DF5B0E4
		   pgs[3] = 0x4E7D70E5
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_猫灵兽形_普通"then
		   pgs[6] = 0xCEB930FC
		   pgs[9] = 0x68185371
		   pgs[8] = 0x7F45C7D3
		   pgs[5] = 0xBE74942A
		   pgs[4] = 0xF2AB6B97
		   pgs[1] = 0x9B1E867C
		   pgs[2] = 0x7502F4F2
		   pgs[7] = 0x3892D237
		   pgs[3] = 0x4C69BB5C
		   pgs[10] = "ZHS.FT"
		elseif pg == "踏云兽_普通"then
		   pgs[6] = 0x33724BBA
		   pgs[9] = 0xA9F3D17A
		   pgs[8] = 0x23E1528D
		   pgs[5] = 0x85D31BA5
		   pgs[4] = 0x7B8F63CF
		   pgs[1] = 0x516D204C
		   pgs[2] = 0xF467075C
		   pgs[7] = 0x5E583382
		   pgs[3] = 0x156E6008
		   pgs[10] = "ZHS.FT"
		elseif pg == "蝎子精_普通"then
		   pgs[6] = 0x41C7F53D
		   pgs[9] = 0x65F2E157
		   pgs[8] = 0x64DC0FBC
		   pgs[5] = 0x145C407A
		   pgs[4] = 0x2A50F0AE
		   pgs[1] = 0x58B231CB
		   pgs[2] = 0x29C847D4
		   pgs[7] = 0x93538640
		   pgs[3] = 0xBFDF1A30
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_蝎子精_普通"then
		   pgs[6] = 0xF3ED4E51
		   pgs[9] = 0x162812C9
		   pgs[8] = 0x8626BE8B
		   pgs[5] = 0xA818B79A
		   pgs[4] = 0x255D0384
		   pgs[1] = 0x7206B355
		   pgs[2] = 0xC40B14F2
		   pgs[7] = 0xA966C9A6
		   pgs[3] = 0x89A61E55
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_蝎子精_普通"then
		   pgs[6] = 0x021AFA79
		   pgs[9] = 0xA830561E
		   pgs[8] = 0xB8937FD7
		   pgs[5] = 0x0C5C5684
		   pgs[4] = 0x695F02C3
		   pgs[1] = 0x257BB6CD
		   pgs[2] = 0x4CD923B6
		   pgs[7] = 0x301FABAA
		   pgs[3] = 0x26777BCC
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------145级
		elseif pg == "巴蛇_普通"then
		   pgs[6] = 0xE636B06F
		   pgs[9] = 0x6AF9DF07
		   pgs[8] = 0x23E46DF2
		   pgs[5] = 0xAE332833
		   pgs[4] = 0xA7FAE263
		   pgs[1] = 0x68308FF7
		   pgs[2] = 0x1DB6DE6F
		   pgs[7] = 0xC2EE1383
		   pgs[3] = 0x5578A329
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_巴蛇_普通"then
		   pgs[6] = 0xE5806D4F
		   pgs[9] = 0x2ADD1492
		   pgs[8] = 0xE8F3D105
		   pgs[5] = 0x50AEEEEA
		   pgs[4] = 0x2ADD1492
		   pgs[1] = 0xE8F3D105
		   pgs[2] = 0xE8F3D105
		   pgs[7] = 0xD55E73C6
		   pgs[3] = 0xA7ECFCFD
		   pgs[10] = "ZHS.FT"
		elseif pg == "机关鸟_普通"then
		   pgs[6] = 0x316AFE01
		   pgs[9] = 0x701A42E0
		   pgs[8] = 0x90F91721
		   pgs[5] = 0x958B00C4
		   pgs[4] = 0xF32491EF
		   pgs[1] = 0x138B00BF
		   pgs[2] = 0xE2EECCC1
		   pgs[7] = 0x48DC6D1E
		   pgs[3] = 0x300243F8
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_机关鸟_普通"then
		   pgs[6] = 0xD16F0139
		   pgs[9] = 0x20EF6B7D
		   pgs[8] = 0x1DEFAAD0
		   pgs[5] = 0xCB089AEB
		   pgs[4] = 0x88EEC7BD
		   pgs[1] = 0x2D11203E
		   pgs[2] = 0x879AA020
		   pgs[7] = 0x1F9F342E
		   pgs[3] = 0x32894C56
		   pgs[10] = "ZHS.FT"
		elseif pg == "机关兽_普通"then
		   pgs[6] = 0x179128CC
		   pgs[9] = 0xB9673740
		   pgs[8] = 0x31EF6204
		   pgs[5] = 0x59B8A4F0
		   pgs[4] = 0xA43A2E28
		   pgs[1] = 0x696D3908
		   pgs[2] = 0xBA1288EA
		   pgs[7] = 0x9ADE59B3
		   pgs[3] = 0xEB325945
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_机关兽_普通"then
		   pgs[6] = 0xF03CB9B8
		   pgs[9] = 0x00B24564
		   pgs[8] = 0x6562B6BA
		   pgs[5] = 0x55E9BB3B
		   pgs[4] = 0x6EB3D66F
		   pgs[1] = 0x6720DA19
		   pgs[2] = 0xF9E032B2
		   pgs[7] = 0xC096B89E
		   pgs[3] = 0x97B8B8A4
		   pgs[10] = "ZHS.FT"
		elseif pg == "连驽车_普通"then
		   pgs[6] = 0x294CEE11
		   pgs[9] = 0x4C43CCFD
		   pgs[8] = 0x9F0EA2F8
		   pgs[5] = 0xB93523EE
		   pgs[4] = 0xE8AD2FB2
		   pgs[1] = 0x2D425A5B
		   pgs[2] = 0xD2B4BAD3
		   pgs[7] = 0x8B01138F
		   pgs[3] = 0xF59B0FEC
		   pgs[10] = "ZHS.FT"
	------------------------------------------------------155级
		elseif pg == "长眉灵猴_普通"then
		   pgs[6] = 0xDD227A4C
		   pgs[9] = 0x9ACB32D6
		   pgs[8] = 0x4C43D1FD
		   pgs[5] = 0x9AE30565
		   pgs[4] = 0x84A834AA
		   pgs[1] = 0x44E17604
		   pgs[2] = 0x44E17604
		   pgs[7] = 0x812A0527
		   pgs[3] = 0x54DD9B88
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_长眉灵猴_普通"then
		   pgs[6] = 0x80F714F5
		   pgs[9] = 0xCE8FA4C7
		   pgs[8] = 0x76437D38
		   pgs[5] = 0x9471B9C4
		   pgs[4] = 0x146ABA49
		   pgs[1] = 0x1F374744
		   pgs[2] = 0x1F374744
		   pgs[7] = 0xBEE70945
		   pgs[3] = 0x664CB9DF
		   pgs[10] = "ZHS.FT"
		elseif pg == "混沌兽_普通"then
		   pgs[6] = 0x79208A5C
		   pgs[9] = 0x92BF8A49
		   pgs[8] = 0x4CBC3FD7
		   pgs[5] = 0x22AB96CF
		   pgs[4] = 0xA0B2A4C0
		   pgs[1] = 0x0F49DDA2
		   pgs[2] = 0x0F49DDA2
		   pgs[7] = 0xDBD75951
		   pgs[3] = 0x5E30118C
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_混沌兽_普通"then
		   pgs[6] = 0x2999EB12
		   pgs[9] = 0x4984A865
		   pgs[8] = 0x59279788
		   pgs[5] = 0x9FCB7CB3
		   pgs[4] = 0xC1D5E833
		   pgs[1] = 0x93F9FB81
		   pgs[2] = 0xC4DB1C7F
		   pgs[7] = 0xBB9D974A
		   pgs[3] = 0x91F20606
		   pgs[10] = "ZHS.FT"
		elseif pg == "巨力神猿_普通"then
		   pgs[6] = 0xD920BF09
		   pgs[9] = 0x1711C384
		   pgs[8] = 0x015EB5F0
		   pgs[5] = 0x998957FB
		   pgs[4] = 0x4BE0E734
		   pgs[1] = 0x09A070E7
		   pgs[2] = 0x09A070E7
		   pgs[7] = 0xB26FBCC9
		   pgs[3] = 0x473EAB27
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_巨力神猿_普通"then
		   pgs[6] = 0xE4D66B65
		   pgs[9] = 0x7C671298
		   pgs[8] = 0x1BDCBE4A
		   pgs[5] = 0xE6C00596
		   pgs[4] = 0x319D2CC8
		   pgs[1] = 0xB51551E5
		   pgs[2] = 0xB51551E5
		   pgs[7] = 0x396F1FF2
		   pgs[3] = 0xF5BE6DE3
		   pgs[10] = "ZHS.FT"
		elseif pg == "狂豹人形_普通"then
		   pgs[6] = 0xC23F4BE2
		   pgs[9] = 0x3D7440B7
		   pgs[8] = 0x4B48D396
		   pgs[5] = 0xD6985217
		   pgs[4] = 0x7684ADE1
		   pgs[1] = 0x6DA3F82F
		   pgs[2] = 0x9C9CBA04
		   pgs[7] = 0x113285C3
		   pgs[3] = 0x321D93B7
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_狂豹人形_普通"then
		   pgs[6] = 0xEA8628BD
		   pgs[9] = 0x68E474BF
		   pgs[8] = 0xC114D3C8
		   pgs[5] = 0x117E5AB0
		   pgs[4] = 0xAFD927D8
		   pgs[1] = 0x7AB878DD
		   pgs[2] = 0x4B54CF50
		   pgs[7] = 0xEFA51000
		   pgs[3] = 0xCD8A8A52
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_狂豹人形_普通"then
		   pgs[6] = 0x8BA088AE
		   pgs[9] = 0x704BD83B
		   pgs[8] = 0x0F0E6B1E
		   pgs[5] = 0x37D5833B
		   pgs[4] = 0xB4330B66
		   pgs[1] = 0x3AD8B8A8
		   pgs[2] = 0x48E8F1DD
		   pgs[7] = 0x4F35ABAA
		   pgs[3] = 0x1850FB34
		   pgs[10] = "ZHS.FT"
		elseif pg == "猫灵人形_普通"then
		   pgs[6] = 0x6623BA7B
		   pgs[9] = 0x7AD5B326
		   pgs[8] = 0xB3BC6AF1
		   pgs[5] = 0x9109CB8C
		   pgs[4] = 0x5ED1D628
		   pgs[1] = 0x9ADAA7FE
		   pgs[2] = 0x445416D9
		   pgs[7] = 0x4C8CBDEE
		   pgs[3] = 0x9AED2889
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_猫灵人形_普通"then
		   pgs[6] = 0x19F7718E
		   pgs[9] = 0xD4307215
		   pgs[8] = 0x7A9CD33C
		   pgs[5] = 0xAFDA4E03
		   pgs[4] = 0x63C23C1F
		   pgs[1] = 0xDAA5FD71
		   pgs[2] = 0x5E90E616
		   pgs[7] = 0xB989251D
		   pgs[3] = 0x79CDBAB9
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_猫灵人形_普通"then
		   pgs[6] = 0x9CAFDA7F
		   pgs[9] = 0x574EA80C
		   pgs[8] = 0x166786E0
		   pgs[5] = 0x03372DB2
		   pgs[4] = 0xE276E26C
		   pgs[1] = 0x0098A72C
		   pgs[2] = 0x6636FD25
		   pgs[7] = 0xE6BADDD3
		   pgs[3] = 0x48FB581C
		   pgs[10] = "ZHS.FT"
		elseif pg == "藤蔓妖花_普通"then
		   pgs[6] = 0xC970B8D0
		   pgs[9] = 0x0983D50D
		   pgs[8] = 0x07F93745
		   pgs[5] = 0x30F1C9E2
		   pgs[4] = 0xE2B2445B
		   pgs[1] = 0xC1611D6D
		   pgs[2] = 0xC5D94B46
		   pgs[7] = 0xAB486FA3
		   pgs[3] = 0x17B4A850
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_藤蔓妖花_普通"then
		   pgs[6] = 0x0C69D45B
		   pgs[9] = 0x34A68B81
		   pgs[8] = 0xCAA3C6B1
		   pgs[5] = 0xBE2EEF6E
		   pgs[4] = 0x02430E4E
		   pgs[1] = 0xFB3087B7
		   pgs[2] = 0xB637A64A
		   pgs[7] = 0x646B9D18
		   pgs[3] = 0x42DC8482
		   pgs[10] = "ZHS.FT"
		elseif pg == "修罗傀儡鬼_普通"then
		   pgs[6] = 0x3090F808
		   pgs[9] = 0xBAEBEAEA
		   pgs[8] = 0x4C5B945B
		   pgs[5] = 0xA2D6BF8C
		   pgs[4] = 0x28E437E2
		   pgs[1] = 0xF1A954D7
		   pgs[2] = 0x2D8299C3
		   pgs[7] = 0x13910678
		   pgs[3] = 0x73729299
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_修罗傀儡鬼_普通"then
		   pgs[6] = 0xA4C0E43E
		   pgs[9] = 0x02C428F0
		   pgs[8] = 0xBCFAAF4B
		   pgs[5] = 0x21F6388D
		   pgs[4] = 0xADD0D660
		   pgs[1] = 0xF797BADE
		   pgs[2] = 0x91FF9026
		   pgs[7] = 0x6F20622A
		   pgs[3] = 0xA1CD1366
		   pgs[10] = "ZHS.FT"
		elseif pg == "蜃气妖_普通"then
		   pgs[6] = 0xA3F8CD7E
		   pgs[9] = 0xC110AF0A
		   pgs[8] = 0x684F4026
		   pgs[5] = 0x39F1D105
		   pgs[4] = 0x2BBB9C11
		   pgs[1] = 0x00C49F8D
		   pgs[2] = 0xCC95E5AB
		   pgs[7] = 0x1C793841
		   pgs[3] = 0xC3968B7E
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_蜃气妖_普通"then
		   pgs[6] = 0xE4ECC415
		   pgs[9] = 0x417C8CEA
		   pgs[8] = 0xA8D0419D
		   pgs[5] = 0xFBA3B6C2
		   pgs[4] = 0x4DC302C0
		   pgs[1] = 0x32E923D4
		   pgs[2] = 0xD275BEF7
		   pgs[7] = 0x6C150CCC
		   pgs[3] = 0x6CC05E1B
		   pgs[10] = "ZHS.FT"
	----------------------------------------------------165
		elseif pg == "金身罗汉_普通"then
		   pgs[6] = 0x37955920
		   pgs[9] = 0xEC96354B
		   pgs[8] = 0xD2B03C99
		   pgs[5] = 0xD2737498
		   pgs[4] = 0x784F490F
		   pgs[1] = 0xC028769E
		   pgs[2] = 0x9C711EDD
		   pgs[7] = 0xD49CA003
		   pgs[3] = 0x15FBF580
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_金身罗汉_普通"then
		   pgs[6] = 0x676E4514
		   pgs[9] = 0xE661D1C4
		   pgs[8] = 0x4DE355A1
		   pgs[5] = 0x1BD399D4
		   pgs[4] = 0x5DAA3352
		   pgs[1] = 0x025E7649
		   pgs[2] = 0xF03453FF
		   pgs[7] = 0xA1592F3C
		   pgs[3] = 0x1B2DB29F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_金身罗汉_普通"then
		   pgs[6] = 0x732E0F1D
		   pgs[9] = 0x7A0641C6
		   pgs[8] = 0x116F3ABE
		   pgs[5] = 0x99D24D07
		   pgs[4] = 0xD9BD4C2E
		   pgs[1] = 0x28C3863E
		   pgs[2] = 0xE4F58375
		   pgs[7] = 0x2C4131C8
		   pgs[3] = 0x6F486F5F
		   pgs[10] = "ZHS.FT"
		elseif pg == "曼珠沙华_普通"then
		   pgs[6] = 0x45ACDEDF
		   pgs[9] = 0x3BDD0DBF
		   pgs[8] = 0x727FCE05
		   pgs[5] = 0x8BF01F13
		   pgs[4] = 0xE8EF1966
		   pgs[1] = 0x9798EC46
		   pgs[2] = 0xDCCF7313
		   pgs[7] = 0x1036684B
		   pgs[3] = 0xBD77FD79
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_曼珠沙华_普通"then
		   pgs[6] = 0x86A626ED
		   pgs[9] = 0xEBF703E7
		   pgs[8] = 0xAA7B8B16
		   pgs[5] = 0xD215CA34
		   pgs[4] = 0xD8916BD6
		   pgs[1] = 0x8A1C124A
		   pgs[2] = 0x32A77033
		   pgs[7] = 0x99D3C540
		   pgs[3] = 0x9D2A42ED
		   pgs[10] = "ZHS.FT"
		elseif pg == "修罗傀儡妖_普通"then
		   pgs[6] = 0x8BE06586
		   pgs[9] = 0xF712F34F
		   pgs[8] = 0x658404D2
		   pgs[5] = 0x7E7E7120
		   pgs[4] = 0x22CAE42D
		   pgs[1] = 0x55B54077
		   pgs[2] = 0x33C182E9
		   pgs[7] = 0x4C07FFA1
		   pgs[3] = 0xB9A4F860
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_修罗傀儡妖_普通"then
		   pgs[6] = 0x6BAC03BC
		   pgs[9] = 0x9829749E
		   pgs[8] = 0xEB155B54
		   pgs[5] = 0xACAA9120
		   pgs[4] = 0x6C778517
		   pgs[1] = 0x14716C76
		   pgs[2] = 0x190C9114
		   pgs[7] = 0x87BD3F23
		   pgs[3] = 0x81C8BEE1
		   pgs[10] = "ZHS.FT"
	------------------------------------------------------175
		elseif pg == "般若天女_普通"then
		   pgs[6] = 0x9707F281
		   pgs[9] = 0xE86E0AA8
		   pgs[8] = 0xF1B7DFED
		   pgs[5] = 0x93A25A4E
		   pgs[4] = 0x08C0F8A8
		   pgs[1] = 0x5FCBF9E2
		   pgs[2] = 0x6210876B
		   pgs[7] = 0x04149886
		   pgs[3] = 0xF0E1008C
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_般若天女_普通"then
		   pgs[6] = 0x0A1059E2
		   pgs[9] = 0xE1F2B9CC
		   pgs[8] = 0xEDAFCFE8
		   pgs[5] = 0x25603438
		   pgs[4] = 0x844C5D01
		   pgs[1] = 0x9549C17D
		   pgs[2] = 0x56C89490
		   pgs[7] = 0x79095C13
		   pgs[3] = 0xA4574A61
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_般若天女_普通"then
		   pgs[6] = 0xC4B644BF
		   pgs[9] = 0xC0F359DB
		   pgs[8] = 0xD03B6B45
		   pgs[5] = 0x742A1FD9
		   pgs[4] = 0x1FE0AB7C
		   pgs[1] = 0x6FEFC800
		   pgs[2] = 0xE3E4E818
		   pgs[7] = 0x54612375
		   pgs[3] = 0xF98F1407
		   pgs[10] = "ZHS.FT"
		elseif pg == "持国巡守_普通"then
		   pgs[6] = 0x784101C7
		   pgs[9] = 0xC73246A6
		   pgs[8] = 0x86FB90B6
		   pgs[5] = 0x60BF7629
		   pgs[4] = 0xBA687362
		   pgs[1] = 0x8EEFF02C
		   pgs[2] = 0x84CB0E62
		   pgs[7] = 0x917EAD51
		   pgs[3] = 0xB367E3EE
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_持国巡守_普通"then
		   pgs[6] = 0x808D7DB0
		   pgs[9] = 0x693B6570
		   pgs[8] = 0x2285D8DA
		   pgs[5] = 0x810AC543
		   pgs[4] = 0x62BBBE35
		   pgs[1] = 0x7062939A
		   pgs[2] = 0x4BDEC579
		   pgs[7] = 0x1CC2CC7F
		   pgs[3] = 0x5F8742D4
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_持国巡守_普通"then
		   pgs[6] = 0x28F2180E
		   pgs[9] = 0x25B3888E
		   pgs[8] = 0x65CFF2A5
		   pgs[5] = 0xEEFFD6B5
		   pgs[4] = 0xD3805DD8
		   pgs[1] = 0xAF59E387
		   pgs[2] = 0x1D16D5B6
		   pgs[7] = 0xA0B1F1DF
		   pgs[3] = 0xD4BC4D3F
		   pgs[10] = "ZHS.FT"
		elseif pg == "灵灯侍者_普通"then
		   pgs[6] = 0xB3183F50
		   pgs[9] = 0x23FFF355
		   pgs[8] = 0x946FA1B5
		   pgs[5] = 0x0BDF4F11
		   pgs[4] = 0xD97F5424
		   pgs[1] = 0xC33D25CB
		   pgs[2] = 0x2A897C32
		   pgs[7] = 0xACEAD4A9
		   pgs[3] = 0x32E364CF
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_灵灯侍者_普通"then
		   pgs[6] = 0x8F60C146
		   pgs[9] = 0x42CC302E
		   pgs[8] = 0xD06A9B95
		   pgs[5] = 0xE7533DAD
		   pgs[4] = 0xB15D88A9
		   pgs[1] = 0x24AEF9EB
		   pgs[2] = 0xB82D81E6
		   pgs[7] = 0x1F9B3F61
		   pgs[3] = 0xB3C083A8
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_灵灯侍者_普通"then
		   pgs[6] = 0xCE89E8B1
		   pgs[9] = 0xF4774111
		   pgs[8] = 0x5CF2B095
		   pgs[5] = 0x9596AC88
		   pgs[4] = 0x3548EE75
		   pgs[1] = 0x05AD1F37
		   pgs[2] = 0xEA295B97
		   pgs[7] = 0xA8642381
		   pgs[3] = 0x0875104B
		   pgs[10] = "ZHS.FT"
		elseif pg == "毗舍童子_普通"then
		   pgs[6] = 0x60D45B9C
		   pgs[9] = 0xF63A1857
		   pgs[8] = 0x71BFCD9C
		   pgs[5] = 0xE7DD6CBE
		   pgs[4] = 0xFC2044B9
		   pgs[1] = 0x371990E9
		   pgs[2] = 0xF449F7B5
		   pgs[7] = 0x15A6778D
		   pgs[3] = 0x3976A73D
		   pgs[10] = "ZHS.FT"
		elseif pg == "增长巡守_普通"then
		   pgs[6] = 0x5886EC62
		   pgs[9] = 0x2606001E
		   pgs[8] = 0x1E9C2F0A
		   pgs[5] = 0x787517EB
		   pgs[4] = 0xE28991E5
		   pgs[1] = 0x26B97F1B
		   pgs[2] = 0x5788E61F
		   pgs[7] = 0x34FBC7D6
		   pgs[3] = 0xA9905F03
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_增长巡守_普通"then
		   pgs[6] = 0x0B75D87B
		   pgs[9] = 0x8E5D5EB4
		   pgs[8] = 0x692F924A
		   pgs[5] = 0xB95FC74A
		   pgs[4] = 0xD22DE925
		   pgs[1] = 0xD0F52DCC
		   pgs[2] = 0x40F630AD
		   pgs[7] = 0x73415758
		   pgs[3] = 0x3B1EC4A5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_增长巡守_普通"then
		   pgs[6] = 0x6647D1E6
		   pgs[9] = 0xCF8C43EC
		   pgs[8] = 0xB97D4400
		   pgs[5] = 0x7CF85713
		   pgs[4] = 0x5A1E5397
		   pgs[1] = 0x49C2ED93
		   pgs[2] = 0x06A7E497
		   pgs[7] = 0x95DF7280
		   pgs[3] = 0xB8510C56
		   pgs[10] = "ZHS.FT"
		elseif pg == "真陀护法_普通"then
		   pgs[6] = 0x9F386CE9
		   pgs[9] = 0x83B1AB72
		   pgs[8] = 0xF227C233
		   pgs[5] = 0x5B45325A
		   pgs[4] = 0x281836EE
		   pgs[1] = 0xD433484F
		   pgs[2] = 0xC48F78A0
		   pgs[7] = 0x5AB8D8B7
		   pgs[3] = 0x439AD1EF
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_真陀护法_普通"then
		   pgs[6] = 0x3AC2C970
		   pgs[9] = 0x0B9071A9
		   pgs[8] = 0x773C6119
		   pgs[5] = 0xFCFA492C
		   pgs[4] = 0x8E37564F
		   pgs[1] = 0xFAE8742D
		   pgs[2] = 0x2E9DD597
		   pgs[7] = 0x1E0B54B2
		   pgs[3] = 0xA56CC3EF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_真陀护法_普通"then
		   pgs[6] = 0x7F27F1A9
		   pgs[9] = 0xD62BA99E
		   pgs[8] = 0xE2DD0587
		   pgs[5] = 0xAE69DB79
		   pgs[4] = 0x546B6920
		   pgs[1] = 0x38A10DBC
		   pgs[2] = 0xD232A413
		   pgs[7] = 0xEDCDDD44
		   pgs[3] = 0x85358EF6
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------45进阶
		elseif pg == "进阶白熊_普通"then
		   pgs[6] = 0x456ED9AE
		   pgs[9] = 0x67374C54
		   pgs[8] = 0xC2E91BB6
		   pgs[5] = 0x87999917
		   pgs[4] = 0x87074A6D
		   pgs[1] = 0x69F0C9F3
		   pgs[2] = 0x36221458
		   pgs[7] = 0x8F754AB3
		   pgs[3] = 0x9D440DE2
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶白熊_普通"then
		   pgs[6] = 0xBBA680CA
		   pgs[9] = 0xD5B07CA4
		   pgs[8] = 0xA491FFD4
		   pgs[5] = 0x54A9B2A1
		   pgs[4] = 0xA7103857
		   pgs[1] = 0xE712AD2E
		   pgs[2] = 0x32A8AEB7
		   pgs[7] = 0x41B3F4C0
		   pgs[3] = 0xBC8D6645
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶古代瑞兽_普通"then
		   pgs[6] = 0xB31DD3FE
		   pgs[9] = 0xCC7353FA
		   pgs[8] = 0x2C2FA1CA
		   pgs[5] = 0xD6A29350
		   pgs[4] = 0xB4613104
		   pgs[1] = 0x7291F291
		   pgs[2] = 0x1FE9B187
		   pgs[7] = 0xC658307A
		   pgs[3] = 0xE04C80B3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶古代瑞兽_普通"then
		   pgs[6] = 0x7F86B00D
		   pgs[9] = 0xAC661546
		   pgs[8] = 0x3DD12FDA
		   pgs[5] = 0xC8C8D162
		   pgs[4] = 0xCC6FFADD
		   pgs[1] = 0xBB273790
		   pgs[2] = 0x81759ED1
		   pgs[7] = 0xB2F424B6
		   pgs[3] = 0xB19A5E7A
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶黑山老妖_普通"then
		   pgs[6] = 0xFA5075B4
		   pgs[9] = 0x6F9C7D69
		   pgs[8] = 0x95FBDF17
		   pgs[5] = 0xDFD6A089
		   pgs[4] = 0x3655BA0F
		   pgs[1] = 0x5DE2E1D7
		   pgs[2] = 0x18B4A333
		   pgs[7] = 0x20E38EFD
		   pgs[3] = 0xE5768F24
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶黑山老妖_普通"then
		   pgs[6] = 0x568B725F
		   pgs[9] = 0x1C2E6B9E
		   pgs[8] = 0xC7F4096B
		   pgs[5] = 0xA0CFA057
		   pgs[4] = 0xD1F403A2
		   pgs[1] = 0x14912994
		   pgs[2] = 0x02D4F692
		   pgs[7] = 0x62AED9B4
		   pgs[3] = 0xE5768F24
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶蝴蝶仙子_普通"then
		   pgs[6] = 0x763FC6A0
		   pgs[9] = 0x067A5102
		   pgs[8] = 0xC3FF9F5A
		   pgs[5] = 0x14FD2FF7
		   pgs[4] = 0xB092EA64
		   pgs[1] = 0x1E9603F8
		   pgs[2] = 0x1EA96EB3
		   pgs[7] = 0x61AC2D26
		   pgs[3] = 0x924D044A
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶蝴蝶仙子_普通"then
		   pgs[6] = 0xE8C34CF6
		   pgs[9] = 0xA83F8BE3
		   pgs[8] = 0x4E74E508
		   pgs[5] = 0xE2D2E5FB
		   pgs[4] = 0x40042FC0
		   pgs[1] = 0x756C092F
		   pgs[2] = 0x2960E331
		   pgs[7] = 0xB800C204
		   pgs[3] = 0xB7D63C57
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶雷鸟人_普通"then
		   pgs[6] = 0xD309B2D8
		   pgs[9] = 0x5B455BAB
		   pgs[8] = 0x9134A8D8
		   pgs[5] = 0x1281D163
		   pgs[4] = 0xA8E9E56A
		   pgs[1] = 0xA4462E00
		   pgs[2] = 0x6C7DE28C
		   pgs[7] = 0x9F6B6DCE
		   pgs[3] = 0x3AA0420A
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶雷鸟人_普通"then
		   pgs[6] = 0x62232EF8
		   pgs[9] = 0x771AE37F
		   pgs[8] = 0x93264A89
		   pgs[5] = 0x63294502
		   pgs[4] = 0xC3CCD0A6
		   pgs[1] = 0x8F9C7339
		   pgs[2] = 0xE02ADFEE
		   pgs[7] = 0xCF3811EE
		   pgs[3] = 0x51836F3F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶雷鸟人_普通"then
		   pgs[6] = 0x7E0ABF25
		   pgs[9] = 0x1D9FFB3A
		   pgs[8] = 0xFD61A5D5
		   pgs[5] = 0xDB4A7B9C
		   pgs[4] = 0x78F5F786
		   pgs[1] = 0x28F477A3
		   pgs[2] = 0x3282FAD7
		   pgs[7] = 0x7A02444F
		   pgs[3] = 0x82C48D61
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------55进阶
		elseif pg == "进阶地狱战神_普通"then
		   pgs[6] = 0x6B19D682
		   pgs[9] = 0x28533A4E
		   pgs[8] = 0x0A0351C4
		   pgs[5] = 0x112726F1
		   pgs[4] = 0xC4F27046
		   pgs[1] = 0x72A69AC3
		   pgs[2] = 0xAF2C4C75
		   pgs[7] = 0x958AC02C
		   pgs[3] = 0x602C6BA1
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶地狱战神_普通"then
		   pgs[6] = 0xC59FC250
		   pgs[9] = 0xCDD6507A
		   pgs[8] = 0xE3451268
		   pgs[5] = 0x22671C0F
		   pgs[4] = 0xB760E037
		   pgs[1] = 0x637D9C6F
		   pgs[2] = 0x941FC7A4
		   pgs[7] = 0xFBB4FD56
		   pgs[3] = 0x602C6BA1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶地狱战神_普通"then
		   pgs[6] = 0x736760B3
		   pgs[9] = 0xA4188017
		   pgs[8] = 0xE70D6D3D
		   pgs[5] = 0xDC1B8BCD
		   pgs[4] = 0xA7CACBB3
		   pgs[1] = 0xD3E28906
		   pgs[2] = 0x17665192
		   pgs[7] = 0x32E859EF
		   pgs[3] = 0xC073FD07
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶风伯_普通"then
		   pgs[6] = 0xE6944AFD
		   pgs[9] = 0xE91519F9
		   pgs[8] = 0x4975EB93
		   pgs[5] = 0x3CD25C40
		   pgs[4] = 0xDD0ABC12
		   pgs[1] = 0x1C3519F4
		   pgs[2] = 0xAB730806
		   pgs[7] = 0x421CDC1C
		   pgs[3] = 0x0B4D5B60
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶风伯_普通"then
		   pgs[6] = 0x100DC708
		   pgs[9] = 0x314A86A4
		   pgs[8] = 0xA773386B
		   pgs[5] = 0x354E676B
		   pgs[4] = 0x6B01FE15
		   pgs[1] = 0xC9B579C3
		   pgs[2] = 0x29B8E4D0
		   pgs[7] = 0x579292FB
		   pgs[3] = 0x08593D0B
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶风伯_普通"then
		   pgs[6] = 0xE277F8F4
		   pgs[9] = 0x1691550D
		   pgs[8] = 0xA891F58F
		   pgs[5] = 0x40B9F376
		   pgs[4] = 0xB4986B9B
		   pgs[1] = 0x61E56853
		   pgs[2] = 0x83F444A2
		   pgs[7] = 0xF32E8EAD
		   pgs[3] = 0x6508E3BC
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶天兵_普通"then
		   pgs[6] = 0x263409F7
		   pgs[9] = 0xCF984CBD
		   pgs[8] = 0xCF9C6012
		   pgs[5] = 0xF348F490
		   pgs[4] = 0x1F48EF9F
		   pgs[1] = 0xE8E36824
		   pgs[2] = 0x35D94E14
		   pgs[7] = 0x941C1119
		   pgs[3] = 0xAF1B4B61
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶天兵_普通"then
		   pgs[6] = 0xEBD9E26C
		   pgs[9] = 0x8A00FC84
		   pgs[8] = 0xFB0B8556
		   pgs[5] = 0xE14247A2
		   pgs[4] = 0x7950EA94
		   pgs[1] = 0x16E3EA8E
		   pgs[2] = 0x3A438D62
		   pgs[7] = 0x2480FBE9
		   pgs[3] = 0x1E69C9C5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶天兵_普通"then
		   pgs[6] = 0xDD00D038
		   pgs[9] = 0x96FE55C2
		   pgs[8] = 0xC87D7F43
		   pgs[5] = 0xC87D7F43
		   pgs[4] = 0xA6ED255E
		   pgs[1] = 0xAD7A902A
		   pgs[2] = 0x658DFAD7
		   pgs[7] = 0xA51E6D05
		   pgs[3] = 0x8728E103
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶天将_普通"then
		   pgs[6] = 0xB90ED478
		   pgs[9] = 0x666FA5FF
		   pgs[8] = 0x2F969199
		   pgs[5] = 0x369154B8
		   pgs[4] = 0xF8941B27
		   pgs[1] = 0xD191238C
		   pgs[2] = 0xAF5E9D11
		   pgs[7] = 0x38A3BA7A
		   pgs[3] = 0x7AFD84CB
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶天将_普通"then
		   pgs[6] = 0xE21D438D
		   pgs[9] = 0xEE404F3B
		   pgs[8] = 0xE4EBC336
		   pgs[5] = 0x7345E462
		   pgs[4] = 0x9E478081
		   pgs[1] = 0x9B5875EE
		   pgs[2] = 0xB46C8172
		   pgs[7] = 0xB45F1A21
		   pgs[3] = 0xA04A8792
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶天将_普通"then
		   pgs[6] = 0x51C04FE9
		   pgs[9] = 0xBD1824C4
		   pgs[8] = 0xAD9C865A
		   pgs[5] = 0x81E5AAD5
		   pgs[4] = 0xA5CEB0A1
		   pgs[1] = 0x355FC178
		   pgs[2] = 0x8EC9BD35
		   pgs[7] = 0xCA0F2175
		   pgs[3] = 0x9FDC474F
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------65进阶
		elseif pg == "进阶蚌精_普通"then
		   pgs[6] = 0xA96DF03A
		   pgs[9] = 0x7A78E890
		   pgs[8] = 0x494B0DD5
		   pgs[5] = 0x2E750AFA
		   pgs[4] = 0x0A8B52DB
		   pgs[1] = 0x8CFE363B
		   pgs[2] = 0x34E9C79F
		   pgs[7] = 0xF0677AD8
		   pgs[3] = 0x42900F8B
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_阶蚌精_普通"then
		   pgs[6] = 0x2B30520D
		   pgs[9] = 0xF3E3E636
		   pgs[8] = 0x1B8544A0
		   pgs[5] = 0x93D65B4B
		   pgs[4] = 0xC20C9884
		   pgs[1] = 0x3F121C87
		   pgs[2] = 0xD2238B8A
		   pgs[7] = 0x9DFE0B66
		   pgs[3] = 0x2A69F4E4
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶碧水夜叉_普通"then
		   pgs[6] = 0x7996D4FD
		   pgs[9] = 0xC7080258
		   pgs[8] = 0xC2B09D04
		   pgs[5] = 0x3995294F
		   pgs[4] = 0x240C4935
		   pgs[1] = 0x41B0D9DE
		   pgs[2] = 0xAAB58620
		   pgs[7] = 0x185A3FA7
		   pgs[3] = 0x1B8CB1C7
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶碧水夜叉_普通"then
		   pgs[6] = 0x024B4816
		   pgs[9] = 0xBFE47349
		   pgs[8] = 0x5D5521EF
		   pgs[5] = 0x3995294F
		   pgs[4] = 0xAF99B5D7
		   pgs[1] = 0xF907D38F
		   pgs[2] = 0x007F3C3C
		   pgs[7] = 0xA2CC594D
		   pgs[3] = 0x23CDF937
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶碧水夜叉_普通"then
		   pgs[6] = 0xC6633DC8
		   pgs[9] = 0xB142DBE7
		   pgs[8] = 0x0728D23A
		   pgs[5] = 0x00730EDA
		   pgs[4] = 0x48F2CE34
		   pgs[1] = 0x8C5CA69A
		   pgs[2] = 0xAB3E1CA6
		   pgs[7] = 0xE6881018
		   pgs[3] = 0x69761652
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶凤凰_普通"then
		   pgs[6] = 0x0CC023FE
		   pgs[9] = 0x0CF889CA
		   pgs[8] = 0xE441F928
		   pgs[5] = 0x8F6C98E0
		   pgs[4] = 0xBED32A52
		   pgs[1] = 0x531998EA
		   pgs[2] = 0xB9144194
		   pgs[7] = 0x441E6903
		   pgs[3] = 0x2FC72F35
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶凤凰_普通"then
		   pgs[6] = 0x6A19B804
		   pgs[9] = 0xDF71ECA7
		   pgs[8] = 0xF55DAC54
		   pgs[5] = 0x2A0AEC73
		   pgs[4] = 0xB9DFCCCA
		   pgs[1] = 0x2B004490
		   pgs[2] = 0x64A74BBD
		   pgs[7] = 0xB282F7D0
		   pgs[3] = 0x6FEE0CDE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶凤凰_普通"then
		   pgs[6] = 0x7D91B644
		   pgs[9] = 0xC90E04F1
		   pgs[8] = 0xAF5D7B92
		   pgs[5] = 0x1C4BF4DB
		   pgs[4] = 0x08FD84B8
		   pgs[1] = 0xB2EAFE77
		   pgs[2] = 0xFC9CEFAA
		   pgs[7] = 0xC746F784
		   pgs[3] = 0x005AF99F
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶雨师_普通"then
		   pgs[6] = 0x18EF6634
		   pgs[9] = 0xC1476207
		   pgs[8] = 0x670544A4
		   pgs[5] = 0x989F5AB2
		   pgs[4] = 0xC3C6CB3F
		   pgs[1] = 0xADCAF1D3
		   pgs[2] = 0x7E79FDC4
		   pgs[7] = 0xFC49420C
		   pgs[3] = 0xBF6028A3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶雨师_普通"then
		   pgs[6] = 0x2DB97952
		   pgs[9] = 0x565988A0
		   pgs[8] = 0xD4758CD0
		   pgs[5] = 0x4C15D02A
		   pgs[4] = 0x997CB8C1
		   pgs[1] = 0x879B0739
		   pgs[2] = 0x0A771A69
		   pgs[7] = 0x530CFEA0
		   pgs[3] = 0x7FC8DBDB
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶蛟龙_普通"then
		   pgs[6] = 0xA3DA0874
		   pgs[9] = 0x48971844
		   pgs[8] = 0x768EC51E
		   pgs[5] = 0xA800B980
		   pgs[4] = 0x644C7AD6
		   pgs[1] = 0xA815AA97
		   pgs[2] = 0xF823CB5B
		   pgs[7] = 0x77FAB65D
		   pgs[3] = 0x79423CC9
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶蛟龙_普通"then
		   pgs[6] = 0x61192DC4
		   pgs[9] = 0x2C3E37A2
		   pgs[8] = 0x8057EF18
		   pgs[5] = 0x59F09DEB
		   pgs[4] = 0xD3F721DD
		   pgs[1] = 0x3541248B
		   pgs[2] = 0x769F42A3
		   pgs[7] = 0xD3CF4F30
		   pgs[3] = 0xA447C9C9
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶蛟龙_普通"then
		   pgs[6] = 0x50C1D1B8
		   pgs[9] = 0x073143DE
		   pgs[8] = 0x32361ABC
		   pgs[5] = 0x8F1A7E57
		   pgs[4] = 0xBAE033EB
		   pgs[1] = 0x65554FC5
		   pgs[2] = 0xFAC1004A
		   pgs[7] = 0xB923763C
		   pgs[3] = 0xFD6B9032
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶鲛人_普通"then
		   pgs[6] = 0x964D1EF1
		   pgs[9] = 0x7A35C54B
		   pgs[8] = 0x01D4B3DC
		   pgs[5] = 0x51768776
		   pgs[4] = 0x3692E598
		   pgs[1] = 0x4DD6DDDA
		   pgs[2] = 0x640F12DD
		   pgs[7] = 0x55462CFE
		   pgs[3] = 0x8FE89A5D
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶鲛人_普通"then
		   pgs[6] = 0xC30E9C64
		   pgs[9] = 0x0E3002E6
		   pgs[8] = 0xD0DF8ED8
		   pgs[5] = 0x210D0B0C
		   pgs[4] = 0x51E67BF4
		   pgs[1] = 0x233E123F
		   pgs[2] = 0xBBF0D5E5
		   pgs[7] = 0xD171ED1C
		   pgs[3] = 0xA7E04BA3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶鲛人_普通"then
		   pgs[6] = 0x1B5790E0
		   pgs[9] = 0xA97D144C
		   pgs[8] = 0x8051E578
		   pgs[5] = 0x3F3E73BF
		   pgs[4] = 0xE33C142F
		   pgs[1] = 0xE7DAEA0A
		   pgs[2] = 0xB9295A75
		   pgs[7] = 0x1E9F260E
		   pgs[3] = 0x2EC50D51
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------75进阶
		elseif pg == "进阶锦毛貂精_普通"then
		   pgs[6] = 0xD34BE63C
		   pgs[9] = 0x4DC8DCA5
		   pgs[8] = 0x8CFB251C
		   pgs[5] = 0xA354A140
		   pgs[4] = 0x75E66AF1
		   pgs[1] = 0xEC43AC2B
		   pgs[2] = 0x5AC32BC4
		   pgs[7] = 0x8ABFBDBB
		   pgs[3] = 0xADEAB1FE
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶锦毛貂精_普通"then
		   pgs[6] = 0xF5452BCE
		   pgs[9] = 0xFB322308
		   pgs[8] = 0x29D2A239
		   pgs[5] = 0x59F66DD6
		   pgs[4] = 0x9893D7B8
		   pgs[1] = 0x566EB6DE
		   pgs[2] = 0x14F072A3
		   pgs[7] = 0xBDDD2CC8
		   pgs[3] = 0x73AB8FF6
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶锦毛貂精_普通"then
		   pgs[6] = 0x00D8BA03
		   pgs[9] = 0x49FE65F2
		   pgs[8] = 0x7E4C7C91
		   pgs[5] = 0x2C2A62E8
		   pgs[4] = 0xD780D9F0
		   pgs[1] = 0x4AB6FE65
		   pgs[2] = 0xAA3530C1
		   pgs[7] = 0xAF8A8AF0
		   pgs[3] = 0x7FA538F2
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶千年蛇魅_普通"then
		   pgs[6] = 0xA14F4A62
		   pgs[9] = 0x6E54D4E3
		   pgs[8] = 0xA4AC72C1
		   pgs[5] = 0x48D305B6
		   pgs[4] = 0x8C0E8FD0
		   pgs[1] = 0xC0B4DE3E
		   pgs[2] = 0xE5E4A1F8
		   pgs[7] = 0xB4F3EE56
		   pgs[3] = 0x7AC3FD14
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶千年蛇魅_普通"then
		   pgs[6] = 0x2FE148ED
		   pgs[9] = 0xEB0B92FA
		   pgs[8] = 0xD112B947
		   pgs[5] = 0xC51BEDE0
		   pgs[4] = 0x4EE14352
		   pgs[1] = 0x28D4057B
		   pgs[2] = 0x320D7BB6
		   pgs[7] = 0xF2558A57
		   pgs[3] = 0x9221732F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶千年蛇魅_普通"then
		   pgs[6] = 0x6269EE72
		   pgs[9] = 0x5AE4D413
		   pgs[8] = 0xE75B86A1
		   pgs[5] = 0x2E505C04
		   pgs[4] = 0xDC63F7E0
		   pgs[1] = 0x39629D8B
		   pgs[2] = 0xE8A88379
		   pgs[7] = 0x27882B73
		   pgs[3] = 0xB20D06E2
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶如意仙子_普通"then
		   pgs[6] = 0xF80DD9AD
		   pgs[9] = 0x4EA4E8E0
		   pgs[8] = 0x4198CF0C
		   pgs[5] = 0x647F99DD
		   pgs[4] = 0xF8B7009E
		   pgs[1] = 0x01197058
		   pgs[2] = 0x81042AE4
		   pgs[7] = 0x504C8CCA
		   pgs[3] = 0x132F1FB6
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶如意仙子_普通"then
		   pgs[6] = 0xB4323647
		   pgs[9] = 0x28D1CE66
		   pgs[8] = 0x51A5BF1A
		   pgs[5] = 0x06123BD5
		   pgs[4] = 0x9237965D
		   pgs[1] = 0x145BA64B
		   pgs[2] = 0xF4B861D9
		   pgs[7] = 0x17537C8C
		   pgs[3] = 0x32CCB8F3
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶犀牛将军人形_普通"then
		   pgs[6] = 0xCDF6AC47
		   pgs[9] = 0xCE559EB3
		   pgs[8] = 0xCFFE9FB9
		   pgs[5] = 0xFE38B1E6
		   pgs[4] = 0x968FE790
		   pgs[1] = 0xFEE95AAF
		   pgs[2] = 0x9D7F2AD0
		   pgs[7] = 0xC2F24E06
		   pgs[3] = 0xB56C293E
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶犀牛将军人形_普通"then
		   pgs[6] = 0x55BA7290
		   pgs[9] = 0x6119DD21
		   pgs[8] = 0x10209E45
		   pgs[5] = 0x79082997
		   pgs[4] = 0x16FEE4F8
		   pgs[1] = 0xBA23FAF3
		   pgs[2] = 0xB0C627DF
		   pgs[7] = 0xF03CBB7C
		   pgs[3] = 0x78DAF314
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶犀牛将军兽形_普通"then
		   pgs[6] = 0x6C15BE14
		   pgs[9] = 0xD5D87FC7
		   pgs[8] = 0xFC69F32B
		   pgs[5] = 0x9DCB7A5A
		   pgs[4] = 0xE43DCAE8
		   pgs[1] = 0xF3B7CF02
		   pgs[2] = 0xD9A93462
		   pgs[7] = 0x14B17887
		   pgs[3] = 0x162C35E1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶犀牛将军兽形_普通"then
		   pgs[6] = 0xE538108A
		   pgs[9] = 0x74110487
		   pgs[8] = 0x335FFC4E
		   pgs[5] = 0xBA9BB39B
		   pgs[4] = 0x3B79CA55
		   pgs[1] = 0x08DB608F
		   pgs[2] = 0x66DAD328
		   pgs[7] = 0x5F269463
		   pgs[3] = 0x9D54C470
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶星灵仙子_普通"then
		   pgs[6] = 0x75E24AAD
		   pgs[9] = 0x2D167267
		   pgs[8] = 0x243B09A1
		   pgs[5] = 0xB4302144
		   pgs[4] = 0x68B373FD
		   pgs[1] = 0xB2A69A57
		   pgs[2] = 0xE160F4FD
		   pgs[7] = 0xCD464FE7
		   pgs[3] = 0xC50D683C
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶星灵仙子_普通"then
		   pgs[6] = 0x66501D93
		   pgs[9] = 0x8BAC6040
		   pgs[8] = 0x3916EE0A
		   pgs[5] = 0xCE9ED092
		   pgs[4] = 0x9E8F265D
		   pgs[1] = 0xA7D22776
		   pgs[2] = 0x37E3E3E1
		   pgs[7] = 0x2F25B66C
		   pgs[3] = 0xE10EA214
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶星灵仙子_普通"then
		   pgs[6] = 0xC847F079
		   pgs[9] = 0xADA8FF44
		   pgs[8] = 0x9DA5A2C7
		   pgs[5] = 0x54AA0696
		   pgs[4] = 0xC5187EB9
		   pgs[1] = 0x8814CBF4
		   pgs[2] = 0x6575B5A0
		   pgs[7] = 0xE70CC6CB
		   pgs[3] = 0x1077BD08
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶巡游天神_普通"then
		   pgs[6] = 0x0F27607E
		   pgs[9] = 0x96C9C11E
		   pgs[8] = 0x447F4F60
		   pgs[5] = 0x913D7AE5
		   pgs[4] = 0xB40DB275
		   pgs[1] = 0xD019B465
		   pgs[2] = 0x61626CF7
		   pgs[7] = 0xD8817986
		   pgs[3] = 0x38DDF499
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶巡游天神_普通"then
		   pgs[6] = 0xFAEE3F9D
		   pgs[9] = 0x7D645673
		   pgs[8] = 0xFD8AE7A0
		   pgs[5] = 0xC8C13E26
		   pgs[4] = 0x2499EE14
		   pgs[1] = 0x2DBDA2A9
		   pgs[2] = 0x756D402A
		   pgs[7] = 0x17F2991C
		   pgs[3] = 0xAFE6EDA0
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶巡游天神_普通"then
		   pgs[6] = 0x255F30FE
		   pgs[9] = 0xDF1D6AB3
		   pgs[8] = 0xDF743588
		   pgs[5] = 0x7465C9DE
		   pgs[4] = 0xDEB3D40A
		   pgs[1] = 0x2BF7C971
		   pgs[2] = 0x7E263138
		   pgs[7] = 0x861E1C4A
		   pgs[3] = 0x89FC46EE
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶芙蓉仙子_普通"then
		   pgs[6] = 0x5A4AB7D0
		   pgs[9] = 0x4F65A668
		   pgs[8] = 0x96BC94AC
		   pgs[5] = 0x00EE18C8
		   pgs[4] = 0x5D9AB3E1
		   pgs[1] = 0x080F22AE
		   pgs[2] = 0x71C7FAC0
		   pgs[7] = 0xA0B65C4E
		   pgs[3] = 0x91C59A1F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶芙蓉仙子_普通"then
		   pgs[6] = 0xFC93FE17
		   pgs[9] = 0xF46A1607
		   pgs[8] = 0xB1ABD7F3
		   pgs[5] = 0x84C8C223
		   pgs[4] = 0xFC2C0A43
		   pgs[1] = 0x4B0B38A2
		   pgs[2] = 0xE7527DDE
		   pgs[7] = 0xCB6D7FCA
		   pgs[3] = 0xB66F315B
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------85进阶
		elseif pg == "进阶百足将军_普通"then
		   pgs[6] = 0xAB6204D0
		   pgs[9] = 0x78DC8F28
		   pgs[8] = 0x6F6B53A9
		   pgs[5] = 0x44A1F1A2
		   pgs[4] = 0x7E435160
		   pgs[1] = 0xE5365183
		   pgs[2] = 0xA2159882
		   pgs[7] = 0x23087F63
		   pgs[3] = 0x40C65332
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶百足将军_普通"then
		   pgs[6] = 0x8043BC29
		   pgs[9] = 0x1EF5586E
		   pgs[8] = 0x29696F70
		   pgs[5] = 0xCFA4C4B0
		   pgs[4] = 0x0F211C03
		   pgs[1] = 0xC62B1385
		   pgs[2] = 0x494C4595
		   pgs[7] = 0xA1A959E0
		   pgs[3] = 0x4954B2BA
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶百足将军_普通"then
		   pgs[6] = 0x5E86A3C9
		   pgs[9] = 0xCD43714F
		   pgs[8] = 0x18A40FC8
		   pgs[5] = 0xDB7CFDE0
		   pgs[4] = 0xBC6E1317
		   pgs[1] = 0x4C30BDF1
		   pgs[2] = 0x6053E629
		   pgs[7] = 0x01CD4680
		   pgs[3] = 0x90A7C6B8
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶镜妖_普通"then--------------
		   -- pgs[6] = 0x
		   -- pgs[9] = 0xE98705C8
		   -- pgs[8] = 0xDF0787C3
		   -- pgs[5] = 0x
		   -- pgs[4] = 0x7246C3FB
		   -- pgs[1] = 0x
		   -- pgs[2] = 0x
		   -- pgs[7] = 0x
		   -- pgs[3] = 0xF7A9EE96
		   -- pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶镜妖_普通"then---------
		   -- pgs[6] = 0x
		   -- pgs[9] = 0x4D652CDE
		   -- pgs[8] = 0x62B9CC72
		   -- pgs[5] = 0x
		   -- pgs[4] = 0xF9E36067
		   -- pgs[1] = 0x
		   -- pgs[2] = 0x
		   -- pgs[7] = 0x
		   -- pgs[3] = 0xC84F54D4
		   -- pgs[10] = "ZHS.FT"
		elseif pg == "进阶泪妖_普通"then
		   pgs[6] = 0xED30E9C7
		   pgs[9] = 0x01C7595D
		   pgs[8] = 0x6272607D
		   pgs[5] = 0x15F0764C
		   pgs[4] = 0xEF70986D
		   pgs[1] = 0x52E785F1
		   pgs[2] = 0x88212DAB
		   pgs[7] = 0x840F0720
		   pgs[3] = 0x70C439E3
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶鼠先锋_普通"then
		   pgs[6] = 0x89A1831C
		   pgs[9] = 0x88D3286C
		   pgs[8] = 0xE0DFA185
		   pgs[5] = 0xB9B7EBDB
		   pgs[4] = 0x0B818F11
		   pgs[1] = 0x078A8E44
		   pgs[2] = 0x98A18E75
		   pgs[7] = 0x3CDD35E9
		   pgs[3] = 0x95ECB854
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶鼠先锋_普通"then---------
		   pgs[6] = 0x81432EEA
		   pgs[9] = 0xF1819C25
		   pgs[8] = 0xE1FED58E
		   pgs[5] = 0xC1186D08
		   pgs[4] = 0x2E396D1F
		   pgs[1] = 0x433B070C
		   pgs[2] = 0x83B37126
		   pgs[7] = 0x1842937F
		   pgs[3] = 0x316180AF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶鼠先锋_普通"then
		   pgs[6] = 0x85505628
		   pgs[9] = 0xA3A77DEA
		   pgs[8] = 0xEF854675
		   pgs[5] = 0xB89A7C9B
		   pgs[4] = 0x8E05C9C3
		   pgs[1] = 0x9D2A7E57
		   pgs[2] = 0xEAF59E67
		   pgs[7] = 0xD18BA7E2
		   pgs[3] = 0xFE54D0DC
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶野猪精_普通"then
		   pgs[6] = 0x27AE027B
		   pgs[9] = 0x69308A69
		   pgs[8] = 0x977F1043
		   pgs[5] = 0x6FF0EF15
		   pgs[4] = 0x2CC0C44E
		   pgs[1] = 0x1FAF1415
		   pgs[2] = 0xFAE66519
		   pgs[7] = 0x14835218
		   pgs[3] = 0x17A99AF2
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶野猪精_普通"then
		   pgs[6] = 0x6732F04F
		   pgs[9] = 0x272C1EC2
		   pgs[8] = 0x10EB7807
		   pgs[5] = 0xF7DF280A
		   pgs[4] = 0x0FC0C135
		   pgs[1] = 0xA3428DB1
		   pgs[2] = 0xF36AED64
		   pgs[7] = 0xA775FA1D
		   pgs[3] = 0x2BF711C4
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶野猪精_普通"then
		   pgs[6] = 0x80A1DB6F
		   pgs[9] = 0x02074FCD
		   pgs[8] = 0x7643F690
		   pgs[5] = 0x42EF0217
		   pgs[4] = 0xEFB41A2C
		   pgs[1] = 0xA0F63C4F
		   pgs[2] = 0x37250FD4
		   pgs[7] = 0xF473E6A2
		   pgs[3] = 0x4B153D00
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------95进阶
		elseif pg == "进阶灵符女娲_普通"then
		   pgs[6] = 0x1F56240E
		   pgs[9] = 0x07454B56
		   pgs[8] = 0xB0A1E3E5
		   pgs[5] = 0xB7DC7BDE
		   pgs[4] = 0x6E281AD5
		   pgs[1] = 0xD6E49319
		   pgs[2] = 0x7BAD3CB7
		   pgs[7] = 0x545C89D4
		   pgs[3] = 0x35642349
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶灵符女娲_普通"then
		   pgs[6] = 0xF1DB39D7
		   pgs[9] = 0x423B2698
		   pgs[8] = 0x9E6B9F72
		   pgs[5] = 0x12A4D257
		   pgs[4] = 0x1DA88A39
		   pgs[1] = 0x602E6A71
		   pgs[2] = 0x2CE5F575
		   pgs[7] = 0x2DD22E8B
		   pgs[3] = 0x03995EAB
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶律法女娲_普通"then
		   pgs[6] = 0xCFFC5B11
		   pgs[9] = 0x1B8CD1D4
		   pgs[8] = 0x1CB46D32
		   pgs[5] = 0xE615A62B
		   pgs[4] = 0x8FC7C545
		   pgs[1] = 0xC0FBA16A
		   pgs[2] = 0x63A7D355
		   pgs[7] = 0x19BBEFC4
		   pgs[3] = 0x790FEFD1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶律法女娲_普通"then
		   pgs[6] = 0x357080CE
		   pgs[9] = 0x93B5B64F
		   pgs[8] = 0x04228ACE
		   pgs[5] = 0x5955BDDA
		   pgs[4] = 0xB9030661
		   pgs[1] = 0xBEA4CEE1
		   pgs[2] = 0x94E88DA5
		   pgs[7] = 0xF34B0717
		   pgs[3] = 0xA03BFE61
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶吸血鬼_普通"then
		   pgs[6] = 0xCE0F146C
		   pgs[9] = 0x73C8493D
		   pgs[8] = 0xCE5EB1D9
		   pgs[5] = 0xAD7C34B3
		   pgs[4] = 0xFA455F90
		   pgs[1] = 0x267C5264
		   pgs[2] = 0x6A6AA6B2
		   pgs[7] = 0xAB400A93
		   pgs[3] = 0xAA1D0068
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶吸血鬼_普通"then
		   pgs[6] = 0xB7F88174
		   pgs[9] = 0xD7BDA0A1
		   pgs[8] = 0x5E368AFF
		   pgs[5] = 0xB3FE5C26
		   pgs[4] = 0x255C1645
		   pgs[1] = 0x1E89D22D
		   pgs[2] = 0xA030CBD3
		   pgs[7] = 0xBC2F170A
		   pgs[3] = 0xE6DB5633
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶吸血鬼_普通"then
		   pgs[6] = 0x9AF63F59
		   pgs[9] = 0x0641BB9C
		   pgs[8] = 0x532B44D3
		   pgs[5] = 0x6CEA2B3E
		   pgs[4] = 0x5E04729B
		   pgs[1] = 0xD7AE33AA
		   pgs[2] = 0x9011564E
		   pgs[7] = 0x5305EC86
		   pgs[3] = 0x9F746920
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶阴阳伞_普通"then
		   pgs[6] = 0x5AF2702C
		   pgs[9] = 0x57DF88DF
		   pgs[8] = 0xA2E4424F
		   pgs[5] = 0x45F9345B
		   pgs[4] = 0x38396F6C
		   pgs[1] = 0x84312A1F
		   pgs[2] = 0x90C5A543
		   pgs[7] = 0xDED79EF9
		   pgs[3] = 0xE27750EF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶阴阳伞_普通"then
		   pgs[6] = 0xE63B72EF
		   pgs[9] = 0xC71F9600
		   pgs[8] = 0x44725BE1
		   pgs[5] = 0x6298BEB3
		   pgs[4] = 0x8C95DEFC
		   pgs[1] = 0x869D47B6
		   pgs[2] = 0x869D47B6
		   pgs[7] = 0x0711C9BC
		   pgs[3] = 0xFBE7F244
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶幽灵_普通"then
		   pgs[6] = 0x7F878E9F
		   pgs[9] = 0x7406D186
		   pgs[8] = 0x3D0C212C
		   pgs[5] = 0x3C7CABBD
		   pgs[4] = 0x46F3A9D8
		   pgs[1] = 0x4C3A025E
		   pgs[2] = 0xEC458690
		   pgs[7] = 0x2039517D
		   pgs[3] = 0x5DD590DF
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------105进阶
		elseif pg == "进阶鬼将_普通"then
		   pgs[6] = 0x6ED55801
		   pgs[9] = 0x8609FB4C
		   pgs[8] = 0xBD7075E0
		   pgs[5] = 0xE82EED34
		   pgs[4] = 0xE7E5E555
		   pgs[1] = 0xFAB026B1
		   pgs[2] = 0xE16C41AF
		   pgs[7] = 0xC19D1009
		   pgs[3] = 0x890EFD9F
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶鬼将_普通"then
		   pgs[6] = 0x41225AA1
		   pgs[9] = 0x3E1168D4
		   pgs[8] = 0x0A7AC5E6
		   pgs[5] = 0x3567D770
		   pgs[4] = 0x09194066
		   pgs[1] = 0xA88CA530
		   pgs[2] = 0xCAF17D5B
		   pgs[7] = 0x90EA6D01
		   pgs[3] = 0x16C2B307
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶鬼将_普通"then
		   pgs[6] = 0xB0F76191
		   pgs[9] = 0x4C897EEA
		   pgs[8] = 0xE7F3C5A3
		   pgs[5] = 0x1D33D134
		   pgs[4] = 0xF590C5D7
		   pgs[1] = 0x819CF008
		   pgs[2] = 0x600B8E33
		   pgs[7] = 0x6F4CDCDA
		   pgs[3] = 0xEFA9096A
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶画魂_普通"then
		   pgs[6] = 0xD32574C5
		   pgs[9] = 0x9E3606F9
		   pgs[8] = 0x42260944
		   pgs[5] = 0x8B61EDBB
		   pgs[4] = 0x710D687A
		   pgs[1] = 0x20CA6389
		   pgs[2] = 0xCAA9C35B
		   pgs[7] = 0x0F2B0078
		   pgs[3] = 0x9C793FC3
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶画魂_普通"then
		   pgs[6] = 0x987932C9
		   pgs[9] = 0x7476CDF0
		   pgs[8] = 0x549EAE57
		   pgs[5] = 0x5ABBBD65
		   pgs[4] = 0x1ABA2BA8
		   pgs[1] = 0xB0766B67
		   pgs[2] = 0x1688D53E
		   pgs[7] = 0x42B6F092
		   pgs[3] = 0x81B6D4D2
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶净瓶女娲_普通"then
		   pgs[6] = 0x1DF3EFF8
		   pgs[9] = 0x658ECFB8
		   pgs[8] = 0xE6E208F4
		   pgs[5] = 0x9B4B998E
		   pgs[4] = 0xCD97E37C
		   pgs[1] = 0x1EF2E640
		   pgs[2] = 0x677663D1
		   pgs[7] = 0x2856C7A1
		   pgs[3] = 0xFECD9387
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶净瓶女娲_普通"then
		   pgs[6] = 0x72755EC0
		   pgs[9] = 0xE70A1E04
		   pgs[8] = 0x66DFEB44
		   pgs[5] = 0xBC439551
		   pgs[4] = 0xA4304300
		   pgs[1] = 0xD40473AA
		   pgs[2] = 0xDE802E9E
		   pgs[7] = 0x91616888
		   pgs[3] = 0xAC1F8F8C
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶幽萤娃娃_普通"then
		   pgs[6] = 0x70E6A2AE
		   pgs[9] = 0x307C8AB4
		   pgs[8] = 0x95212E44
		   pgs[5] = 0x0E48C1C1
		   pgs[4] = 0xDEEF16AF
		   pgs[1] = 0x7554F1D9
		   pgs[2] = 0xDEE2BA96
		   pgs[7] = 0x2D71552F
		   pgs[3] = 0xAA744C4F
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------125进阶
		elseif pg == "进阶大力金刚_普通"then
		   pgs[6] = 0xC875C4BE
		   pgs[9] = 0x5E86E30F
		   pgs[8] = 0x02F7CD64
		   pgs[5] = 0xBD653956
		   pgs[4] = 0x5B9F487A
		   pgs[1] = 0x12706211
		   pgs[2] = 0xE185B5C8
		   pgs[7] = 0x6B659F88
		   pgs[3] = 0x233FC460
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶大力金刚_普通"then
		   pgs[6] = 0x062C6302
		   pgs[9] = 0x081F6EC9
		   pgs[8] = 0xEB60CB0E
		   pgs[5] = 0x24DA5E7C
		   pgs[4] = 0x0F470034
		   pgs[1] = 0xDFC04681
		   pgs[2] = 0x0C5EBE6E
		   pgs[7] = 0x8DA59241
		   pgs[3] = 0x35B27501
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶大力金刚_普通"then
		   pgs[6] = 0x927A5888
		   pgs[9] = 0x5CA7CBB9
		   pgs[8] = 0x64026916
		   pgs[5] = 0x980897E1
		   pgs[4] = 0xD49AB61F
		   pgs[1] = 0xD126F791
		   pgs[2] = 0xE8A2AB2D
		   pgs[7] = 0x3B84D325
		   pgs[3] = 0xF43D4685
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶金铙僧_普通"then
		   pgs[6] = 0x8D076633
		   pgs[9] = 0xFA5D3745
		   pgs[8] = 0x3418E55B
		   pgs[5] = 0xED78FCF7
		   pgs[4] = 0xCDBE73B1
		   pgs[1] = 0x1AD25609
		   pgs[2] = 0x443B7EBE
		   pgs[7] = 0x4E6467C0
		   pgs[3] = 0x76CF52C3
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶金铙僧_普通"then
		   pgs[6] = 0x78F9978E
		   pgs[9] = 0x06EF88D2
		   pgs[8] = 0x145C9DCE
		   pgs[5] = 0x815A4A92
		   pgs[4] = 0xDFDFC98F
		   pgs[1] = 0x9C87ABFB
		   pgs[2] = 0xAA2C6332
		   pgs[7] = 0x0FFA19B1
		   pgs[3] = 0x03093FAE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶金铙僧_普通"then
		   pgs[6] = 0xEDA4EA2C
		   pgs[9] = 0x359D7B4D
		   pgs[8] = 0x3EA1B740
		   pgs[5] = 0x19DAB780
		   pgs[4] = 0x3C180653
		   pgs[1] = 0x7F58C138
		   pgs[2] = 0xFA0B0A8A
		   pgs[7] = 0x018C11D6
		   pgs[3] = 0xCF1D1CFA
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶灵鹤_普通"then
		   pgs[6] = 0xB15E456E
		   pgs[9] = 0x0E98BCB1
		   pgs[8] = 0xDE8086CE
		   pgs[5] = 0xEAB51B1B
		   pgs[4] = 0xE6A9C9B1
		   pgs[1] = 0x73AAF3C5
		   pgs[2] = 0x812AE416
		   pgs[7] = 0x172D80CC
		   pgs[3] = 0x009DC258
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶琴仙_普通"then
		   pgs[6] = 0x77839D70
		   pgs[9] = 0xB1343D2A
		   pgs[8] = 0x5E0B0694
		   pgs[5] = 0x59F5397F
		   pgs[4] = 0x7E11E45C
		   pgs[1] = 0xA7374E9F
		   pgs[2] = 0x89FB39DD
		   pgs[7] = 0x7316DC16
		   pgs[3] = 0x48248008
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶噬天虎_普通"then
		   pgs[6] = 0xD3A37CF0
		   pgs[9] = 0x21F41A63
		   pgs[8] = 0x88628ABC
		   pgs[5] = 0x975D1C9F
		   pgs[4] = 0x8605C3B4
		   pgs[1] = 0x43D13A6E
		   pgs[2] = 0xBD29FB7A
		   pgs[7] = 0x5AE86B6D
		   pgs[3] = 0x934DB2E8
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶噬天虎_普通"then
		   pgs[6] = 0x994C5596
		   pgs[9] = 0x8967F12E
		   pgs[8] = 0xC0B6CA6A
		   pgs[5] = 0x5BB9AE9A
		   pgs[4] = 0x80F7C4D6
		   pgs[1] = 0xE46F9305
		   pgs[2] = 0xC4E76188
		   pgs[7] = 0xABCBC705
		   pgs[3] = 0xB5783A03
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶雾中仙_普通"then
		   pgs[6] = 0x2D88FD84
		   pgs[9] = 0xAAD6F9F0
		   pgs[8] = 0x83538E76
		   pgs[5] = 0xD9A71892
		   pgs[4] = 0x064F72E1
		   pgs[1] = 0x8F04916C
		   pgs[2] = 0xBCD4820C
		   pgs[7] = 0xACD59062
		   pgs[3] = 0x0CC89F7F
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶雾中仙_普通"then
		   pgs[6] = 0x8D47A766
		   pgs[9] = 0x3F4F2417
		   pgs[8] = 0x73B59DB0
		   pgs[5] = 0x37CB1508
		   pgs[4] = 0x11E8BFA5
		   pgs[1] = 0xD10262FB
		   pgs[2] = 0x7E903BF4
		   pgs[7] = 0x77BFE81B
		   pgs[3] = 0x2AAB7C03
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶炎魔神_普通"then
		   pgs[6] = 0x9F3AE035
		   pgs[9] = 0x4EAC2194
		   pgs[8] = 0x47CD2810
		   pgs[5] = 0x32EA9C0B
		   pgs[4] = 0x79E11A39
		   pgs[1] = 0x895C18AB
		   pgs[2] = 0x1CEFC73C
		   pgs[7] = 0x8983AA04
		   pgs[3] = 0xF6C02A88
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶炎魔神_普通"then
		   pgs[6] = 0x6CE03673
		   pgs[9] = 0x2863F762
		   pgs[8] = 0x99D4FAFD
		   pgs[5] = 0x2592CDA1
		   pgs[4] = 0x95C37C43
		   pgs[1] = 0x2327B8D6
		   pgs[2] = 0xBB223177
		   pgs[7] = 0x3B13B79D
		   pgs[3] = 0xC21E54AE
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶炎魔神_普通"then
		   pgs[6] = 0x238C1BD5
		   pgs[9] = 0xBA7DF8C4
		   pgs[8] = 0xAB879488
		   pgs[5] = 0x6C9BF6D6
		   pgs[4] = 0x3AA4D2D4
		   pgs[1] = 0x7B0B3810
		   pgs[2] = 0xA90307F3
		   pgs[7] = 0x02649602
		   pgs[3] = 0x55227EC5
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶夜罗刹_普通"then
		   pgs[6] = 0xA33EA09B
		   pgs[9] = 0xF8BEAF49
		   pgs[8] = 0x04F157F1
		   pgs[5] = 0xC5A45B35
		   pgs[4] = 0xB4575348
		   pgs[1] = 0x5355C202
		   pgs[2] = 0xF8E3BC67
		   pgs[7] = 0x3B1B850E
		   pgs[3] = 0x64EDA875
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------135进阶
		elseif pg == "进阶红萼仙子_普通"then
		   pgs[6] = 0x0E997872
		   pgs[9] = 0xBC4D1C53
		   pgs[8] = 0xC5891CD4
		   pgs[5] = 0x425E4207
		   pgs[4] = 0x7D0DD03D
		   pgs[1] = 0xA125C0D9
		   pgs[2] = 0x6E9BCD5C
		   pgs[7] = 0xEED1B474
		   pgs[3] = 0xF0DCFC91
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶葫芦宝贝_普通"then
		   pgs[6] = 0xCFE3DEDF
		   pgs[9] = 0xB06E3A58
		   pgs[8] = 0x36A02292
		   pgs[5] = 0x9FA21AAB
		   pgs[4] = 0x3C255AE9
		   pgs[1] = 0x78C89358
		   pgs[2] = 0xA7F1FBD6
		   pgs[7] = 0x3B330253
		   pgs[3] = 0x6D8A1791
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶机关人_普通"then
		   pgs[6] = 0xE05C3F97
		   pgs[9] = 0xD3EAE52F
		   pgs[8] = 0xE3E7268D
		   pgs[5] = 0xD9AB0FE6
		   pgs[4] = 0x5026BD9F
		   pgs[1] = 0x9864B2F1
		   pgs[2] = 0x14DE8746
		   pgs[7] = 0x95EAFF9B
		   pgs[3] = 0xDB2916BF
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶机关人_普通"then
		   pgs[6] = 0x9AC1969F
		   pgs[9] = 0x70C9065C
		   pgs[8] = 0xEEF86490
		   pgs[5] = 0x33832208
		   pgs[4] = 0xF225CDEF
		   pgs[1] = 0x1B74F66D
		   pgs[2] = 0x9D79966C
		   pgs[7] = 0x87240F3C
		   pgs[3] = 0x6AB996E5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶机关人_普通"then
		   pgs[6] = 0xE2E72F59
		   pgs[9] = 0x51570829
		   pgs[8] = 0x00E3DB78
		   pgs[5] = 0x94107020
		   pgs[4] = 0x688C0F73
		   pgs[1] = 0x395380C6
		   pgs[2] = 0x82BBA616
		   pgs[7] = 0x959AC4B3
		   pgs[3] = 0xF1BBB49D
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶狂豹兽形_普通"then
		   pgs[6] = 0xC9A778BE
		   pgs[9] = 0x0A77DD67
		   pgs[8] = 0xE4C9CECB
		   pgs[5] = 0x5ADFBAD2
		   pgs[4] = 0x47D85C84
		   pgs[1] = 0x598F2E29
		   pgs[2] = 0x87EA8781
		   pgs[7] = 0xB83FD89C
		   pgs[3] = 0x438491B0
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶龙龟_普通"then
		   pgs[6] = 0x06C0B25B
		   pgs[9] = 0xDE2011E4
		   pgs[8] = 0xE38771B9
		   pgs[5] = 0x7A833B4A
		   pgs[4] = 0x02DC061B
		   pgs[1] = 0x0707FAF0
		   pgs[2] = 0xA120218F
		   pgs[7] = 0xDCA6D558
		   pgs[3] = 0xD82E5536
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶龙龟_普通"then
		   pgs[6] = 0x93DABA9C
		   pgs[9] = 0xBEA6CF04
		   pgs[8] = 0x918FF89D
		   pgs[5] = 0xE8B052B8
		   pgs[4] = 0x028D8B27
		   pgs[1] = 0xE5F8FFDE
		   pgs[2] = 0x341A1652
		   pgs[7] = 0xDBD74492
		   pgs[3] = 0x41C11874
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶猫灵兽形_普通"then
		   pgs[6] = 0x25C3C6CC
		   pgs[9] = 0x44AA7C09
		   pgs[8] = 0x0580F3FE
		   pgs[5] = 0x32130E88
		   pgs[4] = 0xFD1349E1
		   pgs[1] = 0x99BBA06B
		   pgs[2] = 0xAAC73D6E
		   pgs[7] = 0xD497ABF5
		   pgs[3] = 0x78FECA83
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶踏云兽_普通"then
		   pgs[6] = 0x4B1AAA96
		   pgs[9] = 0xD00BAF62
		   pgs[8] = 0x4194B6ED
		   pgs[5] = 0x2BE58ED4
		   pgs[4] = 0xFA3ED116
		   pgs[1] = 0xD04BFDC1
		   pgs[2] = 0xC6E67E4F
		   pgs[7] = 0xB73151F2
		   pgs[3] = 0xB01111D9
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶踏云兽_普通"then
		   pgs[6] = 0x5F06113B
		   pgs[9] = 0x12B329D4
		   pgs[8] = 0x9F080530
		   pgs[5] = 0xB2F769A1
		   pgs[4] = 0xC8079911
		   pgs[1] = 0x2D2B73A7
		   pgs[2] = 0xA9729279
		   pgs[7] = 0x8841F709
		   pgs[3] = 0xFBCEB0CF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶踏云兽_普通"then
		   pgs[6] = 0x683CA512
		   pgs[9] = 0xB0D7EB0B
		   pgs[8] = 0x17F712C3
		   pgs[5] = 0xC2BFB549
		   pgs[4] = 0x5A09ABF2
		   pgs[1] = 0x5145536E
		   pgs[2] = 0xE8B8F7CB
		   pgs[7] = 0x46F38E3C
		   pgs[3] = 0xD88ACCAE
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶蝎子精_普通"then
		   pgs[6] = 0x95FBDCDD
		   pgs[9] = 0x33DE7412
		   pgs[8] = 0x3295E9E7
		   pgs[5] = 0x3D6CAB31
		   pgs[4] = 0x8007428F
		   pgs[1] = 0xBB5D5C67
		   pgs[2] = 0xC9EAA7FD
		   pgs[7] = 0xD5FDDCC6
		   pgs[3] = 0x80535B44
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶蝎子精_普通"then
		   pgs[6] = 0xAB7F802C
		   pgs[9] = 0xC05C6A43
		   pgs[8] = 0xAB796DF9
		   pgs[5] = 0xDBC4C106
		   pgs[4] = 0xBDF99F86
		   pgs[1] = 0x8F463BC3
		   pgs[2] = 0x16141572
		   pgs[7] = 0x9D66E6B1
		   pgs[3] = 0xAC4626EE
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------145进阶
		elseif pg == "进阶巴蛇_普通"then
		   pgs[6] = 0x7A7EF4C7
		   pgs[9] = 0x42A25B91
		   pgs[8] = 0x6E93E040
		   pgs[5] = 0xB56AD372
		   pgs[4] = 0x048A0E39
		   pgs[1] = 0xB5F77ED3
		   pgs[2] = 0xB5421C20
		   pgs[7] = 0x08939F8B
		   pgs[3] = 0x6E6B0CD6
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶巴蛇_普通"then
		   pgs[6] = 0xB73A2598
		   pgs[9] = 0x0C9239DA
		   pgs[8] = 0xE7E4A2D2
		   pgs[5] = 0x98949BBF
		   pgs[4] = 0x0AFA82C0
		   pgs[1] = 0xD7990680
		   pgs[2] = 0x4BA5FF1A
		   pgs[7] = 0x6DC1B4DF
		   pgs[3] = 0x543F3110
		elseif pg == "进阶机关鸟_普通"then
		   pgs[6] = 0x1C2392B6
		   pgs[9] = 0x444DC98A
		   pgs[8] = 0xDA6ACD74
		   pgs[5] = 0x631DFF0D
		   pgs[4] = 0x32AC0E25
		   pgs[1] = 0x3030BC6E
		   pgs[2] = 0x3D0B6105
		   pgs[7] = 0xAEACFDEF
		   pgs[3] = 0xFFB5760E
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶机关鸟_普通"then
		   pgs[6] = 0x006A893A
		   pgs[9] = 0x6FD7F234
		   pgs[8] = 0x6885D06C
		   pgs[5] = 0x79366637
		   pgs[4] = 0xBF06D5B4
		   pgs[1] = 0xF9A2D92F
		   pgs[2] = 0xB00918E0
		   pgs[7] = 0xAD51D1D3
		   pgs[3] = 0xAA6CAFFF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶机关鸟_普通"then
		   pgs[6] = 0xC01ED510
		   pgs[9] = 0x11CDFE55
		   pgs[8] = 0x5C7A882C
		   pgs[5] = 0xDC0D5F87
		   pgs[4] = 0x987C0D8F
		   pgs[1] = 0x4F1DCEEB
		   pgs[2] = 0xDF08379C
		   pgs[7] = 0xE5C7D18C
		   pgs[3] = 0xF050C69A
		elseif pg == "进阶机关兽_普通"then
		   pgs[6] = 0xB8678BB6
		   pgs[9] = 0x414CCFB2
		   pgs[8] = 0x729B428B
		   pgs[5] = 0x6A043734
		   pgs[4] = 0xF65B08C3
		   pgs[1] = 0x92117A35
		   pgs[2] = 0xF16E2546
		   pgs[7] = 0xFE2F0DCE
		   pgs[3] = 0x3076BA30
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶机关兽_普通"then
		   pgs[6] = 0x66E097D4
		   pgs[9] = 0x35719C69
		   pgs[8] = 0xC50FDA15
		   pgs[5] = 0xCED3968E
		   pgs[4] = 0x0C7B1194
		   pgs[1] = 0xE2FF8FCD
		   pgs[2] = 0x2D9F0E3B
		   pgs[7] = 0x5E8E0238
		   pgs[3] = 0x45C599AD
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶连驽车_普通"then
		   pgs[6] = 0xEDE79D1B
		   pgs[9] = 0xC3CFDBBF
		   pgs[8] = 0x92AC68E8
		   pgs[5] = 0xE622FC91
		   pgs[4] = 0xE15EDC19
		   pgs[1] = 0xB63376EE
		   pgs[2] = 0x1E0A19F3
		   pgs[7] = 0xC8B21F9A
		   pgs[3] = 0x82ABE37B
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶连驽车_普通"then
		   pgs[6] = 0x89C32F37
		   pgs[9] = 0x3F5CB06A
		   pgs[8] = 0x2FC03212
		   pgs[5] = 0x7C4582FA
		   pgs[4] = 0xD473C1AE
		   pgs[1] = 0xB3305C85
		   pgs[2] = 0xD64D0170
		   pgs[7] = 0x35E7C606
		   pgs[3] = 0x78E5196B
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶连驽车_普通"then
		   pgs[6] = 0x05B30C39
		   pgs[9] = 0xA6CEF28B
		   pgs[8] = 0xF62EB02F
		   pgs[5] = 0x4B5EE4D0
		   pgs[4] = 0x44FCF5F0
		   pgs[1] = 0x6064D57C
		   pgs[2] = 0x23F9A218
		   pgs[7] = 0xA4ED552F
		   pgs[3] = 0xFB24EDA7
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------155进阶
		elseif pg == "进阶长眉灵猴_普通"then
		   pgs[6] = 0x4D9B1187
		   pgs[9] = 0xA7EF454A
		   pgs[8] = 0xC14B977D
		   pgs[5] = 0x79AB1D7B
		   pgs[4] = 0xD7CC08FF
		   pgs[1] = 0xCC76F2F8
		   pgs[2] = 0xF74A6D83
		   pgs[7] = 0x0CE73800
		   pgs[3] = 0x2BD4447D
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶长眉灵猴_普通"then
		   pgs[6] = 0x6A23896E
		   pgs[9] = 0x425AF14F
		   pgs[8] = 0x6E76ACA8
		   pgs[5] = 0x0512386F
		   pgs[4] = 0x27A28AEE
		   pgs[1] = 0xD51F4EC0
		   pgs[2] = 0x879BDDFD
		   pgs[7] = 0x618921C2
		   pgs[3] = 0xDDB553F1
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶长眉灵猴_普通"then
		   pgs[6] = 0xB1A037C7
		   pgs[9] = 0x5D231D04
		   pgs[8] = 0x1A5D4986
		   pgs[5] = 0x30F1EB6F
		   pgs[4] = 0x512D7DD2
		   pgs[1] = 0x1970FA1D
		   pgs[2] = 0x3A3687F2
		   pgs[7] = 0xD7FFAFA2
		   pgs[3] = 0x1DE75941
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶混沌兽_普通"then
		   pgs[6] = 0xB6C7E183
		   pgs[9] = 0xCEA78367
		   pgs[8] = 0x2EF50C0F
		   pgs[5] = 0x39F7B0C1
		   pgs[4] = 0x44EC58A8
		   pgs[1] = 0x9749DC26
		   pgs[2] = 0x4C5B3EE1
		   pgs[7] = 0x9F328A5A
		   pgs[3] = 0x11D32EF5
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶混沌兽_普通"then
		   pgs[6] = 0xC7C133B9
		   pgs[9] = 0x1E8FDE56
		   pgs[8] = 0x3BCDB53C
		   pgs[5] = 0xD2924B0D
		   pgs[4] = 0x6D48043D
		   pgs[1] = 0x847933E3
		   pgs[2] = 0x85702C4A
		   pgs[7] = 0xDC88B49F
		   pgs[3] = 0x492F9AA1
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶巨力神猿_普通"then
		   pgs[6] = 0xE03B4CB5
		   pgs[9] = 0x6CF30E59
		   pgs[8] = 0x039BDA0E
		   pgs[5] = 0xB224A947
		   pgs[4] = 0xCD453A51
		   pgs[1] = 0xA8CBBA47
		   pgs[2] = 0x3F197B49
		   pgs[7] = 0x1CCCD4F9
		   pgs[3] = 0x71C43236
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶巨力神猿_普通"then
		   pgs[6] = 0x9AA419BC
		   pgs[9] = 0x23FD3878
		   pgs[8] = 0x83FA33A7
		   pgs[5] = 0x5A0A2D79
		   pgs[4] = 0x95C22BD5
		   pgs[1] = 0xA8F2C40E
		   pgs[2] = 0x14D63E3B
		   pgs[7] = 0x18F60D89
		   pgs[3] = 0x6240BB61
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶巨力神猿_普通"then
		   pgs[6] = 0x2326D947
		   pgs[9] = 0xD4C85679
		   pgs[8] = 0x1680DEA0
		   pgs[5] = 0xA9B4C00B
		   pgs[4] = 0x751597BB
		   pgs[1] = 0xF969D00B
		   pgs[2] = 0x27CD2802
		   pgs[7] = 0x20997909
		   pgs[3] = 0xB637EF96
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶狂豹人形_普通"then
		   pgs[6] = 0xE9E47BF8
		   pgs[9] = 0x4D2FA042
		   pgs[8] = 0xEA42D997
		   pgs[5] = 0x500AEF2F
		   pgs[4] = 0xA960269A
		   pgs[1] = 0x3DC7DB59
		   pgs[2] = 0xC04E7031
		   pgs[7] = 0x910E8FDF
		   pgs[3] = 0x5D2CDD31
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶猫灵人形_普通"then
		   pgs[6] = 0x0BD69BBD
		   pgs[9] = 0x1610F435
		   pgs[8] = 0xCAD8AE33
		   pgs[5] = 0xAA29C1A9
		   pgs[4] = 0xC42F5315
		   pgs[1] = 0xB6A0F272
		   pgs[2] = 0xE2244B5C
		   pgs[7] = 0xD7ADADFC
		   pgs[3] = 0xAC426AF0
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶猫灵人形_普通"then
		   pgs[6] = 0x7A5DF2FD
		   pgs[9] = 0x16211A4F
		   pgs[8] = 0x852B6490
		   pgs[5] = 0x43EB47E6
		   pgs[4] = 0xC5471B0A
		   pgs[1] = 0xD339585F
		   pgs[2] = 0x702BAF11
		   pgs[7] = 0xA12B4BDC
		   pgs[3] = 0x9EC6702C
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶藤蔓妖花_普通"then
		   pgs[6] = 0x7118D645
		   pgs[9] = 0xDE1E53D4
		   pgs[8] = 0x0363158B
		   pgs[5] = 0xDF83BE8C
		   pgs[4] = 0x7DCFA008
		   pgs[1] = 0xBD71D80C
		   pgs[2] = 0xC7954921
		   pgs[7] = 0x170A3BB0
		   pgs[3] = 0x21C0D609
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶修罗傀儡鬼_普通"then
		   pgs[6] = 0x554EBB15
		   pgs[9] = 0xE2700BF7
		   pgs[8] = 0xA49270BA
		   pgs[5] = 0xD50E9C97
		   pgs[4] = 0x20E37E1A
		   pgs[1] = 0x9B7DF1FC
		   pgs[2] = 0x73A017D3
		   pgs[7] = 0x9224DF59
		   pgs[3] = 0xC9CE3035
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶修罗傀儡鬼_普通"then
		   pgs[6] = 0x895B9F2A
		   pgs[9] = 0x223C5F00
		   pgs[8] = 0x28D77EED
		   pgs[5] = 0x4BA65826
		   pgs[4] = 0xA5C4C839
		   pgs[1] = 0xCA4D305D
		   pgs[2] = 0xA07CCF46
		   pgs[7] = 0x3AE7FB97
		   pgs[3] = 0x39AD557F
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶蜃气妖_普通"then
		   pgs[6] = 0xB7008671
		   pgs[9] = 0x61A67852
		   pgs[8] = 0x49DD3B77
		   pgs[5] = 0xE3E61F8E
		   pgs[4] = 0x887522C9
		   pgs[1] = 0x481691FA
		   pgs[2] = 0x9D9B1084
		   pgs[7] = 0x244C4F16
		   pgs[3] = 0x92234C89
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------165进阶
		elseif pg == "进阶金身罗汉_普通"then
		   pgs[6] = 0x2D9806FA
		   pgs[9] = 0x61C5BCE3
		   pgs[8] = 0x73625A2A
		   pgs[5] = 0x026319C9
		   pgs[4] = 0xD6CD3ADC
		   pgs[1] = 0x704A7CEC
		   pgs[2] = 0x0F176911
		   pgs[7] = 0xB0D90BEA
		   pgs[3] = 0x1CE650E4
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶金身罗汉_普通"then
		   pgs[6] = 0x87F218DD
		   pgs[9] = 0x97B03DD9
		   pgs[8] = 0x3C24D4EA
		   pgs[5] = 0x0BFBBE37
		   pgs[4] = 0x34CF819C
		   pgs[1] = 0x9FE8463B
		   pgs[2] = 0xC4C1B226
		   pgs[7] = 0xC75421CE
		   pgs[3] = 0xE0E766CF
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶金身罗汉_普通"then
		   pgs[6] = 0x9FAD6A2E
		   pgs[9] = 0xBD8610DF
		   pgs[8] = 0x51A6BC9D
		   pgs[5] = 0xE7E9C4F7
		   pgs[4] = 0x729DA8B5
		   pgs[1] = 0xF42354A0
		   pgs[2] = 0xAEA12116
		   pgs[7] = 0x474252D5
		   pgs[3] = 0x90BF1DCA
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶曼珠沙华_普通"then
		   pgs[6] = 0x886801BB
		   pgs[9] = 0x8EBF1E45
		   pgs[8] = 0xD8E30A74
		   pgs[5] = 0xADCE3DE2
		   pgs[4] = 0xD759BDC3
		   pgs[1] = 0x25240D95
		   pgs[2] = 0x4697E1B3
		   pgs[7] = 0x72B459C4
		   pgs[3] = 0x09B3229A
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶修罗傀儡妖_普通"then
		   pgs[6] = 0xE3884664
		   pgs[9] = 0xF361F1D2
		   pgs[8] = 0x6783BDA7
		   pgs[5] = 0xCFC70F6F
		   pgs[4] = 0xB87C4198
		   pgs[1] = 0xEADFBA86
		   pgs[2] = 0x577EEC90
		   pgs[7] = 0x0BA257CC
		   pgs[3] = 0xEC3103E4
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶修罗傀儡妖_普通"then
		   pgs[6] = 0xADB211E7
		   pgs[9] = 0x20F20B5A
		   pgs[8] = 0x24CE0BB8
		   pgs[5] = 0xED499864
		   pgs[4] = 0xD1DB1510
		   pgs[1] = 0x3401A187
		   pgs[2] = 0xCBD9CBE2
		   pgs[7] = 0x6D63DDF0
		   pgs[3] = 0xED249538
		   pgs[10] = "ZHS.FT"
	-----------------------------------------------------175进阶
		elseif pg == "进阶般若天女_普通"then
		   pgs[6] = 0x78B01CF9
		   pgs[9] = 0x184A6609
		   pgs[8] = 0x2416E380
		   pgs[5] = 0xD73F0793
		   pgs[4] = 0xF04C4E62
		   pgs[1] = 0xE87E7C3F
		   pgs[2] = 0x16371442
		   pgs[7] = 0x6C341850
		   pgs[3] = 0x0C8B30E8
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶般若天女_普通"then
		   pgs[6] = 0x7C384B09
		   pgs[9] = 0x2CE8DDB6
		   pgs[8] = 0xD8002F08
		   pgs[5] = 0x83B23110
		   pgs[4] = 0x75E7FFE4
		   pgs[1] = 0x025C4870
		   pgs[2] = 0x8EFA8FA1
		   pgs[7] = 0x0CCD1EC9
		   pgs[3] = 0x4EA6AECC
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶般若天女_普通"then
		   pgs[6] = 0xA838AEB3
		   pgs[9] = 0x929C1D3E
		   pgs[8] = 0x7469309E
		   pgs[5] = 0x7DFDF056
		   pgs[4] = 0xD7C8F51F
		   pgs[1] = 0x164B3BE8
		   pgs[2] = 0x517971B2
		   pgs[7] = 0x7230CF8D
		   pgs[3] = 0x20D854D0
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶持国巡守_普通"then
		   pgs[6] = 0x45763667
		   pgs[9] = 0x6F97964A
		   pgs[8] = 0xCCC693A8
		   pgs[5] = 0xD447E4B2
		   pgs[4] = 0xCF762FBB
		   pgs[1] = 0x1FD31FF6
		   pgs[2] = 0x5CFD0CC0
		   pgs[7] = 0xACCB92CC
		   pgs[3] = 0x5AC5EAC2
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶灵灯侍者_普通"then
		   pgs[6] = 0x8B46AE73
		   pgs[9] = 0x490DAC93
		   pgs[8] = 0x6C8D5D7F
		   pgs[5] = 0x3206352D
		   pgs[4] = 0x368BCC8A
		   pgs[1] = 0xC49F0CDC
		   pgs[2] = 0x19FAE659
		   pgs[7] = 0x2E4DDED0
		   pgs[3] = 0x72BD4045
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶灵灯侍者_普通"then
		   pgs[6] = 0x2E6C2503
		   pgs[9] = 0x594A34AC
		   pgs[8] = 0xECC31AEE
		   pgs[5] = 0x9C212D30
		   pgs[4] = 0x21234A22
		   pgs[1] = 0x5C64B5E1
		   pgs[2] = 0x33E397C0
		   pgs[7] = 0x239032AD
		   pgs[3] = 0xADDD0DDF
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶毗舍童子_普通"then
		   pgs[6] = 0x7C15F7DE
		   pgs[9] = 0x40D008F4
		   pgs[8] = 0x1C29D8BC
		   pgs[5] = 0x1374B7F4
		   pgs[4] = 0x42A2E9E0
		   pgs[1] = 0x1A04C90E
		   pgs[2] = 0x0AF863CF
		   pgs[7] = 0x1CB769CA
		   pgs[3] = 0xCCDDD94C
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶毗舍童子_普通"then
		   pgs[6] = 0x2090CD58
		   pgs[9] = 0xD2096B29
		   pgs[8] = 0xF3809653
		   pgs[5] = 0x035D3377
		   pgs[4] = 0xBA313F1D
		   pgs[1] = 0x5EB5809C
		   pgs[2] = 0xEC33FC29
		   pgs[7] = 0xEC27B7C7
		   pgs[3] = 0xB97A1FB7
		   pgs[10] = "ZHS.FT"
		elseif pg == "饰品_进阶毗舍童子_普通"then
		   pgs[6] = 0xD8889A17
		   pgs[9] = 0xB931B053
		   pgs[8] = 0xDA5B6CF9
		   pgs[5] = 0x1A00A800
		   pgs[4] = 0x704014FD
		   pgs[1] = 0x7617EB84
		   pgs[2] = 0x9F573325
		   pgs[7] = 0x820B8C89
		   pgs[3] = 0xA052100D
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶增长巡守_普通"then
		   pgs[6] = 0x1E06D7E8
		   pgs[9] = 0x70884304
		   pgs[8] = 0xC0FCC4AE
		   pgs[5] = 0x8533BBC6
		   pgs[4] = 0xF62FE03D
		   pgs[1] = 0x030573CE
		   pgs[2] = 0x7BF37A7D
		   pgs[7] = 0x6876B5D2
		   pgs[3] = 0x2642269B
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶真陀护法_普通"then
		   pgs[6] = 0x06B907B8
		   pgs[9] = 0x31ABF075
		   pgs[8] = 0x2D2984EB
		   pgs[5] = 0x6F06DCB7
		   pgs[4] = 0x8F0BE3CC
		   pgs[1] = 0xA20D1FB9
		   pgs[2] = 0x2DDCB61A
		   pgs[7] = 0xE9AAD573
		   pgs[3] = 0x2070E3B1
		   pgs[10] = "ZHS.FT"
		elseif pg == "武器_进阶真陀护法_普通"then
		   pgs[6] = 0x25CA9002
		   pgs[9] = 0x48182F46
		   pgs[8] = 0xD85DECF6
		   pgs[5] = 0x55BDE154
		   pgs[4] = 0x1F17C455
		   pgs[1] = 0xF7088AA8
		   pgs[2] = 0x696DC25C
		   pgs[7] = 0xB3F86A1F
		   pgs[3] = 0x013EEA44
		   pgs[10] = "ZHS.FT"
	----------------------------------------------NPC
		elseif pg == "帮派妖兽_普通"then
		   pgs[6] = 0xD252E998
		   pgs[9] = 0x2E8C40EC
		   pgs[8] = 0x0EC64162
		   pgs[5] = 0x4D10286A
		   pgs[4] = 0x0CC07284
		   pgs[1] = 0xC1816D39
		   pgs[2] = 0xC1816D39
		   pgs[7] = 0x048CC7DD
		   pgs[3] = 0xD225AEF5
		   pgs[10] = "ZHS.FT"
		elseif pg == "程咬金_普通"then
		   pgs[6] = 0xE1A120FF
		   pgs[9] = 0x5868EDBE
		   pgs[8] = 0xD99529D8
		   pgs[5] = 0x594994E9
		   pgs[4] = 0x87EF3814
		   pgs[1] = 0xD3E3A473
		   pgs[2] = 0x15125FC7
		   pgs[7] = 0xEE96905A
		   pgs[3] = 0xB41DCF3F
		   pgs[10] = "ZHS.FT"
		elseif pg == "地藏王_普通"then
		   pgs[6] = 0x3A11C300
		   pgs[9] = 0xA8312FDB
		   pgs[8] = 0xD13E1FFF
		   pgs[5] = 0x3FD0D0BD
		   pgs[4] = 0x4E4C9223
		   pgs[1] = 0xDBC25AB2
		   pgs[2] = 0xD572D8E4
		   pgs[7] = 0x1A7C6CAC
		   pgs[3] = 0x7AD0002B
		   pgs[10] = "ZHS.FT"
		elseif pg == "阎罗王_普通"then
		   pgs[6] = 0xA5B436EE
		   pgs[9] = 0x455697FE
		   pgs[8] = 0xE5AB6916
		   pgs[5] = 0x883B1581
		   pgs[4] = 0x9402033C
		   pgs[1] = 0x15F50CFF
		   pgs[2] = 0x15F50CFF
		   pgs[7] = 0xA5B436EE
		   pgs[3] = 0x09152660
		   pgs[10] = "ZHS.FT"
		elseif pg == "东海龙王_普通"then
		   pgs[6] = 0x810E3469
		   pgs[9] = 0xB45AC1CB
		   pgs[8] = 0xEA1FB934
		   pgs[5] = 0x325826A0
		   pgs[4] = 0x0623BF6C
		   pgs[1] = 0xA730EB06
		   pgs[2] = 0x12062DFD
		   pgs[7] = 0xD8AC6ADA
		   pgs[3] = 0x263C28F5
		   pgs[10] = "ZHS.FT"
		elseif pg == "二郎神_普通"then---------------------
		   pgs[6] = 0x894CBF63
		   pgs[9] = 0xCFD07D68
		   pgs[8] = 0x829CFEA0
		   pgs[5] = 0x4D6B7663
		   pgs[4] = 0xFDEB3130
		   pgs[1] = 0x0AF3DDAD
		   pgs[2] = 0x3C04B467
		   pgs[7] = 0x3304B122
		   pgs[3] = 0x506FB295
		   pgs[10] = "ZHS.FT"
		elseif pg == "菩提祖师_普通"then
		   pgs[6] = 0x2073F650
		   pgs[9] = 0xE84C322A
		   pgs[8] = 0x88BE1E61
		   pgs[5] = 0x1331085B
		   pgs[4] = 0x0ACD5800
		   pgs[1] = 0x0A0ED7C6
		   pgs[2] = 0xBF5273C3
		   pgs[7] = 0xFC7EAE69
		   pgs[3] = 0xEF5514E5
		   pgs[10] = "ZHS.FT"
		elseif pg == "观音_普通"then
		   pgs[6] = 0xF9F80A42
		   pgs[9] = 0x5D49F27C
		   pgs[8] = 0xF45349EA
		   pgs[5] = 0x5D4BC070
		   pgs[4] = 0xD503EE2F
		   pgs[1] = 0x93DC0379
		   pgs[2] = 0xABC48778
		   pgs[7] = 0x2ECAAEEF
		   pgs[3] = 0x471104A5
		   pgs[10] = "ZHS.FT"
		elseif pg == "胖和尚_普通"then-----------------
		   pgs[6] = 0x4C752C05
		   pgs[9] = 0x472D738E
		   pgs[8] = 0xFF452F8F
		   pgs[5] = 0x56CC8717
		   pgs[4] = 0x4C752C05
		   pgs[1] = 0x4FD41E69
		   pgs[2] = 0x41C73336
		   pgs[7] = 0x9E7ABA66
		   pgs[3] = 0xC7E0C439

		   pgs[10] = "ZHS.FT"
		elseif pg == "老和尚_普通"then------------------
		   -- pgs[6] = 0x
		   -- pgs[9] = 0x4308F507
		   -- pgs[8] = 0x987B611D
		   -- pgs[5] = 0x
		   -- pgs[4] = 0xBD41CBB0
		   -- pgs[1] = 0x3674178B
		   -- pgs[2] = 0xD5F72B15
		   -- pgs[7] = 0x5B721A00
		   -- pgs[3] = 0xAC761E66
		   -- pgs[10] = "ZHS.FT"
		elseif pg == "猴子_普通"then--------------------
		   -- pgs[6] = 0x
		   -- pgs[9] = 0x5DE938BA
		   -- pgs[8] = 0xCCAC3F69
		   -- pgs[5] = 0x31B2CA29
		   -- pgs[4] = 0xB3A2B015
		   -- pgs[1] = 0x6C1336EC
		   -- pgs[2] = 0x6FF03AB0
		   -- pgs[7] = 0xE42F3F94
		   -- pgs[3] = 0x
		   -- pgs[10] = "ZHS.FT"
		elseif pg == "进阶蚩尤_普通"then
		   pgs[6] = 0xB547D6C5
		   pgs[9] = 0x0C53745F
		   pgs[8] = 0x62F5CF03
		   pgs[5] = 0x0E352C9C
		   pgs[4] = 0xA96AF127
		   pgs[1] = 0x37CC913E
		   pgs[2] = 0x9938240A
		   pgs[7] = 0xB95AC7D1
		   pgs[3] = 0xCF27623F
		   静立= 0x5D1C654C
		   行走= 0x84E89BDB
		   pgs[10] = "ZHS.FT"
		elseif pg == "九头精怪_普通"then
		   pgs[6] = 0x7CAF3ABB
		   pgs[9] = 0xCEF45818
		   pgs[8] = 0x6C396AD2
		   pgs[5] = 0x5B2FE6A5
		   pgs[4] = 0x540D1100
		   pgs[1] = 0xC9EC64BB
		   pgs[2] = 0xC9EC64BB
		   pgs[7] = 0xBB56EF5A
		   pgs[3] = 0x92F0C045
		   pgs[10] = "ZHS.FT"
		elseif pg == "模型_普通"then
		   pgs[6] = 0xBFCA22B3
		   pgs[9] = 0x54164C29
		   pgs[8] = 0x0CD46D49
		   pgs[5] = 0x6144B148
		   pgs[4] = 0xF7A4F1E2
		   pgs[1] = 0xA2A59267
		   pgs[2] = 0xA2A59267
		   pgs[7] = 0xC21FDC40
		   pgs[3] = 0x046D74A8
		   pgs[10] = "ZHS.FT"
		elseif pg == "牛魔王_普通"then
		   pgs[6] = 0x0B69B44D
		   pgs[9] = 0x90BB1A9E
		   pgs[8] = 0xD8CFAC15
		   pgs[5] = 0x543D78D5
		   pgs[4] = 0xC096C013
		   pgs[1] = 0xC18A0B9F
		   pgs[2] = 0xDFAB4445
		   pgs[7] = 0x65EB0501
		   pgs[3] = 0x113CB79E
		   pgs[10] = "ZHS.FT"
		elseif pg == "孙婆婆_普通"then
		   pgs[6] = 0xFBB05C33
		   pgs[9] = 0xEED0BF37
		   pgs[8] = 0x6F41C860
		   pgs[5] = 0xAA845245
		   pgs[4] = 0x60F5A1CF
		   pgs[1] = 0x8ACDDCAE
		   pgs[2] = 0x44EE03AD
		   pgs[7] = 0x7E2E23A0
		   pgs[3] = 0x06F4EEC5
		   pgs[10] = "ZHS.FT"
		elseif pg == "白晶晶_普通"then----------------------------
		   -- pgs[6] = 0x
		   -- pgs[9] = 0x7FE8694C
		   -- pgs[8] = 0x2B6371A2
		   -- pgs[5] = 0x164C2F9C
		   -- pgs[4] = 0xEF4D4EBB
		   -- pgs[1] = 0x0D45D160
		   -- pgs[2] = 0x7963E3C2
		   -- pgs[7] = 0xE1D303C3
		   -- pgs[3] = 0xE702BDC7
		   -- pgs[10] = "ZHS.FT"
		elseif pg == "沙僧_普通"then
		   pgs[6] = 0x9E8754B2
		   pgs[9] = 0xFAADCE7F
		   pgs[8] = 0x13550448
		   pgs[5] = 0x1E40F132
		   pgs[4] = 0x63DF2FE0
		   pgs[1] = 0x1D90BAA0
		   pgs[2] = 0x1D90BAA0
		   pgs[7] = 0xBB5DC671
		   pgs[3] = 0xDAFF6024
		   pgs[10] = "ZHS.FT"
		elseif pg == "神木_普通"then---------------
		   pgs[6] = 0xA9DD17BC
		   pgs[9] = 0xA1C77A74
		   pgs[8] = 0x4BC4A592
		   pgs[5] = 0xE217FC34
		   pgs[4] = 0x4286FB27
		   pgs[1] = 0x3D7718F1
		   pgs[2] = 0xAB3F493A
		   pgs[7] = 0x0D6E6F43
		   pgs[3] = 0x6DED77DA
		   pgs[10] = "ZHS.FT"
		elseif pg == "三大王_普通"then
		   pgs[6] = 0xCE62A786
		   pgs[9] = 0x5E8396A5
		   pgs[8] = 0xBE283FFC
		   pgs[5] = 0x8F75AEC7
		   pgs[4] = 0xF8EE8A5D
		   pgs[1] = 0x99871326
		   pgs[2] = 0xBD490B9E
		   pgs[7] = 0xD5A6A3B1
		   pgs[3] = 0xD9B62122
		   pgs[10] = "ZHS.FT"
		elseif pg == "巫师_普通"then
		   pgs[6] = 0x3C76D8F5
		   pgs[9] = 0x57C06F84
		   pgs[8] = 0xE2F35B90
		   pgs[5] = 0x53845609
		   pgs[4] = 0x5DACB03A
		   pgs[1] = 0x51493CA4
		   pgs[2] = 0x51493CA4
		   pgs[7] = 0xA5AE9914
		   pgs[3] = 0x3667C87D
		   pgs[10] = "ZHS.FT"
		elseif pg == "无底洞_普通"then--------------
		   pgs[6] = 0x2EDEAF19
		   pgs[9] = 0xC4FD3D4D
		   pgs[8] = 0xA69241A0
		   pgs[5] = 0x53DDB16A
		   pgs[4] = 0x5E0EFF3A
		   pgs[1] = 0x3AE7A8F2
		   pgs[2] = 0x78F227D8
		   pgs[7] = 0x65192B88
		   pgs[3] = 0xD4124C46
		   pgs[10] = "ZHS.FT"
		elseif pg == "哮天犬_普通"then
		   pgs[6] = 0xE2F56A21
		   pgs[9] = 0x1D94428C
		   pgs[8] = 0xE873A88F
		   pgs[5] = 0x892CF6CE
		   pgs[4] = 0xFB2593E3
		   pgs[1] = 0x67B9A21D
		   pgs[2] = 0xAEC5D0D9
		   pgs[7] = 0xEC8A08AA
		   pgs[3] = 0xD926D7D7
		   静立= 0x39796CC6
		   行走= 0xEE55164B
		   pgs[10] = "ZHS.FT"
		elseif pg == "进阶哮天犬_普通"then
		   pgs[6] = 0xDE1355D4
		   pgs[9] = 0x9856C8FF
		   pgs[8] = 0x603FCBF9
		   pgs[5] = 0x02572BF6
		   pgs[4] = 0x80599206
		   pgs[1] = 0xAA0683ED
		   pgs[2] = 0xEBB6057C
		   pgs[7] = 0xE06F5D8A
		   pgs[3] = 0xFB7E93DE
		   静立= 0x09351393
		   行走= 0x8A284760
		   pgs[10] = "ZHS.FT"
		elseif pg == "小白象_普通"then
		   pgs[6] = 0x4411FB6D
		   pgs[9] = 0x0311FF88
		   pgs[8] = 0xA3AA2E20
		   pgs[5] = 0x781F0443
		   pgs[4] = 0xF4387487
		   pgs[1] = 0x9AEFEBC6
		   pgs[2] = 0x9AEFEBC6
		   pgs[7] = 0xC4F59DF9
		   pgs[3] = 0xEA9A837D
		   静立= 0x098C776C
		   行走= 0x7263E8FA
		   pgs[10] = "ZHS.FT"
		elseif pg == "镇元子_普通"then
		   pgs[6] = 0x75CDF97C
		   pgs[9] = 0x5AD58D24
		   pgs[8] = 0x578BA3B5
		   pgs[5] = 0x9517238B
		   pgs[4] = 0xAE592351
		   pgs[1] = 0x9E9D4886
		   pgs[7] = 0x16385B4D
		   pgs[2] = 0x9F64FB32
		   pgs[3] = 0x054F4C87
		   pgs[10] = "ZHS.FT"
		elseif pg == "知了王_普通"then
		   pgs[6] = 0x790BD1B7
		   pgs[9] = 0xE425B3FE
		   pgs[8] = 0x34713A17
		   pgs[5] = 0x93117A64
		   pgs[4] = 0x03847C1E
		   pgs[1] = 0x34C5779E
		   pgs[2] = 0x34C5779E
		   pgs[7] = 0xA49609CA
		   pgs[3] = 0x2BAD174A
		   pgs[10] = "ZHS.FT"
		elseif pg == "周杰伦_普通"then
		   pgs[6] = 0xA81EA7D2
		   pgs[9] = 0x31B65A0A
		   pgs[8] = 0x3EBB092D
		   pgs[5] = 0xF2AE7F68
		   pgs[4] = 0xB67D1163
		   pgs[1] = 0x7B6CA3D6
		   pgs[2] = 0xAA5313BB
		   pgs[7] = 0xAD73D867
		   pgs[3] = 0xB9C13DAB
		   pgs[10] = "ZHS.FT"
		elseif pg == "猪八戒_普通"then
		   pgs[6] = 0x046ADE41
		   pgs[9] = 0x94C75679
		   pgs[8] = 0xDE64A152
		   pgs[5] = 0x522B9A47
		   pgs[4] = 0x00C8DD73
		   pgs[1] = 0x1D7E5D02
		   pgs[2] = 0x1D7E5D02
		   pgs[7] = 0xAFF97BB0
		   pgs[3] = 0xFD013477
		   pgs[10] = "ZHS.FT"
		elseif pg == "郑镖头_普通"then
		   pgs[6] = 0x4F98584F
		   pgs[9] = 0x2A6F55BB
		   pgs[8] = 0x429649F9
		   pgs[5] = 0x4554878B
		   pgs[4] = 0x20A48C2C
		   pgs[1] = 0x4610A379
		   pgs[2] = 0xA76DE573
		   pgs[7] = 0xEDE20DC2
		   pgs[3] = 0xDD6DFE7A
		   pgs[10] = "ZHS.FT"
		elseif pg == "蚩尤_普通"then
		   pgs[6] = 0x00000221
		   pgs[9] = 0x00000225
		   pgs[8] = 0x00000226
		   pgs[5] = 0x00000223
		   pgs[4] = 0x00000219
		   pgs[1] = 0x00000217
		   pgs[2] = 0x00000217
		   pgs[7] = 0x00000222
		   pgs[3] = 0x00000218
		   静立= 0x00000220
		   行走= 0x00000224
		   pgs[10] = "ZHS.FT"

			end
			-- 1是攻击 2是第二种形态攻击 3是死亡 4是防御 6是待战 7是施法 8是跑去 9是挨打 10是资源包 11返回
			return pgs
		end




