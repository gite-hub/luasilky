-- 加载数据
local 拦截配置列表 = {}
拦截配置列表['坐骑动画.ini'] = require('Script/坐骑ini/坐骑动画数据')

local 置ini = 引擎.置ini
local 读ini = 引擎.读ini
local 当前ini = ''
function 引擎.置ini(ini文件名)
	置ini(ini文件名)
	当前ini = ini文件名
end
function 引擎.读ini(配置节名,配置项名)
	local 数据表 = 拦截配置列表[当前ini]
	if 数据表 then
		return 数据表[配置节名] and 数据表[配置节名][配置项名]
	end

    return 读ini(配置节名,配置项名)
end
