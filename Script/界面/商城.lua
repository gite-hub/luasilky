--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-03-22 14:14:21
--======================================================================--
local 场景类_商店 = class()
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local sts1 = {"单价","数量","总额","仙玉"}
local sts2 = {"单价","数量","总额","积分"}
local tos = 引擎.取金钱颜色
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起
local ani = 引擎.取战斗模型
local bw = require("gge包围盒")(0,0,134,34)
local box = 引擎.画矩形
local bd0 = {"气血","魔法","攻击","防御","速度","灵力"}
local bd = {"体质","魔力","力量","耐力","敏捷"}
local  tp = 引擎.场景


function 场景类_商店:初始化(根)
    tp=根
	self.ID = 56
	self.x = 100
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.本类开关 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.zyz = {
		[1] = 资源:载入('JM.FT',"网易WDF动画",0x5B8B823E),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"购买"),
		--[4] = 资源:载入('JM.FT',"网易WDF动画",0xE9C090A3),B17505CF,E6490543--
		[4] = 自适应.创建(2,1,458,257,3,9),
		[5] = 自适应.创建(3,1,94,19,1,3),
		--[6] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xD1E06D84),0,0,4,true,true),
		--[7] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xB0AB6528),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[7] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[8] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"充值"),
        [9] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"宝石商城"),
        [10] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"法宝商城"),
        [11] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"锦衣商城"),
        [12] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"银币宝宝"),
        [13] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"神兽商城"),
     ---预留
        [14] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"进阶变身"),
		[15] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"特殊法宝"),
		[16] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"一二符石"),
		[17] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"11"),
		[18] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"二级法宝"),
		[19] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"消耗药品"),
		[20] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"脚印商城"),
		[21] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"坐骑商城"),
		[22] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"抽奖系统"),
		[23] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"兑换银子"),
		[24] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"宝石商城"),
		[25] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"三级符石"),
		[26] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"11"),
		[27] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"购买"),
		[28] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"返回"),
		[29] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xD1E06D84),0,0,4,true,true),
		[30] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xB0AB6528),0,0,4,true,true),
		[31] = 自适应.创建(2,1,302,349,3,9),
		[32] = 资源:载入('JM.FT',"网易WDF动画",0x363AAF1B),
		[33] = 自适应.创建(2,1,158,153,3,9),
		[34] = 自适应.创建(2,1,125,125,3,9),
		[45] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"站立"),
		[46] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"行走"),
		[47] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"积分商城"),
		[48] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"比武积分"),
		[49] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"妖魔积分"),
		[50] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"副本积分"),
		[51] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"雪人积分"),
		[52] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"BOSS积分"),
	}
	for n=2,3 do
	   self.zyz[n]:绑定窗口_(56)
	end
	for n=6,30 do
	   self.zyz[n]:绑定窗口_(56)
	end
	self.zyz[45]:绑定窗口_(56)
	self.zyz[46]:绑定窗口_(56)
	self.zyz[47]:绑定窗口_(56)
	self.zyz[48]:绑定窗口_(56)
	self.zyz[49]:绑定窗口_(56)
	self.zyz[50]:绑定窗口_(56)
	self.zyz[51]:绑定窗口_(56)
	self.zyz[52]:绑定窗口_(56)
	self.商品 = {}

	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('商店总控件')
	总控件:置可视(true,true)
	self.srk = 总控件:创建输入("数量输入",self.x + 220,self.y + 345,100,14)
	self.srk:置可视(false,false)
	self.srk:置限制字数(3)
	self.srk:置数字模式()
	self.单价 = 0
	self.数量 = 0
	self.上一次 = 1
	self.窗口时间 = 0

	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts3 = 根.字体表.普通字体__
    self.加入 = 0
	self.结束 = 45
	self.状态= "仙灵店铺"
	self.焦点1 = false
	self.头像组 ={}
	self.召唤兽数据={}
	self.选择 =0
	self.锦衣选中=0
	self.临时动作=1
end
function 场景类_商店:置形象()
	self.zyz[34] = nil
	self.zyz[35] = nil
	if self.召唤兽数据[self.选中召唤兽] ~= nil then
		self.zyz[34] =tp.召唤兽动画类.创建(self.召唤兽数据[self.选中召唤兽].造型,self.召唤兽数据[self.选中召唤兽].变异,self.召唤兽数据[self.选中召唤兽].饰品)

	end
end
function 场景类_商店:锦衣置形象(bw,mc)
	self.锦衣模型 = nil
local 锦衣数据={}
if bw=="衣服" then
	bw="锦衣"

	end
锦衣数据[bw] =mc
 if mc=="华风汉雅" or mc == "夏日清凉" or mc == "萌萌小厨" or mc == "夜影"  then
 	锦衣数据={}
 end
	if self.锦衣选中 ~= 0 then
	self.锦衣模型 =引擎.场景.角色动画类.创建(self.玩家造型,{a=0,b=0,c=0},nil,nil,0,nil,nil,锦衣数据)

	end
end
function 场景类_商店:打开(数据)


	if self.本类开关 then

		self.道具 = nil
		self.单价 = 0
		self.数量 = 0
		self.本类开关 = false

		self.上一次 = 1
		self.srk:置焦点(false)
		self.srk:置可视(false,false)
		self.头像组 ={}
		self.召唤兽数据={}
			self.商品={}
self.锦衣选中=0
self.临时动作=1
		self.选中召唤兽 = 0
	else
		self.选中召唤兽 =0
		self.锦衣选中=0
		self.选择 =0
		self.加入 = 0
        self.头像组 ={}
		self.开始 = 1
		self.结束 = 45
		self.状态= "仙灵店铺"
      self.锦衣选中=0


		 self.临时动作=1

 self.本类开关=true
 self.数据=数据
 self.组号=数据.组号
      if self.数据.造型~=nil then
     	self.玩家造型=self.数据.造型
     end
 for n=1,#self.数据 do

     self.商品[n]=物品数据类.创建(self.数据[n],2)
    self.商品[n].编号=n
    self.商品[n].确定=false
      end


	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.本类开关 = true
	end
end
function 场景类_商店:刷新(数据)
	self.选中召唤兽=0
	self.锦衣选中=0
	self.召唤兽数据={}
	self.头像组 ={}
		-- self.开始 = 1
		-- self.结束 = 45
 self.选中召唤兽 =0
		self.道具 = nil
		self.单价 = 0
		self.数量 = 0

		self.上一次 = 1
		self.srk:置焦点(false)
		self.srk:置可视(false,false)
			self.商品={}

self.临时动作=1
 self.数据=数据
 self.组号=数据.组号

 if self.组号==101  then
 	self.积分=数据.积分
 elseif self.组号==102  then
 	self.积分2=数据.积分2+0
 elseif self.组号==103  then
 	self.积分3=数据.积分3+0
 elseif self.组号==104  then
 	self.积分4=数据.积分4+0
 elseif self.组号==105  then
 	self.积分5=数据.积分5+0
 else
 	self.积分=0
 end
 for n=1,#self.数据 do
     self.商品[n]=物品数据类.创建(self.数据[n],2)
    self.商品[n].编号=n
    self.商品[n].确定=false
      end




end

function 场景类_商店:刷新宝宝(数据)

        self.头像组 ={}
        self.临时动作=1
		self.选择 =0
		self.选中召唤兽 = 0
		self.锦衣选中=0
		self.道具 = nil
		self.单价 = 0
		self.数量 = 0
		self.上一次 = 1
		self.商品={}
		self.srk:置焦点(false)
		self.srk:置可视(false,false)
		self.加入 = 0

		self.召唤兽数据=数据
		self.组号=数据.组号

		for i=1,#self.召唤兽数据 do
			if self.召唤兽数据 [i] ~= nil then
				local n = 引擎.取头像(self.召唤兽数据[i].造型)
				self.头像组[i] = 引擎.场景.资源:载入(n[4],"网易WDF动画",n[3])
			end
	   end
end

function 场景类_商店:更新(dt)
	tp.物品格子焦点_:更新(dt)
	self.zyz[2]:更新(鼠标.x,鼠标.y)
	self.zyz[48]:更新(鼠标.x,鼠标.y)
	self.zyz[49]:更新(鼠标.x,鼠标.y)
	self.zyz[50]:更新(鼠标.x,鼠标.y)
	self.zyz[51]:更新(鼠标.x,鼠标.y)
	self.zyz[52]:更新(鼠标.x,鼠标.y)
			if self.zyz[45]:事件判断() then
				self.临时动作=1
			elseif self.zyz[46]:事件判断() then
                self.临时动作=2
			end
	if (self.zyz[2]:事件判断()) or (self:界面重叠() and 引擎.鼠标按下(0x1) and self.焦点== false) and tp.主界面.界面数据[21].本类开关==false then
	self:打开()
	end

	self.zyz[3]:更新(鼠标.x,鼠标.y,self.道具 ~= nil)
	self.焦点 = false
	self.焦点1 = false
    for i=9,26 do
		self.zyz[i]:更新(鼠标.x,鼠标.y,self.状态~=self.zyz[i]:取文字())
	end
	self.zyz[6]:更新(鼠标.x,鼠标.y,self.开始 ~= 1)
	self.zyz[7]:更新(鼠标.x,鼠标.y,#self.数据 > self.结束 )
	self.zyz[8]:更新(鼠标.x,鼠标.y)
	self.zyz[47]:更新(鼠标.x,鼠标.y)
	if self.zyz[3]:事件判断() then
		  self.发送信息={编号=self.上一次,数量=self.数量,组号=self.组号}
		 客户端:发送数据(self.上一次,self.数量,14,self.组号)

    elseif self.zyz[10]:事件判断() then
     	self.状态= "仙玉店铺"
        客户端:发送数据(50,83,13,"52",1)
        self.开始 = 1
		self.结束 = 45
    elseif self.zyz[11]:事件判断() then
     	self.状态= "锦衣商城"
        客户端:发送数据(50,84,13,"52",1)
        self.开始 = 1
		self.结束 = 30
    elseif self.zyz[47]:事件判断()  then
     	self.状态= "积分商城"
        客户端:发送数据(50,211,13,"52",1)
        self.开始 = 1
		self.结束 = 30

    elseif self.zyz[12]:事件判断() then
     	self.状态= "银币宝宝"
         客户端:发送数据(50,85,13,"52",1)
        self.开始 = 1
		self.结束 = 45
    elseif self.zyz[13]:事件判断() then
        self.状态= "仙玉宝宝"
          客户端:发送数据(50,86,13,"52",1)
        self.开始 = 1
		self.结束 = 45
    elseif self.zyz[9]:事件判断() then
      	self.状态= "仙灵店铺"
         客户端:发送数据(50,82,13,"52",1)
        self.开始 = 1
		self.结束 = 45
	end
	if self.状态=="积分商城"  or self.状态=="积分商城2"or self.状态=="积分商城3"or self.状态=="积分商城4" or self.状态=="积分商城5"then
		  if self.zyz[48]:事件判断()  then
     	self.状态= "积分商城"
        客户端:发送数据(50,211,13,"52",1)
        self.开始 = 1
		self.结束 = 30
    elseif self.zyz[49]:事件判断() then
     	self.状态= "积分商城2"
        客户端:发送数据(50,212,13,"52",1)
        self.开始 = 1
		self.结束 = 30
    elseif self.zyz[50]:事件判断() then
     	self.状态= "积分商城3"
        客户端:发送数据(50,213,13,"52",1)
        self.开始 = 1
		self.结束 = 30
    elseif self.zyz[51]:事件判断() then
     	self.状态= "积分商城4"
        客户端:发送数据(50,214,13,"52",1)
        self.开始 = 1
		self.结束 = 30
    elseif self.zyz[52]:事件判断() then
     	self.状态= "积分商城5"
        客户端:发送数据(50,215,13,"52",1)
        self.开始 = 1
		self.结束 = 30
	end
	end
	if self.状态== "锦衣商城" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
	elseif self.状态== "积分商城" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
	elseif self.状态== "积分商城2" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
	elseif self.状态== "积分商城3" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
	elseif self.状态== "积分商城4" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
	elseif self.状态== "积分商城5" then
	if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 30
		self.结束 = self.结束 - 30
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 30
		self.结束 = self.结束 + 30
	end
else
		if self.zyz[6]:事件判断() then
	    self.开始 = self.开始 - 45
		self.结束 = self.结束 - 45
    elseif self.zyz[7]:事件判断() then
      	self.开始 = self.开始 + 45
		self.结束 = self.结束 + 45
	end
	end
		if self.zyz[27]:事件判断() then
	     self.发送信息={编号=self.选中召唤兽,数量=1,组号=self.组号}
		   客户端:发送数据(self.选中召唤兽,1,14,self.组号)
		elseif self.zyz[28]:事件判断() then

		elseif self.zyz[29]:事件判断() then
			self.选择 =self.选择 -16
		elseif self.zyz[30]:事件判断() then
		    self.选择 =self.选择 +16
		end
		self.zyz[27]:更新(鼠标.x,鼠标.y)
     if self.zyz[34]~=nil and  self.召唤兽数据[self.选中召唤兽] ~=nil then
        self.zyz[34]:更新(dt,"静立")
     end
     if self.锦衣选中~=0 then
     	self.锦衣模型:更新(dt,"静立")
     	self.锦衣模型:更新(dt,"行走")
     end
     self.zyz[29]:更新(鼠标.x,鼠标.y,self.选择 > 0)
  	 self.zyz[30]:更新(鼠标.x,鼠标.y,#self.召唤兽数据 > self.选择)
     self.控件类:更新(dt,鼠标.x,鼠标.y)
end

function 场景类_商店:显示(x,y)
	if self:界面重叠() and tp.主界面.界面数据[21].本类开关 ==false then
	 self:拖拽()
	end
	self.zyz[1]:显示(self.x,self.y)
	zts1:显示(self.x+252,self.y+12,"商  城")
	self.zyz[2]:显示(self.x+522,self.y+26)
	tp.横排花纹背景_:置区域(0,0,478,18)
	tp.横排花纹背景_:显示(self.x+34,self.y+60)
	tp.横排花纹背景_:显示(self.x+34,self.y+340)
	tp.竖排花纹背景_:置区域(0,0,18,300)
	tp.竖排花纹背景1_:置区域(0,0,18,300)
	tp.竖排花纹背景_:显示(self.x+16,self.y+85)
	tp.竖排花纹背景1_:显示(self.x+510,self.y+85)


		self.zyz[9]:显示(self.x+78+0*80,self.y+38,true,nil,nil,self.状态==self.zyz[9]:取文字(),2)
		self.zyz[10]:显示(self.x+78+1*80,self.y+38,true,nil,nil,self.状态==self.zyz[10]:取文字(),2)
		self.zyz[11]:显示(self.x+78+2*80,self.y+38,true,nil,nil,self.状态==self.zyz[11]:取文字(),2)
		self.zyz[47]:显示(self.x+78+3*80,self.y+38,true,nil,nil,self.状态==self.zyz[12]:取文字(),2)
		self.zyz[13]:显示(self.x+78+4*80,self.y+38,true,nil,nil,self.状态==self.zyz[13]:取文字(),2)


	if self.状态 ~= "仙玉宝宝" and self.状态 ~= "银币宝宝" then
			zts:置颜色(0xFFFFFFFF)
	       zts:置字间距(15)
	       local  序号21 = 0
			for i=0,1 do
				for o=0,1 do
					序号21=序号21+1
				self.zyz[5]:显示(self.x+105+o*148,self.y+370+i*24)
				if self.状态 =="仙玉店铺" or  self.状态 =="锦衣商城" then
				zts:显示(self.x+57+o*148,self.y+374+i*24,sts1[序号21])
			   elseif   self.状态 =="积分商城" or self.状态 =="积分商城2" or self.状态 =="积分商城3" or self.状态 =="积分商城4" or self.状态 =="积分商城5" then
			   	zts:显示(self.x+57+o*148,self.y+374+i*24,sts2[序号21])
			    else
			    	zts:显示(self.x+57+o*148,self.y+374+i*24,sts[序号21])
			    end
				end
			end

			zts:置字间距(0)

			if  self.状态 =="锦衣商城" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[34]:显示(self.x+375,self.y+78)
			elseif  self.状态 =="积分商城" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[48]:显示(self.x+385,self.y+90)
			self.zyz[49]:显示(self.x+385,self.y+120)
			self.zyz[50]:显示(self.x+385,self.y+150)
			self.zyz[51]:显示(self.x+385,self.y+180)
			self.zyz[52]:显示(self.x+385,self.y+210)
			--self.zyz[34]:显示(self.x+375,self.y+78)
			elseif  self.状态 =="积分商城2" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[48]:显示(self.x+385,self.y+90)
			self.zyz[49]:显示(self.x+385,self.y+120)
			self.zyz[50]:显示(self.x+385,self.y+150)
			self.zyz[51]:显示(self.x+385,self.y+180)
			self.zyz[52]:显示(self.x+385,self.y+210)
			elseif  self.状态 =="积分商城3" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[48]:显示(self.x+385,self.y+90)
			self.zyz[49]:显示(self.x+385,self.y+120)
			self.zyz[50]:显示(self.x+385,self.y+150)
			self.zyz[51]:显示(self.x+385,self.y+180)
			self.zyz[52]:显示(self.x+385,self.y+210)
			elseif  self.状态 =="积分商城4" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[48]:显示(self.x+385,self.y+90)
			self.zyz[49]:显示(self.x+385,self.y+120)
			self.zyz[50]:显示(self.x+385,self.y+150)
			self.zyz[51]:显示(self.x+385,self.y+180)
			self.zyz[52]:显示(self.x+385,self.y+210)
			elseif  self.状态 =="积分商城5" then
			self.zyz[4]:置宽高(318,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,5 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492-140,self.y+30+51*o,0xFF000053)
			end
			tp.竖排花纹背景1_:置区域(0,0,18,250)
			tp.竖排花纹背景1_:显示(self.x+355,self.y+78)
			self.zyz[6]:显示(self.x+355,self.y+78,true)
			self.zyz[7]:显示(self.x+355,self.y+320,true)
			self.zyz[48]:显示(self.x+385,self.y+90)
			self.zyz[49]:显示(self.x+385,self.y+120)
			self.zyz[50]:显示(self.x+385,self.y+150)
			self.zyz[51]:显示(self.x+385,self.y+180)
			self.zyz[52]:显示(self.x+385,self.y+210)
		else
			self.zyz[4]:置宽高(458,257)
			self.zyz[4]:显示(self.x+35,self.y+80)
			for i=1,8 do
					引擎.画线(self.x+34+i*51,self.y+81,self.x+34+i*51,self.y+336,0xFF000053)
			end
			for o=1,5 do
				引擎.画线(self.x+34,self.y+30+51*o,self.x+492,self.y+30+51*o,0xFF000053)
			end
			self.zyz[6]:显示(self.x+492,self.y+78,true)
			self.zyz[7]:显示(self.x+492,self.y+320,true)
		end
			self.zyz[3]:显示(self.x+393,self.y+373,true)
			-- self.zyz[8]:显示(self.x+455,self.y+373)

			local xx = 0
			local yy = 0
			self.选择1=nil
			if self.状态=="锦衣商城" then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.锦衣选中=i
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end
							 self:锦衣置形象(self.商品[self.锦衣选中].数据.部位,self.商品[self.锦衣选中].数据.名称)
							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end
            if self.锦衣选中~=0 then
           	self.zyz[45]:更新(鼠标.x,鼠标.y)
	         self.zyz[46]:更新(鼠标.x,鼠标.y)
			self.zyz[45]:显示(self.x+385,self.y+246)
			self.zyz[46]:显示(self.x+445,self.y+246)
			 if self.临时动作==1 then
            	self.锦衣模型:显示("静立",self.x+435,self.y+176)
            else
            	self.锦衣模型:显示("行走",self.x+435,self.y+176)
            end
            end
  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
			elseif self.状态=="积分商城"  then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end

							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
			elseif self.状态=="积分商城2"  then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end

							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
			elseif self.状态=="积分商城3"  then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end

							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
			elseif self.状态=="积分商城4"  then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end

							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
				elseif self.状态=="积分商城5"  then
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end

							end

							self.srk:置文本(self.数量)
						end



				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.锦衣选中=i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 6 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
else
			for i=self.开始,self.结束 do
	              if self.商品[i] ~= nil then
					self.商品[i]:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					 if self.商品[i].确定 then
	                	tp.物品格子确定_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
	                 end
					if self.商品[i]:获取焦点() then
						self.选择1 =i
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i]
								self.单价 = self.数据[i].价格
								self.数量 = 1
								self.srk:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end
				        if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i]
									self.单价 = self.数据[i].价格
									self.数量 = 1
									self.srk:置可视(true,true)
								end
							end
							self.srk:置文本(self.数量)
						end

					tp.物品格子焦点_:显示(self.x + xx * 51+36,self.y + yy * 51 +80)
					end
					xx = xx + 1
					if xx == 9 then
						xx = 0
						yy = yy + 1
					end
				 end
			end

  	        if self.选择1 ~= nil  then
				      self.商品[self.选择1]:显示事件()
				end
end
			if self.道具 ~= nil then
				zts:置颜色(-16777216)
				zts:显示(self.x + 115,self.y + 373,self.单价)
				self.srk:置坐标(self.x-57,self.y-52)
				-- if self.srk:取文本() == "" then
				-- 	self.srk:置文本(1)
				-- end
				-- if self.srk:取文本()+0 > 99 then
				-- 	self.srk:置文本(99)
				-- end
				-- if self.srk:取文本()+0 < 0 then
				-- 	self.srk:置文本(0)
				-- end
				self.数量 = (self.srk:取文本() == "") and 0 or self.srk:取文本()
				zts:置颜色(tos(self.数量 * self.单价))
				zts:显示(self.x + 115,self.y + 397,(self.数量 * self.单价))
			end
			if self.状态 == "仙玉店铺" or self.状态 == "锦衣商城" then
			zts:置颜色(tos(self.数据.仙玉+0))
			zts:显示(self.x + 263,self.y + 397,self.数据.仙玉)
		   elseif self.状态 == "积分商城"  then
         	zts:置颜色(tos(self.积分))
			zts:显示(self.x + 263,self.y + 397,self.积分)
		   elseif self.状态 == "积分商城2"  then
         	zts:置颜色(tos(self.积分2))
			zts:显示(self.x + 263,self.y + 397,self.积分2)
		   elseif self.状态 == "积分商城3"  then
         	zts:置颜色(tos(self.积分3))
			zts:显示(self.x + 263,self.y + 397,self.积分3)
		   elseif self.状态 == "积分商城4"  then
         	zts:置颜色(tos(self.积分4))
			zts:显示(self.x + 263,self.y + 397,self.积分4)
		   elseif self.状态 == "积分商城5"  then
         	zts:置颜色(tos(self.积分5))
			zts:显示(self.x + 263,self.y + 397,self.积分5)
			else
			zts:置颜色(tos(self.数据.银子+0))
			zts:显示(self.x + 263,self.y + 397,self.数据.银子)
            end
			if self.srk._已碰撞 then
				self.焦点 = true
			end
  else
  	 self.zyz[31]:显示(self.x+36,self.y+80)
  	 self.zyz[33]:显示(self.x+346,self.y+80)
  	 self.zyz[27]:显示(self.x+370,self.y+382)
  	 -- self.zyz[8]:显示(self.x+443,self.y+382)
  	 for p=0,2 do
  	 	self.zyz[5]:显示(self.x+405,self.y+263+p*25)
       end
  	 self.zyz[29]:显示(self.x+315,self.y+85)
  	 self.zyz[30]:显示(self.x+315,self.y+405)
	  	 if self.选中召唤兽 ~= 0 then
		  	 zts:置颜色(tos(self.召唤兽数据[self.选中召唤兽].价格+0))
			 zts:显示(self.x+412,self.y+266,self.召唤兽数据[self.选中召唤兽].价格)
			 zts:置颜色(0xFF000000)
			 if self.召唤兽数据[self.选中召唤兽].神兽 then
			 zts:显示(self.x+412,self.y+291,"神兽")
			elseif self.召唤兽数据[self.选中召唤兽].变异 then
				zts:显示(self.x+412,self.y+291,"变异")
			elseif self.召唤兽数据[self.选中召唤兽].宝宝 then
				zts:显示(self.x+412,self.y+291,"宝宝")
			end
			 zts:置颜色(0xFFFFFFFF)
			tp.影子:显示(self.x+425,self.y+196)

		    self.zyz[34]:显示("静立",self.x+425,self.y+196)
		 end
		 zts:置颜色(0xFFFFFFFF)
		 zts:显示(self.x+350,self.y+265,"价 格")
		 zts:显示(self.x+350,self.y+290,"类 型")
		 if self.状态 == "仙玉宝宝" then
		 	 zts:显示(self.x+350,self.y+315,"剩余仙玉")
		 zts:置颜色(tos(self.召唤兽数据.仙玉))
		 zts:显示(self.x+412,self.y+316,self.召唤兽数据.仙玉)
		else
			 zts:显示(self.x+350,self.y+315,"剩余银两")
		     zts:置颜色(tos(self.召唤兽数据.银子))
		     zts:显示(self.x+412,self.y+316,self.召唤兽数据.银子)
		end
		 zts:置颜色(0xFFFF0000)
	  	 zts:显示(self.x +369,self.y +240,"右键可以查看属性")
	  	 zts:置颜色(0xFFFFFFFF)
	  	 self.zyz[12]:显示(self.x+78+45+3*92,self.y+60,true,nil,nil,self.状态==self.zyz[12]:取文字(),2)
	  	 local 序号 =0+ self.选择
		for o=1,8 do
			for i=1,2 do
				序号 =序号 +1
			if self.召唤兽数据 [序号]~=nil then
				--local jx = self.x+41
				--local jy = self.y+(i*43)+25
				local jx = self.x+41+137*(i-1)
				local jy =  self.y+(o*43)+43
				  bw:置坐标(jx,jy+1)
	            local xz = bw:检查点(鼠标.x,鼠标.y)
				if self.选中召唤兽 ~= 序号 then
					if  xz  then
						box(jx-1,jy-2,jx+134,jy+40,-3551379)
						self.焦点 = true

						if mouseb(0)   then
			                 self.选中召唤兽 = 序号
			                 self:置形象()
						elseif mouseb(1)  then
							tp.主界面.界面数据[21]:刷新(self.召唤兽数据[序号])
						end
					end
				else
					if  xz  then
						self.焦点 = true
						if mouseb(1) then
							 tp.主界面.界面数据[21]:刷新(self.召唤兽数据[序号])
						end
					end
						box(jx-1,jy-3,jx+134,jy+41,-10790181)
				end
				zts:置颜色(0xFF000000)
				zts:显示(jx+41,jy+2,self.召唤兽数据[序号].名称)
				zts:显示(jx+40,jy+22,"等级"..self.召唤兽数据[序号].等级)
				self.zyz[32]:显示(jx,jy)
				self.头像组[序号]:显示(jx+2,jy+4)
				zts:置颜色(0xFFFFFFFF)
			end
		   end
		end
  end
  self.控件类:显示(x,y)
end

function 场景类_商店:界面重叠(x,y)
	if self.zyz[1]:是否选中(鼠标.x,鼠标.y)  then
		return true
	end
end


function 场景类_商店:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end










return 场景类_商店