local 帮派信息类 = class()

function 帮派信息类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/224-1.png")
	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(黑色)

	self.文字1 = 文字类(simsun, 14)

	self.文字1:置颜色(红色)

	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.金库 = 按钮类.创建("imge/001/0016.png", 2, "金库", 3)
	self.厢房 = 按钮类.创建("imge/001/0016.png", 2, "厢房", 3)
	self.药房 = 按钮类.创建("imge/001/0015.png", 2, "聚义厅", 3)
	self.书院 = 按钮类.创建("imge/001/0016.png", 2, "书院", 3)
	self.仓库 = 按钮类.创建("imge/001/0016.png", 2, "仓库", 3)
	self.兽室 = 按钮类.创建("imge/001/0016.png", 2, "兽室", 3)
	self.设置内政 = 按钮类.创建("imge/001/0015.png", 2, "设置内政", 3)
end

function 帮派信息类:刷新(数据)
	self.本类开关 = true
	self.所需进度 = 0
	self.损耗资金 = 0
	self.损耗人气 = 0
	self.损耗繁荣 = 0
	self.选中类型 = ""
	self.数据 = 数据
end

function 帮派信息类:设置消耗(类型)
	self.选中类型 = 类型



	if 类型 == "金库" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "书院" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "聚义厅" then
		self.所需进度 = self.数据.规模 * 500 + 500
		self.损耗资金 = self.数据.规模 * 5000000 + 5000000
		self.损耗繁荣 = 500
		self.损耗人气 = 500
	else
		self.所需进度 = 200
		self.损耗资金 = 3000000
		self.损耗繁荣 = 100
		self.损耗人气 = 100
	end
end

function 帮派信息类:更新(dt)
	self.关闭:更新()
	self.金库:更新(dt)
	self.厢房:更新(527, 250)
	self.书院:更新(447, 285)
	self.仓库:更新(527, 285)
	self.药房:更新(447, 320)
	self.兽室:更新(527, 320)
	self.设置内政:更新()

	if self.关闭:取是否单击() or self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(右键) then
		self.本类开关 = false
	elseif self.金库:取是否单击() then
		self:设置消耗("金库")
	elseif self.厢房:取是否单击() then
		self:设置消耗("厢房")
	elseif self.书院:取是否单击() then
		self:设置消耗("书院")
	elseif self.仓库:取是否单击() then
		self:设置消耗("仓库")
	elseif self.药房:取是否单击() then
		self:设置消耗("聚义厅")
	elseif self.兽室:取是否单击() then
		self:设置消耗("兽室")
	elseif self.设置内政:取是否单击() then
		if self.选中类型 == "" then
			信息提示:加入提示("#y/请先选择要升级的建筑物")

			return 0
		else
			客户端:发送数据(12, 72, 13, self.选中类型)
		end
	end
end

function 帮派信息类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 帮派信息类:显示(x, y)
	self.背景:显示(220, 60)
	self.关闭:显示(603, 66)
	self.文字:显示(360, 95, self.数据.金库)
	self.文字1:显示(445, 224, "请选择要升级的建筑：")
	self.金库:显示(447, 250)
	self.厢房:显示(527, 250)
	self.书院:显示(447, 285)
	self.仓库:显示(527, 285)
	self.药房:显示(447, 320)
	self.兽室:显示(527, 320)
	self.设置内政:显示(475, 175)
	self.文字:显示(360, 117, self.数据.书院)
	self.文字:显示(360, 139, self.数据.兽室)
	self.文字:显示(360, 161, self.数据.厢房)
	self.文字:显示(360, 183, self.数据.药房)
	self.文字:显示(360, 205, self.数据.仓库)
	self.文字:显示(336, 227, self.数据.资金)
	self.文字:显示(336, 249, self.数据.金库 * 5000000 + 3000000)
	self.文字:显示(360, 271, self.数据.书院 * 2)
	self.文字:显示(360, 293, 0)
	self.文字:显示(360, 315, self.数据.守护兽等级)
	self.文字:显示(360, 337, self.数据.兽室 * 5)
	self.文字:显示(360, 359, self.数据.当前人数)
	self.文字:显示(360, 381, self.数据.人数上限)
	self.文字:显示(336, 403, 0)
	self.文字:显示(336, 425, self.数据.资材)
	self.文字:显示(336, 447, self.数据.仓库 * 100 + 100)
	self.文字:显示(557, 95, self.数据.学费指数)
	self.文字:显示(557, 117, self.数据.物价指数)
	self.文字:显示(557, 139, self.数据.修理指数)
	self.文字:显示(533, 381, self.损耗资金)
	self.文字:显示(533, 403, self.所需进度)
	self.文字:显示(533, 425, self.损耗繁荣)
	self.文字:显示(533, 447, self.损耗人气)
end

return 帮派信息类
