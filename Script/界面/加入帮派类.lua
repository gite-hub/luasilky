local 加入帮派类 = class()
local tp
function 加入帮派类:初始化(根)
	tp=根
	self.背景 = tp.资源:载入('JM.FT',"网易WDF动画",4062291183.0)
	self.本类开关 = false
	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(黑色)

	self.加入 = 按钮类.创建("imge/001/0016.png", 2, "加入", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
	self.选中背景 = 图像类("imge/001/dhs.png")
	self.超丰富文本 = require("丰富文本类")(240, 125)

	self.超丰富文本:添加元素("h", 黑色)
end

function 加入帮派类:刷新(数据)
	self.本类开关 = true
	self.选中数据 = 0
	self.帮派信息 = {}

	for n = 1, #数据, 1 do
		self.帮派信息[n] = {
			编号 = 数据[n].编号,
			帮主 = 数据[n].帮主,
			名称 = 数据[n].名称,
			宗旨 = 数据[n].宗旨,
			包围盒 = 包围盒:创建(150, 192, 64, 18)
		}

		self.帮派信息[n].包围盒:置坐标(290, 125 + 20 * n - 20)
		self.帮派信息[n].包围盒:更新宽高(220, 15)
	end
end

function 加入帮派类:更新(dt)
	self.加入:更新(300, 430)
	self.取消:更新(400, 430)

	if self.取消:取是否单击() or self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(右键) then
		self.本类开关 = false
	elseif self.加入:取是否单击() then
		if self.选中数据 == 0 then
			信息提示:加入提示("#y/请先选中一个帮派")

			return 0
		else
			客户端:发送数据(self.帮派信息[self.选中数据].编号, 64, 13, "1")
		end
	end

end

function 加入帮派类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 加入帮派类:显示(x, y)
	self.背景:显示(250, 60)
	self.加入:显示(320, 415)
	self.取消:显示(420, 415)

	for n = 1, #self.帮派信息, 1 do
		self.显示y = 125 + 20 * n - 20

		if self.选中数据 == n then
			self.选中背景:显示(280, self.显示y - 2)
		end

		self.文字:显示(290, self.显示y, self.帮派信息[n].名称)
		self.文字:显示(380, self.显示y, self.帮派信息[n].编号)
		self.文字:显示(435, self.显示y, self.帮派信息[n].帮主)

		if self.帮派信息[n].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
			self.选中数据 = n

			self.超丰富文本:清空()
			self.超丰富文本:添加文本("#h/" .. self.帮派信息[n].宗旨)
		end
	end

	self.超丰富文本:显示(276, 290)
end

return 加入帮派类
