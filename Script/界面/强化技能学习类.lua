local 强化技能学习类 = class()

function 强化技能学习类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2558-1.png")
	self.选中背景 = 图像类("imge/001/1.jpg")
	self.文字 = 文字类(simsun, 15)

	self.文字:置颜色(黑色)

	self.文字1 = 文字类(simsun, 14)
	self.金钱文字 = 文字类(simsun, 14)

	self.文字1:置颜色(黑色)

	self.允许关闭 = true
	self.按钮 = 按钮类.创建("imge/001/0016.png", 2, "学习", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
	self.强化技能名称 = {
		"伤害强化",
		"灵力强化",
		"防御强化",
		"速度强化",
		"强身术",
		"冥想"
	}
	self.超丰富文本 = require("丰富文本类")(140, 270)

	self.超丰富文本:添加元素("w", 4294967295.0)
	self.超丰富文本:添加元素("h", 4278190080.0)
	self.超丰富文本:添加元素("y", 4294967040.0)
	self.超丰富文本:添加元素("r", 4294901760.0)
	self.超丰富文本:添加元素("g", 4278255360.0)

	self.生活技能名称 = {
		"强身术",
		"冥想",
		"中药医理",
		"烹饪技巧",
		"打造技巧",
		"裁缝技巧",
		"炼金术"
	}
	self.选中编号 = 0
end

function 强化技能学习类:刷新(数据)
	self.本类数据 = 数据
	self.技能数据 = {}



	for n, v in pairs(self.本类数据) do
		if n ~= "方式" and n ~= "银两" and n ~= "经验" and n ~= "编号" and n ~= "存银" and n ~= "储备" then
			self.技能数据[#self.技能数据 + 1] = {}
			self.技能数据[#self.技能数据].等级 = self.本类数据[n]
			self.技能数据[#self.技能数据].方式 = 5
			self.技能数据[#self.技能数据].名称 = n
	          local  xsd = 引擎.取技能(n)

			self.技能数据[#self.技能数据].图标 = tp.资源:载入(xsd[6],"网易WDF动画",xsd[7])
		end
	end

	if self.本类数据.编号 ~= nil then
		self.选中编号 = self.本类数据.编号
	end

	self.本类开关 = true
end

function 强化技能学习类:更新(dt)
	self.按钮:更新(dt)
	self.取消:更新(dt)

	if self.取消:取是否单击() then
		self.本类开关 = false
	elseif self.按钮:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选择一个技能")
		else
			self.发送数据 = {
				名称 = self.技能数据[self.选中编号].名称,
				编号 = self.选中编号
			}

			客户端:发送数据(10, self.选中编号, 10, self.技能数据[self.选中编号].名称, 1)
		end
	end

	if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then self.本类开关 = false end
end

function 强化技能学习类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 强化技能学习类:显示(x, y)
	self.背景:显示(200, 50)

	for n = 1, #self.技能数据, 1 do
		if self.选中编号 == n then
			self.选中背景:显示(215, 70 + 42 * n)
		end

		self.技能数据[n].图标:显示(215, 70 + 42 * n)

		if self.技能数据[n].图标:是否选中(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
			self.选中编号 = n

			self.超丰富文本:清空()
          local  xsd = 引擎.取技能(self.技能数据[n].名称)
			self.临时文本 = xsd[1] .. "\n#g/要求等级：0"  .. "\n#r/使用条件：无"

			self.超丰富文本:添加文本("#h/" .. self.临时文本)
		end

		self.文字:显示(255, 90 + 42 * n, self.技能数据[n].名称)
		self.文字:显示(335, 90 + 42 * n, self.技能数据[n].等级 .. "/" .. 185)
	end

	self.超丰富文本:显示(413, 260)
	self.文字1:显示(284, 410, self.本类数据.经验)
	银两显示(self.金钱文字, self.本类数据.银两, 284, 434)
	银两显示(self.金钱文字, self.本类数据.存银, 284, 455)
	银两显示(self.金钱文字, self.本类数据.储备, 490, 455)

	if self.选中编号 ~= 0 then
		self.文字1:显示(490, 410, math.floor(技能消耗.经验[self.技能数据[self.选中编号].等级 + 1] * self.技能数据[self.选中编号].方式))
		self.文字1:显示(490, 434, math.floor(技能消耗.金钱[self.技能数据[self.选中编号].等级 + 1] * self.技能数据[self.选中编号].方式))
	end

	self.按钮:显示(350, 490)
	self.取消:显示(420, 490)
end

return 强化技能学习类
