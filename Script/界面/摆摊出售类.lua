local 摆摊出售类 = class()

function 摆摊出售类:初始化()
	self.背景 = 图像类("imge/001/wzife_414-1.png")
	self.道具背景 = 图像类("imge/001/wzife_415-1.png")
	self.bb背景 = 图像类("imge/001/wzife_416-1.png")
	self.道具按钮 = 按钮类.创建("imge/001/0015.png", 2, "物品类", 3)
	self.bb按钮 = 按钮类.创建("imge/001/0015.png", 2, "召唤兽类", 3)
	self.制造按钮 = 按钮类.创建("imge/001/0015.png", 2, "制造类", 3)
	self.招牌按钮 = 按钮类.创建("imge/001/0015.png", 2, "更改招牌", 3)
	self.显示类型 = 0
	self.本类开关 = false
	self.文字 = 文字类.创建(simsun, 14)

	self.文字:置颜色(黑色)

	self.bb文字 = 文字类.创建(simsun, 14)

	self.bb文字:置颜色(黑色)

	self.状态文字 = 文字类.创建(simsun, 12)
	self.物品列表 = {}
	self.bb列表 = {}
	self.选中背景 = 图像类("imge/001/125-1.png")
	self.选中背景1 = 图像类("imge/001/dhs.png")
	self.选中编号 = 0
	self.上架 = 按钮类.创建("imge/001/0016.png", 2, "上架", 3)
	self.bb选中 = 0
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "关闭", 3)
	self.收摊按钮 = 按钮类.创建("imge/001/0016.png", 2, "收摊", 3)
	self.物品数据 = {}
	self.bb数据 = {}
	self.价格 = ""
end

function 摆摊出售类:刷新(内容)
	self.本类开关 = true
	self.显示类型 = 1
	self.物品列表 = 内容.道具
	self.bb列表 = 内容.bb

	摆摊改名输入:置可视(true)
	摆摊价格输入:置可视(true)
	摆摊改名输入:置文本(内容.名称)
end

function 摆摊出售类:刷新道具状态(内容)
	self.物品列表[内容.id + 0] = {
		价格 = 内容.价格,
		id = 内容.id
	}
end

function 摆摊出售类:刷新bb状态(内容)
	self.bb列表[内容.id + 0] = {
		价格 = 内容.价格,
		id = 内容.id
	}
end

function 摆摊出售类:移除道具(内容)
	self.物品列表[内容 + 0] = nil
end

function 摆摊出售类:移除bb(内容)
	self.bb列表[内容 + 0] = nil
end

function 摆摊出售类:取重复bb(id)
	for n = 1, 10, 1 do
		if self.bb列表[n] ~= nil and self.bb列表[n].id == id then
			return n
		end
	end

	return 0
end

function 摆摊出售类:取重复物品(id)
	for n = 1, 20, 1 do
		if self.物品列表[n] ~= nil and self.物品列表[n].id == id then
			return n
		end
	end

	return 0
end

function 摆摊出售类:bb选中事件(id)
	self.操作id = self:取重复bb(id)

	if self.操作id ~= 0 then
		摆摊价格输入:置文本(self.bb列表[self.bb选中].价格)

		self.上架.文字 = "下架"
	else
		摆摊价格输入:置文本("")

		self.上架.文字 = "上架"
	end
end

function 摆摊出售类:道具选中事件(id)
	self.操作id = self:取重复物品(id)

	if self.操作id ~= 0 then
		self.价格 = self.物品列表[self.操作id].价格
		摆摊价格输入:置文本(self.物品列表[self.操作id].价格)

		self.上架.文字 = "下架"
	else
		摆摊价格输入:置文本(self.价格)

		self.上架.文字 = "上架"
	end
end

function 摆摊出售类:更新bb状态(数据)
	self.bb列表 = 数据
end

function 摆摊出售类:刷新bb(数据)
	self.显示类型 = 2
	self.bb数据 = 数据
	self.包围盒 = {}
	self.bb选中 = 0

	for n = 1, 10, 1 do
		if self.bb数据[n] ~= nil then
			self.包围盒[n] = 包围盒:创建(150, 192, 64, 18)

			self.包围盒[n]:置坐标(286, 180 + 19 * n)
			self.包围盒[n]:更新宽高(string.len(self.bb数据[n].名称) * 9, 18)
		end
	end
end

function 摆摊出售类:刷新道具(数据)

	self.显示类型 = 1
	self.物品数据 = {}

	for n = 1, 20, 1 do
		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n], 3, "摆摊", n)
			self.物品数据[n].编号 = 数据[n].编号
		end
	end
end

function 摆摊出售类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 摆摊出售类:更新(dt)
	self.道具按钮:更新(270, 162)
	self.bb按钮:更新(370, 162)
	self.制造按钮:更新(370, 162)
	self.招牌按钮:更新(370, 162)
	self.上架:更新(370, 160)
	self.关闭:更新()
	self.收摊按钮:更新(284, 438)
	self.取消按钮:更新(344, 438)

	if self.上架:取是否单击() then
		if self.显示类型 == 1 then
			if self.选中编号 == 0 then
				信息提示:加入提示("#y/请先选中一个要出售的道具")

				return 0
			else
				客户端:发送数据(self.物品数据[self.选中编号].编号, 2, 44, 摆摊价格输入:取文本(), 1)
			end
		elseif self.显示类型 == 2 then
			if self.bb选中 == 0 then
				信息提示:加入提示("#y/请先选中一只召唤兽")

				return 0
			else
				客户端:发送数据(self.bb选中, 6, 44, 摆摊价格输入:取文本(), 1)
			end
		end
	elseif self.招牌按钮:取是否单击() then
		客户端:发送数据(51, 3, 44, 摆摊改名输入:取文本(), 1)
	elseif self.道具按钮:取是否单击() then
		客户端:发送数据(42, 4, 44, "1", 1)
	elseif self.bb按钮:取是否单击() then
		客户端:发送数据(98, 5, 44, "5", 1)
	elseif self.关闭:取是否单击() or self.取消按钮:取是否单击() then
		self.本类开关 = false

		摆摊改名输入:置可视(false)
		摆摊价格输入:置可视(false)
	elseif self.收摊按钮:取是否单击() then
		客户端:发送数据(98, 12, 44, "5", 1)
	end
end

function 摆摊出售类:关闭界面()
	self.本类开关 = false

	摆摊改名输入:置可视(false)
	摆摊价格输入:置可视(false)
end

function 摆摊出售类:取召唤兽显示重叠()
	if tp.主界面.界面数据[21].本类开关 and tp.主界面.界面数据[21].背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 摆摊出售类:显示(x, y)
	-- 引擎.重置锁定(0)
	-- 引擎.重置锁定(1)
	self.背景:显示(260, 70)
	self.道具按钮:显示(280, 162)
	self.bb按钮:显示(370, 162)
	self.制造按钮:显示(460, 162)
	self.上架:显示(435, 407)
	self.招牌按钮:显示(438, 101)
	self.收摊按钮:显示(284, 438)
	self.取消按钮:显示(344, 438)
	self.文字:显示(464, 136, 窗口标题.id)
	self.文字:显示(320, 136, 窗口标题.名称)
	_GUI:显示(鼠标.x, 鼠标.y)
	self.关闭:显示(528, 76)

	if self.显示类型 == 1 then
		self.道具背景:显示(278, 188)

		self.显示id = 0

		for n = 1, 20, 1 do
			if self.物品数据[n] ~= nil then
				if n <= 5 then
					self.显示xy = {
						y = 190,
						x = 231 + n * 50
					}
				elseif n <= 10 then
					self.显示xy = {
						y = 240,
						x = 231 + (n - 5) * 50
					}
				elseif n <= 15 then
					self.显示xy = {
						y = 290,
						x = 231 + (n - 10) * 50
					}
				elseif n <= 20 then
					self.显示xy = {
						y = 340,
						x = 231 + (n - 15) * 50
					}
				end

				if self.物品数据[n]:获取焦点() then
					self.显示id = n
				end

				self.物品数据[n]:显示(self.显示xy.x, self.显示xy.y)

				if self.选中编号 == n then
					self.选中背景:显示(self.显示xy.x - 3, self.显示xy.y)
				end

				if self:取重复物品(n) ~= 0 then
					self.物品数据[n].小动画.精灵:置颜色(0xFF555555)
					local price = math.floor(self.物品列表[n].价格/10000)
					if price > 0 then
						local label = self.状态文字:置颜色(红色)
						if price >= 10000 then
							if (price % 10000) > 0 then
								label:显示(self.显示xy.x + 2, self.显示xy.y + 39, math.floor(price / 10000) .. 'e' .. (price % 10000)  .. 'w')
							else
								label:显示(self.显示xy.x + 2, self.显示xy.y + 39, math.floor(price / 10000) .. 'e')
							end
						else
							label:显示(self.显示xy.x + 10, self.显示xy.y + 38, price ..'w')
						end
					end

				else
					-- self.状态文字:置颜色(绿色):显示(self.显示xy.x + 10, self.显示xy.y + 38, "待售中")
					self.物品数据[n].小动画.精灵:置颜色(0xFFFFFFFF)
					do end
				end
			end
		end

		if self.显示id ~= 0 then
			self.物品数据[self.显示id]:显示事件()

			if 引擎.鼠标弹起(左键) then
				self.选中编号 = self.显示id

				self:道具选中事件(self.显示id)
			end
		end
	elseif self.显示类型 == 2 then
		self.bb背景:显示(278, 188)

		for n = 1, 10, 1 do

			if self.bb数据[n] ~= nil then
				if self.bb选中 == n then
					self.选中背景1:显示(284, 178 + 19 * n)
				end

				if self:取重复bb(n) ~= 0 then
					self.bb文字:置颜色(绿色):显示(286, 180 + 19 * n, self.bb数据[n].名称)
				else
					self.bb文字:置颜色(黑色):显示(286, 180 + 19 * n, self.bb数据[n].名称)
				end

				self.包围盒[n]:显示()

				if self.包围盒[n]:检查点(鼠标.x, 鼠标.y) then
					box = true
					if 引擎.鼠标弹起(左键) and self:取召唤兽显示重叠() == false then
						self.bb选中 = n

						self:bb选中事件(n)
					elseif 引擎.鼠标弹起(右键) and self:取召唤兽显示重叠() == false then
						tp.主界面.界面数据[21]:刷新(self.bb数据[n])
					end
				end
			end
		end
	end
	if 引擎.鼠标弹起(右键) and self:界面重叠() then
		local xx, yy = 鼠标.x - self.背景.x - 20, 鼠标.y - self.背景.y - 120
		if(xx < 0 or yy < 0 or yy > 210 or (xx > (self.显示类型 == 1 and 260 or 235))) then
			self.本类开关 = false
		end
	end
end

return 摆摊出售类
