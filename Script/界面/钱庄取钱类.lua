local 钱庄取钱类 = class()

function 钱庄取钱类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/426-1.png")
	self.金钱文字 = 文字类.创建(simsun, 14)
	self.存入 = 按钮类.创建("imge/001/0016.png", 2, "取出", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
end

function 钱庄取钱类:刷新(数据)
	self.本类开关 = true
	self.临时数据 = 分割文本(数据, "*-*")
	self.银子 = self.临时数据[1]
	self.存银 = self.临时数据[2]

	钱庄取钱输入:置可视(true, "钱庄取钱输入")
	钱庄取钱输入:置文本("0")
end

function 钱庄取钱类:更新(dt)
	if self.背景:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(键盘符号.右键) then
		self.本类开关 = false

		钱庄存钱输入:置可视(false, "钱庄存钱输入")
	end

	self.存入:更新(347, 312)
	self.取消:更新(407, 312)

	if self.存入:取是否单击() then
		self.临时文本 = 钱庄取钱输入:取文本()

		if self.临时文本 == "" then
			信息提示:加入提示("#y/请输入要取出的数量")

			return 0
		else
			self.临时文本 = self.临时文本 + 0

			if self.临时文本 <= 0 then
				信息提示:加入提示("#y/请重新输入要取出的数量")

				return 0
			else
				客户端:发送数据(self.临时文本, 49, 13, 取随机数(1, 99999) .. "", 1)
			end
		end
	elseif self.取消:取是否单击() then
		self.本类开关 = false

		钱庄取钱输入:置可视(false, "钱庄存钱输入")
	end
end

function 钱庄取钱类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 钱庄取钱类:显示(x, y)
	self.背景:显示(303, 168)
	银两显示(self.金钱文字, self.银子, 400, 214)
	银两显示(self.金钱文字, self.存银, 400, 246)
	self.存入:显示(347, 312)
	self.取消:显示(407, 312)
	_GUI:显示(鼠标.x, 鼠标.y)
end

return 钱庄取钱类
