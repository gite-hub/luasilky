local 游戏公告类 = class()

function 游戏公告类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/0047.png")
	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(黄色)

	self.当前序号 = 1
	self.显示数据 = {}
	self.增加数值 = 0
	self.初始xy = {
		x = 850,
		y = 82
	}
	self.显示xy = {
		x = 850,
		y = 82
	}
	self.超级文本 = require("丰富文本类")(9999999999.0, 27999990)
	self.超级文本:添加元素("w", 4294967295.0)
	self.超级文本:添加元素("h", 黄色)
	self.超级文本:添加元素("y", 4294967040.0)
	self.超级文本:添加元素("r", 4294901760.0)
	self.超级文本:添加元素("g", 4278255360.0)

	self.超级文本.禁止换行 = true
	self.超级文本.换行符 = "@"
end
function 游戏公告类:添加公告内容(内容, 退出)
	self.本类开关 = true
	self.显示数据[#self.显示数据 + 1] = {
		内容 = 内容,
		长度 = string.len(内容),
		参数 = 退出
	}
end

function 游戏公告类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end
function 游戏公告类:更新(dt)
	self.显示xy.x = self.显示xy.x - 1
	self.增加数值 = self.增加数值 + 1
	if self.显示数据[self.当前序号] == nil then
		self:结束处理()
		return 0
	end
	if self.显示xy.x + self.显示数据[self.当前序号].长度 * 7 <= 0 then
		self:结束处理()
	end
end

function 游戏公告类:结束处理()
	self.显示xy.x = 850
	self.增加数值 = 0
	if self.当前序号 >= #self.显示数据 then
		self.本类开关 = false
		self.显示数据 = {}
	else
		self.当前序号 = self.当前序号 + 1
	end
end
function 游戏公告类:显示(x, y)
	信息提示:显示(2, 110, 1, -10, 77)
	self.文字:显示(self.显示xy.x, self.显示xy.y, self.显示数据[self.当前序号].内容)
end

return 游戏公告类
