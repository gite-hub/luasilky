local 玩家交易类 = class()

function 玩家交易类:初始化()
	self.锁定 = 按钮类.创建("imge/001/0016.png", 2, "锁定", 3)
	self.提交 = 按钮类.创建("imge/001/0016.png", 2, "交易", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
	self.道具 = 按钮类.创建("imge/001/0015.png", 2, "道具", 3)
	self.召唤兽 = 按钮类.创建("imge/001/0015.png", 2, "召唤兽", 3)
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2391-1.png")
	self.说明字体 = 文字类.创建(simsun, 14)
	self.召唤兽字体 = 文字类.创建(simsun, 14)

	self.召唤兽字体:置颜色(黑色)
end

function 玩家交易类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 玩家交易类:刷新()
	self.本类开关 = true
	self.背景 = 图像类("imge/001/2391-1.png")
	self.物品数据 = {}
	self.召唤兽数据 = {}
	self.格子 = {
		0,
		0,
		0
	}
	self.召唤兽格子 = {
		0,
		0,
		0
	}
	self.交易类型 = "物品"
	self.交易名称 = ""
	self.交易等级 = ""
	self.锁定状态 = false
	self.交易数据 = nil
end

function 玩家交易类:刷新道具(数据)
	self.背景 = 图像类("imge/001/2391-1.png")

	交易银两输入:置可视(true, "交易银两输入")
	交易银两输入:置文本("")

	self.交易类型 = "物品"
	self.道具数据 = 数据

	if self.交易名称 == "" then
		self.交易名称 = self.道具数据.名称
		self.交易等级 = self.道具数据.等级
		self.交易id = self.道具数据.id
	end

	self.物品数据 = {}

	for n = 1, 20, 1 do
		if self.道具数据.道具[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(self.道具数据.道具[n], 3, "包裹", n)
			self.物品数据[n].编号 = self.道具数据.道具[n].编号
		end
	end
end

function 玩家交易类:刷新召唤兽(数据)
	self.背景 = 图像类("imge/001/2390-1.png")
	self.交易类型 = "召唤兽"
	self.召唤兽数据 = {
		数据 = 数据
	}
	self.包围盒 = {}

	for n = 1, #self.召唤兽数据.数据, 1 do
		self.包围盒[n] = 包围盒:创建(150, 192, 64, 18)

		self.包围盒[n]:置坐标(412, 135 + n * 20)
		self.包围盒[n]:更新宽高(string.len(self.召唤兽数据.数据[n].名称) * 9, 18)
	end
end

function 玩家交易类:刷新交易数据(数据)

	self.交易数据 = 数据
	self.交易包围盒 = {}

	for n = 1, #self.交易数据.召唤兽, 1 do
		self.交易包围盒[n] = 包围盒:创建(150, 192, 64, 18)

		self.交易包围盒[n]:置坐标(193, 375 + n * 25 - 25)
		self.交易包围盒[n]:更新宽高(string.len(self.交易数据.召唤兽[n].名称) * 9, 18)
	end

	self.交易物品数据 = {}

	for n = 1, #self.交易数据.道具, 1 do
		self.交易物品数据[#self.交易物品数据 + 1] = 物品数据类.创建(self.交易数据.道具[n], 3, "包裹", n)
	end
end

function 玩家交易类:更新(dt)
	if self.交易类型 == "物品" then
		self:物品更新(dt)
	end

	self.锁定:更新(dt)
	self.提交:更新(dt)
	self.取消:更新(dt)
	self.道具:更新(dt)
	self.召唤兽:更新(dt)

	if self.道具:取是否单击() then
		客户端:发送数据(self.交易id, 14, 31, "P7", 1)
	elseif self.召唤兽:取是否单击() then
		客户端:发送数据(self.交易id, 6, 32, "P7", 1)
	elseif self.锁定:取是否单击() then
		self:锁定交易()
	elseif self.提交:取是否单击() then
		客户端:发送数据(self.交易id, 6, 34, "P7", 1)
	elseif self.取消:取是否单击() then
		客户端:发送数据(self.交易id, 6, 35, "P7", 1)
	end

	_GUI:更新(dt, 鼠标.x, 鼠标.y)
end

function 玩家交易类:锁定交易()
	if 交易银两输入:取文本() == "" then
		self.临时银两 = 0
	else
		self.临时银两 = 交易银两输入:取文本() + 0
	end

	self.发送信息 = {
		id = self.交易id,
		银两 = self.临时银两,
		道具 = self.格子,
		召唤兽 = self.召唤兽格子
	}
	self.文本信息 = ""

	for n = 1, #self.格子, 1 do
		self.文本信息 = self.文本信息 .. self.格子[n] .. "*-*"
	end

	self.文本信息 = self.文本信息 .. "@-@"

	for n = 1, #self.召唤兽格子, 1 do
		self.文本信息 = self.文本信息 .. self.召唤兽格子[n] .. "*-*"
	end

	self.文本信息 = self.文本信息 .. "@-@"

	客户端:发送数据(self.交易id, self.临时银两, 33, self.文本信息, 7)

	self.锁定状态 = true
end

function 玩家交易类:物品更新(dt)
end

function 玩家交易类:召唤兽显示()
	self.显示序列 = 0

	for n = 1, #self.召唤兽数据.数据, 1 do
		if n ~= self.召唤兽格子[1] and n ~= self.召唤兽格子[2] and n ~= self.召唤兽格子[3] then
			self.显示序列 = self.显示序列 + 1

			self.召唤兽字体:显示(412, 135 + self.显示序列 * 20, self.召唤兽数据.数据[n].名称)
			self.包围盒[n]:置坐标(412, 135 + self.显示序列 * 20)
			self.包围盒[n]:显示()

			if self.包围盒[n]:检查点(鼠标.x, 鼠标.y) then
				if 引擎.鼠标弹起(右键) then
					tp.主界面.界面数据[21]:刷新(self.召唤兽数据.数据[n])
				elseif 引擎.鼠标弹起(左键) and self.锁定状态 == false then
					self:增加召唤兽格子(n)
				end
			end
		end
	end

	self.显示序列 = 0

	for n = 1, 3, 1 do
		if self.召唤兽格子[n] ~= 0 then
			self.显示序列 = self.显示序列 + 1

			self.召唤兽字体:显示(194, 155 + self.显示序列 * 25 - 25, self.召唤兽数据.数据[self.召唤兽格子[n]].名称)
			self.包围盒[self.召唤兽格子[n]]:置坐标(194, 155 + self.显示序列 * 25 - 25)
			self.包围盒[self.召唤兽格子[n]]:显示()

			if self.包围盒[self.召唤兽格子[n]]:检查点(鼠标.x, 鼠标.y) then
				if 引擎.鼠标弹起(右键) then
					tp.主界面.界面数据[21]:刷新(self.召唤兽数据.数据[self.召唤兽格子[n]])
				elseif 引擎.鼠标弹起(左键) and self.锁定状态 == false then
					self.召唤兽格子[n] = 0
				end
			end
		end
	end

	if self.交易数据 ~= nil then
		for n = 1, #self.交易数据.召唤兽, 1 do
			self.召唤兽字体:显示(193, 375 + n * 25 - 25, self.交易数据.召唤兽[n].名称)

			if self.交易包围盒[n]:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(右键) then
				tp.主界面.界面数据[21]:刷新(self.交易数据.召唤兽[n])
			end
		end
	end
end

function 玩家交易类:物品显示()
	self.起始xy = {
		x = 370,
		y = 180
	}
	self.焦点格子 = 0

	for n = 1, 20, 1 do
		self.叠加xy = {
			x = 0,
			y = 0
		}

		if n <= 5 then
			self.叠加xy = {
				y = 0,
				x = n * 50 - 50
			}
		else
			self.叠加高度 = math.floor(n / 5)
			self.叠加宽度 = n - math.floor(self.叠加高度 * 5)
			self.叠加宽度 = self.叠加宽度 + 1
			self.叠加xy = {
				x = self.叠加宽度 * 50 - 50,
				y = self.叠加高度 * 50
			}
		end

		if self.物品数据[n] ~= nil and n ~= self.格子[1] and n ~= self.格子[2] and n ~= self.格子[3] then
			self.物品数据[n]:显示(self.起始xy.x + self.叠加xy.x, self.起始xy.y + self.叠加xy.y, 2)

			if self.物品数据[n]:获取焦点() then
				self.焦点格子 = n
			end
		end
	end

	if self.焦点格子 ~= 0 then
		self.物品数据[self.焦点格子]:显示事件(2)

		if 引擎.鼠标弹起(左键) and self.锁定状态 == false then
			self:增加格子(self.焦点格子)
		end
	end

	for n = 1, 3, 1 do
		if self.格子[n] ~= 0 then
			self.物品数据[self.格子[n]]:显示(105 + n * 58, 149, 2)

			if self.物品数据[self.格子[n]]:获取焦点() then
				self.物品数据[self.格子[n]]:显示事件(2)

				if 引擎.鼠标弹起(右键) and self.锁定状态 == false then
					self.格子[n] = 0
				end
			end
		end
	end

	if self.交易数据 ~= nil then
		for n = 1, #self.交易物品数据, 1 do
			self.交易物品数据[n]:显示(108 + n * 56, 367, 2)

			if self.交易物品数据[n]:获取焦点() then
				self.交易物品数据[n]:显示事件(2)
			end
		end
	end
end

function 玩家交易类:增加格子(编号)
	self.赋值 = false

	for n = 1, 3, 1 do
		if self.格子[n] == 0 and self.赋值 == false then
			self.格子[n] = 编号
			self.赋值 = true
		end
	end
end

function 玩家交易类:增加召唤兽格子(编号)
	self.赋值 = false

	for n = 1, 3, 1 do
		if self.召唤兽格子[n] == 0 and self.赋值 == false then
			self.召唤兽格子[n] = 编号
			self.赋值 = true
		end
	end
end

function 玩家交易类:显示(x, y)
	self.背景:显示(150, 70)
	self.锁定:显示(405, 425)
	self.提交:显示(485, 425)
	self.取消:显示(565, 425)
	self.道具:显示(408, 108)
	self.召唤兽:显示(528, 108)
	self.说明字体:置颜色(黄色):显示(238, 255, self.交易名称)
	self.说明字体:置颜色(黄色):显示(208, 295, self.交易等级)
	self.说明字体:置颜色(黄色):显示(289, 295, "陌生人")

	if self.交易数据 ~= nil then
		self.说明字体:置颜色(黑色):显示(277, 338, self.交易数据.银两)
	end

	_GUI:显示(鼠标.x, 鼠标.y)

	if self.交易类型 == "物品" then
		self:物品显示()
	elseif self.交易类型 == "召唤兽" then
		self:召唤兽显示()
	end
end

return 玩家交易类
