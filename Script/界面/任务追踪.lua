--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-03-13 21:05:38
--======================================================================--
local 场景类_任务追踪栏 = class()

local floor = math.floor
local format = string.format
local tp
local xxx = 0
local yyy = 0
local box = 引擎.画矩形
local min = math.min
local max = math.max
local ceil = math.ceil
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起

function 场景类_任务追踪栏:初始化(根)
	self.x = 600
	self.y = 88
	self.xx = 0
	self.yy = 0
	self.焦点 = false
	self.移动窗口 = false
	self.可移动窗口 = false
	self.数据={}
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	tp = 根
	self.资源组 = {
		[1] = 资源:载入("Dat/pic/追踪.png","图片"),
		[2] = 按钮(资源:载入('JM.FT',"网易WDF动画",0xD465F50F),0,0,4),
		[3] = 按钮(资源:载入('JM.FT',"网易WDF动画",0x44F2D2D3),0,0,4),
		[4] = 按钮(资源:载入('JM.FT',"网易WDF动画",0x77608510),0,0,4),
		[5] = 按钮(资源:载入('JM.FT',"网易WDF动画",0x335CECBC),0,0,4),
		[6] = tp._滑块(资源:载入('JM.FT',"网易WDF动画",0x7F027E7B),4,10,139,2),
		[7] = 按钮(资源:载入('JM.FT',"网易WDF动画",0xB7F2FF5E),0,0,4),
		[8] = 按钮(资源:载入('JM.FT',"网易WDF动画",0x36DDB607),0,0,4),
	}
	self.状态 = 2
	self.介绍加入 = 0
	self.窗口时间 = 0
	self.介绍文本 =根._丰富文本(180,170)
	:添加元素("L",-16776961)
end

function 场景类_任务追踪栏:加载(数据)
	self.介绍文本:清空()
	self.数据 = 数据
	for i=1,#self.数据 do
		if self.数据[i].名称~="双倍经验" and self.数据[i].名称~="摄妖香" and self.数据[i].名称~="回天丹" then

		    self.介绍文本:添加文本(format("#G/%s\n◆%s",self.数据[i].名称,self.数据[i].说明))
		    if i < #self.数据 then
			    self.介绍文本:添加文本("")
			end

		end
	end
	for i=1,#self.介绍文本.显示表 - 10 do
		self.介绍文本:滚动(1)
	end
	if #self.介绍文本.显示表 > 10 and self.介绍加入 ~= 0 then
		self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入+1,10,#self.介绍文本.显示表))
		self.介绍加入 = min(ceil((#self.介绍文本.显示表-10)*self.资源组[6]:取百分比()),#self.介绍文本.显示表-10)
		self.介绍文本.加入 = self.介绍加入
	end
end

function 场景类_任务追踪栏:显示(dt,x,y)
	if not tp.任务追踪 then
		return
	end
	self.焦点 = false
	self.资源组[self.状态]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	if self.资源组[self.状态]:事件判断(x,y) then
		if self.状态 == 2 then
			self.状态 = 8
			self.可移动窗口 = true
		else
		    self.状态 = 2
		    self.可移动窗口 = false
		end
	elseif self.资源组[4]:事件判断(x,y) then
		tp.任务追踪 = false
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[self.状态]:显示(self.x+127,self.y+4)
	self.资源组[3]:显示(self.x+148,self.y+4)
	self.资源组[4]:显示(self.x+170,self.y+4)
	if #self.介绍文本.显示表 > 10 then
		self.资源组[5]:更新(x,y,self.介绍加入 > 0)
		self.资源组[7]:更新(x,y,self.介绍加入 < #self.介绍文本.显示表 - 10)
		if self.资源组[5]:事件判断() then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入-1,10,#self.介绍文本.显示表))
		elseif self.资源组[7]:事件判断() then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入+1,10,#self.介绍文本.显示表))
		end
		self.资源组[5]:显示(self.x+185,self.y+31)
		box(self.x+185,self.y+44,self.x+195,self.y+183,ARGB(160,30,30,30))
		self.资源组[6]:显示(self.x+185,self.y+44,x,y,true)
		self.资源组[7]:显示(self.x+185,self.y+183)
		self.介绍加入 = min(ceil((#self.介绍文本.显示表-10)*self.资源组[6]:取百分比()),#self.介绍文本.显示表-10)
		self.介绍文本.加入 = self.介绍加入
	end
	self.介绍文本:显示(self.x+10,self.y+34,4294967295)
	if self.资源组[6].接触 or tp.按钮焦点 then
		self.焦点 = true
	end
	if self.可移动窗口 and self:检查点(x,y) then
		if mousea(0) then
			self:初始移动(x,y)
		end
		if self.移动窗口 and not tp.隐藏UI and not tp.消息栏焦点 then
			self:开始移动(x,y)
		end
	end
	if self.移动窗口 and (mouseb(0) or tp.隐藏UI or tp.消息栏焦点) then
		self.移动窗口 = false
	end
	if self.资源组[1]:是否选中(x,y) and #self.介绍文本.显示表 > 10 then
		local scroll = -引擎.取鼠标滚轮()
		if scroll < 0 and self.介绍加入 > 0 then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入-1,10,#self.介绍文本.显示表))
		elseif scroll > 0 and self.介绍加入 < #self.介绍文本.显示表 - 10 then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入+1,10,#self.介绍文本.显示表))
		end
		self.介绍加入 = min(ceil((#self.介绍文本.显示表-10)*self.资源组[6]:取百分比()),#self.介绍文本.显示表-10)
		self.介绍文本.加入 = self.介绍加入
	end
end



function 场景类_任务追踪栏:检查点(x,y)
	return tp.任务追踪 and self.可移动窗口 and self.资源组[1]:是否选中(x,y) or self.焦点
	-- if 游戏进程==2  and tp.任务追踪 and ((self.可移动窗口 and self.资源组[1]:是否选中(x,y)) or self.焦点)  then
	-- 	return true
	-- end
end

function 场景类_任务追踪栏:检查按钮点(x,y)
	if not tp.任务追踪 then return false end
	x = x - self.x
	y = y - self.y
	return (self.移动窗口 or x > 120) and (y > 0 and y < 24)
end


function 场景类_任务追踪栏:初始移动(x,y)
	if tp.消息栏焦点 then
  		return
 	end
	if not self.焦点 then
		self.移动窗口 = true
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_任务追踪栏:开始移动(x,y)
	self.x = x - self.xx
	self.y = y - self.yy
end

return 场景类_任务追踪栏