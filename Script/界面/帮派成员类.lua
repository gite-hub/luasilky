local 帮派成员类 = class()

function 帮派成员类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2059-1.png")
	self.标题文字 = 文字类(simsun, 15)

	self.标题文字:置颜色(白色)

	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(蓝色)

	self.增加 = 按钮类.创建("imge/001/下移.png", 1, 1, 3)
	self.减少 = 按钮类.创建("imge/001/上移.png", 1, 1, 3)
	self.允许加入 = 按钮类.创建("imge/001/0015.png", 2, "允许加入", 3)
	self.拒绝申请 = 按钮类.创建("imge/001/0015.png", 2, "拒绝申请", 3)
	self.清空列表 = 按钮类.创建("imge/001/0015.png", 2, "清空列表", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
end

function 帮派成员类:刷新(数据)
	self.本类开关 = true
	self.显示数据 = {}
	self.显示序列 = 1
	self.选中编号 = 0
	self.显示数据 = {
		数据.名单[数据.id]
	}

	for n, v in pairs(数据.名单) do
		if n ~= 数据.id then
			self.显示数据[#self.显示数据 + 1] = 数据.名单[n]
		end
	end

	for n = 1, #self.显示数据, 1 do
		if n <= 9 then
			self.显示位置 = n
		else
			self.显示位置 = n - math.floor(n / 10) * 10
		end

		self.显示数据[n].包围盒 = 包围盒:创建(150, 192, 64, 18)

		self.显示数据[n].包围盒:置坐标(170, 140 + self.显示位置 * 23)
		self.显示数据[n].包围盒:更新宽高(400, 15)
	end
end

function 帮派成员类:更新(dt)
	self.减少:更新()
	self.增加:更新()
	self.允许加入:更新(185, 400)
	self.拒绝申请:更新(285, 400)
	self.清空列表:更新(385, 400)
	self.取消:更新(485, 400)

	if self.减少:取是否单击() then
		if self.显示序列 <= 1 then
			-- Nothing
		else
			self.显示序列 = self.显示序列 - 1
		end
	elseif self.增加:取是否单击() then
		if self.显示序列 * 9 > #self.显示数据 then
			-- Nothing
		else
			self.显示序列 = self.显示序列 + 1
		end
	elseif self.清空列表:取是否单击() then
		客户端:发送数据(1, 66, 13, "66")
	elseif self.拒绝申请:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中一个玩家")

			return 0
		end

		客户端:发送数据(self.显示数据[self.选中编号].id, 67, 13, "16")
	elseif self.允许加入:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中一个玩家")

			return 0
		end

		客户端:发送数据(self.显示数据[self.选中编号].id, 68, 13, "16")
	elseif self.取消:取是否单击() then
		self.本类开关 = false
	end
end

function 帮派成员类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 帮派成员类:显示(x, y)
	self.背景:显示(150, 100)
	self.标题文字:显示(347, 104, "帮派成员名单信息")
	self.减少:显示(642, 150)
	self.增加:显示(642, 345)
	self.取消:显示(335, 380)
	self.文字:置颜色(黑色)
	self.文字:显示(220, 140, "离线时间")
	self.文字:显示(360, 140, "名称")
	self.文字:显示(450, 140, "职务")
	self.文字:显示(520, 140, "帮贡")

	for n = 1, 9, 1 do
		self.显示编号 = (self.显示序列 - 1) * 9 + n

		if self.显示数据[self.显示编号] ~= nil then
			if self.选中编号 == self.显示编号 then
				self.文字:置颜色(绿色)
			elseif self.显示编号 == 1 then
				self.文字:置颜色(黄色)
			else
				self.文字:置颜色(蓝色)
			end

			self.文字:显示(170, 140 + n * 23, 测试时间(self.显示数据[self.显示编号].离线时间))
			self.文字:显示(360, 140 + n * 23, self.显示数据[self.显示编号].名称)
			self.文字:显示(450, 140 + n * 23, self.显示数据[self.显示编号].职务)
			self.文字:显示(520, 140 + n * 23, self.显示数据[self.显示编号].帮贡.当前 .. "/" .. self.显示数据[self.显示编号].帮贡.获得)

			if self.显示数据[self.显示编号].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
				if self.选中编号 == self.显示编号 then
					self.选中编号 = 0
				else
					self.选中编号 = self.显示编号
				end
			end
		end
	end
end

return 帮派成员类
