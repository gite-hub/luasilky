local 宝箱解密类 = class()

function 宝箱解密类:初始化()
	self.保留数据 = false
	self.游戏开关 = false
	self.字体 = 文字类.创建(simsun, 14)
	self.提示字体 = 文字类.创建(simsun, 14)

	self.提示字体:置颜色(黄色)

	self.提示内容 = {}
	self.开始游戏 = 按钮类.创建("imge/001/0015.png", 2, "开始游戏", 3)
	self.数字按钮 = {}
	self.本类开关 = false
	self.测试参数 = {}
	self.游戏开关 = false
	self.背景 = 图像类("imge/001/479-1.png")
	self.超级文本 = require("丰富文本类")(380, 350):置文字(self.字体)

	self.超级文本:添加元素("G", 4278255360.0)
	self.超级文本:添加元素("H", 黄色)
	self.超级文本:添加元素("R", 4294901760.0)
	self.超级文本:添加元素("W", 4294967295.0)
	self.超级文本:添加元素("Y", 4294967040.0)
	self.超级文本:添加元素("U", 黑色)

	self.介绍文本 = " #U/这是一个考验智商的小游戏。游戏开始时，会随机生成四个数字。玩家有10次操作机会，每次可以任意搭配4个数字。点击选定数字后，\n #U/会验证是否与生成的数字相同。如果数字不对或顺序不对，则会给出对应的提示（该数字是四个数字里的其中一个为正确，如果该数字的位置也相同则为完美。一个数字如果出现完美也会视为正确）。每次失败后所得奖励都会降低10%。"

	self.超级文本:添加文本(self.介绍文本)

	self.退出游戏 = 按钮类.创建("imge/001/0015.png", 2, "退出游戏", 3)
	self.重置数字 = 按钮类.创建("imge/001/0015.png", 2, "重置数字", 3)
	self.选定数字 = 按钮类.创建("imge/001/0015.png", 2, "选定数字", 3)

	for n = 1, 10, 1 do
		self.数字按钮[n] = 按钮类.创建("imge/001/0046.png", 3, n - 1, 3, 0, {
			红色,
			黄色,
			绿色
		}, 14)
	end
end

function 宝箱解密类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 宝箱解密类:刷新()
	self.选定参数 = {}
	self.提示内容 = {}
	self.游戏开关 = true
	self.操作次数 = 1
	self.奖励参数 = 1.5
	self.奖励类型 = 取随机数(1, 2)
	self.保留数据 = false
end

function 宝箱解密类:选定处理(编号)
	if #self.选定参数 < 4 then
		self.数字重复 = false

		for n = 1, #self.选定参数, 1 do
			if self.选定参数[n] == 编号 - 1 then
				self.数字重复 = true
			end
		end

		if self.数字重复 == false then
			self.选定参数[#self.选定参数 + 1] = 编号 - 1
		else
			信息提示:加入提示("#y/每个数字只能选择一次")
		end
	else
		信息提示:加入提示("#y/你已经选好了4个数字")
	end
end

function 宝箱解密类:更新(dt)
	for n = 1, 10, 1 do
		self.数字按钮[n]:更新(160 + n * 35, 190)

		if self.游戏开关 and self.保留数据 == false and self.数字按钮[n]:取是否单击() then
			self:选定处理(n)
		end
	end

	self.开始游戏:更新()
	self.退出游戏:更新()
	self.选定数字:更新()
	self.重置数字:更新()

	if self.开始游戏:取是否单击() then
		客户端:发送数据(3, 1, 41, "7", 1)
	end

	if self.重置数字:取是否单击() and self.保留数据 == false and self.游戏开关 then
		self.选定参数 = {}
	end

	if self.选定数字:取是否单击() and self.保留数据 == false and self.游戏开关 then
		if #self.选定参数 < 4 then
			信息提示:加入提示("#y/请先选好4个数字，点击上方的数字按钮即可")

			return 0
		else
			self.发送信息 = ""

			for n = 1, 4, 1 do
				self.发送信息 = self.发送信息 .. self.选定参数[n] .. "*-*"
			end

			客户端:发送数据(3, 42, 41, self.发送信息, 1)
		end
	end

	if self.退出游戏:取是否单击() then
		self.本类开关 = false
	end
end

function 宝箱解密类:选定结果(内容)

	self.提示内容[#self.提示内容 + 1] = 内容
	self.选定参数 = {}
end

function 宝箱解密类:显示(x, y)
	self.背景:显示(150, 150)
	self.提示字体:置颜色(白色):显示(340, 152, "数字解谜")

	for n = 1, 10, 1 do
		self.数字按钮[n]:显示(160 + n * 35, 190)
	end

	self.开始游戏:显示(180, 410)
	self.重置数字:显示(380, 410)
	self.选定数字:显示(280, 410)
	self.退出游戏:显示(480, 410)

	if self.游戏开关 == false and self.保留数据 == false then
		self.超级文本:显示(175, 260)
	end

	if self.游戏开关 then
		self.显示文本 = ""

		for n = 1, #self.选定参数, 1 do
			self.显示文本 = self.显示文本 .. self.选定参数[n]
		end

		if self.保留数据 then
			self.提示字体:置颜色(黄色):显示(180, 220, "正确密码：" .. self.测试参数[1] .. self.测试参数[2] .. self.测试参数[3] .. self.测试参数[4])
		else
			self.提示字体:置颜色(蓝色):显示(180, 220, "已选中数字：" .. self.显示文本)
		end

		for n = 1, #self.提示内容, 1 do
			self.递减参数 = 0

			if n <= 5 then
				self.y = 220
				self.x = 170
				self.递减参数 = n
			else
				self.y = 220
				self.x = 370
				self.递减参数 = n - 5
			end

			self.提示字体:置颜色(黑色):显示(self.x, self.y + 30 * self.递减参数, self.提示内容[n])
		end
	end
end

return 宝箱解密类
