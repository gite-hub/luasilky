
local 幻化属性 = class()
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
local 灵饰属性 = {"法术伤害","物理暴击等级","穿刺等级","狂暴等级","法术暴击等级","法术伤害结果","封印命中等级","治疗能力","抗物理暴击等级","格挡值","抗法术暴击等级","抵抗封印等级","气血回复效果"}
function 幻化属性:初始化(根)
	self.ID = 64
	self.x = 218
	self.y = 35
	self.xx = 0
	self.yy = 0
	self.注释 = "幻化属性"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	self.资源组 = {
		[1] = 资源:载入("Dat/Pic/Lings.png","图片")

	 }

	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts1:置颜色(0xFFFFFFFF)

end

function 幻化属性:打开(等级,幻化属性,躲避)
	if self.可视 then
		self.可视 = false
	else
        self.等级=等级
        self.幻化属性=幻化属性
        self.躲避 = 躲避
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 幻化属性:计算显示(等级,数值,暴击)
    if 暴击 ==nil then
    	暴击=0
    else
    	暴击=5.00
    end
	return   math.floor(数值 *1000/等级)/100+暴击
end

function 幻化属性:显示(dt,x,y)
 self.焦点 = false
self.资源组[1]:显示(self.x,self.y)
 zts:置颜色(0xFFFFFFFF)

 zts:显示(self.x+160,self.y+65,self.幻化属性[灵饰属性[1]])
 zts:显示(self.x+160,self.y+90,self.幻化属性[灵饰属性[2]].."(+"..self:计算显示(self.等级,self.幻化属性[灵饰属性[2]],5).."%)")
 for i=3,5 do
 	zts:显示(self.x+160,self.y+40+i*25,self.幻化属性[灵饰属性[i]].."(+"..self:计算显示(self.等级,self.幻化属性[灵饰属性[i]]).."%)")
 end
 zts:显示(self.x+160,self.y+40+6*25,self.幻化属性[灵饰属性[6]])
 zts:显示(self.x+160,self.y+40+7*25,self.幻化属性[灵饰属性[7]].."(+"..self:计算显示(self.等级,self.幻化属性[灵饰属性[7]]).."%)")
 zts:显示(self.x+160,self.y+40+8*25,self.幻化属性[灵饰属性[8]])
 zts:显示(self.x+160,self.y+82+9*25,self.幻化属性[灵饰属性[9]].."(+"..self:计算显示(self.等级,self.幻化属性[灵饰属性[9]]).."%)")
 zts:显示(self.x+160,self.y+82+10*25,self.幻化属性[灵饰属性[10]])

 for i=11,12 do
 	zts:显示(self.x+160,self.y+82+i*25,self.幻化属性[灵饰属性[i]].."(+"..self:计算显示(self.等级,self.幻化属性[灵饰属性[i]]).."%)")
 end
zts:显示(self.x+160,self.y+82+13*25,self.幻化属性[灵饰属性[13]])
zts:显示(self.x+160,self.y+432,self.躲避)
end
function 幻化属性:检查点(x,y)
	if self.资源组[1]:是否选中(x,y) then
		return true
	end
end

function 幻化属性:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 幻化属性:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 幻化属性