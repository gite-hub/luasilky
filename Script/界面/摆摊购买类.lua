local 摆摊购买类 = class()

function 摆摊购买类:初始化()
	self.背景 = 图像类("imge/001/wzife_359-1.png")
	self.道具背景 = 图像类("imge/001/wzife_415-1.png")
	self.bb背景 = 图像类("imge/001/wzife_416-1.png")
	self.道具按钮 = 按钮类.创建("imge/001/0015.png", 2, "物品类", 3)
	self.bb按钮 = 按钮类.创建("imge/001/0015.png", 2, "召唤兽类", 3)
	self.制造按钮 = 按钮类.创建("imge/001/0015.png", 2, "制造类", 3)
	self.招牌按钮 = 按钮类.创建("imge/001/0015.png", 2, "更改招牌", 3)
	self.显示类型 = 0
	self.本类开关 = false
	self.文字 = 文字类.创建(simsun, 14)

	self.文字:置颜色(黑色)

	self.招牌文字 = 文字类.创建(simsun, 15)

	self.招牌文字:置颜色(黄色)

	self.bb文字 = 文字类.创建(simsun, 14)

	self.bb文字:置颜色(黑色)

	self.状态文字 = 文字类.创建(simsun, 12)
	self.金钱文字 = 文字类.创建(simsun, 14)
	self.物品列表 = {}
	self.bb列表 = {}
	self.选中背景 = 图像类("imge/001/125-1.png")
	self.选中背景1 = 图像类("imge/001/dhs.png")
	self.选中编号 = 0
	self.上架 = 按钮类.创建("imge/001/0016.png", 2, "购买", 3)
	self.bb选中 = 0
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "关闭", 3)
	self.收摊按钮 = 按钮类.创建("imge/001/0016.png", 2, "收摊", 3)
end

function 摆摊购买类:刷新(数据)
	self.招牌名称 = 数据.名称
	self.招牌id = 数据.id
	self.招牌角色 = 数据.角色
	self.当前银子 = 数据.银子
	self.物品数据 = {}
	self.bb数据 = {}
	self.本类开关 = true
	self.购买数量 = 1
	self.购买单价 = 0
end

function 摆摊购买类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 摆摊购买类:刷新bb(数据)

	self.显示类型 = 2
	self.bb选中 = 0
	self.购买数量 = 1
	self.购买单价 = 0
	self.bb数据 = {}
	self.包围盒 = {}
	self.显示序列 = 0

	for n = 1, 20, 1 do
		if 数据[n] ~= nil and 数据[n].bb ~= nil then
			self.显示序列 = self.显示序列 + 1
			self.bb数据[n] = 数据[n].bb



			self.bb数据[n].单价 = 数据[n].单价
			self.包围盒[n] = 包围盒:创建(150, 192, 64, 18)

			self.包围盒[n]:置坐标(286, 180 + 19 * self.显示序列)
			self.包围盒[n]:更新宽高(string.len(self.bb数据[n].名称) * 9, 18)
		end
	end
end

function 摆摊购买类:刷新道具(数据)

	self.显示类型 = 1
	self.选中编号 = 0
	self.购买数量 = 1
	self.购买单价 = 0
	self.物品数据 = {}

	for n = 1, 20, 1 do
		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n].道具, 3, "摆摊", n)
			self.物品数据[n].编号 = 数据[n].编号
			self.物品数据[n].单价 = 数据[n].单价
		end
	end
end

function 摆摊购买类:更新(dt)
	self.上架:更新(285, 467)
	self.取消按钮:更新(355, 467)
	self.关闭:更新(528, 76)
	self.道具按钮:更新(270, 162)
	self.bb按钮:更新(370, 162)
	self.制造按钮:更新(370, 162)

	if self.关闭:取是否单击() or self.取消按钮:取是否单击() then
		self.本类开关 = false
	elseif self.上架:取是否单击() then
		if self.显示类型 == 1 then
			if self.选中编号 == 0 then
				信息提示:加入提示("#y/请先选中一个道具")

				return 0
			else
				客户端:发送数据(self.选中编号, 8, 44, self.购买数量, 1)
			end
		elseif self.显示类型 == 2 then
			if self.bb选中 == 0 then
				信息提示:加入提示("#y/请先选中一个道具")

				return 0
			else
				客户端:发送数据(self.bb选中, 11, 44, self.购买数量, 1)
			end
		end
	elseif self.道具按钮:取是否单击() then
		客户端:发送数据(2647, 9, 44, "1", 1)
	elseif self.bb按钮:取是否单击() then
		客户端:发送数据(4512, 10, 44, "2", 1)
	end

	if 引擎.鼠标弹起(右键) and self:界面重叠() then
		local xx, yy = 鼠标.x - self.背景.x - 20, 鼠标.y - self.背景.y - 120
		if(xx < 0 or yy < 0 or yy > 210 or (xx > (self.显示类型 == 1 and 260 or 235))) then
			self.本类开关 = false
		end
	end
end

function 摆摊购买类:道具选中事件(编号)
	self.购买单价 = self.物品数据[编号].单价 + 0
	self.购买数量 = 1
	self.bb选中 = 编号
end

function 摆摊购买类:bb选中事件(编号)
	self.购买单价 = self.bb数据[编号].单价 + 0
	self.购买数量 = 1
end

function 摆摊购买类:取召唤兽显示重叠()
	if tp.主界面.界面数据[21].本类开关 and tp.主界面.界面数据[21].背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 摆摊购买类:显示(x, y)
	self.背景:显示(260, 70)
	self.道具按钮:显示(280, 162)
	self.bb按钮:显示(370, 162)
	self.制造按钮:显示(460, 162)
	self.上架:显示(285, 467)
	self.取消按钮:显示(355, 467)
	self.招牌文字:显示(374, 104, self.招牌名称)
	self.文字:显示(464, 131, self.招牌id)
	self.文字:显示(320, 131, self.招牌角色)
	self.文字:显示(458, 409, self.购买数量)
	银两显示(self.金钱文字, self.当前银子, 457, 436)
	银两显示(self.金钱文字, self.购买单价, 327, 407)
	银两显示(self.金钱文字, self.购买单价 * self.购买数量, 327, 435)
	self.关闭:显示(528, 76)

	if self.显示类型 == 1 then
		self.道具背景:显示(278, 188)

		self.显示id = 0

		for n = 1, 20, 1 do
			if self.物品数据[n] ~= nil then
				if n <= 5 then
					self.显示xy = {
						y = 190,
						x = 231 + n * 50
					}
				elseif n <= 10 then
					self.显示xy = {
						y = 240,
						x = 231 + (n - 5) * 50
					}
				elseif n <= 15 then
					self.显示xy = {
						y = 290,
						x = 231 + (n - 10) * 50
					}
				elseif n <= 20 then
					self.显示xy = {
						y = 340,
						x = 231 + (n - 15) * 50
					}
				end

				if self.物品数据[n]:获取焦点() then
					self.显示id = n
				end

				self.物品数据[n]:显示(self.显示xy.x, self.显示xy.y)

				if self.选中编号 == n then
					self.选中背景:显示(self.显示xy.x - 3, self.显示xy.y)
				end
			end
		end

		if self.显示id ~= 0 then
			self.物品数据[self.显示id]:显示事件()

			if 引擎.鼠标弹起(左键) then
				if self.选中编号 == self.显示id then
					self.购买数量 = self.购买数量 + 1

					if self.购买数量 > 99 then
						self.购买数量 = 99
					end

					return 0
				end

				self.选中编号 = self.显示id

				self:道具选中事件(self.显示id)
			elseif 引擎.鼠标弹起(右键) and self.选中编号 == self.显示id then
				self.购买数量 = self.购买数量 - 1

				if self.购买数量 < 1 then
					self.购买数量 = 1
				end

				return 0
			end
		end
	elseif self.显示类型 == 2 then
		self.bb背景:显示(278, 188)

		self.显示序列 = 0

		for n = 1, 10, 1 do
			if self.bb数据[n] ~= nil then
				self.显示序列 = self.显示序列 + 1

				if self.bb选中 == n then
					self.选中背景1:显示(284, 178 + 19 * self.显示序列)
				end

				self.bb文字:置颜色(黑色):显示(286, 180 + 19 * self.显示序列, self.bb数据[n].名称)
				self.包围盒[n]:显示()

				if self.包围盒[n]:检查点(鼠标.x, 鼠标.y) then
					if 引擎.鼠标弹起(左键) and self:取召唤兽显示重叠() == false then
						self.bb选中 = n

						self:bb选中事件(n)
					elseif 引擎.鼠标弹起(右键) and self:取召唤兽显示重叠() == false then
						tp.主界面.界面数据[21]:刷新(self.bb数据[n])
					end
				end
			end
		end
	end
end

return 摆摊购买类
