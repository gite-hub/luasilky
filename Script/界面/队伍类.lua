local 队伍类 = class()
local tp
function 队伍类:初始化(根)
	tp= 根
	self.本类开关 = false
	self.背景 = 图像类("imge/001/408-1.png")
	self.背景1 = 图像类("imge/001/284-1.png")
	self.文字 = 文字类(simsun, 15)
	self.字体 = 文字类.创建(simsun, 15)
	self.阵型文字 = 文字类.创建(simsun, 15)
	self.文字:置颜色(黑色)
	self.阵型文字:置颜色(黑色)
	self.超级文本 = require("丰富文本类")(200, 200)
	self.超级文本:添加元素("W", 4294967295.0)
	self.超级文本:添加元素("H", 黄色)
	self.超级文本:添加元素("Y", 4294967040.0)
	self.超级文本:添加元素("R", 4294901760.0)
	self.超级文本:添加元素("G", 4278255360.0)
	self.提升队长 = 按钮类("imge/001/0015.png", 2, "升为队长", 3, 0)
	self.请求列表 = 按钮类("imge/001/0015.png", 2, "请求列表", 3, 0)
	self.踢出队伍 = 按钮类("imge/001/0015.png", 2, "踢出队伍", 3, 0)
	self.离开队伍 = 按钮类("imge/001/0015.png", 2, "离开队伍", 3, 0)
	self.选定阵型 = 按钮类("imge/001/0015.png", 2, "选定阵型", 3, 0)
	self.更换位置 = 按钮类("imge/001/0015.png", 2, "更换位置", 3, 0)
	self.申请开关 = false
	self.申请界面 =tp.资源:载入('JM.FT',"网易WDF动画",2412600967)
	self.阵型 = 按钮类("imge/001/0016.png", 2, "阵型", 3, 0)
	self.关闭1 = 按钮类("imge/001/0046.png", 1, "提升队长", 3, 0)
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, 1, 3)
	self.关闭1 = 按钮类.创建("imge/001/0046.png", 1, 1, 3)
	self.允许 = 按钮类("imge/001/0016.png", 2, "允许", 3, 0)
	self.拒绝 = 按钮类("imge/001/0016.png", 2, "拒绝", 3, 0)
	self.选中背景 = {
		图像类("imge/001/148-1.png"),
		图像类("imge/001/147-2.png")
	}
	self.选中编号 = {
		[1.0] = 0,
		[2.0] = 0
	}
	self.阵型开关 = false
	self.显示坐标 = {
		{
			x = 170,
			y = 280
		},
		{
			x = 290,
			y = 280
		},
		{
			x = 410,
			y = 280
		},
		{
			x = 530,
			y = 280
		},
		{
			x = 650,
			y = 280
		}
	}
	self.显示序列 = {
		1,
		2,
		3,
		4,
		5
	}
	self.判定事件 = {}
	self.人偶 = {}

	for n = 1, 5, 1 do
		self.人偶[n] = 图像类("imge/001/287-1.png")
		self.判定事件[n] = false
	end

	self.阵型位置 = {
		鸟翔阵 = {
			{
				x = 363,
				y = 255
			},
			{
				x = 429,
				y = 262
			},
			{
				x = 368,
				y = 298
			},
			{
				x = 429,
				y = 223
			},
			{
				x = 297,
				y = 295
			},
			说明 = "全速型阵法，所有队员的速度大幅度提高。"
		},
		蛇蟠阵 = {
			{
				x = 400,
				y = 287
			},
			{
				x = 367,
				y = 302
			},
			{
				x = 430,
				y = 274
			},
			{
				x = 331,
				y = 276
			},
			{
				x = 433,
				y = 317
			},
			说明 = "灵力型阵法，阵中及两侧抵御敌人法术的几率增加，阵前及阵尾攻击效果提高。"
		},
		龙飞阵 = {
			{
				x = 377,
				y = 293
			},
			{
				x = 408,
				y = 317
			},
			{
				x = 419,
				y = 228
			},
			{
				x = 306,
				y = 292
			},
			{
				x = 346,
				y = 271
			},
			说明 = "全能型阵法，队长法术防御效果提高，阵尾物理防御效果提高，阵前中间队员攻击效果提高，左翼速度提高，右翼法术对敌伤害提高、速度降低。"
		},
		云垂阵 = {
			{
				x = 425,
				y = 305
			},
			{
				x = 387,
				y = 242
			},
			{
				x = 322,
				y = 280
			},
			{
				x = 424,
				y = 219
			},
			{
				x = 279,
				y = 301
			},
			说明 = "防御型阵法，两翼队员速度提高，右肋防御效果提高，左肋攻击效果大幅度提高，阵尾防御效果大幅度提高，速度大幅度降低。"
		},
		风扬阵 = {
			{
				x = 399,
				y = 282
			},
			{
				x = 433,
				y = 264
			},
			{
				x = 365,
				y = 299
			},
			{
				x = 404,
				y = 246
			},
			{
				x = 333,
				y = 284
			},
			说明 = "攻速型阵法，两翼速度效果提高，其余位置攻击效果提高。"
		},
		普通 = {
			{
				x = 357,
				y = 255
			},
			{
				x = 389,
				y = 240
			},
			说明 = "普通的一字阵形，没有特别的站位效果。",
			[3] = {
				x = 324,
				y = 273
			},
			[5] = {
				x = 291,
				y = 292
			},
			[4] = {
				x = 422,
				y = 226
			}
		},
		天覆阵 = {
			{
				x = 379,
				y = 278
			},
			{
				x = 389,
				y = 240
			},
			说明 = "攻击型阵法，所有队员物理、法术攻击效果提高，速度下降。",
			[3] = {
				x = 324,
				y = 273
			},
			[5] = {
				x = 291,
				y = 292
			},
			[4] = {
				x = 422,
				y = 226
			}
		},
		虎翼阵 = {
			{
				x = 426,
				y = 299
			},
			{
				x = 429,
				y = 262
			},
			{
				x = 368,
				y = 298
			},
			{
				x = 429,
				y = 223
			},
			{
				x = 297,
				y = 295
			},
			说明 = "攻击型阵法，两翼攻击效果提高，中间防御效果提高，阵尾攻击效果大幅度提高。"
		},
		地载阵 = {
			{
				x = 399,
				y = 281
			},
			{
				x = 373,
				y = 256
			},
			{
				x = 433,
				y = 266
			},
			{
				x = 364,
				y = 298
			},
			{
				x = 419,
				y = 306
			},
			说明 = "防御型阵法，阵中及两侧物理、法术防御效果提高，阵前攻击效果提高，阵后速度提高。"
		}
	}
	self.阵法名称 = {
		"普通",
		"风扬阵",
		"虎翼阵",
		"天覆阵",
		"云垂阵",
		"鸟翔阵",
		"地载阵",
		"龙飞阵",
		"蛇蟠阵"
	}
	self.阵法按钮 = {}
	self.临时代号 = 0

	for n = 1, 3, 1 do
		for i = 1, 3, 1 do
			self.临时代号 = self.临时代号 + 1
			self.阵法按钮[self.临时代号] = 按钮类.创建("imge/001/0015.png", 2, self.阵法名称[self.临时代号], 3, 1)
		end
	end

	self.显示坐标[1] = {
		x = 170,
		y = 280
	}
	self.显示坐标[2] = {
		x = 290,
		y = 280
	}
	self.显示坐标[3] = {
		x = 410,
		y = 280
	}
	self.显示坐标[4] = {
		x = 530,
		y = 280
	}
	self.显示坐标[5] = {
		x = 650,
		y = 280
	}
	self.显示序列 = {
		1,
		2,
		3,
		4,
		5
	}
	self.当前阵型 = 1

	self.超级文本:添加文本("#G/" .. self.阵型位置.普通.说明)

	self.队伍数据 = {}
end

function 队伍类:刷新请求列表(内容)
	self.申请开关 = true
	self.申请数据 = {}
	self.申请选中 = 0

	if 内容 == nil then
		return 0
	end

	self.申请数据 = {}

	if 内容 ~= nil then
		for n = 1, #内容, 1 do
			self.申请数据[n] = 内容[n]
			self.申请数据[n].动画 = 创建角色动画(self.申请数据[n].造型, self.申请数据[n].染色, self.申请数据[n].武器数据.名称, self.申请数据[n].武器数据.等级, self.申请数据[n].武器数据.类别, self.申请数据[n].武器数据.强化, self.申请数据[n].坐骑, self.申请数据[n].变身,self.申请数据[n].锦衣数据)
		end
	end
end

function 队伍类:刷新(内容)
	self.申请开关 = false
	self.选中编号 = 0
	self.队伍数据 = {}

	if 内容 ~= nil then


		for n = 1, #内容, 1 do
			self.队伍数据[n] = 内容[n]
			print(self.队伍数据[n].造型)
			-- table.print(self.队伍数据[n].染色)
			print(self.队伍数据[n].武器数据.名称)
			print(self.队伍数据[n].武器数据.等级)
			print(self.队伍数据[n].武器数据.类别)
			print(self.队伍数据[n].武器数据.强化)
			print(self.队伍数据[n].坐骑)
			print(self.队伍数据[n].变身)
			-- table.print(self.队伍数据[n].锦衣数据)
			self.队伍数据[n].动画 = 创建角色动画(self.队伍数据[n].造型, self.队伍数据[n].染色, self.队伍数据[n].武器数据.名称, self.队伍数据[n].武器数据.等级, self.队伍数据[n].武器数据.类别, self.队伍数据[n].武器数据.强化, self.队伍数据[n].坐骑, self.队伍数据[n].变身,self.队伍数据[n].锦衣数据)
		end
	end

	self.本类开关 = true
	self.当前阵法 = ""
end

function 队伍类:刷新阵法界面(内容)
	self.阵型开关 = true
	self.拥有阵型 = 内容
end

function 队伍类:刷新阵法(内容)
	self.当前阵法 = 内容
end

function 队伍类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 队伍类:更新(dt)
	self.关闭:更新(dt)
	self.阵型:更新(dt)
	self.请求列表:更新(dt)
	self.踢出队伍:更新(dt)
	self.提升队长:更新(dt)
	self.离开队伍:更新(dt)
	self.更换位置:更新(dt)

	if self.背景:检查点(鼠标.x, 鼠标.y) and self.阵型开关 == false and self.申请开关 == false and 引擎.鼠标弹起(键盘符号.右键) then
		self.本类开关 = false
	end

	if self.阵型:取是否单击() then
		客户端:发送数据(4, 3, 11, "7", 1)
	end

	for n = 1, #self.队伍数据, 1 do
		self.队伍数据[n].动画:更新(dt, "静立")
		-- if self.队伍数据[n].动画["静立"]:是否选中(鼠标.x,鼠标.y) then
		-- 	if 引擎.鼠标弹起(左键) then
		-- 		self.选中编号 = n
		-- 	elseif 引擎.鼠标弹起(左键) and self.选中编号 == n then
		-- 		self.选中编号 = 0
		-- 	end
		-- end
	end

	if self.申请开关 then
		for n = 1, #self.申请数据, 1 do
			self.申请数据[n].动画:更新(dt, "静立")
		end
		-- 12 36 608-12 180-36
		if self:界面重叠() then
			if 引擎.鼠标弹起(1) then
				self.申请开关 = false
			else
				local x, y = 鼠标.x - self.背景.x - 12, 鼠标.y - self.背景.y - 36
				if(x < 0 or y < 0 or x > 596 or y > 146) then
					-- if 引擎.鼠标弹起(1) then self.申请开关 = false end
				else
					local n = math.floor(x / 119) + 1
					if n >= 1 and n <= 5 then
						if 引擎.鼠标弹起(0) then
							self.申请选中 = n
						elseif (引擎.鼠标弹起(0)) and self.申请选中 == n then
							self.申请选中 = 0
						end
					end
				end
			end
		end

		self.允许:更新(dt)
		self.拒绝:更新(dt)

		if self.允许:取是否单击() then
			if self.申请选中 == 0 then
				信息提示:加入提示("#y/请先选中列表中的一位玩家")
			else
				客户端:发送数据(self.申请选中, 8, 11, "66", 1)

				self.申请选中 = 0
			end
		elseif self.拒绝:取是否单击() then
			if self.申请选中 == 0 then
				信息提示:加入提示("#y/请先选中列表中的一位玩家")
			else
				客户端:发送数据(self.申请选中, 9, 11, "66", 1)

				self.申请选中 = 0
			end
		end
	else
		if self:界面重叠() then
			local x, y = 鼠标.x - self.背景:取包围盒().x - 12, 鼠标.y - self.背景:取包围盒().y - 36 - 36
			if(x < 0 or y < 0 or x > 596 or y > (146 + 36)) then
				-- self.本类开关 = false
			else
				local n = math.floor(x / 119) + 1
				if n >= 1 and n <= 5 then
					if 引擎.鼠标弹起(0) then
						self.选中编号 = n
					elseif (引擎.鼠标弹起(0)) and self.选中编号 == n then
						self.选中编号 = 0
					end
				end
			end
		end
	end

	if self.关闭:取是否单击() then
		if self.申请开关 then
			self.申请开关 = false
		else
			self.本类开关 = false
		end
	elseif self.请求列表:取是否单击() then
		客户端:发送数据(8, 7, 11, "66", 1)
	elseif self.离开队伍:取是否单击() then
		客户端:发送数据(4, 11, 11, "77", 1)
	elseif self.踢出队伍:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中列表中的一位玩家")
		elseif tp.屏幕.主角.队长开关 == false then
			信息提示:加入提示("#y/您不是队长，无法进行此操作")
		else
			客户端:发送数据(self.选中编号, 10, 11, "66", 1)
		end
	elseif self.提升队长:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中列表中的一位玩家")
		elseif tp.屏幕.主角.队长开关 == false then
			信息提示:加入提示("#y/您不是队长，无法进行此操作")
		else
			客户端:发送数据(self.选中编号, 12, 11, "66", 1)
		end
	elseif self.更换位置:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中列表中的一位玩家")
		elseif tp.屏幕.主角.队长开关 == false then
			信息提示:加入提示("#y/您不是队长，无法进行此操作")
		else
			客户端:发送数据(self.选中编号, 13, 11, "66", 1)
		end
	end
end

function 队伍类:显示(x, y)
	self.背景:显示(100, 100)
	self.阵型:显示(120, 140)
	self.关闭:显示(695, 103)
	self.请求列表:显示(450, 135)
	self.踢出队伍:显示(530, 123)
	self.提升队长:显示(530, 148)
	self.离开队伍:显示(610, 123)
	self.更换位置:显示(610, 148)
	self.阵型文字:显示(196, 143, self.当前阵法)

	for n = 1, #self.队伍数据, 1 do
		--tp.影子:显示(self.显示坐标[self.显示序列[n]].x, self.显示坐标[self.显示序列[n]].y)
		self.队伍数据[n].动画:显示("静立", self.显示坐标[self.显示序列[n]].x, self.显示坐标[self.显示序列[n]].y)
		self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55, self.显示坐标[self.显示序列[n]].y + 40, self.队伍数据[n].名称)
		self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55, self.显示坐标[self.显示序列[n]].y + 63, self.队伍数据[n].门派)
		self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55, self.显示坐标[self.显示序列[n]].y + 86, self.队伍数据[n].等级 .. "级")

		if self.选中编号 == n then
			self.选中背景[1]:显示(self.显示坐标[self.显示序列[n]].x - 60, self.显示坐标[self.显示序列[n]].y - 110)
		end
	end

	if self.申请开关 then
		self.申请界面:显示(100, 100)
		self.关闭1:显示(695, 103)
		self.允许:显示(505, 363)
		self.拒绝:显示(565, 363)

		self.误差y = 35
		self.误差x = 2

		for n = 1, #self.申请数据, 1 do
			--tp.影子:显示(self.显示坐标[self.显示序列[n]].x + self.误差x, self.显示坐标[self.显示序列[n]].y - self.误差y)
			self.申请数据[n].动画:显示("静立", self.显示坐标[self.显示序列[n]].x + self.误差x, self.显示坐标[self.显示序列[n]].y - self.误差y)
			self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55 + self.误差x, self.显示坐标[self.显示序列[n]].y + 40 - self.误差y, self.申请数据[n].名称)
			self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55 + self.误差x, self.显示坐标[self.显示序列[n]].y + 63 - self.误差y, self.申请数据[n].门派)
			self.文字:显示(self.显示坐标[self.显示序列[n]].x - 55 + self.误差x, self.显示坐标[self.显示序列[n]].y + 86 - self.误差y, self.申请数据[n].等级 .. "级")

			if self.申请选中 == n then
				self.选中背景[1]:显示(self.显示坐标[self.显示序列[n]].x + self.误差x - 60, self.显示坐标[self.显示序列[n]].y - self.误差y - 110)
			end
		end
	end

	if self.阵型开关 then
		self.临时代号1 = 0

		self.背景1:显示(150, 100)
		self.关闭1:显示(615, 105)
		self.关闭1:更新()
		self.选定阵型:更新()

		if self.关闭1:取是否单击() then
			self.阵型开关 = false
		end

		self.选定阵型:显示(190, 381)

		if self.选定阵型:取是否单击() then
			if self.拥有阵型[self.阵法名称[self.当前阵型]] == false then
				信息提示:加入提示("#y/您似乎还没有学会这个阵型")

				return 0
			else
				客户端:发送数据(5, 5, 11, self.阵法名称[self.当前阵型], 1)
			end
		end

		self.超级文本:显示(420, 130)

		self.临时代号 = 5

		for n = 1, 5, 1 do
			self.人偶[self.临时代号]:显示(self.阵型位置[self.阵法名称[self.当前阵型]][self.临时代号].x, self.阵型位置[self.阵法名称[self.当前阵型]][self.临时代号].y)

			self.临时代号 = self.临时代号 - 1
		end

		self.临时代号1 = 0

		for n = 1, 3, 1 do
			for i = 1, 3, 1 do
				self.临时代号1 = self.临时代号1 + 1

				self.阵法按钮[self.临时代号1]:显示(160 + n * 90 - 90, 130 + i * 30 - 30)
				self.阵法按钮[self.临时代号1]:更新()

				if self.阵法按钮[self.临时代号1]:取是否单击() then
					self.当前阵型 = self.临时代号1
					self.临时说明 = "#G/" .. self.阵型位置[self.阵法名称[self.当前阵型]].说明

					if self.阵法名称[self.当前阵型] ~= "普通" and self.拥有阵型[self.阵法名称[self.当前阵型]] == false then
						self.临时说明 = self.临时说明 .. "#R/(你还不会这个阵法)"
					end

					self.超级文本:清空()
					self.超级文本:添加文本(self.临时说明)
				end
			end
		end
	end
end

return 队伍类
