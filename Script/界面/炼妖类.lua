local 炼妖类 = class()

function 炼妖类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/404-1.png")
	self.x,self.y=0,0
	self.按钮 = 按钮类.创建("imge/001/0016.png", 2, "炼妖", 3)
	self.允许关闭 = true
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, 1, 3)
end

function 炼妖类:添加召唤兽(数据)
	self.格子 = {
		0,
		0,
		0
	}
	self.动画 = {}
	self.字体 = {}
	self.名称 = {}
	self.造型 = {}
	self.包围盒 = {}
	self.标示 = {}
	self.临时数据 = {
		数据 = 数据
	}

	for n = 1, #self.临时数据.数据, 1 do
		self.动画[n] = 引擎.场景.召唤兽动画类.创建(self.临时数据.数据[n].造型, self.临时数据.数据[n].变异,self.临时数据.数据[n].饰品)
		self.名称[n] = self.临时数据.数据[n].名称
		self.字体[n] = 文字类(simsun, 14)

		self.字体[n]:置颜色(黑色)

		self.包围盒[n] = 包围盒:创建(150, 192, 64, 18)

		self.包围盒[n]:置坐标(18, 30 + n * 20)
		self.包围盒[n]:更新宽高(string.len(self.名称[n]) * 9, 18)
	end
end

function 炼妖类:添加道具(数据)

	self.当前类型 = "包裹"
	self.物品数据 = {}

	for n = 1, 26, 1 do
		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n], 3, self.当前类型, n)
			self.物品数据[n].编号 = 数据[n].编号
		end
	end
end

function 炼妖类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 炼妖类:刷新()
	self.本类开关 = true
	self.格子 = {
		0,
		0,
		0
	}
end

function 炼妖类:更新(dt)
	self.关闭:更新(dt)

	if self.格子[1] ~= 0 then
		self.动画[self.格子[1]]:更新(dt, "静立")
	end

	if self.格子[2] ~= 0 then
		self.动画[self.格子[2]]:更新(dt, "静立")
	end

	self.按钮:更新(dt)

	if self.按钮:取是否单击() then
		self:条件检测()
	end

	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(右键) then
		self.本类开关 = false
	end

	if self.关闭:取是否单击() then
		self.本类开关 = false
	end
end

function 炼妖类:条件检测()
	if self.格子[1] ~= 0 and self.格子[2] ~= 0 then
		self.发送信息 = {
			self.格子[1],
			self.格子[2]
		}

		客户端:发送数据(self.格子[1], 23, 13, self.格子[2], 1)
	elseif self.格子[1] ~= 0 and self.格子[3] ~= 0 then
		self.发送信息 = {
			self.格子[1],
			self.格子[3]
		}

		客户端:发送数据(self.格子[1], 24, 13, self.格子[3], 1)
	end
end

function 炼妖类:获取格子(id)
	if self.格子[1] == id then
		return 1
	elseif self.格子[2] == id then
		return 2
	else
		return 0
	end
end

function 炼妖类:显示(x, y)
		  if self:界面重叠() then
   self:拖拽()
  end
	self.背景:显示(self.x +100, self.y +50)
	self.按钮:显示(self.x +300, self.y +170)

	self.显示格子 = 0

	self.关闭:显示(self.x +520, self.y +60)

	for n = 1, #self.动画, 1 do
		self.焦点格子 = 0

		if self.包围盒[n]:检查点(鼠标.x, 鼠标.y) then
			if n ~= self.格子[1] and n ~= self.格子[2] then
				self.焦点格子 = n

				self.字体[n]:置颜色(红色)
			end

			if 引擎.鼠标弹起(键盘符号.左键) then
				self.临时格子 = self:获取格子(n)

				if self.临时格子 ~= 0 then
					self.格子[self.临时格子] = 0
				else
					if self.格子[1] == 0 then
						self.临时格子 = 1
					elseif self.格子[2] == 0 then
						self.临时格子 = 2
					end

					self.格子[self.临时格子] = n

					self.字体[n]:置颜色(黄色)
				end
			end
		end

		if n ~= self.格子[1] and n ~= self.格子[2] and n ~= self.焦点格子 then
			self.字体[n]:置颜色(黑色)
		end

		self.包围盒[n]:置坐标(self.x +130, self.y +230 + n * 20)
		self.字体[n]:显示(self.x +130,self.y +230 + n * 20, self.名称[n])
		self.包围盒[n]:显示()
	end

	if self.格子[1] ~= 0 then
		引擎.场景.影子:显示(self.x +180,self.y + 180)
		self.动画[self.格子[1]]:显示("静立", self.x +180, self.y +180)
	end

	if self.格子[2] ~= 0 then
		引擎.场景.影子:显示(self.x +450,self.y + 180)
		self.动画[self.格子[2]]:显示("静立", self.x +450, self.y +180)
	end

	self.临时x = 0
	self.临时y = 0
	self.显示格子 = 0

	for n = 1, 20, 1 do
		self.临时x = self.临时x + 1

		if self.临时x == 6 then
			self.临时y = self.临时y + 1
			self.临时x = 1

			if self.临时y == 5 then
				self.临时y = 1
			end
		end

		if self.物品数据[n] ~= nil and n ~= self.格子[3] then
			self.物品数据[n].小动画:显示(self.x +220 + self.临时x * 52, self.y +210 + self.临时y * 52)

			if self.物品数据[n]:获取焦点() then
				self.显示格子 = n

				if 引擎.鼠标弹起(键盘符号.左键) then
					if self.物品数据[n].名称 == "金柳露" or self.物品数据[n].名称 == "超级金柳露"or self.物品数据[n].名称 == "吸附石"or self.物品数据[n].名称 == "圣兽丹" or self.物品数据[n].名称 == "魔兽要诀" or self.物品数据[n].名称 == "高级魔兽要诀" or self.物品数据[n].名称 == "召唤兽内丹" or self.物品数据[n].名称 == "高级召唤兽内丹" then
						self.格子[3] = n
					else
						信息提示:加入提示("#y/只有金柳露、超级金柳露、魔兽要诀、内丹、吸附石才可以炼妖")
					end
				end
			end
		end
	end

	if self.显示格子 ~= 0 then
		self.物品数据[self.显示格子]:显示事件(2)
	end

	if self.格子[3] ~= 0 then
		self.物品数据[self.格子[3]].小动画:显示(self.x +295, self.y +100)

		if self.物品数据[self.格子[3]]:获取焦点() then
			self.物品数据[self.格子[3]]:显示事件(2)

			if 引擎.鼠标弹起(键盘符号.左键) then
				self.格子[3] = 0
			end
		end
	end
end
function 炼妖类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end


return 炼妖类
