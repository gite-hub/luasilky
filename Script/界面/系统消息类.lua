local 系统消息类 = class()

function 系统消息类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2395-1.png")
	self.精灵头像 = 图像类("imge/001/2875-1.png")
	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(黑色)

	self.超丰富文本 = require("丰富文本类")(300, 110)

	self.超丰富文本:添加元素("w", 4294967295.0)
	self.超丰富文本:添加元素("h", 4278190080.0)
	self.超丰富文本:添加元素("y", 4294967040.0)
	self.超丰富文本:添加元素("r", 4294901760.0)
	self.超丰富文本:添加元素("g", 4278255360.0)

	self.加为好友 = 按钮类.创建("imge/001/0015.png", 2, "加为好友", 3)
	self.查询 = 按钮类.创建("imge/001/0016.png", 2, "查询", 3)
	self.手机 = 按钮类.创建("imge/001/0016.png", 2, "手机", 3)
	self.下一条 = 按钮类.创建("imge/001/0015.png", 2, "下一条", 3)
	self.发送 = 按钮类.创建("imge/001/0016.png", 2, "发送", 3)
end

function 系统消息类:刷新(内容)
	self.本类开关 = true

	self.超丰富文本:清空()
	self.超丰富文本:添加文本(内容)
end

function 系统消息类:更新(dt)
	self.下一条:更新()

	if self.下一条:取是否单击() then
		客户端:发送数据(50, 52, 13, "52", 1)
	end

	if self.背景:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(键盘符号.右键) then
		self.本类开关 = false
	end
end

function 系统消息类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 系统消息类:显示(x, y)
	self.背景:显示(200, 100)
	self.精灵头像:显示(213, 128)
	self.文字:显示(319, 138, "系统")
	self.文字:显示(319, 162, "大唐")
	self.超丰富文本:显示(223, 194)
	self.加为好友:显示(219, 313)
	self.查询:显示(299, 313)
	self.下一条:显示(349, 313)
	self.查询:显示(429, 313)
	self.发送:显示(482, 313)
end

return 系统消息类
