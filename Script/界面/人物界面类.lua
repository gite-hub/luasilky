local 人物界面类 = class()

function 人物界面类:初始化()
	self.本类开关 = false
	self.允许关闭 = true
	self.加点标示 = {
		"体质",
		"魔力",
		"力量",
		"耐力",
		"敏捷"
	}
	self.背景 = 图像类("imge/001/364-1.png")
	self.y = 80
	self.x = 512
	self.偏移模式 = true
	self.偏移坐标 = {
		x = 0,
		y = 0
	}
	self.正常文字 = 文字类(simsun, 16)

	self.正常文字:置颜色(4278190080.0)

	self.加点按钮 = {}
	self.减点按钮 = {}
	self.确认加点 = 按钮类.创建("imge/001/0015.png", 2, "查看技能", 3)
	self.更多属性 = 按钮类.创建("imge/001/0015.png", 2, "更多属性", 3)
	self.炼药 = 按钮类.创建("imge/001/0016.png", 2, "技能", 3)
	self.改名 = 按钮类.创建("imge/001/0016.png", 2, "称谓", 3)
	self.改名文字 = 文字类(simsun, 16)

	self.改名文字:置颜色(黑色)

	self.升级 = 按钮类.创建("imge/001/0016.png", 2, "升级", 3)
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.数据 = {}

	for n = 1, 5, 1 do
		self.加点按钮[n] = 按钮类.创建("imge/001/0013.png", 1, 1, 4)
		self.减点按钮[n] = 按钮类.创建("imge/001/0014.png", 1, 1, 4)
	end
end

function 人物界面类:刷新(数据)
	self.数据 = 数据
	self.本类开关 = true
end

function 人物界面类:更新体活(数据)
	self.数据.当前活力 = 数据.当前活力
	self.数据.当前体力 = 数据.当前体力
end

function 人物界面类:更新(dt)
	self.升级:更新()
	self.关闭:更新()
	self.改名:更新(dt)
	self.确认加点:更新(dt)
	self.更多属性:更新(dt)
	self.炼药:更新(dt)

	for n = 1, 5, 1 do
		self.加点按钮[n]:更新()

		if self.加点按钮[n]:取是否单击() or (引擎.按键按住(KEY.CTRL) and self.加点按钮[n]:取按键事件()) then --调试模式
			客户端:发送数据(0, 3, 5, self.加点标示[n], 1)
		end

		self.减点按钮[n]:更新()
	end
			if self:界面重叠() then
	 self:拖拽()
	end

	if self.升级:取是否单击() then
		客户端:发送数据(0, 2, 5, "0", 1)
	elseif self.关闭:取是否单击() then
		self.本类开关 = false
	elseif self.确认加点:取是否单击() then
		客户端:发送数据(84, 57, 13, "97", 1)
	elseif self.改名:取是否单击() then
		客户端:发送数据(9, 53, 13, "97", 1)
	elseif self.更多属性:取是否单击() then
		-- table.print(self.数据)
		tp.窗口.幻化属性:打开(self.数据.等级,self.数据.幻化属性,self.数据.躲闪)
	end

	if self.背景:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(键盘符号.右键) then
		self.本类开关 = false
	end
end

function 人物界面类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 人物界面类:显示(x, y)

	self.背景:显示(self.x, self.y)
	self.背景:显示(self.x, self.y)
	self.改名:显示(self.x + 5, self.y + 50)
	self.更多属性:显示(self.x+175,self.y+395)
	self.关闭:显示(self.x + 233, self.y + 5)
	self.正常文字:显示(self.x + 56, self.y + 33, self.数据.等级)
	self.正常文字:显示(self.x + 145, self.y + 33, self.数据.名称)
	self.正常文字:显示(self.x + 75, self.y + 102, self.数据.门派)
	self.正常文字:显示(self.x + 210, self.y + 55, self.数据.人气)
	self.正常文字:显示(self.x + 60, self.y + 79, self.数据.帮派)
	self.正常文字:显示(self.x + 210, self.y + 79, self.数据.帮贡)
	self.正常文字:显示(self.x + 210, self.y + 101, self.数据.门贡)
	self.正常文字:显示(self.x + 56, self.y + 132, self.数据.当前气血 .. "/" .. self.数据.气血上限 .. "/" .. self.数据.最大气血)
	self.正常文字:显示(self.x + 56, self.y + 155, self.数据.当前魔法 .. "/" .. self.数据.魔法上限)
	self.正常文字:显示(self.x + 56, self.y + 178, self.数据.愤怒 .. "/" .. 150)
	self.正常文字:显示(self.x + 56, self.y + 201, self.数据.当前活力 .. "/" .. self.数据.活力上限)
	self.正常文字:显示(self.x + 56, self.y + 224, self.数据.当前体力 .. "/" .. self.数据.体力上限)
	self.正常文字:显示(self.x + 54, self.y + 254, self.数据.命中)
	self.正常文字:显示(self.x + 54, self.y + 277, self.数据.伤害)
	self.正常文字:显示(self.x + 54, self.y + 300, self.数据.防御)
	self.正常文字:显示(self.x + 54, self.y + 323, self.数据.速度)
	self.正常文字:显示(self.x + 54, self.y + 346, self.数据.躲闪)
	self.正常文字:显示(self.x + 54, self.y + 369, self.数据.灵力)
	self.正常文字:显示(self.x + 140, self.y + 369, self.数据.潜能)
	self.正常文字:显示(self.x + 150, self.y + 254, self.数据.体质)
	self.正常文字:显示(self.x + 150, self.y + 277, self.数据.魔力)
	self.正常文字:显示(self.x + 150, self.y + 300, self.数据.力量)
	self.正常文字:显示(self.x + 150, self.y + 323, self.数据.耐力)
	self.正常文字:显示(self.x + 150, self.y + 346, self.数据.敏捷)
	self.正常文字:显示(self.x + 85, self.y + 401, self.数据.升级经验)
	self.正常文字:显示(self.x + 85, self.y + 424, self.数据.当前经验)

	for n = 1, 5, 1 do
		self.加点按钮[n]:显示(self.x + 195, self.y + 223 + n * 24)
		self.减点按钮[n]:显示(self.x + 218, self.y + 223 + n * 24)
	end

	self.确认加点:显示(self.x + 175, self.y + 365)
	self.升级:显示(self.x + 175, self.y + 423)
end
function 人物界面类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
    end
    end
return 人物界面类
