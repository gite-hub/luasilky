
local 系统设置类 = class()
local tp

function 系统设置类:初始化(根)
 self.背景=图像类("imge/001/2909-1.png")
 self.本类开关=false
 self.文字=文字类(simsun,16)
 self.增加=按钮类.创建("imge/001/0016.png",2,"增加",3)
 self.减少=按钮类.创建("imge/001/0016.png",2,"减少",3)
 self.关闭=按钮类.创建("imge/001/0046.png",1,"升级",3)
 self.完成设置=按钮类.创建("imge/001/0015.png",2,"完成设置",3)
 self.更改=按钮类.创建("imge/001/0016.png",2,"更改",3)
 tp =根
end

function 系统设置类:界面重叠()

 if self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then

     return true
    else
     return false

 	 end

 end
function 系统设置类:更新(dt)
 self.增加:更新(dt)
 self.减少:更新(dt)
 self.关闭:更新(dt)
 self.完成设置:更新(dt)
 self.更改:更新(dt)

 if self.增加:取是否单击() or self.增加:取按键事件() then

     if 全局变量.音量<100 then

         全局变量.音量=全局变量.音量+1
         self:调整音量()


     	 end

    elseif self.减少:取是否单击() or self.减少:取按键事件() then

     if 全局变量.音量>0 then

         全局变量.音量=全局变量.音量-1
         self:调整音量()


     	 end

    elseif self.完成设置:取是否单击() or self.关闭:取是否单击() or (self.背景:取包围盒():检查点(鼠标.x,鼠标.y) and 引擎.鼠标弹起(右键)) then

      self.本类开关=false

     elseif self.更改:取是否单击()  then
        客户端:发送数据(5,73,13,"1")


 	 end
end

function 系统设置类:调整音量()
 引擎.置ini("配置.ini")
 引擎.写ini("账号信息","音量",全局变量.音量)
 if tp.地图.地图音乐~=nil then


      tp.地图.地图音乐:置音量(全局变量.音量) --*10
      --地图.地图音乐:停止()
     -- 地图.地图音乐:播放()

 	 end





 end



function 系统设置类:显示(x,y)

 self.背景:显示(200,80)
 self.文字:置颜色(白色):显示(218,117,"游戏音量:")
 self.文字:置颜色(黄色):显示(298,117,全局变量.音量)
 self.增加:显示(328,117)
 self.减少:显示(378,117)
 self.关闭:显示(560,85)
 self.完成设置:显示(345,420)

 self.文字:置颜色(白色):显示(218,150,"物品接受开关:")
 self.更改:显示(338,150)
end

return 系统设置类