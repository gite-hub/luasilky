local 帮派宗旨填写类 = class()

function 帮派宗旨填写类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/267-1.png")
	self.说明字体 = 文字类.创建(simsun, 14):置颜色(绿色)
	self.给予按钮 = 按钮类.创建("imge/001/0016.png", 2, "提交", 3, 1)
	self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3, 1)
end

function 帮派宗旨填写类:刷新(dt)
	self.本类开关 = true

	帮派宗旨输入:置可视(true, "帮派宗旨输入")
	帮派宗旨输入:置文本("")
end

function 帮派宗旨填写类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 帮派宗旨填写类:更新(dt)
	self.给予按钮:更新(265, 249)
	self.取消按钮:更新(405, 249)

	if self.取消按钮:取是否单击() then
		self.本类开关 = false

		帮派宗旨输入:置可视(false, "帮派宗旨输入")
	elseif self.给予按钮:取是否单击() then
		if 帮派宗旨输入:取文本() == "" then
			信息提示:加入提示("#y/请先填写帮派名称")
		else
			客户端:发送数据(1, 63, 13, 帮派宗旨输入:取文本())

			self.本类开关 = false

			帮派宗旨输入:置可视(false, "帮派宗旨输入")
		end
	end
end

function 帮派宗旨填写类:显示(x, y)
	self.背景:显示(100, 150)
	self.说明字体:显示(115, 182, "每次修改帮派宗旨都需要花费50万两帮派资金。")
	self.给予按钮:显示(265, 249)
	self.取消按钮:显示(405, 249)
	_GUI:显示(鼠标.x, 鼠标.y)
end

return 帮派宗旨填写类
