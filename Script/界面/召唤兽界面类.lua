
local 召唤兽界面类 = class()


 bw = require("gge包围盒")(0,0,140,34)
local box = 引擎.画矩形
local floor = math.floor
local min = math.min
local max = math.max
local ceil = math.ceil
local tostring = tostring
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起
local insert = table.insert
local zts,zts1,zts2,ztstt3,tp

-- local tp=引擎.场景
-- local bd0 = {"气血","魔法","攻击","防御","速度","灵力"}
-- local bd = {"体质","魔力","力量","耐力","敏捷"}


function 召唤兽界面类:初始化(根)
     tp=根
local 资源 = tp.资源
local 按钮 = tp._按钮
local 自适应 = tp._自适应
    self.本类开关=false
    self.允许关闭=true
    self.背景=图像类("imge/001/zhs1.png")
    self.佩戴背景=图像类("imge/001/720-1.png")
    self.关闭=按钮类.创建("imge/001/0046.png",1,"升级",3)
    self.上页=按钮类.创建("imge/001/0053.png",1,"升级",3)
    self.下页=按钮类.创建("imge/001/0054.png",1,"升级",3)

    self.下滚动=按钮类.创建("imge/001/下移.png",1,1,3)
    self.上滚动=按钮类.创建("imge/001/上移.png",1,1,3)
   self.圣兽丹动画 =根.资源:载入('JM.FT',"网易WDF动画",0xA4F1E391)


    self.宽度 = self.背景:取宽度()
    self.高度 = self.背景:取高度()
    self.x,self.y=0,0
     self.选中背景=图像类("imge/005/2-2.png")
    -- self.资质界面=图像类("imge/001/1423-1.png")
    self.名称按钮=按钮类.创建("imge/001/0016.png",2,"改名",3)
    self.属性按钮=按钮类.创建("imge/001/0015.png",2,"确认加点",3)
    self.资质按钮=按钮类.创建("imge/001/0017.png",2,"查看资质",3,3)
    self.控制按钮=按钮类.创建("imge/001/0016.png",2,"参战",3)
    self.炼妖按钮=按钮类.创建("imge/001/0016.png",2,"炼妖",3)
    self.放生按钮= 按钮类.创建("imge/001/0016.png",2,"放生",3)
    self.驯养按钮= 按钮类.创建("imge/001/0016.png",2,"驯养",3)
    self.文字=文字类(simsun,16)
    self.属性=文字类(simsun,16)
    self.角色相关字体=文字类(simsun,16)
    self.技能名称字体=文字类(simsun,16)
    self.道具参数=文字类.创建(simsun,15)
    self.道具参数1=文字类.创建(simsun,14)
    self.技能名称字体:置颜色(黄色)
    self.属性:置颜色(黑色)
    self.资质开关=false
    --self.超丰富文本=超级文本.创建(self.角色相关字体,2,240,对白处理,"#","")
    self.超丰富文本=require("丰富文本类")(240,270)
    self.超丰富文本:添加元素("w",0xFFFFFFFF)
    self.超丰富文本:添加元素("h",0xFF000000)
    self.超丰富文本:添加元素("y",0xFFFFFF00)
    self.超丰富文本:添加元素("r",0xFFFF0000)
    self.超丰富文本:添加元素("g",0xFF00FF00)
    self.加点标示={"体质","魔力","力量","耐力","敏捷"}
    self.技能数量=0
    self.技能页数=1
    --self.改名文字=文字类(simsun,15)
    --self.改名文字:置颜色(黑色)
   -- self.改名数据=输入框.创建(self.改名文字,黑色,80,15,1)
    self.加点按钮={}
    self.减点按钮={}
  for n=1,5 do
       self.加点按钮[n]=按钮类.创建("imge/001/0013.png",1,1,4)
       self.减点按钮[n]=按钮类.创建("imge/001/0014.png",1,1,4)
  end

self.资源组 = {
    [1] = 资源:载入('JM.FT',"网易WDF动画",0x3906F9F1),--经验
    [2] = 按钮.创建(自适应.创建(20,4,18,19,4,3)),--上滚动
    [3] = 按钮.创建(自适应.创建(21,4,18,19,4,3)),--下滚动
    --[4] = 自适应.创建(33,1,173,182,3,9),--召唤兽选择框
    [5] = 自适应.创建(2,1,158,153,3,9),--宠物显示框
    [6] = 自适应.创建(3,1,109,19,1,3),
    [7] = 自适应.创建(3,1,97,19,1,3),
    [8] = 自适应.创建(3,1,40,19,1,3),
  --  [34] = 资源:载入('JM.FT',"网易WDF动画",0x314C04D8),
    [9] =  tp._滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,143,2),
    [10] = 资源:载入('JM.FT',"网易WDF动画",0x363AAF1B),
  }
  self.加入 = 0
  self.选中 = 0
  self.拽拖计次 = 0
  self.拽拖对象 = 0
  self.拽拖事件 = 0
  self.插入选区 = 0
  self.操作 = nil
  self.头像组 = {}
  zts = tp.字体表.普通字体
  zts1 = tp.字体表.普通字体__
  zts2 = tp.字体表.描边字体
  ztstt3 = tp.字体表.描边字体


 -- //add by 2019
  self.召唤兽起始索引= 1

 end
function 召唤兽界面类:界面重叠()

 if self.背景:取包围盒():检查点(鼠标.x,鼠标.y)  then

     return true
    else
     return false

   end

 end


function 召唤兽界面类:刷新数据1(数据)
  self.临时数据.数据[self.操作序列]=数据
  self.临时数据.数据[self.操作序列]=self.临时数据.数据[self.操作序列]
  if tp.主界面.界面数据[44].本类开关 then

    tp.主界面.界面数据[44]:打开(self.临时数据.数据[self.操作序列],self.操作序列)

   end
 self.技能数量=0
 self.技能页数=1
 end

function 召唤兽界面类:刷新数据(数据)



 self.临时数据={数据=数据}

 self.道具数据={}
 self.技能数量=0
 self.技能页数=1

  if tp.主界面.界面数据[44].本类开关   and self.操作序列~= 0 then

    tp.主界面.界面数据[44]:打开(self.临时数据.数据[self.操作序列],self.操作序列)

   end
 for n=1,#self.临时数据.数据 do
    self.道具数据[n]={}
    for i=1,3 do

     if self.临时数据.数据[n].道具数据~=nil and self.临时数据.数据[n].道具数据[i]~=nil  then

        self.道具数据[n][i]=物品数据类.创建(self.临时数据.数据[n].道具数据[i],99,n,i)
      end

     end
   end
 end


function 召唤兽界面类:刷新(数据)
  self.头像组={}
self.资源组[9]:置起始点(0)
 bb改名输入:置可视(true,"bb改名输入")
 self.本类开关=true
 self.临时数据={数据=数据}
 self.道具数据={}
 self.技能数量=0
 self.技能页数=1

 for n=1,#self.临时数据.数据 do
    self.道具数据[n]={}
    for i=1,3 do

     if self.临时数据.数据[n].道具数据~=nil and self.临时数据.数据[n].道具数据[i]~=nil  then

        self.道具数据[n][i]=物品数据类.创建(self.临时数据.数据[n].道具数据[i],99,n,i)
      end

     end
   end
      if tp.主界面.界面数据[44].本类开关 and self.操作序列~= 0 then

    tp.主界面.界面数据[44]:打开(self.临时数据.数据[self.操作序列],self.操作序列)

   end


 self.操作序列=0

 self.显示序列=0
 self.动画={}
 self.字体={}
 self.名称={}
 self.造型={}
 self.包围盒={}
 self.标示={}


  bb改名输入:置文本("")

 for n=1,#self.临时数据.数据 do
     self.动画[n]=tp.召唤兽动画类.创建(self.临时数据.数据[n].造型,self.临时数据.数据[n].变异,self.临时数据.数据[n].饰品)
     self.名称[n]=self.临时数据.数据[n].名称
     self.字体[n]=文字类(simsun,14)
     self.字体[n]:置颜色(黑色)
     self.包围盒[n]=包围盒:创建(150,192,140,45)
     self.包围盒[n]:置坐标(self.x+18,self.y-12+n*38)
     self.包围盒[n]:更新宽高(140,38) --string.len( self.名称[n])*9

   end

 end


function 召唤兽界面类:更改名称(内容)

 self.临时信息=内容
 self.临时数据.数据[self.临时信息.序号].名称=self.临时信息.名称
 self.名称[self.临时信息.序号]=self.临时信息.名称

 信息提示:加入提示("#y/召唤兽更改名称成功")

  end

function 召唤兽界面类:更改参战(编号,状态)

  if self.本类开关==false then return 0 end

 if 状态 then

   self.临时数据.数据.参战=编号
   信息提示:加入提示("#y/你的这只召唤兽已经设置为出战状态")
   self.控制按钮.文字="休息"
  else


   self.临时数据.数据.参战=0
   信息提示:加入提示("#y/你的这只召唤兽已经设置为休息状态")
   self.控制按钮.文字="参战"

    end

 end



function 召唤兽界面类:更新(dt)
   self.资质按钮:更新()
   self.名称按钮:更新()
   self.控制按钮:更新()
   self.属性按钮:更新()
   self.放生按钮:更新()
   self.驯养按钮:更新()
   self.炼妖按钮:更新()
   self.关闭:更新()

  _GUI:更新(dt,鼠标.x,鼠标.y)
  if self.关闭:取是否单击() then
    tp.主界面.界面数据[44].本类开关=false
    self.本类开关=false
     bb改名输入:置可视(false,"bb改名输入")

  elseif self.放生按钮:取是否单击() then
    if self.操作序列==0 then

     信息提示:加入提示("#y/请先选中一只召唤兽")
    -- elseif tp.主界面.界面数据[4].本类开关 then
    --   信息提示:加入提示("#y/请先关闭炼妖界面")
    else
     if tp.主界面.界面数据[44].本类开关  then
         tp.主界面.界面数据[44].本类开关=false
     end
     --客户端:发送数据(3005,self.操作序列)
       客户端:发送数据(self.操作序列,1,23,"9B",1)

    end

  elseif self.资质按钮:取是否单击() then
   if self.操作序列==0 then
     信息提示:加入提示("#y/请先选中一只召唤兽")
    else

    tp.主界面.界面数据[44]:打开(self.临时数据.数据[self.操作序列],self.操作序列)
     end

  elseif self.控制按钮:取是否单击() then
    if self.操作序列==0 then

      信息提示:加入提示("#y/请先选中一只召唤兽")
    else

    -- 客户端:发送数据(3007,self.操作序列)
     客户端:发送数据(self.操作序列,2,23,"51",1)


     end
  elseif self.圣兽丹动画:是否选中(鼠标.x,鼠标.y) then
      tp.提示:自定义(鼠标.x+40,鼠标.y,"#y/该召唤兽拥有饰品可以提高#g/?%#y/的资质属性")
  elseif self.名称按钮:取是否单击() then
    if self.操作序列==0 then

      信息提示:加入提示("#y/请先选中一只召唤兽")

    elseif bb改名输入:取文本()=="" then
      信息提示:加入提示("#y/请先输入改名内容")

    else
     --self.发送信息={序号=self.操作序列,名称=bb改名输入:取文本()}
     --客户端:发送数据(3006,table.tostring(self.发送信息))
     客户端:发送数据(self.操作序列,3,23,bb改名输入:取文本(),1)


     end

   elseif self.炼妖按钮:取是否单击() then
     客户端:发送数据(6321,22,13,"9s",1)

    end
  for n=1,#self.包围盒 do

      if self.包围盒[n]~=nil and self.包围盒[n]:检查点(鼠标.x,鼠标.y)then
         self.字体[n]:置颜色(0xFFFFFF00)

              if(引擎.鼠标弹起(KEY.LBUTTON))then

              local 索引 = n + self.召唤兽起始索引 - 1

              if self.临时数据.数据[索引] == nil then
                return 0
                else
                self.操作序列=索引
                   if tp.主界面.界面数据[44].本类开关  and self.操作序列~= 0 then

             tp.主界面.界面数据[44]:打开(self.临时数据.数据[self.操作序列],self.操作序列)

             end

              end

              --print(self.操作序列)
              bb改名输入:置文本(self.临时数据.数据[self.操作序列].名称)
              self.技能数量=#self.临时数据.数据[self.操作序列].技能
              self.技能页数=1
              --self.改名数据:清空()
              --self.改名数据:置文本(角色数据[当前角色].召唤兽数据[n].名称)

               if self.临时数据.数据.参战~=self.操作序列 then


                 self.控制按钮.文字="参战"


                else
                 --角色数据[当前角色].召唤兽数据.参战=self.操作序列
                 self.控制按钮.文字="休息"




               end




              end
        else

          self.字体[n]:置颜色(0xFF000000)



         end

      end

 if self.操作序列~=0 then
  -- print(self.操作序列)
     self.动画[self.操作序列]:更新(dt,"静立")
    end

 for n=1,5 do

    self.加点按钮[n]:更新(dt)
    if self.操作序列~=0 and (self.加点按钮[n]:取是否单击() or (引擎.按键按住(KEY.CTRL) and self.加点按钮[n]:取按键事件()))  then --调试模式
      self.发送信息={序号=self.操作序列,参数=self.加点标示[n]}
      --客户端:发送数据(3008,table.tostring(self.发送信息))
      客户端:发送数据(self.操作序列,4,23,self.加点标示[n],1)


      end

   end
 if self.背景:检查点(鼠标.x,鼠标.y) then
      if 引擎.鼠标弹起(键盘符号.右键) then
        tp.主界面.界面数据[44].本类开关=false
          self.本类开关=false
          bb改名输入:置可视(false,"bb改名输入")



         end


    end

end


function 召唤兽界面类:显示(dt,x,y)

 if self:界面重叠() then

  self:拖拽()
end
 local bbs=self.临时数据.数据
 --table.print(bbs)

 local BB=#bbs
 local bb,ls,yl
 self.背景:显示(self.x,self.y)
 self.关闭:显示(self.x+344,self.y+5)
 self.资质按钮:显示(self.x+271,self.y+419)
 self.名称按钮:显示(self.x+310,self.y+215)
 --self.改名数据:显示(55,194)
 self.控制按钮:显示(self.x+310,self.y+187)
 self.属性按钮:显示(self.x+289,self.y+393)
 self.放生按钮:显示(self.x+147,self.y+393)
 self.驯养按钮:显示(self.x+100,self.y+393)
 self.炼妖按钮:显示(self.x+193,self.y+393)
 bb改名输入:置坐标(self.x ,self.y)



 self.文字:显示(self.x+130,self.y+221,#self.临时数据.数据.."/8")
 for n=1,5 do

       self.加点按钮[n]:显示(self.x+309,self.y+219+n*24)
   end




  tp.画线:置区域(0,0,15,159)--滑块虚线
  tp.画线:显示(self.x+171,self.y+34)

 -- if BB > 4 then
 --    self.资源组[9]:置高度(min(floor(143/(BB-4)),30))
 --    self.加入 = min(ceil((BB-4)*self.资源组[9]:取百分比()),BB-4)
 --  end
  self.下滚动:更新()
  self.上滚动:更新()

  self.下滚动:显示(self.x+167,self.y+193)
  self.上滚动:显示(self.x+167,self.y+28)
  if self.上滚动:取是否单击() then
    self.召唤兽起始索引 = self.召唤兽起始索引 - 1
    if self.召唤兽起始索引 < 1 then
      self.召唤兽起始索引 = 1
    end
  end

  if self.下滚动:取是否单击() then
    self.召唤兽起始索引 = self.召唤兽起始索引 + 1
    if self.召唤兽起始索引 > 8 then
      self.召唤兽起始索引 = 8
    end
  end

  -- self.资源组[2]:更新(self.x+167,self.y+31,self.加入 > 0)
  -- self.资源组[3]:更新(self.x+167,self.y+193,self.加入 < BB - 4)
  -- self.资源组[2]:显示(self.x+167,self.y+31)
  -- self.资源组[3]:显示(self.x+167,self.y+193)
 -- self.资源组[4]:显示(self.x+12,self.y+29)
  self.资源组[5]:显示(self.x+197,self.y+28)
  self.资源组[6]:显示(self.x+197,self.y+217)



 _GUI:显示(鼠标.x,鼠标.y)


    for n=1,#self.包围盒 do
      if self.操作序列==n and n >= self.召唤兽起始索引 and n - self.召唤兽起始索引 < 4 then
        local i = n - self.召唤兽起始索引 + 1
        self.选中背景:显示(self.x+16,self.y-12+i*44)
      end
      --self.字体[n]:显示(self.x+18,self.y+30+n*20,self.名称[n])
      self.包围盒[n]:置坐标(self.x+18,self.y-12+n*44)

    end

  if BB > 4 then
    self.资源组[9]:显示(self.x+168,self.y+55,self.x,self.y,self.鼠标)    --滚动条
  end
  zts:置颜色(-16777216)

      for i=1,BB do
      if bbs[i].参战信息 ~= nil then
        if i > 4 then
          self.加入 = i-4
          self.资源组[9]:置起始点(self.资源组[9]:取百分比转换(self.加入,4,#bbs))
        end
        -- self.选中 = i
        -- self.名称输入框:置文本(tp.队伍[1].宝宝列表[self.选中].名称)
        -- self.名称输入框:置可视(true,true)
        -- if tp.窗口.召唤兽资质栏.可视 then
        --   tp.窗口.召唤兽资质栏:打开(tp.队伍[1].宝宝列表[self.选中])
        -- end
        -- self:置形象()
        -- self.拽拖计次 = 0
        -- break
      end
    end






for i=self.召唤兽起始索引, self.召唤兽起始索引 + 3 do


    if bbs[i+self.加入] ~= nil then
    local j = i + self.加入 - self.召唤兽起始索引 + 1
      local jx = self.x+18
      local jy = self.y+(j*43)-6
      bw:置坐标(jx,jy+1)            ---包围盒
      if self.头像组[i+self.加入] == nil then

    local n = 引擎.取头像(bbs[i+self.加入].造型)

         self.头像组[i+self.加入] = 引擎.场景.资源:载入(n[4],"网易WDF动画",n[3])


      end
      --拽拖
      local xz = bw:检查点(self.x,self.y)
      if not self.资源组[9].接触 and xz and self.鼠标 and not tp.消息栏焦点 and self.拽拖对象 == 0 then
        if mousea(0) then
          self.拽拖计次 = self.拽拖计次 + 1
          if self.拽拖计次 >= 28 then
            self.拽拖对象 = i+self.加入
            self.拽拖事件 = {self.头像组[i+self.加入]}
            self.拽拖计次 = 0
          end
        end
      end

      if self.选中 ~= i+self.加入 then
        if not self.资源组[9].接触 and xz and not tp.消息栏焦点 and self.鼠标 then
          if self.拽拖对象 ~= 0 then
            box(jx+70,jy+34,jx+125,jy+39,-16777216)
            self.插入选区 = i+self.加入 -- self.召唤兽起始索引 + 1   --?????????
          end
          if mouseb(0) and self.拽拖对象 == 0 then
            self.选中 = i+self.加入 - self.召唤兽起始索引 + 1     --?????????
            -- self.名称输入框:置文本(bbs[self.选中].名称)
            -- self.名称输入框:置可视(true,true)
            -- if tp.窗口.召唤兽资质栏.可视 then
            --   tp.窗口.召唤兽资质栏:打开(bbs[self.选中])
            -- end
           -- self:置形象()
            self.拽拖计次 = 0
          end
          self.焦点 = true
        end
      else
        if not self.资源组[9].接触 and xz and not tp.消息栏焦点 and self.鼠标 then
          self.焦点 = true
        end
        if self.拽拖对象 ~= i+self.加入 then
          box(jx-1,jy-3,jx+142,jy+41,-10790181)
        end
      end
      self.资源组[10]:显示(jx+1,jy)               -----小头像框
      self.头像组[i+self.加入]:显示(jx+4,jy+4)
      if bbs[i+self.加入].参战信息 ~= nil then
        zts:置颜色(-256)
      else
        zts:置颜色(-16777216)
      end
      zts:显示(jx+41,jy+3,bbs[i+self.加入].名称)
      zts:显示(jx+41,jy+21,bbs[i+self.加入].等级.."级")
    end
  end

-- end
-- end
  if self.拽拖对象 ~= 0 then
    box(x-70,y-25,x+73,y+19,-849650981)
    self.资源组[10]:显示(x-67,y-22)
    self.拽拖事件[1]:显示(x-63,y-18)
    if bbs[self.拽拖对象].参战信息 ~= nil then
      zts:置颜色(-256)
    end
    zts:显示(x-27,y-19,bbs[self.拽拖对象].名称)
    zts:显示(x-27,y,bbs[self.拽拖对象].等级.."级")
    if mouseb(0) then
      if self.鼠标 then
        if self.插入选区 == 0 then
          self.拽拖计次 = 0
          self.拽拖对象 = 0
          self.拽拖事件 = 0
          self.插入选区 = 0
          tp.禁止关闭 = false
        else
          self:排列(self.拽拖对象,self.插入选区)
          self.拽拖计次 = 0
          self.拽拖对象 = 0
          self.拽拖事件 = 0
          self.插入选区 = 0
          tp.禁止关闭 = false
        end
      else
        self.拽拖计次 = 0
        self.拽拖对象 = 0
        self.拽拖事件 = 0
        self.插入选区 = 0
        tp.禁止关闭 = false
      end
    end
    tp.禁止关闭 = true
    self.焦点 = true
  end
  if self.资源组[9].接触 then
    self.焦点 = true
  end


  if self.操作序列~=0 then
    if self.临时数据.数据[self.操作序列].饰品 then
     self.圣兽丹动画:显示(self.x+200,self.y+155)
   end

       引擎.场景.影子:显示(self.x+275,self.y+140)
     self.动画[self.操作序列]:显示("静立",self.x+275,self.y+140)
     self.文字:显示(self.x+265,self.y+194,取召唤兽信息(self.临时数据.数据[self.操作序列].造型,"参战等级"))


     self.属性:显示(self.x+208,self.y+246,self.临时数据.数据[self.操作序列].体质)
     self.属性:显示(self.x+62, self.y+245,self.临时数据.数据[self.操作序列].当前气血.."/"..self.临时数据.数据[self.操作序列].气血上限)
     self.属性:显示(self.x+62, self.y+270,self.临时数据.数据[self.操作序列].当前魔法.."/"..self.临时数据.数据[self.操作序列].魔法上限)
     self.属性:显示(self.x+62, self.y+318,self.临时数据.数据[self.操作序列].防御)
     self.属性:显示(self.x+208,self.y+270,self.临时数据.数据[self.操作序列].魔力)
     self.属性:显示(self.x+62, self.y+294,self.临时数据.数据[self.操作序列].伤害)
     self.属性:显示(self.x+62, self.y+342,self.临时数据.数据[self.操作序列].速度)
     self.属性:显示(self.x+62, self.y+367,self.临时数据.数据[self.操作序列].灵力)
     self.属性:显示(self.x+208,self.y+294,self.临时数据.数据[self.操作序列].力量)
     self.属性:显示(self.x+208,self.y+318,self.临时数据.数据[self.操作序列].耐力)
     self.属性:显示(self.x+208,self.y+342,self.临时数据.数据[self.操作序列].敏捷)
     self.属性:显示(self.x+203,self.y+365,self.临时数据.数据[self.操作序列].潜能)
     if self.临时数据.数据[self.操作序列].进阶属性.力量 > 0 then
      self.属性:置颜色(0xFFFF0099)
      self.属性:显示(self.x + 264,self.y + 223+1*24,"+"..self.临时数据.数据[self.操作序列].进阶属性.体质)
      self.属性:置颜色(-16777216)
     end

      if self.临时数据.数据[self.操作序列].进阶属性.敏捷 > 0 then
      self.属性:置颜色(0xFFFF0099)
      self.属性:显示(self.x + 264,self.y + 223+2*24,"+"..self.临时数据.数据[self.操作序列].进阶属性.魔力)
      self.属性:置颜色(-16777216)
     end
          if self.临时数据.数据[self.操作序列].进阶属性.耐力 > 0 then
      self.属性:置颜色(0xFFFF0099)
      self.属性:显示(self.x + 264,self.y + 223+3*24,"+"..self.临时数据.数据[self.操作序列].进阶属性.力量)
      self.属性:置颜色(-16777216)
     end
          if self.临时数据.数据[self.操作序列].进阶属性.体质 > 0 then
      self.属性:置颜色(0xFFFF0099)
      self.属性:显示(self.x + 264,self.y + 223+4*24,"+"..self.临时数据.数据[self.操作序列].进阶属性.耐力)
      self.属性:置颜色(-16777216)
     end
          if self.临时数据.数据[self.操作序列].进阶属性.魔力 > 0 then
      self.属性:置颜色(0xFFFF0099)
      self.属性:显示(self.x + 264,self.y + 223+5*24,"+"..self.临时数据.数据[self.操作序列].进阶属性.敏捷)
      self.属性:置颜色(-16777216)
     end
     -- self.属性:显示(self.x+90, self.y+362,self.临时数据.数据[self.操作序列].当前经验)
     -- self.属性:显示(self.x+90, self.y+385,self.临时数据.数据[self.操作序列].升级经验)

    self.资源组[1]:置区域(0,0,min(floor(self.临时数据.数据[self.操作序列].当前经验 / self.临时数据.数据[self.操作序列].升级经验 * 176),176),16)
    self.资源组[1]:显示(self.x + 60,self.y + 426)
    self.属性:显示(self.x+120,self.y+427,self.临时数据.数据[self.操作序列].当前经验.."/"..self.临时数据.数据[self.操作序列].升级经验)
    self.属性:显示(self.x+58, self.y+395,math.floor(self.临时数据.数据[self.操作序列].忠诚))


     end
end
 function 召唤兽界面类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end
function 召唤兽界面类:释放()
end

return 召唤兽界面类