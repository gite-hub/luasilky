local 推广员填写类 = class()

function 推广员填写类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/267-1.png")
	self.说明字体 = 文字类.创建(simsun, 14):置颜色(绿色)
	self.给予按钮 = 按钮类.创建("imge/001/0016.png", 2, "提交", 3, 1)
	self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3, 1)
end

function 推广员填写类:刷新(dt)
	self.本类开关 = true

	推广号输入:置可视(true, "推广号输入")
	推广号输入:置文本("")
end

function 推广员填写类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 推广员填写类:更新(dt)
	self.给予按钮:更新(265, 249)
	self.取消按钮:更新(405, 249)

	if self.取消按钮:取是否单击() then
		self.本类开关 = false

		推广号输入:置可视(false, "推广号输入")
	elseif self.给予按钮:取是否单击() then
		if 推广号输入:取文本() == "" then
			信息提示:加入提示("#y/请先填写推广员号")
		else
			客户端:发送数据(4048, 推广号输入:取文本())
		end
	end
end

function 推广员填写类:显示(x, y)
	self.背景:显示(100, 150)
	self.说明字体:显示(115, 182, "若不知晓推广员号，可填写0。等级达到50级后，该推广员自动获得推广奖励。")
	self.给予按钮:显示(265, 249)
	self.取消按钮:显示(405, 249)
	_GUI:显示(鼠标.x, 鼠标.y)
end

return 推广员填写类
