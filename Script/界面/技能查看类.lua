local 技能查看类 = class()

function 技能查看类:初始化()
  self.x,self.y = 0,0
 self.背景1=图像类("imge/001/wzife_310-1.png")
 self.背景2=图像类("imge/001/wzife_413-1.png")
 self.背景3=图像类("imge/001/wzife_2996-1.png")
 self.关闭=按钮类.创建("imge/001/0046.png",1,"升级",3)
 self.本类开关=false
 self.显示类型=0
 self.超丰富文本=require("丰富文本类")(140,270)
 self.超丰富文本:添加元素("w",0xFFFFFFFF)
 self.超丰富文本:添加元素("h",0xFF000000)
 self.超丰富文本:添加元素("y",0xFFFFFF00)
 self.超丰富文本:添加元素("r",0xFFFF0000)
 self.超丰富文本:添加元素("g",0xFF00FF00)
 self.超丰富文本:添加元素("l",蓝色)
 self.文字=文字类.创建(simsun,14)
 self.文字:置颜色(黑色)
 self.文字1=文字类.创建(simsun,16)
 self.文字1:置颜色(黑色)
 self.选中背景=图像类("imge/001/125-1.png")
 self.选中编号=0
 self.门派技能={}
 self.生活技能={}
 self.辅助技能={}
 self.师门按钮=按钮类.创建("imge/001/0015.png",2,"师门技能",3)
 self.辅助按钮=按钮类.创建("imge/001/0015.png",2,"辅助技能",3)
 self.修炼按钮=按钮类.创建("imge/001/0015.png",2,"修炼技能",3)
 self.奇经八脉=按钮类.创建("imge/001/0015.png",2,"奇经八脉",3)
 self.炼药=按钮类.创建("imge/001/0016.png",2,"炼药",3)
 self.烹饪=按钮类.创建("imge/001/0016.png",2,"烹饪",3)
 self.摆摊=按钮类.创建("imge/001/0016.png",2,"摆摊",3)
end



function 技能查看类:刷新(数据)

 self.显示类型=1
 self.本类开关=true
 self.选中编号=0
 self.人物修炼=数据.人物修炼
 self.召唤兽修炼=数据.召唤兽修炼

 for n=1,#数据.门派技能 do



  local  xsd = 引擎.取技能(数据.门派技能[n].名称)

      self.门派技能[n]={}
      self.门派技能[n].等级=数据.门派技能[n].等级
      self.门派技能[n].名称=数据.门派技能[n].名称
      self.门派技能[n].图标=tp.资源:载入(xsd[6],"网易WDF动画",xsd[7])
      self.门派技能[n].小模型 = tp.资源:载入(xsd[6],"网易WDF动画",xsd[8])

     end



 self.辅助技能={}
   for n, v in pairs(数据.生活技能) do
     if n~="经验" and n~="储备" and n~="银两" and n~="方式" and n~="编号" and n~="存银" then
      self.辅助技能[#self.辅助技能+1]={}
      self.辅助技能[#self.辅助技能].等级=v
      self.辅助技能[#self.辅助技能].名称=n

       local  xsd = 引擎.取技能(n)
      self.辅助技能[#self.辅助技能].图标= tp.资源:载入(xsd[6],"网易WDF动画",xsd[7])

  end

     end

     for n, v in pairs(数据.辅助技能) do
     if n~="经验" and n~="储备" and n~="银两" and n~="方式" and n~="编号" and n~="存银"  then
      self.辅助技能[#self.辅助技能+1]={}
      self.辅助技能[#self.辅助技能].等级=v
      self.辅助技能[#self.辅助技能].名称=n
      self.临时编号=引擎.读ini("0",n)
        local  xsd = 引擎.取技能(n)
      self.辅助技能[#self.辅助技能].图标= tp.资源:载入(xsd[6],"网易WDF动画",xsd[7])
     end
     end

  for n, v in pairs(数据.强化技能) do

     if n=="伤害强化" or n=="灵力强化" or n=="防御强化" or n=="速度强化"  or n=="固伤强化"then
      self.辅助技能[#self.辅助技能+1]={}
      self.辅助技能[#self.辅助技能].等级=v
      self.辅助技能[#self.辅助技能].名称=n
        local  xsd = 引擎.取技能(n)
      self.辅助技能[#self.辅助技能].图标=tp.资源:载入(xsd[6],"网易WDF动画",xsd[7])
     end
     end
end



function 技能查看类:界面重叠()

 return self:取界面焦点()

end

function 技能查看类:取界面焦点()

 if self.显示类型==1 and self.背景1:取包围盒():检查点(鼠标.x,鼠标.y) then
     return true
    elseif self.显示类型==2 and self.背景2:取包围盒():检查点(鼠标.x,鼠标.y) then
     return true
    elseif self.显示类型==3 and self.背景3:取包围盒():检查点(鼠标.x,鼠标.y) then
     return true

   end
 return false
end

 function 技能查看类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end


function 技能查看类:更新(dt)
  self.师门按钮:更新(235,100)
  self.辅助按钮:更新(360,100)
  self.修炼按钮:更新(485,100)
  self.奇经八脉:更新()
  self.炼药:更新(281,508)
  self.烹饪:更新(221,508)
  self.关闭:更新(221,508)
  self.摆摊:更新()
  if self.师门按钮:取是否单击() then

     self.显示类型=1
    elseif self.辅助按钮:取是否单击() then
     self.显示类型=2
    elseif self.修炼按钮:取是否单击() then
     self.显示类型=3
    elseif self.奇经八脉:取是否单击() then
      客户端:发送数据(1,1,95,"1",1)
      -- self.本类开关=false
    elseif self.烹饪:取是否单击() then
     客户端:发送数据(56,29,13,"97",1)

    elseif self.炼药:取是否单击() then
     客户端:发送数据(65,28,13,"97",1)
    elseif self.关闭:取是否单击() or (self:取界面焦点() and 引擎.鼠标弹起(右键)) then
     self.本类开关=false
    elseif self.摆摊:取是否单击() then
     客户端:发送数据(2647,1,44,"1",1)
     end
end


function 技能查看类:显示(x,y)

 if self:界面重叠() then

  self:拖拽()
end


 if self.显示类型==1 then

     self.背景1:显示(self.x+180,self.y+70)
     self.师门按钮:显示(self.x+215,self.y+100)
     self.辅助按钮:显示(self.x+300,self.y+100)
     self.修炼按钮:显示(self.x+385,self.y+100)
     self.奇经八脉:显示(self.x+470,self.y+100)
     self.炼药:显示(self.x+279,self.y+505)
     self.烹饪:显示(self.x+219,self.y+505)
     self.摆摊:显示(self.x+339,self.y+505)
     self.关闭:显示(self.x+608,self.y+75)
    -- self.下列排序=1
     for n=1,#self.门派技能 do

          if n>7 then

             self.下列排序=2
             self.上列排序=n-7
            else
             self.下列排序=1
             self.上列排序=n

             end

          self.门派技能[n].图标:显示(self.x+207+60*self.上列排序-60,self.y+295+self.下列排序*50)
          if self.选中编号==n then

             --self.选中背景:显示(207+60*self.上列排序-60,295+self.下列排序*50)

             end




          if self.门派技能[n].图标:取包围盒():检查点(鼠标.x,鼠标.y) then

          if  引擎.鼠标弹起(左键) then

             self.选中编号=n
             self.超丰富文本:清空()
             self.临时文本="#y/      "..self.门派技能[n].名称..'\n'.."#h/"..取技能信息(self.门派技能[n].名称,"说明")..[[

#g/使用等级：]]..取技能信息(self.门派技能[n].名称,"等级")..'\n'.."#l/当前等级: "..self.门派技能[n].等级..[[

#r/使用条件：]]..取技能信息(self.门派技能[n].名称,"条件")
             self.超丰富文本:添加文本(self.临时文本)



          elseif 引擎.鼠标按住(左键)   then
            tp.主界面.抓取技能 =self.门派技能[n]
            tp.主界面.抓取技能id =n





             end
             end


       end

     self.超丰富文本:显示(self.x+429,self.y+143)

    elseif self.显示类型==2 then

     self.背景2:显示(self.x+180,self.y+70)
     self.师门按钮:显示(self.x+215,self.y+100)
     self.辅助按钮:显示(self.x+300,self.y+100)
     self.修炼按钮:显示(self.x+385,self.y+100)
     self.奇经八脉:显示(self.x+470,self.y+100)
     self.炼药:显示(self.x+279,self.y+505)
     self.烹饪:显示(self.x+219,self.y+505)
     self.摆摊:显示(self.x+339,self.y+505)
     self.关闭:显示(self.x+608,self.y+75)
      for n=1,#self.辅助技能 do

          if n>7 then

             self.下列排序=2
             self.上列排序=n-7
            else
             self.下列排序=1
             self.上列排序=n

             end

          self.辅助技能[n].图标:显示(self.x+194+60*self.上列排序-60,self.y+295+self.下列排序*75-25)
          self.文字:显示(self.x+200+60*self.上列排序-50,self.y+295+self.下列排序*75-25+52,self.辅助技能[n].等级)
          if self.辅助技能[n].图标:取包围盒():检查点(鼠标.x,鼠标.y) and 引擎.鼠标弹起(左键) then

             self.选中编号=n
             self.超丰富文本:清空()
             self.临时文本="#y/      "..self.辅助技能[n].名称..'\n'.."#h/"..取技能信息(self.辅助技能[n].名称,"说明")
             self.超丰富文本:添加文本(self.临时文本)

             end

            self.超丰富文本:显示(self.x+429,self.y+143)


          end
    elseif self.显示类型==3 then

     self.背景3:显示(self.x+180,self.y+70)
     self.师门按钮:显示(self.x+215,self.y+100)
     self.辅助按钮:显示(self.x+300,self.y+100)
     self.修炼按钮:显示(self.x+385,self.y+100)
     self.奇经八脉:显示(self.x+470,self.y+100)
     self.摆摊:显示(self.x+339,self.y+505)
     self.关闭:显示(self.x+608,self.y+75)
     self.炼药:显示(self.x+279,self.y+505)
     self.烹饪:显示(self.x+219,self.y+505)
     self.文字1:显示(self.x+227,self.y+190,"攻击修炼     等级: "..self.人物修炼.攻击.等级.."    修炼经验:"..self.人物修炼.攻击.经验..[[/]]..self:计算修炼等级经验(self.人物修炼.攻击.等级,30))
     self.文字1:显示(self.x+227,self.y+210,"防御修炼     等级: "..self.人物修炼.防御.等级.."    修炼经验:"..self.人物修炼.防御.经验..[[/]]..self:计算修炼等级经验(self.人物修炼.防御.等级,30))
     self.文字1:显示(self.x+227,self.y+230,"法术修炼     等级: "..self.人物修炼.法术.等级.."    修炼经验:"..self.人物修炼.法术.经验..[[/]]..self:计算修炼等级经验(self.人物修炼.法术.等级,30))
     self.文字1:显示(self.x+227,self.y+250,"抗法修炼     等级: "..self.人物修炼.法抗.等级.."    修炼经验:"..self.人物修炼.法抗.经验..[[/]]..self:计算修炼等级经验(self.人物修炼.法抗.等级,30))
     self.文字1:显示(self.x+227,self.y+270,"猎术修炼     等级: "..self.人物修炼.猎术.等级.."    修炼经验:"..self.人物修炼.猎术.经验..[[/]]..self:计算修炼等级经验(self.人物修炼.猎术.等级,30))


     self.文字1:显示(self.x+227,self.y+360,"攻击修炼     等级: "..self.召唤兽修炼.攻击.等级.."    修炼经验:"..self.召唤兽修炼.攻击.经验..[[/]]..self:计算修炼等级经验(self.召唤兽修炼.攻击.等级,30))
     self.文字1:显示(self.x+227,self.y+380,"防御修炼     等级: "..self.召唤兽修炼.防御.等级.."    修炼经验:"..self.召唤兽修炼.防御.经验..[[/]]..self:计算修炼等级经验(self.召唤兽修炼.防御.等级,30))
     self.文字1:显示(self.x+227,self.y+400,"法术修炼     等级: "..self.召唤兽修炼.法术.等级.."    修炼经验:"..self.召唤兽修炼.法术.经验..[[/]]..self:计算修炼等级经验(self.召唤兽修炼.法术.等级,30))
     self.文字1:显示(self.x+227,self.y+420,"抗法修炼     等级: "..self.召唤兽修炼.法抗.等级.."    修炼经验:"..self.召唤兽修炼.法抗.经验..[[/]]..self:计算修炼等级经验(self.召唤兽修炼.法抗.等级,30))



   end

end
function 技能查看类:计算修炼等级经验(等级,上限)

 if 等级==0 then return 110 end

 self.临时经验=110
  for n=1,上限+1 do

   self.临时经验=self.临时经验+20+n*20
   if n==等级 then return self.临时经验 end


    end




 end
return 技能查看类
