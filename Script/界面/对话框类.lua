local 对话框类 = class()

function 对话框类:初始化()
	self.本类开关 = false
	self.字体 = 文字类.创建("宋体", 14)
	self.背景 = 图像类("imge/001/302-1.png")
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, 1, 3)
	self.名称字体 = 文字类(simsun, 14)
    self.背景1 =WAS动画类.创建(addon,0xEBD0E004)
	self.名称字体:置颜色(黄色)

	self.名称内容 = "???"
	self.答题字体 = 文字类.创建("宋体", 15)

	self.答题字体:置颜色(绿色)

	self.对话人物 = {}
	self.超级文本 = require("丰富文本类")(510, 270)
	:置文字(文字类(simsun, 16))
	-- self.超级文本 = require("丰富文本类")(460, 270)
	-- self.超级文本:置行距(10)
	self.超级文本:添加元素("W", 4294967295.0)
	self.超级文本:添加元素("H", 黄色)
	self.超级文本:添加元素("Y", 4294967040.0)
	self.超级文本:添加元素("R", 4294901760.0)
	self.超级文本:添加元素("G", 4278255360.0)


	self.选择框 = 资源:载入("wzife.wd4","网易WDF动画",0x802EB60A)
end

function 对话框类:添加对话(数据)

local tx= 引擎.取头像(数据.编号)
	if 数据.编号 ~= nil and tx ~= false then


		self.头像 =   引擎.场景.资源:载入(tx[4],"网易WDF动画",tx[1])
		self.头像大小 = {
			x = self.头像:取宽度(),
			y = self.头像:取高度()
		}
	else
		self.头像 = nil
	end

	self.超级文本:清空()
	self.超级文本:添加文本(数据.对话)

	self.名称内容 = 数据.名称
	self.本类开关 = true --继续
end

function 对话框类:更新(dt)
	-- local x = 引擎.是否锁定(0)
	-- 引擎.重置锁定(0)
	-- 引擎.重置锁定(1)
	-- print("对话框")
	self.超级文本:更新(dt, 鼠标.x, 鼠标.y)
	if 引擎.鼠标弹起(2) then self.超级文本:取回调包围盒() end
	if self.背景:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(键盘符号.右键) then
		self.本类开关 = false
		x = true
	end

	self.关闭:更新(dt)

	if self.关闭:取是否单击() then
		self.本类开关 = false
		x = true
	end
	-- if(x) then 引擎.鼠标锁定(0) end
end

function 对话框类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 对话框类:显示(x, y)
	if self.头像 ~= nil then
		self.头像:显示(120, 260 - self.头像大小.y)
	end
    self.背景1:显示(90, 220)
	self.背景:显示(90, 250)
	self.名称字体:显示(130, 230, "" .. self.名称内容 .. "")
	self.超级文本:显示(105, 275 - 8)

	-- self.选择框:显示(100, 275)
end

return 对话框类
