-- @作者: baidwwy
-- @邮箱:  313738139@qq.com
-- @创建时间:   2019-07-08 15:10:10
-- @最后修改来自: baidwwy
-- @Last Modified time: 2020-03-17 02:08:03
--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2019-06-29 09:36:35
--======================================================================--
local 场景类_召唤兽资质栏 = class()
local zts,tp,zts1
local yx = {{68,4},{25,28},{111,28},{24,75},{111,75},{68,97}}
local insert = table.insert
local bds = {"攻击资质","防御资质","体力资质","法力资质","速度资质","躲闪资质"}
local bds1 = {"寿命","成长","五行"}

local item ={}
function 场景类_召唤兽资质栏:初始化(根)
  self.x,self.y = 374,0
	self.本类开关=false
   tp=根
  local 资源 = tp.资源
  local 按钮 = tp._按钮
  local 自适应 = tp._自适应   --文件,数量,标识,文字
	self.资源组 = {
		[1] = 自适应.创建(0,1,242,453,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3)),
		[3] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x1F996671)),
		[4] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x9C24F376)),
		[5] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xCD999F0B)),
		[6] = 资源:载入('JM.FT',"网易WDF动画",0x68D384BD),
		[7] = 资源:载入('JM.FT',"网易WDF动画",0x7367031D),
		[8] = 资源:载入('JM.FT',"网易WDF动画",0x1E714129),
		[9] = 资源:载入('JM.FT',"网易WDF动画",0xF2FC2425),
		[10] = 资源:载入('JM.FT',"网易WDF动画",0x10E2B4A7),
		[11] = 资源:载入('JM.FT',"网易WDF动画",0xC361C087),
		[12] = 资源:载入("Dat/pic/qy.png","图片"),
		[13] = 自适应.创建(3,1,95,19,1,3),
		[14] = 自适应.创建(1,1,204,18,1,3,nil,18),
		[15] = 资源:载入('JM.FT',"网易WDF动画",0x3FEEB486),


	}
	    self.上页=按钮类.创建("imge/001/0053.png",1,"升级",3)
    self.下页=按钮类.创建("imge/001/0054.png",1,"升级",3)
	self.佩戴背景=图像类("imge/001/720-1.png")
	self.显示编号 = 0
  self.技能名称字体=文字类(simsun,16)
  self.超丰富文本=require("丰富文本类")(240,270)
  self.超丰富文本:添加元素("w",0xFFFFFFFF)
  self.超丰富文本:添加元素("h",0xFF000000)
  self.超丰富文本:添加元素("y",0xFFFFFF00)
  self.超丰富文本:添加元素("r",0xFFFF0000)
  self.超丰富文本:添加元素("g",0xFF00FF00)
   self.技能名称字体:置颜色(黄色)
	self.进阶 ={
        [16] = 资源:载入('JM.FT',"网易WDF动画",0x1094AD16),----进阶界面
 	    [1] = 资源:载入('JM.FT',"网易WDF动画",0x4536A03D),--进阶1
		[2] = 资源:载入('JM.FT',"网易WDF动画",0x714C3706),--2
		[3] = 资源:载入('JM.FT',"网易WDF动画",0xD60014B8),-----3
		[4] = 资源:载入('JM.FT',"网易WDF动画",0xF7EBF987), ----4
		[5] = 资源:载入('JM.FT',"网易WDF动画",0x11963488),----5
		[6] = 资源:载入('JM.FT',"网易WDF动画",0x9A4F1961),--6
		[7] = 资源:载入('JM.FT',"网易WDF动画",0x1E7ABB94), --7
		[8] = 资源:载入('JM.FT',"网易WDF动画",0xA6C9A76A),--8
		[9] = 资源:载入('JM.FT',"网易WDF动画",0x2982E3F7),---9
		[10] = 资源:载入('JM.FT',"网易WDF动画",0x1D0717D7),---91
		[11] = 资源:载入('JM.FT',"网易WDF动画",0xC44F0602),--94
		[12] = 资源:载入('JM.FT',"网易WDF动画",0x9765D0B3),--97
		[13] = 资源:载入('JM.FT',"网易WDF动画",0x36A2C1A6),--100
		[14] = 资源:载入('JM.FT',"网易WDF动画",0xAFC2E161), --未完成内框
		[15] = 资源:载入('JM.FT',"网易WDF动画",0x27E24CFA), --完成
   }
  self.窗口标题背景_=资源:载入('JM.FT',"网易WDF动画",0xC69BF9F3).精灵

	self.物品 = {}
  self.道具 ={}


	for i=2,5 do
	    self.资源组[i]:绑定窗口_(8)
	end
	self.内丹={}

	tp = 根
	zts = 文字类(simsun, 16) --tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.状态 = 1
	self.窗口时间 = 0
	self.数据 ={}
			self.技能数量 =0
		self.技能页数 =1
		self.焦点1 =false

end

function 场景类_召唤兽资质栏:打开(b,编号)

	if self.本类开关 then
		if b ~= nil and  b ~= bb then
			self.数据 =b
			self.技能=self.数据.技能
			self.技能数量=#self.数据.技能
			  self.道具 ={}
			  self.技能页数 = 1
        if self.数据.道具数据~=nil then
		for i=1,3 do
		if self.数据.道具数据[i] ~= nil then
		self.道具[i]=  物品数据类.创建(self.数据.道具数据[i],99,编号,i)
		end
		end
		end


       	for i=1,6 do
			if self.数据.内丹[i] ~= nil and self.数据.内丹[i].技能 ~= nil then
			self.内丹[i] =self.数据.内丹[i]
			item = self:取内丹数据(self.数据.内丹[i].技能,self.数据.内丹[i].等级)
			self.内丹[i].说明= item.说明
			self.内丹[i].效果 = item.效果
			self.内丹[i].模型 = 引擎.场景.资源:载入(item.资源,"网易WDF动画",item.模型)
			self.内丹[i].小模型 = 引擎.场景.资源:载入(item.资源,"网易WDF动画",item.模型)
			else
			self.内丹[i]=nil

			end

       end
			return
		end
		self.技能数量 =0
		self.技能页数 =1
		self.本类开关 = false
		  self.道具 ={}
	else
		self.数据 =b
		self.技能=self.数据.技能
		if self.数据.道具数据~=nil then
		for i=1,3 do
		if self.数据.道具数据[i] ~= nil then
		self.道具[i]=  物品数据类.创建(self.数据.道具数据[i],99,编号,i)
		end
		end
		end
		     self.技能数量=#self.数据.技能

       	for i=1,6 do
			if self.数据.内丹[i] ~= nil and self.数据.内丹[i].技能 ~= nil then
			self.内丹[i] =self.数据.内丹[i]


			item = self:取内丹数据(self.数据.内丹[i].技能,self.数据.内丹[i].等级)
			self.内丹[i].说明= item.说明
			self.内丹[i].效果 = item.效果
			self.内丹[i].模型 = 引擎.场景.资源:载入(item.资源,"网易WDF动画",item.模型)
			self.内丹[i].小模型 = 引擎.场景.资源:载入(item.资源,"网易WDF动画",item.模型)
			else
			self.内丹[i]=nil

			end

        end



		self.状态 = 1
	    self.本类开关 = true
	end
end

function 场景类_召唤兽资质栏:更新(dt)

	-- 引擎.重置锁定(1)
	for i=1,3 do
		if self.道具[i] ~= nil  then
			if self.道具[i]:获取焦点()  then
			 self.焦点1 = true
			else
			--self.焦点1 =false
			end
		end
	end

 self.焦点 = false
	self.资源组[2]:更新(鼠标.x,鼠标.y)
	self.资源组[3]:更新(鼠标.x,鼠标.y,self.状态 ~= 1)
	self.资源组[4]:更新(鼠标.x,鼠标.y,self.状态 ~= 2)
	self.资源组[5]:更新(鼠标.x,鼠标.y,self.状态 ~= 3)

    self.资源组[15]:更新(dt)
	if self.资源组[2]:事件判断() or( 引擎.鼠标弹起(1) and self:界面重叠()) and self.焦点1 ==false then -- and self.焦点1 ==false
		self.本类开关= false
	elseif self.资源组[3]:事件判断() then
		self.状态 = 1
	elseif self.资源组[4]:事件判断() then
		self.状态 = 2
	elseif self.资源组[5]:事件判断() then
		self.状态 = 3
	end


end
 function 场景类_召唤兽资质栏:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end
function 场景类_召唤兽资质栏:显示(x,y)
	if self.资源组[1]:是否选中(鼠标.x,鼠标.y) then

	self:拖拽()
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[14]:显示(self.x+6,self.y+3)
	self.窗口标题背景_:置区域(0,0,84,16)
	self.窗口标题背景_:显示(self.x+71,self.y+3)
	zts1:置字间距(1)
	zts1:显示(self.x+76,self.y+3,"召唤兽资质")
	zts1:置字间距(0)
	self.资源组[12]:显示(self.x+30,self.y+27)
	zts1:置字间距(10)
	for i=0,5 do
		zts1:显示(self.x+23,self.y+98+i*23,bds[i+1])
		self.资源组[13]:显示(self.x+124,self.y+96+i*23)
	end
	zts1:置字间距(58)
	zts1:置颜色(-1404907)
	for i=0,2 do
		zts1:显示(self.x+23,self.y+236+i*23,bds1[i+1])
		self.资源组[13]:显示(self.x+124,self.y+234+i*23)
	end
	zts1:置颜色(4294967295)
	zts1:置字间距(0)
	self.资源组[2]:显示(self.x+216,self.y+5)
	self.资源组[3]:显示(self.x+208,self.y+305,true,nil,nil,self.状态 == 1,2)
	self.资源组[4]:显示(self.x+208,self.y+351,true,nil,nil,self.状态 == 2,2)
	self.资源组[5]:显示(self.x+208,self.y+397,true,nil,nil,self.状态 == 3,2)

     self.显示编号=0

              for i=1,3 do


           if self.道具[i]~=nil then
              if self.道具[i].名称=="七星宝甲" then
               self.装备坐标={x=self.x+157,y=self.y+31}
               self.背景坐标={x=self.x+157,y=self.y+31}
              elseif self.道具[i].名称=="嵌宝金腕" then
               self.装备坐标={x=self.x+35,y=self.y+32}
               self.背景坐标={x=self.x+35,y=self.y+32}
              elseif self.道具[i].名称=="冰蚕丝圈" then
               self.装备坐标={x=self.x+96,y=self.y+31}
               self.背景坐标={x=self.x+96,y=self.y+31}
               end
               self.佩戴背景:显示(self.背景坐标.x,self.背景坐标.y)
              self.道具[i]:显示(self.装备坐标.x,self.装备坐标.y)

              if self.道具[i]:获取焦点()  then

               self.显示编号=i
                end

             end

           end
           if self.显示编号~=0 then
             self.道具[self.显示编号]:显示事件()
             end




	local xx = 0
	local yy = 0

	if self.状态 == 1 then
		for i=1,12 do
			local jx = self.x+20+(xx*41)
			local jy = self.y+309+(yy*41)
			self.资源组[6]:显示(jx,jy)

			xx = xx + 1
			if xx > 3 then
				xx = 0
				yy = yy + 1
			end
		end




---------------
         self.b=0
         if self.技能数量>12 then
             self.显示数量=12
			if self.技能页数==1 then
				self.下页:更新()
				self.下页:显示(self.x+192,self.y+406)
				if self.下页:取是否单击() then
				self.技能页数=self.技能页数+1
				end
		  else
				self.上页:更新()
				self.上页:显示(self.x+192,self.y+326)
				if self.上页:取是否单击() then
				self.技能页数=self.技能页数-1
				end

			end

          else

            self.显示数量=self.技能数量


           end
          for n=1,self.显示数量 do

             self.a=n/4

         if(self.a<1)then
                  self.a=1
               elseif(self.a>1 and self.a<2)then
                  self.a=2
               elseif(self.a>2 and self.a<3)then
                  self.a=3

               end

                 self.b=self.b+1
                 if(self.b==5)then
                     self.b=1

                     end
               self.显示序列=self.技能页数*12-12+n
               if self.显示序列>self.技能数量 then return 0 end

              技能.召唤兽.图标[召唤兽技能信息(self.技能[self.显示序列],"编号")]:显示(self.x+22+self.b*41-41,self.y+310+self.a*41-41)
              if(技能.召唤兽.图标[召唤兽技能信息(self.技能[self.显示序列],"编号")]:检查点(鼠标.x,鼠标.y))then
                   信息提示:显示(2,30,10,鼠标.x-35,鼠标.y-120)
                   self.超丰富文本:清空()
                   self.超丰富文本:添加文本("#g/"..召唤兽技能信息(self.技能[self.显示序列],"说明"))
                   self.技能名称字体:显示(鼠标.x+70,鼠标.y-110,self.技能[self.显示序列])
                   self.超丰富文本:显示(鼠标.x-30,鼠标.y-90)
                end


           end

---------------

	elseif self.状态 == 2 then

		self.资源组[7]:显示(self.x+20,self.y+309)
		local v1 = self.数据.内丹.总数
		for i=1,6 do
			local jxx = self.x+20 + yx[i][1]
			local jxy = self.y+309 + yx[i][2]

			if i <= v1 and   self.内丹[i] == nil  then
					self.资源组[9]:显示(jxx,jxy)
		        if self.鼠标 and self.资源组[9]:是否选中(鼠标.x,鼠标.y) then
		        tp.提示:自定义(鼠标.x,鼠标.y,"可以学习的内丹技能格")
		        self.焦点 = true
		       end
			elseif i > v1 and   self.内丹[i] == nil  then
				self.资源组[8]:显示(jxx,jxy)
				if self.资源组[8]:是否选中(鼠标.x,鼠标.y) then
					tp.提示:自定义(鼠标.x,鼠标.y,"不可用内丹技能格,召唤兽可用内丹格数量与其参战等级相关")
					self.焦点 = true
				end
			end
         if  self.内丹[i] ~= nil then
			  self.内丹[i].模型:显示(jxx,jxy)
			  if self.内丹[i].等级<5 then
			      self.资源组[15]:显示(jxx,jxy)
			  end


	   	      if self.内丹[i].模型:是否选中(鼠标.x,鼠标.y) then
			    tp.提示:内丹提示(鼠标.x,鼠标.y,self.内丹[i])
			    self.内丹[i].模型:置高亮()
			  else
			  	self.内丹[i].模型:取消高亮()
			 end
			end
		end
	elseif self.状态 == 3 then
		local jx = self.x+20
		local jy = self.y+309
		self.资源组[11]:显示(jx,jy)
		self.进阶[16]:显示(jx+40,jy+7)
		if self.进阶[16]:是否选中(鼠标.x,鼠标.y) then
			tp.提示:自定义(鼠标.x,鼠标.y,"#W/使用#Y/易经丹#W/可以提升该召唤兽灵性,当灵性到达到50可以获得新的造型")
		end
		if self.数据.灵性>0 and self.数据.灵性<0 then
	    elseif self.数据.灵性>0 and self.数据.灵性<= 10 then
	    	self.进阶[1]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>10 and self.数据.灵性<= 20 then
	    	self.进阶[2]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>20 and self.数据.灵性<= 30 then
	    	self.进阶[3]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>30 and self.数据.灵性<= 40 then
	    	self.进阶[4]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>40 and self.数据.灵性<= 50 then
	    	self.进阶[5]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>50 and self.数据.灵性<= 60 then
	    	self.进阶[6]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>60 and self.数据.灵性<= 70 then
	    	self.进阶[7]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>70 and self.数据.灵性<= 80 then
	    	self.进阶[8]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>80 and self.数据.灵性<= 90 then
	        self.进阶[9]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>90 and self.数据.灵性<= 91 then
	    	self.进阶[10]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>91 and self.数据.灵性<= 93 then
	    	self.进阶[11]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>93 and self.数据.灵性<= 97 then
	        self.进阶[12]:显示(jx+39,jy+6)
	    elseif self.数据.灵性>=98  then
	        self.进阶[13]:显示(jx+39,jy+6)
		end

		if self.数据.灵性>80 and self.数据.特性=="无" then
		self.进阶[14]:显示(jx+51,jy+30)
		elseif self.数据.灵性>80 and self.数据.特性~="无" then
		self.进阶[15]:显示(jx+67,jy+36)
		end

		if self.数据.特性 ~="无" then
		zts1:置颜色(0xFFFFFFA4)
		zts1:显示(jx+74,jy+45,self.数据.特性)
		zts1:置颜色(0xFFFFFFFF)
	    end
	    if (self.进阶[14]:是否选中(鼠标.x,鼠标.y) or self.进阶[15]:是否选中(鼠标.x,鼠标.y)) and self.数据.特性几率~=0 and self.数据.特性~= "无" then
			tp.提示:特性(鼠标.x,鼠标.y,self.数据.特性,self.数据.等级,self.数据.特性几率)
		end

		if self.数据.最高灵性显示 then
			zts1:置颜色(0xFFFFFFFF)
		  zts1:显示(jx+20,jy+115,"灵性:"..self.数据.灵性)
			zts1:置颜色(0xFFFFFFFF)
		 zts1:显示(jx+75,jy+115,"(历史最高值"..self.数据.最高灵性..")")
		elseif  self.数据.灵性>0 then
				zts1:置颜色(0xFFFFFFFF)
		zts1:显示(jx+65,jy+115,"灵性:"..self.数据.灵性)
		end
	end


	zts:置颜色(-16777216)
	zts:显示(self.x+131,self.y+100,self.数据.攻资)
	zts:显示(self.x+131,self.y+123,self.数据.防资)
	zts:显示(self.x+131,self.y+146,self.数据.体资)
	zts:显示(self.x+131,self.y+169,self.数据.法资)
	zts:显示(self.x+131,self.y+192,self.数据.速资)
	zts:显示(self.x+131,self.y+215,self.数据.躲资)


   if self.数据.特殊资质.攻资> 0 then
	zts:置颜色(0xFF8000FF)
	zts:显示(self.x+181,self.y+100,"+"..self.数据.特殊资质.攻资)
	zts:显示(self.x+181,self.y+123,"+"..self.数据.特殊资质.防资)
	zts:显示(self.x+181,self.y+146,"+"..self.数据.特殊资质.体资)
	zts:显示(self.x+181,self.y+169,"+"..self.数据.特殊资质.法资)
	zts:显示(self.x+181,self.y+192,"+"..self.数据.特殊资质.速资)
	zts:显示(self.x+181,self.y+215,"+"..self.数据.特殊资质.躲资)
    zts:置颜色(-16777216)
    end


	if self.数据.神兽 then
		zts:显示(self.x+131,self.y+238,"★永生★")
	else
	zts:显示(self.x+131,self.y+238,self.数据.寿命)
   end
	zts:显示(self.x+131,self.y+261,self.数据.成长)
	zts:显示(self.x+131,self.y+284,self.数据.五行)
end


function 场景类_召唤兽资质栏:取内丹数据(wd,s)
  local wds = {}

    if wd == "迅敏" then
        wds.说明 = "提升召唤兽伤害力与速度，提升效果受召唤兽自身等级影响。"
        wds.效果 = "增加"..(s*50).."点伤害与"..(s*20).."点速度，随内丹等级提升而增加。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x6FA0B3A8
    elseif wd == "狂怒" then
        wds.说明 = "提升必杀/高级必杀技能触发时造成的伤害，但是受到水、土二系法术攻击时将承受额外的伤害。"
        wds.效果 = "必杀时增加"..(s*80).."点伤害，但是额外受到15%水、土系法术伤害。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x956BD457
    elseif wd == "阴伤" then
        wds.说明 = "提升连击/高级连击技能第二次攻击造成的伤害，但是受到火、雷二系法术攻击时将承受额外的伤害。"
        wds.效果 = "连击时增加"..(s*50).."点伤害，但是额外受到15%火、雷系法术伤害。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x3EF0A9BF
    elseif wd == "静岳" then
        wds.说明 = "提升召唤兽灵力与气血，提升效果受召唤兽自身等级影响。"
        wds.效果 = "增加"..(s*32).."点灵力与"..(s*80).."点气血，随内丹等级提升而增加。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xF58C6B1D
    elseif wd == "擅咒" then
        wds.说明 = "你对目标的法术伤害得到提升。"
        wds.效果 = "法术伤害结果增加"..(s*12).."点。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x4D74B795
    elseif wd == "灵身" then
        wds.说明 = "提升法术爆击/高级法术爆击触发时的伤害，但是受到携带强力与高级强力技能召唤兽物理攻击时，承受额外的伤害。"
        wds.效果 = "法术暴击伤害增加"..(s*7).."%，但是受到强力、高强力技能额外伤害。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x55124270
     elseif wd == "矫健" then
        wds.说明 = "提升召唤兽气血与速度，提升效果受召唤兽自身等级影响。"
        wds.效果 = "增加"..(s*120).."点气血与"..(s*15).."点速度，随召唤兽等级提升而增加。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x3877515B
    elseif wd == "深思" then
        wds.说明 = "提升高冥思技能效果。"
        wds.效果 = "高冥思效果增加"..(s*5).."点"
        wds.资源 = "JM.FT"
        wds.模型 = 0x9912B979
    elseif wd == "钢化" then
        wds.说明 = "拥有防御或高级防御技能的召唤兽能提升防御效果，但在受到除固定伤害外的其他法术攻击时，受到的伤害增加。"
        wds.效果 = "防御、高级防御效果增加"..(s*20).."点，随召唤兽等级提升而增加，所受法术伤害增加10%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x89C1F027
    elseif wd == "坚甲" then
        wds.说明 = "拥有反震或高级反震技能的召唤兽能提升对敌人造成的反震伤害。"
        wds.效果 = "对敌人所造成的反震伤害增加"..(s*100).."点。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x2788B6E8
    elseif wd == "慧心" then
        wds.说明 = "提升召唤兽抵抗封印几率。"
        wds.效果 = "增加"..(s*6).."%的抗封印几率。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xB6A27748
    elseif wd == "撞击" then
        wds.说明 = "提升召唤兽物理攻击命中几率，提升效果受召唤兽体质点影响，同时提升一定的伤害结果。"
        wds.效果 = "物理攻击时增加"..(s*5).."%命中几率，效果与召唤兽体质点相关，同时增加5点伤害结果。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xE837F9B1
    elseif wd == "无畏" then
        wds.说明 = "提升对拥有反震/高级反震技能目标的物理伤害。"
        wds.效果 = "对待有反震、高级反震技能的目标造成的物理伤害增加"..(s*2).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xE8FDD3F4
    elseif wd == "愤恨" then
        wds.说明 = "提升召唤兽对拥有幸运/高级幸运技能目标的物理伤害。"
        wds.效果 = "对待有幸运、高级幸运技能的目标造成的物理伤害增加"..(s*2).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x9F97DB7F
    elseif wd == "淬毒" then
        wds.说明 = "提升毒/高级毒技能的中毒触发几率。"
        wds.效果 = "命中目标后致毒的几率增加"..(s*5).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x4A491950
    elseif wd == "狙刺" then
        wds.说明 = "提升召唤兽对施法选定目标的法术伤害，提升效果受召唤兽等级影响"
        wds.效果 = "对施法选定目标法术伤害增加"..(s*35).."点，效果随召唤兽等级提升而增加。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x9762CCF9
    elseif wd == "连环" then
        wds.说明 = "提升连击/高级连击技能触发连击的几率。"
        wds.效果 = "连击的几率增加"..(s*2).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xE2D19F8F
    elseif wd == "圣洁" then
        wds.说明 = "提升驱鬼/高级驱鬼技对于鬼魂/高级鬼魂技能召唤兽的法术伤害"
        wds.效果 = "驱鬼的效果在原基础上增加"..(s*10).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x809E53A3
    elseif wd == "灵光" then
        wds.说明 = "提升召唤兽法术伤害，提升效果受召唤兽自身法力点数影响。"
        wds.效果 = "增加"..(s*30).."点法术伤害，效果受自身法力点数影响。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xC0EAFCA3
    elseif wd == "神机步" then
        wds.说明 = "进入战斗后三回合内提升召唤兽躲避力。"
        wds.效果 = "进入战斗时，"..(s*3).."回合内的躲避值增加100%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x19D18973
    elseif wd == "腾挪劲" then
        wds.说明 = "召唤兽受到物理攻击时，有一定几率化解部分伤害。"
        wds.效果 = "受到物理攻击时有"..(s*3).."%的几率抵挡50%的伤害。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x06EF9E7B
    elseif wd == "玄武躯" then
        wds.说明 = "提升召唤兽气血，提升效果受召唤兽等级影响，但是将减少召唤兽所有攻击方式造成的伤害。"
        wds.效果 = "增加"..(s*30).."点气血，随召唤兽等级提升而增加，对目标造成伤害减少50%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xC842B66C
    elseif wd == "龙胄铠" then
        wds.说明 = "提升召唤兽防御，提升效果受召唤兽等级影响，但是将减少召唤兽所有攻击方式造成的伤害。"
        wds.效果 = "增加"..(s*20).."点防御，随召唤兽等级提升而增加，对目标造成伤害减少50%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x4E9FBEE0
    elseif wd == "玉砥柱" then
        wds.说明 = "降低召唤兽受到壁垒击破技能攻击时所承受的伤害，但是将减少召唤兽所有攻击方式造成的伤害。"
        wds.效果 = "受到壁垒击破技能伤害减少"..(s*7).."%；但是对其他目标造成的伤害减少20%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x5DAD21DF
     elseif wd == "碎甲刃" then
        wds.说明 = "召唤兽普通物理攻击时将有一定几率降低攻击目标的物理防御，效果持续两回合。"
        wds.效果 = "普通物理攻击后减低目标后有30%几率降低目标"..(s*20).."点防御值，持续2回合，效果受自身力量点影响。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xCDDAE9FF
    elseif wd == "阴阳护" then
        wds.说明 = "降低召唤兽保护其他目标时所承受的伤害。"
        wds.效果 = "保护目标时承受的伤害减少"..(s*10).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x9041DD3F
    elseif wd == "凛冽气" then
        wds.说明 = "携带此内丹的召唤兽在战斗中降低本方包含其在内所有召唤兽的逃跑几率。"
        wds.效果 = "本方所有召唤兽逃跑的几率减少"..(s*10).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x38808205
    elseif wd == "舍身击" then
        wds.说明 = "提升物理攻击造成的伤害，提升效果受召唤兽自身力量点数影响。"
        wds.效果 = "物理攻击伤害结果增加"..(s*30).."点，效果受自身力量点影响。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x10E2B4A7
    elseif wd == "电魂闪" then
        wds.说明 = "使用单体法术命中目标时将有一定几率驱散目标的某种增益状态。"
        wds.效果 = "单体法术命中目标时有"..(s*8).."%几率驱散目标随机一种状态。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x624D3F68
    elseif wd == "通灵法" then
        wds.说明 = "提升召唤兽忽视目标法术减免效果的能力。"
        wds.效果 = "忽视"..(s*4).."%的法术伤害减免能力。"
        wds.资源 = "JM.FT"
        wds.模型 = 0xBD739B98
    elseif wd == "双星爆" then
        wds.说明 = "提升法术连击/高级法术连击技能触发第二次法术攻击造成的伤害。"
        wds.效果 = "法术连击第二下伤害增加"..(s*12).."%。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x8AD71FAF
    elseif wd == "催心浪" then
        wds.说明 = "提升法术波动/高级法术波动技能触发时的伤害波动下限。"
        wds.效果 = "法术波动下限提升了。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x5CEDA8EC
    elseif wd == "隐匿击" then
        wds.说明 = "降低拥有高级隐身技能的召唤兽在隐身状态下减少的伤害值，但隐身结束后将消耗额外的魔法。"
        wds.效果 = "隐身状态下降低的伤害降低"..(s*1).."%，同时隐身结束后额外消耗100%的魔法值。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x0A21302B
    elseif wd == "生死决" then
        wds.说明 = "提升召唤兽将自身防御的一部分转化为伤害力的几率，提升效果持续到本回合结束。"
        wds.效果 = "增加"..(s*3).."%的狂暴几率。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x2AA03A67
    elseif wd == "血债偿" then
        wds.说明 = "本方不带本技能召唤兽每被击飞一次，提升一次拥有本技能召唤兽对目标造成的法术伤害，持续到战斗结束。提升效果最多叠加5次，且受召唤兽自身魔力点数影响，不能与鬼魂和高级鬼魂技能共存。"
        wds.效果 = "本方每被击飞一个不带有此技能的召唤兽，自身对目标造成的法术伤害增加"..(s*50).."点。持续到战斗结束，最多可叠加5次，此技能与鬼魂、高鬼魂冲突，效果受自身魔力点数影响。"
        wds.资源 = "JM.FT"
        wds.模型 = 0x97117DEE

    end
    return  wds
end

function 场景类_召唤兽资质栏:界面重叠()

 if  self.资源组[1]:是否选中(鼠标.x,鼠标.y) then -- and  self.焦点1 ==false

     return true
    else
     return false

   end

 end




return 场景类_召唤兽资质栏