local 召唤兽显示类 = class()

function 召唤兽显示类:初始化(根)
	self.背景 = 图像类("imge/001/2340-1.png")
	self.本类开关 = false
	self.文字 = 文字类(simsun, 14)
    self.x,self.y = 0,0
	self.文字:置颜色(黑色)

	self.属性 = 文字类(simsun, 14)
	self.角色相关字体 = 文字类(simsun, 14)
	self.技能名称字体 = 文字类(simsun, 16)
	self.道具参数 = 文字类.创建(simsun, 15)
	self.道具参数1 = 文字类.创建(simsun, 14)
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.超丰富文本 = require("丰富文本类")(240, 270)
    self.圣兽丹动画 = 根.资源:载入('JM.FT',"网易WDF动画",0xA4F1E391)
	self.超丰富文本:添加元素("w", 4294967295.0)
	self.超丰富文本:添加元素("h", 4278190080.0)
	self.超丰富文本:添加元素("y", 4294967040.0)
	self.超丰富文本:添加元素("r", 4294901760.0)
	self.超丰富文本:添加元素("g", 4278255360.0)
	self.技能名称字体:置颜色(黄色)

	self.延迟时间 = 0
end

function 召唤兽显示类:刷新(数据)
	self.本类数据 = 数据
	self.本类开关 = true
	self.动画 = 引擎.场景.召唤兽动画类.创建(self.本类数据.造型, self.本类数据.变异,self.本类数据.饰品)
end

function 召唤兽显示类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 召唤兽显示类:更新(dt)
	self.动画:更新(dt, "静立")
	self.关闭:更新()

	-- local x = 引擎.是否锁定(1)
	-- 引擎.重置锁定(0)
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) and (引擎.鼠标弹起(右键) or self.关闭:取是否单击()) then
		self.本类开关 = false
		self.延迟时间 = os.time()
		-- x = true
	end
	-- if x then 引擎.鼠标锁定(1) end
end

function 召唤兽显示类:显示(x, y)
	if self:界面重叠() then

	 self:拖拽()
	end
	self.背景:显示(self.x+100, self.y+150)
	self.关闭:显示(self.x+530,self.y+ 156)
	引擎.场景.影子:显示(self.x+220, self.y+287)
	self.动画:显示("静立",self.x+ 220,self.y+ 287)
     self.文字:显示(self.x+220,self.y+358,self.本类数据.参站等级)
	self.文字:显示(self.x+323, self.y+358, self.本类数据.等级)
	self.文字:显示(self.x+163, self.y+382, self.本类数据.气血上限)
	self.文字:显示(self.x+163, self.y+407, self.本类数据.魔法上限)
	self.文字:显示(self.x+163, self.y+431, self.本类数据.伤害)
	self.文字:显示(self.x+163, self.y+455, self.本类数据.防御)
	self.文字:显示(self.x+163, self.y+479, self.本类数据.速度)
	self.文字:显示(self.x+163, self.y+503, self.本类数据.灵力)
	self.文字:显示(self.x+308, self.y+382, self.本类数据.体质)
	self.文字:显示(self.x+308, self.y+407, self.本类数据.魔力)
	self.文字:显示(self.x+308, self.y+431, self.本类数据.力量)
	self.文字:显示(self.x+308, self.y+455, self.本类数据.耐力)
	self.文字:显示(self.x+308, self.y+479, self.本类数据.敏捷)
	self.文字:显示(self.x+308, self.y+503, self.本类数据.潜能)
	self.文字:显示(self.x+478, self.y+183, self.本类数据.攻资)
	self.文字:显示(self.x+478, self.y+205, self.本类数据.防资)
	self.文字:显示(self.x+478, self.y+227, self.本类数据.体资)
	self.文字:显示(self.x+478, self.y+249, self.本类数据.法资)
	self.文字:显示(self.x+478, self.y+271, self.本类数据.速资)
	self.文字:显示(self.x+478, self.y+293, self.本类数据.躲资)

	if self.本类数据.神兽 == nil then
		self.文字:显示(self.x+478, self.y+320, self.本类数据.寿命)
	else
		self.文字:显示(self.x+478, self.y+320, "永生")
	end

	self.文字:显示(self.x+478, self.y+342, self.本类数据.成长)
	self.文字:显示(self.x+478, self.y+364, self.本类数据.五行)

	-- self.b = 0

	local a, b
	for n = 1, #self.本类数据.技能, 1 do
		a = (n - 1) % 4
		b = math.floor((n - 1) / 4)


		-- if self.a < 1 then
		-- 	self.a = 1
		-- elseif self.a > 1 and self.a < 2 then
		-- 	self.a = 2
		-- elseif self.a > 2 and self.a < 3 then
		-- 	self.a = 3
		-- end

		-- self.b = self.b + 1

		-- if self.b == 5 then
		-- 	self.b = 1
		-- end
		a = self.x + 365 + a * 42
		b = self.y + 390 + b * 42

		              技能.召唤兽.图标[召唤兽技能信息(self.本类数据.技能[n],"编号")]:显示(a, b)
              if(技能.召唤兽.图标[召唤兽技能信息(self.本类数据.技能[n],"编号")]:检查点(鼠标.x,鼠标.y))then
                   信息提示:显示(2,30,10,鼠标.x-35,鼠标.y-120)
                   self.超丰富文本:清空()
                   self.超丰富文本:添加文本("#g/"..召唤兽技能信息(self.本类数据.技能[n],"说明"))
                   self.技能名称字体:显示(鼠标.x+70,鼠标.y-110,self.本类数据.技能[n])
                   self.超丰富文本:显示(鼠标.x-30,鼠标.y-90)
                end

	end
end
function 召唤兽显示类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end
return 召唤兽显示类
