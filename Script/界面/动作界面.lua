local 动作界面 = class()
local bw = require("gge包围盒")(0,0,173,20)
local box = 引擎.画矩形
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
function 动作界面:初始化(根)
	self.ID = 58
	self.x = 680
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家给与"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
			[0] = 自适应.创建(0,1,111,403,3,9),
			[1] = 资源:载入('JM.FT',"网易WDF动画",0x6338AB06),
			[2] = 资源:载入('JM.FT',"网易WDF动画",0x5BC1F2ED),
			[3] = 资源:载入('JM.FT',"网易WDF动画",0x345EA714),
			[4] = 资源:载入('JM.FT',"网易WDF动画",0x72D0A395),
			[5] = 资源:载入('JM.FT',"网易WDF动画",0x00FE6656),
			[6] = 资源:载入('JM.FT',"网易WDF动画",0x257FA7C6),
			[7] = 资源:载入('JM.FT',"网易WDF动画",0xD9EDD6B2),
			[8] = 资源:载入('JM.FT',"网易WDF动画",0x4DCB4B68),
			[9] = 资源:载入('JM.FT',"网易WDF动画",0x793467AD),
			[10] = 资源:载入('JM.FT',"网易WDF动画",0xB05C49A6),
			[11] = 资源:载入('JM.FT',"网易WDF动画",0xD78D714C),
			[12] = 资源:载入('JM.FT',"网易WDF动画",0x267AB0F3),
			[13] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
            [14] = 自适应.创建(1,1,78,18,1,3,nil,18),
            [15] = 资源:载入('JM.FT',"网易WDF动画",0xB4955D73),
	 }
	 self.资源组[13]:绑定窗口_(58)
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts1:置颜色(0xFFFFFFFF)
	self.文本={
	[1]="#W/站立 #Y/等级：0 快捷键：Alt+1",
	[2]="#W/招呼 #Y/等级：0 快捷键：Alt+2",
	[3]="#W/行礼 #Y/等级：5 快捷键：Alt+3",
	[4]="#W/舞蹈 #Y/等级：10 快捷键：Alt+4",
	[5]="#W/跑步 #Y/等级：15 快捷键：Alt+5",
	[6]="#W/吃惊 #Y/等级：20 快捷键：Alt+6",
	[7]="#W/晕倒 #Y/等级：25 快捷键：Alt+7",
	[8]="#W/悲伤 #Y/等级：30 快捷键：Alt+8",
	[9]="#W/攻击 #Y/等级：35 快捷键：Alt+9",
	[10]="#W/发怒 #Y/等级：40 快捷键：Alt+10",
	[11]="#W/施法 #Y/等级：40 快捷键：Alt+11",
	[12]="#W/休息 #Y/等级：40 快捷键：Alt+12",
     }
end


function 动作界面:打开(数据)
	if self.可视 then
		self.可视 = false
		 引擎.置ini("配置.ini")
		 引擎.写ini("账号信息","音量",全局变量.音量)
	else

		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end



function 动作界面:显示(dt,x,y)
	self.资源组[13]:更新(x,y)
    self.资源组[0]:显示(self.x,self.y)
    self.资源组[14]:显示(self.x+6,self.y+3)
    self.资源组[13]:显示(self.x+90,self.y+5)
    self.焦点 = false
    if self.资源组[13]:事件判断() then
		self:打开()
	end
	tp.窗口标题背景_:置区域(0,0,51,16)
	tp.窗口标题背景_:显示(self.x+22,self.y+3)
	-- zts2:置字间距(7)
	zts2:显示(self.x+ 18,self.y+3, "音量:" .. 全局变量.音量 .. "%") --"动作"
	-- zts2:置字间距(0)
	tp.字体表.普通字体:置颜色(-16777216)
    for i=1,10 do
    	self.资源组[15]:显示(self.x+58,self.y+36*i-7)
    	self.资源组[i]:显示(self.x+60,self.y+36*i-5)
    end
       for i=1,10 do
    	self.资源组[15]:显示(self.x+18,self.y+36*i-7)
    end
     for i=1,2 do
    	self.资源组[i+10]:显示(self.x+20,self.y+36*i-5)
    end
    for i=1,12 do
    	if self.资源组[i]:是否选中(x,y) then
    		self.资源组[i]:置高亮()
    	  tp.提示:自定义(x,y-30,self.文本[i])
    	else
            self.资源组[i]:取消高亮()
        end
    end

    if self.可视 then --self.资源组[0]:是否选中(鼠标.x, 鼠标.y)
    	全局变量.音量 = 全局变量.音量 + 引擎.取鼠标滚轮() * 5
    	if 全局变量.音量 < 0 then 全局变量.音量 = 0 end
    	if 全局变量.音量 > 100 then 全局变量.音量 = 100 end
		 if tp.地图.地图音乐~=nil then
		      tp.地图.地图音乐:置音量(全局变量.音量) --*10
		      --地图.地图音乐:停止()
		     -- 地图.地图音乐:播放()
		 end
    end
end

function 动作界面:检查点(x,y)
	if self.资源组[0]:是否选中(x,y) then
		return true
	end
end

function 动作界面:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 动作界面:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 动作界面
