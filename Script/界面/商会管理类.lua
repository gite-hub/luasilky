
local 商会管理类 = class()


function 商会管理类:初始化()
 self.背景=图像类("imge/001/469-1.png")
 self.背景1=图像类("imge/001/415-1.png")
 self.背景2=图像类("imge/001/415-1.png")
 self.选中背景=图像类("imge/001/126-1.png")
 self.选中动画=图像类("imge/001/125-1.png")
 self.文字=文字类(simsun,14)
 self.状态文字=文字类(simsun,12)
 self.文字:置颜色(白色)
 self.文字1=文字类(simsun,14)
 self.文字1:置颜色(黑色)
 self.本类开关=false
 self.改名=按钮类.创建("imge/001/0016.png",2,"改名",3)
 self.上间=按钮类.创建("imge/001/0016.png",2,"上间",3)
 self.下间=按钮类.创建("imge/001/0016.png",2,"下间",3)
 self.离开=按钮类.创建("imge/001/0016.png",2,"离开",3)
 self.上柜=按钮类.创建("imge/001/0016.png",2,"上柜",3)
 self.更改宗旨=按钮类.创建("imge/001/0015.png",2,"更改宗旨",3)
 self.更改费用=按钮类.创建("imge/001/0015.png",2,"更改费用",3)
 self.投入资金=按钮类.创建("imge/001/0015.png",2,"投入资金",3)
 self.取出资金=按钮类.创建("imge/001/0015.png",2,"取出资金",3)
 self.查看账单=按钮类.创建("imge/001/0015.png",2,"查看账单",3)
 self.出售登记=按钮类.创建("imge/001/0015.png",2,"出售登记",3)
 self.扩张柜台=按钮类.创建("imge/001/0015.png",2,"扩张柜台",3)
 self.削减柜台=按钮类.创建("imge/001/0015.png",2,"削减柜台",3)
 self.暂停营业=按钮类.创建("imge/001/0015.png",2,"暂停营业",3)
 self.放回身上=按钮类.创建("imge/001/0015.png",2,"放回身上",3)
 self.玩家道具={}
 self.玩家选中=0
 self.商会道具={}
 self.商会选中=0
 self.价格 = ""
end


function 商会管理类:更新(dt)
 self.改名:更新(dt)
 self.上间:更新(dt)
 self.下间:更新(dt)
 self.上柜:更新(dt)
 self.离开:更新(dt)
 self.查看账单:更新(dt)
 self.出售登记:更新(dt)
 self.扩张柜台:更新(dt)
 self.削减柜台:更新(dt)
 self.放回身上:更新(dt)
 self.暂停营业:更新(dt)
 self.更改宗旨:更新(dt)
 self.更改费用:更新(dt)
 self.取出资金:更新(dt)
 self.投入资金:更新(dt)

 if self.离开:取是否单击() then

     商铺改名输入:置可视(false,"商铺改名输入")
     商铺宗旨输入:置可视(false,"商铺改名输入")
     上柜银两输入:置可视(false,"上柜银两输入")
     self.本类开关=false

    elseif self.改名:取是否单击() then

     if 商铺改名输入:取文本()=="" then

         信息提示:加入提示("#y/请先取个店名")

        else

        -- 客户端:发送数据(12001,商铺改名输入:取文本())
        客户端:发送数据(983,121,36,商铺改名输入:取文本(),1)

     	 end
    elseif self.更改宗旨:取是否单击() then

     if 商铺宗旨输入:取文本()=="" then

         信息提示:加入提示("#y/宗旨总不能什么话都没有吧")

        else

         --客户端:发送数据(12002,商铺宗旨输入:取文本())
         客户端:发送数据(761,122,36,商铺宗旨输入:取文本(),1)

     	 end

    elseif self.扩张柜台:取是否单击() then

     --客户端:发送数据(12003,"1")
     客户端:发送数据(275,123,36,"8A",1)
     商铺改名输入:置可视(false,"商铺改名输入")
     商铺宗旨输入:置可视(false,"商铺改名输入")
     上柜银两输入:置可视(false,"上柜银两输入")
     self.本类开关=false
    elseif self.出售登记:取是否单击() then

     if self.玩家选中==0 then

         信息提示:加入提示("#y/请先选中一个道具")

        else

         --客户端:发送数据(12002,商铺宗旨输入:取文本())
         --self.发送信息={类型=self.当前店面,格子=self.玩家选中}
         --客户端:发送数据(12005,table.tostring(self.发送信息))
         客户端:发送数据(self.玩家选中,124,36,self.当前店面,1)

     	 end
    elseif self.上柜:取是否单击() then

     if self.商会选中==0 then

         信息提示:加入提示("#y/请先选中一个道具")

        elseif 上柜银两输入:取文本()=="" or 上柜银两输入:取文本()+0<=0 then

         信息提示:加入提示("#y/请先输入价格")
        else

         --客户端:发送数据(12002,商铺宗旨输入:取文本())
        -- self.发送信息={类型=self.当前店面,格子=self.商会选中,价格=上柜银两输入:取文本()}
         --客户端:发送数据(12006,table.tostring(self.发送信息))
         客户端:发送数据(self.商会选中,self.当前店面,37,上柜银两输入:取文本(),1)

     	 end
    elseif self.放回身上:取是否单击() then

     if self.商会选中==0 then

         信息提示:加入提示("#y/请先选中一个道具")

        else

         self.发送信息={类型=self.当前店面,格子=self.商会选中}
       --  客户端:发送数据(12007,table.tostring(self.发送信息))
         客户端:发送数据(self.商会选中,125,36,self.当前店面,1)

     	 end

    elseif self.暂停营业:取是否单击() then

    -- 客户端:发送数据(12008,"1")
     客户端:发送数据(87842,126,36,"1",1)
    elseif self.上间:取是否单击() then

     --客户端:发送数据(12009,self.当前店面)
     客户端:发送数据(12995,127,36,self.当前店面,1)
    elseif self.下间:取是否单击() then

    -- 客户端:发送数据(12010,self.当前店面)
    客户端:发送数据(12995,128,36,self.当前店面,1)

    elseif self.取出资金:取是否单击() then

     --客户端:发送数据(12011,"1")
     客户端:发送数据(9653144,129,36,"1",1)


 	 end

  if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then
    local x, y = 鼠标.x - self.背景.x - 16, 鼠标.y - self.背景.y - 110
    if(x < 0 or y < 0 or x > 260 or y > 210) then
      x = x - 305
      y = y - 44
      if(x < 0 or y < 0 or x > 260 or y > 210) then self.本类开关 = false end
    end
  end
end

function 商会管理类:刷新(数据)
 self.玩家道具={}
 self.玩家选中=0
 self.商会道具={}
 self.商会选中=0
 self.本类开关=true
 self.店铺信息=数据
 self.当前店面=1
 商铺改名输入:置文本(self.店铺信息.店名)
 商铺改名输入:置可视(true,"商铺改名输入")
 上柜银两输入:置可视(true,"上柜银两输入")
 商铺宗旨输入:置文本(self.店铺信息.宣言)
 商铺宗旨输入:置可视(true,"商铺改名输入")
 if self.店铺信息.营业==false then

     self.暂停营业.文字="开始营业"

    else
     self.暂停营业.文字="暂停营业"

 	 end
 self.物品数据={}
 self.物品数据1={}
end

function 商会管理类:刷新柜台道具(数据)



 self.当前店面=数据.类型
 self.商会选中=0
 self.物品数据1={}
  for n=1,20 do

     if 数据[n]~=nil then

         self.物品数据1[n]=物品数据类.创建(数据[n].道具,3,self.当前类型,n)
         self.物品数据1[n].状态=数据[n].状态
         self.物品数据1[n].价格=数据[n].价格
      end


      end


 end





function 商会管理类:刷新玩家道具(数据)


 self.当前类型="包裹"
 self.玩家选中=0
 self.物品数据={}
  for n=1,20 do

     if 数据[n]~=nil then
         self.物品数据[n]=物品数据类.创建(数据[n],3,self.当前类型,n)
         self.物品数据[n].编号=数据[n].编号
      end


      end


 end

function 商会管理类:界面重叠()

 if self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then

     return true
    else
     return false

   end


 end

function 商会管理类:显示(x,y)

 self.背景:显示(100,50)
 self.背景1:显示(115,161)
 self.背景2:显示(420,205)
 self.文字:显示(189,110,self.店铺信息.创店日期)
 self.文字:显示(454,181,self.店铺信息.现金)
 self.文字:显示(189,403,"0%")
 self.文字:显示(189,423,"0%")
 self.文字1:显示(306,85,self.店铺信息.类型.."店")
 self.文字1:显示(186,137,self.店铺信息.店主名称)
 self.文字1:显示(334,138,self.店铺信息.店主id)
 self.文字1:显示(523,134,self.店铺信息.基础运营资金)
 self.文字1:显示(523,159,self.店铺信息.日常运营资金)
 self.文字1:显示(157,446,self.当前店面.."/"..self.店铺信息.店面)

 self.改名:显示(215,83)
 self.上间:显示(196,443)
 self.下间:显示(240,443)
 self.上柜:显示(284,375)
 self.离开:显示(637,443)
 self.查看账单:显示(215,413)
 self.出售登记:显示(295,413)
 self.扩张柜台:显示(284,443)
 self.削减柜台:显示(360,443)
 self.放回身上:显示(436,443)
 self.暂停营业:显示(556,443)
 self.更改宗旨:显示(605,130)
 self.更改费用:显示(605,155)
 self.取出资金:显示(605,180)
 self.投入资金:显示(530,180)
 _GUI:显示(鼠标.x,鼠标.y)
 self:玩家道具显示()
 self:柜台道具显示()
end

function 商会管理类:玩家道具显示()

 self.临时x=0
 self.临时y=0
 self.显示格子=0
for n=1,20 do
       self.临时x=self.临时x+1
	    if(self.临时x==6)then


		    self.临时y=self.临时y+1
		    self.临时x=1

		    if(self.临时y==5)then

			  self.临时y=1





			  end



		   end

     if self.物品数据[n]~=nil  then

           self.物品数据[n].小动画:显示(365+self.临时x*52,210+self.临时y*52)
           if  self.玩家选中==n then

              self.选中动画:显示(365+self.临时x*52+1,210+self.临时y*52-5)
           	 end

           if self.物品数据[n]:获取焦点() then
           	  self.显示格子=n
           	  if 引擎.鼠标弹起(键盘符号.左键) then

                  self.玩家选中=n

           	     end
            end

			end

     end


    if self.显示格子~=0 then self.物品数据[self.显示格子]:显示事件(2)  end

 end



function 商会管理类:柜台道具显示()

 self.临时x=0
 self.临时y=0
 self.显示格子=0
for n=1,20 do
       self.临时x=self.临时x+1
	    if(self.临时x==6)then


		    self.临时y=self.临时y+1
		    self.临时x=1

		    if(self.临时y==5)then

			  self.临时y=1





			  end



		   end

     if self.物品数据1[n]~=nil  then

           self.物品数据1[n].小动画:显示(61+self.临时x*52,165+self.临时y*52)
           if  self.商会选中==n then

              self.选中动画:显示(61+self.临时x*52+1,165+self.临时y*52-5)
           	 end

            if self.物品数据1[n].状态 then

              self.状态文字:置颜色(红色):显示(61+self.临时x*52+15,165+self.临时y*52+30,"售")

            else
             self.状态文字:置颜色(绿色):显示(61+self.临时x*52+15,165+self.临时y*52+30,"待")

             end

           if self.物品数据1[n]:获取焦点() then
           	  self.显示格子=n
           	  if 引擎.鼠标弹起(键盘符号.左键) then

                  self.商会选中=n
                  if self.物品数据1[n].状态 then
                      self.价格 = self.物品数据1[n].价格
                      上柜银两输入:置文本(self.物品数据1[n].价格)
                      self.上柜.文字="下柜"

                    else
                      上柜银两输入:置文本(self.价格)
                      self.上柜.文字="上柜"
                     end

           	     end
            end

			end

     end


    if self.显示格子~=0 then self.物品数据1[self.显示格子]:显示事件(2)  end

 end









return 商会管理类