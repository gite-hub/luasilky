local 包裹类 = class()

function 包裹类:初始化(根)
	self.本类开关 = false
	self.x,self.y=0,0
	self.作弊参数 = {
		敏捷 = 200,
		伤害 = 1500,
		气血 = 800,
		命中 = 1500,
		魔法 = 500,
		防御 = 500
	}
	self.背景 = 图像类("Dat/UI图标/行囊.png")
	self.背景1 = 图像类("Dat/UI图标/装备.png")
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.关闭1 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.关闭2 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.道具按钮 = 按钮类.创建("imge/001/0016.png", 2, "道具", 3)
	self.行囊按钮 = 按钮类.创建("imge/001/0016.png", 2, "行囊", 3)
	self.出售按钮 = 按钮类.创建("imge/001/0016.png", 2, "回收", 3)
	self.分解按钮 = 按钮类.创建("imge/001/0016.png", 2, "分解", 3)
	self.法宝 = 按钮类.创建("imge/001/0016.png", 2, "法宝", 3)
	self.鉴定按钮 = 按钮类.创建("imge/001/0016.png", 2, "鉴定", 3)
	self.打造按钮 = 按钮类.创建("imge/001/0016.png", 2, "临时", 3)
	self.选中动画 = 图像类("imge/001/125-1.png")
	self.选中背景 = 图像类("imge/001/126-1.png")
	self.丢弃背景 = 图像类("imge/001/245-1.png")
	self.佩戴背景 = 图像类("imge/001/720-1.png")
	self.银两文字 = 文字类.创建(simsun, 17)
	self.道具名称 = 文字类.创建(simsun, 25)
	self.丢弃名称 = 文字类.创建(simsun, 14)
	self.丢弃名称:置颜色(黄色)
	self.道具名称:置颜色(黄色)
	self.拿起对象 = 0
	self.道具说明 = 文字类.创建(simsun, 15)
	self.道具参数 = 文字类.创建(simsun, 15)
	self.道具参数1 = 文字类.创建(simsun, 14)
	self.金钱文字 = 文字类.创建(simsun, 17)
	self.灵饰开关 = false
	self.锦衣开关 = false
	self.灵饰背景 = 图像类("imge/001/ls.png")
	self.锦衣背景 =  根._自适应.创建(0,1,129,143,3,9)
	self.银子类型 = 1
	self.现金 = 按钮类.创建("imge/001/0016.png", 2, "现金", 3)
	self.饰品按钮 = 按钮类.创建("imge/001/0052.png", 2, "饰", 3, nil, nil, nil, -3)
	self.看按钮 = 按钮类.创建("imge/001/0052.png", 2, "锦", 3, nil, nil, nil, -3)
	self.按钮3 = 按钮类.创建("imge/001/0052.png", 2, "骑", 3, nil, nil, nil, -3)
	self.按钮2 = 按钮类.创建("imge/001/0052.png", 2, "2", 3, nil, nil, nil, -3)
	self.按钮1 = 按钮类.创建("imge/001/0052.png", 2, "1", 3, nil, nil, nil, -3)
	self.人物 = 按钮类.创建("imge/001/0051.png", 2, "人物", 3, nil, nil, nil, -1)
	self.召唤 = 按钮类.创建("imge/001/0051.png", 2, "召唤", 3, nil, nil, nil, -1)
	self.坐骑 = 按钮类.创建("imge/001/0051.png", 2, "坐骑", 3, nil, nil, nil, -1)
	self.子女 = 按钮类.创建("imge/001/0051.png", 2, "子女", 3, nil, nil, nil, -1)
	self.锦衣按钮 = 按钮类.创建("imge/001/0051.png", 2, "锦衣", 3, nil, nil, nil, -1)  --你准备放哪里
	self.锦衣横框 = 根._自适应.创建(1,1,99,18,1,3,nil,18)
	self.锦衣背景图片 = {
		[1] = 根.资源:载入('JM.FT',"网易WDF动画",0x219FF008),
		[2] = 根.资源:载入('JM.FT',"网易WDF动画",0x21246323),
		[3] = 根.资源:载入('JM.FT',"网易WDF动画",0xC2966B6F),
		[4] = 根.资源:载入('JM.FT',"网易WDF动画",0xCF39217A),}


	self.法宝背景 = 根.资源:载入('wzife.wdf',"网易WDF动画",0X419DCF2A)
	self.炼丹炉背景 = 根.资源:载入('wzife.wdf',"网易WDF动画",0X300C54D8)
	self.法宝佩戴背景 = 根.资源:载入('wzife.wdf',"网易WDF动画",0xCF39217A)
	self.法宝佩戴背景1 = 根.资源:载入('wzife.wdf',"网易WDF动画",0xFE7DE843)
	self.法宝佩戴背景2 = 根.资源:载入('wzife.wdf',"网易WDF动画",0xC0520208)
	self.法宝佩戴背景3 = 根.资源:载入('wzife.wdf',"网易WDF动画",0x9CC4F242)
	self.使用 = 按钮类.创建("imge/001/0016.png", 2, "使用", 3)
	self.修炼 = 按钮类.创建("imge/001/0016.png", 2, "修炼", 3)

	self.道具 = {
		{
			x =self.x+ 30,
			y =self.y+ 200
		},
		[10] = {
			x =self.x+ 230,
			y =self.y+ 250
		},
		[9] = {
			x =self.x+ 180,
			y =self.y+ 250
		},
		[8] = {
			x =self.x+ 130,
			y =self.y+ 250
		},
		[7] = {
			x =self.x+ 80,
			y =self.y+ 250
		},
		[6] = {
			x =self.x+ 30,
			y =self.y+ 250
		},
		[5] = {
			x =self.x+ 230,
			y =self.y+ 200
		},
		[4] = {
			x =self.x+ 180,
			y =self.y+ 200
		},
		[3] = {
			x =self.x+ 130,
			y =self.y+ 200
		},
		[2] = {
			x =self.x+ 80,
			y =self.y+ 200
		},
		[11] = {
			x =self.x+ 230,
			y =self.y+ 300
		},
		[12] = {
			x =self.x+ 180,
			y =self.y+ 300
		},
		[13] = {
			x =self.x+ 130,
			y =self.y+ 300
		},
		[14] = {
			x =self.x+ 80,
			y =self.y+ 300
		},
		[15] = {
			x =self.x+ 30,
			y =self.y+ 300
		},
		[16] = {
			x =self.x+ 230,
			y =self.y+ 355
		},
		[17] = {
			x =self.x+ 180,
			y =self.y+ 355
		},
		[18] = {
			x =self.x+ 130,
			y =self.y+ 355
		},
		[19] = {
			x =self.x+ 80,
			y =self.y+ 355
		},
		[20] = {
			x =self.x+ 30,
			y =self.y+ 355
		},
		[21] = {
			x =self.x+ 200,
			y =self.y+ 80
		},
		[22] = {
			x =self.x+ 200,
			y =self.y+ 30
		},
		[23] = {
			x =self.x+ 200,
			y =self.y+ 135
		},
		[24] = {
			x =self.x+ 250,
			y =self.y+ 30
		},
		[25] = {
			x =self.x+ 250,
			y =self.y+ 135
		},
		[26] = {
			x =self.x+ 250,
			y =self.y+ 80
		}
	}
	self.道具[35]={x=188,y=28}
 	self.道具[36]={x=250,y=28}
 	self.道具[37]={x=188,y=88}
	self.道具[38]={x=250,y=88}

	self.操作对象 = 0
	self.当前类型 = "包裹"
	self.包围盒 = {}
	self.灵饰坐标 = {
		[31] = {
			x =self.x+ 321,
			y =self.y+ 25
		},
		[32] = {
			x =self.x+ 376,
			y =self.y+ 25
		},
		[33] = {
			x =self.x+ 321,
			y =self.y+ 80
		},
		[34] = {
			x =self.x+ 375,
			y =self.y+ 80
		}
	}
	self.锦衣坐标 = {
		[35] = {
			x =self.x+ 321,
			y =self.y+ 165
		},
		[36] = {
			x =self.x+ 376,
			y =self.y+ 165
		},
		[37] = {
			x =self.x+ 321,
			y =self.y+ 220
		},
		[38] = {
			x =self.x+ 375,
			y =self.y+ 220
		}
	}

	for n = 1, 26, 1 do
		self.包围盒[n] = 包围盒.创建(0, 0, 50, 50)

		self.包围盒[n]:置坐标(self.道具[n].x+self.x, self.道具[n].y+self.y)
	end

	self.移动开关 = false
end

function 包裹类:刷新头像()
	self.头像偏差 = 0

	local qtx = 引擎.取头像(tp.屏幕.主角.数据.造型)
	self.头像图片 = 引擎.场景.资源:载入(qtx[4],"网易WDF动画",qtx[6])
end

function 包裹类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) or (self.灵饰开关 and self.灵饰背景:取包围盒():检查点(鼠标.x, 鼠标.y)) or (self.锦衣开关 and self.锦衣背景:是否选中(鼠标.x, 鼠标.y))  then


		return true
	else
		return false
	end
end

function 包裹类:刷新道具(数据)
	  打开符石 = 0


	for n = 1, 26, 1 do
		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n], 1, self.当前类型, n)
			self.物品数据[n].编号 = 数据[n].编号
		end
	end
end

function 包裹类:刷新灵饰(数据1)
	self.灵饰开关 = true
	self.灵饰数据 = {}

	for n = 31, 34, 1 do
		if 数据1[n] ~= 0 then
			self.灵饰数据[n] = 物品数据类.创建(数据1[n], 1, self.当前类型, n)
			self.灵饰数据[n].编号 = n
		else
			self.灵饰数据[n] = 0
		end
	end
end
function 包裹类:刷新锦衣(数据1)
	self.锦衣开关 = true
	self.锦衣数据 = {}

	for n = 35, 38, 1 do
		if 数据1[n] ~= 0 then
			self.锦衣数据[n] = 物品数据类.创建(数据1[n], 1, self.当前类型, n)
			self.锦衣数据[n].编号 = n
		else
			self.锦衣数据[n] = 0
		end
	end
end
function 包裹类:刷新(数据)
	打开符石 = 0
	self.本类开关 = true

	self:刷新头像()

	self.银两 = 数据.银两
	self.存银 = 数据.存银
	self.储备 = 数据.储备
	self.当前类型 = 数据.类型
	if 数据.类型 == "包裹" then
		self.类型编号 = 1
		self.选中对象 = 0
	elseif 数据.类型 == "行囊" then
		self.类型编号 = 2
		self.选中对象 = 0
	elseif 数据.类型 == "法宝" then
		self.类型编号 = 3
	end

	self.物品数据 = {}
	self.商品编号 = 0
	self.拿起对象 = 0

	for n = 1, 26, 1 do


		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n], 1, self.当前类型, n)
			self.物品数据[n].编号 = 数据[n].编号
			if self.类型编号 == 3 then
			    self.物品数据[n].修炼经验 = 数据[n].修炼经验
			    self.物品数据[n].所需经验 = 数据[n].升级经验
			end
		end
	end
	if self.类型编号 == 3 then
		for n = 35, 38, 1 do
			if 数据[n] ~= nil then

				self.物品数据[n] = 物品数据类.创建(数据[n], 1, self.当前类型, n)
				self.物品数据[n].编号 = 数据[n].编号
				self.物品数据[n].修炼经验 = 数据[n].修炼经验
			    self.物品数据[n].所需经验 = 数据[n].升级经验
			end
		end

	end
end

function 包裹类:更新(dt)
	if tp.主界面:界面重叠1() then
		return 0
	end

	self.道具按钮:更新(self.x+16,self.y+411)
	self.行囊按钮:更新(self.x+61,self.y+411)
	self.出售按钮:更新(self.x+61,self.y+411)
	self.分解按钮:更新(self.x+61,self.y+411)
	self.鉴定按钮:更新(self.x+61,self.y+411)
	self.打造按钮:更新(self.x+61,self.y+411)
	self.法宝:更新(self.x+61,self.y+411)
	self.关闭:更新(self.x+289, self.y+6)
	self.使用:更新()
	self.修炼:更新()
	if self.类型编号 ~= 3 then
		self.现金:更新()
		self.饰品按钮:更新()
		-- self.锦衣按钮:更新()
		self.看按钮:更新(self.x+170, self.y+99)
		self.按钮3:更新(self.x+170, self.y+74)
		-- self.按钮2:更新(self.x+170, self.y+49)
		-- self.按钮1:更新(self.x+170, self.y+24)
		-- self.人物:更新(self.x+-3, self.y+25)
		-- self.召唤:更新(self.x+-3, self.y+53)
		-- self.坐骑:更新(self.x+-3, self.y+77)
		-- self.子女:更新(self.x+-3, self.y+101)
	end
	if self.道具按钮:取是否单击() then
		客户端:发送数据(1, 1, 13, "9", 1)
	elseif self.行囊按钮:取是否单击() then
		客户端:发送数据(2, 1, 13, "9", 1)
	elseif self.召唤:取是否单击() then
		客户端:发送数据(6, 1, 22, "6A", 1)

		self.本类开关 = false
	elseif self.关闭:取是否单击() then
		self.本类开关 = false
	elseif self.打造按钮:取是否单击() then
		-- 客户端:发送数据(2, 6, 13, "9", 1)
	elseif self.分解按钮:取是否单击() then
		客户端:发送数据(204, 204, 13, 1)
	-- elseif self.坐骑:取是否单击() then
	elseif self.按钮3:取是否单击() then
		客户端:发送数据(2, 6, 47, "9", 1)

		self.本类开关 = false
	elseif self.饰品按钮:取是否单击() then
		if self.灵饰开关 then
			self.灵饰开关 = false

			return 0
		end

		客户端:发送数据(2, 74, 13, "9", 1)
	-- elseif self.锦衣按钮:取是否单击() then
	elseif self.看按钮:取是否单击() then
		if self.锦衣开关 then
			self.锦衣开关 = false

			return 0
		end

		客户端:发送数据(2, 202, 13, "9", 1)
	elseif self.法宝:取是否单击() and self.类型编号 ~= 3 then

		客户端:发送数据(3, 1, 13, "9", 1)
	elseif self.修炼:取是否单击()  then
		客户端:发送数据(self.选中对象, 208, 13, "法宝", 1)
	elseif self.现金:取是否单击() then
		if self.银子类型 == 1 then
			self.银子类型 = 2
			self.现金.文字 = "储备"
		else
			self.银子类型 = 1
			self.现金.文字 = "现金"
		end
	end
end

function 包裹类:显示(x, y)
				if self:界面重叠() then
	 self:拖拽()
	end
	self.背景:显示(self.x+0, self.y+0)
	self.关闭:显示(self.x+289, self.y+6)
	self.道具按钮:显示(self.x+16, self.y+411)
	self.行囊按钮:显示(self.x+62, self.y+411)
	self.鉴定按钮:显示(self.x+203, self.y+411)
	self.打造按钮:显示(self.x+108, self.y+411)
	self.出售按钮:显示(self.x+250, self.y+411)
	--self.分解按钮:显示(self.x+250, self.y+411)
	self.丢弃名称:显示(self.x+11, self.y+434, "将道具拖曳至上面对应的按钮处即可使用该功能")
	if self.类型编号 == 2 or self.类型编号 == 1 then
		self.背景1:显示(self.x+-1, self.y+24)
		self.现金:显示(self.x+7, self.y+150)

		if self.银子类型 == 1 then
			银两显示(self.金钱文字, self.银两, self.x+68, self.y+152)
		else
			银两显示(self.金钱文字, self.储备, self.x+68, self.y+152)
		end
		for n = 1, 26, 1 do

			self.包围盒[n]:置坐标(self.道具[n].x+self.x, self.道具[n].y+self.y)
		end

		银两显示(self.金钱文字, self.存银, self.x+68, self.y+175)


		self.头像图片:显示(self.x+38 + self.头像偏差, self.y+25)

		self.显示id = 0
		self.包围盒碰撞 = false
		self.交换触发 = true

		self.饰品按钮:显示(self.x+170, self.y+98)
		self.看按钮:显示(self.x+170, self.y+122 + 3)
		self.按钮3:显示(self.x+170, self.y + 74 - 3)
		-- self.按钮2:显示(self.x+170, self.y+50)
		-- self.按钮1:显示(self.x+170, self.y+25)
		-- self.人物:显示(self.x+-0, self.y+26)
		-- self.召唤:显示(self.x+-0, self.y+53)
		-- self.坐骑:显示(self.x+-0, self.y+77)
		-- self.子女:显示(self.x+-0, self.y+101)
		self.法宝:显示(self.x+155, self.y+411)
		-- self.锦衣按钮:显示(self.x+-0, self.y+125)
		self.灵饰显示id = 0
		self.锦衣显示id = 0
	elseif self.类型编号 == 3 then
		self.法宝背景:显示(self.x+0, self.y+24)
		self.炼丹炉背景:显示(self.x+8, self.y+24)
		self.法宝佩戴背景:显示(self.x+185, self.y+28)
		self.法宝佩戴背景1:显示(self.x+245, self.y+28)
		self.法宝佩戴背景2:显示(self.x+185, self.y+88)
		self.法宝佩戴背景3:显示(self.x+245, self.y+88)
		self.使用:显示(self.x+250, self.y+150)
		self.修炼:显示(self.x+250, self.y+175)
	end

	if self.灵饰开关 then
		self.灵饰背景:显示(self.x+310, self.y+0)
		self.关闭1:更新()
		self.关闭1:显示(self.x+414, self.y+4)

		if self.关闭1:取是否单击() then
			self.灵饰开关 = false
		end

		for n = 31, 34, 1 do
			if self.灵饰数据[n] ~= 0 then
				self.佩戴背景:显示(self.x+self.灵饰坐标[n].x, self.y+self.灵饰坐标[n].y)
				self.灵饰数据[n]:显示(self.x+self.灵饰坐标[n].x + 3, self.y+self.灵饰坐标[n].y + 3)

				if self.灵饰数据[n]:获取焦点() then
					self.灵饰显示id = n
				end
			end
		end
		  if 引擎.鼠标弹起(1) and self.灵饰背景:取包围盒():检查点(鼠标.x,鼠标.y) then
		    local y = 鼠标.y - self.灵饰背景.y
		    if(y > 0 and y < 27) then self.灵饰开关 = false end
		  end

	end
	if self.锦衣开关 then
		self.锦衣背景:显示(self.x+310, self.y+140)
		self.锦衣横框:显示(self.x+6+310,self.y+3+140)
		self.关闭2:更新()
		self.关闭2:显示(self.x+414, self.y+144)
		tp.窗口标题背景_:置区域(0,0,40,16)
		tp.窗口标题背景_:显示(self.x+37+310,self.y+3+140)
		zts1:置字间距(3)
		zts1:显示(self.x+41+310,self.y+3+140,"锦衣")
		zts1:置字间距(0)
		for i=1,4 do
			self.锦衣背景图片[i]:显示(self.x+self.锦衣坐标[i+34].x, self.y+self.锦衣坐标[i+34].y)
		end
		if self.关闭2:取是否单击() then
			self.锦衣开关 = false
		end

		for n = 35, 38, 1 do
			if self.锦衣数据[n] ~= 0 then
					self.佩戴背景:显示(self.x+self.锦衣坐标[n].x, self.y+self.锦衣坐标[n].y)
				self.锦衣数据[n]:显示(self.x+self.锦衣坐标[n].x + 3, self.y+self.锦衣坐标[n].y + 3)

				if self.锦衣数据[n]:获取焦点() then
					self.锦衣显示id = n
				end
			end
		end

		if 引擎.鼠标弹起(1) and self.锦衣背景:是否选中(鼠标.x,鼠标.y) then
		    local y = 鼠标.y - self.锦衣背景.y
		    if(y > 0 and y < 27) then self.锦衣开关 = false end
		  end
	end
	self.显示id = 0

	for n = 1, 38, 1 do
		if self.物品数据[n] ~= nil then
			self.临时误差 = {
				x = 0,
				y = 0
			}

			if n > 20 then
				self.临时误差 = {
					x =4,
					y = 1
				}

				if n == 22 then
					self.临时误差.y = 5
				elseif n == 24 then
					self.临时误差.y = 5
					self.临时误差.x = 0
				elseif n == 25 or n == 26 then
					self.临时误差.x = 0
				end

				self.佩戴背景:显示(self.道具[n].x - self.临时误差.x+self.x, self.道具[n].y - self.临时误差.y+self.y)
			end

			if n ~= self.拿起对象 and n ~= self.选中对象 then
				self.物品数据[n]:显示(self.x+self.道具[n].x - self.临时误差.x, self.y+self.道具[n].y - self.临时误差.y)
			elseif self.选中对象 ~= nil and n == self.选中对象 and self.选中对象>0 and self.类型编号 == 3 then
				self.物品数据[n]:显示(self.x+self.道具[n].x - self.临时误差.x, self.y+self.道具[n].y - self.临时误差.y)
				self.选中动画:显示(self.x+self.道具[n].x - self.临时误差.x, self.y+self.道具[n].y - self.临时误差.y)
				self.金钱文字:显示(self.x+93, self.y+153,self.物品数据[n].所需经验)
				self.金钱文字:显示(self.x+93, self.y+175,self.物品数据[n].修炼经验)
			else
				self.物品数据[n]:显示(鼠标.x - 20, 鼠标.y - 20)
				self.选中动画:显示(self.x+self.道具[n].x - self.临时误差.x, self.y+self.道具[n].y - self.临时误差.y)
			end

			if self.物品数据[n]:获取焦点() then
				self.显示id = n
			end
		end
	end

	if self.显示id ~= 0 then
		if 引擎.鼠标弹起(左键) and tp.主界面:界面重叠1() == false then
			if self.拿起对象 == 0 then
				if 引擎.鼠标弹起(左键) and  self.当前类型 == "法宝" and self.选中对象 ~= nil and  self.选中对象==self.显示id then
					self.拿起对象 = self.选中对象
					self.选中对象 = 0
				elseif self.当前类型 == "法宝" then
					self.选中对象 = self.显示id
				else
					self.拿起对象 = self.显示id
				end
			else
				for n = 1, #self.包围盒, 1 do
					if self.包围盒[n]:检查点(鼠标.x, 鼠标.y) then
						self.发送信息 = {
							原始格子 = self.拿起对象,
							交换格子 = n,
							类型 = self.当前类型
						}

						客户端:发送数据(self.拿起对象, n, 15, self.当前类型)

						self.包围盒碰撞 = true
					end
				end
           		if 引擎.鼠标弹起(0) and self:界面重叠()==false then
					self.发送信息 = {
						格子 = self.拿起对象,
						类型 = self.当前类型
					}

					客户端:发送数据(self.拿起对象, 3, 13, self.当前类型)

					self.拿起对象 = 0
				end
				if self.包围盒碰撞 == false then
					if self.道具按钮.包围盒:检查点(鼠标.x, 鼠标.y) and self.当前类型 ~= "包裹" then
						客户端:发送数据(self.拿起对象, self.类型编号, 16, "包裹")
					elseif self.行囊按钮.包围盒:检查点(鼠标.x, 鼠标.y) and self.当前类型 ~= "行囊" then
						客户端:发送数据(self.拿起对象, self.类型编号, 16, "行囊")

					elseif self.鉴定按钮.包围盒:检查点(鼠标.x, 鼠标.y) then
						self.发送信息 = {
							格子 = self.拿起对象,
							当前类型 = self.当前类型
						}

						客户端:发送数据(self.拿起对象, 5, 13, self.当前类型)

						self.拿起对象 = 0
					elseif self.出售按钮.包围盒:检查点(鼠标.x, 鼠标.y) then
						self.发送信息 = {
							格子 = self.拿起对象,
							类型 = self.当前类型
						}

						客户端:发送数据(self.拿起对象, 4, 13, self.当前类型)

						self.拿起对象 = 0
					elseif self.法宝.包围盒:检查点(鼠标.x, 鼠标.y)  and self.当前类型 ~= "法宝" then
						客户端:发送数据(self.拿起对象, self.类型编号, 16, "法宝")

					end
				end
			end
		elseif 引擎.鼠标弹起(右键) and self.拿起对象 ~= 0 and self.拿起对象 == self.显示id then
			self.拿起对象 = 0
		end
	end

	if self.灵饰显示id ~= 0 then
		self.灵饰数据[self.灵饰显示id]:显示事件()
	end
	if self.锦衣显示id ~= 0 then
		self.锦衣数据[self.锦衣显示id]:显示事件()
	end
	if self.显示id ~= 0 then
		self.物品数据[self.显示id]:显示事件()
	end



   if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then
    local x, y = 鼠标.x - self.背景.x - 185, 鼠标.y - self.背景.y - 27
    if(x < 0 or y < 0 or x > 120 or y > 165) then
      local x, y = 鼠标.x - self.背景.x - 27, 鼠标.y - self.背景.y - 197
      if(x < 0 or y < 0 or x > 260 or y > 210) then self.本类开关 = false end
    end
  end

end
function 包裹类:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
    end
    end
return 包裹类
