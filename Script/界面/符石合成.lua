
local 场景类_开运 = class()
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 场景类_开运:初始化(根)
	tp= 根
	self.ID = 59
	self.x = 224
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.本类开关 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.FT',"网易WDF动画",0xC885F689),
		[2] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[3] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x2BD1DEF7),0,0,4,true,true,"合成"),

	}

	self.物品 = {}

	for n=2,3 do
	   self.资源组[n]:绑定窗口_(59)
	end
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	self.选中 =0
	self.材料数量 = 0
	self.材料 = {}
	self.选中材料= {}
end

function 场景类_开运:打开(数据)
	if self.本类开关 then
		self.本类开关 = false
		self.选中 =0
		self.材料={}
self.材料数量 = 0
		self.物品={}

	else

    self.选中材料= {}


       self.数据 = 数据

self.材料数量 = 0
 for n=1,20 do
     if 数据[n]~=nil then
         self.物品[n]=物品数据类.创建(数据[n],3,"包裹",n)
         self.物品[n].编号=数据[n].编号
      end
     end


	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.本类开关 = true
	end
end
function 场景类_开运:更新(dt)
	self.资源组[2]:更新(鼠标.x,鼠标.y)
	self.资源组[3]:更新(鼠标.x,鼠标.y,self.材料数量 >= 4)
	if self.资源组[2]:事件判断() or (self:界面重叠() and 引擎.鼠标按下(0x1) ) then
		if self.本类开关 then
			local x, y = 鼠标.x - self.资源组[1].x - 20, 鼠标.y - self.资源组[1].y - 150
		    if(x < 0 or y < 0 or x > 260 or y > 210) then self:打开() end
		else self:打开() end
	elseif self.资源组[3]:事件判断() then
客户端:发送数据(1,185,13,self.选中材料[1].."*-*"..self.选中材料[2].."*-*"..self.选中材料[3].."*-*"..self.选中材料[4])
self:打开()
   	end
end

function 场景类_开运:显示(x,y)
		if self:界面重叠()  then
	self:拖拽()
    end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+288,self.y+12)
	self.资源组[3]:显示(self.x+190,self.y+152,true,nil,nil,self.材料数量 < 4,3)
    zts:置颜色(引擎.取金钱颜色(self.数据.银子))
    zts:显示(self.x+230,self.y+72,self.数据.银子)
    zts:置颜色(0xFFFFFF00)
    zts:显示(self.x+230,self.y+129,self.数据.体力)
    zts:置颜色(0xFFFFFFFF)

    local is = 0
	for h=1,4 do
		for l=1,5 do
			is = is + 1
		if self.物品[is] ~= nil  then
			self.物品[is]:显示(l * 51 - 14 + self.x,h * 51 + self.y + 131)
			if  self.物品[is]:获取焦点() then
				self.物品[is]:显示事件(2)
				if 引擎.鼠标按下(0) then
						if self.物品[is].类型 == "符石" or self.物品[is].名称 == "符石卷轴" then
						  if self.材料数量 < 4 then
							for i=1,4 do
								if self.材料[i] == nil then
									self.选中 =is
				               		self.材料[i]=物品数据类.创建(self.数据[is],3,"包裹",is)
									self.物品[is]=nil
									self.材料数量 = self.材料数量 + 1
									self.选中材料[i]=is
									break
								end
							end
						   else

			                    信息提示:加入提示("#r/放入的物品太多了，不允许再放入了！")
			                end
		                else
		                	信息提示:加入提示("#r/只有符石和符石卷轴才能合成！")
		                end

				end
			end
		end
		end
	end

	is = 0
	for h=1,2 do
		for l=1,2 do
			is = is + 1
            if self.材料[is] ~= nil then
			self.材料[is]:显示(l * 58 - 23 + self.x,h * 55 + self.y + 8)
				if  self.材料[is]:获取焦点() then
				self.材料[is]:显示事件(2)
					if mouseb(0)  then
					self.物品[self.选中]=物品数据类.创建(self.数据[self.选中],3,"包裹",self.选中)
					self.材料[is]=nil
					self.材料数量 = self.材料数量 - 1
					end
				end
		    end
       end
   end

		if #self.材料 >= 4 then
		zts:置颜色(引擎.取金钱颜色(500000))
		zts:显示(self.x + 230,self.y + 43,500000)
		zts:置颜色(0xFFFFFF00)
		zts:显示(self.x + 230,self.y + 99,30)
		zts:置颜色(0xFFFFFFFF)


		end

end
function 场景类_开运:界面重叠()
	if self.资源组[1]:是否选中(鼠标.x,鼠标.y)  then
		return true
	end
end
function 场景类_开运:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end
return 场景类_开运


