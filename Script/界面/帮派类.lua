local 帮派类 = class()

function 帮派类:初始化()
	self.背景 = 图像类("imge/001/243-1.png")
	self.本类开关 = false
	self.文字 = 文字类(simsun, 14)

	self.文字:置颜色(黑色)

	self.文字1 = 文字类(simsun, 14)
	self.加入 = 按钮类.创建("imge/001/0016.png", 2, "加入", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.增加 = 按钮类.创建("imge/001/下移.png", 1, 1, 3)
	self.减少 = 按钮类.创建("imge/001/上移.png", 1, 1, 3)
	self.更改职务 = 按钮类.创建("imge/001/0015.png", 2, "更改职务", 3)
	self.申请列表 = 按钮类.创建("imge/001/0015.png", 2, "申请名单", 3)
	self.踢出帮派 = 按钮类.创建("imge/001/0015.png", 2, "请离帮派", 3)
	self.脱离帮派 = 按钮类.创建("imge/001/0015.png", 2, "脱离帮派", 3)
	self.选中背景 = 图像类("imge/001/lsbj.png")
	self.超丰富文本 = require("丰富文本类")(145, 130)

	self.超丰富文本:添加元素("h", 黑色)
end

function 帮派类:刷新(数据)
	self.本类开关 = true
	self.数据 = 数据

	self.显示序列 = 1
	self.选中编号 = 0

	self.超丰富文本:清空()
	self.超丰富文本:添加文本("#h/" .. self.数据.宗旨)

	self.成员显示 = {}

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "帮主" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "副帮主" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "左护法" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "右护法" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "长老" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].在线 and self.数据.成员名单[n].显示状态 == nil then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].在线 == false and self.数据.成员名单[n].显示状态 == nil then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end

	for n = 1, #self.成员显示, 1 do
		self.成员显示[n].包围盒 = 包围盒:创建(150, 192, 64, 18)

		self.成员显示[n].包围盒:更新宽高(140, 15)
	end
end

function 帮派类:更新(dt)
	self.关闭:更新()
	self.减少:更新(378, 280)
	self.增加:更新(378, 428)
	self.更改职务:更新(249, 467)
	self.申请列表:更新(349, 467)
	self.踢出帮派:更新(449, 467)
	self.脱离帮派:更新(549, 467)

	if self.关闭:取是否单击() then
		self.本类开关 = false
	elseif self.减少:取是否单击() then
		if self.显示序列 <= 1 then
			self.显示序列 = 1
		else
			self.显示序列 = self.显示序列 - 1
		end
	elseif self.增加:取是否单击() then
		if self.显示序列 + 5 > #self.成员显示 then
			信息提示:加入提示("#y/没有更多的帮派成员了")
		else
			self.显示序列 = self.显示序列 + 1
		end
	elseif self.申请列表:取是否单击() then
		客户端:发送数据(6, 65, 13, "5")
	elseif self.脱离帮派:取是否单击() then
		客户端:发送数据(6, 69, 13, "5")
	elseif self.踢出帮派:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中一个帮派成员")

			return 0
		end

		客户端:发送数据(self.成员显示[self.选中编号].id, 70, 13, "5")
	elseif self.更改职务:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选中一个帮派成员")

			return 0
		end

		客户端:发送数据(self.成员显示[self.选中编号].id, 71, 13, "5")
	end

  if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then self.本类开关 = false end

end

function 帮派类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 帮派类:显示(x, y)
	self.背景:显示(200, 70)
	self.关闭:显示(582, 76)
	self.减少:显示(378, 280)
	self.增加:显示(378, 428)
	self.更改职务:显示(215, 457)
	self.申请列表:显示(315, 457)
	self.踢出帮派:显示(415, 457)
	self.脱离帮派:显示(515, 457)
	self.文字:置颜色(黑色)
	self.文字:显示(294, 104, self.数据.名称)
	self.文字:显示(294, 126, self.数据.创始人.名称)
	self.文字:显示(294, 148, self.数据.现任帮主.名称)
	self.文字:显示(294, 170, self.数据.敌对.名称)
	self.文字:显示(294, 192, self.数据.掌控区域.名称)
	self.文字:显示(294, 214, self.数据.当前内政.名称)
	self.文字:显示(294, 236, self.数据.同盟.名称)
	self.文字:显示(314, 258, self.数据.行动力)
	self.文字:显示(484, 104, self.数据.当前人数 .. "/" .. self.数据.人数上限)
	self.文字:显示(484, 126, self.数据.资金)
	self.文字:显示(484, 148, self.数据.安定)
	self.文字:显示(484, 170, self.数据.人气)
	self.文字:显示(484, 192, self.数据.繁荣)
	self.文字:显示(484, 214, self.数据.规模)
	self.文字:显示(484, 236, self.数据.编号)
	self.文字:显示(524, 258, self.数据.敌对.编号)
	self.超丰富文本:显示(417, 313)

	self.显示位置 = 0

	for n = 1, #self.成员显示, 1 do
		if self.显示序列 <= n and n <= self.显示序列 + 5 then
			self.显示位置 = n - self.显示序列 + 1

			if self.选中编号 == n then
				self.选中背景:显示(225, 317 + self.显示位置 * 20 - 20)
			end

			if self.成员显示[n].在线 then
				self.文字1:置颜色(绿色):显示(229, 317 + self.显示位置 * 20 - 20, "[" .. self.成员显示[n].职务 .. "]")
				self.文字1:置颜色(绿色):显示(289, 317 + self.显示位置 * 20 - 20, self.成员显示[n].名称)
			else
				self.文字1:置颜色(黑色):显示(229, 317 + self.显示位置 * 20 - 20, "[" .. self.成员显示[n].职务 .. "]")
				self.文字1:置颜色(黑色):显示(289, 317 + self.显示位置 * 20 - 20, self.成员显示[n].名称)
			end

			self.成员显示[n].包围盒:置坐标(229, 316 + self.显示位置 * 20 - 20)

			if self.成员显示[n].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
				self.选中编号 = n
			end
		end
	end
end

return 帮派类
