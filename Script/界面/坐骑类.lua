local 坐骑类 = class()

function 坐骑类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2910-1.png")
	self.背景2 = 图像类("imge/001/2910-1.png")
	self.背景2:置区域(0, 360, 180, 60)
	self.选中背景 = 图像类("imge/005/2.jpg")
	self.骑乘 = 按钮类.创建("imge/001/0016.png", 2, "骑乘", 3)
	self.装饰 = 按钮类.创建("imge/001/0016.png", 2, "合体", 3)
	self.观看 = 按钮类.创建("imge/001/0016.png", 2, "观看", 3)
	self.修炼 = 按钮类.创建("imge/001/0016.png", 2, "修炼", 3)
	self.驯养 = 按钮类.创建("imge/001/0016.png", 2, "驯养", 3)
	self.喂养 = 按钮类.创建("imge/001/0016.png", 2, "喂养", 3)
	self.关闭 = 按钮类.创建("imge/001/0046.png", 1, "升级", 3)
	self.属性按钮 = 按钮类.创建("imge/001/0015.png", 2, "更改属性", 3)
	self.加点按钮 = {}
	self.减点按钮 = {}

	for n = 1, 5, 1 do
		self.加点按钮[n] = 按钮类.创建("imge/001/0013.png", 1, 1, 4)
		self.减点按钮[n] = 按钮类.创建("imge/001/0014.png", 1, 1, 4)
	end
end

function 坐骑类:刷新指定坐骑(数据)
	x = 360
	y = 80
	self.坐骑数据[数据.编号] = 数据.坐骑
	self.坐骑数据[数据.编号].包围盒 = 包围盒:创建(x - 300 + 150, y - 50 + 192, 64, 18)

	self.坐骑数据[数据.编号].包围盒:置坐标(x - 300 + 320, y - 50 + 240 + 数据.编号 * 20)
	self.坐骑数据[数据.编号].包围盒:更新宽高(string.len(self.坐骑数据[数据.编号].类型) * 9, 18)
end

function 坐骑类:刷新坐骑(数据)
	self.本类开关 = true
	self.坐骑数据 = 数据.坐骑
	self.参战编号 = 数据.参战
	self.合体编号 = 数据.合体

	if self.参战编号 == nil then
		self.参战编号 = 0
	end

	self.文字 = 文字类(simsun, 16):置颜色(黑色)

	x = 360
	y = 80
	for n = 1, #self.坐骑数据, 1 do
		self.坐骑数据[n].包围盒 = 包围盒:创建(x - 300 + 150, y - 50 + 192, 64, 18)

		self.坐骑数据[n].包围盒:置坐标(x - 300 + 320, y - 50 + 240 + n * 20)
		self.坐骑数据[n].包围盒:更新宽高(string.len(self.坐骑数据[n].类型) * 9, 18)
	end

	self.选中编号 = 0
end

function 坐骑类:创建动画()
	self.主角类型 = tp.屏幕.主角.数据.造型
	self.主角染色 = tp.屏幕.主角.数据.染色

	引擎.置ini("坐骑动画.ini")

	self.坐骑 = {}
	self.临时字符 = self.坐骑数据[self.选中编号].类型 .. "_" .. "原始" .. "_" .. "站立"
	self.临时资源 = 引擎.读ini("原始坐骑", self.临时字符)
	self.坐骑动画 =引擎.场景.资源:载入("ZY.FT","网易WDF动画",self.临时资源)


	if self.参战编号 ~= self.选中编号 then
		self.人物动画 = nil
	else
		引擎.置ini("坐骑动画.ini")

		self.坐骑 = {}
		self.临时字符 = self.主角类型 .. "_" .. self.坐骑数据[self.选中编号].类型 .. "_" .. "站立"
		-- self.临时资源 = 引擎.读ini("人物", self.临时字符)
		-- self.人物动画 = WAS动画类.创建(shape5, self.临时资源)
	end
end

function 坐骑类:更新(dt)
	for n = 1, #self.坐骑数据, 1 do
		if self.坐骑数据[n].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(左键) then
			self.选中编号 = n

			self:创建动画()

			if n == self.参战编号 then
				self.骑乘.文字 = "下骑"
			else
				self.骑乘.文字 = "骑乘"
			end

			if n == self.合体编号 then
				self.装饰.文字 = "解体"
			else
				self.装饰.文字 = "合体"
			end

		end
	end

	if self.坐骑动画 ~= nil then
		self.坐骑动画:更新(dt)
	end

	if self.人物动画 ~= nil then
		self.人物动画:更新(dt)
	end

	x = 360
	y = 80
	self.骑乘:更新(x - 300 + 315, y - 50 + 230)
	self.装饰:更新(x - 300 + 375, y - 50 + 230)
	self.观看:更新(x - 300 + 435, y - 50 + 230)
	self.修炼:更新(x - 300 + 315, y - 50 + 230)
	self.驯养:更新(x - 300 + 375, y - 50 + 230)
	self.喂养:更新(x - 300 + 435, y - 50 + 230)
	self.关闭:更新(x - 300 + 652, y - 50 + 54)
	self.属性按钮:更新(x - 300 + 590, y - 50 + 253)

	if self.骑乘:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选择一只坐骑")

			return 0
		else
			客户端:发送数据(self.选中编号, 6, 48, "9", 1)
		end
	elseif self.驯养:取是否单击() then
		if self.选中编号 == 0 then
			信息提示:加入提示("#y/请先选择一只坐骑")

			return 0
		else
			客户端:发送数据(self.选中编号, 6, 49, "9", 1)
		end
	elseif self.关闭:取是否单击() then
		self.本类开关 = false
	end

	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(右键) then
		self.本类开关 = false
	end

	for n = 1, 5, 1 do
		self.加点按钮[n]:更新(x - 300 + 630, y - 50 + 107 + n * 24)
		self.减点按钮[n]:更新(x - 300 + 653, y - 50 + 107 + n * 24)

		if self.加点按钮[n]:取是否单击() then
			if self.选中编号 == 0 then
				信息提示:加入提示("#y/请先选择一只坐骑")

				return 0
			else
				客户端:发送数据(self.选中编号, n, 50, "9", 1)
			end
		end
	end
end

function 坐骑类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 坐骑类:显示(x, y)
	x = 360
	y = 80
	self.背景:显示(x, y)
	self.背景2:显示(x, y + 300)
	self.骑乘:显示(x - 300 + 315, y - 50 + 230)
	self.装饰:显示(x - 300 + 375, y - 50 + 230)
	self.观看:显示(x - 300 + 435, y - 50 + 230)
	self.修炼:显示(x - 300 + 500, y - 50 + 440)
	self.驯养:显示(x - 300 + 560, y - 50 + 440)
	self.喂养:显示(x - 300 + 620, y - 50 + 440)
	self.关闭:显示(x - 300 + 652, y - 50 + 54)
	self.属性按钮:显示(x - 300 + 590, y - 50 + 253)

	for n = 1, 5, 1 do
		self.加点按钮[n]:显示(x - 300 + 630, y - 50 + 107 + n * 24)
		self.减点按钮[n]:显示(x - 300 + 653, y - 50 + 107 + n * 24)
	end

	for n = 1, #self.坐骑数据, 1 do
		if self.选中编号 == n then
			self.选中背景:显示(x - 300 + 320, y - 50 + 240 + n * 20)
			self.文字:显示(x - 300 + 533, y - 50 + 82, self.坐骑数据[n].类型)
			self.文字:显示(x - 300 + 533, y - 50 + 107, self.坐骑数据[n].等级)
			self.文字:显示(x - 300 + 533, y - 50 + 132, self.坐骑数据[n].体质)
			self.文字:显示(x - 300 + 533, y - 50 + 157, self.坐骑数据[n].魔力)
			self.文字:显示(x - 300 + 533, y - 50 + 182, self.坐骑数据[n].力量)
			self.文字:显示(x - 300 + 533, y - 50 + 207, self.坐骑数据[n].耐力)
			self.文字:显示(x - 300 + 533, y - 50 + 232, self.坐骑数据[n].敏捷)
			self.文字:显示(x - 300 + 533, y - 50 + 257, self.坐骑数据[n].潜能)
			self.文字:显示(x - 300 + 583, y - 50 + 282, self.坐骑数据[n].成长)
			self.文字:显示(x - 300 + 583, y - 50 + 307, self.坐骑数据[n].环境度)
			self.文字:显示(x - 300 + 583, y - 50 + 332, self.坐骑数据[n].好感度)
			self.文字:显示(x - 300 + 583, y - 50 + 357, self.坐骑数据[n].饱食度)
			self.文字:显示(x - 300 + 563, y - 50 + 382, self.坐骑数据[n].当前经验)
			self.文字:显示(x - 300 + 563, y - 50 + 407, self.坐骑数据[n].升级经验)
		end

		self.文字:显示(x - 300 + 320, y - 50 + 240 + n * 20, self.坐骑数据[n].类型)
	end

	if self.坐骑动画 ~= nil then
		self.坐骑动画:显示(x - 300 + 390, y - 50 + 200)

		if self.人物动画 ~= nil then
			self.人物动画:显示(x - 300 + 390, y - 50 + 200)
		end

		引擎.场景.影子:显示(x - 300 + 390, y - 50 + 200)
	end
end

return 坐骑类
