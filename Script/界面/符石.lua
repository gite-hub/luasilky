
local 场景类_符石 = class()
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标按下

function 场景类_符石:取符石介绍(数据)
  local jies = ""
    if 数据.名称=="无心插柳" then
	    if 数据.等级 == 1 then
	        jies="目标的15%受到伤害,溅射的随机目标。"
	    elseif 数据.等级 == 2 then
	        jies="目标的20%受到伤害,溅射的随机目标。"
	    elseif 数据.等级 == 3 then
	      jies="目标的25%受到伤害,溅射的随机目标。"
	    elseif 数据.等级 == 4 then
	      jies="目标的30%受到伤害,溅射的随机目标。"
	    end
    elseif 数据.名称=="万丈霞光" then
	    if 数据.等级 == 1 then
	        jies="增加50点恢复气血效果，包括师门技能和特技"
	    elseif 数据.等级 == 2 then
	        jies="增加80点恢复气血效果，包括师门技能和特技"
	    elseif 数据.等级 == 3 then
	      jies="增加120点恢复气血效果，包括师门技能和特技"
	    elseif 数据.等级 == 4 then
	      jies="增加200点恢复气血效果，包括师门技能和特技"
	    end
    elseif 数据.名称=="百步穿杨" then
	    if 数据.等级 == 1 then
	        jies="物理类攻击时有20%的几率给目标额外造成200点伤害"
	    elseif 数据.等级 == 2 then
	        jies="物理类攻击时有20%的几率给目标额外造成450点伤害"
	    elseif 数据.等级 == 3 then
	      jies="物理类攻击时有20%的几率给目标额外造成600点伤害"
	    elseif 数据.等级 == 4 then
	      jies="物理类攻击时有20%的几率给目标额外造成800点伤害"
	    end
	elseif 数据.名称=="隔山打牛" then
	    if 数据.等级 == 1 then
	        jies="法术攻击时有20%的几率临时提升自身80点灵力"
	    elseif 数据.等级 == 2 then
	        jies="法术攻击时有20%的几率临时提升自身120点灵力"
	    elseif 数据.等级 == 3 then
	      jies="法术攻击时有25%的几率临时提升自身170点灵力"
	    elseif 数据.等级 == 4 then
	      jies="法术攻击时有25%的几率临时提升自身200点灵力"
	    end
    elseif 数据.名称=="心随我动" then
	    if 数据.等级 == 1 then
	        jies="遭受物理类攻击时有25%几率抵挡250点伤害"
	    elseif 数据.等级 == 2 then
	        jies="遭受物理攻击时有25%几率抵挡400点伤害"
	    elseif 数据.等级 == 3 then
	      jies="遭受物理类攻击时有25%几率抵挡700点伤害"
	    elseif 数据.等级 == 4 then
	      jies="遭受物理类攻击时有25%几率抵挡900点伤害"
	    end
	elseif 数据.名称=="云随风舞" then
	    if 数据.等级 == 1 then
	        jies="遭受法术攻击时有20%几率抵挡200点伤害"
	    elseif 数据.等级 == 2 then
	        jies="遭受法术攻击时有20%几率抵挡400点伤害"
	    elseif 数据.等级 == 3 then
	      jies="遭受法术攻击时有20%几率抵挡700点伤害"
	    elseif 数据.等级 == 4 then
	      jies="遭受法术攻击时有20%几率抵挡800点伤害"
	    end
 	elseif 数据.名称=="无懈可击" then
        jies="提升自身30点防御力"
  	elseif 数据.名称=="望穿秋水" then
        jies="提升自身30点灵力"
  	elseif 数据.名称=="万里横行" then
        jies="提升自身40点伤害"
  	elseif 数据.名称=="日落西山" then
        jies="提升自身40点速度"
  	elseif 数据.名称=="网罗乾坤" then
		if 数据.等级 == 1 then
		jies="使用天罗地网时，增加人物等级/2的伤害"
		elseif 数据.等级 == 2 then
		jies="使用天罗地网时，增加人物等级/1.5的伤害"
		elseif 数据.等级 == 3 then
		jies="使用天罗地网时，增加人物等级的伤害"
		end
    elseif 数据.名称=="石破天惊" then
	    if 数据.等级 == 1 then
	        jies="使用落雷符时增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用落雷符时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用落雷符时增加人物等级的伤害，装备该组合时降低5%的防御"
	    end
    elseif 数据.名称=="天雷地火" then
	    if 数据.等级 == 1 then
	        jies="使用天雷斩、雷霆万钧时增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用天雷斩、雷霆万钧时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用天雷斩、雷霆万钧时增加人物等级的伤害，装备该组合时降低5%的防御"
	    end
    elseif 数据.名称=="烟雨飘摇" then
	    if 数据.等级 == 1 then
	        jies="使用烟雨剑法、飘渺式时增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用烟雨剑法、飘渺式时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用烟雨剑法、飘渺式时增加人物等级的伤害，装备该组合时降低5%的防御"
	    end
    elseif 数据.名称=="索命无常" then
	    if 数据.等级 == 1 then
	        jies="使用阎罗令时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用阎罗令时，增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用阎罗令时，增加人物等级的伤害，装备该组合时降低5%的防御"
	    end
    elseif 数据.名称=="行云流水" then
	    if 数据.等级 == 1 then
	        jies="使用五行法术时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用五行法术时，增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用五行法术时，增加人物等级的伤害，装备该组合时降低5%的防御"
	    end
    elseif 数据.名称=="福泽天下" then
	    if 数据.等级 == 1 then
	        jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 2 then
	        jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    elseif 数据.等级 == 3 then
	      jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
	    end
	elseif 数据.名称=="暗度陈仓" then
		jies="受到物理攻击时，降低3%的所受伤害。"
	elseif 数据.名称=="点石成金" then
		jies="防御时，遭受物理攻击所受到的伤害降低20%"
	elseif 数据.名称=="化敌为友" then
		jies="受到法术攻击时，降低3%的所受伤害。"
	elseif 数据.名称=="百步穿杨" then
		jies="物理类攻击时有20%的几率给目标额外造成850点伤害"
	elseif 数据.名称=="凤舞九天" then
		jies="提升自身500点灵力"
	elseif 数据.名称=="君临天下" then
		jies="提升自身500点伤害"
	elseif 数据.名称=="势如破竹" then
		jies="提升自身500点敏捷"
	elseif 数据.名称=="不动明王" then
		jies="提升自身300点防御和500点体质"
    elseif 数据.名称=="高山流水" then
	    if 数据.等级 == 1 then
	        jies="增加人物等级/3+30的法术伤害。(该组合全身只有一件装备起效)"
	    elseif 数据.等级 == 2 then
	        jies="增加人物等级/2+30的法术伤害。(该组合全身只有一件装备起效)"
	    elseif 数据.等级 == 3 then
	      jies="增加人物等级+30的法术伤害。(该组合全身只有一件装备起效)"
	    end
    elseif 数据.名称=="百无禁忌" then
	    if 数据.等级 == 1 then
	        jies="提高自身4%对抗封印类技能的能力"
	    elseif 数据.等级 == 2 then
	        jies="提高自身8%对抗封印类技能的能力"
	    elseif 数据.等级 == 3 then
	      jies="提高自身12%对抗封印类技能的能力"
	    end
   end
  return jies
end


function 场景类_符石:初始化(根)
	tp= 根
	self.ID = 57
	self.x = 224
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.本类开关 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[0] = 资源:载入('JM.FT',"网易WDF动画",0x444EB0FF),--背景
		[1] = 资源:载入('JM.FT',"网易WDF动画",0x0B9E4143),--背景1
		[2] = 资源:载入('JM.FT',"网易WDF动画",0xB0C4E507),--背景2
		[3] = 资源:载入('JM.FT',"网易WDF动画",0x685ABA69),--背景3
		[4] = 资源:载入('JM.FT',"网易WDF动画",0x431EEEC5),--背景4
		[5] = 资源:载入('JM.FT',"网易WDF动画",0xC53B87D1),--背景5
		[6] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"镶嵌"),
		[7] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"还原"),
		[8] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[9] = 按钮.创建(资源:载入('JM.FT',"网易WDF动画",0x786AC306)),
	}
	for n=6,9 do
	   self.资源组[n]:绑定窗口_(57)
	end

	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.道具字体
    ff = 根._丰富文本
	self.介绍文本 = ff(189,480,根.字体表.普通字体)
  self.选中符石 ={}
	self.物品 = {}
	for i=1,5 do
		self.选中符石[i] = 0
	end
	self.符石数量 =0
end



function 场景类_符石:打开(数据,说明,大动画,格子)
	if self.本类开关 then
       self.介绍文本:清空()
		self.本类开关 = false
self.物品={}
       	for i=1,5 do

		self.选中符石[i] = 0
		end
       self.符石数量 =0


	else

		self.格子 = 格子
       self.大动画= 大动画
		self.数据=数据
       self.说明 = 说明
		self.符石数量 =#self.数据

		self.介绍文本:添加文本("")
		if 数据.符石 ~=nil then

			for i=1,5 do
			 if 数据.符石[i] ~=nil then
			  self.物品[i]=物品数据类.创建(数据.符石[i],3,"包裹",i)
			  self.物品[i].编号 =i
			  self.选中符石[i] = 0
			end
			end
	   end
       self:刷新属性(self.物品)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.本类开关 = true
	end
end
function 场景类_符石:更新(dt)
		self.资源组[9]:更新(鼠标.x,鼠标.y)
	self.资源组[8]:更新(鼠标.x,鼠标.y)
	self.资源组[6]:更新(鼠标.x,鼠标.y,self.符石数量 ~= 0)
	self.资源组[7]:更新(鼠标.x,鼠标.y,self.符石数量 ~= 0)
	if self.资源组[8]:事件判断() or (self:界面重叠()and 引擎.鼠标按下(0x1)) and self.焦点1 == false then
		self:打开()
	elseif self.资源组[6]:事件判断() then

   客户端:发送数据(self.格子,self.格子,106,self.选中符石[1].."*-*"..self.选中符石[2].."*-*"..self.选中符石[3].."*-*"..self.选中符石[4].."*-*"..self.选中符石[5])
    self:打开()
	elseif  self.资源组[7]:事件判断() then
			self.物品 ={}
			if self.数据.符石 ~=nil then
				for i=1,5 do
					if self.数据.符石[i]~=nil then
					self.物品[i]=物品数据类.创建(self.数据.符石[i],3,"包裹",i)
					self.选中符石[i]=0
					end
				end
			end
			self.符石数量 =#self.数据
			self:刷新属性(self.物品)
	end

    tp.符石边框:更新(dt)
	self.焦点 = false
	self.焦点1 = false

end
function 场景类_符石:拖拽()
  if 引擎.鼠标按下(KEY.LBUTTON) then
    self.tx,self.ty = self.x,self.y    --图原始坐标先保存
    self.xx,self.yy = 引擎.取鼠标坐标()   --按下时的坐标
  end
  if 引擎.鼠标按住(KEY.LBUTTON) then
    local x,y = 引擎.取鼠标坐标()           ---随时移动的最后结果坐标
    if(self.xx==nil or self.yy==nil) then   ---未移动过时为空
      self.tx,self.ty = self.x,self.y
      self.xx,self.yy = 引擎.取鼠标坐标()
    end
    self.x =  self.tx+(x - self.xx)       --坐标差
    self.y =  self.ty+(y - self.yy)
  end
end

function 场景类_符石:显示(dt,x,y)
	if self:界面重叠() then
	 self:拖拽()

	end
	self.资源组[0]:显示(self.x,self.y)
	self.资源组[6]:显示(self.x+109,self.y+445)
	self.资源组[7]:显示(self.x+200,self.y+445)
	self.资源组[8]:显示(self.x+339,self.y+9)
	self.资源组[9]:显示(self.x+60,self.y+220)
    self.大动画:显示(self.x+30,self.y+80)
    zts1:置颜色(0xFFFFFF00)
    zts1:显示(self.x+148,self.y+50,self.数据.名称)

    self.介绍文本:显示(self.x+148,self.y+75)
 if  self.数据.符石 ~= nil then
    if self.数据.符石.最小孔数 == 1 then
	 	self.资源组[1]:显示(self.x+158,self.y+382)
	 	if self.物品[1] ~=nil then
		 self.物品[1]:显示(self.x+166,self.y+387)
         tp.符石边框:显示(22+self.x+166,24+self.y+387)
	 	end
  elseif self.数据.符石.最小孔数 == 2 then
	  	self.资源组[1]:显示(self.x+108,self.y+382)
	  	if self.物品[1] ~=nil then
	     self.物品[1]:显示(self.x+114,self.y+387)
	      tp.符石边框:显示(22+self.x+114,24+self.y+387)
	 	end
	  	self.资源组[2]:显示(self.x+208,self.y+382)
	  	if self.物品[2] ~=nil then
		 self.物品[2]:显示(self.x+214,self.y+387)
		  tp.符石边框:显示(22+self.x+214,24+self.y+387)
	 	end
  elseif self.数据.符石.最小孔数 == 3 then
	  	self.资源组[1]:显示(self.x+58,self.y+382)
	  	 if self.物品[1] ~=nil then
	     self.物品[1]:显示(self.x+64,self.y+387)
	      tp.符石边框:显示(22+self.x+64,24+self.y+387)
	 	end
	  	self.资源组[2]:显示(self.x+158,self.y+382)
	  	 	if self.物品[2] ~=nil then
	     self.物品[2]:显示(self.x+164,self.y+387)
           tp.符石边框:显示(22+self.x+164,24+self.y+387)
	 	end
	  	self.资源组[3]:显示(self.x+258,self.y+382)
	  	 	if self.物品[3] ~=nil then
	     self.物品[3]:显示(self.x+264,self.y+387)
	     tp.符石边框:显示(22+self.x+264,24+self.y+387)
	 	end
  elseif self.数据.符石.最小孔数 == 4 then
	  	self.资源组[1]:显示(self.x+48,self.y+382)
	  	 	if self.物品[1] ~=nil then
		 self.物品[1]:显示(self.x+54,self.y+387)
		 tp.符石边框:显示(22+self.x+54,24+self.y+387)
	 	end
	  	self.资源组[2]:显示(self.x+122,self.y+382)
	  	 	if self.物品[2] ~=nil then
		 self.物品[2]:显示(self.x+128,self.y+387)
		 tp.符石边框:显示(22+self.x+128,24+self.y+387)
	 	end
	  	self.资源组[3]:显示(self.x+197,self.y+382)
	  	 if self.物品[3] ~=nil then
	     self.物品[3]:显示(self.x+203,self.y+387)
	     tp.符石边框:显示(22+self.x+203,24+self.y+387)
	 	end
	  	self.资源组[4]:显示(self.x+268,self.y+382)
	  	 if self.物品[4] ~=nil then
	     self.物品[4]:显示(self.x+274,self.y+387)
	     tp.符石边框:显示(22+self.x+274,24+self.y+387)
	 	end
  elseif self.数据.符石.最小孔数 == 5 then
	  	self.资源组[1]:显示(self.x+38,self.y+382)
	  	 	if self.物品[1] ~=nil then
	     self.物品[1]:显示(self.x+44,self.y+387)
	     tp.符石边框:显示(22+self.x+44,24+self.y+387)
	 	end
	  	self.资源组[2]:显示(self.x+98,self.y+382)
	  	 	if self.物品[2] ~=nil then
	     self.物品[2]:显示(self.x+104,self.y+387)
	     tp.符石边框:显示(22+self.x+104,24+self.y+387)
	 	end
	  	self.资源组[3]:显示(self.x+158,self.y+382)
	  	 if self.物品[3] ~=nil then
	     self.物品[3]:显示(self.x+164,self.y+387)
	     tp.符石边框:显示(22+self.x+164,24+self.y+387)
	 	end
	  	self.资源组[4]:显示(self.x+218,self.y+382)
	  	 	if self.物品[4] ~=nil then
	     self.物品[4]:显示(self.x+224,self.y+387)
	     tp.符石边框:显示(22+self.x+224,24+self.y+387)
	 	end
	  	self.资源组[5]:显示(self.x+278,self.y+382)
	  	 	if self.物品[5] ~=nil then
	     self.物品[5]:显示(self.x+284,self.y+387)
	     tp.符石边框:显示(22+self.x+284,24+self.y+387)
	 	end
 end
end

  for i=1,5 do

    if self.资源组[i]:是否选中(鼠标.x,鼠标.y)  then
    	self.焦点1 = true
    	if self.物品[i] ~= nil and self.物品[i]:获取焦点()  then
				self.物品[i]:显示事件(2)
			end
 	    if tp.主界面.界面数据[3].拿起对象 ~=0 and mouseb(0)  and  tp.主界面.界面数据[3].物品数据[tp.主界面.界面数据[3].拿起对象].类型 =="符石"   then
		  self.物品[i]=物品数据类.创建(tp.主界面.界面数据[3].物品数据[tp.主界面.界面数据[3].拿起对象],3,"包裹",tp.主界面.界面数据[3].拿起对象)
         tp.主界面.界面数据[3].物品数据[tp.主界面.界面数据[3].拿起对象] = nil
		         self.选中符石[i] = tp.主界面.界面数据[3].拿起对象
    		 		tp.主界面.界面数据[3].拿起对象 = 0
                 self.符石数量 =self.符石数量+1
                 self:刷新属性(self.物品)

 	    end
 	end
 end

end

function 场景类_符石:刷新属性(物品数据)
	self.介绍文本:清空()
	self.介绍文本:添加文本("#w/"..self.说明)
		self.介绍文本:添加文本("#w/【装备条件】等级"..self.数据.等级)
         if self.数据.符石~= nil and self.数据.符石.最小孔数>0 then
		self.介绍文本:添加文本("#g/开运孔数："..self.数据.符石.最小孔数.."/"..self.数据.符石.最大孔数)
	    else
	    self.介绍文本:添加文本("#g/开运孔数：当前装备还未开孔。")
	    end
 local sx ={}

   for i=1,5 do
        if  物品数据~=nil and self.数据.符石~=nil and  (物品数据[i] ~= nil or self.数据.符石[i] ~= nil)  then
		 	    	sx[i] = ""
					local 气血={}
					气血[i] = 物品数据[i].气血
					local 魔法={}
					魔法[i] = 物品数据[i].魔法
					local 命中={}
					命中[i] = 物品数据[i].命中
					local 伤害={}
					伤害[i] = 物品数据[i].伤害
					local 防御={}
					防御[i] = 物品数据[i].防御
					local 速度={}
					速度[i] = 物品数据[i].速度
					local 躲避={}
					躲避[i] = 物品数据[i].躲避
					local 灵力={}
					灵力[i] = 物品数据[i].灵力
					local 敏捷={}
					敏捷[i] = 物品数据[i].敏捷
					local 体质={}
					体质[i] = 物品数据[i].体质
					local 耐力={}
					耐力[i] = 物品数据[i].耐力
					local 法术防御={}
					法术防御[i] = 物品数据[i].法术防御
					local 魔力={}
					魔力[i] = 物品数据[i].魔力
					local 力量={}
					力量[i] = 物品数据[i].力量
					local 法术伤害={}
					法术伤害[i] = 物品数据[i].法术伤害
					local 固定伤害={}
					固定伤害[i] = 物品数据[i].固定伤害

					if 体质[i] ~= 0 and 体质[i] ~= nil  then
						sx[i] = sx[i].."体质 +"..体质[i].." "
					end
					if 耐力[i] ~= 0 and 耐力[i] ~= nil  then
						sx[i] = sx[i].."耐力 +"..耐力[i].." "
					end
					if 法术防御[i] ~= 0 and 法术防御[i] ~= nil  then
						sx[i] = sx[i].."法术防御 +"..法术防御[i].." "
					end
					if 魔力[i] ~= 0 and 魔力[i] ~= nil  then
						sx[i] = sx[i].."魔力 +"..魔力[i].." "
					end
					if 力量[i] ~= 0 and 力量[i] ~= nil  then
						sx[i] = sx[i].."力量 +"..力量[i].." "
					end
					if 法术伤害[i] ~= 0 and 法术伤害[i] ~= nil  then
						sx[i] = sx[i].."法术伤害 +"..法术伤害[i].." "
					end
					if 固定伤害[i] ~= 0 and 固定伤害[i] ~= nil  then
						sx[i] = sx[i].."固定伤害 +"..固定伤害[i].." "
					end
					if 伤害[i] ~= 0 and 伤害[i] ~= nil  then
						sx[i] = sx[i].."伤害 +"..伤害[i].." "
					end
					if 命中[i] ~= 0  and 命中[i] ~= nil then
						sx[i] = sx[i].."命中 +"..命中[i].." "
					end
					if 防御[i] ~= 0 and 防御[i] ~= nil  then
						sx[i] = sx[i].."防御 +"..防御[i].." "
					end
					if 气血[i] ~= 0 and 气血[i] ~= nil then
						sx[i] = sx[i].."气血 +"..气血[i].." "
					end
					if 魔法[i] ~= 0 and 魔法[i] ~= nil  then
						sx[i] = sx[i].."魔法 +"..魔法[i].." "
					end
					if 速度[i] ~= 0 and 速度[i] ~= nil  then
						sx[i] = sx[i].."速度 +"..速度[i].." "
					end
					if 躲避[i] ~= 0 and 躲避[i] ~= nil  then
						sx[i] = sx[i].."躲避 +"..躲避[i].." "
					end
					if 灵力[i] ~= 0 and 灵力[i] ~= nil  then
						sx[i] = sx[i].."灵力 +"..灵力[i].." "
					end
					if 敏捷[i] ~= 0 and 敏捷[i] ~= nil  then
						sx[i] = sx[i].."敏捷 +"..敏捷[i].." "
					end
					if sx[i] ~= "" then
						self.介绍文本:添加文本("#g/符石:"..sx[i])
					end
       end
   end

    if  self.数据.符石 ~=nil then
        self.数据.符石.组合=nil
    end
if 物品数据[1]~=nil and 物品数据[1].颜色=="绿色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="紫色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="白色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="红色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="黄色" then
           self.数据.符石.组合={名称="万丈霞光",等级=4}
         else
           self.数据.符石.组合={名称="万丈霞光",等级=3}
         end
       else
          self.数据.符石.组合={名称="万丈霞光",等级=2}
       end
     else
        self.数据.符石.组合={名称="万丈霞光",等级=1}
     end
  end
end

if 物品数据[1]~=nil and 物品数据[1].颜色=="黑色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="紫色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="蓝色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="黄色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="绿色" then
           self.数据.符石.组合={名称="百步穿杨",等级=4}
         else
           self.数据.符石.组合={名称="百步穿杨",等级=3}
         end
       else
          self.数据.符石.组合={名称="百步穿杨",等级=2}
       end
     else
        self.数据.符石.组合={名称="百步穿杨",等级=1}
     end
  end
end

if 物品数据[1]~=nil and 物品数据[1].颜色=="白色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="红色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="紫色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="蓝色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="黄色" then
           self.数据.符石.组合={名称="隔山打牛",等级=4}
         else
           self.数据.符石.组合={名称="隔山打牛",等级=3}
         end
       else
          self.数据.符石.组合={名称="隔山打牛",等级=2}
       end
     else
        self.数据.符石.组合={名称="隔山打牛",等级=1}
     end
  end
end

if 物品数据[1]~=nil and 物品数据[1].颜色=="黑色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="蓝色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="红色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="绿色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="白色" then
           self.数据.符石.组合={名称="心随我动",等级=4}
         else
           self.数据.符石.组合={名称="心随我动",等级=3}
         end
       else
          self.数据.符石.组合={名称="心随我动",等级=2}
       end
     else
        self.数据.符石.组合={名称="心随我动",等级=1}
     end
  end
end

if 物品数据[1]~=nil and 物品数据[1].颜色=="白色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黄色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="红色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="绿色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="黑色" then
           self.数据.符石.组合={名称="云随风舞",等级=4}
         else
           self.数据.符石.组合={名称="云随风舞",等级=3}
         end
       else
          self.数据.符石.组合={名称="云随风舞",等级=2}
       end
     else
        self.数据.符石.组合={名称="云随风舞",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="蓝色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="白色" then
        self.数据.符石.组合={名称="望穿秋水",等级=1}
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="黄色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黑色" then
        self.数据.符石.组合={名称="万里横行",等级=1}
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="红色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="紫色" then
        self.数据.符石.组合={名称="日落西山",等级=1}
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="黄色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="绿色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黄色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="黑色" then
           self.数据.符石.组合={名称="网罗乾坤",等级=3}
       else
          self.数据.符石.组合={名称="网罗乾坤",等级=2}
       end
     else
        self.数据.符石.组合={名称="网罗乾坤",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="绿色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="红色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="红色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
           self.数据.符石.组合={名称="石破天惊",等级=3}
       else
          self.数据.符石.组合={名称="石破天惊",等级=2}
       end
     else
        self.数据.符石.组合={名称="石破天惊",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="红色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="红色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="蓝色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="紫色" then
           self.数据.符石.组合={名称="天雷地火",等级=3}
       else
          self.数据.符石.组合={名称="天雷地火",等级=2}
       end
     else
        self.数据.符石.组合={名称="天雷地火",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="蓝色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黄色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黄色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="紫色" then
           self.数据.符石.组合={名称="烟雨飘摇",等级=3}
       else
          self.数据.符石.组合={名称="烟雨飘摇",等级=2}
       end
     else
        self.数据.符石.组合={名称="烟雨飘摇",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="黄色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="蓝色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黄色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
           self.数据.符石.组合={名称="索命无常",等级=3}
       else
          self.数据.符石.组合={名称="索命无常",等级=2}
       end
     else
        self.数据.符石.组合={名称="索命无常",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="绿色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黄色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="红色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="紫色" then
           self.数据.符石.组合={名称="行云流水",等级=3}
       else
          self.数据.符石.组合={名称="行云流水",等级=2}
       end
     else
        self.数据.符石.组合={名称="行云流水",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="黄色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="红色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="蓝色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
           self.数据.符石.组合={名称="福泽天下",等级=3}
       else
          self.数据.符石.组合={名称="福泽天下",等级=2}
       end
     else
        self.数据.符石.组合={名称="福泽天下",等级=1}
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="蓝色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="蓝色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黄色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="黑色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="黑色" then
           self.数据.符石.组合={名称="无心插柳",等级=4}
         else
           self.数据.符石.组合={名称="无心插柳",等级=3}
         end
       else
          self.数据.符石.组合={名称="无心插柳",等级=2}
       end
     else
        self.数据.符石.组合={名称="无心插柳",等级=1}
     end
  end
end




if 物品数据[1]~=nil and 物品数据[1].颜色=="白色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="紫色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="蓝色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="红色" then
           self.数据.符石.组合={名称="高山流水",等级=3}
         else
           self.数据.符石.组合={名称="高山流水",等级=2}
         end
       else
          self.数据.符石.组合={名称="高山流水",等级=1}
       end
     end
  end
end




if 物品数据[1]~=nil and 物品数据[1].颜色=="黑色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="白色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="紫色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="绿色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="红色" then
           self.数据.符石.组合={名称="百无禁忌",等级=3}
         else
           self.数据.符石.组合={名称="百无禁忌",等级=2}
         end
       else
          self.数据.符石.组合={名称="百无禁忌",等级=1}
       end
     end
  end
end








if 物品数据[1]~=nil and 物品数据[1].颜色=="蓝色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="红色" then
        self.数据.符石.组合={名称="无懈可击",等级=1}

  end
end



if 物品数据[1]~=nil and 物品数据[1].颜色=="白色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="蓝色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黄色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="紫色" then
           self.数据.符石.组合={名称="暗度陈仓",等级=3}
         end
       end
     end
  end
end



if 物品数据[1]~=nil and 物品数据[1].颜色=="红色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黄色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="紫色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="紫色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="白色" then
           self.数据.符石.组合={名称="化敌为友",等级=3}
         end
       end
     end
  end
end

if 物品数据[1]~=nil and 物品数据[1].颜色=="紫色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="绿色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黑色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="红色" then
           self.数据.符石.组合={名称="点石成金",等级=3}

       end
     end
  end
end


if 物品数据[1]~=nil and 物品数据[1].颜色=="蓝色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="蓝色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="蓝色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="蓝色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="蓝色" then
           self.数据.符石.组合={名称="不动明王",等级=3}
         end
       end
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="紫色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="紫色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="紫色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="紫色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="紫色" then
           self.数据.符石.组合={名称="势如破竹",等级=3}
         end
       end
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="黑色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="黑色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="黑色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="黑色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="黑色" then
           self.数据.符石.组合={名称="君临天下",等级=3}
         end
       end
     end
  end
end
if 物品数据[1]~=nil and 物品数据[1].颜色=="白色" then
  if 物品数据[2]~=nil and 物品数据[2].颜色 =="白色" then
     if 物品数据[3]~=nil and 物品数据[3].颜色 =="白色" then
       if 物品数据[4]~=nil and 物品数据[4].颜色 =="白色" then
         if 物品数据[5]~=nil and 物品数据[5].颜色 =="白色" then
           self.数据.符石.组合={名称="凤舞九天",等级=3}
         end
       end
     end
  end
end
				if self.数据.符石~=nil and self.数据.符石.组合 ~=nil then
				 	self.介绍文本:添加文本("#f/符石组合："..self.数据.符石.组合.名称)
				 	self.介绍文本:添加文本("#f/门派条件：无")
		            self.介绍文本:添加文本("#f/部位条件：无")
		            self.介绍文本:添加文本("#f/"..self:取符石介绍(self.数据.符石.组合))
			    end

end







function 场景类_符石:界面重叠()
	if self.资源组[0]:是否选中(鼠标.x,鼠标.y)  then
		return true
	end
end



return 场景类_符石