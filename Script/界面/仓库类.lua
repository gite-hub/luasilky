local 仓库类 = class()

function 仓库类:初始化()
	self.背景 = 图像类("imge/001/2870-1.png")
	self.本类开关 = false
	self.按钮字体 = 文字类.创建(simsun, 14)
	self.仓库字体 = 文字类.创建(simsun, 14)

	self.仓库字体:置颜色(黑色)

	self.退出 = 按钮类.创建("imge/001/0016.png", 2, "退出", 3)
	self.道具 = 按钮类.创建("imge/001/0016.png", 2, "道具", 3)
	self.行囊 = 按钮类.创建("imge/001/0016.png", 2, "行囊", 3)
	self.当前仓库 = 1
	self.当前类型 = "包裹"
	self.仓库数量 = 0
	self.仓库数据 = {}
	self.仓库按钮 = {}
end

function 仓库类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 仓库类:刷新仓库(数据)
	self.仓库按钮 = {}
	self.仓库数量 = 数据.总数
	for n = 1, 数据.总数, 1 do
		self.仓库按钮[n] = 按钮类.创建("imge/001/0050.png", 2, n, 3)
	end

	self.当前仓库 = 数据.类型
	self.仓库物品 = {}
	self.仓库物品 = {
		[self.当前仓库] = {}
	}

	for n = 1, 20, 1 do


		if 数据[n] ~= nil and 数据[n].道具 ~= nil then
			self.仓库物品[self.当前仓库][n] = 物品数据类.创建(数据[n].道具, 3, self.当前类型, n)
			self.仓库物品[self.当前仓库][n].编号 = 数据[n].编号
		end
	end
end

function 仓库类:刷新(数据)
	self.当前类型 = 数据.类型
	self.本类开关 = true
	self.物品数据 = {}
	self.仓库物品 = {
		[self.当前仓库] = {}
	}

	for n = 1, 20, 1 do


		if 数据[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(数据[n], 3, self.当前类型, n)
			self.物品数据[n].编号 = 数据[n].编号
		end
	end
end

function 仓库类:更新(dt)
	self.退出:更新(dt)
	self.道具:更新(dt)
	self.行囊:更新(dt)

	if self.退出:取是否单击(dt) then
		self.本类开关 = false
	elseif self.道具:取是否单击(dt) then
		客户端:发送数据(6, 11, 13, "包裹", 2)
	elseif self.行囊:取是否单击(dt) then
		客户端:发送数据(6, 11, 13, "行囊", 2)
	end

	for n = 1, #self.仓库按钮, 1 do
		self.仓库按钮[n]:更新(dt)

		if self.仓库按钮[n]:取是否单击(dt) then
			客户端:发送数据(n, 12, 13, "11", 2)
		end
	end
end

function 仓库类:存放事件(id)
	self.发送信息 = {
		格子 = id,
		当前类型 = self.当前类型,
		仓库 = self.当前仓库
	}

	客户端:发送数据(id, self.当前仓库, 20, self.当前类型)
end

function 仓库类:拿走事件(id)
	self.发送信息 = {
		格子 = id,
		当前类型 = self.当前类型,
		仓库 = self.当前仓库
	}

	客户端:发送数据(id, self.当前仓库, 21, self.当前类型)
end

function 仓库类:显示(x, y)
	-- local a, b = 引擎.是否锁定(0), 引擎.是否锁定(1)
	-- 引擎.重置锁定(0)
	-- 引擎.重置锁定(1)
	self.背景:显示(100, 150)
	self.退出:显示(625, 398)
	self.道具:显示(525, 398)
	self.行囊:显示(575, 398)
	self.仓库字体:显示(181, 423, self.当前仓库)
	self.仓库字体:显示(181, 398, self.仓库数量)

	self.道具y = 182
	self.道具x = 400
	self.计数 = 0
	self.y = 395
	self.x = 200

	for n = 1, self.仓库数量, 1 do
		self.计数 = self.计数 + 1

		if self.计数 > 8 and self.y == 395 then
			self.计数 = 1
			self.y = self.y + 28
		end

		self.x = 200 + 25 * self.计数 - 10

		self.仓库按钮[n]:显示(self.x, self.y)
	end

	self.道具y = 182
	self.道具x = 400
	self.计数 = 0
	self.y = 395
	self.x = 200
	self.道具选中 = 0
	self.仓库选中 = 0
	self.计数 = 0

	for n = 1, 20, 1 do
		self.计数 = self.计数 + 1

		if self.计数 > 5 then
			self.计数 = 1
			self.道具y = self.道具y + 50
		end

		self.道具x = 400 + self.计数 * 50 - 43

		if self.物品数据[n] ~= nil then
			self.物品数据[n]:显示(self.道具x, self.道具y)

			if self.物品数据[n]:获取焦点() then
				self.道具选中 = n
			end
		end
	end

	if self.道具选中 ~= 0 then
		self.物品数据[self.道具选中]:显示事件(2)

		if 引擎.鼠标弹起(键盘符号.右键) then
			self:存放事件(self.道具选中)
			-- b = true
		end
	end

	self.道具y = 182
	self.道具x = 110
	self.计数 = 0
	self.仓库位置 = {
		x = {},
		y = {}
	}

	for n = 1, 20, 1 do
		self.计数 = self.计数 + 1

		if self.计数 > 5 then
			self.计数 = 1
			self.道具y = self.道具y + 50
		end

		self.道具x = 110 + self.计数 * 50 - 43

		if self.仓库物品[self.当前仓库][n] ~= nil then
			self.仓库物品[self.当前仓库][n]:显示(self.道具x, self.道具y)

			if self.仓库物品[self.当前仓库][n]:获取焦点() then
				self.仓库选中 = n
			end
		end
	end

	if self.仓库选中 ~= 0 then
		self.仓库物品[self.当前仓库][self.仓库选中]:显示事件(2)

		if 引擎.鼠标弹起(键盘符号.右键) then
			self:拿走事件(self.仓库物品[self.当前仓库][self.仓库选中].编号)
			b = true
		end
	end

	if self.道具选中 == 0 and self.仓库选中 == 0 and 引擎.鼠标弹起(键盘符号.右键) and self:界面重叠() then
		local x, y  = 鼠标.x - self.背景.x - 16, 鼠标.y - self.背景.y - 32
		if(x < 0 or y < 0 or x > 260 or y > 210) then
			x = x - 290
			if(x < 0 or x > 260) then self.本类开关 = false end
		end

		-- b = true
	end
	-- 引擎.鼠标锁定(0)
	-- if(b) then 引擎.鼠标锁定(1) end
end

return 仓库类
