local 主界面类 = class()

local tp
function 主界面类:初始化(根)
	tp=根
	 local 资源 = tp.资源
	 local 按钮 = tp._按钮
    self.格子={}
  for i=1,8 do
    self.格子[i] = 资源:载入('JM.FT',"网易WDF动画",0x0493FA27)
  end
  self.快捷技能显示 =false
  zts1 = tp.字体表.描边字体
  self.图片组 = {}
	self.数据显示 = true
	self.全屏模式 = false
	self.位置y = 450
	self.位置x = 545
	技能 = {召唤兽 = {}}
 技能.召唤兽.图标={}
 for n=1,95 do

     self.a=733+n
     技能.召唤兽.图标[n]= 图像类("imge/003/".. self.a.."-1.png") --技能图标
     end
	 self:创建界面(根)
end
function 主界面类:创建界面(lo)
	self.界面数据 = {
	          [1]= require("Script/界面/人物界面类").创建(lo),
              [2]= require("Script/界面/召唤兽界面类").创建(lo),
              [3]= require("Script/界面/包裹类").创建(lo),
              [4]= require("Script/界面/炼妖类").创建(lo),
              [5]= require("Script/界面/装备制造类").创建(lo),
              [6]= require("Script/界面/称谓类").创建(lo),
              [7]= require("Script/界面/队伍类").创建(lo),
              [8]= require("Script/界面/对话框类").创建(lo),
              [9]= require("Script/界面/商店类").创建(lo),
              [10] = require("Script/界面/飞行符类").创建(lo),
              [11] = require("Script/界面/玩家给予类").创建(lo),
              [12] = require("Script/界面/技能学习类").创建(lo),
              [13] = require("Script/界面/任务栏").创建(lo),----
              [14] = require("Script/界面/仓库类").创建(lo),
              [15] = require("Script/界面/游戏公告类").创建(lo),
              [16] = require("Script/界面/辅助技能学习类").创建(lo),
              [17] = require("Script/界面/强化技能学习类").创建(lo),
              [18] = require("Script/界面/系统给予类").创建(lo),
              [19] = require("Script/界面/玩家交易类").创建(lo),
              [20] = require("Script/界面/玩家交易类").创建(lo),
              [21] = require("Script/界面/召唤兽显示类").创建(lo),
              [22] = require("Script/界面/推广员填写类").创建(lo),
              [23] = require("Script/界面/宝箱解密类").创建(lo),
              [24] = require("Script/界面/商会管理类").创建(lo),
              [25] = require("Script/界面/商会购买类").创建(lo),
              [26] = require("Script/界面/装备修理类").创建(lo),
              [27] = require("Script/界面/生活技能学习类").创建(lo),
              [28] = require("Script/界面/钱庄存钱类").创建(lo),
              [29] = require("Script/界面/钱庄取钱类").创建(lo),
              [30] = require("Script/界面/系统消息类").创建(lo),
              [31] = require("Script/界面/摆摊出售类").创建(lo),
              [32] = require("Script/界面/摆摊购买类").创建(lo),
              [33] = require("Script/界面/技能查看类").创建(lo),
              [34] = require("Script/界面/合成旗类").创建(lo),
              [35] = require("Script/界面/系统设置类").创建(lo),
              [36] = require("Script/界面/加入帮派类").创建(lo),
              [37] = require("Script/界面/帮派类").创建(lo),
              [38] = require("Script/界面/帮派名称填写类").创建(lo),
              [39] = require("Script/界面/帮派宗旨填写类").创建(lo),
              [40] = require("Script/界面/帮派申请列表类").创建(lo),
              [41] = require("Script/界面/帮派成员类").创建(lo),
              [42] = require("Script/界面/帮派信息类").创建(lo),
              [43] = require("Script/界面/坐骑类").创建(lo),
              [44] = require("Script/界面/召唤兽资质栏").创建(lo),
              [45] = require("Script/界面/商城").创建(lo),
              [46] = require("Script/界面/任务追踪").创建(lo),---
              [47] = require("Script/界面/商城").创建(lo),
              [48] = require("Script/界面/符石").创建(lo),
              [49] = require("Script/界面/开运界面").创建(lo),
              [50] = require("Script/界面/符石合成").创建(lo),
              [51] = require("Script/界面/改名").创建(lo),
              [52] = require("Script/界面/排行榜类").创建(lo),
	}

  self.层序组 = {}
  for n = 1, #self.界面数据 do
    self.层序组[n] = n
  end
  self.top_for_map = 0
end
function 主界面类:界面重叠()
	for n = 1, #self.界面数据, 1 do
		if self.界面数据[n].本类开关 and self.界面数据[n]:界面重叠() then
			return true
		end
	end
    if self.界面数据[46]:检查按钮点(鼠标.x, 鼠标.y) then
     return  true
   end
	if self:本类重叠() then
		return true
	end

	return false
end

function 主界面类:界面重叠1()
	for n = 1, #self.界面数据, 1 do
		if self.界面数据[n].本类开关 and n ~= 3 and self.界面数据[n]:界面重叠() then
			return true
		end
	end

	return false
end


function 主界面类:本类重叠()
	if tp.窗口.底图框:检查点(鼠标.x, 鼠标.y) then
		return true
	elseif tp.窗口.人物框:检查点(鼠标.x, 鼠标.y) then
		return true
	elseif self.快捷技能显示 then
      -- bw:检查点(鼠标.x,鼠标.y) then
      for i=1,8 do
         -- if require("gge包围盒")(self.格子[i].x,self.格子[i].y,self.格子[i].宽度,self.格子[i].高度):检查点(鼠标.x, 鼠标.y) then
         if(self.格子[i]:是否选中(鼠标.x, 鼠标.y))then
            return true
        end
      end
  end
	return false
end

function 主界面类:更新(dt,x, y)

   if  self.抓取技能~= nil and 引擎.鼠标弹起(0x1) then
            self.抓取技能 =nil
          self.抓取技能id = nil
          self.抓取技能类型= nil
  end
if tp.战斗中 and self.快捷技能栏数据~=nil  then -- yxjc== 3 and  self.快捷技能显示 and
  if self.快捷技能栏数据[1]~=nil and 引擎.按键按下(KEY.F1)  then
    tp.战斗类:快捷技能(self.快捷技能栏数据[1].名称)
  elseif self.快捷技能栏数据[2]~=nil and 引擎.按键按下(KEY.F2)  then
    tp.战斗类:快捷技能(self.快捷技能栏数据[2].名称)
  elseif self.快捷技能栏数据[3]~=nil and 引擎.按键按下(KEY.F3)  then
   tp.战斗类:快捷技能(self.快捷技能栏数据[3].名称)
  elseif self.快捷技能栏数据[4]~=nil and 引擎.按键按下(KEY.F4)  then
   tp.战斗类:快捷技能(self.快捷技能栏数据[4].名称)
  elseif self.快捷技能栏数据[5]~=nil and 引擎.按键按下(KEY.F5)  then
   tp.战斗类:快捷技能(self.快捷技能栏数据[5].名称)
  elseif self.快捷技能栏数据[6]~=nil and 引擎.按键按下(KEY.F6)  then
    tp.战斗类:快捷技能(self.快捷技能栏数据[6].名称)
  elseif self.快捷技能栏数据[7]~=nil and 引擎.按键按下(KEY.F7) then
   tp.战斗类:快捷技能(self.快捷技能栏数据[7].名称)
  elseif   self.快捷技能栏数据[8]~=nil and 引擎.按键按下(KEY.F8) then
   tp.战斗类:快捷技能(self.快捷技能栏数据[8].名称)
  end
end

  local topk = 0
  local tempv = 0
  for k, v in pairs(self.层序组) do
    if yxjc == 2 and self.界面数据[v].本类开关 and self.界面数据[v]:界面重叠() then
        topk = k
        tempv = v
        self.top_for_map = topk
        break
    end
  end
  local tempk = 0
  if topk > 0 then
      引擎.重置锁定(0)
      if 引擎.鼠标按下(0x0) then
          tempk = topk
          -- print(tempv)
      end
  end
  if(tempk > 0) then
     for k = tempk, 2, -1 do
         self.层序组[k] = self.层序组[k - 1]
     end
     self.层序组[1] = tempv
     -- local str = ""
     --  for k,v in pairs(self.层序组) do
     --      str = str .. v .. ','
     --    end
     --  print(str)
  end

  for k, v in pairs(self.层序组) do
    if yxjc == 2 and self.界面数据[v].本类开关 then
      -- if(v == 8 or v == 10) then 引擎.重置锁定(0) end
      self.界面数据[v]:更新(dt)
      -- if v ~= 3 and v == tempv and not self.界面数据[8].本类开关 then
        -- if 引擎.鼠标弹起(0) then 引擎.鼠标锁定(0)end
        -- if 引擎.鼠标弹起(1) then 引擎.鼠标锁定(1)end
      -- end
    end
  end
end

function 主界面类:显示(dt,x, y)

--    if  self.抓取技能~= nil and 引擎.鼠标弹起(0x1) then
--             self.抓取技能 =nil
--           self.抓取技能id = nil
--           self.抓取技能类型= nil
--   end
-- if self.快捷技能显示 and  yxjc== 3 and self.快捷技能栏数据~=nil  then
--   if self.快捷技能栏数据[1]~=nil and 引擎.按键按下(KEY.F1)  then
--     tp.战斗类:快捷技能(self.快捷技能栏数据[1].名称)
--   elseif self.快捷技能栏数据[2]~=nil and 引擎.按键按下(KEY.F2)  then
--     tp.战斗类:快捷技能(self.快捷技能栏数据[2].名称)
--   elseif self.快捷技能栏数据[3]~=nil and 引擎.按键按下(KEY.F3)  then
--    tp.战斗类:快捷技能(self.快捷技能栏数据[3].名称)
--   elseif self.快捷技能栏数据[4]~=nil and 引擎.按键按下(KEY.F4)  then
--    tp.战斗类:快捷技能(self.快捷技能栏数据[4].名称)
--   elseif self.快捷技能栏数据[5]~=nil and 引擎.按键按下(KEY.F5)  then
--    tp.战斗类:快捷技能(self.快捷技能栏数据[5].名称)
--   elseif self.快捷技能栏数据[6]~=nil and 引擎.按键按下(KEY.F6)  then
--     tp.战斗类:快捷技能(self.快捷技能栏数据[6].名称)
--   elseif self.快捷技能栏数据[7]~=nil and 引擎.按键按下(KEY.F7) then
--    tp.战斗类:快捷技能(self.快捷技能栏数据[7].名称)
--   elseif   self.快捷技能栏数据[8]~=nil and 引擎.按键按下(KEY.F8) then
--    tp.战斗类:快捷技能(self.快捷技能栏数据[8].名称)
--   end

-- end

--   local tempk = 0
--   local tempv
--   for k, v in pairs(self.层序组) do
--     if yxjc == 2 and self.界面数据[v].本类开关 and 引擎.鼠标按下(0x0) and self.界面数据[v]:界面重叠() then
--         tempk = k
--         tempv = v
--         break
--     end
--   end
--   if(tempk > 0) then
--      for k = tempk, 2, -1 do
--          self.层序组[k] = self.层序组[k - 1]
--      end
--      self.层序组[1] = tempv
--      local str = ""
--       for k,v in pairs(self.层序组) do
--           str = str .. v .. ','
--         end
--       print(str)
--   end

--   for k, v in pairs(self.层序组) do
--     if yxjc == 2 and self.界面数据[v].本类开关 then
--       self.界面数据[v]:更新(dt)
--     end
--   end





	-- for n = 1, #self.界面数据, 1 do
	-- 	if yxjc == 2 and self.界面数据[n].本类开关 then
	-- 		self.界面数据[n]:更新(dt)
	-- 	end
	-- end

     if self.快捷技能显示 then
  for i=1,8 do
       self.格子[i]:显示(427+i*33,535)
      if self.快捷技能栏数据~=nil and self.快捷技能栏数据[i] then
        local xsd = 引擎.取技能(self.快捷技能栏数据[i].名称)
          self.快捷技能栏数据[i].剩余冷却回合 = xsd[12]
          self.快捷技能栏数据[i].介绍=xsd[1]
          self.快捷技能栏数据[i].消耗说明=xsd[4]
          self.快捷技能栏数据[i].使用条件=xsd[5]
          self.快捷技能栏数据[i].小模型 = tp.资源:载入(xsd[6],"网易WDF动画",xsd[8])
        if self.图片组[i] == nil or self.图片组[i].名称 ~= self.快捷技能栏数据[i].名称 then
          self.图片组[i] = tp.资源:载入(xsd[6],"网易WDF动画",xsd[8])
        end

        self.图片组[i]:显示(431+i*33,539)
        if self.图片组[i]:是否选中(鼠标.x,鼠标.y)  then
          tp.提示:技能(鼠标.x-130,鼠标.y,self.快捷技能栏数据[i],nil,true)
        end
      end

       if self.格子[i]:是否选中(鼠标.x,鼠标.y) and yxjc~=3 then
        if 引擎.鼠标弹起(0) and self.抓取技能 ~= nil then
             客户端:发送数据(i,1, 9960,self.抓取技能.名称, 6)
          self.抓取技能 =nil
          self.抓取技能id =nil
        elseif 引擎.鼠标弹起(0) and self.抓取技能 == nil and self.图片组[i]~= nil then
          self.抓取技能 =self.快捷技能栏数据[i]
          self.抓取技能id =i
          self.图片组[i]= nil
          客户端:发送数据(i, 2, 9960, self.抓取技能.名称, 1)
        end
       end
      zts1:显示(435+i*33,519,"F"..i)
  end
     end
 --  if self.界面数据[48].本类开关 then
	-- 	self.界面数据[48]:显示()
	-- end
	-- for n = 1, #self.界面数据, 1 do
	-- 	if yxjc == 2 and self.界面数据[n].本类开关 and n ~= 21 and n ~= 8 and n~= 48 then
	-- 		self.界面数据[n]:显示()
	-- 	end
	-- end

  local v
  for k = #self.层序组, 1, -1 do
    v = self.层序组[k]
    if v == 8 then do end elseif (v == 21 or v == 48 or yxjc == 2) and self.界面数据[v].本类开关 then
      -- if v == tempv then
      --   引擎.重置锁定(0)
      --   引擎.重置锁定(1)
      -- end
      self.界面数据[v]:显示()
      -- if v == tempv then
      --   if 引擎.鼠标弹起(0) then 引擎.鼠标锁定(0)end
      --   if 引擎.鼠标弹起(1) then 引擎.鼠标锁定(1)end
      -- end
    end
  end

	-- if self.界面数据[21].本类开关 then
	-- 	self.界面数据[21]:显示()
	-- end

	if self.界面数据[8].本类开关 then
		self.界面数据[8]:显示()
	end
	  if  self.抓取技能~= nil  then
    self.抓取技能.小模型:显示(鼠标.x-10,鼠标.y-10)
  end
end

return 主界面类
