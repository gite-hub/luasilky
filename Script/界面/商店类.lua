local 商店类 = class()

function 商店类:初始化()
	self.背景 = 图像类("imge/001/574-1.png")
	self.选中显示 = 图像类("imge/001/125-1.png")
	self.本类开关 = false
	self.银两文字 = 文字类.创建(simsun, 15)
	self.银两文字1 = 文字类.创建(simsun, 15)
	self.银两文字2 = 文字类.创建(simsun, 15)
	self.银两文字3 = 文字类.创建(simsun, 15)

	self.银两文字3:置颜色(黑色)

	self.改名文字 = 文字类(simsun, 15)

	self.改名文字:置颜色(黑色)

	self.银两数额 = 0
	self.购买 = 按钮类.创建("imge/001/0016.png", 2, "购买", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3)
	self.位置 = {
		{
			x = 192,
			y = 58
		},
		{
			x = 242,
			y = 58
		},
		{
			x = 292,
			y = 58
		},
		{
			x = 342,
			y = 58
		},
		{
			x = 392,
			y = 58
		},
		{
			x = 192,
			y = 108
		},
		{
			x = 242,
			y = 108
		},
		{
			x = 292,
			y = 108
		},
		{
			x = 342,
			y = 108
		},
		{
			x = 392,
			y = 108
		},
		{
			x = 192,
			y = 158
		},
		{
			x = 242,
			y = 158
		},
		{
			x = 292,
			y = 158
		},
		{
			x = 342,
			y = 158
		},
		{
			x = 392,
			y = 158
		},
		{
			x = 192,
			y = 208
		},
		{
			x = 242,
			y = 208
		},
		{
			x = 292,
			y = 208
		},
		{
			x = 342,
			y = 208
		},
		{
			x = 392,
			y = 208
		}
	}
end

function 商店类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 商店类:刷新(数据)

	self.本类开关 = true

	商品数量输入:置文本("1")
	商品数量输入:置可视(true)

	self.数据 = 数据
	self.组号 = 数据.组号
	self.物品数据 = {}
	self.商品编号 = 0

	for n = 1, #数据, 1 do
		self.物品数据[n] = 物品数据类.创建(数据[n], 2)
		self.物品数据[n].编号 = n
	end

	self.数量 = 1
	self.价格 = 0
	self.银两 = 数据.银子
	self.总额 = 0
end

function 商店类:更新(dt)
	_GUI:更新(dt, 鼠标.x, 鼠标.y)

	if 商品数量输入:取文本() ~= "" and self.数量 ~= 商品数量输入:取文本() + 0 then
		self.数量 = 商品数量输入:取文本()
		self.总额 = self.数量 * self.价格
	end

	self.购买:更新(dt)
	self.取消:更新(dt)

	if self.购买:取是否单击() then
		if self.商品编号 == 0 then
			信息提示:加入提示("#y/请先选择一件商品")
		else
			self.发送信息 = {
				编号 = self.物品数据[self.商品编号].编号,
				数量 = self.数量,
				组号 = self.组号
			}

			客户端:发送数据(self.物品数据[self.商品编号].编号, self.数量, 14, self.组号)
		end
	elseif self.取消:取是否单击() then
		self.本类开关 = false

		商品数量输入:置可视(false)
	end



	if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then
		local x, y = 鼠标.x - self.背景.x - 10, 鼠标.y - self.背景.y - 36
		if(x < 0 or y < 0 or x > 260 or y > 210) then self.本类开关 = false 商品数量输入:置可视(false) end
	end
end

function 商店类:显示(x, y)
	self.背景:显示(182, 20)
	银两显示(self.银两文字, self.银两, 310, 370)
	银两显示(self.银两文字1, self.价格, 310, 299)
	银两显示(self.银两文字2, self.总额, 310, 346)
	_GUI:显示(鼠标.x, 鼠标.y)
	self.购买:显示(288, 403)
	self.取消:显示(358, 403)

	self.选中编号 = 0

	 for n=1,#self.物品数据 do
	 	if self.物品数据[n] ~= nil and self.位置[n] ~= nil then
			self.物品数据[n]:显示(self.位置[n].x, self.位置[n].y)

			if self.物品数据[n]:获取焦点() then
				self.选中编号 = n
			end
		end
	end

	if self.商品编号 ~= 0 then
		self.选中显示:显示(self.位置[self.商品编号].x - 2, self.位置[self.商品编号].y - 3)
	end

	if self.选中编号 ~= 0 then

		self.物品数据[self.选中编号]:显示事件()
        if 引擎.鼠标弹起(左键) then

		   self.价格=self.数据[self.选中编号].价格
            if self.商品编号==self.选中编号 then
                self.数量 = self.数量 + 1
                self.总额 = self.数量 * self.价格

				商品数量输入:置文本(self.数量)

				return 0
			else
				self.数量 = 1
				self.总额 = self.数量 * self.价格

				商品数量输入:置文本(self.数量)
			end

			self.商品编号 = self.选中编号
		elseif 引擎.鼠标弹起(左键) and self.商品编号 == self.选中编号 and self.数量 >= 1 then
			self.数量 = self.数量 - 1
			self.总额 = self.数量 * self.价格

			商品数量输入:置文本(self.数量)
		end
	end
end

return 商店类
