local 商会购买类 = class()

function 商会购买类:初始化()
	self.背景 = 图像类("imge/001/357-1.png")
	self.背景1 = 图像类("imge/001/358-1.png")
	self.背景2 = 图像类("imge/001/415-1.png")
	self.本类开关 = false
	self.上页 = 按钮类.创建("imge/001/0016.png", 2, "上页", 3)
	self.下页 = 按钮类.创建("imge/001/0016.png", 2, "下页", 3)
	self.确定 = 按钮类.创建("imge/001/0016.png", 2, "确定", 3)
	self.离开 = 按钮类.创建("imge/001/0016.png", 2, "离开", 3)
	self.文字 = 文字类(simsun, 17)
	self.信息文字 = 文字类(simsun, 14)
	self.金钱文字 = 文字类(simsun, 16)

	self.金钱文字:置颜色(黄色)

	self.金钱文字1 = 文字类(simsun, 14)
	self.信息文字1 = 文字类(simsun, 14)
	self.信息文字2 = 文字类(simsun, 14)

	self.信息文字2:置颜色(黑色)

	self.信息文字3 = 文字类(simsun, 17)

	self.信息文字3:置颜色(黄色)
	self.信息文字:置颜色(蓝色)

	self.选中动画 = 图像类("imge/001/125-1.png")
	self.购买开关 = false
	self.上间 = 按钮类.创建("imge/001/0016.png", 2, "上间", 3)
	self.下间 = 按钮类.创建("imge/001/0016.png", 2, "下间", 3)
	self.取消 = 按钮类.创建("imge/001/0016.png", 2, "离开", 3)
	self.购买 = 按钮类.创建("imge/001/0016.png", 2, "购买", 3)
end

function 商会购买类:创建购买界面(数据)
	self.购买开关 = true
	self.购买数据 = 数据
	self.商会选中 = 0
	self.物品数据1 = {}

	for n = 1, 20, 1 do
		if self.购买数据[n] ~= nil then
			self.物品数据1[n] = 物品数据类.创建(self.购买数据[n].道具, 3, self.当前类型, n)
			self.物品数据1[n].状态 = self.购买数据[n].状态
			self.物品数据1[n].价格 = self.购买数据[n].价格
		end
	end
end

function 商会购买类:刷新(数据)
	self.购买开关 = false
	self.本类开关 = true
	self.店铺信息 = 数据
	self.显示序列 = 1
	self.坐标重置 = 0
	self.商品选中 = 0

	for n = 1, #self.店铺信息, 1 do
		self.坐标重置 = self.坐标重置 + 1

		if self.坐标重置 > 8 then
			self.坐标重置 = 1
		end

		self.店铺信息[n].包围盒 = 包围盒:创建(150, 192, 64, 18)

		self.店铺信息[n].包围盒:置坐标(272, 130 + self.坐标重置 * 20)
		self.店铺信息[n].包围盒:更新宽高(string.len(self.店铺信息[n].店名) * 9, 20)
	end
end

function 商会购买类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 商会购买类:更新(dt)
	if self.购买开关 then
		self.上间:更新()
		self.下间:更新()
		self.购买:更新()
		self.取消:更新()

		if self.取消:取是否单击() then
			self.购买开关 = false
		elseif self.上间:取是否单击() then
			self.发送信息 = {
				类型 = self.购买数据.类型,
				商铺id = self.购买数据.商铺id
			}

			客户端:发送数据(self.购买数据.类型, 131, 36, self.店铺信息[self.商品选中].商铺id, 1)
		elseif self.下间:取是否单击() then
			self.发送信息 = {
				类型 = self.购买数据.类型,
				商铺id = self.购买数据.商铺id
			}

			客户端:发送数据(self.购买数据.类型, 132, 36, self.店铺信息[self.商品选中].商铺id, 1)
		elseif self.购买:取是否单击() then
			if self.商会选中 == 0 then
				信息提示:加入提示("#y/请先选中要购买的商品")

				return 0
			end

			self.发送信息 = {
				类型 = self.购买数据.类型,
				商铺id = self.购买数据.商铺id,
				商品id = self.商会选中
			}

			客户端:发送数据(self.购买数据.类型, self.商会选中, 38, self.店铺信息[self.商品选中].商铺id, 1)
		end


		if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then
	    	local x, y = 鼠标.x - self.背景.x - 22, 鼠标.y - self.背景.y - 110
	    	if(x < 0 or y < 0 or x > 260 or y > 210) then self.购买开关 = false end
	    end

	else
		self.上页:更新()
		self.下页:更新()
		self.确定:更新()
		self.离开:更新()

		if self.上页:取是否单击() and self.购买开关 == false then
			if self.显示序列 <= 1 then
				信息提示:加入提示("#y/当前已经是第一页了")
			else
				self.显示序列 = self.显示序列 - 1
			end
		elseif self.下页:取是否单击() and self.购买开关 == false then
			if self.显示序列 * 8 + 1 > #self.店铺信息 then
				信息提示:加入提示("#y/当前已经是最后一页了")
			else
				self.显示序列 = self.显示序列 + 1
			end
		elseif self.离开:取是否单击() and self.购买开关 == false then
			self.本类开关 = false
		elseif self.确定:取是否单击() and self.购买开关 == false then
			if self.商品选中 == 0 then
				信息提示:加入提示("#y/请先选中一个商店")
			else
				客户端:发送数据(7, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
			end
		end


		if 引擎.鼠标弹起(1) and self.背景:取包围盒():检查点(鼠标.x,鼠标.y) then
	    	local x, y = 鼠标.x - self.背景.x - 20, 鼠标.y - self.背景.y - 88
	    	if(x < 0 or y < 0 or x > 240 or y > 190) then
	    		self.本类开关 = false
	    	else
	    		if(self.商品选中 and self.商品选中 > 0) then
		    		y = y + 10 - (self.商品选中 % 8) * 20
		    		print(y)
		    		if(x > 0 and y > 0 and x < 240 and y < 20) then
		    			客户端:发送数据(7, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
		    		end
		    	end
	    	end
	    end

	end

end

function 商会购买类:显示(x, y)
	self.背景:显示(250, 50)
	self.上页:显示(280, 444)
	self.下页:显示(340, 444)
	self.确定:显示(400, 444)
	self.离开:显示(460, 444)

	for n = 1, 8, 1 do
		self.显示起始 = (self.显示序列 - 1) * 8 + n

		if self.店铺信息[self.显示起始] ~= nil then
			if self.商品选中 == self.显示起始 then
				self.文字:置颜色(红色):显示(272, 130 + n * 20, self.店铺信息[self.显示起始].店名)
				self.文字:置颜色(红色):显示(402, 130 + n * 20, self.店铺信息[self.显示起始].店面 .. "/" .. self.店铺信息[self.显示起始].店面)
				self.文字:置颜色(红色):显示(452, 130 + n * 20, "物品店")
			else
				self.文字:置颜色(黑色):显示(272, 130 + n * 20, self.店铺信息[self.显示起始].店名)
				self.文字:置颜色(黑色):显示(402, 130 + n * 20, self.店铺信息[self.显示起始].店面 .. "/" .. self.店铺信息[self.显示起始].店面)
				self.文字:置颜色(黑色):显示(452, 130 + n * 20, "物品店")
			end

			if self.店铺信息[self.显示起始].包围盒:检查点(鼠标.x, 鼠标.y) and 引擎.鼠标弹起(KEY.LBUTTON) and self.购买开关 == false then
				self.商品选中 = self.显示起始
			end
		end
	end

	if self.商品选中 ~= 0 then
		self.信息文字:显示(270, 343, "本店创始于" .. self.店铺信息[self.商品选中].创店日期 .. "  名店折扣:  0%")
		self.信息文字:显示(270, 360, "店主：" .. self.店铺信息[self.商品选中].店主名称 .. "  ID:" .. self.店铺信息[self.商品选中].店主id)
		self.信息文字:显示(270, 377, self.店铺信息[self.商品选中].宗旨)
	end

	if self.购买开关 then
		self:购买显示()
	end
end

function 商会购买类:购买显示(x, y)
	self.背景1:显示(250, 50)
	self.背景2:显示(269, 158)
	self.信息文字1:显示(370, 109, self.购买数据.创店日期)
	self.信息文字1:显示(488, 87, "0%")
	self.信息文字2:显示(309, 133, self.购买数据.店主名称)
	self.信息文字2:显示(453, 132, self.购买数据.店主id)
	self.信息文字2:显示(307, 446, self.购买数据.类型 .. "/" .. self.购买数据.店面)
	self.信息文字3:显示(265, 88, self.购买数据.店名)
	self.金钱文字:显示(350, 412, self.购买数据.现金)
	self.上间:显示(350, 444)
	self.下间:显示(395, 444)
	self.购买:显示(440, 444)
	self.取消:显示(485, 444)

	self.临时x = 0
	self.临时y = 0
	self.显示格子 = 0

	for n = 1, 20, 1 do
		self.临时x = self.临时x + 1

		if self.临时x == 6 then
			self.临时y = self.临时y + 1
			self.临时x = 1

			if self.临时y == 5 then
				self.临时y = 1
			end
		end

		if self.物品数据1[n] ~= nil then
			self.物品数据1[n].小动画:显示(217 + self.临时x * 52, 158 + self.临时y * 52)

			if self.商会选中 == n then
				self.选中动画:显示(217 + self.临时x * 52 + 1, 158 + self.临时y * 52 - 5)
			end

			if self.物品数据1[n]:获取焦点() then
				self.显示格子 = n

				if 引擎.鼠标弹起(键盘符号.左键) then
					self.商会选中 = n
				end
			end
		end
	end

	if self.显示格子 ~= 0 then
		self.物品数据1[self.显示格子]:显示事件(2)
	end

	if self.商会选中 ~= 0 then
		银两显示(self.金钱文字1, self.物品数据1[self.商会选中].价格, 353, 383)
	end
end

return 商会购买类
