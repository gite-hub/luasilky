local 玩家给予类 = class()

function 玩家给予类:初始化()
	self.本类开关 = false
	self.背景 = 图像类("imge/001/2388-1.png")
	self.说明字体 = 文字类.创建(simsun, 14)
	self.给予按钮 = 按钮类.创建("imge/001/0016.png", 2, "给予", 3, 1)
	self.取消按钮 = 按钮类.创建("imge/001/0016.png", 2, "取消", 3, 1)
	self.给予对象 = 1
end

function 玩家给予类:增加格子(编号)
	self.赋值 = false

	for n = 1, 3, 1 do
		if self.格子[n] == 0 and self.赋值 == false then
			self.格子[n] = 编号
			self.赋值 = true
		end
	end
end

function 玩家给予类:刷新(数据)
	self.本类数据 = 数据

	给予银两输入:置可视(true, "给予银两输入")

	self.本类数据.id = self.本类数据.id + 0
	self.给予名称 = self.本类数据.名称
	self.给予等级 = self.本类数据.等级
	self.给予id = self.本类数据.id
	self.本类开关 = true
	self.格子 = {
		0,
		0,
		0
	}
	self.物品数据 = {}

	for n = 1, 20, 1 do
		if self.本类数据.道具[n] ~= nil then
			self.物品数据[n] = 物品数据类.创建(self.本类数据.道具[n], 3, "包裹", n)
			self.物品数据[n].编号 = self.本类数据.道具[n].编号
		end
	end
end

function 玩家给予类:界面重叠()
	if self.背景:取包围盒():检查点(鼠标.x, 鼠标.y) then
		return true
	else
		return false
	end
end

function 玩家给予类:更新(dt)
	_GUI:更新(dt, 鼠标.x, 鼠标.y)
	self.给予按钮:更新(dt)

	if self.给予按钮:取是否单击() then
		self:交易物品()
	end

	self.取消按钮:更新(dt)

	if self.取消按钮:取是否单击() then
		self.本类开关 = false

		给予银两输入:置可视(false, "给予银两输入")
	end

	if 给予银两输入:取文本() ~= "" and self.本类数据.道具.银两 < 给予银两输入:取文本() + 0 then
		给予银两输入:置文本(self.本类数据.道具.银两)
	end
end

function 玩家给予类:交易物品()
	self.物品存在 = 0

	for n = 1, 3, 1 do
		if self.格子[n] == 0 then
			self.物品存在 = self.物品存在 + 1
		end
	end

	if 给予银两输入:取文本() == "" then
		self.临时银两 = 0
	else
		self.临时银两 = 给予银两输入:取文本() + 0
	end

	if self.物品存在 == 3 and self.临时银两 == 0 then
		信息提示:加入提示("#y/你要给予对方什么？")
	else
		self.格子信息 = ""

		for n = 1, #self.格子, 1 do
			self.格子信息 = self.格子信息 .. self.格子[n] .. "*-*"
		end

		客户端:发送数据(self.本类数据.id, self.临时银两, 29, self.格子信息, 1)

		self.本类开关 = false

		给予银两输入:置可视(false, "给予银两输入")
	end
end

function 玩家给予类:显示(x, y)
	self.背景:显示(250, 20)
	self.给予按钮:显示(320, 440)
	self.取消按钮:显示(400, 440)
	self.说明字体:置颜色(黄色):显示(258, 410, "对象:")
	self.说明字体:置颜色(黄色):显示(295, 410, self.给予名称)
	self.说明字体:置颜色(红色):显示(435, 414, self.给予等级)
	银两显示(self.说明字体, self.本类数据.道具.银两, 295, 385)
	_GUI:显示(鼠标.x, 鼠标.y)

	self.焦点格子 = 0
	self.a = 0
	self.b = 0

	for n = 1, 20, 1 do
		self.a = n / 5

		if self.a < 1 then
			self.a = 1
		elseif self.a > 1 and self.a < 2 then
			self.a = 2
		elseif self.a > 2 and self.a < 3 then
			self.a = 3
		elseif self.a > 3 and self.a < 4 then
			self.a = 4
		end

		self.b = self.b + 1

		if self.b == 6 then
			self.b = 1
		end

		if self.物品数据[n] ~= nil and n ~= self.格子[1] and n ~= self.格子[2] and n ~= self.格子[3] then
			self.物品数据[n]:显示(260 + self.b * 50 - 50, 57 + self.a * 50 - 50, 2)

			if self.物品数据[n]:获取焦点() then
				self.焦点格子 = n
			end
		end
	end

	for n = 1, 3, 1 do
		if self.格子[n] ~= 0 then
			self.物品数据[self.格子[n]]:显示(202 + n * 80, 294, 2)

			if self.物品数据[self.格子[n]]:获取焦点() then
				self.物品数据[self.格子[n]]:显示事件(2)

				if 引擎.鼠标弹起(右键) then
					self.格子[n] = 0
				end
			end
		end
	end

	if self.焦点格子 ~= 0 then
		self.物品数据[self.焦点格子]:显示事件(2)

		if 引擎.鼠标弹起(左键) then
			self:增加格子(self.焦点格子)
		end
	end
end

return 玩家给予类
