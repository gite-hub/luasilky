
local 角色动画类 = class()
local zdjy = 取锦衣模型()
local zdjyjy = 取锦衣身体模型()


function 角色动画类:初始化(造型,染色,武器名称,武器级别,武器类型,武器强化,坐骑,锦衣数据)

  self.偏移={x=0,y=0}
 self.按帧更新=false

  if 武器名称=="" or 武器名称==nil or (坐骑~=nil and (锦衣数据.锦衣 == nil or 锦衣数据.锦衣 == "")) or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "蜜蜂嗡嗡" )or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "顽皮小恶魔" ) or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "铃儿叮当" ) or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "尖叫服" ) or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "锦绣幻梦" ) or (锦衣数据~=nil and 锦衣数据.锦衣 ~= nil and 锦衣数据.锦衣 == "萤雪轻裘" )  then
        self.武器=nil
     local 资源= 引擎.取模型(造型)
        if 染色~=nil then
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1],造型,染色)
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2],造型,染色)
        else
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1])
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2])
        end
  else
      local v = 武器类型
        if 武器名称 == "龙鸣寒水" or 武器名称 == "非攻" then
          v = "弓弩1"
        end
        local 资源= 引擎.取模型(造型,v)
        if 染色~=nil then
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1],造型,染色)
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2],造型,染色)
        else
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1])
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2])
        end
        self:创建武器(武器名称,武器类型,武器级别,造型)
   end

    if 坐骑~=nil and (锦衣数据.锦衣 == nil or 锦衣数据.锦衣 == "") then
      self.临时资源=引擎.读ini("人物",self.临时字符)
     引擎.置ini("坐骑动画.ini")

      if 坐骑== "暗影战豹" and 造型 == "飞燕女"then
       self.偏移={x=0,y=-3}
      end

        if 染色~=nil then
          self.静立=引擎.场景.资源:载入("ZY.FT","网易WDF动画",引擎.读ini("人物",造型.."_"..坐骑.."_".."站立"),造型,染色)
          self.行走= 引擎.场景.资源:载入("ZY.FT","网易WDF动画",引擎.读ini("人物",造型.."_"..坐骑.."_".."奔跑"),造型,染色)
        else
          self.静立=引擎.场景.资源:载入("ZY.FT","网易WDF动画",引擎.读ini("人物",造型.."_"..坐骑.."_".."站立"))
          self.行走= 引擎.场景.资源:载入("ZY.FT","网易WDF动画",引擎.读ini("人物",造型.."_"..坐骑.."_".."奔跑"))
        end

      self.坐骑={}
      self.临时字符=坐骑.."_".."原始".."_".."站立"
      self.临时资源=引擎.读ini("原始坐骑",self.临时字符)
      self.坐骑["静立"]=引擎.场景.资源:载入("ZY.FT","网易WDF动画",self.临时资源)
      self.临时字符=坐骑.."_".."原始".."_".."奔跑"
      self.临时资源=引擎.读ini("原始坐骑",self.临时字符)
      self.坐骑["行走"]=引擎.场景.资源:载入("ZY.FT","网易WDF动画",self.临时资源)
    end

    if 锦衣数据 then
     if 锦衣数据.光环~=nil  then
      self.光环= {}
      local n=引擎.取光环(锦衣数据.光环)
          self.光环.静立=引擎.场景.资源:载入("ZY.FT","网易WDF动画",n[1])
          self.光环.行走= 引擎.场景.资源:载入("ZY.FT","网易WDF动画",n[2])
    else
      self.光环=nil
     end

    if 锦衣数据.足迹~=nil  then
      self.足迹= {}
      local n=引擎.取足迹(锦衣数据.足迹)
          self.足迹.行走= 引擎.场景.资源:载入("ZY.FT","网易WDF动画",n[1])
    else
      self.足迹=nil
    end

    if  锦衣数据.锦衣  then
        if 锦衣数据.锦衣 == "华风汉雅" or 锦衣数据.锦衣 == "夏日清凉" or 锦衣数据.锦衣 == "萌萌小厨" or 锦衣数据.锦衣 == "夜影" then
          local 临时资源
          local 临时资源包 = "shape2.npk"
          if  锦衣数据.锦衣 == "夏日清凉" or 锦衣数据.锦衣 == "夜影" then
              临时资源包 = "shape0.npk"
          end
          临时资源  = 造型..武器类型.."_静立_"..锦衣数据.锦衣
          临时资源  = 取锦衣模型(临时资源)
          self.静立=引擎.场景.资源:载入(临时资源包,"网易锦衣动画",临时资源)
          临时锦衣字符  = 造型..武器类型.."_行走_"..锦衣数据.锦衣
          临时资源  = 造型..武器类型.."_行走_"..锦衣数据.锦衣
          临时资源  = 取锦衣模型(临时资源)
          self.行走= 引擎.场景.资源:载入(临时资源包,"网易锦衣动画",临时资源)
          if 锦衣数据.锦衣 == "华风汉雅" or 锦衣数据.锦衣 == "夏日清凉" or 锦衣数据.锦衣 == "萌萌小厨" then

            临时资源 = 造型..武器类型.."_静立_"..锦衣数据.锦衣.."_头"
            临时资源=取锦衣身体模型(临时资源)
            self.静立.身体 =引擎.场景.资源:载入("shape0.npk","网易锦衣动画",临时资源)
            临时资源 = 造型..武器类型.."_行走_"..锦衣数据.锦衣.."_头"
            临时资源=取锦衣身体模型(临时资源)
            self.行走.身体 = 引擎.场景.资源:载入("shape0.npk","网易锦衣动画",临时资源)
          end
        else
          local zy= 引擎.取模型(锦衣数据.锦衣.."_"..造型)
          self.静立=引擎.场景.资源:载入(zy[3],"网易WDF动画",zy[1])
          self.行走= 引擎.场景.资源:载入(zy[3],"网易WDF动画",zy[2])
        end

    end
  end

end
function 角色动画类:取包围盒(类型)
  return self[类型].精灵:取包围盒()
 end

function 角色动画类:取当前帧(类型)
  return self[类型].当前帧
 end

function 角色动画类:是否选中(类型,x,y)
  return self[类型]:是否选中(鼠标.x,鼠标.y)
end

function 角色动画类:取结束帧(类型)
  return self[类型].结束帧
 end

 function 角色动画类:取开始帧(类型)
  return self[类型].开始帧
 end

function 角色动画类:置颜色(类型,颜色)
  return self[类型].精灵:置颜色(颜色)
 end

function 角色动画类:创建武器(名称,类别,等级,造型)
  self.武器=nil
  if 名称~="" then
    self.武器={}
    local m = 引擎.场景:qfjmc(类别,等级,名称)
    local n = 引擎.取模型(m.."_"..造型)
    self.武器.静立=引擎.场景.资源:载入(n[3],"网易WDF动画",n[1])
    self.武器.行走= 引擎.场景.资源:载入(n[3],"网易WDF动画",n[2])
  end
 end

function 角色动画类:置帧率(类型,帧率)
  self[类型]:置提速(帧率)
  if self[类型].身体 ~= nil then
    self[类型].身体:置提速(帧率)
  end
  if self.光环~=nil and self.光环[类型]~=nil then
    self.光环[类型]:置提速(帧率)
  end
  if self.足迹~=nil then
  if self.足迹.行走~=nil  then
    self.足迹.行走:置提速(帧率)
  end
end
  if self.武器~=nil then
    self.武器[类型]:置提速(帧率)
  end
  if self.坐骑~=nil and self.坐骑[类型]~=nil then
    self.坐骑[类型]:置提速(帧率)
  end
end

function 角色动画类:更新(dt,类型)
  if self[类型]==nil then return 0 end
  if self.按帧更新==false then
  if self.光环~=nil and self.光环[类型]~=nil then
     self.光环[类型]:更新(dt)
  end
 if self.足迹~=nil then
    if self.足迹.行走~=nil  then
      self.足迹.行走:更新(dt)
    end
 end
    if self.坐骑~=nil and self.坐骑[类型]~=nil then
      self.坐骑[类型]:更新(dt)
    end
    self[类型]:更新(dt)
    if self[类型].身体 ~= nil then
      self[类型].身体:更新(dt)
    end
    if self.武器~=nil then
      self.武器[类型]:更新(dt)
      if self.武器[类型].帧率~=self[类型].帧率 then self.武器[类型].帧率=self[类型].帧率 end
    end
  end
end

function 角色动画类:显示(类型,x,y)
  if self.光环~=nil then
    self.光环[类型]:显示(x,y)
  end
if self.足迹~=nil then
    if 类型=="行走" then
    self.足迹.行走:显示(x,y)
  end
end
  引擎.场景.影子:显示(x,y)
  if self.坐骑~=nil and self.坐骑[类型]~=nil then
    self.坐骑[类型]:显示(x,y)
  end
  if self[类型]==nil then
    return 0
  end
  if self[类型].身体 ~= nil then
      self[类型].身体:显示(x+self.偏移.x,y+self.偏移.y)
  end
  self[类型]:显示(x+self.偏移.x,y+self.偏移.y)
  if self.武器~=nil then
    self.武器[类型]:显示(x,y)
  end
end

function 角色动画类:换方向(类型,方向)
  self[类型]:置方向(方向)
  if self[类型].身体 ~= nil then
      self[类型].身体:置方向(方向)
  end
  if self.坐骑~=nil and self.坐骑[类型]~=nil then
    self.坐骑[类型]:置方向(方向)
  end
  if self.足迹~=nil then
    self.足迹.行走:置方向(方向)
  end
  if  self.武器~=nil then
    self.武器[类型]:置方向(方向)
  end
end

return 角色动画类