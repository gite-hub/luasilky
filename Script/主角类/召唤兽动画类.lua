
local 召唤兽动画类 = class()
function 召唤兽动画类:初始化(造型,染色,饰品)
  local 资源= 引擎.取模型(造型)
        if 染色 then
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1],造型,染色,true)
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2],造型,染色,true)
        else
          self.静立=引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[1])
          self.行走= 引擎.场景.资源:载入(资源[3],"网易WDF动画",资源[2])
        end

       if 饰品 then
         if 取召唤兽饰品(造型) then
            local wq= 引擎.取模型("饰品_"..造型)
            self.饰品={
          静立=引擎.场景.资源:载入(wq[3],"网易WDF动画",wq[1]),
          行走= 引擎.场景.资源:载入(wq[3],"网易WDF动画",wq[2]),
          }
        end
      else
         if 取召唤兽武器(造型) then
            local wq= 引擎.取模型("武器_"..造型)
            self.武器={
          静立=引擎.场景.资源:载入(wq[3],"网易WDF动画",wq[1]),
          行走= 引擎.场景.资源:载入(wq[3],"网易WDF动画",wq[2]),
          }
        else
            self.武器=nil
        end
        self.饰品= nil
      end

end


function 召唤兽动画类:置帧率(类型,帧率)
  self[类型].帧率=帧率
  if self.饰品~=nil then
   if self.饰品[类型] ~=nil then
   self.饰品[类型].帧率=帧率
   end
  end

 end
function 召唤兽动画类:更新(dt,类型)
  if self[类型]==nil then
   return 0
  end
     self[类型]:更新(dt)
    if self.饰品~=nil and self.饰品[类型]~=nil then
        self.饰品[类型]:更新(dt)
    end
    if self.武器~=nil and self.武器[类型]~=nil then
        self.武器[类型]:更新(dt)
    end
end
function 召唤兽动画类:取包围盒(类型)
  return self[类型].精灵:取包围盒()
 end

function 召唤兽动画类:取当前帧(类型)
  return self[类型].当前帧
 end


function 召唤兽动画类:取结束帧(类型)
  return self[类型].结束帧
 end
function 召唤兽动画类:取开始帧(类型)
  return self[类型].开始帧
 end
function 召唤兽动画类:置颜色(类型,颜色)
  return self[类型].精灵:置颜色(颜色)
 end

function 召唤兽动画类:显示(类型,x,y)

 if self[类型]==nil then return 0 end
 引擎.场景.影子:显示(x,y)
 self[类型]:显示(x,y)
 if self.饰品~=nil and self.饰品[类型]~=nil then
  self.饰品[类型]:显示(x,y)
 end
   if self.武器~=nil and self.武器[类型]~=nil then
    self.武器[类型]:显示(x,y)
   end
end

function 召唤兽动画类:换方向(类型,方向,换帧)
  if self[类型]==nil then
    return 0
  end
   self[类型]:置方向(方向)
  if self.饰品~=nil and self.饰品[类型]~=nil then
    self.饰品[类型]:置方向(方向)
   end
  if self.武器~=nil and self.武器[类型]~=nil then
    self.武器[类型]:置方向(方向)
   end
end

return 召唤兽动画类